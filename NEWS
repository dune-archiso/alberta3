2023-11-29
  ALBERTA 3.1.0
  This release comes as a mayor overhaul for the build system and
  solves tons of warnings.

  - Require Autoconf 2.69 and Automake 1.15

  - C standard bumped to C11

  - Continuous testing for Debian, Ubuntu, openSuse, and macOS
    thanks to GitLab.com

  - All executables are no longer build as part of `make all`, the
    default target. They were moved to `make check` and serve as
    regression tests for the continuous testing system. As a
    consequence, these executable are no longer built by default
    and are not installed.

  - Drop support for Unixes beside Linux and macOS, namely
    DEC, HP, SGI, and SUN. They might still work; feel free to
    submit non-disruptive patches to keep your working
    environment working.

  - Disable ALBERTA option `plain-malloc` by default. Today other
    ways of debugging memory issues are available and should be
    used instead of this.

  - Removed most of gnu-compat/ - namely malloc and getops - as there
    are other ways to be compatible with Glibc. Only obstack remains
    as an updated copy from Glibc.

2018-11-12
	ALBERTA 3.0.3

	Fixes some long-standing silly bugs. Please have a look at the
	history at

        https://gitlab.mathematik.uni-stuttgart.de/ians-nmh/alberta/alberta3/

2015-10-03
	ALBERTA 3.0.2

	Get rid of libltdl when --disable-fem-toolbox is "active" and get rid
	of some compiler warnings when including the stuff into C++ code.

	Some of the basis function implementations had a chance to return
	a pointer to a local non-static buffer.

2014-05-07
	ALBERTA 3.0.1

	Update the copyright notice in the demo-package to a sane
	state + some minor issues.

2014-03-13
	ALBERTA 3.0.0

        Nothing really new for the past few years, just call the
        current state 3.0.0 and live with it. Actually, v3 has been
        stable at least since 2009. Some notes:

        - The documentation is probably again slightly out of
          sync. The demo-suite, however, compiles and works to my
          knowledge.

        - compat changes: the header files now have to be included like so:

          #include <alberta/alberta.h>

          Also, utility library now is called libalberta_utilities to enable
          side-by-side installation with alberta-1.2.

        - bug fixes, see ChangeLog

        Some notes concerning the Fremen:

        - to please people from the Dune community it is possible to
          omit the compilation of the FEM discretization stuff, what
          remains is a library which contains only the grid
          implementation (well, and other stuff ...) To do this, there
          is now a new library libalberta-fem which provides the
          FEM-toolbox if needed. The configure switch is called
          "--disable-fem-toolbox".

        - there is also another interesting "kill" switch
          "disable-graphics" which will inhibit the compilation of the
          code related to online-graphics. In effect this eliminates
          the dependency on X11 and OpenGL. "--disable-fem-toolbox"
          implies "--disable-graphics".

        - There are now some experimental pkgconfig files which are
          installed into their proper default location by "make
          install". Requested by Christoph Grüninger, also related to
          Dune.

2009-03-26
	ALBERTA 3.0-rc1

	- per default ALBERTA installs itself into the directory
          hierarchy below "/usr/local/". Previous versions of ALBERTA
          installed themselves into the build-directory.

	- separate graphics and "core" ALBERTA libraries, and a
          global configure-time graphics kill-switch
          "--disable-graphics".

	- BLAS is not used by default, but can optionally be enable by
          running configure with the "--enable-fortran-blas" switch.

        - easy-to-use (iso-)parametric meshes, up to piecewise polynomial
          degree of 4. The coordinate vectors are now dumped to disk
          along with the mesh-structure when calling write_mesh().

        - parametric meshes for arbitrary co-dimension for 1d/2d/3d meshes
          (computations on embedded 3-manifolds are now possible, e.g.)

        - periodic meshes; with non-periodic and periodic finite element
          spaces on the same mesh, definition via geometric or
          combinatorial face transformations.

        - support for non-oriented meshes; affects mainly 2d (think,
          e.g., of a Klein's bottle or a Moebius-strip).

        - sub-mesh hierarchies, with proper mapping of
          trace-spaces. Sub-meshes inherit the parametric and/or
          periodic structures of their master-meshes.

        - _optional_ per-element intializers for basis functions and
          quadratures, for extensions which require more complicated stuff
          than is provided by ALBERTA.

        - fast (i.e. caching) quadratures for faces.

        - marginally improved quadrature rules for 3d.

	- discontinuous orthogonal basis functions up to degree 2.

        - extended demo-program suite, with example programs for parametric
          meshes (iso-parametric, but also higher co-dimension),
          non-orientable surfaces, stationary Quasi-Stokes with stress
          boundary conditions, periodic meshes.

        - improved GRAPE interface, interfaces to GMV, Silo, Geomview,
          Paraview (with varying degree of completeness and
          usability).

2007-10-XX
	ALBERTA 2.0
	- support of submeshes, DIM constant removed
	
	- support of iso-parametric meshes up to p.w. polynomial degree 2

	- more generalized node projection mechanism

	- graphics output interface to GMV

	- discontinuous basis functions up to degree 2.

	- get_fe_space() works at any time

	- INDEX macro only available for ALBERTA_DEBUG=1

	- NEIGH_IN_EL, EL_INDEX macros removed

	- former SOLVER package incorporated into "alberta_util",
	  "PLOT_ANSI" package removed

	- ALBERTA_DEBUG=1 introduces additional safety checks

	- changed "preserve_coarse_dofs" mechanism

	- changed "fe_space" entry in DOF_[DOWB_]MATRIX into a
	  "row_fe_space" and a "col_fe_space". Fixed issue with
	  dof_compress() and different fe_spaces.

	- added a "DOF_PTR_VEC" type.

2004-12-07
	ALBERTA 1.2.1
	
	Bug fixes.

2004-07-30
	ALBERTA 1.2

	Renamed from ALBERT to ALBERTA (copyright issues)

	New features:
	- mixed methods
	- GRAPE interface

	Configuration
	- configure options are more consistent
	- it is possible to build only selected libraries
	- default CFLAGS are now -O3 (gcc) or -O (non-gcc), can be
	  overridden by environment variables or on the command line
	- it is possible to specify an alternate blas-library

	Removed:
	- distributed BLAS library

	Support:
	- ALBERTA now has a mailing list at

	http://www.mathematik.uni-freiburg.de/IAM/ALBERTA/mailinglist.html
