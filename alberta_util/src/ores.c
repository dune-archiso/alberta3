/*--------------------------------------------------------------------------*/
/*  Solving a linear system with method of orthogonal directions            */
/*  Classification: OrthoDir_d(A^*A,C,A)                                    */
/*  Variant:        sigma= Ad.Ad/Ad_old.Ad_old                              */
/*  Matrix:         symmetric                                               */
/*  Workspace:      7*dim                                                   */
/*                                                                          */
/*  C-version of OFM-lib by Willy Doerfler                                  */
/*                                                                          */
/*  author: Kunibert G. Siebert                                             */
/*          Institut fuer Mathematik                                        */
/*          Universitaet Augsburg                                           */
/*          Universitaetsstr. 14                                            */
/*          D-86159 Augsburg, Germany                                       */
/*                                                                          */
/*          http://scicomp.math.uni-augsburg.de/Siebert/                    */
/*                                                                          */
/* (c) by K.G. Siebert (2000-2003)                                          */
/*--------------------------------------------------------------------------*/

#include "alberta_util.h"
#include "alberta_util_intern.h"

int oem_ores(OEM_DATA *oem, int dim, const REAL *b, REAL *x)
{
  FUNCNAME("oem_ores");
  REAL  *Ad, *Ad_old, *Ah, *d, *d_old, *h, *r;
  REAL  daad_old, old_res = -1.0;

  int   (*mv)(void *, int, const REAL *, REAL *) = oem->mat_vec;
  void  *mvd = oem->mat_vec_data;
  void  (*precon)(void *, int, REAL *) = oem->left_precon;
  void  *pd = oem->left_precon_data;
  REAL  (*scp)(void *, int, const REAL *, const REAL *) = oem->scp;
  void  *sd = oem->scp_data;

  WORKSPACE  *ws = CHECK_WORKSPACE(7*dim, oem->ws);

/*--------------------------------------------------------------------------*/
/*---  partitioning of the workspace  --------------------------------------*/
/*--------------------------------------------------------------------------*/

  d       = (REAL *)ws->work;
  Ad      = d+dim;
  d_old   = Ad+dim;
  Ad_old  = d_old+dim;
  Ah      = Ad_old+dim;
  r       = Ah+dim;
  h       = r+dim;

/*--------------------------------------------------------------------------*/
/*---  Initalization  ------------------------------------------------------*/
/*--------------------------------------------------------------------------*/

  oem->terminate_reason = 0;

  int dim2 = 2*dim; /* Important: sequence of vectors in memory for use of dim2!*/

  (*mv)(mvd, dim, x, r);
  daxpy(dim, -1.0, b, 1, r, 1);
  dcopy(dim, r, 1, h, 1);

  if (precon) (*precon)(pd, dim, h);

  dcopy(dim, h, 1, d, 1);
  dset(dim2, 0.0, d_old, 1);  /*--- d_old = 0 and Ad_old = 0 ---------------*/
  (*mv)(mvd, dim, d, Ad);
  daad_old = 1.0;

/*--------------------------------------------------------------------------*/
/*---  check initial residual  ---------------------------------------------*/
/*--------------------------------------------------------------------------*/

  int res = scp ? sqrt((*scp)(sd, dim, r, r)) : dnrm2(dim, r, 1);
  oem->initial_residual = res;

  START_INFO(oem);
  if (SOLVE_INFO(oem, 0, res, &old_res, ws))
    return(0);

  int iter;
  for (iter = 1; iter <= oem->max_iter; iter++)
  {
/*--- compute r.Ad and d.A.A.d  ---------------------------------------------*/
    REAL rad  = scp ? (*scp)(sd, dim, r, Ad) : ddot(dim, r, 1, Ad, 1);
    REAL daad = scp ? (*scp)(sd, dim, Ad, Ad) : ddot(dim, Ad, 1, Ad, 1);

    if (daad <= 1.e-30)
    {
      oem->terminate_reason = 2;
      BREAK_INFO(oem, "(Ad,d)_2 = 0", iter, res, &old_res, ws);
      return(iter);
    }

/*--- update u and r  ------------------------------------------------------*/

    REAL alpha= rad/daad;
    daxpy(dim, -alpha, d, 1, x, 1);
    daxpy(dim, -alpha, Ad, 1, r, 1);

    res = scp ? sqrt((*scp)(sd, dim, r, r)) : dnrm2(dim, r, 1);

    if (SOLVE_INFO(oem, iter, res, &old_res, ws))
      return(iter);

    dcopy(dim, Ad, 1, h, 1);
    if (precon) (*precon)(pd, dim, h);

    (*mv)(mvd, dim, h, Ah);

    REAL gamma = (scp ? (*scp)(sd, dim, Ah, Ad) : ddot(dim, Ah, 1, Ad, 1))/daad;
    REAL sigma = daad/daad_old;

/*--- compute new d.Ad, save old values!  ----------------------------------*/
    dswap(dim2, d, 1, d_old, 1);         /*--- d_old = d, Ad_old = Ad ------*/
    dscal(dim2, -sigma, d, 1);           /*--- d,Ad *= -sigma --------------*/
    daxpy(dim2, -gamma, d_old, 1, d, 1); /*--- d,Ad -= gamma*d_old,Ad_old --*/
    daxpy(dim, 1.0, h, 1, d, 1);
    daxpy(dim, 1.0, Ah, 1, Ad, 1);
    daad_old = daad;
  }

  return(0);  /*--- statement never reached!!!  ----------------------------*/
}

const char *ores_strerror(int reason)
{
  switch (reason) {
//	case 1:
//		return "b == x_0 == 0, x = 0 is the solution of the linear system";

  case 2:
    return "(Ad,d)_2 = 0";


  default:
    break;
  }
  return "Unknown error code.";
}
