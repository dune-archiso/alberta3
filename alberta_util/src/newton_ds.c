/*--------------------------------------------------------------------------*/
/*  solve nonlinear systems by a Newton method with step size control       */
/*                                                                          */
/*  Workspace:      4*dim                                                   */
/*                                                                          */
/*  C-version of NGL-lib by Willy Doerfler                                  */
/*                                                                          */
/*  author: Kunibert G. Siebert                                             */
/*          Institut fuer Mathematik                                        */
/*          Universitaet Augsburg                                           */
/*          Universitaetsstr. 14                                            */
/*          D-86159 Augsburg, Germany                                       */
/*                                                                          */
/*          http://scicomp.math.uni-augsburg.de/Siebert/                    */
/*                                                                          */
/* (c) by K.G. Siebert (2000-2003)                                          */
/*--------------------------------------------------------------------------*/

#include "alberta_util.h"
#include "alberta_util_intern.h"

int nls_newton_ds(NLS_DATA *data, int dim, REAL *x)
{
  FUNCNAME("nls_newton_ds");
  REAL *b, *d, *db, *y;
  REAL err = 0.0, err_old = -1.0;
  REAL atol, factor, tau;
  int  iter, j, n, m, mmax, halved;

  int  info = data->info;
  void (*update)(void *, int, const REAL *, bool, REAL *) = data->update;
  void *ud = data->update_data;
  int  (*solve)(void *, int, const REAL *, REAL *) = data->solve;
  void *sd = data->solve_data;
  REAL (*norm)(void *, int, const REAL *) = data->norm;
  void *nd = data->norm_data;

  WORKSPACE  *ws = CHECK_WORKSPACE(4*dim, data->ws);

/*--- Memory initialization ------------------------------------------------*/
  b  = (REAL *)ws->work;
  d  = b+dim;
  db = d+dim;
  y  = db+dim;

/*--------------------------------------------------------------------------*/
/*--- Newton initialization ------------------------------------------------*/
/*--------------------------------------------------------------------------*/

  (*update)(ud, dim, x, 1, b);
/*--- Initial guess is zero ------------------------------------------------*/
  dset(dim, 0.0, d, 1);
/*--- solve linear system --------------------------------------------------*/
  n = (*solve)(sd, dim, b, d);
/*--- compute norm of |d| --------------------------------------------------*/
  err = err_old = norm ? (*norm)(nd, dim, d) : dnrm2(dim, d, 1);
  data->initial_residual = err;

  INFO(info,2,"iter. |     residual |     red. |    n |  m |\n");
  INFO(info,2,"%5d | %12.5le | -------- | %4d | -- |\n", 0, err, n);

  if ((data->residual = err) < data->tolerance)
  {
    INFO(info,4,"finished succesfully\n");
    if (ws != data->ws) FREE_WORKSPACE(ws);
    return(0);
  }

/*--- still initalization part ---------------------------------------------*/
  
  mmax        = MAX(2,MIN(data->restart,32));
  m           = 0;
  tau         = 1.0;
  halved      = true;
  atol        = sqrt(10.0*data->tolerance);

/*--- start iterations -----------------------------------------------------*/

  for (iter = 1; iter <= data->max_iter+1; iter++)
  {
/*--- look for step size ---------------------------------------------------*/
    if (!halved)
    {
      m = MAX(m-1,0);
      tau = tau < 0.5 ? 2.0*tau : 1.0;
    }
/*--- aim: |DF(u_k)^{-1} F(u_k+\tau d)| \le (1-0.5\tau) |d| ----------------*/
    
    for (j = 0; j <= mmax; j++)
    {
      dcopy(dim, x, 1, y, 1);             /*-- y = x -----------------------*/
      daxpy(dim, -tau, d, 1, y, 1);       /*-- y -= tau*d ------------------*/

      (*update)(ud, dim, y, 0, b);        /*-- update F(x) -----------------*/

      dset(dim, 0.0, db, 1);              /*-- initial guess is zero -------*/
      n = (*solve)(sd, dim, b, db);       /*-- simplified correct. ---------*/

      err = norm ? (*norm)(nd, dim, db) : dnrm2(dim, db, 1);

      factor = 1.0 - 0.5*tau;
      if (err <= factor*err_old)
      {
	halved = j > 0;
	break;
      }
      else
      {
	if (m == mmax) break;

	m = m+1;
	tau *= 0.5;
      }
    }

    dcopy(dim, y, 1, x, 1);               /*-- x = y  ----------------------*/

    if (err_old <= 0)
      INFO(info,2,"%5d | %12.5le | -------- | %4d | %2d |\n", 
		       iter, err, n, m);
    else
      INFO(info,2,"%5d | %12.5le | %8.2le | %4d | %2d |\n", 
		       iter, err, err/err_old, n, m);

    if ((data->residual = err) < data->tolerance && err < atol && m == 0)
    {
      dmxpy(dim, db, 1, x, 1);           /*--  x -= db ---------------------*/
      INFO(info,4,"finished successfully\n");
      if (ws != data->ws) FREE_WORKSPACE(ws);
      return(iter);
    } 
    else if (iter > data->max_iter)
    {
      dmxpy(dim, db, 1, x, 1);           /*--  x -= db ---------------------*/
      if (info < 2) {
	INFO(info,1,"iter. %d, residual: %12.5le\n", iter, err);
      }
      INFO(info,1,"tolerance %le not reached\n", data->tolerance);
      if (ws != data->ws) FREE_WORKSPACE(ws);
      return(iter);
    }

    (*update)(ud, dim, x, 1, NULL);       /*-- update DF(x) -----------------*/
    dset(dim, 0.0, d, 1);
    n = (*solve)(sd, dim, b, d);
/*--- compute norm of |d| --------------------------------------------------*/
    err_old = err;
    err = norm ? (*norm)(nd, dim, d) : dnrm2(dim, d, 1);
  }
  return(iter);  /*--- statement never reached -----------------------------*/
}
