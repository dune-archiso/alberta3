/*******************************************************************************
 *
 *
 *  authors:   Notger Noll
 *             Abteilung fuer angewandte Mathematik
 *             Albert Ludwigs Universitaet Freiburg
 *             Herman Herder Str. 10
 *             D-79106 Freiburg, Germany
 *
 *             Claus-Justus Heine
 *             Abteilung fuer Angewandte Mathematik
 *             Albert-Ludwigs-Universitaet Freiburg
 *             Hermann-Herder-Str. 10
 *             79104 Freiburg
 *             Germany
 *             Claus.Heine@Mathematik.Uni-Freiburg.DE
 *
 *  www.mathematik.uni-freiburg.de/IAM/ALBERTA
 *
 *  (c) by N. Noll (2008),
 *         C.-J. Heine (2008).
 *
 ******************************************************************************/

#include "alberta_util.h"
#include "alberta_util_intern.h"


const char *oem_strerror(OEM_SOLVER solver, int reason)
{
  FUNCNAME("oem_strerror");

  if (reason == 0)
    return "no errors";
  else if (reason == 1)
    return "b == x_0 == 0, x = 0 is the solution of the linear system";
  else
    switch (solver) {
    case  BiCGStab:
      return bicgstab_strerror(reason);

    case  CG:
      return cg_strerror(reason);

    case  GMRes:
      return gmres_strerror(reason);

    case  ODir:
      return odir_strerror(reason);

    case  ORes:
      return ores_strerror(reason);

    case  TfQMR:
      return qmr_strerror(reason);

    case  GMRes_k:
      return gmres_strerror(reason);

    case  SymmLQ:
      return symmlq_strerror(reason);

    default:
      break;
    }

  return "ERROR in oem_strerror:  Unknown Solver";
}

