#include "alberta_util_intern.h"

void dswap(int n, double *x, int ix, double *y, int iy)
{
  DSWAP_F77(&n, x, &ix, y, &iy);
}
