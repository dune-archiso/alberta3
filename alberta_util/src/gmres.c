/*--------------------------------------------------------------------------*/
/*  Solving a linear system with GMRES                                      */
/*  Matrix:         regular                                                 */
/*  Restart:        k = MIN(restart,dim)                                    */
/*  Workspace:      ((k+2)*dim + k*(k+4))*sizeof(REAL)                      */
/*                                                                          */
/*  author: Kunibert G. Siebert                                             */
/*          Institut fuer Mathematik                                        */
/*          Universitaet Augsburg                                           */
/*          Universitaetsstr. 14                                            */
/*          D-86159 Augsburg, Germany                                       */
/*                                                                          */
/*          http://scicomp.math.uni-augsburg.de/Siebert/                    */
/*                                                                          */
/* (c) by K.G. Siebert (2000-2003)                                          */
/*--------------------------------------------------------------------------*/

#include "alberta_util.h"
#include "alberta_util_intern.h"

static const REAL TOL = 1.e-25;

static REAL householder_vec(int n, double sigma, REAL *u, REAL *x)
{
  FUNCNAME("householder_vec");
  int     i;
  double  beta;

  if (n <= 0)
  {
    MSG("ha ha\n");
    return(0.0);
  }

  if (x[0] >= 0)
  {
    beta = sqrt(0.5/(sigma*(sigma + x[0])));
    if (u)
    {
      u[0] = (x[0] + sigma)*beta;
      for (i = 1; i < n; i++)
	u[i] = x[i]*beta;
    }
    return(-sigma);
  }
  else
  {
    beta = sqrt(0.5/(sigma*(sigma - x[0])));
    if (u)
    {
      u[0] = (x[0] - sigma)*beta;

      for (i = 1; i < n; i++)
	u[i] = x[i]*beta;
    }
    return(sigma);
  }
}

#if 0
static void Pm_v(REAL *v, REAL *um, int m, int n)
{
  int      i;
  double   skp = 0.0;

  for (i = m; i < n; i++)
    skp += um[i]*v[i];
  skp *= 2.0;

  for (i = m; i < n; i++)
    v[i] -= skp*um[i];
}
#endif

static void new_basisvec(int m, int n, int k, REAL *U, REAL *LR,
			 REAL *v, REAL *b, REAL *y, void *mvd,
			 int  (*mv)(void *, int, const REAL *, REAL *),
			 void *lpd, void (*lpc)(void *, int, REAL *),
			 void *rpd, void (*rpc)(void *, int, REAL *))
{
  int      i, j, l;

/****************************************************************************/
/*  first calculate y = 2 U_m^T e_m                                         */
/****************************************************************************/

  for (j = 0; j < m; j++)
    b[j] = 2.0*U[j*n+(m-1)];

/****************************************************************************/
/*  now solve L_m^T y = b in R^m  (upper triagonal system, diagonal == 1.0  */
/****************************************************************************/

  y[m-1] = b[m-1];
  for (j = m-2; j >= 0; j--)
  {
    double yj = b[j];

    for (l = j + 1; l < m; l++)
      yj -= LR[l*k+j]*y[l];

    y[j] = yj;
  }

/****************************************************************************/
/*  b = -U_m y + e_m                                                        */
/****************************************************************************/

  for (i = 0; i < n; i++)
  {
    double bi = 0.0;

    for (j = 0; j < MIN(i+1,m); j++)
      bi += U[j*n+i]*y[j];

    b[i] = -bi;
  }
  b[m-1] += 1.0;

/****************************************************************************/
/*  v = Ab                                                                  */
/****************************************************************************/

  if (rpc)
    (*rpc)(rpd, n, b);

  (*mv)(mvd, n, b, v);

  if (lpc)
    (*lpc)(lpd, n, v);

/****************************************************************************/
/*  b = 2 U_m^T v in R^m                                                    */
/****************************************************************************/

  for (j = 0; j < m; j++)
  {
    double bj = 0.0;

    for (i = j; i < n; i++)
      bj += U[j*n+i]*v[i];
    b[j] = 2.0*bj;
  }

/****************************************************************************/
/*  now solve L_m y = b in R^m  (lower triagonal system, diagonal == 1.0    */
/****************************************************************************/

  y[0] = b[0];
  for (j = 1; j < m; j++)
  {
    double yj = b[j];

    for (l = 0; l < j; l++)
      yj -= LR[j*k+l]*y[l];

    y[j] = yj;
  }

/****************************************************************************/
/*  v = v - U_m y                                                           */
/****************************************************************************/

  for (i = 0; i < n; i++)
  {
    double vi = 0.0;

    for (j = 0; j < MIN(i+1,m); j++)
      vi += U[j*n+i]*y[j];

    v[i] -= vi;
  }

  return;
}

/*--------------------------------------------------------------------------*/
/*  partitioning of the workspace:                                          */
/*                                                                          */
/*  k = MIN(restart,n)                                                      */
/*  workspace:  ((k+2)*n + k*(k+4))*sizeof(double)                          */
/*  we use the following partioning                                         */
/*                                                                          */
/*  |---------------------- n -----------------------------------|          */
/*   ____________________________________________________________           */
/*  |____________________________________________________________| r        */
/*  |____________________________________________________________| v        */
/*  |____________________________________________________________| u[0]     */
/*  |____________________________________________________________| .        */
/*                                                                 .        */
/*                                                                 .        */
/*   ____________________________________________________________  .        */
/*  |____________________________________________________________| u[k-2]   */
/*  |____________________________________________________________| u[k-1]   */
/*  |_________________________| 0                                           */
/*  |_|_______________________| 1    upper triangular matrix + diogonal:    */
/*  |___|_____________________| 2    transformed Hessenberg R_k matrix;     */
/*  |_____|___________________| .                                           */
/*                              .                                           */
/*                              .    lower triangluar matrix: matrix L_k    */
/*   _________________________       for the calculation of the (m+1)th     */
/*  |_____________________|___| k-2  column of Q_m = P_m ... P_1; diagonal  */
/*  |_______________________|_| k-1  elements L_k[i][i] = 1.0; not stored!  */
/*  |_________________________|      Givens rotations: storing c and s      */
/*  |_________________________|                                             */
/*  |_________________________|      additional workspace!                  */
/*                                                                          */
/*  |--------- k -------------|                                             */
/*--------------------------------------------------------------------------*/
REAL gmres_k_residual_0 = 0.0;

int oem_gmres_k(OEM_DATA *oem, int n, const REAL *b, REAL *x)
{
  FUNCNAME("oem_gmres_k");
  int      i, j, l, m;
  REAL     *v, *r, *um1,*LR, *U, (*givens)[2], *w, *y;
  double   c, s, wm1, norm;

  int      k   = MAX(0,MIN(oem->restart,n));

  int      (*mv)(void *, int, const REAL *, REAL *) = oem->mat_vec;
  void     *mvd = oem->mat_vec_data;
  void     (*lpc)(void *, int, REAL *) = oem->left_precon;
  void     *lpd = oem->left_precon_data;
  void     (*rpc)(void *, int, REAL *) = oem->right_precon;
  void     *rpd = oem->right_precon_data;

  WORKSPACE *ws = CHECK_WORKSPACE((k+2)*n + k*(k+4), oem->ws);

/*--------------------------------------------------------------------------*/
/*---  partitioning of the workspace  --------------------------------------*/
/*--------------------------------------------------------------------------*/
  r      = (REAL *)ws->work;
  v      = r+n;
  U      = v+n;
  LR     = U+(k*n);
  givens = (double (*)[2]) (LR+(k*k));
  w      = (REAL *) (givens+k);
  y      = w+k;

/*--------------------------------------------------------------------------*/
/*  Initialization                                                          */
/*--------------------------------------------------------------------------*/

  oem->terminate_reason = 0;

  (*mv)(mvd, n, x, r);

  for (i = 0; i < n; i++)
    r[i] = b[i] - r[i];

  if (lpc)
    (*lpc)(lpd, n, r);

  gmres_k_residual_0 = oem->initial_residual = norm = dnrm2(n, r, 1);
  if (norm < oem->tolerance)
  {
    oem->residual = norm;
    free_oem_workspace(ws,oem);
    return(0);
  }

/*--------------------------------------------------------------------------*/
/*  construct k-dimensional Krylov space                                    */
/*--------------------------------------------------------------------------*/

  wm1 = householder_vec(n, norm, U, r);
  um1 = U;

  for (m = 0; m < k; m++)
  {
    w[m] = wm1;

    new_basisvec(m+1, n, k, U, LR, r, v, y, mvd, mv, lpd, lpc, rpd, rpc);

    if (m+1 < n)
    {
      norm = 0;
      for (i = m+1; i < n; i++)
	norm += r[i]*r[i];
      norm = sqrt(norm);

      if (norm > TOL)
      {
/****************************************************************************/
/*  one of the last components of r is not zero; calculate Householder      */
/*  vector; if m < k-1, we need the Householder vector for the calculation  */
/*  of the next basis vector => store it; if m == k-1 we do not need this   */
/*  vector anymore => do not store it!                                      */
/****************************************************************************/
	if (m < k-1)
	{
	  um1 = um1+n;
	  r[m+1] = householder_vec(n-(m+1), norm, um1+(m+1), r+(m+1));
	}
	else
	  r[m+1] = householder_vec(n-(m+1), norm, NULL, r+(m+1));
      }
    }

    for (j = 0; j < m; j++)
    {
      double   rj = r[j];

      c = givens[j][0];
      s = givens[j][1];

      r[j]   =  c*rj + s*r[j+1];
      r[j+1] = -s*rj + c*r[j+1];
    }

    if (m+1 < n  &&  ABS(r[m+1]) > TOL)
    {
/****************************************************************************/
/*  Find Givens rotation J_m such that,                                     */
/*       a)   (J_m r)[i] = r[i],  i < m,                                    */
/*       b)   (J_m r)[m+1] = 0.0                                            */
/*         => (J_m r)[m] = +- sqrt(r[m]^2 + r[m+1]^2) =: sigma              */
/*                                                                          */
/*                                                                          */
/*                     |1  0  .  .  .  .  0|                                */
/*                     |0  1  .  .  .  .  .|                                */
/*                     |.     .           .|          c = r[m]/sigma        */
/*            J_m =    |.        .        .|          s = r[m+1]/sigma      */
/*                     |.           .     .|                                */
/*                     |.              c  s| m                              */
/*                     |0  .  .  .  . -s  c| m+1                            */
/*                                     m  m+1                               */
/****************************************************************************/

      double   sigma, maxi;

      maxi = MAX(r[m], r[m+1]);

      c = r[m]/maxi;
      s = r[m+1]/maxi;
      sigma = maxi*sqrt(c*c + s*s);
      if (r[m] < 0)
	sigma = -sigma;
      givens[m][0] = c = r[m]/sigma;
      givens[m][1] = s = r[m+1]/sigma;

      r[m] = sigma;  /* r[m+1] == 0   automatically!                        */

      wm1 = -s*w[m];  /*  |wm1| is the new residual!                        */
      w[m] *= c;
    }
    else
      wm1 = 0.0;

/****************************************************************************/
/*  now, store the first m components of the column vector r in the the mth */
/*  column of LR                                                            */
/****************************************************************************/

    for (j = 0; j <= m; j++)
      LR[j*k+m] = r[j];

/****************************************************************************/
/*  test, whether tolarance is reached or not                               */
/****************************************************************************/

    if (ABS(wm1) < oem->tolerance)
    {
      m++;
      break;
    }

/****************************************************************************/
/*  tolerance not reached: calculate and store (m+1)th row of matrix L;     */
/*  this row is only needed for the computation of the (m+1)th column of    */
/*  the orthogonal matrix Q_m; this vector is the additional basis vector   */
/*  for the enlargement of the Krylov space; only needed for m < k-1        */
/*  L[m+1][j] = <u_(m+1),u_j>_2                                             */
/*  (m+1)th vector u = umi is allready stored at U+(m+1)                    */
/****************************************************************************/

    if (m < k-1)
    {
      for (j = 0; j < m+1; j++)
      {
	double  *uj = U+(j*n);
	double  val = 0.0;

	for (i = m+1; i < n; i++)
	  val += um1[i]*uj[i];

	LR[(m+1)*k+j] = 2.0*val;
      }
    }
  }

/****************************************************************************/
/*  and now solve the upper triangular system                               */
/****************************************************************************/

  y[m-1] = w[m-1]/LR[(m-1)*(k+1)];  /* = LR[(m-1)*k+(m-1)]!                 */
  for (j = m-2; j >= 0; j--)
  {
    double yj = w[j];

    for (l = j + 1; l < m; l++)
      yj -= LR[j*k+l]*y[l];

    y[j] = yj/LR[j*(k+1)];
  }

/****************************************************************************/
/*  calculate v = 2 U_m^T [e_0,....,e_m-1] y                                */
/****************************************************************************/

  for (i = 0; i < m; i++)
  {
    double val = 0.0;

    for (j = i; j < m; j++)
      val += U[i*n+j]*y[j];
    v[i] = 2.0*val;
  }

/****************************************************************************/
/*  now solve L_m^T w = v in R^m  (upper triagonal system, diagonal == 1.0  */
/****************************************************************************/

  w[m-1] = v[m-1];
  for (j = m-2; j >= 0; j--)
  {
    double wj = v[j];

    for (l = j + 1; l < m; l++)
      wj -= LR[l*k+j]*w[l];

    w[j] = wj;
  }

/****************************************************************************/
/*  v = [e_0,....,e_m-1] y - U_m w                                          */
/****************************************************************************/

  for (i = 0; i < n; i++)
  {
    double  vi = 0.0;
    for (j = 0; j < MIN(i+1,m); j++)
      vi += U[j*n+i]*w[j];
    v[i] = -vi;
  }

  for (j = 0; j < m; j++)
    v[j] += y[j];

/****************************************************************************/
/*  and now, make the update of u :-)                                       */
/****************************************************************************/

  if (rpc)
    (*rpc)(rpd, n, v);

  for (i = 0; i < n; i++)
    x[i] += v[i];

  oem->residual = ABS(wm1);
  free_oem_workspace(ws, oem);
  return(m);
}

int oem_gmres(OEM_DATA *oem, int dim, const REAL *b, REAL *x)
{
  FUNCNAME("oem_gmres");
  int       iter, k;
  REAL      old_res = -1.0;
  WORKSPACE *ws;
  OEM_DATA  data;

  oem->terminate_reason = 0;

  if (dnrm2(dim, b, 1) < TOL && dnrm2(dim, x, 1) < TOL)
  {
	oem->terminate_reason = 1;
    INFO(oem->info,2,
	 "b == x_0 == 0, x = 0 is the solution of the linear system\n");
    dset(dim, 0.0, x, 1);
    oem->initial_residual = oem->residual = 0.0;
    return(0);
  }

  if (oem->restart <= 0  ||  oem->restart > dim)
    k = MIN(10, dim);
  else
    k = oem->restart;

  ws = CHECK_WORKSPACE((k+2)*dim + k*(k+4), oem->ws);

  data = *oem;
  data.restart = k;
  data.ws = ws;

  START_INFO(oem);
  for (iter = 0; iter <= oem->max_iter; iter++)
  {
    k = oem_gmres_k(&data, dim, b, x);
    if (!iter)  oem->initial_residual = data.initial_residual;
    oem->residual = data.residual;
    if (SOLVE_INFO(oem, iter, data.residual, &old_res, ws))
      return(iter);
    TEST_EXIT(k != 0, "this must not happen\n");
  }
  return(0);   /*  statement never reached!!!                              */
}

const char *gmres_strerror(int reason)
{
  switch (reason) {
  case 1:
    return "b == x_0 == 0, x = 0 is the solution of the linear system";

  default:
    break;
  }
  return "Unknown error code.";
}

