/*--------------------------------------------------------------------------*/
/*  solve nonlinear systems by a Newton method with step size control       */
/*  by Bank and Rose, Numer. Math. 37 (1981) pp. 279-295.                   */
/*                                                                          */
/*  Workspace:      4*dim                                                   */
/*                                                                          */
/*  author: Kunibert G. Siebert                                             */
/*          Institut fuer Mathematik                                        */
/*          Universitaet Augsburg                                           */
/*          Universitaetsstr. 14                                            */
/*          D-86159 Augsburg, Germany                                       */
/*                                                                          */
/*          http://scicomp.math.uni-augsburg.de/Siebert/                    */
/*                                                                          */
/* (c) by K.G. Siebert (2002-2003)                                          */
/*--------------------------------------------------------------------------*/

#include "alberta_util.h"
#include "alberta_util_intern.h"

int nls_newton_br(NLS_DATA *data, REAL delta, int dim, REAL *x)
{
  FUNCNAME("nls_newton_br");
  REAL err, err_old;
  int  iter;

  int  info = data->info;
  void (*update)(void *, int, const REAL *, bool, REAL *) = data->update;
  void *ud = data->update_data;
  int  (*solve)(void *, int, const REAL *, REAL *) = data->solve;
  void *sd = data->solve_data;
  REAL (*norm)(void *, int, const REAL *) = data->norm;
  void *nd = data->norm_data;

  WORKSPACE  *ws = CHECK_WORKSPACE(3*dim, data->ws);

/*--- Memory initialization ------------------------------------------------*/
  REAL *b = (REAL *)ws->work;
  REAL *d = b+dim;
  REAL *y = d+dim;
 
/*--------------------------------------------------------------------------*/
/*--- Newton initialization ------------------------------------------------*/
/*--------------------------------------------------------------------------*/

  (*update)(ud, dim, x, false, b);       /*-- update F(x)  -----------------*/
  err = err_old = norm ? (*norm)(nd, dim, b) : dnrm2(dim, b, 1);

  INFO(info,2,"iter. |     residual |     red. |    n |  m |\n");
  INFO(info,2,"%5d | %12.5le | -------- | ---- | -- |\n", 0, err);

  if ((data->residual = err) < data->tolerance)
  {
    INFO(info,4,"finished succesfully\n");
    if (ws != data->ws) FREE_WORKSPACE(ws);
    return(0);
  }

/*--- still initalization part ---------------------------------------------*/
  int mmax = MAX(2,MIN(data->restart,32));
  REAL K    = 0.0;

/*--- start iterations -----------------------------------------------------*/

  for (iter = 1; iter <= data->max_iter+1; iter++)
  {
    (*update)(ud, dim, x, true, NULL);   /*-- update DF(x) -----------------*/

    dset(dim, 0.0, d, 1);                /*-- initial guess is zero --------*/
    int n = (*solve)(sd, dim, b, d);         /*-- solve DF(x) d = b ------------*/
    int m;

    for (m = 0; m <= mmax; m++)
    {
/*--- aim: |F(u_k+\tau d)| \le ????               --------------------------*/
      REAL tau = 1.0/(1.0 + K*err_old);
      dcopy(dim, x, 1, y, 1);            /*-- y = x ------------------------*/
      daxpy(dim, -tau, d, 1, y, 1);      /*-- y -= tau*d -------------------*/
      (*update)(ud, dim, y, false, b);   /*-- update F(y) ------------------*/
      
      err = norm ? (*norm)(nd, dim, b) : dnrm2(dim, b, 1);
      if ((1.0 - err/err_old)/tau < delta)
      {
	K = (K == 0.0) ? 1.0 : 10.0*K;
      }
      else
      {
	K *= 0.1;
	break;
      }
    }
    dcopy(dim, y, 1, x, 1);             /*-- x = y  (update x!) ------------*/

    if (err_old <= 0)
      INFO(info,2,"%5d | %12.5le | -------- | %4d | %2d |\n", 
			 iter, err, n, m);
    else
      INFO(info,2,"%5d | %12.5le | %8.2le | %4d | %2d |\n", 
		       iter, err, err/err_old, n, m);

    if ((data->residual = err) < data->tolerance)
    {
      INFO(info,4,"finished successfully\n");
      if (ws != data->ws) FREE_WORKSPACE(ws);
      return(iter);
    } 
    else if (iter > data->max_iter)
    {
      if (info < 2) {
	INFO(info,1,"iter. %d, residual: %12.5le\n", iter, err);
      }
      INFO(info,1,"tolerance %le not reached\n", data->tolerance);
      if (ws != data->ws) FREE_WORKSPACE(ws);
      return(iter);
    }
    err_old = err;
  }
  return(iter);  /*--- statement never reached -----------------------------*/
}
