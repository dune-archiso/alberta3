/*--------------------------------------------------------------------------*/
/*  Stabilized BiCG method                                                  */
/*  Classification: ???                                                     */
/*  Matrix:         symmetric                                               */
/*  Workspace:      5*dim                                                   */
/*                                                                          */
/*  C-version of OFM-lib by Willy Doerfler                                  */
/*                                                                          */
/*  author: Kunibert G. Siebert                                             */
/*          Institut fuer Mathematik                                        */
/*          Universitaet Augsburg                                           */
/*          Universitaetsstr. 14                                            */
/*          D-86159 Augsburg, Germany                                       */
/*                                                                          */
/*          http://scicomp.math.uni-augsburg.de/Siebert/                    */
/*                                                                          */
/* (c) by K.G. Siebert (2000-2003)                                          */
/*--------------------------------------------------------------------------*/

#include "alberta_util.h"
#include "alberta_util_intern.h"

int oem_bicgstab(OEM_DATA *oem, int dim, const REAL *b, REAL *x)
{
  FUNCNAME("oem_bicgstab");
  REAL  *rstar, *d, *s, *CAd, *h, *t;
  REAL  res, old_res = -1.0;
  int   iter;

  int   (*mv)(void *, int, const REAL *, REAL *) = oem->mat_vec;
  void  *mvd = oem->mat_vec_data;
  void  (*precon)(void *, int, REAL *) = oem->left_precon;
  void  *pd = oem->left_precon_data;
  REAL  (*scp)(void *, int, const REAL *, const REAL *) = oem->scp;
  void  *sd = oem->scp_data;

  const REAL TOL = 1.e-30;
  WORKSPACE  *ws = CHECK_WORKSPACE(5*dim, oem->ws);

/*--------------------------------------------------------------------------*/
/*---  partitioning of the workspace  --------------------------------------*/
/*--------------------------------------------------------------------------*/

  rstar = (REAL *)ws->work;
  d     = rstar + dim;
  s     = d + dim;
  CAd   = s + dim;
  h     = CAd + dim;
  t     = h; /* !!! */

/*--------------------------------------------------------------------------*/
/*---  Initalization  ------------------------------------------------------*/
/*--------------------------------------------------------------------------*/

  oem->terminate_reason = 0;

  if ((scp ? sqrt((*scp)(sd, dim, b, b)) : dnrm2(dim, b, 1)) < TOL
      &&
      (scp ? sqrt((*scp)(sd, dim, x, x)) : dnrm2(dim, x, 1)) < TOL)
  {
	oem->terminate_reason = 1;
    INFO(oem->info,2,
	 "b == x_0 == 0, x = 0 is the solution of the linear system\n");
    dset(dim, 0.0, x, 1);
    oem->initial_residual = oem->residual = 0.0;
    return(0);
  }

  (*mv)(mvd, dim, x, h);
  daxpy(dim, -1.0, b, 1, h, 1);
  dcopy(dim, h, 1, rstar, 1);

  if (precon) (*precon)(pd, dim, h);

/*--------------------------------------------------------------------------*/
/*---  check initial residual  ---------------------------------------------*/
/*--------------------------------------------------------------------------*/
  res = scp ? sqrt((*scp)(sd, dim, h, h)) : dnrm2(dim, h, 1);
  oem->initial_residual = res;

  START_INFO(oem);
  if (SOLVE_INFO(oem, 0, res, &old_res, ws))
    return(0);

  dcopy(dim, h, 1, d, 1);
  REAL rh1 = scp ? (*scp)(sd, dim, h, rstar) : ddot(dim, h, 1, rstar,1);

/*--------------------------------------------------------------------------*/
/*---  Iteration  ----------------------------------------------------------*/
/*--------------------------------------------------------------------------*/

  for (iter = 1; iter <= oem->max_iter; iter++)
  {
/*--------------------------------------------------------------------------*/
/*---  calculate CA.d and CA.d*rstar  --------------------------------------*/
/*--------------------------------------------------------------------------*/

    (*mv)(mvd, dim, d, CAd);
    if (precon) (*precon)(pd, dim, CAd);
    REAL dad = scp ? (*scp)(sd, dim, CAd, rstar) : ddot(dim, CAd, 1, rstar, 1);

    if (ABS(rh1) < TOL)
    {
      oem->terminate_reason = 2;
      BREAK_INFO(oem, "(h,r^*)_2 = 0", iter, res, &old_res, ws);
      return(iter);
    }
    if (ABS(dad) < TOL)
    {
      oem->terminate_reason = 3;
      BREAK_INFO(oem, "(Ad,d^*)_2 = 0", iter, res, &old_res, ws);
      return(iter);
    }

/*-------------------------------------------------------------------------*/
/*---  update s and t  ----------------------------------------------------*/
/*-------------------------------------------------------------------------*/

    REAL alpha = rh1/dad;
    dcopy(dim, h, 1, s, 1);
    daxpy(dim, -alpha, CAd, 1, s, 1);

    (*mv)(mvd, dim, s, t);
    if (precon) (*precon)(pd, dim, t);

/*--------------------------------------------------------------------------*/
/*---  calculate omega  ----------------------------------------------------*/
/*--------------------------------------------------------------------------*/

    REAL ast = scp ? (*scp)(sd, dim, s, t) : ddot(dim, s, 1, t, 1);
    REAL att = scp ? (*scp)(sd, dim, t, t) : ddot(dim, t, 1, t, 1);

    if (ABS(ast) < TOL)
    {
      oem->terminate_reason = 4;
      INFO(oem->info,4,"omega = 0\n");
      ast = ast > 0 ? TOL : -TOL;
    }

    if (att < TOL)
    {
      oem->terminate_reason = 5;
      INFO(oem->info,4,"t = 0\n");
      att = TOL;
    }

    REAL omega = ast/att;

/*--------------------------------------------------------------------------*/
/*---   update x and calculate new h  --------------------------------------*/
/*--------------------------------------------------------------------------*/

    daxpy(dim, -alpha, d, 1, x, 1);
    daxpy(dim, -omega, s, 1, x, 1);
    dxpay(dim, s, 1, -omega, h, 1);    /*---  t=h!!!  ----------------------*/

    res = scp ? sqrt((*scp)(sd, dim, h, h)) : dnrm2(dim, h, 1);
    if (SOLVE_INFO(oem, iter, res, &old_res, ws))
      return(iter);

    REAL rh2 = scp ? (*scp)(sd, dim, h, rstar) : ddot(dim, h, 1, rstar, 1);

    REAL beta= (rh2/rh1)* (alpha/omega);
    daxpy(dim, -omega, CAd, 1, d, 1);
    dxpay(dim, h, 1, beta, d, 1);

    rh1 = rh2;
  }
  return(0);    /*---  statement never reached!!!  -------------------------*/
}


const char *bicgstab_strerror(int reason)
{
  switch (reason) {
  case 1:
    return "b == x_0 == 0, x = 0 is the solution of the linear system";

  case 2:
    return "(h,r^*)_2 = 0";

  case 3:
    return "(Ad,d^*)_2 = 0";

  case 4:
    return "omega = 0";

  case 5:
    return "t = 0";

  default:
    break;
  }
  return "Unknown error code.";
}
