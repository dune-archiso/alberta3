#include "alberta_util_intern.h"

void dcopy(int n, const double *x, int ix, double *y, int iy)
{
  DCOPY_F77(&n, x, &ix, y, &iy);
  return;
}
