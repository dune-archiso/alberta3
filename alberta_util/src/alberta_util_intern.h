/*--------------------------------------------------------------------------*/
/* ALBERTA_UTIL:  tools for messages, memory allocation, parameters, etc.   */
/*                                                                          */
/* file: alberta_util_intern.h                                              */
/*                                                                          */
/* description: internal header file of the ALBERTA_UTIL package            */
/*                                                                          */
/*--------------------------------------------------------------------------*/
/*                                                                          */
/*  authors:   Alfred Schmidt                                               */
/*             Zentrum fuer Technomathematik                                */
/*             Fachbereich 3 Mathematik/Informatik                          */
/*             Universitaet Bremen                                          */
/*             Bibliothekstr. 2                                             */
/*             D-28359 Bremen, Germany                                      */
/*                                                                          */
/*             Kunibert G. Siebert                                          */
/*             Institut fuer Mathematik                                     */
/*             Universitaet Augsburg                                        */
/*             Universitaetsstr. 14                                         */
/*             D-86159 Augsburg, Germany                                    */
/*                                                                          */
/*  http://www.mathematik.uni-freiburg.de/IAM/ALBERTA                       */
/*                                                                          */
/*  (c) by A. Schmidt and K.G. Siebert (1996-2003)                          */
/*                                                                          */
/*--------------------------------------------------------------------------*/

#ifndef _ALBERTA_UTIL_INTERN_H_
#define _ALBERTA_UTIL_INTERN_H_

#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------------------------------------------------------*/
/*  functions for printing information (file info.c)                        */
/*--------------------------------------------------------------------------*/

void start_info(const char *, OEM_DATA *);
void break_info(const char *, OEM_DATA *, const char *, int, REAL, REAL *,
		WORKSPACE *);
int  solve_info(const char *, OEM_DATA *, int, REAL, REAL *, WORKSPACE *);

#define START_INFO(oem)  start_info(funcName, oem)
#define BREAK_INFO(oem,st,it,res,ores,ws)\
  break_info(funcName,oem,st,it,res,ores,ws)
#define SOLVE_INFO(oem,it,res,ores,ws) solve_info(funcName,oem,it,res,ores,ws)

WORKSPACE *check_workspace(const char *, const char *, int, size_t, 
			   WORKSPACE *);
void free_oem_workspace(WORKSPACE *ws, OEM_DATA *oem);
#define CHECK_WORKSPACE(size,ws)\
  check_workspace(funcName,__FILE__,__LINE__,size,ws)

const char *bicgstab_strerror(int reason);
const char *cg_strerror(int reason);
const char *gmres_strerror(int reason);
const char *odir_strerror(int reason);
const char *ores_strerror(int reason);
const char *qmr_strerror(int reason);
const char *symmlq_strerror(int reason);

#ifdef __cplusplus
}
#endif

#endif  /* _ALBERTA_UTIL_INTERN_H_  */
