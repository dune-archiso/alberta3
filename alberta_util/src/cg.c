/*--------------------------------------------------------------------------*/
/*  Solving a linear system with conjugate gradient algoritm                */
/*  Classification: cg(A,C,A) = OrthoDir_r(A,C,A)                           */
/*  Matrix:         symmetric and positve definite                          */
/*  Workspace:      3*dim                                                   */
/*                                                                          */
/*  author: Kunibert G. Siebert                                             */
/*          Institut fuer Mathematik                                        */
/*          Universitaet Augsburg                                           */
/*          Universitaetsstr. 14                                            */
/*          D-86159 Augsburg, Germany                                       */
/*                                                                          */
/*          http://scicomp.math.uni-augsburg.de/Siebert/                    */
/*                                                                          */
/* (c) by K.G. Siebert (2000-2003)                                          */
/*--------------------------------------------------------------------------*/

#include "alberta_util.h"
#include "alberta_util_intern.h"

int oem_cg(OEM_DATA *oem, int dim, const REAL *b, REAL *x)
{
  FUNCNAME("oem_cg");
  REAL  *d, *h, *r, *Cr, old_res = -1.0;
  int   iter;

  int   (*mv)(void *, int, const REAL *, REAL *) = oem->mat_vec;
  void  *mvd = oem->mat_vec_data;
  void  (*precon)(void *, int, REAL *) = oem->left_precon;
  void  *pd = oem->left_precon_data;
  REAL  (*scp)(void *, int, const REAL *, const REAL *) = oem->scp;
  void  *sd = oem->scp_data;

  const REAL TOL = SQR(REAL_EPSILON);
  WORKSPACE  *ws = CHECK_WORKSPACE(3*dim, oem->ws);

/*--------------------------------------------------------------------------*/
/*---  partitioning of the workspace  --------------------------------------*/
/*--------------------------------------------------------------------------*/

  r = (REAL *)ws->work;
  d = r + dim;
  h = d + dim;
  Cr = precon ? h : r;

  oem->terminate_reason = 0;

  if ((scp ? sqrt((*scp)(sd, dim, b, b)) : dnrm2(dim, b, 1)) < TOL
      &&
      (scp ? sqrt((*scp)(sd, dim, x, x)) : dnrm2(dim, x, 1)) < TOL)
  {
	oem->terminate_reason = 1;
    INFO(oem->info,2,
	 "b == x_0 == 0, x = 0 is the solution of the linear system\n");
    dset(dim, 0.0, x, 1);
    oem->initial_residual = oem->residual = 0.0;
    free_oem_workspace(ws, oem);
    return(0);
  }

  (*mv)(mvd, dim, x, r);
  daxpy(dim, -1.0, b, 1, r, 1);
  if (precon)
  {
    dcopy(dim, r, 1, Cr, 1);
    (*precon)(pd, dim, Cr);
  }

/*--------------------------------------------------------------------------*/
/*---  check initial residual  ---------------------------------------------*/
/*--------------------------------------------------------------------------*/
  REAL delta = scp ? (*scp)(sd, dim, r, Cr) : ddot(dim, r, 1, Cr, 1);
  START_INFO(oem);
  if (SOLVE_INFO(oem, 0, oem->initial_residual = sqrt(delta), &old_res, ws))
    return(0);

  dcopy(dim, Cr, 1, d, 1);

/*--------------------------------------------------------------------------*/
/*---  Iteration  ----------------------------------------------------------*/
/*--------------------------------------------------------------------------*/

  for (iter = 1; iter <= oem->max_iter; iter++)
  {
    (*mv)(mvd, dim, d, h);
/*--------------------------------------------------------------------------*/
/*---  descent parameter rho = delta/(h,d)_2  ------------------------------*/
/*--------------------------------------------------------------------------*/
    REAL hd_2 = scp ? (*scp)(sd, dim , h, d) : ddot(dim, h, 1, d, 1);
    if (ABS(hd_2) < TOL)
    {
      BREAK_INFO(oem, "(Ad,d)_2 = 0", iter, sqrt(delta), &old_res, ws);
      return(iter);
    }

/*--------------------------------------------------------------------------*/
/*---  update x and r  -----------------------------------------------------*/
/*--------------------------------------------------------------------------*/
    REAL rho = delta/hd_2;
    daxpy(dim, -rho, d, 1, x, 1);
    daxpy(dim, -rho, h, 1, r, 1);

    if (precon)
    {
      dcopy(dim, r, 1, Cr, 1);
      (*precon)(pd, dim, Cr);
    }

    REAL gamma = 1.0/delta;
    delta = scp ? (*scp)(sd, dim, r, Cr) : ddot(dim, r, 1, Cr, 1);

    if (SOLVE_INFO(oem, iter, sqrt(delta), &old_res, ws))
      return(iter);

/*--------------------------------------------------------------------------*/
/*---  update of the descent direction  ------------------------------------*/
/*--------------------------------------------------------------------------*/
    gamma *= delta;
    dxpay(dim, Cr, 1, gamma, d, 1);
  }
  return(0);    /*  statement never reached!!!                              */
}

const char *cg_strerror(int reason)
{
  switch (reason) {
  case 1:
    return "b == x_0 == 0, x = 0 is the solution of the linear system";


  default:
    break;
  }
  return "Unknown error code.";
}

