/*--------------------------------------------------------------------------*/
/*  Transpose-free QMR-Solver (nonsymetric, non-positiv-definit)            */
/*  Classification: ???                                                     */
/*                                                                          */
/*  author: Oliver Kriessl                                                  */
/*          Lehrstuhl fuer Angewandte Analysis mit Schwerpunkt Numerik      */
/*          Universitaetsstrasse 14                                         */
/*          D-86159 Augsburg                                                */
/*                                                                          */
/*          http://www.math.uni-augsburg.de/~oli                            */
/*                                                                          */
/* (c) by O.Kriessl (2005)                                                  */
/*--------------------------------------------------------------------------*/

#include "alberta_util.h"
#include "alberta_util_intern.h"

int oem_tfqmr(OEM_DATA *oem, int dim, const REAL *b, REAL *x)
{
  FUNCNAME("oem_tfqmr");

  static int tres = 0;

  const REAL TOL = 1.e-30;

  REAL  *xt, *bt, *rt, *vn, *wn, *yn, *dn, *Ayn, *tmp, *ptmp;
  REAL  *btmp, *upbt, *mult;
  REAL  resn = 1.0, rho = 1.0, var = 0.0, eta = 0.0, tau;
  REAL  old_res = -1.0;
  int   iter, ierr = 0, flag = 0;

  int   (*mv)(void *, int, const REAL *, REAL *) = oem->mat_vec;
  void  *mvd = oem->mat_vec_data;
  void  (*l_precon)(void *, int, REAL *) = oem->left_precon;
  void  *l_pd = oem->left_precon_data;
  void  (*r_precon)(void *, int, REAL *) = oem->right_precon;
  void  *r_pd = oem->right_precon_data;

  WORKSPACE  *ws = CHECK_WORKSPACE(11*dim, oem->ws);

/*--------------------------------------------------------------------------*/
/*---  checking the inputs  -----------------------------------------------*/
/*--------------------------------------------------------------------------*/
  if (dim<0 || oem->max_iter<1) {
	oem->terminate_reason = 2;
    printf("oem_tfqmr:  bad inputs detected ... exiting!\n");
    exit(-1);
  }

  if (oem->tolerance<=0.0) oem->tolerance = 1.0e-16;

/*--------------------------------------------------------------------------*/
/*---  partitioning of the workspace  --------------------------------------*/
/*--------------------------------------------------------------------------*/
  xt = (REAL *)ws->work;
  bt = xt + dim;
  rt = bt + dim;
  vn = rt + dim;
  wn = vn + dim;
  yn = wn + dim;
  dn = yn + dim;
  Ayn = dn + dim;
  tmp = Ayn + dim;
  ptmp = tmp + dim;
  btmp = ptmp + dim;
  upbt = l_precon ? btmp : bt;

/*--------------------------------------------------------------------------*/
/*---  Initalization  ------------------------------------------------------*/
/*--------------------------------------------------------------------------*/
  oem->terminate_reason = 0;

  if (dnrm2(dim, b, 1) < TOL && dnrm2(dim, x, 1) < TOL)
  {
	oem->terminate_reason = 1;
    INFO(oem->info,2,
	 "b == x_0 == 0, x = 0 is the solution of the linear system\n");
    dset(dim, 0.0, x, 1);
    oem->initial_residual = oem->residual = 0.0;
    return(0);
  }

  (*mv)(mvd, dim, x, xt);
  dcopy(dim, b, 1, bt, 1);
  daxpy(dim, -1.0, xt, 1, bt, 1);
  if (l_precon) {
    dcopy(dim, bt, 1, upbt, 1);
    (*l_precon)(l_pd, dim, bt);
  }

  dset(dim, 0.0, xt, 1);
  dcopy(dim, bt, 1, wn, 1);
  drandn(dim, rt, 1);
  dset(dim, 0.0, vn, 1);
  dset(dim, 0.0, yn, 1);
  dset(dim, 0.0, Ayn, 1);

  tau = ddot(dim, bt, 1, bt, 1);

/*--------------------------------------------------------------------------*/
/*---  check initial residual  ---------------------------------------------*/
/*--------------------------------------------------------------------------*/
  resn = sqrt(ddot(dim, bt, 1, upbt, 1));
  START_INFO(oem);
  if (SOLVE_INFO(oem, 0, oem->initial_residual = resn, &old_res, ws))
    return(0);

/*--------------------------------------------------------------------------*/
/*---  Iteration  ----------------------------------------------------------*/
/*--------------------------------------------------------------------------*/

  for (iter = 1; iter <= oem->max_iter; iter++)
  {
    flag = 0;
/*--------------------------------------------------------------------------*/
/*---  Compute \beta_{n-1} and \rho_{n-1}  ---------------------------------*/
/*--------------------------------------------------------------------------*/
    REAL dtmp = ddot(dim, rt, 1, wn, 1);
    REAL beta = dtmp / rho;
    rho = dtmp;

/*--------------------------------------------------------------------------*/
/*---  Compute \y_{2n-1}, v_{n-1}, and A y_{2n-1}  -------------------------*/
/*--------------------------------------------------------------------------*/
    dxpay(dim, Ayn, 1, beta, vn, 1);
    dxpay(dim, wn, 1, beta, yn, 1);
    mult = yn;
    if (r_precon) {
      dcopy(dim, yn, 1, ptmp, 1);
      (*r_precon)(r_pd, dim, ptmp);
      mult = ptmp;
    }
    (*mv)(mvd, dim, mult, tmp);
    if (l_precon) (*l_precon)(l_pd, dim, tmp);
    dxpay(dim, tmp, 1, beta, vn, 1);

/*--------------------------------------------------------------------------*/
/*---  Compute \sigma{n-1} and check for breakdowns  -----------------------*/
/*--------------------------------------------------------------------------*/
    dtmp = ddot(dim, rt, 1, vn, 1);
    if (dtmp==0.0 || rho==0.0) {
      ierr = 8;
      break;
    }

/*--------------------------------------------------------------------------*/
/*---  Compute \alpha_{n-1}, d_{2n-1} and w_{2n}  --------------------------*/
/*--------------------------------------------------------------------------*/
    REAL alpha = rho / dtmp;
    dtmp = var * eta / alpha;
    dxpay(dim, yn, 1, dtmp, dn, 1);
    daxpy(dim, -alpha, tmp, 1, wn, 1);

/*--------------------------------------------------------------------------*/
/*---  Compute \varepsilon_{2n-1}^2, \eta_{2n-1}^2, c_{2n-1}^2, and  -------*/
/*---    \tau_{2n-1}^2                                               -------*/
/*--------------------------------------------------------------------------*/
    dtmp = dnrm2(dim, wn, 1);
    dtmp = dtmp * dtmp;
    var = dtmp / tau;
    REAL dcos = 1.0 / (1.0 + var);
    tau = dtmp * dcos;
    eta = alpha * dcos;

/*--------------------------------------------------------------------------*/
/*---  Compute x_{2n-1} and the upper bound for its residual norm.  --------*/
/*--------------------------------------------------------------------------*/
    daxpy(dim, eta, dn, 1, xt, 1);

/*--------------------------------------------------------------------------*/
/*---  Compute the residual norm upper bound.                         ------*/
/*---  If the scaled upper bound is within one order of magnitude of  ------*/
/*---  the target convergence norm, compute the true residual norm.   ------*/
/*--------------------------------------------------------------------------*/
    REAL unrm = sqrt(tau * (double)(2*iter));
    REAL uchk = unrm;
    if (tres!=0 || unrm<=10.0*oem->tolerance) {
      mult = xt;
      if (r_precon) {
	dcopy(dim, xt, 1, ptmp, 1);
	(*r_precon)(r_pd, dim, ptmp);
	mult = ptmp;
      }
      (*mv)(mvd, dim, mult, tmp);
      if (l_precon) {
	dcopy(dim, tmp, 1, ptmp, 1);
	dxpay(dim, upbt, 1, -1.0, ptmp, 1);
	(*l_precon)(l_pd, dim, tmp);
	dxpay(dim, bt, 1, -1.0, tmp, 1);
	resn = ddot(dim, tmp, 1, ptmp, 1);
	resn = sqrt(resn);
      }
      else {
	dxpay(dim, bt, 1, -1.0, tmp, 1);
	resn = dnrm2(dim, tmp, 1);
      }
      uchk = resn;
      flag = 1;
    }

/*--------------------------------------------------------------------------*/
/*---  Check for convergence or termination. Stop if:                   ----*/
/*---    1. algorithm converged;                                        ----*/
/*---    2. the residual norm upper bound is smaller than the computed  ----*/
/*---       residual norm by a factor of at least 100.                  ----*/
/*--------------------------------------------------------------------------*/
    if (resn <= oem->tolerance) {
      ierr = 0;
      break;
    }
    else if (100.0*unrm<uchk) {
      ierr = 4;
      break;
    }

    flag = 0;
/*--------------------------------------------------------------------------*/
/*---  Compute y_{2n}, A y_{2n}, d_{2n}, and w_{2n+1}  ---------------------*/
/*--------------------------------------------------------------------------*/
    daxpy(dim, -alpha, vn, 1, yn, 1);
    dtmp = var * dcos;
    dxpay(dim, yn, 1, dtmp, dn, 1);
    mult = yn;
    if (r_precon) {
      dcopy(dim, yn, 1, ptmp, 1);
      (*r_precon)(r_pd, dim, ptmp);
      mult = ptmp;
    }
    (*mv)(mvd, dim, mult, Ayn);
    if (l_precon) (*l_precon)(l_pd, dim, Ayn);
    daxpy(dim, -alpha, Ayn, 1, wn, 1);

/*--------------------------------------------------------------------------*/
/*---  Compute \varepsilon_{2n}^2, \eta_{2n}^2, c_{2n}^2,  -----------------*/
/*---  and \tau_{2n}^2                                     -----------------*/
/*--------------------------------------------------------------------------*/
    dtmp = dnrm2(dim, wn, 1);
    dtmp = dtmp * dtmp;
    var = dtmp / tau;
    dcos = 1.0 / (1.0 + var);
    tau = dtmp * dcos;
    eta = alpha * dcos;

/*--------------------------------------------------------------------------*/
/*---  Compute x_{2n}  -----------------------------------------------------*/
/*--------------------------------------------------------------------------*/
    daxpy(dim, eta, dn, 1, xt, 1);

/*--------------------------------------------------------------------------*/
/*---  Compute the residual norm upper bound.                         ------*/
/*---  If the scaled upper bound is within one order of magnitude of  ------*/
/*---  the target convergence norm, compute the true residual norm.   ------*/
/*--------------------------------------------------------------------------*/
    unrm = sqrt(tau * (double)(2*(iter+1)));
    uchk = unrm;
    if (tres!=0 || unrm<=10.0*oem->tolerance || iter>=oem->max_iter) {
      mult = xt;
      if (r_precon) {
	dcopy(dim, xt, 1, ptmp, 1);
	(*r_precon)(r_pd, dim, ptmp);
	mult = ptmp;
      }
      (*mv)(mvd, dim, mult, tmp);
      if (l_precon) {
	dcopy(dim, tmp, 1, ptmp, 1);
	dxpay(dim, upbt, 1, -1.0, ptmp, 1);
	(*l_precon)(l_pd, dim, tmp);
	dxpay(dim, bt, 1, -1.0, tmp, 1);
	resn = ddot(dim, tmp, 1, ptmp, 1);
	resn = sqrt(resn);
      }
      else {
	dxpay(dim, bt, 1, -1.0, tmp, 1);
	resn = dnrm2(dim, tmp, 1);
      }
      uchk = resn;
      flag = 1;
    }

/*--------------------------------------------------------------------------*/
/*---  Check for convergence or termination. Stop if:                   ----*/
/*---    1. algorithm converged;                                        ----*/
/*---    2. the residual norm upper bound is smaller than the computed  ----*/
/*---       residual norm by a factor of at least 100;                  ----*/
/*---    3. algorithm exceeded the iterations limit.                    ----*/
/*--------------------------------------------------------------------------*/
    if (resn <= oem->tolerance) {
      ierr = 0;
      break;
    }
    else if (100.0*unrm<uchk) {
      ierr = 4;
      break;
    }
    else if (iter>=oem->max_iter) {
      ierr = 4;
    }
  }

  if (iter > oem->max_iter) iter--;
/*--------------------------------------------------------------------------*/
/*---  save solution in x --------------------------------------------------*/
/*--------------------------------------------------------------------------*/
  mult = xt;
  if (!r_precon) daxpy(dim, 1.0, xt, 1, x, 1);
  else if (flag && !l_precon) {
    daxpy(dim, 1.0, ptmp, 1, x, 1);
    mult = ptmp;
  }
  else {
    (*r_precon)(r_pd, dim, xt);
    daxpy(dim, 1.0, xt, 1, x, 1);
  }

/*--------------------------------------------------------------------------*/
/*---  check final residual  -----------------------------------------------*/
/*--------------------------------------------------------------------------*/
  if (!flag) {
    (*mv)(mvd, dim, mult, tmp);
    if (l_precon) {
      dcopy(dim, tmp, 1, ptmp, 1);
      dxpay(dim, upbt, 1, -1.0, ptmp, 1);
      (*l_precon)(l_pd, dim, tmp);
      dxpay(dim, bt, 1, -1.0, tmp, 1);
      resn = ddot(dim, tmp, 1, ptmp, 1);
      resn = sqrt(resn);
    }
    else {
      dxpay(dim, b, 1, -1.0, tmp, 1);
      resn = dnrm2(dim, tmp, 1);
    }
  }

  if (!ierr) {
    SOLVE_INFO(oem, iter, resn, &old_res, ws);
    return(iter);
  }
  else if (ierr==4) {
	oem->terminate_reason = 4;
    BREAK_INFO(oem,"The algorithm did not converge.", iter, resn, &old_res, ws);
    return(iter);
  }
  else if (ierr==8) {
	oem->terminate_reason = 8;
    BREAK_INFO(oem,"The algorithm broke down.", iter, resn, &old_res, ws);
    return(iter);
  }
  else {
    BREAK_INFO(oem,"Unknown error code.", iter, resn, &old_res, ws);
    return(-1000);
  }

  return(0);     /* statement never reached!!! */
}

const char *qmr_strerror(int reason)
{
  switch (reason) {
  case 1:
    return "b == x_0 == 0, x = 0 is the solution of the linear system";

  case 2:
    return "oem_tfqmr:  bad inputs detected ... exiting!";

  case 4:
    return "The algorithm did not converge.";

  case 8:
    return "The algorithm broke down.";

  default:
    break;
  }
  return "Unknown error code.";
}

