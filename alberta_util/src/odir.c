/*--------------------------------------------------------------------------*/
/*  Solving a linear system with "OrhtoDir"                                 */
/*  Classification: OrthoDir_d(A,C,A)                                       */
/*  Variant:        sigma= d.Ad/d.Ad_old                                    */
/*  Matrix:         symmetric and positve                                   */
/*  Workspace:      5*dim                                                   */
/*                                                                          */
/*  C-version of OFM-lib by Willy Doerfler                                  */
/*                                                                          */
/*  author: Kunibert G. Siebert                                             */
/*          Institut fuer Mathematik                                        */
/*          Universitaet Augsburg                                           */
/*          Universitaetsstr. 14                                            */
/*          D-86159 Augsburg, Germany                                       */
/*                                                                          */
/*          http://scicomp.math.uni-augsburg.de/Siebert/                    */
/*                                                                          */
/* (c) by K.G. Siebert (2000-2003)                                          */
/*--------------------------------------------------------------------------*/

#include "alberta_util.h"
#include "alberta_util_intern.h"

int oem_odir(OEM_DATA *oem, int dim, const REAL *b, REAL *x)
{
  FUNCNAME("oem_odir");
  REAL  *Ad, *d, *d_old, *r, *h;
  REAL  res, old_res = -1.0;
  int   iter;

  int   (*mv)(void *, int, const REAL *, REAL *) = oem->mat_vec;
  void  *mvd = oem->mat_vec_data;
  void  (*precon)(void *, int, REAL *) = oem->left_precon;
  void  *pd = oem->left_precon_data;
  REAL  (*scp)(void *, int, const REAL *, const REAL *) = oem->scp;
  void  *sd = oem->scp_data;

  const REAL TOL = 1.e-30;
  WORKSPACE  *ws = CHECK_WORKSPACE(6*dim, oem->ws);

/*--------------------------------------------------------------------------*/
/*---  partitioning of the workspace  --------------------------------------*/
/*--------------------------------------------------------------------------*/
  Ad    = (REAL *)ws->work;
  d     = Ad+dim;
  d_old = d+dim;
  r     = d_old+dim;
  h     = r+dim;

/*--------------------------------------------------------------------------*/
/*---  Initalization  ------------------------------------------------------*/
/*--------------------------------------------------------------------------*/

  oem->terminate_reason = 0;

  if ((scp ? sqrt((*scp)(sd, dim, b, b)) : dnrm2(dim, b, 1)) < TOL
      &&
      (scp ? sqrt((*scp)(sd, dim, x, x)) : dnrm2(dim, x, 1)) < TOL)
  {
	oem->terminate_reason = 1;
    INFO(oem->info,2,
	 "b == x_0 == 0, x = 0 is the solution of the linear system\n");
    dset(dim, 0.0, x, 1);
    oem->initial_residual = oem->residual = 0.0;
    return(0);
  }

  (*mv)(mvd, dim, x, r);
  daxpy(dim, -1.0, b, 1, r, 1);
  dcopy(dim, r, 1, h, 1);
  if (precon) (*precon)(pd, dim, h);

  dcopy(dim, h, 1, d, 1);
  dset(dim, 0.0, d_old, 1);
  REAL dad_old = 1.0;

/*--------------------------------------------------------------------------*/
/*---  check initial residual  ---------------------------------------------*/
/*--------------------------------------------------------------------------*/

  res = scp ? sqrt((*scp)(sd, dim, r, r)) : dnrm2(dim, r, 1);
  oem->initial_residual = res;

  START_INFO(oem);
  if (SOLVE_INFO(oem, 0, res, &old_res, ws))
    return(0);

/*--------------------------------------------------------------------------*/
/*---  Iteration  ----------------------------------------------------------*/
/*--------------------------------------------------------------------------*/

  for (iter = 1; iter <= oem->max_iter; iter++)
  {
/*--------------------------------------------------------------------------*/
/*---  calculate r.d, A.d and d.A.d  ---------------------------------------*/
/*--------------------------------------------------------------------------*/
    REAL rd = scp ? (*scp)(sd, dim, r, d) : ddot(dim, r, 1, d, 1);
    (*mv)(mvd, dim, d, Ad);
    REAL dad = scp ? (*scp)(sd, dim, d, Ad) : ddot(dim, d, 1, Ad, 1);

    if (ABS(dad) < TOL)
    {
      oem->terminate_reason = 2;
      BREAK_INFO(oem, "(Ad,d)_2 = 0", iter, res, &old_res, ws);
      return(iter);
    }

/*--------------------------------------------------------------------------*/
/*---   update x and calculate new r  --------------------------------------*/
/*--------------------------------------------------------------------------*/
    REAL alpha = rd/dad;
    daxpy(dim, -alpha, d, 1, x, 1);
    daxpy(dim, -alpha, Ad, 1, r, 1);

    res = scp ? sqrt((*scp)(sd, dim, r, r)) : dnrm2(dim, r, 1);
    if (SOLVE_INFO(oem, iter, res, &old_res, ws))
      return(iter);

    dcopy(dim, Ad, 1, h, 1);
    if (precon) (*precon)(pd, dim, h);

/*--------------------------------------------------------------------------*/
/*---  calculate gamma and sigma  ------------------------------------------*/
/*--------------------------------------------------------------------------*/

      REAL gamma = (scp ? (*scp)(sd, dim, h, Ad) : ddot(dim, h, 1, Ad, 1))/dad;
      REAL sigma = dad/dad_old;

/*--------------------------------------------------------------------------*/
/*---  compute new d and save old values  ----------------------------------*/
/*--------------------------------------------------------------------------*/

      dswap(dim, d, 1, d_old, 1);
      dscal(dim, -sigma, d, 1);
      daxpy(dim, -gamma, d_old, 1, d, 1);
      dexpy(dim, h, 1, d, 1);
      dad_old= dad;
  }
  return(0);    /*  statement never reached!!!                              */
}


const char *odir_strerror(int reason)
{
  switch (reason) {
  case 1:
    return "b == x_0 == 0, x = 0 is the solution of the linear system";

  case 2:
    return "(Ad,d)_2 = 0";


  default:
    break;
  }
  return "Unknown error code.";
}

