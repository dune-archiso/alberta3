      subroutine dxpay(n,dx,incx,da,dy,incy)
      implicit NONE
      real*8   dx(1),dy(1),da
      integer  i,incx,incy,ix,iy,m,mod,n

      if (n.le.0) return
      if (da.eq.0.d0) then
         call dcopy(n,dx,incx,dy,incy)
         return
      end if

      if (incx.eq.1.and.incy.eq.1) then
c        *** unrolled loop ***
         m= mod(n,4)
         if (m.eq.0) then
            m= 1
          else
            do 1000 i=1,m
               dy(i)= da*dy(i)+dx(i)
 1000       continue
            if (n.lt.4) then
               return
            end if
            m= m+1
         end if
         do 1002 i=m,n,4
            dy(i)  = da*dy(i)  +dx(i)
            dy(i+1)= da*dy(i+1)+dx(i+1)
            dy(i+2)= da*dy(i+2)+dx(i+2)
            dy(i+3)= da*dy(i+3)+dx(i+3)
 1002    continue
       else
         ix= 1
         iy= 1
         if (incx.lt.0) ix= (-n+1)*incx+1
         if (incy.lt.0) iy= (-n+1)*incy+1
         do 1004 i=1,n
            dy(iy)= da*dy(iy)+dx(ix)
            ix    = ix+incx
            iy    = iy+incy
 1004    continue
      end if

      return
      end
