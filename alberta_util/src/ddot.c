#include "alberta_util_intern.h"

double ddot(int n, const double *x, int ix, const double *y, int iy)
{
  return DDOT_F77(&n, x, &ix, y, &iy);
}
