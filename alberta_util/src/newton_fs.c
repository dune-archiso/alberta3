/*--------------------------------------------------------------------------*/
/*  solve nonlinear systems by a Newton method with step size control       */
/*                                                                          */
/*  Workspace:      3*dim                                                   */
/*                                                                          */
/*  C-version of NGL-lib by Willy Doerfler                                  */
/*                                                                          */
/*  author: Kunibert G. Siebert                                             */
/*          Institut fuer Mathematik                                        */
/*          Universitaet Augsburg                                           */
/*          Universitaetsstr. 14                                            */
/*          D-86159 Augsburg, Germany                                       */
/*                                                                          */
/*          http://scicomp.math.uni-augsburg.de/Siebert/                    */
/*                                                                          */
/* (c) by K.G. Siebert (2000-2003)                                          */
/*--------------------------------------------------------------------------*/

#include "alberta_util.h"
#include "alberta_util_intern.h"

int nls_newton_fs(NLS_DATA *data, int dim, REAL *x)
{
  FUNCNAME("nls_newton_fs");
  REAL *b, *d, *y;
  REAL err, err_old, tau;
  int  iter, m, mmax, halved;

  int  info = data->info;
  void (*update)(void *, int, const REAL *, bool, REAL *) = data->update;
  void *ud = data->update_data;
  int  (*solve)(void *, int, const REAL *, REAL *) = data->solve;
  void *sd = data->solve_data;
  REAL (*norm)(void *, int, const REAL *) = data->norm;
  void *nd = data->norm_data;

  WORKSPACE  *ws = CHECK_WORKSPACE(3*dim, data->ws);

/*--- Memory initialization ------------------------------------------------*/
  b  = (REAL *)ws->work;
  d  = b+dim;
  y  = d+dim;

/*--------------------------------------------------------------------------*/
/*--- Newton initialization ------------------------------------------------*/
/*--------------------------------------------------------------------------*/

  (*update)(ud, dim, x, 0, b);           /*-- update F(x)  -----------------*/
  err = err_old = norm ? (*norm)(nd, dim, b) : dnrm2(dim, b, 1);
  data->initial_residual = err;

  INFO(info,2,"iter. |     residual |     red. |    n |  m |\n");
  INFO(info,2,"%5d | %12.5le | -------- | ---- | -- |\n", 0, err);

  if ((data->residual = err) < data->tolerance)
  {
    INFO(info,4,"finished succesfully\n");
    if (ws != data->ws) FREE_WORKSPACE(ws);
    return(0);
  }

/*--- still initalization part ---------------------------------------------*/
  
  mmax        = MAX(2,MIN(data->restart,32));
  m           = 0;
  tau         = 1.0;
  halved      = true;

/*--- start iterations -----------------------------------------------------*/

  for (iter = 1; iter <= data->max_iter+1; iter++)
  {
    (*update)(ud, dim, x, 1, NULL);       /*-- update DF(x) -----------------*/

    dset(dim, 0.0, d, 1);                /*-- initial guess is zero --------*/
    int n = (*solve)(sd, dim, b, d);         /*-- solve DF(x) d = b ------------*/

/*--- look for step size ---------------------------------------------------*/
    if (!halved)
    {
      m = MAX(m-1,0);
      tau = tau < 0.5 ? 2.0*tau : 1.0;
    }

    for (int j = 0; j <= mmax; j++)
    {
/*--- aim: |F(u_k+\tau d)| \le (1-0.5\tau) |F(u)| --------------------------*/
      dcopy(dim, x, 1, y, 1);            /*-- y = x ------------------------*/
      daxpy(dim, -tau, d, 1, y, 1);      /*-- y -= tau*d -------------------*/
      (*update)(ud, dim, y, 0, b);       /*-- update F(y) ------------------*/
      
      err = norm ? (*norm)(nd, dim, b) : dnrm2(dim, b, 1);
      if (err <= (1.0 - 0.5*tau)*err_old)
      {
	halved = j > 0;
	break;
      }
      else
      {
	if (m == mmax) break;
	
	m++;
	tau *= 0.5;
      }
    }
    dcopy(dim, y, 1, x, 1);             /*-- x = y  (update x!) ------------*/

    if (err_old <= 0)
      INFO(info,2,"%5d | %12.5le | -------- | %4d | %2d |\n", 
			 iter, err, n, m);
    else
      INFO(info,2,"%5d | %12.5le | %8.2le | %4d | %2d |\n", 
		       iter, err, err/err_old, n, m);

    if ((data->residual = err) < data->tolerance && m == 0)
    {
      INFO(info,4,"finished successfully\n");
      if (ws != data->ws) FREE_WORKSPACE(ws);
      return(iter);
    } 
    else if (iter > data->max_iter)
    {
      if (info < 2) {
	INFO(info,1,"iter. %d, residual: %12.5le\n", iter, err);
      }
      INFO(info,1,"tolerance %le not reached\n", data->tolerance);
      if (ws != data->ws) FREE_WORKSPACE(ws);
      return(iter);
    }
    err_old = err;
  }
  return(iter);  /*--- statement never reached -----------------------------*/
}
