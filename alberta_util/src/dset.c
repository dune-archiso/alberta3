void dset(int n, double alpha, double *x, int ix)
{
  int i, nix = n*ix;
  for (i = 0; i < nix; i += ix)
    x[i] = alpha;
  return;
}
