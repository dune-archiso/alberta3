#! /bin/sh
#
# invocation with
#
# SCRIPT SRC DST
#

SOURCES=""
while test "$#" != "1"
do
    SRC="$1"
    DIR=`dirname "${SRC}"`
    SRC=`basename "${SRC}"`
    DIR=`cd "${DIR}"; pwd`
    SOURCES="${SOURCES} ${DIR}/${SRC}"
    shift
done

DST="$1"

ln -sf ${SOURCES} "${DST}"


