\section{Per-element initializers for quadrature rules and basis function sets}
\label{S:init_element}

This section is \emph{not} concerned with the per-element initializers
contained in the \code{OPERATOR\_INFO}, \code{BNDRY\_OPERATOR\_INFO}
and \code{PARAMETRIC} data-structures, they obey other rules and are
explained in the respective sections, see
\secref{S:matrix_assemblage_scalar} and \secref{S:parametric_meshes}.

\idx{Per-element initializers!Q11_PSI_PHI@{\code{Q11\_PSI\_PHI}}|(}
\idx{Per-element initializers!Q01_PSI_PHI@{\code{Q01\_PSI\_PHI}}|(}
\idx{Per-element initializers!Q10_PSI_PHI@{\code{Q10\_PSI\_PHI}}|(}
\idx{Per-element initializers!Q00_PSI_PHI@{\code{Q00\_PSI\_PHI}}|(}
\idx{Per-element initializers!QUAD_FAST@{\code{QUAD\_FAST}}|(}
\idx{Per-element initializers!QUAD@{\code{QUAD}}|(}
\idx{Per-element initializers!WALL\_QUAD@{\code{WALL\_QUAD}}|(}
\idx{Per-element initializers!BAS\_FCTS@{\code{BAS\_FCTS}}|(}
%%
\idx{init_element()@{\code{init\_element()}}!Q11_PSI_PHI@{\code{Q11\_PSI\_PHI}}|(}
\idx{init_element()@{\code{init\_element()}}!Q01_PSI_PHI@{\code{Q01\_PSI\_PHI}}|(}
\idx{init_element()@{\code{init\_element()}}!Q10_PSI_PHI@{\code{Q10\_PSI\_PHI}}|(}
\idx{init_element()@{\code{init\_element()}}!Q00_PSI_PHI@{\code{Q00\_PSI\_PHI}}|(}
\idx{init_element()@{\code{init\_element()}}!QUAD_FAST@{\code{QUAD\_FAST}}|(}
\idx{init_element()@{\code{init\_element()}}!QUAD@{\code{QUAD}}|(}
\idx{init_element()@{\code{init\_element()}}!WALL\_QUAD@{\code{WALL\_QUAD}}|(}
\idx{init_element()@{\code{init\_element()}}!BAS\_FCTS@{\code{BAS\_FCTS}}|(}
\subsection{Basics}
Several data-structure allow for a function hook which is used to
perform per-element initialization. This is useful in, e.g., for
vector-valued basis functions which depend on the element geometry
like face-bubbles, or Raviart-Thomas elements, and in other contexts.
Other prominent examples can be quadrature rules in the context of
unfitted finite element methods, or for the integration of
discontinues functions, where the discontinuity is co-dimension $1$
sub-manifold intersecting the mesh, cutting wildly through the
element, e.g. for interface problems.

The basic data structures allowing for such initializers are
\begin{descr}
\kitem{BAS\_FCTS} (\secref{S:BAS_FCTS})
\kitem{QUAD} (\secref{S:QUAD})
\kitem{WALL\_QUAD} (\secref{S:WALL_QUAD})
\end{descr}

Naturally, the quadrature caches should derive from any per-element
initializers present in the underlying quadrature and basis-function
data-structures, so the following data-structures may have per-element
initializers as well:
\begin{descr}
\kitem{QUAD\_FAST} (\secref{S:QUAD_FAST})
\kitem{WALL\_QUAD\_FAST} (\secref{S:WALL_QUAD_FAST})
\kitem{Q11\_PSI\_PHI} (\secref{S:Q11_PSI_PHI})
\kitem{Q01\_PSI\_PHI} (\secref{S:Q01_PSI_PHI})
\kitem{Q10\_PSI\_PHI} (\secref{S:Q10_PSI_PHI})
\kitem{Q00\_PSI\_PHI} (\secref{S:Q00_PSI_PHI})
\end{descr}
The derived initializers are assigned during the construction of the
quadrature caches, examining the underlying \code{BAS\_FCTS} and
\code{QUAD} structures. The \code{get\_quad\_fast()} and
\code{get\_wall\_quad\_fast()} routines, as well as the constructors
for the \code{\dots\_PSI\_PHI}-caches take care of this automatically.

The initialization-subroutine is hooked as a function-pointer into the
data-structure. The basic definitions are

\fdx{INIT_ELEMENT()@{\code{INIT\_ELEMENT()}}|(}
%%
\cdx{INIT_EL_TAG_NONE@{\code{INIT\_EL\_TAG\_NONE}}|(}
\cdx{INIT_EL_TAG_DFLT@{\code{INIT\_EL\_TAG\_DFLT}}|(}
\cdx{INIT_EL_TAG_NULL@{\code{INIT\_EL\_TAG\_NULL}}|(}
%%
\mdx{INIT_ELEMENT()@{\code{INIT\_ELEMENT()}}|(}
\mdx{INIT_OBJECT()@{\code{INIT\_OBJECT()}}|(}
%%
\idx{Per-element initializers!INIT_ELEMENT()@{\code{INIT\_ELEMENT()}}|(}
\idx{Per-element initializers!INIT_OBJECT()@{\code{INIT\_OBJECT()}}|(}
\idx{Per-element initializers!INIT_EL_TAG_NONE@{\code{INIT\_EL\_TAG\_NONE}}|(}
\idx{Per-element initializers!INIT_EL_TAG_NULL@{\code{INIT\_EL\_TAG\_NULL}}|(}
\idx{Per-element initializers!INIT_EL_TAG_DFLT@{\code{INIT\_EL\_TAG\_DFLT}}|(}
%%
\idx{init_element()@{\code{init\_element()}}!INIT_ELEMENT()@{\code{INIT\_ELEMENT()}}|(}
\idx{init_element()@{\code{init\_element()}}!INIT_OBJECT()@{\code{INIT\_OBJECT()}}|(}
\idx{init_element()@{\code{init\_element()}}!INIT_EL_TAG_NONE@{\code{INIT\_EL\_TAG\_NONE}}|(}
\idx{init_element()@{\code{init\_element()}}!INIT_EL_TAG_NULL@{\code{INIT\_EL\_TAG\_NULL}}|(}
\idx{init_element()@{\code{init\_element()}}!INIT_EL_TAG_DFLT@{\code{INIT\_EL\_TAG\_DFLT}}|(}
\bv\begin{lstlisting}[name=INIT_ELEMENT,label=C:INIT_ELEMENT]
typedef unsigned int INIT_EL_TAG;
typedef INIT_EL_TAG (*INIT_ELEMENT_FCT)(const EL_INFO *el_info, void *thisptr);

enum {
  INIT_EL_TAG_NONE = 0, /* invalid tag */
  INIT_EL_TAG_DFLT = 1, /* default case */
  INIT_EL_TAG_NULL = 2  /* something is 0, e.g. no quad-points, basis
			 * functions are identically zero and so on.
			 */
};

#define INIT_ELEMENT(el_info, object)					\
  (if (object->init_element != NULL)					\
   ? object->init_element(el_info, (void *)object) : INIT_EL_TAG_DFLT)

#define INIT_OBJECT(object) (void)INIT_ELEMENT(NULL, object)
\end{lstlisting}\ev

The idea behind the ``tag'' definitions is that an object possibly may
have a default-state on the majority of mesh-elements, possibly may
evaluate to an empty object on many elements (e.g. the number of basis
functions is zero, or the quadrature rule has no points), and has a
special state on some of the elements. This is, for example, the case
when defining a quadrature rule in the context of unfitted finite
elements, where most mesh-elements belong to the interior of the
domain, may are located outside the domain of computation, and some
are actually intersected by the boundary. To handle such cases
efficiently, \code{INIT\_ELEMENT(el\_info, object)} must follow these
conventions:
\begin{itemize}
\item INIT\_ELEMENT(el\_info, object) evaluates to
  \code{INIT\_EL\_TAG\_DFLT} when no per-element initializer is
  present.
\item An \code{init\_element()} method \emph{must} allow a \nil
  pointer for the \code{el\_info} argument. If called with
  \code{el\_info == \nil} the \code{init\_element()} method must
  restore its default state.  The ``default case'' is what the
  implementation defines as default; for performance reasons the
  default case should be the one which applies to the majority of mesh
  elements. The convenience-macro \code{INIT\_OBJECT(object)} just
  forwards to \code{INIT\_ELEMENT(\nil, object)}.
\item The return value of the \code{init\_element()} method must be
  \code{INIT\_EL\_TAG\_DFLT} for the default case.
\item The return value of the \code{init\_element()} method must be
  \code{INIT\_EL\_TAG\_NULL} for the \nil case, meaning, e.g., the
  number of basis functions is zero, or the number of quadrature
  points is zero. The application can assume that in the \nil case the
  structure does not contain any real data.
\item In all other cases the return value is a tag which is used to
  efficiently cache values of intermediate computations, e.g. the
  values of basis functions at quadrature points. This tag should be
  locally unique, meaning that consecutive invocations of
  \code{init\_element()} should return different tags for different
  simplexes.  This can be used for optimizations: if the tag returned
  by an \code{init\_element()} routine does not change, then the
  calling function may assume that the underlying object has not
  changed.
\end{itemize}

The meaning of the reserved tag-names is
\begin{descr}
  \kitem{INIT\_EL\_TAG\_NONE} An invalid tag that does not correspond
  to any state of the underlying object. If an object is in this
  state, then the data contained in the object is undefined and its
  per-element initializer must be called before accessing any
  components of the object (with the exception of the
  \code{init\_element()}-hook itself, of course.
  %%
  \kitem{INIT\_EL\_TAG\_NULL} The object is in the \nil-state. An
  application can assume that in this state a quadrature rule contains
  no points or that the local basis function set is empty.
  %%
  \kitem{INIT\_EL\_TAG\_DFLT} The object is in a default state. What
  that means is implementation dependent. A sensible implementation
  should choose as default state the state it attains on the majority
  of mesh elements. It can be that an objects does not have any
  sensible default state, for example if it is a local basis functions
  set depending on the element geometry.
  %%
  \kitem{\dots any other number} defines a unique state. The
  implementation underlying a quadrature rule or a local basis
  function set should make sure that repeated calls to the
  \code{init\_element()}-hook return the same tag. This can then be
  used by applications to cache derived data across sub-routine calls,
  using the tag-value to invalidate the caches.
\end{descr}

\subsection{Per-element initializers and vector-valued basis functions}
\label{S:init_element_vector_bfcts}

\idx{Per-element initializers!vector-valued basis functions}
\idx{init_element()@{\code{init\_element()}}!vector-valued basis functions}
%%
There is one thing special for vector-valued geometry-dependent basis
function sets. Following the rules developed above, the
\code{init\_element()}-hook for such functions would have to return a
unique tag on each element, thus invalidating the quadrature caches on
each new element. Because this is inefficient, \ALBERTA's
implementation factors vector-valued basis-functions into a geometry
dependent vector-valued part and a geometry-independent scalar part.
Vector-valued \code{BAS\_FCTS} instances carrying a
\code{init\_element()} method should therefore return
\code{INIT\_EL\_TAG\_DFLT} if just the vector-valued factor has
changed, but the scalar factor is not affected by the
\code{init\_element()}-method. This way the quadratures caches defined
by \code{QUAD\_FAST} and the \code{\dots\_PSI\_PHI}-caches are not
invalidated, which helps to keep the assembling of linear systems
efficient. Compare also the remarks in \secref{S:vector_bfcts},
dealing with vector-valued basis functions in general.

\fdx{INIT_ELEMENT()@{\code{INIT\_ELEMENT()}}|)}
%%
\cdx{INIT_EL_TAG_NONE@{\code{INIT\_EL\_TAG\_NONE}}|)}
\cdx{INIT_EL_TAG_DFLT@{\code{INIT\_EL\_TAG\_DFLT}}|)}
\cdx{INIT_EL_TAG_NULL@{\code{INIT\_EL\_TAG\_NULL}}|)}
%%
\mdx{INIT_ELEMENT()@{\code{INIT\_ELEMENT()}}|)}
\mdx{INIT_OBJECT()@{\code{INIT\_OBJECT()}}|)}
%%
\idx{Per-element initializers!INIT_ELEMENT()@{\code{INIT\_ELEMENT()}}|)}
\idx{Per-element initializers!INIT_OBJECT()@{\code{INIT\_OBJECT()}}|)}
\idx{Per-element initializers!INIT_EL_TAG_NONE@{\code{INIT\_EL\_TAG\_NONE}}|)}
\idx{Per-element initializers!INIT_EL_TAG_NULL@{\code{INIT\_EL\_TAG\_NULL}}|)}
\idx{Per-element initializers!INIT_EL_TAG_DFLT@{\code{INIT\_EL\_TAG\_DFLT}}|)}
%%
\idx{init_element()@{\code{init\_element()}}!INIT_ELEMENT()@{\code{INIT\_ELEMENT()}}|)}
\idx{init_element()@{\code{init\_element()}}!INIT_OBJECT()@{\code{INIT\_OBJECT()}}|)}
\idx{init_element()@{\code{init\_element()}}!INIT_EL_TAG_NONE@{\code{INIT\_EL\_TAG\_NONE}}|)}
\idx{init_element()@{\code{init\_element()}}!INIT_EL_TAG_NULL@{\code{INIT\_EL\_TAG\_NULL}}|)}
\idx{init_element()@{\code{init\_element()}}!INIT_EL_TAG_DFLT@{\code{INIT\_EL\_TAG\_DFLT}}|)}

\subsection{Tag management}

To aid the implementation of the scheme described above there are
some support structures and macros concerning the management of the
tags returned by the \code{init\_element()} hooks:

\ddx{INIT_EL_TAG_CTX@{\code{INIT\_EL\_TAG\_CTX}}|(}
\mdx{INIT_EL_TAG_CTX_INIT()@{\code{INIT\_EL\_TAG\_CTX\_INIT()}}|(}
\mdx{INIT_EL_TAG_CTX_UNIQ()@{\code{INIT\_EL\_TAG\_CTX\_UNIQ()}}|(}
\mdx{INIT_EL_TAG_CTX_NULL()@{\code{INIT\_EL\_TAG\_CTX\_NULL()}}|(}
\mdx{INIT_EL_TAG_CTX_DFLT()@{\code{INIT\_EL\_TAG\_CTX\_DFLT()}}|(}
\mdx{INIT_EL_TAG_CTX_TAG()@{\code{INIT\_EL\_TAG\_CTX\_TAG()}}|(}
%%
\idx{Per-element initializers!INIT_EL_TAG_CTX@{\code{INIT\_EL\_TAG\_CTX}}|(}
\idx{Per-element initializers!INIT_EL_TAG_CTX_INIT()@{\code{INIT\_EL\_TAG\_INIT()}}|(}
\idx{Per-element initializers!INIT_EL_TAG_CTX_UNIQ()@{\code{INIT\_EL\_TAG\_UNIQ()}}|(}
\idx{Per-element initializers!INIT_EL_TAG_CTX_NULL()@{\code{INIT\_EL\_TAG\_NULL()}}|(}
\idx{Per-element initializers!INIT_EL_TAG_CTX_DFLT()@{\code{INIT\_EL\_TAG\_DFLT()}}|(}
\idx{Per-element initializers!INIT_EL_TAG_CTX_TAG()@{\code{INIT\_EL\_TAG\_TAG()}}|(}
%%
\idx{init_element()@{\code{init\_element()}}!INIT_EL_TAG_CTX@{\code{INIT\_EL\_TAG\_CTX}}|(}
\idx{init_element()@{\code{init\_element()}}!INIT_EL_TAG_CTX_INIT()@{\code{INIT\_EL\_TAG\_INIT()}}|(}
\idx{init_element()@{\code{init\_element()}}!INIT_EL_TAG_CTX_UNIQ()@{\code{INIT\_EL\_TAG\_UNIQ()}}|(}
\idx{init_element()@{\code{init\_element()}}!INIT_EL_TAG_CTX_NULL()@{\code{INIT\_EL\_TAG\_NULL()}}|(}
\idx{init_element()@{\code{init\_element()}}!INIT_EL_TAG_CTX_DFLT()@{\code{INIT\_EL\_TAG\_DFLT()}}|(}
\idx{init_element()@{\code{init\_element()}}!INIT_EL_TAG_CTX_TAG()@{\code{INIT\_EL\_TAG\_TAG()}}|(}
%%
\bv\begin{lstlisting}
/* Tag context. */
typedef struct init_el_tag_ctx {
  INIT_EL_TAG  tag;
  unsigned int cnt;
} INIT_EL_TAG_CTX;

#define INIT_EL_TAG_CTX_INIT(ctx)		\
  {						\
    (ctx)->tag = INIT_EL_TAG_DFLT;		\
    (ctx)->cnt = 0;				\
  }

/* Generate a new unique tag != NULL & DFLT */
#define INIT_EL_TAG_CTX_UNIQ(ctx)			\
  {							\
    (ctx)->tag = INIT_EL_TAG_NULL + (++((ctx)->cnt));	\
    if ((ctx)->tag == INIT_EL_TAG_NONE) {		\
      (ctx)->cnt = 1;					\
      (ctx)->tag = INIT_EL_TAG_NULL + 1;		\
    }							\
  }
#define INIT_EL_TAG_CTX_NULL(ctx) (ctx)->tag = INIT_EL_TAG_NULL
#define INIT_EL_TAG_CTX_DFLT(ctx) (ctx)->tag = INIT_EL_TAG_DFLT
#define INIT_EL_TAG_CTX_TAG(ctx)  (ctx)->tag

#define INIT_ELEMENT_DECL			\
  INIT_ELEMENT_FCT init_element;		\
  FLAGS fill_flags;				\
  INIT_EL_TAG_CTX tag_ctx

#define INIT_ELEMENT_INITIALIZER(init_el, flags)	\
  (init_el), (flags), { INIT_EL_TAG_DFLT, 0 }
\end{lstlisting}\ev
%%
The meaning of the components of \code{INIT\_EL\_TAG\_CTX}:
\begin{descr}
  \kitem{tag} The current tag value.
  %%
  \kitem{cnt} A counter, used to generate locally unique per-element
  tags.
\end{descr}
%%
An application \emph{must not} access the two components of the
tag-context directly, but has direct the access to the tag and the
counter through the access macros defined above. Some for the
implementation an object carrying such a per-element initializers.
Obeying this rule ensures compatibility with future version of
\ALBERTA, hopefully.
The meaning of the tag-management-macros is as follows:
\begin{descr}
  \kitem{INIT\_EL\_TAG\_CTX\_INIT(ctx)} Initialize the points
  \code{ctx}, pointing to an existing tag-context.
  %%
  \kitem{INIT\_EL\_TAG\_CTX\_UNIQ(ctx)} Generate a new uniquq tag by
  incrementing \code{ctx->cnt}. The macro takes care of jumping over
  the reserved tags \code{INIT\_EL\_TAG\_NONE},
  \code{INIT\_EL\_TAG\_CTX\_NULL} and \code{INIT\_EL\_TAG\_DFLT}, thus
  protecting the generated tags against wrap-around.
  %%
  \kitem{INIT\_EL\_TAG\_CTX\_NULL(ctx)} Set \code{ctx->tag} to
  \code{INIT\_EL\_TAG\_NULL}.
  %%
  \kitem{INIT\_EL\_TAG\_CTX\_DFLT(ctx)} Set \code{ctx->tag} to
  \code{INIT\_EL\_TAG\_DFLT}.
  %%
  \kitem{INIT\_EL\_TAG\_CTX\_TAG(ctx)} Return the current tag.
\end{descr}
%%
\ddx{INIT_EL_TAG_CTX@{\code{INIT\_EL\_TAG\_CTX}}|)}
\mdx{INIT_EL_TAG_CTX_INIT()@{\code{INIT\_EL\_TAG\_CTX\_INIT()}}|)}
\mdx{INIT_EL_TAG_CTX_UNIQ()@{\code{INIT\_EL\_TAG\_CTX\_UNIQ()}}|)}
\mdx{INIT_EL_TAG_CTX_NULL()@{\code{INIT\_EL\_TAG\_CTX\_NULL()}}|)}
\mdx{INIT_EL_TAG_CTX_DFLT()@{\code{INIT\_EL\_TAG\_CTX\_DFLT()}}|)}
\mdx{INIT_EL_TAG_CTX_TAG()@{\code{INIT\_EL\_TAG\_CTX\_TAG()}}|)}
%%
\idx{Per-element initializers!INIT_EL_TAG_CTX@{\code{INIT\_EL\_TAG\_CTX}}|)}
\idx{Per-element initializers!INIT_EL_TAG_CTX_INIT()@{\code{INIT\_EL\_TAG\_INIT()}}|)}
\idx{Per-element initializers!INIT_EL_TAG_CTX_UNIQ()@{\code{INIT\_EL\_TAG\_UNIQ()}}|)}
\idx{Per-element initializers!INIT_EL_TAG_CTX_NULL()@{\code{INIT\_EL\_TAG\_NULL()}}|)}
\idx{Per-element initializers!INIT_EL_TAG_CTX_DFLT()@{\code{INIT\_EL\_TAG\_DFLT()}}|)}
\idx{Per-element initializers!INIT_EL_TAG_CTX_TAG()@{\code{INIT\_EL\_TAG\_TAG()}}|)}
%%
\idx{init_element()@{\code{init\_element()}}!INIT_EL_TAG_CTX@{\code{INIT\_EL\_TAG\_CTX}}|)}
\idx{init_element()@{\code{init\_element()}}!INIT_EL_TAG_CTX_INIT()@{\code{INIT\_EL\_TAG\_INIT()}}|)}
\idx{init_element()@{\code{init\_element()}}!INIT_EL_TAG_CTX_UNIQ()@{\code{INIT\_EL\_TAG\_UNIQ()}}|)}
\idx{init_element()@{\code{init\_element()}}!INIT_EL_TAG_CTX_NULL()@{\code{INIT\_EL\_TAG\_NULL()}}|)}
\idx{init_element()@{\code{init\_element()}}!INIT_EL_TAG_CTX_DFLT()@{\code{INIT\_EL\_TAG\_DFLT()}}|)}
\idx{init_element()@{\code{init\_element()}}!INIT_EL_TAG_CTX_TAG()@{\code{INIT\_EL\_TAG\_TAG()}}|)}

\subsection{Mesh-traversal and per-element initializers}

Objects which depend on the mesh-element they are living on often may
require special information, for instance about the geometry of the
mesh-element. Because this information is only selectively available
during a mesh-traversal -- \ALBERTA fills most information in a
root-to-leaf manner, compare \secref{S:traverse} -- there is danger
that the current \code{EL\_INFO} structure does not carry enough
information in order for the \code{init\_element()}-method to do its
work. To cope with this problem an object with such an initializer
should also record its needs concerning the available information
during mesh-traversal in an additional component \code{FLAGS
  fill\_flag}. It is advisable that implementations for
element-dependent objects make use of the following definitions from
\code{alberta.h}:

\mdx{INIT_ELEMENT_DECL@{\code{INIT\_ELEMENT\_DECL}}}
\idx{Per-element initializers!INIT_ELEMENT_DECL@{\code{INIT\_ELEMENT\_DECL}}}
\idx{init_element()@{\code{init\_element()}}!INIT_ELEMENT_DECL@{\code{INIT\_ELEMENT\_DECL}}}
\bv\begin{lstlisting}[label=M:INIT_ELEMENT_DECL]
#define INIT_ELEMENT_DECL			\
  INIT_ELEMENT_FCT init_element;		\
  FLAGS fill_flags;				\
  INIT_EL_TAG_CTX tag_ctx

#define INIT_ELEMENT_INITIALIZER(init_el, flags)	\
  (init_el), (flags), { INIT_EL_TAG_DFLT, 0 }
\end{lstlisting}\ev

The \code{INIT\_ELEMENT\_DECL} macro should be inserted in the
definition of each structure carrying such an initializer, e.g.
%%
\bv\begin{lstlisting}
struct foobar
{
  ... /* other stuff */
  INIT_ELEMENT_DECL;
  ... /* other stuff */
};
\end{lstlisting}\ev
%%
The macro \code{INIT\_ELEMENT\_INITIALIZER()} can be used during the
(static) initialization of such data-structures, e.g.
%%
\bv\begin{lstlisting}
static struct foobar = {
  ... /* other stuff */,
  INIT_ELEMENT_INITIALIZER(FILL_NEIGH|FILL_COORDS, foobar_init),
  ... /* other stuff */
};
\end{lstlisting}\ev

Compare also the definitions for the data-structures in the
source-listings on the pages \pageref{T:QUAD}, \pageref{T:BAS_FCTS},
\pageref{T:WALL_QUAD}, \pageref{T:QUAD_FAST},
\pageref{T:WALL_QUAD_FAST}, \pageref{T:Q11_PSI_PHI},
\pageref{T:Q10_PSI_PHI}, \pageref{T:Q01_PSI_PHI},
\pageref{T:Q00_PSI_PHI}.
%%
\exampleref{E:parametric_mesh_traverse} demonstrates in a
half-real world setting how to take care of such fill-flags and
per-element initializers of \code{BAS\_FCTS} structures, see there.

%% Index lead-out:
%%
\idx{Per-element initializers!Q11_PSI_PHI@{\code{Q11\_PSI\_PHI}}|)}
\idx{Per-element initializers!Q01_PSI_PHI@{\code{Q01\_PSI\_PHI}}|)}
\idx{Per-element initializers!Q10_PSI_PHI@{\code{Q10\_PSI\_PHI}}|)}
\idx{Per-element initializers!Q00_PSI_PHI@{\code{Q00\_PSI\_PHI}}|)}
\idx{Per-element initializers!QUAD_FAST@{\code{QUAD\_FAST}}|)}
\idx{Per-element initializers!QUAD@{\code{QUAD}}|)}
\idx{Per-element initializers!WALL\_QUAD@{\code{WALL\_QUAD}}|)}
\idx{Per-element initializers!BAS\_FCTS@{\code{BAS\_FCTS}}|)}
%%
\idx{init_element()@{\code{init\_element()}}!Q11_PSI_PHI@{\code{Q11\_PSI\_PHI}}|)}
\idx{init_element()@{\code{init\_element()}}!Q01_PSI_PHI@{\code{Q01\_PSI\_PHI}}|)}
\idx{init_element()@{\code{init\_element()}}!Q10_PSI_PHI@{\code{Q10\_PSI\_PHI}}|)}
\idx{init_element()@{\code{init\_element()}}!Q00_PSI_PHI@{\code{Q00\_PSI\_PHI}}|)}
\idx{init_element()@{\code{init\_element()}}!QUAD_FAST@{\code{QUAD\_FAST}}|)}
\idx{init_element()@{\code{init\_element()}}!QUAD@{\code{QUAD}}|)}
\idx{init_element()@{\code{init\_element()}}!WALL\_QUAD@{\code{WALL\_QUAD}}|)}
\idx{init_element()@{\code{init\_element()}}!BAS\_FCTS@{\code{BAS\_FCTS}}|)}

%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "alberta-man"
%%% End: 
