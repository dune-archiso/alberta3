%format albert
\RequirePackage{ifpdf}
\ifpdf
\documentclass[pdftex,11pt,twoside,a4paper]{book}
\else
\documentclass[dvips,11pt,twoside,a4paper]{book}
\fi

\usepackage{latexsym,amssymb,amsthm,amsmath}
%\usepackage[notcite,notref]{showkeys}
\usepackage[english]{babel}
\usepackage{xspace}
\usepackage{fancyvrb}
\usepackage{newvbtm}
\usepackage{verbdef}
\usepackage{listings} %% better support for source-code listing
\usepackage{url}
\usepackage{path}
% Better processing of multiple indices, unfortunately this package is 
% not standard on my machine. (DK)   :-|
%\usepackage{makeidx}
\usepackage[makeindex,idxcommands]{splitidx}
\usepackage{comment}
\usepackage{xr} %% xr is for referring to external (TeX) documents.
\usepackage{xr-hyper} %% for interaction with hyperref package


\usepackage{bold-extra} %% bold-typewriter. USE IT, otherwise \ktime{}
                        %% labels are hardly readable

\newif\ifletter
%\lettertrue
\letterfalse

\ifletter
%% Letter Option
\setlength{\topmargin}{1.0cm}
\setlength{\textheight}{22.7cm}
\setlength{\textwidth}{16.4cm}
\setlength{\oddsidemargin}{0cm}
\setlength{\evensidemargin}{0cm}
\setlength{\floatsep}{2mm}
\setlength{\textfloatsep}{2.5mm}
\setlength{\intextsep}{2.5mm}
\else
%% Din A4 Option
\usepackage{a4wide}
%\setlength{\topmargin}{-1.0cm}
%\setlength{\textheight}{24cm}
%\setlength{\textwidth}{15.5cm}
%\setlength{\oddsidemargin}{0cm}
%\setlength{\evensidemargin}{0cm}
\fi
\newlength{\MPwidth}
\setlength{\MPwidth}{0.8\textwidth}

\renewcommand{\textfraction}{0.05}
\renewcommand{\topfraction}{0.95}
\renewcommand{\bottomfraction}{0.95}
\renewcommand{\floatpagefraction}{1}

%\dump
%\newif\ifpdf
%\ifx\pdfoutput\undefined
%  \pdffalse   % not PDFLaTeX
%\else
%  \pdfoutput=1 % PDFLaTeX
%  \pdftrue
%\fi

\ifpdf
  \usepackage{graphicx}
  \DeclareGraphicsExtensions{.jpg,.pdf,.png}
  \usepackage{tocbibind}
  \usepackage[colorlinks=true,bookmarks=true,linkcolor=blue,plainpages=false]{hyperref}
\else
  \usepackage[nottoc]{tocbibind}
  \usepackage{graphicx}
  \usepackage{color}
  \DeclareGraphicsExtensions{.eps}
  \usepackage[colorlinks=true,bookmarks=true,linkcolor=blue,plainpages=false]{hyperref}
\fi
\graphicspath{{./EPS/}{./Logo/}}

% Settings for the depth of subsub...section numbering
\setcounter{secnumdepth}{3}  % Only to \subsubsections!
\setcounter{tocdepth}{2}      % Only to \subsections!

\frenchspacing
\sloppy

% Index definitions
\newindex[Index]{idx} % General index
\newindex[List of data types]{ddx}
\newindex[List of symbolic constants]{cdx}
\newindex[List of functions]{fdx}
\newindex[List of macros]{mdx}

\pagestyle{headings}
\input defs.tex

\providecommand{\lstname}{}
\lstset{language=C,caption={[\lstname]},breaklines=true,breakautoindent=true,basicstyle=\footnotesize,breakatwhitespace=true}
\renewcommand{\lstlistlistingname}{Source Code Listings}
\renewcommand{\lstlistingname}{Source Code Listing}

%% For external references to the book. Note: that optional argument
%% ``book'' means, that any references to the ``book''-part need to be
%% prefixed by ``book:'', like \secref{book-S:introduction} or so.
\externaldocument[book:]{alberta-book} 

%\includeonly{intro,concept,examples,impl,tools}
%\includeonly{tools}

\begin{document}

%% title-page
\frontmatter

%\title{\ALBERTA: An adaptive hierarchical finite element toolbox}
\title{\ALBERTA 3.0: Technical Manual}

%\author{Alfred Schmidt and Kunibert G.~Siebert\\[0.2cm]%
%Institut f\"ur Angewandte Mathematik\strut\\%
%Universit\"at Freiburg i.~Br.\strut\\%
%{\small\tt alfred, kunibert@mathematik.uni-freiburg.de}}

\author{Alfred Schmidt and Kunibert G.\ Siebert and Daniel K\"oster
  and Claus-Justus Heine}

\vspace*{10mm}
\thispagestyle{empty}

\begin{center}
\LARGE 
%\ALBERTA: An adaptive hierarchical finite element toolbox\\[5mm]
\ALBERTA 3.0: Technical Manual
\vfil\vfil

\small
\begin{minipage}{0.4\textwidth}
\textbf{Alfred Schmidt}\\[1mm]
Zentrum fuer Technomathematik\strut\\
%Fachbereich 3 Mathematik/Informatik\strut\\
Universit\"at Bremen\strut\\
Bibliothekstr. 2\\
D-28359 Bremen, Germany
\\[5mm]
\textbf{Daniel K\"oster}\\[1mm]
P+Z Engineering GmbH\strut\\%
(formerly Universit\"at Augsburg)\strut\\%
\strut\\
\end{minipage}\hfill
\begin{minipage}{0.4\textwidth}
\textbf{Kunibert G.~Siebert}\\[1mm]
Fachbereich Mathematik\strut\\%
Universit\"at Stuttgart\strut\\%
Pfaffenwaldring 57\strut\\%
70569 Stuttgart, Germany
\\[5mm]
\textbf{Claus-Justus Heine}\\[1mm]
Fachbereich Mathematik\strut\\%
Universit\"at Stuttgart\strut\\%
Pfaffenwaldring 57\strut\\%
70569 Stuttgart, Germany
\end{minipage}

\vspace{5mm}
{\normalsize\url{http://www.alberta-fem.de}}
\vfil
\includegraphics[scale=1.0]{alberta}
\vfil

\ALBERTA is an \textsf{A}daptive multi-\textsf{L}evel finite element
toolbox using \textsf{B}i\-sec\-tion\-ing refinement and \textsf{E}rror
control by \textsf{R}esidual \textsf{T}echniques for scientific
\textsf{A}pplications. 
\vfil

Version: \ALBERTA-3.0, \today\strut
\end{center}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\cleardoublepage
%\pagenumbering{roman}
%\input{preface}
%\end{document}

\input{preface-man}

\newpage
\tableofcontents       % Inhaltsverzeichnis
\listoffigures
\listoftables
\newpage

\cleardoublepage
%\input{intro}          % Titel, Abstrakt, Einleitung
\cleardoublepage
\mainmatter

%\pagenumbering{arabic}

%%%%%%%%%%% THIS IS NOW SEPARATED FROM THE TECHNICAL MANUAL %%%%%%%%%%%%%%%%%%
%\include{concept}        % Ch. 1 Concepts and abstract algorithms
%\cleardoublepage
\setcounter{chapter}{1}

\include{examples}       % Ch. 2 Implementation of model problems
\cleardoublepage
\include{impl}           % Ch. 3 Data structures and implementation
\cleardoublepage
\include{tools}          % Ch. 4 Tools for finite element calculations

%  Literaturverzeichnis   %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\backmatter

\cleardoublepage
%addcontentsline{toc}{chapter}{\bibname}%{Bibliography}
\bibliographystyle{siam}
\bibliography{alberta}  % Literaturverzeichnis
%% Index                  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\cleardoublepage
%addcontentsline{toc}{chapter}{\indexname}%{Index}
%\lstlistoflistings
%\addcontentsline{toc}{chapter}{List of \lstlistlistingname}
\printindex[idx]
\cleardoublepage
\input{struct-func}    % List of data structures and functions
\end{document}
