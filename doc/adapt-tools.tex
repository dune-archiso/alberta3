\section{Data structures and procedures for adaptive methods}%
\label{S:adapt_data}%
\idx{adaptive methods!implementation|(}

\subsection{\ALBERTA adaptive method for stationary problems}%
\label{S:adapt_stat_in_ALBERTA}
          
The basic data structure \code{ADAPT\_STAT} for stationary adaptive
methods contains pointers to problem dependent routines to build the
linear or nonlinear system(s) of equations on an adapted
mesh, and to a routine which solves the discrete problem and computes
the new discrete solution(s).  For flexibility and efficiency reasons,
building and solution of the system(s) may be split into several
parts, which are called at various stages of the mesh adaption
process.

\code{ADAPT\_STAT} also holds parameters used for the adaptive procedure.
Some of the parameters are optional or used only when a special
marking strategy is selected.
%
\ddx{ADAPT_STAT@\code{ADAPT\_STAT}}
\idx{adaptive methods!ADAPT_STAT@{\code{ADAPT\_STAT}}}
\bv\begin{verbatim}
typedef struct adapt_stat       ADAPT_STAT;

struct adapt_stat
{
  const char  *name;
  REAL        tolerance;
  REAL        p;                         /* power in estimator norm        */
  int         max_iteration;
  int         info;

  REAL   (*estimate)(MESH *mesh, ADAPT_STAT *adapt);
  REAL   (*get_el_est)(EL *el);          /* local error indicator          */
  REAL   (*get_el_estc)(EL *el);         /* local coarsening error         */
  U_CHAR (*marking)(MESH *mesh, ADAPT_STAT *adapt);

  void   *est_info;                      /* estimator parameters           */
  REAL   err_sum, err_max;               /* sum and max of el_est          */

  void   (*build_before_refine)(MESH *mesh, U_CHAR flag);
  void   (*build_before_coarsen)(MESH *mesh, U_CHAR flag);
  void   (*build_after_coarsen)(MESH *mesh, U_CHAR flag);
  void   (*solve)(MESH *mesh);

  int    refine_bisections;
  int    coarsen_allowed;                /* 0 : 1                          */
  int    coarse_bisections;

  int    strategy;                       /* 1=GR, 2=MS, 3=ES, 4=GERS       */
  REAL   MS_gamma, MS_gamma_c;           /*  maximum strategy              */
  REAL   ES_theta, ES_theta_c;           /*  equidistribution strategy     */
  REAL   GERS_theta_star, GERS_nu, GERS_theta_c;  /* GERS strategy         */
};
\end{verbatim}\ev
The entries yield following information:
\begin{descr}
\kitem{name} textual description of the adaptive method, or \nil.
\kitem{tolerance} given tolerance for the (absolute or relative) error.
\kitem{p}    power $p$ used in estimate (\ref{book:E:adapt.p}),
             $1\leq p < \infty$.
\kitem{max\_iteration} maximal allowed number of iterations of the adaptive
             procedure; if \code{max\_iteration <= 0}, no iteration bound
             is used.
\kitem{info} level of information printed during the adaptive procedure;
             if \code{info >= 2}, the iteration count and final error
             estimate are printed; if \code{info >= 4}, then
             information is printed after each iteration of the
             adaptive procedure; if \code{info >= 6}, additional
             information about the CPU time used for mesh adaption and
             building the linear systems is printed.
%
\fdx{estimate()@\code{estimate()}}
\idx{adaptive methods!estimate()@{\code{estimate()}}}

\kitem{estimate} pointer to a problem dependent function for computing
     the global error estimate and the local error indicators; must 
     not be \nil;
     
     \code{estimate(mesh, adapt)} computes the error estimate
     and fills the entries \code{adapt->err\_sum} and
     \code{adapt->err\_max} with
\[
 \code{adapt->err\_sum} = 
\Big(\sum_{S \in \tria_h} \eta_S(u_h)^p \Big)^{1/p},
 \quad
 \code{adapt->err\_max} = \max_{S \in \tria_h} \eta_S(u_h)^p.
\]
The return value is the total error estimate \code{adapt->err\_sum}.
User data, like additional parameters for \code{estimate()}, can
be passed via the \code{est\_info} entry of the \code{ADAPT\_STAT} 
structure to a (problem dependent)
parameter structure. Usually, \code{estimate()} stores the local
error indicator(s) $\eta_S(u_h)^p$ (and coarsening error indicator(s)
$\eta_{c,S}(u_h)^p$) in \code{LEAF\_DATA(el)}.

For sample implementations of error estimators for quasi-linear
elliptic and parabolic problems, see Section \ref{S:estimator}.
%
\fdx{get_el_est()@{\code{get\_el\_est()}}}
\idx{adaptive methods!get_el_est()@{\code{get\_el\_est()}}}
\kitem{get\_el\_est} pointer to a problem dependent subroutine returning
             the value of the local error indicator; must not be \nil if via
             the entry \code{strategy} adaptive refinement is selected
             and the specialized marking routine \code{marking} is \nil;
             
             \code{get\_el\_est(el)} returns the value
             $\eta_S(u_h)^p$, of the local error indicator on leaf
             element \code{el}; usually, local error indicators are
             computed by \code{estimate()} and 
             stored in \code{LEAF\_DATA(el)}, which is problem
             dependent and thus not directly accessible by
             general--purpose routines.  \code{get\_el\_est()} is
             needed by the \ALBERTA marking strategies.
%
\fdx{get_el_estc()@{\code{get\_el\_estc()}}}
\idx{adaptive methods!get_el_estc()@{\code{get\_el\_estc()}}}
\kitem{get\_el\_estc} pointer to a function which returns the local
             coarsening error indicator;
             
             \code{get\_el\_estc(el)} returns the value
             $\eta_{c,S}(u_h)^p$ of the local coarsening error
             indicator on leaf element \code{el}, usually computed
             by \code{estimate()} and stored in
             \code{LEAF\_DATA(el)}; if not \nil,
             \code{get\_el\_estc()} is called by the \ALBERTA marking
             routines; this pointer may be \nil, which means
             $\eta_{c,S}(u_h)=0$.

\kitem{marking} specialized marking strategy; if \nil, a standard 
      \ALBERTA marking routine is selected via the entry \code{strategy};

   \code{marking(mesh, adapt)} selects and marks elements 
   for refinement or coarsening; the return value is
\cdx{MESH_REFINED@\code{MESH\_REFINED}}
\cdx{MESH_COARSENED@\code{MESH\_COARSENED}}
  \begin{descr}
  \kitem{0} no element is marked;
  \kitem{MESH\_REFINED} elements are marked but only for refinement;
  \kitem{MESH\_COARSENED} elements are marked but only for coarsening;
  \kitem{MESH\_REFINED|MESH\_COARSENED} elements are marked for
    refinement and coarsening.
  \end{descr}
  
\kitem{est\_info} pointer to (problem dependent) parameters
        for the \code{estimate()} routine; via this pointer the user
        can pass information to the estimate routine; this pointer may
        be \nil.  

\kitem{err\_sum} variable to hold the sum of local
        error indicators $(\sum_{S\in\tria} \eta_S(u_h)^p)^{1/p}$; the
        value for this entry must be set by the function
        \code{estimate()}.  

\kitem{err\_max} variable to hold the
        maximal local error indicators $\max_{S\in\tria}
        \eta_S(u_h)^p$; the value for this entry must be set by the
        function \code{estimate()}.

\kitem{build\_before\_refine} pointer to a subroutine that 
        builds parts of the (non-)linear system(s) before any
        mesh adaptation; 
        if it is \nil, this assemblage stage omitted;

      \code{build\_before\_refine(mesh, flag)} launches the assembling
      of the assembling of the discrete system on \code{mesh}; \code{flag}
      gives information which part of the system has to be built;
      the 
      mesh will be refined if the \code{MESH\_REFINED} bit is set in
      \code{flag} and it will be coarsened if the bit \code{MESH\_COARSENED}
      is set in \code{flag}.
      
\kitem{build\_before\_coarsen} pointer to a subroutine that builds parts 
       of the (non-)linear system(s) between the refinement and coarsening;
       if it is \nil, this assemblage stage omitted;
        
       \code{build\_before\_coarsen(mesh, flag)} performs an
       intermediate assembling step on \code{mesh} (compare
       \secref{book:S:inter_restrict} for an example when such a step is
       needed); \code{flag} gives information which part of the system
       has to be built; the mesh was refined if the
       \code{MESH\_REFINED} bit is set in \code{flag} and it will be
       coarsened if the bit \code{MESH\_COARSENED} is set in
       \code{flag}.

\kitem{build\_after\_coarsen} pointer to a
        subroutine that builds parts of the (non-)linear system(s)
        after all mesh adaptation;
        if it is \nil, this assemblage stage omitted;
        
        \code{build\_before\_coarsen(mesh, flag)} performs the final
        assembling step on \code{mesh}; \code{flag} gives information
        which part of the system has to be built; the mesh was refined
        if the \code{MESH\_REFINED} bit is set in \code{flag} and it
        was coarsened if the bit \code{MESH\_COARSENED} is set in
        \code{flag}.

\kitem{solve} pointer to a subroutine for solving the discrete
      (non-)linear system(s); if it is \nil, the solution step is omitted;

      \code{solve(mesh)} computes the new discrete solution(s) on 
       \code{mesh}.

\kitem{refine\_bisections}  number of bisection steps for the refinement
        of an element marked for refinement; 
        used by the \ALBERTA marking strategies;
        default value is $d$.

\kitem{coarsen\_allowed} flag used by the \ALBERTA marking strategies to
       allow (\code{true}) or forbid (\code{false}) mesh coarsening;

\kitem{coarse\_bisections} number of bisection steps for the coarsening
        of an element marked for coarsening; 
        used by the \ALBERTA marking strategies;
        default value is $d$.

\kitem{strategy} parameter to select an \ALBERTA marking routine;
        possible values are:
  \begin{descr}
  \kitem{0} no mesh adaption,
  \kitem{1} global refinement (GR),
  \kitem{2} maximum strategy (MS), 
  \kitem{3} equidistribution strategy (ES),
  \kitem{4} guaranteed error reduction strategy (GERS),
  \end{descr}
             see Section \ref{S:ALBERTA_marking}.
\kitem{MS\_gamma, MS\_gamma\_c} parameters for the marking 
             \emph{maximum strategy}, see Sections
             \ref{book:S:refinement_strategies} and \ref{book:S:coarsening_strategies}.
\kitem{ES\_theta, ES\_theta\_c} parameters for the marking
             \emph{equidistribution strategy}, see Sections
             \ref{book:S:refinement_strategies} and \ref{book:S:coarsening_strategies}.
\kitem{GERS\_theta\_star, GERS\_nu, GERS\_theta\_c} parameters for the marking
             \emph{guaranteed error reduction strategy}, see Sections
             \ref{book:S:refinement_strategies} and \ref{book:S:coarsening_strategies}.
\end{descr}

The routine \code{adapt\_method\_stat()} implements the whole
adaptive procedure for a stationary problem, using the parameters
given in \code{ADAPT\_STAT}:
%
\fdx{adapt_method_stat()@{\code{adapt\_method\_stat()}}}
\idx{adaptive methods!adapt_method_stat@{\code{adapt\_method\_stat()}}}
\bv\begin{verbatim}
void adapt_method_stat(MESH *, ADAPT_STAT *);
\end{verbatim}\ev
%
Description:
\begin{descr}
\kitem{adapt\_method\_stat(mesh, adapt\_stat)} solves adaptively a
stationary problem on \code{mesh} by the
adaptive procedure described in Section \ref{book:S:adapt_stat_prob}; 
\code{adapt\_stat} is a pointer to a filled
\code{ADAPT\_STAT} data structure, holding all
information about the problem to be solved and parameters for
the adaptive method.
\end{descr}
%
The main loop of the adaptive method is given in the following 
source fragment:
%
\bv\begin{verbatim}
void adapt_method_stat(MESH *mesh, ADAPT_STAT *adapt)
{
  int      iter;
  REAL     est;

  ...

  /* get solution on initial mesh */
  if (adapt->build_before_refine)  adapt->build_before_refine(mesh, 0);
  if (adapt->build_before_coarsen) adapt->build_before_coarsen(mesh, 0);
  if (adapt->build_after_coarsen)  adapt->build_after_coarsen(mesh, 0);
  if (adapt->solve) adapt->solve(mesh);
  est = adapt->estimate(mesh, adapt);

  for (iter = 0;
       (est > adapt->tolerance) &&
         ((adapt->max_iteration <= 0) || (iter < adapt->max_iteration));
       iter++)
  {
    if (adapt_mesh(mesh, adapt))
    {
      if (adapt->solve) adapt->solve(mesh);
      est = adapt->estimate(mesh, adapt);
    }

    ...
  }
}
\end{verbatim}\ev
The actual mesh adaption is done in a subroutine
\code{adapt\_mesh()}, which combines marking, refinement, coarsening
and the linear system building routines:
\cdx{MESH_REFINED@\code{MESH\_REFINED}}
\cdx{MESH_COARSENED@\code{MESH\_COARSENED}}
\fdx{adapt_mesh()@\code{adapt\_mesh()}}
\idx{adaptive methods!adapt_mesh()@{\code{adapt\_mesh()}}}
\bv\begin{verbatim}
static U_CHAR adapt_mesh(MESH *mesh, ADAPT_STAT *adapt)
{
  U_CHAR   flag = 0;
  U_CHAR   mark_flag;

  ...

  if (adapt->marking)
    mark_flag = adapt->marking(mesh, adapt);
  else
    mark_flag = marking(mesh, adapt);             /* use standard marking() */

  if (!adapt->coarsen_allowed)
    mark_flag &= MESH_REFINED;                    /* use refine mark only   */

  if (adapt->build_before_refine)  adapt->build_before_refine(mesh, mark_flag);

  if (mark_flag & MESH_REFINED)    flag = refine(mesh);

  if (adapt->build_before_coarsen) adapt->build_before_coarsen(mesh, mark_flag);

  if (mark_flag & MESH_COARSENED)  flag |= coarsen(mesh);

  if (adapt->build_after_coarsen)  adapt->build_after_coarsen(mesh, flag);

  ...
  return(flag);
}
\end{verbatim}\ev

\begin{remark}
  As the same procedure is used for time dependent problems in single
  time steps, different pointers to routines for building parts of the
  (non-)linear systems make it possible, for example, to assemble the
  right hand side including a functional involving the solution from
  the old time step {\em before} coarsening the mesh, and then using
  the \code{DOF\_VEC} restriction during coarsening to compute exactly
  the projection to the coarsened finite element space, without
  losing any information, compare \secref{book:S:inter_restrict}.
\end{remark}

\begin{remark}
For time dependent problems, the system matrices usually depend on the
current time step size. Thus, matrices may have to be rebuilt even if
meshes are not changed, but when the time step size was changed. Such
changes can be detected in the \verb|set_time()| routine, for example.
\end{remark}


\subsection{Standard \ALBERTA marking routine}\label{S:ALBERTA_marking}

When the \code{marking} procedure pointer in the \code{ADAPT\_STAT}
structure is \nil, then the standard \ALBERTA
marking\idx{marking} routine is called. The \code{strategy}
entry, allows the selection of one of five different marking
strategies (compare Sections~\ref{book:S:refinement_strategies} and
\ref{book:S:coarsening_strategies}).  Elements are only marked for
coarsening and coarsening parameters are only used if the entry
\code{coarsen\_allowed} is \code{true}.  The number of bisection steps
for refinement and coarsening is selected by the entries
\code{refine\_bisections} and \code{coarse\_bisections}.
%
\idx{ALBERTA marking strategies@\ALBERTA marking strategies}
\idx{adaptive methods!ALBERTA marking strategies@\ALBERTA marking strategies}
\begin{description}
\item[\code{strategy=0}:] no refinement or coarsening is performed;
\item[\code{strategy=1}:] Global Refinement (GR):\\
         the mesh is refined globally, no coarsening is performed;
\item[\code{strategy=2}:] Maximum Strategy (MS):\\
         the entries \code{MS\_gamma}, \code{MS\_gamma\_c} are used
         as refinement and coarsening parameters;
\item[\code{strategy=3}:] Equidistribution strategy (ES):\\
         the entries \code{ES\_theta}, \code{ES\_theta\_c} are used
         as refinement and coarsening parameters; 
\item[\code{strategy=4}:] Guaranteed error reduction strategy (GERS):\\
         the entries \code{GERS\_theta\_star}, \code{GERS\_nu}, and
         \code{GERS\_theta\_c} are used as refinement and coarsening
         parameters.
\end{description}

\begin{remark}
As \code{get\_el\_est()} and \code{get\_el\_estc()} return the $p$--th
power of the local estimates, all algorithms are implemented to use
the values $\eta_S^p$ instead of $\eta_S$. This results in a small
change to the coarsening tolerances for the equidistribution 
strategy described in \secref{book:S:coarsening_strategies}. 
The implemented equidistribution strategy uses the inequality
\[
\eta_S^p + \eta_{c,S}^p \leq c^p \, \tol^p / N_k
\]
instead of 
\[
\eta_S + \eta_{c,S} \leq c \, \tol / N_k^{1/p}.
\]
\end{remark}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{\ALBERTA adaptive method for time dependent problems}
\label{S:adapt_instat_in_ALBERTA}

Similar to the data structure \code{ADAPT\_STAT} for collecting
information about the adaptive solution for a stationary problem, the
data structure \code{ADAPT\_INSTAT} is used for gather all information
needed for the time and space adaptive solution of instationary
problems. Using a time stepping scheme, in each time step a stationary
problem is solved; the adaptive method for this is based
on the \code{adapt\_method\_stat()} routine described in Section
\ref{S:adapt_stat_in_ALBERTA}, the \code{ADAPT\_INSTAT} structure
includes two \code{ADAPT\_STAT} parameter structures. Additional
entries give information about the time adaptive procedure.
\goodbreak

\ddx{ADAPT_INSTAT@\code{ADAPT\_INSTAT}}
\idx{adaptive methods!ADAPT_INSTAT@{\code{ADAPT\_INSTAT}}}
\bv\begin{verbatim}
typedef struct adapt_instat     ADAPT_INSTAT;
struct adapt_instat
{
  const char  *name;

  ADAPT_STAT adapt_initial[1];
  ADAPT_STAT adapt_space[1];

  REAL   time;
  REAL   start_time, end_time;
  REAL   timestep;

  void   (*init_timestep)(MESH *, ADAPT_INSTAT *);
  void   (*set_time)(MESH *, ADAPT_INSTAT *);
  void   (*one_timestep)(MESH *, ADAPT_INSTAT *);
  REAL   (*get_time_est)(MESH *, ADAPT_INSTAT *);
  void   (*close_timestep)(MESH *, ADAPT_INSTAT *);

  int    strategy; 
  int    max_iteration;

  REAL   tolerance;
  REAL   rel_initial_error;
  REAL   rel_space_error;
  REAL   rel_time_error;
  REAL   time_theta_1;
  REAL   time_theta_2;
  REAL   time_delta_1;
  REAL   time_delta_2;
  int    info;
};
\end{verbatim}\ev
The entries yield following information:
\begin{descr}
\kitem{name} textual description of the adaptive method, or \nil.
\kitem{adapt\_initial} mesh adaption parameters for the initial mesh,
                       compare Section \ref{S:adapt_stat_in_ALBERTA}.
\kitem{adapt\_space}   mesh adaption parameters during time steps,
                       compare Section \ref{S:adapt_stat_in_ALBERTA}.
\kitem{time}  actual time, end of time interval for current time step.
\kitem{start\_time}    initial time for the adaptive simulation.
\kitem{end\_time}      final time for the adaptive simulation.
\kitem{timestep}       current time step size, will be changed by the time
                       adaptive method.
\kitem{init\_timestep} pointer to a routine called at beginning 
       of each time step; if \nil, initialization of a new time step 
       is omitted;

       \code{init\_timestep(mesh, adapt)} initializes a new
       time step; 
\kitem{set\_time}  pointer to a routine called after changes of the
       time step size if not \nil;

       \code{set\_time(mesh, adapt)} is called by the adaptive
       method each time when the actual time \code{adapt->time}
       has changed, i.\,e. at a new time step and after a change of the 
       time step size \code{adapt->timestep}; information about 
       actual time and time step size is available via \code{adapt}.

\kitem{one\_timestep}  pointer to a routine which implements one
      (adaptive) time step, if \nil, a default routine is called;

       \code{one\_timestep(mesh, adapt)} implements the (adaptive)
       solution of the problem in one single time step; information
       about the stationary problem of the time step is available in
       the \code{adapt->adapt\_space} data structure.
      
\kitem{get\_time\_est} pointer to a routine returning an estimate
       for the time error; if \nil, no time step adaptation is
       done;

       \code{get\_time\_est(mesh, adapt)} returns an estimate
       $\eta_\tau$ for the current time error at time \code{adapt->time}
       on \code{mesh}.

\kitem{close\_timestep} pointer to a routine called after finishing
       a time step, may be \nil.
       
       \code{close\_timestep(mesh, adapt)} is called after accepting
       the solution(s) of the discrete problem on \code{mesh} at time
       \code{adapt->time} by the time--space adaptive method; can be
       used for visualization and export to file for post--processing
       of the \code{mesh} and discrete solution(s).

\kitem{strategy} parameter for the default \ALBERTA
    \code{one\_timestep} routine; possible values are:
  \begin{descr}
  \kitem{0} explicit strategy,
  \kitem{1} implicit strategy.
\end{descr}

\kitem{max\_iteration} parameter for the default \code{one\_timestep} routine;
  maximal number of time step size adaptation steps, only used by the
  implicit strategy.

\kitem{tolerance}  given total error tolerance $\tol$.

\kitem{rel\_initial\_error} portion $\Gamma_0$ of tolerance allowed for
     initial error, compare \secref{book:S:time_adaptive_strategies}; 
\kitem{rel\_space\_error} portion $\Gamma_h$ of tolerance allowed for
    error from spatial discretization in each time step, 
    compare \secref{book:S:time_adaptive_strategies}.
\kitem{rel\_time\_error}  portion $\Gamma_\tau$ of tolerance 
    allowed for error from time discretization in each time
    step, compare \secref{book:S:time_adaptive_strategies}.

\kitem{time\_theta\_1} safety parameter $\theta_1$ for the 
 time adaptive method in the default \ALBERTA \code{one\_timestep()} routine;
 the tolerance for the time estimate $\eta_\tau$ is
 $\theta_1\,\Gamma_\tau\,\tol$,
 compare Algorithm \ref{book:A:Alg.timestepsize}.
 
\kitem{time\_theta\_2} safety parameter $\theta_2$ for the time
 adaptive method in the default \ALBERTA \code{one\_timestep()} routine;
 enlargement of the time step size is only allowed for $\eta_\tau \leq
 \theta_2\,\Gamma_\tau\,\tol$, compare Algorithm
 \ref{book:A:Alg.timestepsize}.

\kitem{time\_delta\_1} factor $\delta_1$ used for the reduction of
 the time step size in the default \ALBERTA \code{one\_timestep()} routine, 
 compare Algorithm \ref{book:A:Alg.timestepsize}.
\kitem{time\_delta\_2} factor $\delta_2$ used for the enlargement
 of the time step size in the default \ALBERTA \code{one\_timestep()} routine, 
 compare Algorithm \ref{book:A:Alg.timestepsize}.

\kitem{info} level of information produced by the time--space adaptive
 procedure.
\end{descr}
%
Using information given in the \code{ADAPT\_INSTAT} data structure,
the space and time adaptive procedure is performed by:
\fdx{adapt_method_instat()@\code{adapt\_method\_instat()}}\
\idx{adaptive methods!adapt_method_instat()@{\code{adapt\_method\_instat()}}}
\bv\begin{verbatim}
void adapt_method_instat(MESH *, ADAPT_INSTAT *);
\end{verbatim}\ev
Description:
\begin{descr}
  \kitem{adapt\_method\_instat(mesh, adapt\_instat)} solves an
  instationary problem on \code{mesh} by the space--time adaptive
  procedure described in \secref{book:S:time_adaptive_strategies};
  \code{adapt\_instat} is a pointer to a filled \code{ADAPT\_INSTAT}
  data structure, holding all information about the problem to be
  solved and parameters for the adaptive method.
\end{descr}

Implementation of the routine is very simple. All essential work is
done by calling \code{adapt\_method\_stat()} for the generation of the
initial mesh, based on parameters given in
\code{adapt->adapt\_initial} with tolerance
\code{adapt->tolerance*adapt->rel\_space\_error}, and in
\code{one\_timestep()} which solves the discrete problem and does mesh
adaption and time step adjustment for one single time step.

\fdx{adapt_method_instat()@\code{adapt\_method\_instat()}}
\idx{adaptive methods!adapt_method_instat()@{\code{adapt\_method\_instat()}}}
\bv\begin{verbatim}
void adapt_method_instat(MESH *mesh, ADAPT_INSTAT *adapt)
{
/*------------------------------------------------------------------------*/
/*  adaptation of the initial grid: done by adapt_method_stat()           */
/*------------------------------------------------------------------------*/

  adapt->time = adapt->start_time;
  if (adapt->set_time) adapt->set_time(mesh, adapt);

  adapt->adapt_initial->tolerance
     = adapt->tolerance * adapt->rel_initial_error;
  adapt->adapt_space->tolerance
     = adapt->tolerance * adapt->rel_space_error;
  adapt_method_stat(mesh, adapt->adapt_initial);

  if (adapt->close_timestep)
    adapt->close_timestep(mesh, adapt);

/*------------------------------------------------------------------------*/
/*  adaptation of timestepsize and mesh: done by one_timestep()           */
/*------------------------------------------------------------------------*/

  while (adapt->time < adapt->end_time)
  {
    if (adapt->init_timestep)
      adapt->init_timestep(mesh, adapt);

    if (adapt->one_timestep)
      adapt->one_timestep(mesh, adapt);
    else
      one_timestep(mesh, adapt);

    if (adapt->close_timestep)
      adapt->close_timestep(mesh, adapt);
  }
}
\end{verbatim}\ev

\subsubsection{The default \ALBERTA \code{one\_timestep()} routine}

\fdx{one_timestep()@{\code{one\_timestep()}}}
\idx{adaptive methods!one_timestep()@{\code{one\_timestep()}}}
The default \code{one\_timestep()} routine provided by \ALBERTA
implements both the explicit strategy and the implicit time strategy
A. The semi--implicit strategy described in Section
\ref{book:S:time_adaptive_strategies} is only a special case of the
implicit strategy with a limited number of iterations (exactly one).

The routine uses the parameter \code{adapt->strategy} to select
the strategy:
\begin{center}
\code{strategy 0}: ~Explicit strategy, \qquad
\code{strategy 1}: ~Implicit strategy.
\end{center}

\paragraph{Explicit strategy.}
The explicit strategy does one adaption of the mesh based on the error
estimate computed from the last time step's discrete solution by
using parameters given in
\code{adapt->adapt\_space}, with \code{tolerance} set to
\code{adapt->tolerance*adapt->rel\_space\_error}. Then the current time 
step's discrete problem is solved, and the error estimators are
computed. No time step size adjustment is done.

\paragraph{Implicit strategy.}
The implicit strategy starts with the old mesh from last time step.
Using parameters given in \code{adapt->adapt\_space}, the discrete
problem is solved on the current mesh. Error estimates are computed,
and time step size and mesh are adjusted, as shown in 
\algorithmref{book:A:time-space-algo}, with tolerances given by
\code{adapt->tolerance*adapt->rel\_time\_error} and
\code{adapt->tolerance*adapt->rel\_space\_error}, respectively. 
This is iterated until the given error bounds are reached, or until
\code{adapt->max\_iteration} is reached. 

With parameter \code{adapt->max\_iteration==0}, this is equivalent to the
semi--implicit strategy described in \secref{book:S:time_adaptive_strategies}.


\subsection{Initialization of data structures for adaptive methods}%
\label{S:get_adapt}%

\ALBERTA provides functions for the initialization of the data structures
\code{ADAPT\_STAT} and  \code{ADAPT\_INSTAT}. Both functions do
\emph{not} fill any function pointer entry in the structures! 
These function pointers have to be adjusted in the application.
%
\begin{table}[htbp]
\begin{center}\begin{small}
\begin{tabular}{|l|c|l|}\hline
member & default & parameter key\\\hline\hline
\code{tolerance}          & 1.0  & \code{prefix->tolerance}\\
\code{p}                  & 2    & \code{prefix->p}\\
\code{max\_iteration}     & 30   & \code{prefix->max\_iteration}\\
\code{info}               & 2    & \code{prefix->info}\\
\code{refine\_bisections} & $d$  & \code{prefix->refine\_bisections}\\
\code{coarsen\_allowed}   & 0    & \code{prefix->coarsen\_allowed}\\
\code{coarse\_bisections} & $d$  & \code{prefix->coarse\_bisections}\\
\code{strategy}           & 1  & \code{prefix->strategy}\\
\code{MS\_gamma}          & 0.5  & \code{prefix->MS\_gamma}\\
\code{MS\_gamma\_c}       & 0.1  & \code{prefix->MS\_gamma\_c}\\
\code{ES\_theta}          & 0.9  & \code{prefix->ES\_theta}\\
\code{ES\_theta\_c}       & 0.2  & \code{prefix->ES\_theta\_c}\\
\code{GERS\_theta\_star}  & 0.6  & \code{prefix->GERS\_theta\_star}\\
\code{GERS\_nu}           & 0.1  & \code{prefix->GERS\_nu}\\
\code{GERS\_theta\_c}     & 0.1  & \code{prefix->GERS\_theta\_c}\\\hline
\end{tabular}
\end{small}\end{center}
\caption[\code{ADAPT\_STAT} structure, default initialization]{Initialized members of an \code{ADAPT\_STAT} structure,
  the default values and the key for the initialization by
  \code{GET\_PARAMETER()}.}\label{T:adapt_stat}
\end{table}
%
\fdx{get_adapt_stat()@{\code{get\_adapt\_stat()}}}
\idx{adaptive methods!get_adapt_stat@{\code{get\_adapt\_stat()}}}
\fdx{get_adapt_instat()@{\code{get\_adapt\_instat()}}}
\idx{adaptive methods!get_adapt_instat@{\code{get\_adapt\_instat()}}}
\bv\begin{verbatim}
ADAPT_STAT *get_adapt_stat(const int, const char *, const char *,
                          int, ADAPT_STAT *);

ADAPT_INSTAT *get_adapt_instat(const int, const char *, const char *,
                               int, ADAPT_INSTAT *);
\end{verbatim}\ev
Description:
\begin{descr}
\kitem{get\_adapt\_stat(dim, name, prefix, info, adapt)} returns a pointer
       to a partly initialized \code{ADAPT\_STAT} structure; 
       if the argument \code{adapt} is \nil, a new structure is
       created, the name \code{name} is duplicated at the
       name entry of the structure, if \code{name} is not \nil;
       if \code{name} is \nil, and \code{prefix} is not \nil,
       this string is duplicated at the name entry; \code{dim} is the 
       mesh dimension $d$;
       for a newly created structure, all function pointers of the
       structure are initialized with \nil; all other members are
       initialized with some default value; if the argument
       \code{adapt} is not \nil, this initialization part is skipped,
       the name and function pointers are not changed;

       if \code{prefix} is not \nil, \code{get\_adapt\_stat()} tries
       then to initialize members by a call of
       \code{GET\_PARAMETER()}, where the key for each member is
       \code{value(prefix)->member name}; the argument \code{info} is
       the first argument of \code{GET\_PARAMETER()} giving the level
       of information for the initialization;

       only the parameters for the actually chosen strategy are initialized
       using the function \code{GET\_PARAMETER()}: for \code{strategy == 2}
       only \code{MS\_gamma} and \code{MS\_gamma\_c}, for 
       \code{strategy == 3} only \code{ES\_theta} and  \code{ES\_theta\_c},
       and for \code{strategy == 4} only \code{GERS\_theta\_star},
       \code{GERS\_nu}, and \code{GERS\_theta\_c};

       since the parameter tools are used for the initialization,
       \code{get\_adapt\_stat()} should be called \emph{after} the
       initialization of all parameters; there may be no initializer in
       the parameter file(s) for some member, if the default value
       should be used; if \code{info} is not zero and there is no
       initializer for some member this will result in an error
       message by \code{GET\_PARAMETER()} which can be ignored;

       Table \ref{T:adapt_stat} shows the initialized members,
       the default values and the key used for the initialization by
       \code{GET\_PARAMETER()};
\end{descr}
%
\begin{table}[hbtp]
\begin{center}\begin{small}
\begin{tabular}{|l|c|l|}\hline
member & default& parameter key\\\hline\hline
\code{start\_time} & 0.0  & \code{prefix->start\_time}\\
\code{end\_time}   & 1.0  & \code{prefix->end\_time}\\
\code{timestep}    & 0.01 & \code{prefix->timestep}\\
\code{strategy}    & 0    & \code{prefix->strategy}\\
\code{max\_iteration} &0  & \code{prefix->max\_iteration}\\
\code{tolerance}   &  1.0 & \code{prefix->tolerance}\\
\code{rel\_initial\_error} &0.1 & \code{prefix->rel\_initial\_error}\\
\code{rel\_space\_error} & 0.4 & \code{prefix->rel\_space\_error}\\
\code{rel\_time\_error} & 0.4 & \code{prefix->rel\_time\_error}\\
\code{time\_theta\_1} & 1.0 & \code{prefix->time\_theta\_1}\\
\code{time\_theta\_2} & 0.3 & \code{prefix->time\_theta\_2}\\
\code{time\_delta\_1} & 0.7071 & \code{prefix->time\_delta\_1}\\
\code{time\_delta\_2} & 1.4142& \code{prefix->time\_delta\_2}\\
\code{info} & 8 & \code{prefix->info}\\\hline
\end{tabular}
\end{small}
\caption[\code{ADAPT\_INSTAT}, default initialization]{Initialization
  of the main parameters in an \code{ADAPT\_INSTAT} structure for the
  time-adaptive strategy; initialized members, the default values and
  keys used for the initialization by \code{GET\_PARAMETER()}.}
\label{T:adapt_instat-a}
\end{center}
\end{table}
%
\begin{descr}
\kitem{get\_adapt\_instat(dim, name, prefix, info, adapt)} returns a pointer
       to a partly initialized \code{ADAPT\_INSTAT} structure; 
       if the argument \code{adapt} is \nil, a new structure is
       created, the name \code{name} is duplicated at the
       name entry of the structure, if \code{name} is not \nil;
       if \code{name} is \nil, and \code{prefix} is not \nil,
       this string is duplicated at the name entry; \code{dim} is the
       mesh dimension $d$;
       for a newly created structure, all function pointers of the
       structure are initialized with \nil; all other members are
       initialized with some default value; if the argument
       \code{adapt} is not \nil, this default initialization part is
       skipped;

       if \code{prefix} is not \nil, \code{get\_adapt\_instat()} tries
       then to initialize members by a call of
       \code{GET\_PARAMETER()}, where the key for each member is
       \code{value(prefix)->member name}; the argument \code{info} is
       the first argument of \code{GET\_PARAMETER()} giving the level
       of information for the initialization;
       
       Tables \ref{T:adapt_instat-a}--\ref{T:adapt_instat-c}
       shows the initialized members, the
       default values and the key used for the initialization by
       \code{GET\_PARAMETER()}. The tolerances in the sub-structures
       \code{adapt\_initial} and \code{adapt\_space} are set to the
       values \code{adapt->tolerance*adapt->rel\_initial\_error} and
       \code{adapt->tolerance*adapt->rel\_space\_error}, respectively.
       A special initialization is done for the \code{info}
       parameters: when \code{adapt\_initial->info} or
       \code{adapt\_space->info} are negative, then they are set to
       \code{adapt->info-2}.
\end{descr}

\begin{table}[hbtp]
\begin{center}\begin{small}
\begin{tabular}{|l|c|l|}\hline
member & default& parameter key\\\hline\hline
\code{adapt\_initial->tolerance}          & --  &  --\\
\code{adapt\_initial->p}                  & 2    &
  \code{prefix->initial->p}\\
\code{adapt\_initial->max\_iteration}     & 30   &
  \code{prefix->initial->max\_iteration}\\
\code{adapt\_initial->info}               & 2    &
  \code{prefix->initial->info}\\
\code{adapt\_initial->refine\_bisections} & $d$ &
  \code{prefix->initial->refine\_bisections}\\
\code{adapt\_initial->coarsen\_allowed}   & 0    &
  \code{prefix->initial->coarsen\_allowed}\\
\code{adapt\_initial->coarse\_bisections} & $d$ &
  \code{prefix->initial->coarse\_bisections}\\
\code{adapt\_initial->strategy}           & 1  &
  \code{prefix->initial->strategy}\\
\code{adapt\_initial->MS\_gamma}          & 0.5  &
  \code{prefix->initial->MS\_gamma}\\
\code{adapt\_initial->MS\_gamma\_c}       & 0.1  &
  \code{prefix->initial->MS\_gamma\_c}\\
\code{adapt\_initial->ES\_theta}          & 0.9  & 
  \code{prefix->initial->ES\_theta}\\
\code{adapt\_initial->ES\_theta\_c}       & 0.2  &
   \code{prefix->initial->ES\_theta\_c}\\
\code{adapt\_initial->GERS\_theta\_star}  & 0.6  &
   \code{prefix->initial->GERS\_theta\_star}\\
\code{adapt\_initial->GERS\_nu}           & 0.1  &
  \code{prefix->initial->GERS\_nu}\\
\code{adapt\_initial->GERS\_theta\_c}     & 0.1  &
  \code{prefix->initial->GERS\_theta\_c}\\\hline
\end{tabular}
\end{small}
\caption[\code{adapt\_initial} sub-structure of an
\code{ADAPT\_INSTAT}, default initialization]{Initialization of the
  \code{adapt\_initial} sub-structure of an \code{ADAPT\_INSTAT}
  structure for the adaptation of the initial grid; initialized
  members, the default values and keys used for the initialization by
  \code{GET\_PARAMETER()}.}
\label{T:adapt_instat-b}
\end{center}
\end{table}

\begin{table}[hbtp]
\begin{center}\begin{small}
\begin{tabular}{|l|c|l|}\hline
member & default& parameter key\\\hline\hline
\code{adapt\_space->tolerance}          & --  & --\\
\code{adapt\_space->p}                  & 2    &
  \code{prefix->space->p}\\
\code{adapt\_space->max\_iteration}     & 30   &
  \code{prefix->space->max\_iteration}\\
\code{adapt\_space->info}               & 2    &
  \code{prefix->space->info}\\
\code{adapt\_space->refine\_bisections} & $d$ &
  \code{prefix->space->refine\_bisections}\\
\code{adapt\_space->coarsen\_allowed}   & 1    &
 \code{prefix->space->coarsen\_allowed}\\
\code{adapt\_space->coarse\_bisections} & $d$ &
  \code{prefix->space->coarse\_bisections}\\
\code{adapt\_space->strategy}           & 1  &
  \code{prefix->space->strategy}\\
\code{adapt\_space->MS\_gamma}          & 0.5  &
  \code{prefix->space->MS\_gamma}\\
\code{adapt\_space->MS\_gamma\_c}       & 0.1  &
  \code{prefix->space->MS\_gamma\_c}\\
\code{adapt\_space->ES\_theta}          & 0.9  &
 \code{prefix->space->ES\_theta}\\
\code{adapt\_space->ES\_theta\_c}       & 0.2  &
 \code{prefix->space->ES\_theta\_c}\\
\code{adapt\_space->GERS\_theta\_star}  & 0.6  &
 \code{prefix->space->GERS\_theta\_star}\\
\code{adapt\_space->GERS\_nu}           & 0.1  &
 \code{prefix->space->GERS\_nu}\\
\code{adapt\_space->GERS\_theta\_c}     & 0.1  & 
 \code{prefix->space->GERS\_theta\_c}\\\hline
\end{tabular}
\end{small}
\caption[\code{adapt\_space} sub-structure of an \code{ADAPT\_INSTAT},
default initialization]{Initialization of the \code{adapt\_space}
  sub-structure of an \code{ADAPT\_INSTAT} structure for the
  adaptation of the grids during time-stepping; initialized members,
  the default values and keys used for the initialization by
  \code{GET\_PARAMETER()}.}
\label{T:adapt_instat-c}
\end{center}
\end{table}

\idx{adaptive methods!implementation|)}

%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "alberta-man"
%%% End: 
