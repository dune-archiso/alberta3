\section*{Preface to the Technical Manual for \ALBERTA-3.0}
\addcontentsline{toc}{chapter}{Preface}
\markboth{PREFACE}{PREFACE}

This is the ``Technical Manual'' for the finite-element toolbox
\ALBERTA, version 3, a reference manual which intentionally lists all
functions and data-structures exported to application programs. In
other words: this manual contains the definition of what commonly is
referred to as ``API'' -- Application Program Interface. After the
release of version 1.2 -- which was accompanied by publishing the
\ALBERTA-book \cite{ALBERTABOOK} through Springer (or vice-versa: the
book was accompanied by the release of version 1.2) -- there was
already a successor labelled \ALBERTA-2.0 (with slight bug-fixes in
version 2.0.1), see
\href{http://www.alberta-fem.de}{www.alberta-fem.de}.  Version 2.0 was
in its principal part the outcome of the labours of Daniel K\"oster.

Already at that time it was felt by the developers of \ALBERTA that at
least a reference manual -- documenting the API -- should be available
as part of the source-code distribution of \ALBERTA -- or at least
should be accessible through a less ``fixed'' medium than a book,
prominently to make it easier to cover new developments and fix bogus
documentation concerning API-functions, without having to republish
the entire book. As a slightly strange side-effect, the reference
manual starts with Chapter \ref{CH:model}, which explains the example
applications contained in the \code{alberta-3.0-demo} package.
Occasionally, this manual contains back-references to ``The Book'',
which is inconvenient, because that part is not yet publicly
available. My apologies; the reader is referred to the \ALBERTA-1.2
book \cite{ALBERTABOOK}. The theoretical concepts explained there
still hold.

On the other hand: providing ``on-line'' documentation does not grant
the same merits as publishing a book. One way out of this dilemma was
to separate a ``book''-section from the API-description. The book
intentionally describes the abstract concepts underlying the
\ALBERTA-toolbox, while the API-documention -- this document -- lists
the available functions and data-structures in a (hopefully)
application oriented manner, is available on-line, and thus can be
maintained more easily.

\ALBERTA-3.0 is primarily the product of extensions added by me to the
toolbox during my time at the University of Freiburg. The principal
differences to version 2.0 -- according to my judgement -- are
%%
\begin{itemize}
\item Vector-valued basis functions.
  %% 
\item Direct sums of finite element spaces, which -- together with the
  previous point -- allow for the implementation of several of the
  known stable mixed discretizations for the Stokes-problem in a
  fairly convenient manner.
  %%
\item An add-on package \code{libalbas} (distributed with the
  core-package) implementing some of the more fancy mixed
  Stokes-discretizations.
  %%
\item Periodic boundary conditions, including, but \emph{not} limited
  to mere translations, maintaining compatibility with the
  ``sub-mesh'' -- or better: ``trace-meshes'' -- introduced in version
  2.0.
  %%
\item (Iso-)parametric meshes of arbitrary degree (the support in
  \ALBERTA-2.0 was limited to at most piece-wise quadratic
  parameterizations). To be honest: V3.0 does not support anything
  beyond degree $4$, but simply because the underlying Lagrangian
  finite element spaces are only implemented up to degree $4$.
  %% 
\item Space-meshes in arbitrary co-dimension, of course with support
  for higher order parameterizations.
  %%
\item Iso-parametric higher-order boundary approximation, implementing
  the algorithms described in \cite{Lenoir:86}. A fairly complicated
  issue.
  %%
\item A cleaner separation of geometric data defined in the
  macro-triangulation and the interpretation of this data by the
  application. In particular, the implementation of boundary
  conditions has changed substantially, see \secref{S:boundary}.
  %% 
\item Limited support for Discontinuous Galerkin methods, implemented
  through a new structure describing kind of
  \hyperref[T:BNDRY_OPERATOR_INFO]{boundary operators}.
  %%
\item Likewise, differential operators optionally may be accompanied
  by contributions ``living'' on the boundary of the computational
  domain. This opens the possibility to, e.g., implement Robin
  boundary conditions without having to define a trace-mesh, simply
  because a boundary integral has to be computed to assemble the
  operator.
\item Example-programs for most of the new features, distributed along
  with the suite of demo-programs (see Chapter \ref{CH:model}).
\end{itemize}
%%
There are special ``Compatibility Notes'' interspersed with the
documentation for the individual structures and functions, concerning
differences to the predecessor \ALBERTA-2.0.

%%
Most of the time my work on the toolbox through the recent years --
after Daniel K\"oster stopped working on \ALBERTA because of his
occupation in his industrial employment -- was a one-man show.
However, lately there were noticable contributions by the following
people:
%%
Rebecca Stotz (Paraview interface, static condensation for the
``Mini''-element, synchronization between the reference manual and the
source-code of the library, ``\code{HOWTO-port-ellipt.txt}''
document),
%%
Notger Noll (\code{C}-source-code for a block-matrix solver interface,
work on the compatibility layer for
\hyperref[S:file_formats]{\code{read\_mesh()}} function, inclusion of
the \code{symmlq}-solver \cite{PaigeSaunders:75}, synchronization
between the reference manual and the source code),
%%
Christian Haarhaus (\hyperref[S:file_formats]{\code{read\_mesh()}}
compatibility layer, thus enabling \ALBERTA-3.0 to read version-1.2
data.  Grid-generator interface from \emph{FreeFem++} to \ALBERTA),
%%
Bj\"orn Stinner (contributing code for mesh-smoothing through the
computation of conformal mappings to the unit-sphere. ``Ported'' to
recent versions of the toolbox by Rebecca Stotz)

Further, my thanks go to Thilo Moshagen for beta-testing and fruitful
discussions, Thomas Bonesky for beta-testing, Robert N\"urnberg and Ed
Tucker for their bug-reports. In the likely case that somebody is
missing in above lists: my apologies, if so, then he or she was left
out unintentionally. \ALBERTA-3.0 serves as a back-end for Thilo
Moshagens \emph{albertasystems} package (a \code{C++} toolbox for the
discretization of systems of many scalar equations), my own
\emph{unfem++}-toolbox (a toolbox for unfitted finite elements); it is
also supported by the recent development versions of \emph{Dune}
(which uses the implementation of the hierarchical mesh from \ALBERTA,
besides supporting ``mesh-implementations'' from a variety of other
packages).

Of course, most prominently I'd like to thank the two principal
authors of \ALBERTA-1.2, Kunibert Siebert and Alfred Schmidt, and
Daniel K\"oster.

\bigskip

\bigskip

\noindent
Freiburg im Breisgau, \today \hfill Claus-Justus Heine

%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "alberta-man"
%%% End: 
