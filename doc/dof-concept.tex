\section{Degrees of freedom}%
\label{S:DOFs1}%
\idx{DOFs!concepts|(}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Degrees of freedom\idx{DOFS@{degree of freedom (DOFs)}} (DOFs) connect
finite element data with geometric information of a triangulation.
Each finite element function is uniquely determined by the values
(coefficients) of all its degrees of freedom.

For example, a continuous and piecewise linear finite element function
can be described by the values of this function at all vertices of the
triangulation. They build this function's degrees of freedom.  A
piecewise constant function is determined by its value in each
element. In \ALBERTA, every abstract DOF is realized as an integer
index into vectors, which corresponds to the global index in a vector
of coefficients.

For the definition of general finite element spaces DOFs located at
vertices of elements, at edges (in 2d and 3d), at faces (in 3d), or in
the interior of elements are needed.  DOFs at a vertex are shared by
all elements which meet at this vertex, DOFs at an edge or face are
shared by all elements which contain this edge or face, and DOFs
inside an element are not shared with any other element. The location
of a DOF and the sharing between elements corresponds directly to the
support of basis functions that are connected to them, see 
Figure~\ref{F:patch_dof}.

\begin{figure}[htbp]
\centerline{\includegraphics[scale=1.0]{EPS/patch_vertex}\quad
\includegraphics[scale=1.2]{EPS/patch_edge}\quad
\includegraphics[scale=1.1]{EPS/patch_face}\quad
\includegraphics[scale=1.3]{EPS/patch_center}}
\caption{Support of basis functions connected with a DOF at a vertex,
edge, face (only in 3d), and the interior.}\label{F:patch_dof}
\end{figure}

When DOFs and basis functions are used in a hierarchical manner, then
the above applies only to a single hierarchical level. Due to the
hierarchies, the supports of basis functions which belong to different
levels do overlap.

{\def\CIRC{\includegraphics[scale=0.9]{EPS/circ}\xspace}
\def\PLUS{\includegraphics[scale=0.9]{EPS/plus}\xspace}
For general applications, it may be necessary to handle several
different sets of degrees of freedom on the same triangulation. For
example, in mixed finite element methods for the Navier--Stokes
problem, different polynomial degrees are used for discrete velocity
and pressure functions. In Figure~\ref{F:dof_distribution}, three
examples of DOF distributions for continuous finite elements in 2d are
shown: piecewise quadratic finite elements \CIRC (left), piecewise
linear \PLUS and piecewise quadratic \CIRC finite elements (middle,
Taylor--Hood element for Navier--Stokes: linear pressure and quadratic
velocity), piecewise cubic \PLUS and piecewise quartic \CIRC finite
elements (right, Taylor--Hood element for Navier--Stokes: quartic
velocity and linear pressure).
}
\begin{figure}[htbp]
\centerline{\includegraphics[scale=0.8]{EPS/tria_dof}}
\caption{Examples of DOF distributions in 2d.}\label{F:dof_distribution}
\end{figure}

Additionally, different finite element spaces
may use the same set of degrees of freedom, if appropriate. For
example, higher order elements with Lagrange type basis or a
hierarchical type basis can share the same abstract set of DOFs.

The DOFs are directly connected to the mesh and its elements, by the
connection between local (on each element) and global degrees of
freedom. On the other hand, an application uses DOFs only in
connection with finite element spaces and basis functions. Thus,
while the administration of DOFs is handled by the mesh, definition
and access to DOFs is mainly done via finite element spaces.
\idx{DOFs!concepts|)}
%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "alberta-book"
%%% End: 
