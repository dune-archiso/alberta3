dnl ALBERTA_CHECK_PACKAGE()
dnl
dnl Check for a library + header files
dnl
dnl Arguments:
dnl
dnl $1: symbolic name (fancy)
dnl $2: space separated list of library names (base name,
dnl     lib$2[.so|.a|.la]
dnl $3: Optional space separated list of functions to check for.
dnl     We check first for the presence of the libraries listed in $2,
dnl     and then for all functions listed in $3, while linking with
dnl     all libraries listed in $2, and the additional libraries
dnl     listed in $5.
dnl $4: library path (-L$4)
dnl $5: additional libraries needed (e.g. -lm -lGL); if $5 contains the
dnl     string libtool, then libtool is used to resolve library
dnl     dependencies (works, of course, only if lib$2.la is installed
dnl $6: space separated list of header files to check for
dnl $7: include path(s) for $6 (-I$7). May be a space separated list
dnl     of directories.
dnl $8: additional include paths (e.g. when $6 includes some other headers)
dnl $9: a space separated list of the keywords
dnl
dnl     optional, required, disabled, enabled
dnl
dnl     optional: failing to detect the package is not fatal
dnl     required: configure is terminated when the package is not detected
dnl               (default)
dnl     disabled: package is initially disabled and needs the appropriate
dnl               --with-blah switch, otherwise the package is not checked for
dnl     enabled:  package is initially enabled (the default)
dnl
dnl Default is to check for libraries below $prefix/lib/ and for header-files
dnl below $prefix/include/
dnl
dnl $5 may be empty (e.g. to check for a Fortran library). In this case
dnl $6 and $7 are ignored
dnl
dnl This Macro defines the following variables (UPNAME means a
dnl canonicalized version of $1: i.e. uppercase and - converted to _)
dnl
dnl Makefile-substitution
dnl
dnl UPNAME[]_INCLUDE_PATH
dnl UPNAME[]_LIB_PATHS                  
dnl UPNAME[]_INCLUDES
dnl UPNAME[]_LIBS       libraries and linker flags excluding $5
dnl UPNAME[]_ALL_LIBS   libraries linker flags including $5
dnl
dnl config.h preprocessor macros
dnl
dnl HAVE_LIB[name] for all names listed in $2
dnl HAVE_[name]_H for all names listed in $6
dnl
dnl Automake conditional
dnl
dnl AM_CONDITIONAL([HAVE_]UPNAME, [test -n "$[]UPNAME[_LIBS]"])

dnl
dnl Helper-macros
dnl

dnl
dnl ALBERTA_CHECK_PKG_OPT(PKGNAME)
dnl
AC_DEFUN([ALBERTA_CHECK_PKG_OPT],
[m4_define([UPNAME], [m4_bpatsubst(m4_toupper([$1]),-,_)])
m4_if($2,enabled,
      [m4_define([NEGDEFAULT],[without])
       m4_define([NEGVERB],[disable])
       m4_define([DEFAULT],[auto-detect])],
      [m4_define([NEGDEFAULT],[with])
       m4_define([NEGVERB],[enable])
       m4_define([DEFAULT],[disabled])])
AC_ARG_WITH($1,
AS_HELP_STRING(--[]NEGDEFAULT[]-$1,
[NEGVERB use of package $1 (default: DEFAULT)]),
	[if test -z "$[]UPNAME[_DISABLE]"; then
	    case "$withval" in
		yes)
			UPNAME[_DISABLE]=no
			;;
		no)
			UPNAME[_DISABLE]=yes
			AC_MSG_RESULT([Disabling "$1"])
			;;
		*)
	      AC_MSG_ERROR(["$withval" should have been either "yes" or "no"])
			  ;;
	    esac
	fi],
	[if test -z "$[]UPNAME[_DISABLE]"; then
		m4_if($2,enabled,
                      [UPNAME[_DISABLE]=no],
                      [UPNAME[_DISABLE]=yes])
	fi])
])
dnl
dnl ALBERTA_CHECK_PKG_HDR_OPT(PKGNAME, INCLUDEPATH)
dnl
AC_DEFUN([ALBERTA_CHECK_PKG_HDR_OPT],
[m4_define([UPNAME], [m4_bpatsubst(m4_toupper([$1]),-,_)])
AC_ARG_WITH($1-headers,
m4_if($2,[],
[AS_HELP_STRING([--with-$1-headers=DIR],
  [Use $1 include files below directory DIR, DIR may actually be a space
  separated list of multiple directories; in this case each is searched
  in turn. (default: PREFIX/include/)])],
[AS_HELP_STRING([--with-$1-headers=DIR],
  [Use $1 include files below directory DIR, DIR may actually be a space
  separated list of multiple directories; in this case each is searched
  in turn.  (default: $2)])]),
[case "$withval" in
    yes) AC_MSG_ERROR("option \"--with-$1-headers\" requires an argument")
	;;
    no) AC_MSG_ERROR("option \"--with-$1-headers\" requires an argument")
	;;
    *) UPNAME[_INCLUDE_PATH]="$withval"
	;;
esac],
test -z "`eval eval eval echo ${UPNAME[_INCLUDE_PATH]}`" && UPNAME[_INCLUDE_PATH]="${DEFAULT_INCDIR}")
])
dnl
dnl headers and libraries below the same directory :(
dnl
AC_DEFUN([ALBERTA_CHECK_PKG_DIR_OPT],
[m4_define([UPNAME], [m4_bpatsubst(m4_toupper([$1]),-,_)])
AC_ARG_WITH($1-dir,
AS_HELP_STRING([--with-$1-dir=DIR],
              [use $1 library (and headers) below directory DIR (no default)]),
[case "$withval" in
    yes) AC_MSG_ERROR("option \"--with-$1-dir\" requires an argument")
	;;
    no) AC_MSG_ERROR("option \"--with-$1-dir\" requires an argument")
	;;
    *) UPNAME[_LIB_PATHS]="$withval"
       UPNAME[_INCLUDE_PATH]="$withval"
	;;
esac])
])

dnl ***************************************************************************
dnl ***************************************************************************
dnl ***************************************************************************
dnl ***************************************************************************

dnl
dnl the macro itself
dnl
AC_DEFUN([ALBERTA_CHECK_PACKAGE],
[

m4_define([alb_ac_PKGNAME],[$1])
m4_define([alb_ac_LIBNAMES],[$2])
m4_define([alb_ac_LIBFCTS],[$3])
m4_define([alb_ac_LIBPATHS],[$4])
m4_define([alb_ac_EXTRA_LIB],[$5])
m4_define([alb_ac_HEADERS],[$6])
m4_define([alb_ac_INCPATHS],[$7])
m4_define([alb_ac_EXTRA_INC],[$8])
m4_define([alb_ac_OPTIONS],[$9])

AC_REQUIRE([ALBERTA_SET_PREFIX])
m4_if($#,8,[],
      [m4_if($#,9,[],
             [errprint([$0] needs eight (8) or nine (9) arguments, but got $#)
             m4exit(1)])])
dnl
dnl upcase $1
dnl
m4_define([UPNAME], [m4_bpatsubst(m4_toupper(alb_ac_PKGNAME),-,_)])
dnl
dnl need to use m4_if, the $i arguments are not shell variables
dnl
m4_define([ENABLED],[enabled])
m4_define([OPTIONAL],[required])
m4_if($#,9,[
  m4_foreach_w([alb_ac_lvar],alb_ac_OPTIONS,
    [m4_if(alb_ac_lvar,[disabled],
           [m4_define([ENABLED],alb_ac_lvar)],
           alb_ac_lvar,[enabled],
           [m4_define([ENABLED],alb_ac_lvar)],
           alb_ac_lvar,[optional],
           [m4_define([OPTIONAL],alb_ac_lvar)],
           alb_ac_lvar,[required],
           [m4_define([OPTIONAL],alb_ac_lvar)])])])
m4_if(OPTIONAL,[optional],[ALBERTA_CHECK_PKG_OPT(alb_ac_PKGNAME,[ENABLED])])
dnl
dnl bail out if package is completely disabled
dnl
if test "${UPNAME[_DISABLE]}" = yes; then
	:
else

m4_if(alb_ac_HEADERS,[],[],
  [AC_MSG_RESULT([])
  AC_MSG_RESULT([**** Checking for OPTIONAL package alb_ac_PKGNAME ****])])

m4_if(alb_ac_LIBPATHS,[],[DEFAULT_LIBDIR="${libdir}"],
	    [DEFAULT_LIBDIR="alb_ac_LIBPATHS"])

if test "${DEFAULT_LIBDIR}" = ''
then
	DEFAULT_LIBDIR="'${libdir}'"
fi
dnl
dnl Optionally use an alternate name (e.g. MesaGL instead of GL etc.)
dnl
AC_ARG_WITH(alb_ac_PKGNAME[-name],
AS_HELP_STRING([--with-]alb_ac_PKGNAME[-name=NAME],
  [use NAME as the name of the alb_ac_PKGNAME library (without leading "lib" prefix and
  trailing suffix). If the default ("alb_ac_LIBNAMES") consists of multiple
  libraries, then NAME must be a space separated list of replacement names
  -- one name for each library.]),
[case "$withval" in
    yes) AC_MSG_ERROR(["option \"--with-]alb_ac_PKGNAME[-name\" requires an argument"])
	;;
    no) AC_MSG_ERROR(["option \"--with-]alb_ac_PKGNAME[-name\" requires an argument"])
	;;
    *) UPNAME[_NAMES]="$withval"
	;;
esac],
UPNAME[_NAMES]="alb_ac_LIBNAMES")
unset UPNAME[_LINK_NAMES]
for name in ${UPNAME[_NAMES]}; do
    if test -z "${UPNAME[_LINK_NAMES]}"; then
	UPNAME[_LINK_NAMES]="${name}"
    else
	UPNAME[_LINK_NAMES]="${UPNAME[_LINK_NAMES]} -l${name}"
    fi
done
dnl
dnl headers and libraries below the same directory :(
dnl If we have no header to check for, then this additional option does not
dnl make sense, hence the m4_if()
dnl
m4_if(alb_ac_HEADERS,[],[],[ALBERTA_CHECK_PKG_DIR_OPT(alb_ac_PKGNAME)])
dnl
dnl location of library
dnl
AC_ARG_WITH(alb_ac_PKGNAME[-lib],
m4_if(alb_ac_LIBPATHS,[],
[AS_HELP_STRING([--with-]alb_ac_PKGNAME[-lib=DIR],
               [use alb_ac_PKGNAME library below directory DIR.
               DIR maybe a space separated directory list.
               (default: EPREFIX/lib/)])],
[AS_HELP_STRING([--with-]alb_ac_PKGNAME[-lib=DIR],
               [use alb_ac_PKGNAME library below directory DIR.
               DIR maybe a space separated directory list.
               (default: alb_ac_LIBPATHS)])]),
[case "$withval" in
    yes) AC_MSG_ERROR(["option \"--with-]alb_ac_PKGNAME[-lib\" requires an argument"])
	;;
    no) AC_MSG_ERROR(["option \"--with-]alb_ac_PKGNAME[-lib\" requires an argument"])
	;;
    *) UPNAME[_LIB_PATHS]="$withval"
	;;
esac],
test -z "`eval eval eval echo ${UPNAME[_LIB_PATHS]}`" \
  && UPNAME[_LIB_PATHS]="${DEFAULT_LIBDIR}")
unset alb_ac_tmp
for path in ${UPNAME[_LIB_PATHS]}; do
  alb_ac_tmp="${alb_ac_tmp} -L${path}"
done
UPNAME[_LIB_PATHS]="${alb_ac_tmp}"
dnl
dnl now for the header file
dnl
m4_if(alb_ac_HEADERS,[],[],
	[m4_if(alb_ac_INCPATHS,[],
           [DEFAULT_INCDIR="${includedir}"
           ALBERTA_CHECK_PKG_HDR_OPT(alb_ac_PKGNAME, [PREFIX/include/])],
           [DEFAULT_INCDIR="alb_ac_INCPATHS"
           ALBERTA_CHECK_PKG_HDR_OPT(alb_ac_PKGNAME, alb_ac_INCPATHS[]/)])])
m4_define([alb_ac_LINKER],[$CC])
m4_ifdef([alb_ac_AUXLIBS], [m4_undefine([alb_ac_AUXLIBS])])
m4_foreach_w([alb_ac_lvar],alb_ac_EXTRA_LIB,
  [m4_if(alb_ac_lvar,[libtool],
     [AC_LANG_CASE([C],[m4_define([alb_ac_lang_TAG],[CC])],
                   [C++],[m4_define([alb_ac_lang_TAG],[CXX])],
                   [Fortran 77],[m4_define([alb_ac_lang_TAG],[F77])])
     m4_define([alb_ac_LINKER],
       [libtool --tag=alb_ac_lang_TAG --mode=link ${alb_ac_lang_TAG}])],
  [m4_append([alb_ac_AUXLIBS], alb_ac_lvar,[ ])])])
alb_ac_save_CC="${CC}"
CC="`eval eval eval echo alb_ac_LINKER`"
m4_ifdef([alb_ac_AUXLIBS],[],[m4_define([alb_ac_AUXLIBS],[])])
UPNAME[_AUXLIBS]="alb_ac_AUXLIBS"
dnl
dnl now check if the library and header files exist
dnl
AC_CACHE_CHECK([for libraries for package alb_ac_PKGNAME],
  [alb_ac_cv_]UPNAME[_libs],[
dnl
dnl Cache check start
dnl
m4_if(OPTIONAL,[optional],
  [AC_CHECK_LIB(${UPNAME[_LINK_NAMES]}, main,
    [UPNAME[_LIBS]="${UPNAME[_LIB_PATHS]} -l${UPNAME[_LINK_NAMES]}"
     UPNAME[_ALL_LIBS]="${UPNAME[_LIB_PATHS]} -l${UPNAME[_LINK_NAMES]} ${UPNAME[_AUXLIBS]}"],
    [UPNAME[_LIBS]=""
     UPNAME[_ALL_LIBS]=""
     UPNAME[_LIB_PATHS]=""
     UPNAME[_INCLUDES]=""
     UPNAME[_ALL_INCLUDES]=""
     UPNAME[_INCLUDE_PATH]=""],
    [`eval eval eval echo ${UPNAME[_LIB_PATHS]}` \
     `eval eval eval echo ${UPNAME[_AUXLIBS]}`])],
  [AC_CHECK_LIB(${UPNAME[_LINK_NAMES]}, main,
     [UPNAME[_LIBS]="${UPNAME[_LIB_PATHS]} -l${UPNAME[_LINK_NAMES]}"
      UPNAME[_ALL_LIBS]="${UPNAME[_LIB_PATHS]} -l${UPNAME[_LINK_NAMES]} ${UPNAME[_AUXLIBS]}"],
     [case "$host" in
	*darwin*)
		AC_MSG_RESULT([Running the test for "alb_ac_PKGNAME" again with -framework switch])
		;;
	*)
		AC_MSG_ERROR([Failed to find "lib{${UPNAME[_NAMES]}}"])
		;;
	esac],
     [`eval eval eval echo $UPNAME[_LIB_PATHS]` \
      `eval eval eval echo ${UPNAME[_AUXLIBS]}`])])
dnl
dnl On MacOS X we have that funky -framework switch ...
dnl So just run the test again with the framework switch in case the
dnl package was not found.
dnl
if test "x${UPNAME[_LIBS]}" = "x" ; then
    case "$host" in
	*darwin*)
	as_ac_Lib=`echo "ac_cv_lib_${BLAS_LINK_NAMES}''_main" | $as_tr_sh`
	eval "unset $as_ac_Lib"
	unset UPNAME[_LINK_NAMES]
	for name in ${UPNAME[_NAMES]}; do
	    if test -z "${UPNAME[_LINK_NAMES]}"; then
		UPNAME[_LINK_NAMES]="${name}"
	    else
		UPNAME[_LINK_NAMES]="${UPNAME[_LINK_NAMES]} -framework ${name}"
	    fi
	done
	unset alb_ac_tmp
	for path in ${UPNAME[_LIB_PATHS]}; do
	    alb_ac_tmp="${alb_ac_tmp} -F${path}"
	done
	UPNAME[_LIB_PATHS]="${alb_ac_tmp}"
m4_if(OPTIONAL,[optional],
  [AC_CHECK_FRAMEWORK(${UPNAME[_LINK_NAMES]}, main,
    [UPNAME[_LIBS]="${UPNAME[_LIB_PATHS]} -framework ${UPNAME[_LINK_NAMES]}"
     UPNAME[_ALL_LIBS]="${UPNAME[_LIB_PATHS]} -framework ${UPNAME[_LINK_NAMES]} ${UPNAME[_AUXLIBS]}"],
    [UPNAME[_LIBS]=""
     UPNAME[_ALL_LIBS]=""
     UPNAME[_LIB_PATHS]=""
     UPNAME[_INCLUDES]=""
     UPNAME[_ALL_INCLUDES]=""
     UPNAME[_INCLUDE_PATH]=""],
    [`eval eval eval echo ${UPNAME[_LIB_PATHS]}`
     `eval eval eval echo ${UPNAME[_AUXLIBS]}`])],
  [AC_CHECK_FRAMEWORK(${UPNAME[_NAMES]}, main,
     [UPNAME[_LIBS]="${UPNAME[_LIB_PATHS]} -framework ${UPNAME[_LINK_NAMES]}"
      UPNAME[_ALL_LIBS]="${UPNAME[_LIB_PATHS]} -framework ${UPNAME[_LINK_NAMES]} ${UPNAME[_AUXLIBS]}"],
     [AC_MSG_ERROR([Framework "${UPNAME[_NAMES]}" was not found])],
     [`eval eval eval echo $UPNAME[_LIB_PATHS]` \
      `eval eval eval echo ${UPNAME[_AUXLIBS]}`])])
	;;
  esac
fi

dnl
dnl Maybe the library seems to exist. Now check for all
dnl functions listed in alb_ac_LIBFCTS.
dnl
m4_if(alb_ac_LIBFCTS, [], [], [
if test "x${UPNAME[_LIBS]}" = "x" ; then
	:
else
  UPNAME[_LIB_EXPAND]="`eval eval eval echo ${UPNAME[_LIBS]}`"
  AC_MSG_CHECKING([for m4_split(alb_ac_LIBFCTS) in \"${UPNAME[_LIB_EXPAND]}\"])
  AC_MSG_RESULT([])
  alb_ac_save_LIBS="${LIBS}"
  LIBS="`eval eval eval echo ${UPNAME[_ALL_LIBS]}` ${LIBS}"
  AC_CHECK_FUNCS(alb_ac_LIBFCTS,[],
    [m4_if(OPTIONAL,[required],
       [AC_MSG_ERROR([Check for library function for required package alb_ac_PKGNAME failed])])
     UPNAME[_LIBS]=""
     UPNAME[_ALL_LIBS]=""
     UPNAME[_LIB_PATHS]=""
     UPNAME[_INCLUDES]=""
     UPNAME[_ALL_INCLUDES]=""
     UPNAME[_INCLUDE_PATH]=""])
  LIBS="${alb_ac_save_LIBS}"
fi])
if test "x${UPNAME[_LIBS]}" = "x" ; then
  [alb_ac_cv_]UPNAME[_libs]="notfound"
else
  [alb_ac_cv_]UPNAME[_libs]="${UPNAME[_LIBS]}"
fi
]) dnl Cache check end

if test "x${[alb_ac_cv_]UPNAME[_libs]}" = "xnotfound"; then
  m4_if(OPTIONAL,[optional],
    [UPNAME[_LIBS]=""
    UPNAME[_ALL_LIBS]=""
    UPNAME[_LIB_PATHS]=""
    UPNAME[_INCLUDES]=""
    UPNAME[_INCLUDE_PATH]=""],
    [AC_MSG_ERROR([Failed to find libraries for alb_ac_PKGNAME])])
else
  UPNAME[_LIBS]="${[alb_ac_cv_]UPNAME[_libs]}"
  UPNAME[_ALL_LIBS]="${[alb_ac_cv_]UPNAME[_libs]} ${UPNAME[_AUXLIBS]}"
fi

dnl
dnl restore default linker
dnl
CC="${alb_ac_save_CC}"

if test "x${UPNAME[_LIBS]}" = "x" ; then
	:
else
  m4_if(alb_ac_HEADERS,[],[],[
    dnl
    dnl  check for the header file(s)
    dnl
    AC_CACHE_CHECK(
      [for include-files for package alb_ac_PKGNAME],
      [alb_ac_cv_]UPNAME[_incdir],[
      [alb_ac_]UPNAME[_save_CPPFLAGS]="$CPPFLAGS"
      for [alb_ac_cv_]UPNAME[_incdir] in ${UPNAME[_INCLUDE_PATH]} ""; do
        unset alb_ac_header_status
        unset alb_ac_unset_names
        alb_ac_incdir_exp="`eval eval eval echo ${[alb_ac_cv_]UPNAME[_incdir]}`"
        if test -n "${alb_ac_incdir_exp}"; then
  	  CPPFLAGS="-I${alb_ac_incdir_exp} alb_ac_EXTRA_INC ${[alb_ac_]UPNAME[_save_CPPFLAGS]}"
          AC_MSG_CHECKING([for header(s) \"alb_ac_HEADERS\" in \"${alb_ac_incdir_exp}\" and system include path])
          AC_MSG_RESULT([])
        else
          AC_MSG_CHECKING([for header(s) \"alb_ac_HEADERS\" in system include path])
          AC_MSG_RESULT([])
        fi
        AC_CHECK_HEADERS(alb_ac_HEADERS,
          [alb_ac_unset_names="${alb_ac_unset_names} $as_ac_Header"],
          [alb_ac_unset_names="${alb_ac_unset_names} $as_ac_Header"
          alb_ac_header_status=notfound])
        if test "x${alb_ac_header_status}" = "xnotfound"; then
	  for name in ${alb_ac_unset_names}; do
            eval "unset ${name}"
	  done
        else
     	  break
        fi
      done
      CPPFLAGS="${[alb_ac_]UPNAME[_save_CPPFLAGS]}"
      if test "x${alb_ac_header_status}" = "xnotfound"; then
	[alb_ac_cv_]UPNAME[_incdir]="notfound"
      fi
    ])
    if test "x${[alb_ac_cv_]UPNAME[_incdir]}" = "xnotfound"; then
      m4_if(OPTIONAL,[optional],
        [UPNAME[_LIBS]=""
	UPNAME[_ALL_LIBS]=""
	UPNAME[_LIB_PATHS]=""
	UPNAME[_INCLUDES]=""
	UPNAME[_INCLUDE_PATH]=""],
        [AC_MSG_ERROR([Header file(s) \"alb_ac_HEADERS\" were not found])])
    else
      UPNAME[_ALL_INCLUDES]="-I${[alb_ac_cv_]UPNAME[_incdir]} alb_ac_EXTRA_INC"
      UPNAME[_INCLUDES]="-I${[alb_ac_cv_]UPNAME[_incdir]}"
      UPNAME[_INCLUDE_PATH]="${[alb_ac_cv_]UPNAME[_incdir]}"
    fi
  ])
  dnl
  dnl define makefile substitutions and config.h macros
  dnl
  if test "x${UPNAME[_LIBS]}" = "x" ; then
	:
  else
    m4_foreach_w([alb_ac_lvar],alb_ac_LIBNAMES,
      [AC_DEFINE(m4_bpatsubst(m4_toupper([HAVE_LIB]alb_ac_lvar),-,_),
                 1, [Define to 1 if you have lib]alb_ac_lvar)])
    AC_DEFINE([HAVE_PKG_]UPNAME,
              1, [Define to 1 if you have package \"ac_alb_PKGNAME\"])
  fi
fi

m4_if(alb_ac_HEADERS,[],[],[
if test -n "$[]UPNAME[_LIBS]"; then
  AC_MSG_RESULT([**** Successfully finished checks for OPTIONAL package alb_ac_PKGNAME ****])
else
  m4_if(OPTIONAL,[required],
    [AC_MSG_ERROR([**** Checks for required package alb_ac_PKGNAME finished UNSUCCESSFULLY ****])],
    [AC_MSG_RESULT([**** Checks for optional package alb_ac_PKGNAME finished UNSUCCESSFULLY ****])])
fi])

fi dnl disable fi

AM_CONDITIONAL([HAVE_]UPNAME, [test -n "$[]UPNAME[_LIBS]"])
AC_SUBST(UPNAME[_INCLUDE_PATH])
AC_SUBST(UPNAME[_INCLUDES])
AC_SUBST(UPNAME[_ALL_INCLUDES])
AC_SUBST(UPNAME[_LIB_PATHS])
AC_SUBST(UPNAME[_LIBS])
AC_SUBST(UPNAME[_ALL_LIBS])
AC_SUBST(UPNAME[_NAMES])
])
