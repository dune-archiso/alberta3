dnl
dnl $1 == required: bail out on error
dnl $1 == optional: just issue a warning.
dnl
AC_DEFUN([ALBERTA_CHECK_QT],
[AC_ARG_WITH([qtdir],
[  --with-qtdir=QTDIR     Attempt to use the QT toolkit located in QTDIR.
                         Default: ${QTDIR}.],
[case "$withval" in
	yes)
		;;
	no)
		test "$1" = required && AC_MSG_ERROR([Nope, we _need_ QT])
		;;
	*)
		QTDIR=${withval}
		;;
esac],
[for i in /usr/lib/qt3 /usr/qt/3; do
	if test -d $i; then
		QTDIR=$i
		break
	fi
done
])
AC_LANG_PUSH([C++])
ALBERTA_CHECK_PACKAGE(Qt, qt-mt, ${QTDIR}/lib, ${X_LIBS} -lX11,
                     qgl.h, ${QTDIR}/include, $1)
AC_LANG_POP
dnl
dnl check for QGLWidget
dnl
AC_CACHE_CHECK([whether class QGLWidget compiles and links],
   [ac_cv_qglwidget_links],
   [ac_cv_qglwidget_links=no
    ac_save_LDFLAGS=${LDFLAGS}
    LDFLAGS="${Qt_LIB} ${OpenGL_LIB} ${LDFLAGS}"
    AC_LANG_PUSH([C++])
    AC_LINK_IFELSE([AC_LANG_PROGRAM([[#if HAVE_QGL_H
# include <qgl.h>
#endif]],
                                    [[QGLWidget qgl;]])],
                    [ac_cv_qglwidget_links=yes],
                    [if test "$1" = required; then
                       AC_MSG_FAILURE([Unable to compile and link a "QGLWidget"])
                     else
                       AC_MSG_RESULT([Unable to compile and link a "QGLWidget"])
                     fi
                   ])
    AC_LANG_POP
    LDFLAGS="${ac_save_LDFLAGS}"
])
dnl
dnl the meta-object compiler
dnl
AC_ARG_WITH([moc],
[  --with-moc=PATH_TO_MOC Path to the "moc" program. Default: ${QTDIR}/bin/],
[case "$withval" in
	yes)
		;;
	no)
		test "$1" = required && AC_MSG_ERROR([Nope, we need moc])
		;;
	*)
		MOCPATH=${withval}
		;;
esac],
[MOCPATH=${QTDIR}/bin])
AC_PATH_PROG([MOC], moc,
  [test "$1" == required && AC_MSG_ERROR(["moc" not found])],
  [$MOCPATH:$QTDIR/bin:$PATH])

if test -n "$Qt_LIB" -a -n "$MOC" -a "$ac_cv_qglwidget_links" = yes; then
	AC_DEFINE([HAVE_QGL_WIDGET], 1,
		  [Define to "1" if Qt's QGLWidget is present and compiles])
fi
])
