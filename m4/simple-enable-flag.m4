#
# $1: name of the enable-switch (e.g. el-index)
# $2: help string
# $3: one of {0,1}, default value
#
# _OR_
#
# $1: name of the enable/disable option
# $2: help string
# $3: default value
# $4: name of the preprocessor/Makefile/shell variable
# $5: optional, if set it is a list of the keywords DEFINE, SUBST and COND
#     and specifies whether to AC_DEFINE, AC_SUBST and define an
#     AM_CONDITIONAL corresponding to the flag. The default is (if the 5th
#     argument is missing to just AC_SUBST and define a conditional.
#
# Results: declare proper AC_ARG_ENABLE(), define make-file
# subsitution (e.g. EL_INDEX), define automake conditional
# (e.g. EL_INDEX).
#
AC_DEFUN([ALBERTA_ENABLE_FLAG],
[m4_if($#,3,
       [m4_define([FLAGNAME], [m4_bpatsubst(m4_toupper([$1]),-,_)])],
       [m4_define([FLAGNAME], [m4_bpatsubst(m4_toupper([$4]),-,_)])])
m4_define([NEGDEFAULT],[enable])
m4_define([DEFAULT],[disabled])
m4_define([VALUE],[0])
m4_if([$3],[1],
      [m4_define([NEGDEFAULT],[disable])
       m4_define([DEFAULT],[enabled])
       m4_define([VALUE],[1])])
m4_if([$3],[yes],
      [m4_define([NEGDEFAULT],[disable])
       m4_define([DEFAULT],[enabled])
       m4_define([VALUE],[1])])
m4_if([$3],[enabled],
      [m4_define([NEGDEFAULT],[disable])
       m4_define([DEFAULT],[enabled])
       m4_define([VALUE],[1])])
AC_ARG_ENABLE($1,
	AS_HELP_STRING(--[]NEGDEFAULT[]-$1,
			[$2 (default: DEFAULT)]),
	[case "$enableval" in
		yes)
			FLAGNAME=1
			;;
		no)
			FLAGNAME=0
			;;
		*)
			AC_MSG_ERROR(["--[]NEGDEFAULT[]-$1" does not take an argument.])
			;;
	esac],
	[FLAGNAME=VALUE])
m4_if($#,5,
[for i in m4_toupper([$5]); do
    case $i in
      DEF*)
	AC_DEFINE_UNQUOTED(FLAGNAME, $[]FLAGNAME,
    		[Flag corresponding to the "$1" enable/disable option])
	;;
      SUBST*)	
	AC_SUBST(FLAGNAME)
	;;
      COND*)
	AM_CONDITIONAL(FLAGNAME, [test "$[]FLAGNAME" -eq 1])
	;;
    esac
done],
[AC_SUBST(FLAGNAME)
AM_CONDITIONAL(FLAGNAME, [test "$[]FLAGNAME" -eq 1])])
])
