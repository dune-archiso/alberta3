#
# check for gltools and stuff
#

AC_DEFUN([ALBERTA_GLTOOLS_CHECK],
[AC_REQUIRE([ALBERTA_OPENGL_CHECK])
AC_LANG_PUSH([C])
ALBERTA_CHECK_PACKAGE([gltools],[gltools],[glrCreate],[],[${OPENGL_ALL_LIBS}],
                      [glwin.h glrnd.h],[],[${OPENGL_ALL_INCLUDES}],[optional enabled])
if ! test "z${GLTOOLS_LIBS}" = "z"; then
    AC_MSG_CHECKING([for gltools version])
    ac_al_save_CPPFLAGS="${CPPFLAGS}"
    CPPFLAGS="`eval eval eval echo ${GLTOOLS_ALL_INCLUDES}` ${CPPFLAGS}" 
    AC_COMPILE_IFELSE(
	[AC_LANG_PROGRAM([[#include <glmesh.h>
			static glMesh m;
			static glmLoopCallback fptr;
			static glmSimplexCallback splxcb;]],
		[[fptr(m, (void *)0, splxcb, (int)0)]])],
	[AC_MSG_RESULT([using cH's special hacked gltools version])
	    AC_DEFINE([HAVE_GLMLOOPCALLBACK_COORDS_ONLY], 1,
		[Define if glmLoopCallback accepts an "coords_only" argument.])],
	[AC_MSG_RESULT([using vanilla gltools version])
	    AC_DEFINE([HAVE_GLMLOOPCALLBACK_COORDS_ONLY], 0,
		[Define if glmLoopCallback accepts an "coords_only" argument.])])
    ac_al_save_LIBS="${LIBS}"
    LIBS="`eval eval eval echo ${GLTOOLS_ALL_LIBS}` ${LIBS}"
    AC_CHECK_FUNC([glrGetPOV], [GLTOOLS_VERSION="25"], [GLTOOLS_VERSION="24"])
    LIBS="${ac_al_save_LIBS}"
    if test "$GLTOOLS_VERSION" = "24"; then
	AC_MSG_RESULT([Using gltools 2.4])
    else
	AC_MSG_RESULT([Using gltools 2.5 or later])
    fi
    AC_SUBST([GLTOOLS_VERSION])
    AC_DEFINE_UNQUOTED([GLTOOLS_VERSION], ${GLTOOLS_VERSION},
      [Define to 24 if using gltools 2.4 and to 25 when using gltools-2.5])
    CPPFLAGS="${ac_al_save_CPPFLAGS}"
fi
AC_LANG_POP([C])
])

