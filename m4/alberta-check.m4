dnl
dnl Check for ALBERTA libraries at given dimension, taking DEBUG
dnl into account. $1 is DIM_OF_WORLD.
dnl
dnl Current style (ALBERTA-2.0): library name is
dnl
dnl libalberta<DOW>[_DEBUG]
dnl

AC_DEFUN([ALBERTA_CHECK],
[AC_REQUIRE([ALBERTA_TYPE_SIZES])
AC_REQUIRE([ALBERTA_OPTIONS])
AC_REQUIRE([ALBERTA_SET_PREFIX])
AC_REQUIRE([ALBERTA_AUX_CHECK])
ALBERTA_CHECK_NEWLIBS($1)
])

dnl
dnl New-style, only DIM_OF_WORLD is fixed.
dnl
dnl DIM_OF_WORLD is passed in $1
dnl 
AC_DEFUN([ALBERTA_CHECK_NEWLIBS],
[
if test "${ALBERTA_DEBUG}" = "1"; then
	ALBERTA_LIB_$1=alberta_$1d_debug
	ALBERTA_LIB_$1_C=ac_cv_lib_ALBERTA$1_DEBUG
elif test "${ALBERTA_PROFILE}" = "1"; then
	ALBERTA_LIB_$1=alberta_$1d_profile
	ALBERTA_LIB_$1_C=ac_cv_lib_ALBERTA$1_PROFILE
else
	ALBERTA_LIB_$1=alberta_$1d
	ALBERTA_LIB_$1_C=ac_cv_lib_ALBERTA$1
fi
#
AC_CACHE_CHECK([for additional libraries needed to link],
	[ac_cv_libalberta$1_gfxlibs],
	[ac_cv_libalberta$1_gfxlibs=unset
	for GFX_LIBS in	"" \
			"$OPENGL_LIB ${X_LIBS} -lX11" \
			"$GLTOOLS_LIB $OPENGL_LIB ${X_LIBS} -lX11"
	do
	  eval "unset ${ALBERTA_LIB_$1_C}___main"
	  AC_CHECK_LIB($ALBERTA_LIB_$1, main,
	  [ac_cv_libalberta$1_gfxlibs="$GFX_LIBS"
	  break],,
	  [$GFX_LIBS])
	done])

if test "$ac_cv_libalberta$1_gfxlibs" = unset; then
    AC_MSG_ERROR([Unable to link a program with lib$ALBERTA_LIB_$1!])
else
    ALBERTA_LIBS_$1="-L$ALBERTA_LIB_PATH -l$ALBERTA_LIB_$1 $ac_cv_libalberta$1_gfxlibs"
fi
AC_SUBST([ALBERTA_LIBS_$1])
dnl
dnl search for alberta.h header
dnl
AC_CACHE_CHECK(
	[whether alberta.h exist and works with DIM_OF_WORLD=$1],
	[ac_cv_header_alberta$1_h],
	[ac_alberta_save_CPPFLAGS="$CPPFLAGS"
	unset ac_cv_header_alberta_h
	CPPFLAGS="-DDIM_OF_WORLD=$1 -I$ALBERTA_INCLUDE_PATH $CPPFLAGS"
	AC_CHECK_HEADERS(alberta.h,
		[unset ac_cv_header_alberta_h
		ac_cv_header_alberta$1_h=yes],
		[ac_cv_header_alberta$1_h=no
		AC_MSG_ERROR([Header file "alberta.h" not found])])
	CPPFLAGS="$ac_alberta_save_CPPFLAGS"
	])
	if test $ac_cv_header_alberta$1_h = yes; then
		AC_DEFINE([HAVE_ALBERTA$1_H], 1,
			[Define if "alberta.h" works with DIM_OF_WORLD=$1.])
	fi
])

dnl
dnl Define this outside ALBERTA_CHECK() so that it can be AC_REQUIRE'd
dnl

AC_DEFUN([ALBERTA_OPTIONS],
[
AC_ARG_ENABLE(debug,
  AS_HELP_STRING([--enable-debug],
    [Use debug-enabled ALBERTA libraries (default: off)]),
  [ALBERTA_DEBUG=1],
  [ALBERTA_DEBUG=0])
AC_SUBST([ALBERTA_DEBUG])
AC_ARG_ENABLE(profiling,
  AS_HELP_STRING([--enable-profiling],
    [Use profiling-enabled ALBERTA libraries (default: off)]),
  [ALBERTA_PROFILE=1],
  [ALBERTA_PROFILE=0])
AC_SUBST([ALBERTA_PROFILE])
])

dnl
dnl check for possiby needed packages, DEFUN for AC_REQUIRE
dnl
AC_DEFUN([ALBERTA_AUX_CHECK],
[AC_REQUIRE([AC_PATH_X])
AC_REQUIRE([AC_PATH_XTRA])
dnl
dnl library location
dnl
AC_ARG_WITH(alberta-libs,
[  --with-alberta-libs=LOCATION  use ALBERTA libraries installed below LOCATION.
                          The library is expected under "LOCATION/".
                          Default is the value of the environment variable
                          ALBERTA_LIB_PATH, if set, otherwise 'PREFIX/lib'],
[case "$withval" in
        yes|no)
                ;;
        *)      ALBERTA_LIB_PATH=${withval}
                ;;
esac],
[if test "x$ALBERTA_LIB_PATH" = "x" ; then
        ALBERTA_LIB_PATH=`eval eval echo ${libdir}`
fi])
AC_SUBST(ALBERTA_LIB_PATH)
dnl
dnl next for headers
dnl
AC_ARG_WITH(alberta-includes,
[  --with-alberta-includes=LOCATION  use ALBERTA includes installed below
                          LOCATION. Default is the value of the environment
                          variable ALBERTA_INCLUDE_PATH, if set, otherwise
                          'PREFIX/include'],
[case "$withval" in
        yes|no)
                ;;
        *)      ALBERTA_INCLUDE_PATH=${withval}
                ;;
esac],
[if test "x$ALBERTA_INCLUDE_PATH" = "x" ; then
        ALBERTA_INCLUDE_PATH=`eval eval echo ${includedir}`
fi])
AC_SUBST(ALBERTA_INCLUDE_PATH)
dnl
dnl Now check for the libraries ALBERTA might depend on. Take into
dnl account that we might need gltools and OpenGL
dnl
ALBERTA_CHECK_PACKAGE([OpenGL],[GL],[glMakeCurrent],
                      [${x_libraries}],[${X_LIBS} -lX11],
                      [gl.h],[${x_includes}/GL],[${X_CFLAGS}],[optional])
ALBERTA_CHECK_PACKAGE([gltools],[gltools],[glrCreate],[],[${OPENGL_ALL_LIBS}],
                      [glwin.h glrnd.h],[],[],[optional])
AC_LANG_PUSH(Fortran 77)
AC_CHECK_LIB(blas, dnrm2,
             [FLIBS="-lblas ${FLIBS}"
             AC_DEFINE(HAVE_LIBBLAS, 1, [Define to 1 if you have libblas])],
             [AC_MSG_ERROR([Required BLAS library was not found])])
AC_LANG_POP(Fortran 77)
# Cache check-point
AC_CACHE_SAVE
ALBERTA_CHECK_PACKAGE([alberta2_util],[],
                      [alberta2_util],[$ALBERTA_LIB_PATH],[-lm ${FLIBS}],
                      [alberta_util.h],[$ALBERTA_INCLUDE_PATH],[],[required])
])
