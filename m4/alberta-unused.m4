# Check if __attribute__((unused)) is supported
# Stolen from dune-common (thanks)

AC_DEFUN([ALBERTA_CHECK_UNUSED],[
    AC_CACHE_CHECK([for __attribute__((unused))], dune_cv_attribute_unused, [
        AC_LANG_PUSH([C++])
        AC_COMPILE_IFELSE([AC_LANG_PROGRAM([[
                        #define UNUSED __attribute__((unused))
                        void f(int a UNUSED, int UNUSED)
                        {
                          int UNUSED b;
                        }
                          ]],
                          [])],
                        dune_cv_attribute_unused="yes",
                        dune_cv_attribute_unused="no")
        AC_LANG_POP([C++])
    ])

    AS_IF([test "x$dune_cv_attribute_unused" = "xyes"],
      [AC_DEFINE_UNQUOTED(HAS_ATTRIBUTE_UNUSED,
                          1,
                          [does the compiler support __attribute__((unused))?])],)
])
