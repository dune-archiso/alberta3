dnl
dnl $1: dim, $2: DOW, $3: optional/required
dnl
AC_DEFUN([ALBERTA_CHECK_QGL],
[if test "${with_alberta_qgl22+set}" = set -a \
         "$with_alberta_qgl22" = no; then
	:
else 
	ALBERTA_CHECK_QT($3)
	ac_alberta_save_CPPFLAGS="$CPPFLAGS"
	CPPFLAGS="-DDIM_OF_WORLD=$2 -I$ALBERTA_INCLUDE_PATH $CPPFLAGS"
	ALBERTA_CHECK_PACKAGE(alberta-qgl$1$2, alberta-qgl$1$2, 
	  $ALBERTA_LIB_PATH, $ALBERTA_LIBS_$1 $Qt_LIB $OpenGL_LIB,
          alberta-qgl.h, $ALBERTA_INCLUDE_PATH, $3)
	CPPFLAGS="$ac_alberta_save_CPPFLAGS"
fi
])