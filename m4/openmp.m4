AC_DEFUN([ALBERTA_OPENMP],
[AC_REQUIRE([AC_PROG_CC])
AC_CHECK_HEADERS([omp.h])
_alberta_save_cflags="${CFLAGS}"
CFLAGS="${CFLAGS} ${OPENMP_CFLAGS}"
AC_LANG_PUSH([C])
AC_MSG_CHECKING(
  [for OpenMP availibility with "${CC} ${CFLAGS}"])
AC_LINK_IFELSE(
	[AC_LANG_PROGRAM(
[#ifdef HAVE_OMP_H
# include <omp.h>
#endif
#include <stdio.h>],
[int nthreads, tid;

/* Fork a team of threads giving them their own copies of variables */
#pragma omp parallel private(tid)
  {

  /* Obtain and print thread id */
  tid = omp_get_thread_num();
  printf("Hello World from thread = %d\n", tid);

  /* Only master thread does this */
  if (tid == 0) 
    {
    nthreads = omp_get_num_threads();
    printf("Number of threads = %d\n", nthreads);
    }

  }  /* All threads join master thread and terminate */])],
[AC_MSG_RESULT([OpenMP seems to be available])
AC_DEFINE([HAVE_OPENMP], 1, [Define to 1 if OpenMP is available])],
[AC_MSG_RESULT([OpenMP seems not to be available])
AC_DEFINE([HAVE_OPENMP], 0, [Define to 1 if OpenMP is available])])
AC_LANG_POP([C])
CFLAGS="${_alberta_save_cflags}"
AC_ARG_VAR([OPENMP_CFLAGS], [C/C++ compiler flags to enable OpenMP])
])
