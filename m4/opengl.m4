dnl
dnl check for OpenGL (tm)
dnl
AC_DEFUN([ALBERTA_OPENGL_CHECK],
[# ALBERTA_OPENGL_CHECK start
AC_REQUIRE([ALBERTA_X_WINDOW_SYSTEM])
AS_IF([test -z "${x_includes}" -a "${OSX}" = true],[OGL_DFLTINCDIR=/usr/X11R6/include],[OGL_DFLTINCDIR=${x_includes}])
ALBERTA_CHECK_PACKAGE([OpenGL],[GL],[glXMakeCurrent],
  [${x_libraries}],[${X_ALL_LIBS}],
  [GL/gl.h],[${x_includes:+${x_includes}} ${OGL_DFLTINCDIR} ${includedir} ${oldincludedir}],[${X_CFLAGS}],
  [optional])
# ALBERTA_OPENGL_CHECK stop
])

