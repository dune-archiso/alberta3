AC_DEFUN([ALBERTA_GRAPE_STUFF],[
dnl
dnl Some defines to enable building of alberta_movi. This is more
dnl complicated than necessary so that it is easier to build a
dnl stand-alone version of albert_movi.
dnl

ALBERTA_CHECK_PACKAGE([grape],[gr],[grape],[],[${OPENGL_ALL_LIBS}],
                      [grape.h],[],[],[optional])
ALBERTA_LIB_PATHS='$prefix/lib'
ALBERTA_ALL_INCLUDES='-I${top_builddir}/include -I${top_builddir}/include/alberta'

#
# ordinary libraries
#

ALBERTA_LIBS_1='$(top_builddir)/alberta/src/alberta_1d/libalberta_fem_1d.la $(top_builddir)/alberta/src/alberta_1d/libalberta_1d.la $(top_builddir)/alberta_util/src/libalberta_utilities.la $(GPSKCA_ALL_LIBS) $(BLAS_ALL_LIBS)'

ALBERTA_LIBS_2='$(top_builddir)/alberta/src/alberta_2d/libalberta_fem_2d.la $(top_builddir)/alberta/src/alberta_2d/libalberta_2d.la $(top_builddir)/alberta_util/src/libalberta_utilities.la $(GPSKCA_ALL_LIBS) $(BLAS_ALL_LIBS)'

ALBERTA_LIBS_3='$(top_builddir)/alberta/src/alberta_3d/libalberta_fem_3d.la $(top_builddir)/alberta/src/alberta_3d/libalberta_3d.la $(top_builddir)/alberta_util/src/libalberta_utilities.la $(GPSKCA_ALL_LIBS) $(BLAS_ALL_LIBS)'
ALBERTA_DEBUG=

#
# debug libraries
#

ALBERTA_DEBUG_LIBS_1='$(top_builddir)/alberta/src/alberta_1d_debug/libalberta_fem_1d_debug.la $(top_builddir)/alberta/src/alberta_1d_debug/libalberta_1d_debug.la $(top_builddir)/alberta_util/src/libalberta_utilities_debug.la $(GPSKCA_ALL_LIBS) $(BLAS_ALL_LIBS)'

ALBERTA_DEBUG_LIBS_2='$(top_builddir)/alberta/src/alberta_2d_debug/libalberta_fem_2d_debug.la $(top_builddir)/alberta/src/alberta_2d_debug/libalberta_2d_debug.la $(top_builddir)/alberta_util/src/libalberta_utilities_debug.la $(GPSKCA_ALL_LIBS) $(BLAS_ALL_LIBS)'

ALBERTA_DEBUG_LIBS_3='$(top_builddir)/alberta/src/alberta_3d_debug/libalberta_fem_3d_debug.la $(top_builddir)/alberta/src/alberta_3d_debug/libalberta_3d_debug.la $(top_builddir)/alberta_util/src/libalberta_utilities_debug.la $(GPSKCA_ALL_LIBS) $(BLAS_ALL_LIBS)'

#
# profiling libraries
#

ALBERTA_PROFILE_LIBS_1='$(top_builddir)/alberta/src/alberta_1d_profile/libalberta_fem_1d_profile.la $(top_builddir)/alberta/src/alberta_1d_profile/libalberta_1d_profile.la $(top_builddir)/alberta_util/src/libalberta_utilities_profile.la $(GPSKCA_ALL_LIBS) $(BLAS_ALL_LIBS)'

ALBERTA_PROFILE_LIBS_2='$(top_builddir)/alberta/src/alberta_2d_profile/libalberta_fem_2d_profile.la $(top_builddir)/alberta/src/alberta_2d_profile/libalberta_2d_profile.la $(top_builddir)/alberta_util/src/libalberta_utilities_profile.la $(GPSKCA_ALL_LIBS) $(BLAS_ALL_LIBS)'

ALBERTA_PROFILE_LIBS_3='$(top_builddir)/alberta/src/alberta_3d_profile/libalberta_fem_3d_profile.la $(top_builddir)/alberta/src/alberta_3d_profile/libalberta_3d_profile.la $(top_builddir)/alberta_util/src/libalberta_utilities_profile.la $(GPSKCA_ALL_LIBS) $(BLAS_ALL_LIBS)'

#
# ordinary gfx libraries
#

ALBERTA_GFX_LIBS_1='$(top_builddir)/alberta/src/alberta_1d/libalberta_gfx_1d.la'
ALBERTA_GFX_LIBS_2='$(top_builddir)/alberta/src/alberta_2d/libalberta_gfx_2d.la'
ALBERTA_GFX_LIBS_3='$(top_builddir)/alberta/src/alberta_3d/libalberta_gfx_3d.la'

#
# debug gfx libraries
#

ALBERTA_DEBUG_GFX_LIBS_1='$(top_builddir)/alberta/src/alberta_1d_debug/libalberta_gfx_1d_debug.la'
ALBERTA_DEBUG_GFX_LIBS_2='$(top_builddir)/alberta/src/alberta_2d_debug/libalberta_gfx_2d_debug.la'
ALBERTA_DEBUG_GFX_LIBS_3='$(top_builddir)/alberta/src/alberta_3d_debug/libalberta_gfx_3d_debug.la'

#
# profiling gfx libraries
#

ALBERTA_PROFILE_GFX_LIBS_1='$(top_builddir)/alberta/src/alberta_1d_debug/libalberta_gfx_1d_profile.la'
ALBERTA_PROFILE_GFX_LIBS_2='$(top_builddir)/alberta/src/alberta_2d_debug/libalberta_gfx_2d_profile.la'
ALBERTA_PROFILE_GFX_LIBS_3='$(top_builddir)/alberta/src/alberta_3d_debug/libalberta_gfx_3d_profile.la'

ALBERTA_DEBUG=

ALBERTA_PROFILE=

AC_SUBST(ALBERTA_DEBUG)
AC_SUBST(ALBERTA_LIB_PATHS)
AC_SUBST(ALBERTA_ALL_INCLUDES)

AC_SUBST(ALBERTA_LIBS_1)
AC_SUBST(ALBERTA_LIBS_2)
AC_SUBST(ALBERTA_LIBS_3)
AC_SUBST(ALBERTA_DEBUG_LIBS_1)
AC_SUBST(ALBERTA_DEBUG_LIBS_2)
AC_SUBST(ALBERTA_DEBUG_LIBS_3)
AC_SUBST(ALBERTA_PROFILE_LIBS_1)
AC_SUBST(ALBERTA_PROFILE_LIBS_2)
AC_SUBST(ALBERTA_PROFILE_LIBS_3)

AC_SUBST(ALBERTA_GFX_LIBS_1)
AC_SUBST(ALBERTA_GFX_LIBS_2)
AC_SUBST(ALBERTA_GFX_LIBS_3)
AC_SUBST(ALBERTA_DEBUG_GFX_LIBS_1)
AC_SUBST(ALBERTA_DEBUG_GFX_LIBS_2)
AC_SUBST(ALBERTA_DEBUG_GFX_LIBS_3)
AC_SUBST(ALBERTA_PROFILE_GFX_LIBS_1)
AC_SUBST(ALBERTA_PROFILE_GFX_LIBS_2)
AC_SUBST(ALBERTA_PROFILE_GFX_LIBS_3)

dnl
dnl end of ALBERTA_GRAPE_STUFF
dnl
])