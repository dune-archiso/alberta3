#!/bin/sh
#
# Generate all ALBERTA Makefile.am's from a template. I was just fed
# up with changing 6 different files which looked essentially the
# same.
#

for debug in 0 1 2; do
    for DIM_OF_WORLD in 1 2 3; do
	if test $debug -eq 0; then
	    SUFFIX=""
	    LIBCODE=_${DIM_OF_WORLD}d
	    EFLAGS=OPTIMIZE 
	    ALBERTA_DEBUG=0
	    ALBERTA_PROFILE=0
	elif test $debug -eq 1; then
	    SUFFIX="_debug"
	    LIBCODE=_${DIM_OF_WORLD}d_debug
	    EFLAGS=DEBUG 
	    ALBERTA_DEBUG=1
	    ALBERTA_PROFILE=0
	elif test $debug -eq 2; then
	    SUFFIX="_profile"
	    LIBCODE=_${DIM_OF_WORLD}d_profile
	    EFLAGS=PROFILE
	    ALBERTA_DEBUG=0
	    ALBERTA_PROFILE=1
	else
	    exit 1
	fi
	DIRECTORY="alberta/src/alberta${LIBCODE}"
        mkdir -p ${DIRECTORY}
	sed \
	    -e "s/%DIM_OF_WORLD%/$DIM_OF_WORLD/g" \
	    -e "s/%DEBUG%/$DEBUG/g" \
	    -e "s/%SUFFIX%/$SUFFIX/g" \
	    -e "s/%ALBERTA_DEBUG%/$ALBERTA_DEBUG/g" \
	    -e "s/%ALBERTA_PROFILE%/$ALBERTA_PROFILE/g" \
	    -e "s/%LIBCODE%/$LIBCODE/g" \
	    -e "s/%EFLAGS%/$EFLAGS/g" \
	    -e "s/%NO_MULTI_DIM%//g" \
	    -e "s/%MULTI_DIM%/#/g" \
	    Makefile.am.template > ${DIRECTORY}/Makefile.am
    done
    if test $debug -eq 0; then
	DIRECTORY="alberta/src/alberta_Nd"
    elif test $debug -eq 1; then
	DIRECTORY="alberta/src/alberta_Nd_debug"
    elif test $debug -eq 2; then
	DIRECTORY="alberta/src/alberta_Nd_profile"
    else
	exit 1
    fi
    mkdir -p ${DIRECTORY}
    for DIM_OF_WORLD in 4 5 6 7 8 9; do
	if test $debug -eq 0; then
	    SUFFIX=""
	    LIBCODE=_${DIM_OF_WORLD}d
	    EFLAGS=OPTIMIZE 
	    ALBERTA_DEBUG=0
	    ALBERTA_PROFILE=0
	elif test $debug -eq 1; then
	    SUFFIX="_debug"
	    LIBCODE=_${DIM_OF_WORLD}d_debug
	    EFLAGS=DEBUG 
	    ALBERTA_DEBUG=1
	    ALBERTA_PROFILE=0
	elif test $debug -eq 2; then
	    SUFFIX="_profile"
	    LIBCODE=_${DIM_OF_WORLD}d_profile
	    EFLAGS=PROFILE
	    ALBERTA_DEBUG=0
	    ALBERTA_PROFILE=1
	else
	    exit 1
	fi
	sed \
	    -e "s/%DIM_OF_WORLD%/$DIM_OF_WORLD/g" \
	    -e "s/%DEBUG%/$DEBUG/g" \
	    -e "s/%SUFFIX%/$SUFFIX/g" \
	    -e "s/%ALBERTA_DEBUG%/$ALBERTA_DEBUG/g" \
	    -e "s/%ALBERTA_PROFILE%/$ALBERTA_PROFILE/g" \
	    -e "s/%LIBCODE%/$LIBCODE/g" \
	    -e "s/%EFLAGS%/$EFLAGS/g" \
	    -e "s/%NO_MULTI_DIM%/#/g" \
	    -e "s/%MULTI_DIM%//g" \
	    Makefile.am.template > ${DIRECTORY}/Makefile-${DIM_OF_WORLD}d.am
    done
done
