/*!******************************************************
Class for the assemblage of a multi component block matrix system.
Wir vereinbaren die folgenden Restriktionen an das System, aus folgenden Gruenden:

Dirichlet-Rand muss im Diagonalblock eingebunden sein, d.h die entsprechenden Matrixzeilen
muessen e_i sein, was in ALBERTA durch die Setzung von add_el_matrix(... ,bound) realisiert ist.
Diese Setzung ist aber nur dann moeglich, wenn in diesem Block 
		row_fe_space=col_fe_space 
ist! Deshalb gilt in der build_...-Erbfolge diese Einschraenkung, da wir hier nicht neue Methoden
fuer Randbedingungen schreiben wollen. Faellt mir auch eben keine Methode ein, bei der es unbedingt
anders sein muss.
Also: fe_space von uh[i] =	fe_space von fh[i]
Daraus folgt auch, dass ncomps Quadraturformeln aller Art genuegen. 
*/

class build_sys:  public  build_diff
{
protected:
  virtual int set_at_el(const EL_INFO * const el_info,REAL eval_time);
  virtual int calc(const EL_INFO *el_info);
 DOF_MATRIX     *mass_matrix;
 int  rhs_scp_testfuncs(const EL_INFO* el_info);

public:
  int assemble();
//warum virtual?
  virtual ~build_sys()	
	{ 
	};
public:
						
  build_sys(const char *const name,  FE_SPACE const *const fe_space
		       ,const PROB_DATA_DIFF * const* const prob_data
		       , fem_elfunc *  common_source_contrib
  			,int quad_degree, int ncomps
			, REAL const   * const time, const  REAL *const timestep, 
						 const REAL theta);
  build_sys(const char *const name  
			 , DOF_REAL_VEC **u_h, DOF_REAL_VEC   **u_h_old, DOF_SCHAR_VEC** bound_vec
			 , DOF_REAL_VEC   **f_h 
			, const PROB_DATA_DIFF * const * const prob_data
		       , fem_elfunc *  common_source_contrib
			,const int ncomps, int quad_degree
			,REAL const   * const time, REAL const   * const timestep
			, const REAL theta);
};

