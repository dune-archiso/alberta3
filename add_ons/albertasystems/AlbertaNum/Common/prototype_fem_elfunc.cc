#include <alberta.h>

#include "newalberta.hh"
/****************************************************************************/
#include "prototype_fem_elfunc.hh"
/*Queastion is wether we should throw away this constructor. This is supposed to be an
abstract class*/
prototype_fem_elfunc::prototype_fem_elfunc( const BAS_FCTS   * const bas_fcts,  int degree,
				 const  REAL *const timestep)
		:sol_bas_fcts(bas_fcts)//, prob_data_ptr(prob_data_ptr), 
		 ,  timestep(timestep)
{   FLAGS              fill_flag;

   if (degree < 0)
			 degree = 2*bas_fcts->degree;//das is wieder so implizitees Zeug...
   if (bas_fcts->degree > 1)
     fill_flag = INIT_PHI|INIT_GRD_PHI|INIT_D2_PHI;
   else
     fill_flag = INIT_PHI|INIT_GRD_PHI;
   quad = get_quadrature(	bas_fcts->dim, degree);
   quad_fast_uh = get_quad_fast(bas_fcts, quad, fill_flag);//fuer Elemente

  uh_el     = 	new REAL[quad_fast_uh->n_bas_fcts];
  uh_old_el     = 
		new REAL[quad_fast_uh->n_bas_fcts];
  
   uh_qp	= new REAL[quad_fast_uh->n_points];
   uh_old_qp	= new REAL[quad_fast_uh->n_points];
   is_diag  = true; //?
}

//fuer Systeme. Muss erstmal offen bleiben,da hier Groesse der Vektoren unbekannt
prototype_fem_elfunc::prototype_fem_elfunc( /* int degree,*/
				 const  REAL *const timestep)
		: //prob_data_ptr(prob_data_ptr), 
		   timestep(timestep)
{ // 	FUNCNAME("prototype_fem_elfunc constuctor");
   is_diag  = true;	//?
}
