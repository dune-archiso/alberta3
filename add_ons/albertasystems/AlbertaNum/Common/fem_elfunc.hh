/*!Base class for algorithms that evaluate expressions related to a fem solution u_h and to test
functions, together with quadrature of those expressions, e.g. <f_i_comp,phi_test_i>.
I think this should be an abstract class!?*/
class fem_elfunc : public  prototype_fem_elfunc 
{public://protected, oder?
 	int set_at_el(const EL_INFO * const el_info,	REAL eval_time);
				
	int set_test_quad_fast(const QUAD_FAST  *  test_quad_fast);
//	evtl obsolet? I see no way round this
 	REAL calc(/*const REAL* const lambda,*/ const int iq)
	{ FUNCNAME("fem_elfunc::calc(const int iq)");
		
		return(calc(iq,0)); 
	};
	virtual REAL func_scp_testfunc( const int i_phi, const int j_comp);
/*!We decided to consider this part of the elfunc because e.g. elast RHS uses scalar 
product of gradients instead of this, and thus spoils stricter division of method and 
problem coeff function:*/
 
 	virtual REAL calc(const  REAL* const lambda)
	{
//	 FUNCNAME("fem_elfunc::calc(const int iq)")c
		return(calc(lambda,0));
	};
 	virtual REAL calc(const  REAL* const lambda, const int i_comp)
	{ FUNCNAME("fem_elfunc::calc(const  REAL* const lambda,const int i_comp)");
	 	ERROR_EXIT("only method of derived classes should be called\n");
		return(0.0);
	};
 	virtual REAL calc(const  int qp, const int i_comp)
	{ FUNCNAME("fem_elfunc::calc(const int iq, const int i_comp)");
	 	ERROR_EXIT("only method of derived classes should be called\n");
		return(0.0);
	};
	fem_elfunc(	 const BAS_FCTS   * const bas_fcts, 	const BAS_FCTS   * const test_bas_fcts,
	  		int degree,  const  REAL *const timestep);
	fem_elfunc(const BAS_FCTS   * const bas_fcts,  int degree
						,const  REAL *const timestep);
	fem_elfunc( /* const int degree ,*/const  REAL *const timestep);//dieser ueberlaesst die Initialisierungen dem
				//Erben	
       ~fem_elfunc()
	 	{
		//~prototype_fem_elfunc();//muss das eigentlich?
		if (col_dof != row_dof)
	  		delete [] col_dof;    
		delete [] row_dof;    
 		delete []  bound;
		};
   protected:
 	const EL_INFO 	*  el_info_ptr;
   //werden diese Datenpointer so lassen u. ggf durch den Erben umsetzen...
	const QUAD_FAST *  test_quad_fast;      /*--  ... element integration  -------------*/
    DOF    *  row_dof ; 
    DOF    *  col_dof ; 
    S_CHAR *bound; 

 };
