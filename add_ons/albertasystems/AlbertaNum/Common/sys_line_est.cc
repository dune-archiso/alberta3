#include <alberta.h>
#if (defined(ALBERTA_VER) && (ALBERTA_VER==2))
//# include <alberta_inlines.h>
//#include <alberta_util_inlines.h>
# include "alberta1_wrapper.h"
#endif

 #include "newalberta.hh"
 /****************************************************************************/
 #include "do_on_el.hh"
 #include "do_elfunc_on_el.hh"
// #include "public_do_elfunc_on_el.hh"
#include "prototype_fem_elfunc.hh"
#include "fem_elfunc.hh"
 #include "fem_base.hh"
 #include "traverse.hh"
 #include "est.hh"
 #include "ellipt_est.hh"
 #include "parab_est.hh"
 #include "sys_line_est.hh"
/****************************************************************************/
 /*                                                                          */
 /* error estimator for (nonlinear) heat equation:                           */
 /*              u_t - div A grad u + f(x,t,u,grad u) = 0                    */
 /*                                                                          */
 /* eta_h = C[0]*||h^2 ((U - Uold)/tau - div A grad U + f(x,t,U,grad U))||   */
 /*           + C[1]*|||h^1.5 [A grad U]|||                                  */
 /* eta_c = C[2]*||(Uold - Uold_coarse)/tau||                                */
 /* eta_t = C[3]*||U - Uold||                                                */
 /*                                                                          */
 /* heat_est() return value is the TIME DISCRETIZATION ESTIMATE, eta_t       */
 /*                                                                          */

 /*--------------------------------------------------------------------------*/
 /* element residual:  C2*h_S^2*|| U_t -div A nabla u^h + r ||_L^2(S)        */
 /*--------------------------------------------------------------------------*/
 /* time residual:  C4*|| U - Uold ||_L^2(S)                                 */
 /*--------------------------------------------------------------------------*/
 /* quadrature:     C5*h_S^2*||nabla(R)||_L^2(S)                             */
 /*--------------------------------------------------------------------------*/
 /* source:         C6*||f - In fn||_L^2(S)                                  */
 /*--------------------------------------------------------------------------*/


 /*--------------------------------------------------------------------------*/
 /* element residual:  C0*h_S^2*|| U_t -div A nabla u^h + r ||_L^2(S)        */
 /*--------------------------------------------------------------------------*/
 /* time residual:  C3*|| U - Uold ||_L^2(S)  neu aus estimator.c                               */
 /*--------------------------------------------------------------------------*/

 /*--------------------------------------------------------------------------*/


int sys_line_est::set_at_el(const EL_INFO * const el_info,REAL eval_time)
{
  int comp;
  parab_est::set_at_el(el_info, eval_time);
  for (comp=0; comp < nr_of_columns; comp++)
  {
//   uh_ptr 	= u_h[comp];
//   prob_data_ptr = op_arg_infos[comp]->prob_data_ptr;	//Unsinn. Sind alle Member der line_est_ptr[comp]
   line_est_ptr[comp]->set_at_el(el_info, eval_time);
      //REAL  Lambda[DIM+1][DIM_OF_WORLD];    /*  the gradient of the barycentric coordinates gibts in
  
	 prob_data_ptr->diffmat->set_at_el(el_info, eval_time);//elementwise Initialisation
	 if(prob_data_ptr->order0func) 
	 		prob_data_ptr->order0func->set_at_el(el_info,  eval_time);
  }
  return(0);
}

REAL sys_line_est::calc(/*const REAL* const lambda,*/ const int qp)
{int comp;
   REAL val=0.0;
 for (comp=0; comp < nr_of_columns; comp++)
 {
    	val += line_est_ptr[comp]->calc(/*lambda,*/ qp);
 } 
    
   return(val);
}



 /****************************************************************************/
 
sys_line_est::sys_line_est(const char *const name, fem_base** discrete_probs_lptr, 
				 int line, int nr_of_columns,
					 const REAL  *const time,const  REAL *const timestep, int degree)
	:	parab_est( name, discrete_probs_lptr[line]/*, time,*/, timestep 
				, degree),// diagonalelemen (mit Zeitableitung)
						nr_of_columns(nr_of_columns), line(line)
{  FUNCNAME("sys_line_est constructor");

	int comp;
  char  key[128];
	REAL C[4] = {-1.0, -1.0, -1.0, -1.0};
   TEST_EXIT(const_fe_space,"no const_fe_space\n");
//  get_C(name); macht parab_est!!
      C[0]  = C0 ;
      C[1]  = C1 ;
      C[2]  = C2 ;
      C[3]  = C3 ;
  INFO(info_level, 4,
	"estimation line %d C0: %e, C1: %e, C2: %e, C3: %e\n",line, C0,C1,C2,C3);
	line_est_ptr =  new ellipt_est* [nr_of_columns];
	sprintf(key, "%s_est_h%d%d", name,line, line);
     	line_est_ptr[line] = new parab_est(name, discrete_probs_lptr[line]
						, err_estimates
     						, err_c_estimates,
					/*this->time,*/ this->timestep, C,	degree );
for (comp = 0; comp < nr_of_columns; comp++)
	{
	if (line == comp) continue;
	 sprintf(key, "%s_est_h%d%d", name,line, comp);
      line_est_ptr[comp] = new ellipt_est(name, discrete_probs_lptr[comp]
      						, err_estimates, err_c_estimates,
					/*this->time,*/ this->timestep, C,	degree );
	}

}

