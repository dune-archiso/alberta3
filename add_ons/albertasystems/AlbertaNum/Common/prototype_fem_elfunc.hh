/**Base class for algorithms  that evaluate expressions related to a fem solution u_h*/
//hier geht es mehr um Daten als um Methoden.
//man kann das irgendwann mal ueberdenken.
class prototype_fem_elfunc: public elfunc
{public:
	prototype_fem_elfunc(	const BAS_FCTS   * const bas_fcts,  int degree
						,const  REAL *const timestep);
	prototype_fem_elfunc( /* int degree,*/
				 const  REAL *const timestep);
	~prototype_fem_elfunc()
	{
	delete [] 	uh_el;    
  	delete []  	uh_old_el;
	delete []     	uh_qp;
    	delete []	uh_old_qp;
	};
  protected:
	int         is_diag;
  	const BAS_FCTS   *  sol_bas_fcts;	
		//refers to  u_h bas_fcts
  	REAL    *uh_el;         
	 	/*--  vector for storing uh on el  -----*/
	REAL    *uh_old_el;      
		/*  vector for storing uh_old on el  -----*/
	REAL     *  uh_qp;
	REAL	 *  uh_old_qp;
	PROB_DATA_DIFF const *  prob_data_ptr;
	const QUAD* quad; 
	/*we keep that! Basis function independant integration is important
			and reliable.*/
	const QUAD_FAST  *  quad_fast_uh;     
	 	/*--  ... element integration  -------------*/
		//made with sol_fe_space information if made, may be just a pointer set to
		//test_quad_fast if we're dealing with a system.
  	REAL 	  const * const timestep;

};

