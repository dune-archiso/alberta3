#include <alberta.h>
#if (defined(ALBERTA_VER) && (ALBERTA_VER==2))
# include "alberta1_wrapper.h"
#endif
 
#include "newalberta.hh"
 /****************************************************************************/
#include "do_on_el.hh"
#include "do_elfunc_on_el.hh"
#include "prototype_fem_elfunc.hh"
#include "fem_elfunc.hh"
#include "traverse.hh"
#include "fem_base.hh"
#include "est.hh"
#include "ellipt_est.hh"
#include "parab_est.hh"
 /****************************************************************************/
 /*                                                                          */
 /* error estimator for (nonlinear) heat equation:                           */
 /*              u_t - div A grad u + f(x,t,u,grad u) = 0                    */
 /*                                                                          */
 /* eta_h = C[0]*||h^2 ((U - Uold)/tau - div A grad U + f(x,t,U,grad U))||   */
 /*           + C[1]*|||h^1.5 [A grad U]|||                                  */
 /* eta_c = C[2]*||(Uold - Uold_coarse)/tau||                                */
 /* eta_t = C[3]*||U - Uold||                                                */
 /*                                                                          */
 /* heat_est() return value is the TIME DISCRETIZATION ESTIMATE, eta_t       */
 /*                                                                          */
 /****************************************************************************/
 //static const BAS_FCTS  *lagrange=get_lagrange(0);
//const  FE_SPACE *	est<parab_est>::const_fe_space;


 /*--------------------------------------------------------------------------*/
 /* element residual:  C2*h_S^2*|| U_t -div A nabla u^h + r ||_L^2(S)        */
 /*--------------------------------------------------------------------------*/
 /* time residual:  C4*|| U - Uold ||_L^2(S)                                 */
 /*--------------------------------------------------------------------------*/
 /* quadrature:     C5*h_S^2*||nabla(R)||_L^2(S)                             */
 /*--------------------------------------------------------------------------*/
 /* source:         C6*||f - In fn||_L^2(S)                                  */
 /*--------------------------------------------------------------------------*/


 /*--------------------------------------------------------------------------*/
 /* element residual:  C0*h_S^2*|| U_t -div A nabla u^h + r ||_L^2(S)        */
 /*--------------------------------------------------------------------------*/
 /* time residual:  C3*|| U - Uold ||_L^2(S)  neu aus estimator.c                               */
 /*--------------------------------------------------------------------------*/
 //dies fuer die Komponenten ohne Zeitableitung. fast clone heat_el_res. 
 /*vorher: element residual:  C0*h_S^2*||   -div A nabla u^h  ||_L^2(S)        */
 /* jetzt: element residual contribution vector:    -	div A nabla u^h        */


 /*--------------------------------------------------------------------------*/
 /*  face residuals:  C1*h_Gamma*||[A(u_h)]||_L^2(Gamma)^2                   */
 /*  Since det_S = det_Gamma*h_Gamma we use for det_Gamma*h_Gamma the term   */
 /*  0.5(det_S + det_S')                                                     */
 /*--------------------------------------------------------------------------*/
//same as ellipt_est

 /*--------------------------------------------------------------------------*/
int parab_est::set_at_el(const EL_INFO * const el_info,REAL dummy)
{
    fill_el_geom_cache(el_info, 0U); // scheinbar noetig??
    fill_el_geom_cache(el_info, FILL_EL_DET|FILL_EL_LAMBDA);
    const_fe_space->bas_fcts->get_dof_indices(el_info->el
	               ,err_estimates[0]->fe_space->admin, dof);               //Baustelle.
                        //uh_old_el: das ist der einzige Unterschied zu ellipt_est. Damit u-u_old=0
    sol_bas_fcts->get_real_vec(el_info->el,    u_old[0],       uh_old_el);
    sol_bas_fcts->get_real_vec(el_info->el,	uh_ptr,    uh_el);
    est_el_res_contrib::set_at_el(el_info,  op_arg_info->eval_time);
  return(0);
}

 parab_est::parab_est(const char *const name, const fem_base* discrete_prob
 			,const  REAL *const timestep,int degree)
	 : ellipt_est(name, discrete_prob
 			, timestep, degree)
 {
  char  key[128];
	sprintf(key, "%s estimator C3", name);
    	GET_PARAMETER(1, key , "%f", &C3);
     C3    = C3 > 1.e-25 ? SQR(C3) : 0.0;

 }

//fuer line_est_ptr
 parab_est::parab_est(const char *const name, const fem_base* discrete_prob,
                        DOF_REAL_VEC  * *const err_ests, DOF_REAL_VEC ** const err_c_ests,
			   const REAL * const timestep, REAL C[4], int degree)
	 :     ellipt_est(name,  discrete_prob,
                        err_ests,  err_c_ests,
			  timestep, C,  degree)

	 { C3 =C[3];
	 }

