//#include <alberta.h>
//#include <alberta_util.h>
//#include "newalberta.hh"
/*!Wenn wir die Gestaltung von c, Lb0 u LALT-Koeffunktionen nach *problem.cc
auslagern, brauchen wir erstmal nur wenig Lalts etc. deshalb werden diese nu hier definiert.*/
/*es heisst jetzt:		
		class matfunc;
{public:
virtual int setmat(const EL_INFO * const el_info,const REAL* const lambda,
					const ASSEMBLE_EL_INFO* const 	assem_el_info)=0;
	REAL getcomp(int i, int j) 
	const{
		return mat[i][j];
		}

//		REAL order1koeff;	spaeter mal so machen!
		REAL order0koeff(const EL_INFO * const el_info,const REAL* const lambda,
					const ASSEMBLE_EL_INFO* const assem_el_info, const REAL eval_time,
				 		);*/

#ifndef _operator_parts_
#define _operator_parts_ 1
//beide Klassen in newalberta vereinbart
class constmat : public matfunc
	{public:
 	int set_at_el(const EL_INFO * const el_info/*,const REAL* const lambda*/,
					REAL dummy)
		{
		return(0);
		};
 	int set_at_bary(const REAL* const lambda)
		{
		return(0);
		};

	constmat(REAL coeff);
//	~constmat();
	};


class vardiffmat : public matfunc 
	{public:
 	int set_at_el(const EL_INFO * const el_info,	REAL eval_time);
 	int set_at_bary(const REAL* const lambda);
//	REAL calc( const REAL* const lambda); automatisch

	vardiffmat( elfunc* const function );
	private:
	 elfunc* const  elfunc_ptr;
//	 ~vardiffmat();
	};


class constfunc : public elfunc
	{public:
 	inline int set_at_el(const EL_INFO * const el_info,	REAL dummy)
				{return 0;};
 	inline REAL calc(const REAL* const lambda)
		{//koennen ruhig alle Argumente 0 sein
		return(fac);
		};

	constfunc(REAL coeff);
	};

/*!general LALt for treatment of diffusion matrix functions of the form
a*Id*/
 const REAL (*LALt(const EL_INFO *el_info, const QUAD *quad,
                         int iq, void *ud))[DIM+1]
;
/*!general LALt for treatment of diffusion matrix functions of diagonal  form*/
const REAL (*LALtdiag(const EL_INFO *el_info, const QUAD *quad,
                         int iq, void *ud))[DIM+1]
			 ;
/*!general LALt for treatment of diffusion matrix functions of any, i.e.
 not-only-diagonal and non-symmetric form (needed by elast)*/
 const REAL (*LALtfull(const EL_INFO *el_info, const QUAD *quad,
                         int iq, void *ud))[DIM+1]
;

// REAL c_std(const EL_INFO *el_info, const QUAD *quad, int iq, void *ud)
/* bei Bedarf: static const REAL (*LALt_aniso(const EL_INFO *el_info, const QUAD *quad,
                         int iq, void *ud))[DIM+1]*/

 REAL c_std(const EL_INFO *el_info, const QUAD *quad, int iq, void *ud);
 _Bool init_element(const EL_INFO * el_info,const QUAD* quad[3], void * ud );

#endif
