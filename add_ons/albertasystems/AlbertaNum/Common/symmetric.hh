//----------------------------------
// neuer symmetrischer Rand 
#define SYMMETRIC (-1000)
#define IS_SYMMETRIC(boundary)\
 ((boundary)? (boundary)->bound <= SYMMETRIC : false)
  //--------------------------------

#define NO_SIDE (-1)
#define NO_SYM  (-1)
#define NO_DOF  (-1)
#define NO_CHILD (-1)
// mark active boundary side for matrix assembly
#define SYM_ACTIVE (0)
#define SYM_PASSIVE (1)
/*
struct dof_pairs 
{ 
  DOF active;
  DOF passive;
};
*/
void new_sym(char *name, DOF *pairs[2], int nrofpairs);
int assemble_sym(DOF_MATRIX *A,DOF_REAL_VEC *f);
void refine_inter_sym(DOF_REAL_VEC *drv, RC_LIST_EL *list, int n);
void coarse_restr_sym(DOF_REAL_VEC *drv, RC_LIST_EL *list, int n);

