#ifndef _master_
#define _master_ 1
#include <ctype.h>
#include "alberta.h"
#include "newalberta.hh"

extern const ADAPT_INSTAT   *adapt_master;


void init_subdom_dof_admin(MESH *mesh);
int get_subdom(const EL *el);
REAL get_plot_subdom( EL *el);

// gem. S. 194
//static void  const_int_refine_interpol(DOF_INT_VEC* drv, RC_LIST_EL *list, int n);
//static void const_int_coarsen_interpol(DOF_INT_VEC* drv, RC_LIST_EL *list, int n);
/*Uhr: verwendet z.B. in PE_graphics.c zum Speichern*/
double get_time()
;
extern REAL time_est;
//hier addieren alle Teilschaetzer ihren Fehler zu. Main muss ihn reinitialisieren
//static const BOUNDARY *init_boundary(MESH *mesh, int ibdry);

int init_master(MESH *mesh, ADAPT_INSTAT *adapt_master);
//standard set_time. sollte bei allen adapt_instats angewendet werde, da dort der Schaetzer
//adapt_instat->timestep u adapt_instat->time braucht. Sonst NaNs.
void set_time(MESH *mesh, ADAPT_INSTAT *adapt);
#endif
