/*--------------------------------------------------------------------------*/
/* ALBERTA:  an Adaptive multi Level finite element toolbox using           */
/*           Bisectioning refinement and Error control by Residual          */
/*           Techniques for scientific Applications                         */
/*                                                                          */
/*--------------------------------------------------------------------------*/
/*                                                                          */
/* file: graphics.c                                                         */
/*                                                                          */
/*                                                                          */
/* description: graphical output for mesh, discrete solution and estimate   */
/*                                                                          */
/*--------------------------------------------------------------------------*/
/*                                                                          */
/*  authors:   Alfred Schmidt                                               */
/*             Zentrum fuer Technomathematik                                */
/*             Fachbereich 3 Mathematik/Informatik                          */
/*             Universitaet Bremen                                          */
/*             Bibliothekstr. 2                                             */
/*             D-28359 Bremen, Germany                                      */
/*                                                                          */
/*             Kunibert G. Siebert                                          */
/*             Institut fuer Mathematik                                     */
/*             Universitaet Augsburg                                        */
/*             Universitaetsstr. 14                                         */
/*             D-86159 Augsburg, Germany                                    */
/*                                                                          */
/*  http://www.alberta-fem.de/                                              */
/*                                                                          */
/*--------------------------------------------------------------------------*/
/*  (c) by A. Schmidt and K.G. Siebert (1996-2004)                          */
/*                                                                          */
/*     This program is free software; you can redistribute it and/or modify */
/*     it under the terms of the GNU General Public License as published by */
/*     the Free Software Foundation; either version 2 of the License, or    */
/*     any later version.                                                   */
/*                                                                          */
/*     This program is distributed in the hope that it will be useful,      */
/*     but WITHOUT ANY WARRANTY; without even the implied warranty of       */
/*     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        */
/*     GNU General Public License for more details.                         */
/*--------------------------------------------------------------------------*/

#include <alberta.h>
#if (defined(ALBERTA_VER) && (ALBERTA_VER==2))
//# include <alberta_inlines.h>
//#include <alberta_util_inlines.h>
# include "alberta1_wrapper.h"
#endif
#include "master.hh"


#define USE_GLTOOLS (HAVE_LIBGLTOOLS  && true)
#define NWIN 6  /* mesh_ph, phase, theta, stress, deform */
static int            nwrite   = -1/*40*/;
  static int            graph_count = 0;
    static char path[128] = "";


#define USE_GLTOOLS 1

#if !USE_GLTOOLS
/*---8<---------------------------------------------------------------------*/
/*---   simple GL graphics ...                                           ---*/
/*---   nothing will be done in 3d                                       ---*/
/*--------------------------------------------------------------------->8---*/

#else
/* fuer die Speicherung von tensorkomponenten als DOF_REAL_VECS. Kopie aus PE_graphics*/
void 	save_tensorcomps(DOF_REAL_VEC **strain, char *name)
{	char filename[20];		
  	static MESH           *mesh;
	int i;
    TEST_EXIT(mesh = strain[0]->fe_space->mesh,"no mesh\n");
 	if (! (graph_count % nwrite))
		{
		double time;
		time=get_time();
		for (i=0;i<DIM_OF_WORLD*(DIM_OF_WORLD+1)/2;i++)
			{
			sprintf(filename,"%s%s%d%s%1.5f%s",path,name,i,".",time,".xdr");
/*	MSG("Saving * :%x , (%s) \n", strain[i], filename);
 MSG("that is:\n");
  print_dof_real_vec(strain[0]);
*/			write_dof_real_vec_xdr(strain[i],filename);
			}
		}
}


 REAL get_el_mark(EL *el)
{
  if (IS_LEAF_EL(el))
	{
    return REAL(el->mark);
	}
  else
    return(0.0);
}



void graphics(MESH *mesh,  REAL (*get_el_val)(EL *el),  const DOF_REAL_VEC*  const realvec,
   		const DOF_REAL_D_VEC *  vecvec,
 			int wnr)
{
  FUNCNAME("graphics");
  static int first = true;
  static GLTOOLS_WINDOW win[NWIN] = {nil,nil,nil,nil,nil,nil};
//  static int            graph_mod   = 1;
//  static REAL             disp_scale = -1.0;
  /*static DOF_REAL_D_VEC   *u_h_scaled = nil;*/
  static int         i;//, temp_nwrite;
		double time;
		time=get_time();

//  static MESH           *mesh;
//  static GRAPH_WINDOW win_est = nil, win_val = nil, win_mesh = nil;
  static REAL  min=0.0, max=-1.0;
//  int    refine;


 if (first)
  {
    int size[NWIN] = {0}, x = 0, y = 0,max_size = 0;
    char   geom[128] = "500x500+0+0";
    char        *name[NWIN] = {"Mesh", "estimate(el val)",/* "Perlit"*/"Sol Comp1",
			    "Sol Comp 2", "Stress", "Deformation"};
    const int   frame = 12;
    GET_PARAMETER(1, "graphic windows", "%d %d %d %d %d %d", size, size+1, size+2, size+3, size+4, size+5);

    GET_PARAMETER(1, "graphic range", "%e %e", &min, &max);//muss anders

   for (i = 0; i < NWIN; i++)
    {
      if (size[i] > 0)
      	{
	  	if (x + size[i] > 1272)
	  		{y += max_size/**rel_height*/+50;
			 x = 0; /* guck ma nach!*/
       		}
		sprintf(geom, "%dx%d+%d+%d",
		size[i], size[i], x, y); 

//      win_val = open_gltools_window("ALBERTA values", geom, nil, mesh, true);
	  	win[i] = open_gltools_window(name[i], geom,nil /*world*/, mesh, true);
	  	x += size[i] + frame;
	  	max_size = MAX(max_size,size[i]);
      	}
	}
    first = false;
  }


  if (mesh && win[0]) {
//alt    gltools_mesh(win[0], mesh, 0);
    gltools_mesh(win[0], mesh, 0/*mark*/, time);
  }


  if (realvec && win[wnr]) {
    gltools_drv(win[wnr], realvec,1.0, -1.0, time); 		//min, max);
  }

  if (win[1])
  {
    if (get_el_val)
      gltools_est(win[1], mesh, get_el_val, 0.0, -1.0, time);
/*     else if (mesh) */
/*       gltools_mesh(win_est, mesh, 0); */
  }


  if (vecvec && win[wnr])
    {
     static DOF_REAL_VEC *vecvec_norm = nil;
      REAL *dv;
     /*if (!vecvev_norm)*/
	 vecvec_norm = get_dof_real_vec("|deformation|",
						       vecvec->fe_space);
      FOR_ALL_DOFS(vecvec->fe_space->admin,
		   dv = vecvec->vec[dof];
		   vecvec_norm->vec[dof] = NORM_DOW(dv);
		   );
      MSG("values of vecvec_norm in [%le, %le]\n",
	  dof_min(vecvec_norm), dof_max(vecvec_norm));
      gltools_drv(win[4], vecvec_norm, 1.0, -1.0, time);
    	}
//  else
//	{ERROR_EXIT("*deform unset but called in graphics\n");
//	}
//  if (!win_val && !win_est)  WAIT;

  return;
}
#endif
