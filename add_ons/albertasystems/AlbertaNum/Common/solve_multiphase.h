struct solve_data 
{
  REAL       tolerance;
  int        restart;
  int        max_iter;
  int        info;
};

typedef struct solve_data SOLVE_DATA;

/* aus : alberta-1.2/SOLVER/src/cg.c */ 
/* arbeiten mit dof_* typen: das mag langsam aber dafuer durchschaubar sein */
/* beachte reihenfolge: A, b, x !*/
int cg_multi(const SOLVE_DATA *sd, int n, const DOF_MATRIX *const*const a, 
		      const DOF_REAL_VEC *const*const b, DOF_REAL_VEC *const*const x);
