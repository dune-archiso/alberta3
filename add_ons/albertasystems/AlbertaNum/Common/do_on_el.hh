/*abstract base class that serves as an argument for the mesh_traverse function. Everything derived
from this can be executed elementwise during mesh traversal.*/
class do_on_el 
{
public:

  virtual void el_fct(const EL_INFO *el_info) 	=0;  
/*the function called on each element by mesh_traverse*/
//  virtual void el_clear(EL_INFO *el_info) 	=0;  
  virtual ~do_on_el()			{};
};

