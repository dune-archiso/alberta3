/*!The elfunc that calculates the element residuum. Works for elliptic and parabolic (parab_est) problems, in
the first case old solution is ste to new one.*/
class est_el_res_contrib : public  prototype_fem_elfunc
{public://protected, oder?
 	int set_at_el(const EL_INFO * const el_info, REAL eval_time);
				
 	REAL calc( const int iq);
 	REAL calc(const REAL* const lambda)
		{FUNCNAME("fem_base::calc(const REAL*)");
		ERROR_EXIT("unimplemented Method");
		return(0.0);
		};
		/*dummy*/
	est_el_res_contrib(	 const BAS_FCTS   * const bas_fcts,  int degree
					,  const  REAL *const timestep);
	 ~est_el_res_contrib()
	 	{
//	 	delete []  uh_qp;		Baustelle mal sehen
//		delete [] uh_old_qp;
		};
//   private:
    REAL_DD   *D2uhqp;
 };

/*!error estimator for elliptic problems. Implements alberta original in our C++ ways*/
class ellipt_est :  public est , public est_el_res_contrib
{
	friend class sys_line_est;//warum?
   public:
	REAL estimator( ADAPT_INSTAT *adapt/*, ungut so const REAL given_time, const REAL tau*/ ); //koennte mal wichtig werden
  	ellipt_est(const char *const name, const fem_base* discrete_prob
				,const  REAL *const timestep,int quad_degree);
 	ellipt_est(const char *const name, const fem_base* discrete_prob,
                       DOF_REAL_VEC   * * const err_estimates,
                       DOF_REAL_VEC   * * const err_c_estimates,
			  const REAL * const timestep,REAL C[4],int quad_degree);
	~ellipt_est()
		{
		delete [] contrib_qp;
  		delete [] jump_contrib_qp;
  		delete [] jumpg_contrib_qp ;
		};
protected:
  	virtual int calc(const EL_INFO *el_info);// das muss so wegen einsatz von mesh_traverse
	virtual int set_at_el(const EL_INFO * const el_info, REAL eval_time);
	virtual REAL calc(/*const REAL* const lambda,*/ const int qp);

	int 	heat_neumann_res2(const EL_INFO *el_info, int face, 
			       	 REAL* vals);
	int 	heat_jump_res2(const EL_INFO *el_info, int face,
			     REAL* vals,
			   REAL *valg);

	void get_C(const char *const name);
//private:
	DOF_REAL_VEC const *uh_ptr;
  	const QUAD_FAST  **  quad_fasts;      /*--  ...element integration  -------------*/
 		 REAL             C0, C1, C2, C3;   /*--  interior,jump,coarsen,time coefs -*/
//   		REAL      *uh_qp, *uh_old_qp; jetzt ererbt
	 REAL *contrib_qp, *jump_contrib_qp, *jumpg_contrib_qp;
private:
	int	common_construct_func(int quad_degree);

};
