/*!class for calculating componentwise error estimate for a system which was assembled with
build_sys. This class takes a shallow copy of the fem_base, usually the one of build_sys*/

class sys_est : public parab_est 
{
   public:
	sys_est(const char *const name, const fem_base* discrete_prob 
				 ,const REAL  *const time,const  REAL *const timestep,  int degree);
private:
	int set_at_el(const EL_INFO * const el_info,REAL timestep);
	REAL calc( const int qp);
	int calc(const EL_INFO * const el_info);
//	fuer Zeigerarithmetrik
	 int current_line;
	REAL *const u_old_begin;// = uh_old_el;
	REAL* const u_begin;//=uh_el;
	REAL* const uh_qp_begin ;//	=	uh_qp;
	REAL* const uh_old_qp_begin;// 	=	uh_old_qp;

};
