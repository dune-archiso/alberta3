
class est : public  fem_base//, public do_elfunc_on_el
{
public:
  virtual REAL estimator( ADAPT_INSTAT *adapt)=0;
  static const FE_SPACE * const_fe_space; 
 est(const fem_base *discrete_prob)
                       : fem_base( *discrete_prob), dof(new DOF [1]),
                       err_estimates (new DOF_REAL_VEC* [ncomps]),
                       err_c_estimates(new DOF_REAL_VEC* [ncomps]),
			 marks(new DOF_INT_VEC* [ncomps]),
			  est_sum(0.0), est_max(0.0), est_t_sum(0.0)
                               { }
//nur fuer sys_line_est
 est(const fem_base *discrete_prob,
                 DOF_REAL_VEC  *  * const err_ests,
                 DOF_REAL_VEC *  * const err_c_ests)
                       : fem_base( *discrete_prob), dof(new DOF [1]),
                       err_estimates (err_ests), err_c_estimates(err_c_ests),
                         marks(new DOF_INT_VEC* [ncomps]),
			  est_sum(0.0), est_max(0.0), est_t_sum(0.0)
                              { }


 virtual ~est()
  		{//delete [] uh_el; 
		//unfertig
		};
protected:
#if DIM > 1
//vorher  		const QUAD       *quad;           /*--  face integration     -------muessen wir mal quad_face nennen.------*/
//jetzt  const WALL_QUAD_FAST *fem_base::wall_quad_fast; /* wall integration */
#endif
protected:
       DOF * dof;
       REAL * est_vec;                 //fuer err_estimates[]->fec
       DOF_REAL_VEC **const err_estimates; //fuer Systeme
       REAL * est_c_vec;               //fuer err_estimates[]->fec
       DOF_REAL_VEC **const err_c_estimates;
       DOF_INT_VEC **const marks;
       int* mark_vec;
 		REAL             est_sum;
  		REAL             est_max;
  		REAL             est_t_sum;//nur fuer instationaeres gedacht

public:
  DOF_REAL_VEC  const *get_err_estimate(int row) const
  {
    TEST_EXIT(row <=ncomps, "%s comps %d called, which is bigger than ncomps\n", err_estimates[0]->name,row);
    return((DOF_REAL_VEC  const * )err_estimates[row]);
  };
  DOF_REAL_VEC  const *get_err_estimate(void) const
  {
    return((DOF_REAL_VEC  const * )err_estimates[0]);
  };
		//fuer Grafik
  DOF_REAL_VEC   const *get_err_c_estimate(int row) const
  {
    TEST_EXIT(row <=ncomps, "%s comps %d called, which is bigger than ncomps\n", err_c_estimates[0]->name,row);
     return((DOF_REAL_VEC  const * )err_c_estimates[row]);
  };

  DOF_REAL_VEC   const *get_err_c_estimate(void) const
  {
    return((DOF_REAL_VEC  const * )err_c_estimates[0]);
  };

  REAL get_el_est(EL *el, int comp) const;

  REAL * rw_el_est(EL const *el, int comp);
  REAL * rw_el_estc(EL const *el, int comp);
  void clear_indicator_fct(const EL_INFO *el_info);

  void clear_indicator(MESH * mesh)
  {
  for (int i=0; i<ncomps; i++)
  {
     dof_set(0.0, err_estimates[i]);
     dof_set(0.0, err_c_estimates[i]);
   }
  };
};



void init_err_dof_admin(MESH *mesh);



class clear_mark :  public do_on_el
{
  virtual void el_fct(const EL_INFO *el_info) 	
  {
    el_info->el->mark=1;
  };  
};
