/*!abstract base class that serves as an argument for the mesh_traverse function and initializes
elfunc via calling elfunc::set_at_el( el_info, nil), then doing a calculation on the whole element
 during mesh traversal.*/
class do_elfunc_on_el : public do_on_el, public elfunc 
{
public:
	void el_fct(const EL_INFO *el_info)
	{  	set_at_el( el_info, 0); 
	/* !eval_time has to be determined by derived class.*/
		calc(el_info);
	}
	/*!calls set_at_el and calc of derived class*/
protected:
 	virtual REAL calc( const int iq)=0;
	virtual int calc(const EL_INFO *el_info)=0;
	//fuer Dinge, die auf dem ganzen Element zu erledigen sind (die meisten)
};


