//Matrixaddition B=B+A:
int dof_ApB(const DOF_MATRIX* A, DOF_MATRIX* B,const DOF_SCHAR_VEC   *bound);

/*!Assemblage of a  parabolic system, one solution component.*/

class build_diff:  public  build_stat
{
private:
	OPERATOR_INFO    mass_op_info ;
 	const REAL      *const *mass_mat;
	int 		set_mass_op_info(int i, int j);
//	int 		assemble(const REAL time,const REAL tau,const REAL theta);
protected:
  //immer ein Vektor und ein Zeiger auf das zum aktuellen Block gehoerende Element. So laesst sich der
  //code am Besten recyclen und fuer Blocksysteme einsetzen
  	EL_MATRIX_INFO   *const massmatrix_infos;
   	EL_MATRIX_INFO   * massmatrix_info;
 	virtual int calc(const EL_INFO*);
//	virtual REAL calc(/*const REAL*,*/ int)
//	{	FUNCNAME("build_diff::do_elfunc_on_el::calc(const REAL*, int)");
//		ERROR("unimplemented Method. Should not be called");
//		return(0.0);
//	};
public:
 int common_construct_func(); //for constructing everything thats ok for all constructors
 			// and nonconst,  called by the others 
 build_diff(const char *const name,  
			 DOF_REAL_VEC **u_h, DOF_REAL_VEC   **u_h_old, DOF_SCHAR_VEC** bound_vec,
			  DOF_REAL_VEC	   **f_h 
				, const PROB_DATA_DIFF * const * const prob_data
				, int quad_degree,  const int ncomps
				, REAL const   * const time
				, REAL const   * const timestep, const REAL theta);


  build_diff(const char *const name,fem_base const * const base, DOF_REAL_VEC   **f_h, int quad_degree
		, const  REAL *const timestep, const REAL theta);

  build_diff(const char *const name,  FE_SPACE const *const fe_space,
		       const PROB_DATA_DIFF * const *const  prob_data,  int quad_degree, int ncomps
					,  REAL const   * const time,const  REAL *const timestep, 
						 const REAL theta);
  
};

