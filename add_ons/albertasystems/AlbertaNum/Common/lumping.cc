/****************************************************************************/
/*  mass lumping quadrature formulas for dimension 0,1,2,3                  */
/****************************************************************************/
#include <alberta.h>
/****************************************************************************/

const QUAD *get_lumping_quadrature(int dim)
{
  FUNCNAME("get_lumping_quadrature");
#define StdVol 1.0
  static const double lambda00[1] = {1.0};
  static const double *lambda0[1] = {lambda00};
  static const double weight0[1]  = {StdVol*1.0};

  static const double lambda10[2] = {1.0, 0.0};
  static const double lambda11[2] = {0.0, 1.0};
  static const double *lambda1[2] = {lambda10, lambda11};
  static const double weight1[2]  = {StdVol*0.5, StdVol*0.5};
#undef StdVol

#define StdVol 0.5
  static const double lambda20[3] = {1.0, 0.0, 0.0};
  static const double lambda21[3] = {0.0, 1.0, 0.0};
  static const double lambda22[3] = {0.0, 0.0, 1.0};
  static const double *lambda2[3] = {lambda20, lambda21, lambda22};
  static const double weight2[3]  = {StdVol/3.0, StdVol/3.0, StdVol/3.0};
#undef StdVol

#define StdVol (1.0/6.0)
  static const double lambda30[4] = {1.0, 0.0, 0.0, 0.0};
  static const double lambda31[4] = {0.0, 1.0, 0.0, 0.0};
  static const double lambda32[4] = {0.0, 0.0, 1.0, 0.0};
  static const double lambda33[4] = {0.0, 0.0, 0.0, 1.0};
  static const double *lambda3[4] = {lambda30, lambda31, lambda32, lambda33};
  static const double weight3[4]  = {StdVol/4.0, StdVol/4.0,
				     StdVol/4.0, StdVol/4.0};
#undef StdVol
  
  static const QUAD lumping[4]={
    { "lump0", 1, 0, 1, lambda0, weight0},
    { "lump1", 1, 1, 2, lambda1, weight1},
    { "lump2", 1, 2, 3, lambda2, weight2},
    { "lump3", 1, 3, 4, lambda3, weight3} };
  
  TEST_EXIT(dim >= 0 && dim < 4,"invalid dim: %d\n", dim);
  
  return(lumping+dim);
}

