//#include ".hh" muss vorher auftauchen

class sys_line_est : public parab_est 
{
   public:
	ellipt_est** line_est_ptr; 
	sys_line_est(const char *const name, fem_base** discrete_probs_lptr, 
					 int line, int nr_of_columns
					 ,const REAL  *const time,const  REAL *const timestep,  int degree);
private:
	int set_at_el(const EL_INFO * const el_info,REAL eval_time);
	REAL calc(/*const REAL* const lambda,*/ const int qp);
	const int nr_of_columns;
	const int line;

};
