class th_strain_rhs: public fem_elfunc
	{ public:
	th_strain_rhs(const DOF_REAL_VEC * const  temperature
			, const  PROB_DATA_DIFF* const (prob_datas_elast)[DIM*DIM ], 
					const int quad_degree);
	int set_test_func(const BAS_FCTS  *  test_bas_fcts);
//	int calc(const EL_INFO * const el_info);
 	~th_strain_rhs();
    protected:
	virtual int set_at_el(const EL_INFO * const el_info,const LAMBDA_DET* const assem_el_info);
 	REAL func_scp_testfunc( const int i_phi, const int j_comp);
//	REAL calc(const REAL* const lambda); 
	REAL calc(const REAL* const lambda, const int comp_nr); 
	virtual REAL calc( const int iq, const int comp_nr);
//	virtual REAL calc( const int iq);
	REAL elast_rhs_th(REAL th, REAL a, REAL p, REAL m);	
//    private:
	const DOF_REAL_VEC* temperature;
//      REAL   *pearlite_loc;
//		REAL *austenite_loc;
      	REAL *pearlite_at_qp;
	REAL *austenite_at_qp;
     	const  PROB_DATA_DIFF*  (prob_datas_elast)[DIM*DIM];
 //     const REAL   *(*get_austenite_loc)(const EL *, const DOF_REAL_VEC *, REAL *);
//		const REAL   *(*get_pearlite_loc)(const EL *, const DOF_REAL_VEC *, REAL *);	
	 REAL grd; //REAL_D
 	REAL lambda3_mu2;
	REAL lambda_mu23; 
	};
