#ifndef __phase_elast_hh
#define __phase_elast_hh

#include <alberta.h>
#include "newalberta.hh"
//phase.hh muss vorher includet werden
/****************************************************************************/

#define N_MESHES 1
#define MESH_HEAT  0
#define MESH_ELAST 0
#if N_MESHES==1
#define MESH_PHASE 0
#else
#define MESH_PHASE 1
#endif

/****************************************************************************/
/* use compact storage for deviator (symmetric, trace=0) */
#if DIM==2
#define TRIP_DIM 2
#else
#if DIM==3
#define TRIP_DIM 5
#endif
#endif
//#define NROFDOMS 4
#define LATENT_HEAT 1 //jetzt in phase
#define LATENT_MARTENSITE 2
//extern int nrofphases;
//#define ALPHA 0
//#define SMALL  1
//#define  BETA  2
//#define GAMMA  3
//#define C_SIGMA 4
#if 0
#if DIM == 1
#define h2_from_det(det) ((det)*(det))
#endif
#if DIM == 2
#define h2_from_det(det) (det)
#endif
#if DIM == 3
#define h2_from_det(det)  pow((det), 2.0/3.0)
#endif
#endif

typedef struct prob_data PROB_DATA;

struct prob_data {
  REAL t_start, t_end, dt, dt_max;
  REAL_D *(*anisotropy)(const REAL_D grd); /* anisotropy function         */
                                           /* returns matrix D2(A(grd))   */
/*sollte man wahrscheinlich auch entsprechend oben buendeln*/
  const REAL   *(*u_0)(const REAL_D x, REAL_D u);
  const REAL   *(*fu)(const REAL_D x, REAL t, REAL_D);
  const REAL   *(*gu_D)(const REAL_D x, REAL t, REAL_D);
  const REAL   *(*gu_N)(const REAL_D x, REAL t, S_CHAR bound, REAL_D);

  const REAL   *(*u_ex)(const REAL_D x,REAL_D u );
  const REAL_D *(*grd_u_ex)(const REAL_D x, REAL_DD);/*Kompatibel m H1_err_d  */

  REAL         mu;
  REAL         lambda;
  REAL         mu_lambda_scale;

  REAL         rho;

  REAL         th_a;
  REAL         th_m;
  REAL         (*elast_rhs_th)(REAL th, REAL a, REAL p, REAL m);
  REAL         (*elast_rhs_rho)(REAL th, REAL a, REAL p, REAL m);
  REAL         *(*grd_elast_rhs_th)(REAL th, REAL a, REAL p, REAL m,
                                    	REAL *grd_th, REAL *grd_a,
                                    		REAL *grd_p, REAL *grd_m);
  REAL         *(*grd_elast_rhs_rho)(REAL th, REAL a, REAL p, REAL m,
                                    	REAL *grd_th, REAL *grd_a,
                                    		REAL *grd_p, REAL *grd_m);

  REAL         trip_Kappa;
  REAL_D       *(*e_ex)(REAL t);
  REAL         *(*e_trip)(REAL t);

};

/****************************************************************************/


/****lin_sys: jetzt in PE_elast.h definiert****/
struct elast_info
{
  REAL_D  Lambda[DIM+1];
  REAL    det;

  REAL    lambda_2mu_det, mu_det, lambda_det;
  REAL    lambda3_mu2_det, lambda_mu23_det;

  const DOF_REAL_VEC  *th_h, *m_h;
  const DOF_REAL_VEC  /* **p_h*/ *p_h;

  const QUAD_FAST     *quad_fast_u, *quad_fast_th,*quad_fast_pm;
};

/****************************************************************************/

struct PE_leaf_data
{
  REAL estimate_elast;            /*  one real for the estimate                   */
  REAL est_c_elast;               /*  one real for the coarsening estimate        */
  U_CHAR mark;              /*  needed for special refinement ????          */
};

const QUAD *get_lumping_quadrature(int dim);

/*src/Common/adapt.c----------------------------------------------------------------------*/

/*int marking(MESH *mesh, ADAPT_STAT *adapt);*/
extern U_CHAR okmarking(MESH *mesh, ADAPT_STAT *adapt);

/****************************************************************************/
#endif
