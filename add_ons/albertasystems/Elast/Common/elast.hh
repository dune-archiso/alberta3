
void init_elast_dof_admin(MESH *mesh);
void init_elast(ADAPT_INSTAT *adapt
	,  const fem_elfunc* rhs_el_func_ptr
	,const  PROB_DATA_DIFF* const (*prob_datas_phase_ptr)[DIM*DIM]);
void 	clear_indicator_elast(MESH * mesh);

//const DOF_REAL_VEC  *const * const get_dtphase(void);
const DOF_REAL_VEC *const  * const get_uh_estimate(void); 

const DOF_REAL_VEC *const * const get_deformation(void);
const DOF_REAL_VEC *const * const get_uh_old(void);

 void calc_stress(void);
const DOF_REAL_VEC *const get_stress(void);

