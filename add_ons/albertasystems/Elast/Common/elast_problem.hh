#ifndef __elast_hh__
#define __elast_hh__ 1
class const_full_mat : public constmat
{
 public:
	const_full_mat(REAL* coeffs);
//	~constmat();
};

#define MU  0
#define LAMBDA 1
#define MU_LAMBDA_SCALE 2
#define  RHO  3

extern PROB_DATA_DIFF prob_data_s11;
extern PROB_DATA_DIFF prob_data_s22;
extern PROB_DATA_DIFF prob_data_s33 ;
extern PROB_DATA_DIFF prob_data_s12 ;
extern PROB_DATA_DIFF prob_data_s13;
extern PROB_DATA_DIFF prob_data_s23 ;
extern PROB_DATA_DIFF prob_data_s21;
extern PROB_DATA_DIFF prob_data_s31;
extern PROB_DATA_DIFF prob_data_s32 ;

 void init_problem_standard_steel(void);
#endif
