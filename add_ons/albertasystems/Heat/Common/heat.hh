void init_heat(ADAPT_INSTAT *adapt_heat,const PROB_DATA_DIFF* prob_data);
void init_heat_dof_admin(MESH *mesh);
const DOF_REAL_VEC * const get_temperature(void);
void    clear_indicator_heat(MESH * mesh);
/*static REAL get_el_est(EL *el); //brauchen wir fuer graphics. nee, sollns se
doch aus adapt holen.*/
