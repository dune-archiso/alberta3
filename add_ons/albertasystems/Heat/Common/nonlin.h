/*--------------------------------------------------------------------------*/
/* ALBERTA:  an Adaptive multi Level finite element toolbox using           */
/*           Bisectioning refinement and Error control by Residual          */
/*           Techniques for scientific Applications                         */
/*                                                                          */
/*--------------------------------------------------------------------------*/
/*                                                                          */
/* file:     nonlin.h                                                       */
/*                                                                          */
/* description:  solver for an nonlinear elliptic model problem             */
/*                                                                          */
/*               -k \Delta u + \sigma u^4 = f  in \Omega                    */
/*                                      u = g  on \partial \Omega           */
/*                                                                          */
/*               header file with definition of data structures and         */
/*               function definitions                                       */
/*                                                                          */
/*--------------------------------------------------------------------------*/
/*                                                                          */
/*  authors:   Alfred Schmidt                                               */
/*             Zentrum fuer Technomathematik                                */
/*             Fachbereich 3 Mathematik/Informatik                          */
/*             Universitaet Bremen                                          */
/*             Bibliothekstr. 2                                             */
/*             D-28359 Bremen, Germany                                      */
/*                                                                          */
/*             Kunibert G. Siebert                                          */
/*             Institut fuer Mathematik                                     */
/*             Universitaet Augsburg                                        */
/*             Universitaetsstr. 14                                         */
/*             D-86159 Augsburg, Germany                                    */
/*                                                                          */
/*  http://www.alberta-fem.de/                                               */
/*                                                                          */
/*--------------------------------------------------------------------------*/
/*  (c) by A. Schmidt and K.G. Siebert (1996-2004)                          */
/*                                                                          */
/*     This program is free software; you can redistribute it and/or modify */
/*     it under the terms of the GNU General Public License as published by */
/*     the Free Software Foundation; either version 2 of the License, or    */
/*     any later version.                                                   */
/*                                                                          */
/*     This program is distributed in the hope that it will be useful,      */
/*     but WITHOUT ANY WARRANTY; without even the implied warranty of       */
/*     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        */
/*     GNU General Public License for more details.                         */
/*--------------------------------------------------------------------------*/

#include <alberta.h>

/*--------------------------------------------------------------------------*/
/*  structure for collecting data of the problem:                           */
/*    k:     consant heat conductivity                                      */
/*    sigma: Stefan--Bolzmann constant                                      */
/*    g:     pointer to a function for evaluating boundary values           */
/*    f:     pointer to a function for evaluating the right-hand side       */
/*           (heat source)                                                  */
/*    u0:    if not nil, pointer to a function for evaluating an initial    */
/*           guess for the discrete solution on the macro triangulation     */
/*    u:     if not nil, pointer to a function for evaluating the true      */
/*           solution (only for test purpose)                               */
/*    grd_u: if not nil, pointer to a function for evaluating the gradient  */
/*           of the true solution (only for test purpose)                   */
/*--------------------------------------------------------------------------*/

typedef struct prob_data PROB_DATA;
struct prob_data
{
  REAL          k, sigma;

  REAL          (*g)(const REAL_D x);
  REAL          (*f)(const REAL_D x);

  REAL          (*u0)(const REAL_D x);

  REAL          (*u)(const REAL_D x);
  const REAL    *(*grd_u)(const REAL_D x);
};

/*--- file nlprob.c --------------------------------------------------------*/
const PROB_DATA *init_problem(MESH *mesh);

/*--- file nlsolve.c -------------------------------------------------------*/
int nlsolve(DOF_REAL_VEC *, REAL, REAL, REAL (*)(const REAL_D));

/*--- file graphics.c ------------------------------------------------------*/
void graphics(MESH *, DOF_REAL_VEC *, REAL (*)(EL *));
