static const FE_SPACE *fe_space;         /* initialized by init_dof_admin() */
#ifndef _new_alberta_hh_
static const FE_SPACE *const_fe_space;         /* initialized by init_dof_admin() */
static ADAPT_INSTAT   *adapt_instat;     /* initialized by main()           */
static DOF_REAL_VEC   *u_old = nil;      /* initialized by main()           */
static DOF_MATRIX     *matrix = nil;     /* initialized by main()           */
static DOF_REAL_VEC   *error_estimate = nil;      /* initialized by main()           */
#endif
static DOF_REAL_VEC   *f_h = nil;        /* initialized by main()           */
static REAL theta = 0.5;   /*---  parameter of the time discretization   ---*/
static REAL err_L2  = 0.0; /*---  spatial error in a single time step    ---*/
static DOF_REAL_VEC   *u_h = nil;        /* initialized by main()           */

void graphics(MESH *mesh, DOF_REAL_VEC *u_h, REAL (*get_est)(EL *el));

static REAL eval_time_u = 0.0;

static void init_dof_admin(MESH *mesh);
