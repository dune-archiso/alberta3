/******************************************************************************
 *
 * ALBERTA:  an Adaptive multi Level finite element toolbox  using
 *           Bisectioning refinement and Error control by Residual
 *           Techniques
 *
 * www.alberta-fem.de
 *
 *****************************************************************************
 *
 * File:     static-condensation.h
 *
 * Description: header for 'static-condensation.c'
 *
 ******************************************************************************
 *
 *  author:     Rebecca Stotz
 *              Abteilung fuer Angewandte Mathematik
 *              Albert-Ludwigs-Universitaet Freiburg
 *              Hermann-Herder-Str. 10
 *              79104 Freiburg
 *              Germany
 *
 *  http://www.mathematik.uni-freiburg.de/IAM/ALBERTA
 *
 *  (c) by R. Stotz (2009)
 *
 ******************************************************************************/

#ifndef _ALBERTA_STATIC_CONDENSATION_H_
#define _ALBERTA_STATIC_CONDENSATION_H_

#ifdef __cplusplus
extern "C" {
#elif 0
}
#endif

extern void condense_mini_spp(const DOF_REAL_VEC_D *u_h, 
			      const DOF_REAL_VEC_D *f_h,
			      const DOF_REAL_VEC *g_h, 
			      BNDRY_FLAGS dirichlet_mask,
			      EL_MATRIX_INFO *A_minfo, EL_MATRIX_INFO *B_minfo,
			      BLOCK_DOF_MATRIX *system_matrix, 
			      BLOCK_DOF_VEC *up_h, BLOCK_DOF_VEC *load_vector);


extern void expand_mini_spp(const BLOCK_DOF_VEC *up_h,
			    const DOF_REAL_VEC_D *f_h,
			    DOF_REAL_VEC_D *uh, BNDRY_FLAGS dirichlet_mask, 
			    EL_MATRIX_INFO *A_minfo, EL_MATRIX_INFO *B_minfo);


extern void condense_mini_spp_dd(const DOF_REAL_VEC_D *u_h, 
				 const DOF_REAL_VEC_D *f_h,
				 const DOF_REAL_VEC *g_h, 
				 BNDRY_FLAGS dirichlet_mask,
				 EL_MATRIX_INFO *A_minfo, 
				 EL_MATRIX_INFO *B_minfo,
				 BLOCK_DOF_MATRIX *system_matrix, 
				 BLOCK_DOF_VEC *up_h, 
				 BLOCK_DOF_VEC *load_vector);


extern void expand_mini_spp_dd(const BLOCK_DOF_VEC *up_h,
			       const DOF_REAL_VEC_D *f_h,
			       DOF_REAL_VEC_D *uh, BNDRY_FLAGS dirichlet_mask, 
			       EL_MATRIX_INFO *A_minfo, 
			       EL_MATRIX_INFO *B_minfo);


#ifdef __cplusplus
}
#endif

#endif
