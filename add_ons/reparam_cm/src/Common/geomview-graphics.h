/******************************************************************************
 *
 * ALBERTA:  an Adaptive multi Level finite element toolbox  using
 *           Bisectioning refinement and Error control by Residual
 *           Techniques
 *
 * www.alberta-fem.de
 *
 ******************************************************************************
 *
 * File:     geomview-graphics.c
 *
 * Description: very simplistic online-graphics with Geomview, we use
 *              togeomview to actually pipe data to geomview.
 *
 ******************************************************************************
 *
 *  author:     Claus-Justus Heine
 *              Abteilung fuer Angewandte Mathematik
 *              Albert-Ludwigs-Universitaet Freiburg
 *              Hermann-Herder-Str. 10
 *              79104 Freiburg
 *              Germany
 *              Claus.Heine@Mathematik.Uni-Freiburg.DE
 *
 *  (c) by C.-J. Heine (2006-2007)
 *
 ******************************************************************************/

#ifndef _ALBERTA_GEOMVIEW_GRAPHICS_H_
#define _ALBERTA_GEOMVIEW_GRAPHICS_H_

#ifdef __cplusplus
extern "C" {
#elif 0
}
#endif

extern void togeomview(MESH *mesh,
		       const DOF_REAL_VEC *u_h,
		       REAL uh_min, REAL uh_max,
		       REAL (*get_est)(EL *el),
		       REAL est_min, REAL est_max,
		       REAL (*u_loc)(const EL_INFO *el_info,
				     const REAL_B lambda,
				     void *ud),
		       void *ud, FLAGS fill_flags,
		       REAL u_min, REAL u_max);

#ifdef __cplusplus
}
#endif
  
#endif
