/**@file 
 *
 * NULL basis functions.
 *
 *      author:  Claus-Justus Heine
 *               Abteilung fuer Angewandte Mathematik                     
 *               Albert-Ludwigs-Universitaet Freiburg
 *               Hermann-Herder-Str. 10
 *               79104 Freiburg i.Br.
 *               Germany
 *               claus@mathematik.uni-freiburg.de
 *
 * Copyright (C) by Claus-Justus Heine (2008).
 *
 * Trivial zero-space, this is needed to implement the trace space of
 * bubble functions, e.g.
 *
 */

#include <alberta/alberta.h>

#include "albas.h"

static const EL_BNDRY_VEC *
null_get_bound(BNDRY_FLAGS vec[],
	       const EL_INFO *el_info, const BAS_FCTS *thisptr)
{
  static DEF_EL_VEC_CONST(BNDRY, rvec, 0, 0);

  return vec ? NULL : rvec;
}

static const EL_DOF_VEC *
null_get_dof_indices(DOF vec[],
		     const EL *el,
		     const DOF_ADMIN *admin,
		     const BAS_FCTS *thisptr)
{
  static DEF_EL_VEC_CONST(DOF, rvec, 0, 0);

  return vec ? NULL : rvec;
}

static void
null_interpol(EL_REAL_VEC *coeff,
	      const EL_INFO *el_info, int wall,
	      int n, const int *indices,
	      LOC_FCT_AT_QP f, void *ud,
	      const BAS_FCTS *thisptr)
{}

static void
null_interpol_d(EL_REAL_D_VEC *coeff,
		const EL_INFO *el_info, int wall,
		int n, const int *indices,
		LOC_FCT_D_AT_QP f, void *ud,
		const BAS_FCTS *thisptr)
{}

static void
null_interpol_dow(EL_REAL_VEC_D *coeff,
		  const EL_INFO *el_info, int wall,
		  int n, const int *indices,
		  LOC_FCT_D_AT_QP f, void *ud,
		  const BAS_FCTS *thisptr)
{}

const BAS_FCTS *get_null_bfcts(unsigned int dim)
{
  FUNCNAME("bfcts_null");
  static BAS_FCTS *null_bfcts[DIM_MAX+1];
  
  TEST_EXIT(dim <= DIM_MAX, "dim = %d > DIM_MAX = %d.\n", dim, DIM_MAX);

  if (null_bfcts[dim] == NULL) {
    null_bfcts[dim] = MEM_CALLOC(1, BAS_FCTS);
    null_bfcts[dim]->name = "NULL";
    null_bfcts[dim]->dim  = dim;
    null_bfcts[dim]->rdim = 1;
    null_bfcts[dim]->trace_admin = -1;
    CHAIN_INIT(null_bfcts[dim]);
    if (dim > 0) {
      null_bfcts[dim]->trace_bas_fcts = get_null_bfcts(dim-1);
    }
    null_bfcts[dim]->get_dof_indices = null_get_dof_indices;
    null_bfcts[dim]->get_bound = null_get_bound;
    null_bfcts[dim]->interpol = null_interpol;
    null_bfcts[dim]->interpol_d = null_interpol_d;
    null_bfcts[dim]->interpol_dow = null_interpol_dow;
    null_bfcts[dim]->get_int_vec = default_get_int_vec;
    null_bfcts[dim]->get_real_vec = default_get_real_vec;
    null_bfcts[dim]->get_real_d_vec = default_get_real_d_vec;
    null_bfcts[dim]->get_real_dd_vec = default_get_real_dd_vec;
    null_bfcts[dim]->get_real_vec_d = default_get_real_vec_d;
    null_bfcts[dim]->get_uchar_vec = default_get_uchar_vec;
    null_bfcts[dim]->get_schar_vec = default_get_schar_vec;
    null_bfcts[dim]->get_ptr_vec = default_get_ptr_vec;
  }
  
  return null_bfcts[dim];
}
