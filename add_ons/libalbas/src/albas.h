/**@file 
 *
 * General support routines for the basis-functions library.
 *
 *      author:  Claus-Justus Heine
 *               Abteilung fuer Angewandte Mathematik                     
 *               Albert-Ludwigs-Universitaet Freiburg
 *               Hermann-Herder-Str. 10
 *               79104 Freiburg i.Br.
 *               Germany
 *               claus@mathematik.uni-freiburg.de
 *
 * Copyright (C) by Claus-Justus Heine (2008).
 *
 */
#ifndef _ALBAS_H_
#define _ALBAS_H_

#include <alberta/alberta.h>

#ifdef __cplusplus
extern "C" {
#elif 0
} /* some editors try to indent because of the brace above */
#endif

typedef struct stokes_triple STOKES_TRIPLE;
typedef struct stokes_triple STOKES_PAIR;
struct stokes_triple 
{
  const BAS_FCTS *velocity;
  const BAS_FCTS *pressure;
  const BAS_FCTS *slip_stress;
};
extern STOKES_TRIPLE stokes_triple(const char *name,
				   unsigned dim, unsigned degree);
static inline STOKES_PAIR stokes_pair(const char *name,
				      unsigned dim, unsigned degree)
{
  return stokes_triple(name, dim, degree);
}

extern const BAS_FCTS *get_null_bfcts(unsigned dim);
extern const BAS_FCTS *get_bubble(unsigned dim, unsigned inter_deg);
extern const BAS_FCTS *get_wall_bubbles(unsigned dim, unsigned inter_deg);
extern const BAS_FCTS *get_trace_bubble(unsigned dim, unsigned inter_deg);
extern const BAS_FCTS *get_bulk_trace_bubble(unsigned int dim,
					     unsigned int inter_deg,
					     int trace_id);
extern const BAS_FCTS *get_tensor_wall_bubbles(unsigned int dim,
					       unsigned int tensor_deg,
					       unsigned int inter_deg);
extern const BAS_FCTS *get_trace_tensor_bubbles(unsigned int dim,
						unsigned int tensor_deg,
						unsigned int inter_deg);
extern const BAS_FCTS *get_bulk_trace_tensor_bubbles(unsigned int dim,
						     unsigned int tensor_deg,
						     unsigned int inter_deg,
						     int trace_id);
extern const BAS_FCTS *get_raviart_thomas(unsigned dim, unsigned inter_deg);
extern const BAS_FCTS *get_old_mini_element(unsigned dim);

extern const BAS_FCTS *bas_fcts_init(int dim, int dow, const char *name);

#ifdef __cplusplus
} /* extern "C" */
#endif

#endif
