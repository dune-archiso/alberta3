/*******************************************************************************
 *
 * ALBERTA:  an Adaptive multi Level finite element toolbox  using
 *           Bisectioning refinement and Error control by Residual
 *           Techniques
 *
 * www.alberta-fem.de
 *
 * file: alberta-demo.h
 *
 * description: proto-types for some helper-functions shared across
 * the various demo-programs.
 *
 ******************************************************************************
 *
 * this file's author(s):
 *            Claus-Justus Heine
 *            Abteilung fuer Angewandte Mathematik
 *            Albert-Ludwigs-Universitaet Freiburg
 *            Hermann-Herder-Str. 10
 *            79104 Freiburg
 *            Germany
 *            Claus.Heine@Mathematik.Uni-Freiburg.DE
 *
 * (c) by C.-J. Heine (2007)
 *
 ******************************************************************************/
#ifndef _ALBERTA_DEMO_H_
#define _ALBERTA_DEMO_H_

#include <limits.h>
#ifndef PATH_MAX
# define PATH_MAX 1024
#endif

#include <alberta/alberta.h>
#include "graphics.h"
#include "geomview-graphics.h"

#ifdef __cplusplus
extern "C" {
#elif 0
} /* otherwise some editors attempt automatic indentation based on above '{' */
#endif

extern void parse_parameters(int argc, char *argv[], const char *init_file);

#ifdef __cplusplus
}
#endif

#endif
