# define tests for "make check"
check_PROGRAMS = ellipt3d heat3d nonlin3d \
                 ellipt-isoparam3d ellipt-periodic3d \
                 ellipt-sphere3d ellipt-moebius ellipt-torus3d \
                 stokes3d quasi-stokes3d quasi-stokes-slip3d \
                 ellipt-dg3d ellipt2

TESTS = ellipt-isoparam3d ellipt-periodic3d \
        ellipt-sphere3d ellipt-moebius ellipt-torus3d \
        ellipt2 \
        stokes3d quasi-stokes3d quasi-stokes-slip3d
# some tests take too much time, don't run them
# ellipt3d heat3d nonlin3d
# ellipt-dg3d

# failing tests
XFAIL_TESTS = ellipt-isoparam3d ellipt-torus3d ellipt-sphere3d ellipt-moebius ellipt2

# includes
AM_CPPFLAGS = -I$(top_srcdir) \
              $(ALBERTA_ALL_INCLUDES) \
              @OBSTACK_CPPFLAGS@ \
              $(EXTRA_INC)

# ellipt3d
ellipt3d_SOURCES = ../Common/ellipt.c \
                 ../Common/alberta-demo.h \
                 ../Common/cmdline.c \
                 ../Common/graphics.c ../Common/graphics.h \
                 ../Common/geomview-graphics.c ../Common/geomview-graphics.h
if ALBERTA_USE_GRAPHICS
ellipt3d_LDADD = \
 $(ALBERTA_GFX_LIBS_3)\
 @GLTOOLS_LIBS@ @OPENDX_ALL_LIBS@ @OPENGL_LIBS@ @X_ALL_LIBS@\
 $(ALBERTA_LIBS_3)\
 @LIBS@ @DYLOADER_LIBS@
else
ellipt3d_LDADD = \
 $(ALBERTA_LIBS_3)\
 @LIBS@ @DYLOADER_LIBS@
endif
ellipt3d_DEPENDENCIES = #
ellipt3d_CPPFLAGS = $(AM_CPPFLAGS) -DALBERTA_DEBUG=0 -DDIM_OF_WORLD=3
ellipt3d_CFLAGS = $(ALBERTA_OPTIMIZE_CFLAGS)

# nonlin3d
nonlin3d_SOURCES = ../Common/nonlin.c ../Common/nonlin.h\
                 ../Common/nlprob.c ../Common/nlsolve.c \
                 ../Common/alberta-demo.h \
                 ../Common/cmdline.c \
                 ../Common/graphics.c ../Common/graphics.h \
                 ../Common/geomview-graphics.c ../Common/geomview-graphics.h
if ALBERTA_USE_GRAPHICS
nonlin3d_LDADD = \
 $(ALBERTA_GFX_LIBS_3)\
 @GLTOOLS_LIBS@ @OPENDX_ALL_LIBS@ @OPENGL_LIBS@ @X_ALL_LIBS@\
 $(ALBERTA_LIBS_3)\
 @LIBS@ @DYLOADER_LIBS@
else
nonlin3d_LDADD = \
 $(ALBERTA_LIBS_3)\
 @LIBS@ @DYLOADER_LIBS@
endif
nonlin3d_DEPENDENCIES = #
nonlin3d_CPPFLAGS = $(AM_CPPFLAGS) -DALBERTA_DEBUG=0 -DDIM_OF_WORLD=3
nonlin3d_CFLAGS = $(ALBERTA_OPTIMIZE_CFLAGS)

# heat3d
heat3d_SOURCES = ../Common/heat.c \
                 ../Common/alberta-demo.h \
                 ../Common/cmdline.c \
                 ../Common/graphics.c ../Common/graphics.h \
                 ../Common/geomview-graphics.c ../Common/geomview-graphics.h
if ALBERTA_USE_GRAPHICS
heat3d_LDADD = \
 $(ALBERTA_GFX_LIBS_3)\
 @GLTOOLS_LIBS@ @OPENDX_ALL_LIBS@ @OPENGL_LIBS@ @X_ALL_LIBS@\
 $(ALBERTA_LIBS_3)\
 @LIBS@ @DYLOADER_LIBS@
else
heat3d_LDADD = \
 $(ALBERTA_LIBS_3)\
 @LIBS@ @DYLOADER_LIBS@
endif
heat3d_DEPENDENCIES = #
heat3d_CPPFLAGS = $(AM_CPPFLAGS) -DALBERTA_DEBUG=0 -DDIM_OF_WORLD=3
heat3d_CFLAGS = $(ALBERTA_OPTIMIZE_CFLAGS)

# ellipt-isoparam3d
ellipt_isoparam3d_SOURCES = ../Common/ellipt-isoparam.c \
                 ../Common/alberta-demo.h \
                 ../Common/cmdline.c \
                 ../Common/graphics.c ../Common/graphics.h \
                 ../Common/geomview-graphics.c ../Common/geomview-graphics.h
if ALBERTA_USE_GRAPHICS
ellipt_isoparam3d_LDADD = \
 $(ALBERTA_GFX_LIBS_3)\
 @GLTOOLS_LIBS@ @OPENDX_ALL_LIBS@ @OPENGL_LIBS@ @X_ALL_LIBS@\
 $(ALBERTA_LIBS_3)\
 @LIBS@ @DYLOADER_LIBS@
else
ellipt_isoparam3d_LDADD = \
 $(ALBERTA_LIBS_3)\
 @LIBS@ @DYLOADER_LIBS@
endif
ellipt_isoparam3d_DEPENDENCIES = #
ellipt_isoparam3d_CPPFLAGS = $(AM_CPPFLAGS) -DALBERTA_DEBUG=0 -DDIM_OF_WORLD=3
ellipt_isoparam3d_CFLAGS = $(ALBERTA_OPTIMIZE_CFLAGS)

# ellipt-periodic3d
ellipt_periodic3d_SOURCES = ../Common/ellipt-periodic.c \
                 ../Common/alberta-demo.h \
                 ../Common/cmdline.c \
                 ../Common/graphics.c ../Common/graphics.h \
                 ../Common/geomview-graphics.c ../Common/geomview-graphics.h
if ALBERTA_USE_GRAPHICS
ellipt_periodic3d_LDADD = \
 $(ALBERTA_GFX_LIBS_3)\
 @GLTOOLS_LIBS@ @OPENDX_ALL_LIBS@ @OPENGL_LIBS@ @X_ALL_LIBS@\
 $(ALBERTA_LIBS_3)\
 @LIBS@ @DYLOADER_LIBS@
else
ellipt_periodic3d_LDADD = \
 $(ALBERTA_LIBS_3)\
 @LIBS@ @DYLOADER_LIBS@
endif
ellipt_periodic3d_DEPENDENCIES = #
ellipt_periodic3d_CPPFLAGS = $(AM_CPPFLAGS) -DALBERTA_DEBUG=0 -DDIM_OF_WORLD=3
ellipt_periodic3d_CFLAGS = $(ALBERTA_OPTIMIZE_CFLAGS)

# ellipt-sphere3d
ellipt_sphere3d_SOURCES = ../Common/ellipt-sphere.c \
                 ../Common/alberta-demo.h \
                 ../Common/cmdline.c \
                 ../Common/graphics.c ../Common/graphics.h \
                 ../Common/geomview-graphics.c ../Common/geomview-graphics.h
if ALBERTA_USE_GRAPHICS
ellipt_sphere3d_LDADD = \
 $(ALBERTA_GFX_LIBS_3)\
 @GLTOOLS_LIBS@ @OPENDX_ALL_LIBS@ @OPENGL_LIBS@ @X_ALL_LIBS@\
 $(ALBERTA_LIBS_3)\
 @LIBS@ @DYLOADER_LIBS@
else
ellipt_sphere3d_LDADD = \
 $(ALBERTA_LIBS_3)\
 @LIBS@ @DYLOADER_LIBS@
endif
ellipt_sphere3d_DEPENDENCIES = #
ellipt_sphere3d_CPPFLAGS = $(AM_CPPFLAGS) -DALBERTA_DEBUG=0 -DDIM_OF_WORLD=3
ellipt_sphere3d_CFLAGS = $(ALBERTA_OPTIMIZE_CFLAGS)

# ellipt-moebius
ellipt_moebius_SOURCES = ellipt-moebius.c \
                 ../Common/alberta-demo.h \
                 ../Common/cmdline.c \
                 ../Common/graphics.c ../Common/graphics.h \
                 ../Common/geomview-graphics.c ../Common/geomview-graphics.h
if ALBERTA_USE_GRAPHICS
ellipt_moebius_LDADD = \
 $(ALBERTA_GFX_LIBS_3)\
 @GLTOOLS_LIBS@ @OPENDX_ALL_LIBS@ @OPENGL_LIBS@ @X_ALL_LIBS@\
 $(ALBERTA_LIBS_3)\
 @LIBS@ @DYLOADER_LIBS@
else
ellipt_moebius_LDADD = \
 $(ALBERTA_LIBS_3)\
 @LIBS@ @DYLOADER_LIBS@
endif
ellipt_moebius_DEPENDENCIES = #
ellipt_moebius_CPPFLAGS = $(AM_CPPFLAGS) -DALBERTA_DEBUG=0 -DDIM_OF_WORLD=3
ellipt_moebius_CFLAGS = $(ALBERTA_OPTIMIZE_CFLAGS)

# ellipt-torus3d
ellipt_torus3d_SOURCES = ../Common/ellipt-torus.c \
                 ../Common/alberta-demo.h \
                 ../Common/cmdline.c \
                 ../Common/graphics.c ../Common/graphics.h \
                 ../Common/geomview-graphics.c ../Common/geomview-graphics.h
if ALBERTA_USE_GRAPHICS
ellipt_torus3d_LDADD = \
 $(ALBERTA_GFX_LIBS_3)\
 @GLTOOLS_LIBS@ @OPENDX_ALL_LIBS@ @OPENGL_LIBS@ @X_ALL_LIBS@\
 $(ALBERTA_LIBS_3)\
 @LIBS@ @DYLOADER_LIBS@
else
ellipt_torus3d_LDADD = \
 $(ALBERTA_LIBS_3)\
 @LIBS@ @DYLOADER_LIBS@
endif
ellipt_torus3d_DEPENDENCIES = #
ellipt_torus3d_CPPFLAGS = $(AM_CPPFLAGS) -DALBERTA_DEBUG=0 -DDIM_OF_WORLD=3
ellipt_torus3d_CFLAGS = $(ALBERTA_OPTIMIZE_CFLAGS)

# stokes3d
stokes3d_SOURCES = ../Common/stokes.c \
                 ../Common/alberta-demo.h \
                 ../Common/cmdline.c \
                 ../Common/graphics.c ../Common/graphics.h \
                 ../Common/geomview-graphics.c ../Common/geomview-graphics.h
if ALBERTA_USE_GRAPHICS
stokes3d_LDADD = \
 $(ALBERTA_GFX_LIBS_3)\
 $(top_builddir)/add_ons/libalbas/src/libalbas_3d.la\
 @GLTOOLS_LIBS@ @OPENDX_ALL_LIBS@ @OPENGL_LIBS@ @X_ALL_LIBS@\
 $(ALBERTA_LIBS_3)\
 @LIBS@ @DYLOADER_LIBS@
else
stokes3d_LDADD = \
 $(ALBERTA_LIBS_3)\
 $(top_builddir)/add_ons/libalbas/src/libalbas_3d.la\
 @LIBS@ @DYLOADER_LIBS@
endif
stokes3d_DEPENDENCIES = #
stokes3d_CPPFLAGS = $(AM_CPPFLAGS) -DALBERTA_DEBUG=0 -DDIM_OF_WORLD=3
stokes3d_CFLAGS = $(ALBERTA_OPTIMIZE_CFLAGS)

# quasi-stokes3d
quasi_stokes3d_SOURCES = ../Common/quasi-stokes.c \
                 ../Common/alberta-demo.h \
                 ../Common/cmdline.c \
                 ../Common/graphics.c ../Common/graphics.h \
                 ../Common/geomview-graphics.c ../Common/geomview-graphics.h
if ALBERTA_USE_GRAPHICS
quasi_stokes3d_LDADD = \
 $(ALBERTA_GFX_LIBS_3)\
 $(top_builddir)/add_ons/libalbas/src/libalbas_3d.la\
 @GLTOOLS_LIBS@ @OPENDX_ALL_LIBS@ @OPENGL_LIBS@ @X_ALL_LIBS@\
 $(ALBERTA_LIBS_3)\
 @LIBS@ @DYLOADER_LIBS@
else
quasi_stokes3d_LDADD = \
 $(ALBERTA_LIBS_3)\
 $(top_builddir)/add_ons/libalbas/src/libalbas_3d.la\
 @LIBS@ @DYLOADER_LIBS@
endif
quasi_stokes3d_DEPENDENCIES = #
quasi_stokes3d_CPPFLAGS = $(AM_CPPFLAGS) -DALBERTA_DEBUG=0 -DDIM_OF_WORLD=3
quasi_stokes3d_CFLAGS = $(ALBERTA_OPTIMIZE_CFLAGS)

# quasi-stokes-slip3d
quasi_stokes_slip3d_SOURCES = ../Common/quasi-stokes-slip.c \
                 ../Common/alberta-demo.h \
                 ../Common/cmdline.c \
                 ../Common/graphics.c ../Common/graphics.h \
                 ../Common/geomview-graphics.c ../Common/geomview-graphics.h
if ALBERTA_USE_GRAPHICS
quasi_stokes_slip3d_LDADD = \
 $(ALBERTA_GFX_LIBS_3)\
 @GLTOOLS_LIBS@ @OPENDX_ALL_LIBS@ @OPENGL_LIBS@ @X_ALL_LIBS@\
 $(ALBERTA_LIBS_3)\
 $(top_builddir)/add_ons/libalbas/src/libalbas_3d.la\
 @LIBS@ @DYLOADER_LIBS@
else
quasi_stokes_slip3d_LDADD = \
 $(ALBERTA_LIBS_3)\
 $(top_builddir)/add_ons/libalbas/src/libalbas_3d.la\
 @LIBS@ @DYLOADER_LIBS@
endif
quasi_stokes_slip3d_DEPENDENCIES = #
quasi_stokes_slip3d_CPPFLAGS = $(AM_CPPFLAGS) -DALBERTA_DEBUG=0 -DDIM_OF_WORLD=3
quasi_stokes_slip3d_CFLAGS = $(ALBERTA_OPTIMIZE_CFLAGS)

# ellipt-dg3d
ellipt_dg3d_SOURCES = ../Common/ellipt-dg.c \
                 ../Common/alberta-demo.h \
                 ../Common/cmdline.c \
                 ../Common/graphics.c ../Common/graphics.h \
                 ../Common/geomview-graphics.c ../Common/geomview-graphics.h
if ALBERTA_USE_GRAPHICS
ellipt_dg3d_LDADD = \
 $(ALBERTA_GFX_LIBS_3)\
 @GLTOOLS_LIBS@ @OPENDX_ALL_LIBS@ @OPENGL_LIBS@ @X_ALL_LIBS@\
 $(ALBERTA_LIBS_3)\
 @LIBS@ @DYLOADER_LIBS@
else
ellipt_dg3d_LDADD = \
 $(ALBERTA_LIBS_3)\
 @LIBS@ @DYLOADER_LIBS@
endif
ellipt_dg3d_DEPENDENCIES = #
ellipt_dg3d_CPPFLAGS = $(AM_CPPFLAGS) -DALBERTA_DEBUG=0 -DDIM_OF_WORLD=3
ellipt_dg3d_CFLAGS = $(ALBERTA_OPTIMIZE_CFLAGS)

# ellipt2
ellipt2_SOURCES = ellipt2.c \
                 ../Common/alberta-demo.h \
                 ../Common/cmdline.c \
                 ../Common/graphics.c ../Common/graphics.h \
                 ../Common/geomview-graphics.c ../Common/geomview-graphics.h
if ALBERTA_USE_GRAPHICS
ellipt2_LDADD = \
 $(ALBERTA_GFX_LIBS_3)\
 @GLTOOLS_LIBS@ @OPENDX_ALL_LIBS@ @OPENGL_LIBS@ @X_ALL_LIBS@\
 $(ALBERTA_LIBS_3)\
 @LIBS@ @DYLOADER_LIBS@
else
ellipt2_LDADD = \
 $(ALBERTA_LIBS_3)\
 @LIBS@ @DYLOADER_LIBS@
endif
ellipt2_DEPENDENCIES = #
ellipt2_CPPFLAGS = $(AM_CPPFLAGS) -DALBERTA_DEBUG=0 -DDIM_OF_WORLD=3
ellipt2_CFLAGS = $(ALBERTA_OPTIMIZE_CFLAGS)

distclean-local:
	rm -f foobar.mesh
