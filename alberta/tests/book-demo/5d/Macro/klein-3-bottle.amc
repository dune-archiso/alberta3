# Note: the coordinate information below looks very funny, and in
# fact: this macro-triangulation CANNOT be used "as is": it has to be
# refined two times, the resulting mesh has to be re-converted into a
# macro triangulation, and only THEN it is possible to define a
# periodic structure on the refined macro-triangulation and use it for
# a geometry a la "3d Klein's bottle". This procedure is exercised in
# ellipt-klein-3-bottle.c
#
# Here we simply define a quite degenerated manifold, assign boundary
# types to the periodic boundaries, the rest has to be done by the
# application program -- i.e. by "ellipt-klein-3-bottle.c". See there.
#
# The non-orientable 3-manifold is generated from a 4d embedded
# 3-torus where the smallest generating circle is rotated by the angle
# of \pi through the additional 5th dimension.
#
# "ellipt-klein-3-bottle.c" benefits very much from the fact that
# ALBERTA is able to identify periodic twins by matching vertex
# coordinates; also: the neighbourhood information is generated
# automatically. The application just has to make sure that it
# assignes the correct parameterisation to each vertex.
#
# We use the following explicit parameterisation (parameter ranges are
# [0..2\pi]), constants are R_0 = 1, R_1 = 0.5, R_2 = 0.25 (radii).
#
# x_0 = cos(s) (R_0 + (R_1 + R_2 cos(u)) cos(t) sin(t/2))
# x_1 = sin(s) (R_0 + (R_1 + R_2 cos(u)) cos(t) sin(t/2))
# x_2 =               (R_1 + R_2 cos(u)) sin(t) sin(t/2))
# x_3 =                      R_2 sin(u)
# x_4 =                      R_2 cos(u) cos(t/2)
#

DIM:          3
DIM_OF_WORLD: 5

number of vertices: 30
number of elements: 48

#
# the notions "bottom layer" and "top layer are w.r.t the parameter
# space (largest circle "s"-direction, mid-circle "t"-direction,
# smallest circle "u"-direction (u=0 == bottom, u=2\pi == top))
#
vertex coordinates:
# "bottom" layer (u == 0, t == 0, s runs)
  1.0   0.0                  0.0 0.0  0.75
  0.0   1.53033008588991064  0.0 0.0  0.530330085889910644
 -1.75  0.0                  0.0 0.0  0.0
  0.0  -1.53033008588991064  0.0 0.0 -0.530330085889910644
  1.0   0.0                  0.0 0.0 -0.75
# "bottom" layer (u == 0, t == \pi, s runs)
  1.0   0.0                  0.0 0.0 -0.75
  0.0   0.469669914110089356 0.0 0.0 -0.530330085889910644
 -0.25  0.0                  0.0 0.0  0.0
  0.0  -0.469669914110089356 0.0 0.0  0.530330085889910644
  1.0   0.0                  0.0 0.0  0.75
# "bottom" layer (u == 0, t == 2\pi, s runs)
  1.0   0.0                  0.0 0.0  0.75
  0.0   1.53033008588991064  0.0 0.0  0.530330085889910644
 -1.75  0.0                  0.0 0.0  0.0
  0.0  -1.53033008588991064  0.0 0.0 -0.530330085889910644
  1.0   0.0                  0.0 0.0 -0.75
# "top" layer (u == 2\pi, t == 0, s runs)
  1.0   0.0 0.0 0.0                  0.75
  0.0   1.53033008588991064  0.0 0.0  0.530330085889910644
 -1.75  0.0                  0.0 0.0  0.0
  0.0  -1.53033008588991064  0.0 0.0 -0.530330085889910644
  1.0   0.0                  0.0 0.0 -0.75
# "top" layer (u == 2\pi, t == \pi, s runs)
  1.0   0.0                  0.0 0.0 -0.75
  0.0   0.469669914110089356 0.0 0.0 -0.530330085889910644
 -0.25  0.0                  0.0 0.0  0.0
  0.0  -0.469669914110089356 0.0 0.0  0.530330085889910644
  1.0   0.0                  0.0 0.0  0.75
# "top" layer (u == 2\pi, t == 2\pi, s runs)
  1.0   0.0                  0.0 0.0  0.75
  0.0   1.53033008588991064  0.0 0.0  0.530330085889910644
 -1.75  0.0                  0.0 0.0  0.0
  0.0  -1.53033008588991064  0.0 0.0 -0.530330085889910644
  1.0   0.0                  0.0 0.0 -0.75

element vertices:
# 1st cube
  0   21    6    1
  0   21   16    1
  0   21   16   15
  0   21    6    5
  0   21   20    5
  0   21   20   15  
# 2nd cube
  1   22    7    2
  1   22   17    2
  1   22   17   16
  1   22    7    6
  1   22   21    6
  1   22   21   16  
# 3rd cube
  2   23    8    3
  2   23   18    3
  2   23   18   17
  2   23    8    7
  2   23   22    7
  2   23   22   17  
# 4th cube
  3   24    9    4
  3   24   19    4
  3   24   19   18
  3   24    9    8
  3   24   23    8
  3   24   23   18  
# 5th cube
  5   26   11    6
  5   26   21    6
  5   26   21   20
  5   26   11   10
  5   26   25   10
  5   26   25   20  
# 6th cube
  6   27   12    7
  6   27   22    7
  6   27   22   21
  6   27   12   11
  6   27   26   11
  6   27   26   21  
# 7th cube
  7   28   13    8
  7   28   23    8
  7   28   23   22
  7   28   13   12
  7   28   27   12
  7   28   27   22  
# 8th cube
  8   29   14    9
  8   29   24    9
  8   29   24   23
  8   29   14   13
  8   29   28   13
  8   29   28   23  

# We assign boundary types 1, 2 and 3 to the boundaries across the
# long circle, the mid and the short circle, respectively.
element boundaries:
# 1st cube
  0  3  0  0
  0  2  0  0
 -3  2  0  0
  0  3  0  0
  0  1  0  0
 -3  1  0  0
# 2nd cube
  0  3  0  0
  0  2  0  0
 -3  2  0  0
  0  3  0  0
  0  0  0  0
 -3  0  0  0
# 3rd cube
  0  3  0  0
  0  2  0  0
 -3  2  0  0
  0  3  0  0
  0  0  0  0
 -3  0  0  0
# 4th cube
 -1  3  0  0
 -1  2  0  0
 -3  2  0  0
  0  3  0  0
  0  0  0  0
 -3  0  0  0
# 5th cube
  0  3  0  0
  0  0  0  0
 -3  0  0  0
 -2  3  0  0
 -2  1  0  0
 -3  1  0  0
# 6th cube
  0  3  0  0
  0  0  0  0
 -3  0  0  0
 -2  3  0  0
 -2  0  0  0
 -3  0  0  0
# 7th cube
  0  3  0  0
  0  0  0  0
 -3  0  0  0
 -2  3  0  0
 -2  0  0  0
 -3  0  0  0
# 8th cube
 -1  3  0  0
 -1  0  0  0
 -3  0  0  0
 -2  3  0  0
 -2  0  0  0
 -3  0  0  0

# (X)Emacs stuff (for editing purposes)
# Local Variables: ***
# comment-start: "# " ***
# End: ***
