SUBDIRS = book-demo

AM_CFLAGS = $(ALBERTA_OPTIMIZE_CFLAGS)

#
# Defines. @...@ is substituted by running configure.
#
DEFS = -DHAVE_CONFIG_H

#
# Include paths.
#
AM_CPPFLAGS = -I$(top_srcdir) \
              $(ALBERTA_ALL_INCLUDES) \
              @OBSTACK_CPPFLAGS@ \
              $(EXTRA_INC)

# define tests for "make check"
check_PROGRAMS = read_mesh_test1d \
                 read_mesh_test2d \
                 read_mesh_test3d

if BUILD_DEBUG_LIBS
check_PROGRAMS += read_mesh_test1d_debug \
                  read_mesh_test2d_debug \
                  read_mesh_test3d_debug
endif

TESTS = $(check_PROGRAMS)
# because the grid file "INIT/read_mesh_test.dat" is missing, all tests fail
XFAIL_TESTS = $(check_PROGRAMS)


sources = \
 read_mesh_test.c \
 alberta-demo.h \
 cmdline.c \
 graphics.c \
 graphics.h 

#################################################################
# 1d                                                            #
#################################################################

read_mesh_test1d_SOURCES = $(sources)
if ALBERTA_USE_GRAPHICS
read_mesh_test1d_LDADD = \
 $(ALBERTA_GFX_LIBS_1)\
 @GLTOOLS_LIBS@ @OPENDX_ALL_LIBS@ @OPENGL_LIBS@ @X_ALL_LIBS@\
 $(ALBERTA_LIBS_1)\
 @LIBS@ @DYLOADER_LIBS@
else
read_mesh_test1d_LDADD = \
 $(ALBERTA_LIBS_1)\
 @LIBS@ @DYLOADER_LIBS@
endif
read_mesh_test1d_DEPENDENCIES = #
read_mesh_test1d_CPPFLAGS = $(AM_CPPFLAGS) -DALBERTA_DEBUG=0 -DDIM_OF_WORLD=1
read_mesh_test1d_CFLAGS = $(ALBERTA_OPTIMIZE_CFLAGS)

if BUILD_DEBUG_LIBS
read_mesh_test1d_debug_SOURCES = $(sources)
if ALBERTA_USE_GRAPHICS
read_mesh_test1d_debug_LDADD =\
 $(ALBERTA_DEBUG_GFX_LIBS_1)\
 @GLTOOLS_LIBS@ @OPENDX_ALL_LIBS@ @OPENGL_LIBS@ @X_ALL_LIBS@\
 $(ALBERTA_DEBUG_LIBS_1)\
 @LIBS@ @DYLOADER_LIBS@
else
read_mesh_test1d_debug_LDADD =\
 $(ALBERTA_DEBUG_LIBS_1)\
 @LIBS@ @DYLOADER_LIBS@
endif
read_mesh_test1d_debug_DEPENDENCIES = # otherwise automake emits a dependency 
# on $(ALBERTA_LIBS_1)
read_mesh_test1d_debug_CPPFLAGS = $(AM_CPPFLAGS) -DALBERTA_DEBUG=1 -DDIM_OF_WORLD=1
read_mesh_test1d_debug_CFLAGS = $(ALBERTA_DEBUG_CFLAGS)
endif

#################################################################
# 2d                                                            #
#################################################################

read_mesh_test2d_SOURCES = $(sources)
if ALBERTA_USE_GRAPHICS
read_mesh_test2d_LDADD = \
 $(ALBERTA_GFX_LIBS_2)\
 @GLTOOLS_LIBS@ @OPENDX_ALL_LIBS@ @OPENGL_LIBS@ @X_ALL_LIBS@\
 $(ALBERTA_LIBS_2)\
 @LIBS@ @DYLOADER_LIBS@
else
read_mesh_test2d_LDADD = \
 $(ALBERTA_LIBS_2)\
 @LIBS@ @DYLOADER_LIBS@
endif
read_mesh_test2d_DEPENDENCIES = #
read_mesh_test2d_CPPFLAGS = $(AM_CPPFLAGS) -DALBERTA_DEBUG=0 -DDIM_OF_WORLD=2
read_mesh_test2d_CFLAGS = $(ALBERTA_OPTIMIZE_CFLAGS)

if BUILD_DEBUG_LIBS
read_mesh_test2d_debug_SOURCES = $(sources)
if ALBERTA_USE_GRAPHICS
read_mesh_test2d_debug_LDADD =\
 $(ALBERTA_DEBUG_GFX_LIBS_2)\
 @GLTOOLS_LIBS@ @OPENDX_ALL_LIBS@ @OPENGL_LIBS@ @X_ALL_LIBS@\
 $(ALBERTA_DEBUG_LIBS_2)\
 @LIBS@ @DYLOADER_LIBS@
else
read_mesh_test2d_debug_LDADD =\
 $(ALBERTA_DEBUG_LIBS_2)\
 @LIBS@ @DYLOADER_LIBS@
endif
read_mesh_test2d_debug_DEPENDENCIES = # otherwise automake emits a dependency 
# on $(ALBERTA_LIBS_2)
read_mesh_test2d_debug_CPPFLAGS = $(AM_CPPFLAGS) -DALBERTA_DEBUG=1 -DDIM_OF_WORLD=2
read_mesh_test2d_debug_CFLAGS = $(ALBERTA_DEBUG_CFLAGS)
endif

#################################################################
# 3d                                                            #
#################################################################

read_mesh_test3d_SOURCES = $(sources)
if ALBERTA_USE_GRAPHICS
read_mesh_test3d_LDADD = \
 $(ALBERTA_GFX_LIBS_3)\
 @GLTOOLS_LIBS@ @OPENDX_ALL_LIBS@ @OPENGL_LIBS@ @X_ALL_LIBS@\
 $(ALBERTA_LIBS_3)\
 @LIBS@ @DYLOADER_LIBS@
else
read_mesh_test3d_LDADD = \
 $(ALBERTA_LIBS_3)\
 @LIBS@ @DYLOADER_LIBS@
endif
read_mesh_test3d_DEPENDENCIES = #
read_mesh_test3d_CPPFLAGS = $(AM_CPPFLAGS) -DALBERTA_DEBUG=0 -DDIM_OF_WORLD=3
read_mesh_test3d_CFLAGS = $(ALBERTA_OPTIMIZE_CFLAGS)

if BUILD_DEBUG_LIBS
read_mesh_test3d_debug_SOURCES = $(sources)
if ALBERTA_USE_GRAPHICS
read_mesh_test3d_debug_LDADD =\
 $(ALBERTA_DEBUG_GFX_LIBS_3)\
 @GLTOOLS_LIBS@ @OPENDX_ALL_LIBS@ @OPENGL_LIBS@ @X_ALL_LIBS@\
 $(ALBERTA_DEBUG_LIBS_3)\
 @LIBS@ @DYLOADER_LIBS@
else
read_mesh_test3d_debug_LDADD =\
 $(ALBERTA_DEBUG_LIBS_3)\
 @LIBS@ @DYLOADER_LIBS@
endif
read_mesh_test3d_debug_DEPENDENCIES = # otherwise automake emits a dependency 
# on $(ALBERTA_LIBS_3)
read_mesh_test3d_debug_CPPFLAGS = $(AM_CPPFLAGS) -DALBERTA_DEBUG=1 -DDIM_OF_WORLD=3
read_mesh_test3d_debug_CFLAGS = $(ALBERTA_DEBUG_CFLAGS)
endif
