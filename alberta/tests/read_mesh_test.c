/*******************************************************************************
 *
 * ALBERTA:  an Adaptive multi Level finite element toolbox  using
 *           Bisectioning refinement and Error control by Residual
 *           Techniques
 *
 * www.alberta-fem.de
 *
 *
 * file:     ellipt.c
 *
 * description:  solver for an elliptic model problem
 *
 *                        -\Delta u = f  in \Omega
 *                                u = g  on \partial \Omega
 *
 ******************************************************************************
 *
 *  authors:   Alfred Schmidt
 *             Zentrum fuer Technomathematik
 *             Fachbereich 3 Mathematik/Informatik
 *             Universitaet Bremen
 *             Bibliothekstr. 2
 *             D-28359 Bremen, Germany
 *
 *             Kunibert G. Siebert
 *             Institut fuer Mathematik
 *             Universitaet Augsburg
 *             Universitaetsstr. 14
 *             D-86159 Augsburg, Germany
 *
 *  (c) by A. Schmidt and K.G. Siebert (1996-2003)
 *
 ******************************************************************************/

#include "alberta-demo.h"

/*******************************************************************************
 * main program
 ******************************************************************************/

int main(int argc, char *argv[])
{
  FUNCNAME("main");
  MESH           *mesh;
  const char     filename[PATH_MAX];
  REAL time;

  parse_parameters(argc, argv, "INIT/read_mesh_test.dat"); // (for graphics)
  GET_PARAMETER(1, "mesh file name", "%s", filename);

  time = 0;
  MSG("reading mesh `%s'\n", filename /*argv[1]*/);
  mesh = read_mesh_xdr(filename /*argv[1]*/, &time, NULL, NULL);

  MSG("refining one time\n");
  global_refine(mesh, 1, FILL_NOTHING);

#if DIM_OF_WORLD <= 3  
  MSG("done. Attempt to display result:\n");
  MSG("Displaying the mesh.\n");
  graphics(mesh, NULL /* u_h */, NULL /* get_est()*/ , NULL /* u_exact() */,
	   /*HUGE_VAL*/ time);
#elif DIM_OF_WORLD == 3
  MSG("done. Attempt to write the stuff to disk.\n");
  write_mesh_xdr(mesh, "3dtest"/*filename*/, time); // write mesh for paraview
#else
# error Only for DIM_OF_WORLD <= 3
#endif
  
  return 0;
}
