/*--------------------------------------------------------------------------*/
/* ALBERTA:  an Adaptive multi Level finite element toolbox using           */
/*           Bisectioning refinement and Error control by Residual          */
/*           Techniques for scientific Applications                         */
/*                                                                          */
/* file:     element_0d.c                                                   */
/*                                                                          */
/*                                                                          */
/* description:  routines on elements that depend on the dimension in 1d    */
/*                                                                          */
/*--------------------------------------------------------------------------*/
/*                                                                          */
/*  authors:   Alfred Schmidt                                               */
/*             Zentrum fuer Technomathematik                                */
/*             Fachbereich 3 Mathematik/Informatik                          */
/*             Universitaet Bremen                                          */
/*             Bibliothekstr. 2                                             */
/*             D-28359 Bremen, Germany                                      */
/*                                                                          */
/*             Kunibert G. Siebert                                          */
/*             Institut fuer Mathematik                                     */
/*             Universitaet Augsburg                                        */
/*             Universitaetsstr. 14                                         */
/*             D-86159 Augsburg, Germany                                    */
/*                                                                          */
/*  http://www.mathematik.uni-freiburg.de/IAM/ALBERTA                       */
/*                                                                          */
/*  (c) by A. Schmidt and K.G. Siebert (1996-2003)                          */
/*                                                                          */
/*--------------------------------------------------------------------------*/

#ifdef HAVE_CONFIG_H
# include "config.h" /* probably a good idea to pull this one in here ... */
#endif

#include "alberta.h"

/****************************************************************************/
/* Some trivial 0d stuff.                                                   */
/****************************************************************************/

int world_to_coord_0d(const EL_INFO *el_info,
		      const REAL *x, REAL_B lambda)
{
  FUNCNAME("world_to_coord_0d");
  int i;

  lambda[0] = 1.0;
  for (i = 1; i < N_LAMBDA_MAX; i++) {
    lambda[i] = 0.0;
  }

  return -1;
}

const REAL *coord_to_world_0d(const EL_INFO *el_info, const REAL *l, REAL_D w)
{
  FUNCNAME("coord_to_world_0d");
  static REAL world[DIM_OF_WORLD];
  REAL        *ret;
  int         i;

  DEBUG_TEST_EXIT((el_info->fill_flag & FILL_COORDS) ||
		  !el_info->mesh->parametric ||
		  el_info->mesh->parametric->use_reference_mesh,
		  "You must enable the use_reference_mesh entry in the "
		  "PARAMETRIC structure to use this function on the "
		  "reference mesh. Use parametric->coord_to_world() "
		  "to access the parametric mesh\n");

  ret = w ? w : world;

  for(i = 0; i < DIM_OF_WORLD; i++)
    ret[i] = el_info->coord[0][i];

  return (const REAL *)ret;
}

REAL el_grd_lambda_0d(const EL_INFO *el_info, REAL_BD grd_lam)
{
  SET_DOW(0.0, grd_lam[0]);

  return 1.0;
}

REAL el_det_0d(const EL_INFO *el_info)
{
    return 1.0;
}

REAL el_volume_0d(const EL_INFO *el_info)
{
    return 1.0;
}

REAL get_wall_normal_0d(const EL_INFO *el_info, int i0, REAL_D normal)
{
  FUNCNAME("get_face_normal_0d");

  WARNING("Does not makes sense for dim == 0!\n");

  return HUGE_VAL;
}

int wall_orientation_0d(const EL *el, int wall)
{
  FUNCNAME("wall_orientation");

  WARNING("Does not makes sense for dim == 0!\n");

  return -1;
}

int wall_rel_orientation_0d(const EL *el, const EL *neigh, int wall, int ov)
{
  FUNCNAME("wall_orientation_rel_0d");

  WARNING("Does not makes sense for dim == 0!\n");

  return -1;
}

