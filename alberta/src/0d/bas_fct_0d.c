/*--------------------------------------------------------------------*/
/*--- ALBERTA:  an Adaptive multi Level finite element toolbox using -*/
/*---           Bisectioning refinement and Error control by Residual */
/*---           Techniques for scientific Applications             ---*/
/*---                                                              ---*/
/*--- file: bas_fct_0d.c                                           ---*/
/*---                                                              ---*/
/*--- description: implementation of trivial basis functions in 0d ---*/
/*---              "Honi soit qui mal y pense."                    ---*/
/*---                                                              ---*/
/*--------------------------------------------------------------------*/
/*---                                                              ---*/
/*--- author:  Daniel Koester                                      ---*/
/*---          Institut fuer Mathematik                            ---*/
/*---          Universitaet Augsburg                               ---*/
/*---          Universitaetsstr. 14                                ---*/
/*---          D-86159 Augsburg, Germany                           ---*/
/*---                                                              ---*/
/*---                                                              ---*/
/*--- http://www.mathematik.uni-freiburg.de/IAM/ALBERTA            ---*/
/*---                                                              ---*/
/*--- (c) by D. Koester (2004)                                     ---*/
/*---                                                              ---*/
/*--------------------------------------------------------------------*/

#define N_BAS_LAG_0D 1

static const REAL_B bary_0d[N_BAS_LAG_0D] = { INIT_BARY_0D(1.0) };

static LAGRANGE_DATA lag_0d_data = {
  bary_0d,
  NULL /* lumping_quad */,
};

/*--------------------------------------------------------------------------*/
/*---  basisfunction located at vertex 0                                 ---*/
/*--------------------------------------------------------------------------*/

static REAL phi_1_0d(const REAL_B lambda, const BAS_FCTS *thisptr)/* !!! ...(lambda, -) */
{
  return(1.0);
}

static const REAL *grd_phi_1_0d(const REAL_B lambda, const BAS_FCTS *thisptr)
{
  static const REAL_B grd = {0};

  return(grd);
}

static const REAL_B *D2_phi_1_0d(const REAL_B lambda, const BAS_FCTS *thisptr)
{
  static const REAL_BB D2 = {{0}};

  return(D2);
}

#undef DEF_EL_VEC_0D
#define DEF_EL_VEC_0D(type, name)				\
  DEF_EL_VEC_CONST(type, name, N_BAS_LAG_0D, N_BAS_LAG_0D)

#define DEFUN_GET_EL_VEC_0D(name, type, el, admin, body, ...)		\
static const EL_##type##_VEC *						\
get_##name##_0d(type##_VEC_TYPE *vec, __VA_ARGS__)			\
{									\
  FUNCNAME("get_"#name"_0d");						\
  static DEF_EL_VEC_0D(type, rvec_space);				\
  type##_VEC_TYPE *rvec = vec ? vec : rvec_space->vec;			\
  int n0, node;								\
  DOF **dof = (el)->dof;						\
									\
  DEBUG_TEST_EXIT(true, "");						\
									\
/*--------------------------------------------------------------------*/ \
/*--- DOFs at vertices                                             ---*/ \
/*--------------------------------------------------------------------*/ \
									\
  node = admin->mesh->node[VERTEX];					\
  n0   = admin->n0_dof[VERTEX];						\
									\
  body;									\
									\
  return vec ? NULL : rvec_space;					\
}									\
struct _AI_semicolon_dummy

/*--------------------------------------------------------------------*/
/*--- function for accessing local DOFs on an element              ---*/
/*--------------------------------------------------------------------*/

DEFUN_GET_EL_VEC_0D(dof_indices, DOF, el, admin,
		    { rvec[0] = dof[node][n0]; },
		    const EL *el, const DOF_ADMIN *admin,
		    const BAS_FCTS *thisptr);

/*--------------------------------------------------------------------*/
/*--- function for accessing boundary type of DOFs                 ---*/
/*--------------------------------------------------------------------*/

static const EL_BNDRY_VEC *
get_bound_0d(BNDRY_FLAGS *vec,
	     const EL_INFO *el_info, const BAS_FCTS *thisptr)
{
  static DEF_EL_VEC_0D(BNDRY, rvec_space);
  BNDRY_FLAGS *rvec = vec ? vec : rvec_space->vec;
  
  BNDRY_FLAGS_ALL(rvec[0]);

  return vec ? NULL : rvec_space;
}

/*--------------------------------------------------------------------*/
/*--- function for local interpolaton of scalar functions          ---*/
/*--------------------------------------------------------------------*/

#define bary0_0d      bary_0d
#define lag_0_0d_data lag_0d_data
GENERATE_INTERPOL(/**/, 0, 0, N_BAS_LAG_0D);

/*--------------------------------------------------------------------*/
/*--- function for local interpolaton of vector functions          ---*/
/*--------------------------------------------------------------------*/

GENERATE_INTERPOL_D(/**/, 0, 0, N_BAS_LAG_0D);

GENERATE_INTERPOL_DOW(/**/, 0, 0, N_BAS_LAG_0D);

/*--------------------------------------------------------------------*/
/*--- function for accessing a local DOF_INT_VEC                   ---*/
/*--------------------------------------------------------------------*/

DEFUN_GET_EL_VEC_0D(int_vec, INT, el, dv->fe_space->admin,
		    rvec[0] = dv->vec[dof[node][n0]],
		    const EL *el, const DOF_INT_VEC *dv);

/*--------------------------------------------------------------------*/
/*--- function for accessing a local DOF_REAL_VEC                  ---*/
/*--------------------------------------------------------------------*/

DEFUN_GET_EL_VEC_0D(real_vec, REAL, el, dv->fe_space->admin,
		    rvec[0] = dv->vec[dof[node][n0]],
		    const EL *el, const DOF_REAL_VEC *dv);

/*--------------------------------------------------------------------*/
/*--- function for accessing a local DOF_REAL_D_VEC                ---*/
/*--------------------------------------------------------------------*/

DEFUN_GET_EL_VEC_0D(real_d_vec, REAL_D, el, dv->fe_space->admin,
		    COPY_DOW(dv->vec[dof[node][n0]], rvec[0]),
		    const EL *el, const DOF_REAL_D_VEC *dv);

/*--------------------------------------------------------------------*/
/*--- function for accessing a local DOF_SCHAR_VEC                 ---*/
/*--------------------------------------------------------------------*/

DEFUN_GET_EL_VEC_0D(schar_vec, SCHAR, el, dv->fe_space->admin,
		    rvec[0] = dv->vec[dof[node][n0]],
		    const EL *el, const DOF_SCHAR_VEC *dv);

/*--------------------------------------------------------------------*/
/*--- function for accessing a local DOF_UCHAR_VEC                 ---*/
/*--------------------------------------------------------------------*/

DEFUN_GET_EL_VEC_0D(uchar_vec, UCHAR, el, dv->fe_space->admin,
		    rvec[0] = dv->vec[dof[node][n0]],
		    const EL *el, const DOF_UCHAR_VEC *dv);

/*--------------------------------------------------------------------*/
/*--- function for accessing a local DOF_PTR_VEC                   ---*/
/*--------------------------------------------------------------------*/

DEFUN_GET_EL_VEC_0D(ptr_vec, PTR, el, dv->fe_space->admin,
		    rvec[0] = dv->vec[dof[node][n0]],
		    const EL *el, const DOF_PTR_VEC *dv);

/*--------------------------------------------------------------------*/
/*--- function for accessing a local DOF_REAL_DD_VEC               ---*/
/*--------------------------------------------------------------------*/

DEFUN_GET_EL_VEC_0D(real_dd_vec, REAL_DD, el, dv->fe_space->admin,
		    MCOPY_DOW((const REAL_D *)dv->vec[dof[node][n0]], rvec[0]),
		    const EL *el, const DOF_REAL_DD_VEC *dv);


/*--------------------------------------------------------------------*/
/*--- Collect all information about basis functions                ---*/
/*--------------------------------------------------------------------*/

static const BAS_FCT     phi_0d[N_BAS_LAG_0D]     = {phi_1_0d};
static const GRD_BAS_FCT grd_phi_0d[N_BAS_LAG_0D] = {grd_phi_1_0d};
static const D2_BAS_FCT  D2_phi_0d[N_BAS_LAG_0D]  = {D2_phi_1_0d};

static const BAS_FCTS lagrange_0d =
{
  "lagrange_0d", 0, 1, N_BAS_LAG_0D, N_BAS_LAG_0D, 1,
  {1, 0, 0, 0},    /* VERTEX, CENTER, EDGE, FACE   */
  -1, /* trace_admin */
  INIT_BFCTS_CHAIN(lagrange_0d),
  INIT_ELEMENT_INITIALIZER(NULL, FILL_NOTHING), /* init_element + fill-flags */
  phi_0d, grd_phi_0d, D2_phi_0d,
  NULL, NULL, /* thrid and fourth derivatives */
  NULL, NULL, NULL, false, /* phi_d etc. */  
  /********************/
  NULL,                /* trace space */
  { { { NULL, }, }, }, /* trace_dof_map */
  { -1, },             /* n_trace_bas_fcts */
  /********************/
  get_dof_indices_0d,
  get_bound_0d,
  interpol0_0d,
  interpol_d_0_0d,
  interpol_dow_0_0d,
  get_int_vec_0d,
  get_real_vec_0d,
  get_real_d_vec_0d,
  (GET_REAL_VEC_D_TYPE)get_real_d_vec_0d,
  get_uchar_vec_0d,
  get_schar_vec_0d,
  get_ptr_vec_0d,
  get_real_dd_vec_0d,
  NULL,
  NULL,
  NULL,
  NULL,
  NULL,
  NULL,
  NULL,
  NULL,
  NULL,
  (void *)&lag_0d_data
};
