/*--------------------------------------------------------------------*/
/*--- ALBERTA:  an Adaptive multi Level finite element toolbox using -*/
/*---           Bisectioning refinement and Error control by Residual */
/*---           Techniques for scientific Applications             ---*/
/*---                                                              ---*/
/*--- file: lagrange_1_1d.c                                        ---*/
/*---                                                              ---*/
/*--- description: implementation of the basis functions           ---*/
/*---              lagrange1 in 1d                                 ---*/
/*---                                                              ---*/
/*--- created by: kgs on host enigma                               ---*/
/*---           at 17:19 on 28 of March 2003                       ---*/
/*--------------------------------------------------------------------*/
/*---                                                              ---*/
/*--- authors:   Alfred Schmidt                                    ---*/
/*---            Zentrum fuer Technomathematik                     ---*/
/*---            Fachbereich 3 Mathematik/Informatik               ---*/
/*---            Universitaet Bremen                               ---*/
/*---            Bibliothekstr. 2                                  ---*/
/*---            D-28359 Bremen, Germany                           ---*/
/*---                                                              ---*/
/*---            Kunibert G. Siebert                               ---*/
/*---            Institut fuer Mathematik                          ---*/
/*---            Universitaet Augsburg                             ---*/
/*---            Universitaetsstr. 14                              ---*/
/*---            D-86159 Augsburg, Germany                         ---*/
/*---                                                              ---*/
/*--- http://www.mathematik.uni-freiburg.de/IAM/ALBERTA            ---*/
/*---                                                              ---*/
/*--- (c) by A. Schmidt and K.G. Siebert (1996-2003)               ---*/
/*---                                                              ---*/
/*--------------------------------------------------------------------*/

static const REAL_B bary1_1d[N_BAS_LAG_1_1D] = {INIT_BARY_1D(1.0, 0.0),
						INIT_BARY_1D(0.0, 1.0)};

static LAGRANGE_DATA lag_1_1d_data = {
  bary1_1d,
  NULL /* lumping_quad */,
};

/*--------------------------------------------------------------------------*/
/*---  basisfunction 0 located at vertex 0                               ---*/
/*--------------------------------------------------------------------------*/

static REAL phi1_0_1d(const REAL_B lambda, const BAS_FCTS *thisptr)
{
  return(lambda[0]);
}

static const REAL *grd_phi1_0_1d(const REAL_B lambda, const BAS_FCTS *thisptr)
{
  static const REAL_B grd = INIT_BARY_1D(1.0, 0.0);

  return(grd);
}

static const REAL_B *D2_phi1_0_1d(const REAL *lambda, const BAS_FCTS *thisptr)
{
  static const REAL_BB D2 = {{0.0}};

  return(D2);
}

/*--------------------------------------------------------------------------*/
/*---  basisfunction 1 located at vertex 1                               ---*/
/*--------------------------------------------------------------------------*/

static REAL phi1_1_1d(const REAL_B lambda, const BAS_FCTS *thisptr)
{
  return(lambda[1]);
}

static const REAL *grd_phi1_1_1d(const REAL_B lambda, const BAS_FCTS *thisptr)
{
  static const REAL_B grd = INIT_BARY_1D(0.0, 1.0);

  return(grd);
}

static const REAL_B *D2_phi1_1_1d(const REAL *lambda, const BAS_FCTS *thisptr)
{
  static const REAL_BB D2 = {{0.0}};

  return(D2);
}

#undef DEFUN_GET_EL_VEC_1_1D
#define DEFUN_GET_EL_VEC_1_1D(name, type, admin, body, ...)		\
static const EL_##type##_VEC *						\
get_##name##1_1d(type##_VEC_TYPE * vec, const EL *el, __VA_ARGS__)	\
{									\
  FUNCNAME("get_"#name"_0d");						\
  static DEF_EL_VEC_CONST(type, rvec_space, N_BAS_LAG_1_1D, N_BAS_LAG_1_1D); \
  type##_VEC_TYPE *rvec = vec ? vec : rvec_space->vec;			\
  int n0, node;								\
  DOF **dof = el->dof;							\
									\
  DEBUG_TEST_EXIT(true, "");						\
									\
/*--------------------------------------------------------------------*/ \
/*--- DOFs at vertices                                             ---*/ \
/*--------------------------------------------------------------------*/ \
									\
  node = (admin)->mesh->node[VERTEX];					\
  n0   = (admin)->n0_dof[VERTEX];					\
									\
  body;									\
									\
  return vec ? NULL : rvec_space;					\
}									\
struct _AI_semicolon_dummy

#undef DEFUN_GET_DOF_VEC_1_1D
#define DEFUN_GET_EL_DOF_VEC_1_1D(name, type, ASSIGN)			\
static const EL_##type##_VEC *						\
get_##name##1_1d(type##_VEC_TYPE *vec, const EL *el,			\
		 const DOF_##type##_VEC *dv)				\
{									\
  FUNCNAME("get_"#name"_0d");						\
  static DEF_EL_VEC_CONST(type, rvec_space, N_BAS_LAG_1_1D, N_BAS_LAG_1_1D); \
  EL_##type##_VEC *el_vec = dv->vec_loc ? dv->vec_loc : rvec_space;	\
  type##_VEC_TYPE *rvec = vec ? vec : el_vec->vec;			\
  int n0, node;								\
  DOF **dof = el->dof;							\
  int i;								\
  									\
									\
  DEBUG_TEST_EXIT(true, "");						\
									\
/*--------------------------------------------------------------------*/ \
/*--- DOFs at vertices                                             ---*/ \
/*--------------------------------------------------------------------*/ \
									\
  node = dv->fe_space->admin->mesh->node[VERTEX];			\
  n0   = dv->fe_space->admin->n0_dof[VERTEX];				\
  									\
  for (i = 0; i < N_VERTICES_1D; i++) {					\
    ASSIGN(dv->vec[dof[node+i][n0]], rvec[i]);				\
  }									\
									\
  return vec ? NULL : el_vec;						\
}									\
struct _AI_semicolon_dummy


#undef COPY_EQ
#define COPY_EQ(a, b) (b) = (a)

/*--------------------------------------------------------------------*/
/*--- function for accessing local DOFs on an element              ---*/
/*--------------------------------------------------------------------*/

DEFUN_GET_EL_VEC_1_1D(dof_indices, DOF, admin, {
    int i;
    
    for (i = 0; i < N_VERTICES_1D; i++) {
      rvec[i] = dof[node+i][n0];
    }
  },
  const DOF_ADMIN *admin, const BAS_FCTS *thisptr);

/*--------------------------------------------------------------------*/
/*--- function for accessing boundary type of DOFs                 ---*/
/*--------------------------------------------------------------------*/

static const EL_BNDRY_VEC *get_bound1_1d(BNDRY_FLAGS *vec,
					 const EL_INFO *el_info,
					 const BAS_FCTS *thisptr)
{
  FUNCNAME("get_bound1_1d");
  static DEF_EL_VEC_CONST(BNDRY, rvec_space, N_BAS_LAG_0D, N_BAS_LAG_0D);
  BNDRY_FLAGS *rvec = vec ? vec : rvec_space->vec;
  int            i;

  DEBUG_TEST_FLAG(FILL_BOUND, el_info);

/*--------------------------------------------------------------------*/
/*--- basis functions at vertices                                  ---*/
/*--------------------------------------------------------------------*/

  for (i = 0; i < N_VERTICES_1D; i++) {
    BNDRY_FLAGS_CPY(rvec[i], el_info->vertex_bound[i]);
  }

  return vec ? NULL : rvec_space;
}

/*--------------------------------------------------------------------*/
/*--- function for local interpolaton of scalar functions          ---*/
/*--------------------------------------------------------------------*/
 
GENERATE_INTERPOL(/**/, 1, 1, N_BAS_LAG_1_1D);

/*--------------------------------------------------------------------*/
/*--- function for local interpolaton of vector functions          ---*/
/*--------------------------------------------------------------------*/
 
GENERATE_INTERPOL_D(/**/, 1, 1, N_BAS_LAG_1_1D);

GENERATE_INTERPOL_DOW(/**/, 1, 1, N_BAS_LAG_1_1D);

/*--------------------------------------------------------------------*/
/*--- function for accessing a local DOF_INT_VEC                   ---*/
/*--------------------------------------------------------------------*/

DEFUN_GET_EL_DOF_VEC_1_1D(int_vec, INT, COPY_EQ);

/*--------------------------------------------------------------------*/
/*--- function for accessing a local DOF_REAL_VEC                  ---*/
/*--------------------------------------------------------------------*/

DEFUN_GET_EL_DOF_VEC_1_1D(real_vec, REAL, COPY_EQ);

/*--------------------------------------------------------------------*/
/*--- function for accessing a local DOF_REAL_D_VEC                ---*/
/*--------------------------------------------------------------------*/

DEFUN_GET_EL_DOF_VEC_1_1D(real_d_vec, REAL_D, COPY_DOW);

/*--------------------------------------------------------------------*/
/*--- function for accessing a local DOF_SCHAR_VEC                 ---*/
/*--------------------------------------------------------------------*/

DEFUN_GET_EL_DOF_VEC_1_1D(schar_vec, SCHAR, COPY_EQ);

/*--------------------------------------------------------------------*/
/*--- function for accessing a local DOF_UCHAR_VEC                 ---*/
/*--------------------------------------------------------------------*/

DEFUN_GET_EL_DOF_VEC_1_1D(uchar_vec, UCHAR, COPY_EQ);

/*--------------------------------------------------------------------*/
/*--- function for accessing a local DOF_PTR_VEC                   ---*/
/*--------------------------------------------------------------------*/

DEFUN_GET_EL_DOF_VEC_1_1D(ptr_vec, PTR, COPY_EQ);

/*--------------------------------------------------------------------*/
/*--- function for accessing a local DOF_REAL_DD_VEC               ---*/
/*--------------------------------------------------------------------*/

#define MYMCOPY(a, b) MCOPY_DOW((const REAL_D *)(a), (b))

DEFUN_GET_EL_DOF_VEC_1_1D(real_dd_vec, REAL_DD, MYMCOPY);

/*--------------------------------------------------------------------*/
/*--- function for interpolaton DOF_REAL_VECs during refinement    ---*/
/*--------------------------------------------------------------------*/

static void real_refine_inter1_1d(DOF_REAL_VEC *dv, RC_LIST_EL *list, int n_el)
{
  EL                *el;
  DOF               cdof, pdof0, pdof1;
  const DOF_ADMIN   *admin = dv->fe_space->admin;
  int               n0;

  n0 = admin->n0_dof[VERTEX];
  el = list->el_info.el;

  pdof0 = el->dof[0][n0];
  pdof1 = el->dof[1][n0];
  cdof  = el->child[0]->dof[1][n0];

  dv->vec[cdof] = (0.5*dv->vec[pdof0] + 0.5*dv->vec[pdof1]);

  return;
}

/*--------------------------------------------------------------------*/
/*--- function for interpolaton DOF_REAL_D_VECs during refinement  ---*/
/*--------------------------------------------------------------------*/

static void
real_d_refine_inter1_1d(DOF_REAL_D_VEC *dv, RC_LIST_EL *list, int n_el)
{
  EL              *el;
  DOF             cdof, pdof0, pdof1;
  const DOF_ADMIN *admin = dv->fe_space->admin;
  int             n0;

  n0 = admin->n0_dof[VERTEX];
  el = list->el_info.el;

  pdof0 = el->dof[0][n0];
  pdof1 = el->dof[1][n0];
  cdof  = el->child[0]->dof[1][n0];

  AXPBY_DOW(0.5, dv->vec[pdof0], 0.5, dv->vec[pdof1], dv->vec[cdof]);
}

/*--------------------------------------------------------------------*/
/*--- function for interpolaton DOF_REAL_VECs during coarsening    ---*/
/*--------------------------------------------------------------------*/

static void real_coarse_restr1_1d(DOF_REAL_VEC *dv, RC_LIST_EL *list, int n_el)
{
  EL              *el;
  DOF             cdof, pdof0, pdof1;
  const DOF_ADMIN *admin = dv->fe_space->admin;
  int             n0;

  n0 = admin->n0_dof[VERTEX];
  el = list->el_info.el;

  pdof0 = el->dof[0][n0];
  pdof1 = el->dof[1][n0];
  cdof  = el->child[0]->dof[1][n0];

  dv->vec[pdof0] += 0.5*dv->vec[cdof];
  dv->vec[pdof1] += 0.5*dv->vec[cdof];
}

/*--------------------------------------------------------------------*/
/*--- function for interpolaton DOF_REAL_D_VECs during coarsening  ---*/
/*--------------------------------------------------------------------*/

static void
real_d_coarse_restr1_1d(DOF_REAL_D_VEC *dv, RC_LIST_EL *list, int n_el)
{
  EL              *el;
  DOF             cdof, pdof0, pdof1;
  const DOF_ADMIN *admin = dv->fe_space->admin;
  int             n0;

  n0 = admin->n0_dof[VERTEX];
  el = list->el_info.el;

  pdof0 = el->dof[0][n0];
  pdof1 = el->dof[1][n0];
  cdof  = el->child[0]->dof[1][n0];

  AXPY_DOW(0.5, dv->vec[cdof], dv->vec[pdof0]);
  AXPY_DOW(0.5, dv->vec[cdof], dv->vec[pdof1]);
}

/*--------------------------------------------------------------------*/
/*--- Collect all information about basis functions                ---*/
/*--------------------------------------------------------------------*/

static const BAS_FCT phi1_1d[N_BAS_LAG_1_1D] =
{
  phi1_0_1d, phi1_1_1d
};

static const GRD_BAS_FCT grd_phi1_1d[N_BAS_LAG_1_1D] =
{
  grd_phi1_0_1d, grd_phi1_1_1d
};
static const D2_BAS_FCT D2_phi1_1d[N_BAS_LAG_1_1D] =
{
  D2_phi1_0_1d, D2_phi1_1_1d
};

/* For each wall the mapping from the wall basis functions to the
 * local DOFs on the reference element.
 */
static const int trace_mapping_lag_1_1d[N_WALLS_1D][N_BAS_LAG_0D] = {
  { 1 }, { 0 }
};

static const BAS_FCTS lagrange1_1d =
{
  "lagrange1_1d", 1, 1, N_BAS_LAG_1_1D, N_BAS_LAG_1_1D, 1,
  {1, 0, 0, 0},    /* VERTEX, CENTER, EDGE, FACE   */
  -1, /* trace_admin */
  INIT_BFCTS_CHAIN(lagrange1_1d),
  INIT_ELEMENT_INITIALIZER(NULL, FILL_NOTHING), /* init_element + fill-flags */
  phi1_1d, grd_phi1_1d, D2_phi1_1d,
  NULL, NULL, /* third and fourth derivatives */
  NULL, NULL, NULL, false, /* phi_d etc. */  
  /********************/
  &lagrange_0d,                             /* trace space */
  { { { trace_mapping_lag_1_1d[0],
	trace_mapping_lag_1_1d[1], }, }, }, /* trace mapping */
  { N_BAS_LAG_0D, N_BAS_LAG_0D, },          /* n_trace_bas_fcts */
  get_dof_indices1_1d,
  get_bound1_1d,
  interpol1_1d,
  interpol_d_1_1d,
  interpol_dow_1_1d,
  get_int_vec1_1d,
  get_real_vec1_1d,
  get_real_d_vec1_1d,
  (GET_REAL_VEC_D_TYPE)get_real_d_vec1_1d,
  get_uchar_vec1_1d,
  get_schar_vec1_1d,
  get_ptr_vec1_1d,
  get_real_dd_vec1_1d,
  real_refine_inter1_1d,
  NULL,
  real_coarse_restr1_1d,
  real_d_refine_inter1_1d,
  NULL,
  real_d_coarse_restr1_1d, 
  (REF_INTER_FCT_D)real_d_refine_inter1_1d,
  NULL,
  (REF_INTER_FCT_D)real_d_coarse_restr1_1d,
 (void *)&lag_1_1d_data
};
