/*--------------------------------------------------------------------------*/
/* ALBERTA:  an Adaptive multi Level finite element toolbox using           */
/*           Bisectioning refinement and Error control by Residual          */
/*           Techniques for scientific Applications                         */
/*                                                                          */
/* file:     macro_1d.c                                                     */
/*                                                                          */
/*                                                                          */
/* description:  dimension dependent part of reading/writing macro          */
/*               triangulations for 1d                                      */
/*                                                                          */
/*--------------------------------------------------------------------------*/
/*                                                                          */
/*  authors:   Alfred Schmidt                                               */
/*             Zentrum fuer Technomathematik                                */
/*             Fachbereich 3 Mathematik/Informatik                          */
/*             Universitaet Bremen                                          */
/*             Bibliothekstr. 2                                             */
/*             D-28359 Bremen, Germany                                      */
/*                                                                          */
/*             Kunibert G. Siebert                                          */
/*             Institut fuer Mathematik                                     */
/*             Universitaet Augsburg                                        */
/*             Universitaetsstr. 14                                         */
/*             D-86159 Augsburg, Germany                                    */
/*                                                                          */
/*             Daniel Koester                                               */
/*             Institut fuer Mathematik                                     */
/*             Albert-Ludwigs-Universitaet Freiburg                         */
/*             Hermann-Herder-Str. 10                                       */
/*             D-79104 Freiburg                                             */
/*                                                                          */
/*  http://www.mathematik.uni-freiburg.de/IAM/ALBERTA                       */
/*                                                                          */
/*  (c) by A. Schmidt and K.G. Siebert (1996-2003)                          */
/*                                                                          */
/*--------------------------------------------------------------------------*/

/*--------------------------------------------------------------------------*/
/* read data->bound into mel[].bound[]                                      */
/* WARNING: Please note the index mapping used here!                        */
/*--------------------------------------------------------------------------*/

static void
fill_bound_info_1d(MESH *mesh, const int *mel_vertices, int nv, int ne)
{
  return;
}

#if 0
static void fill_mel_orientation_1d(MACRO_EL *first, int n_mels)
{
  FUNCNAME("_AI_fill_mel_orientation_1d");
  MACRO_EL *mel, *mel_n;

  for (mel = first; mel < first + n_mels; ++mel) {
    mel->orientation = 0;
    for (n = 0; n < N_NEIGH_1D; n++) {
      mel_n = mel->neigh[n];
      if (mel_n && mel->coord[1-n] == mel_n->coord[1-n]) {
	mel->orientation |= (1 << n);
      }
    }
  }
}
#endif

/*--------------------------------------------------------------------------*/
/* orientation_1d(): checks and corrects whether the element is oriented    */
/* from left to right.                                                      */
/*--------------------------------------------------------------------------*/

#if DIM_OF_WORLD == 1
static U_CHAR orientation_1d(MACRO_DATA *data)
{
  int        i, vert_buffer, neigh_buffer;
  BNDRY_TYPE bound_buffer;
  U_CHAR     result = false;

  for(i = 0; i < data->n_macro_elements; i++) {
    if(data->coords[data->mel_vertices[VERT_IND(1,i,0)]][0] >
       data->coords[data->mel_vertices[VERT_IND(1,i,1)]][0]) {
      result = true;

      vert_buffer = data->mel_vertices[VERT_IND(1,i,0)];
      data->mel_vertices[VERT_IND(1,i,0)] =
	data->mel_vertices[VERT_IND(1,i,1)];
      data->mel_vertices[VERT_IND(1,i,1)] = vert_buffer;

      neigh_buffer = data->neigh[NEIGH_IND(1,i,0)];
      data->neigh[NEIGH_IND(1,i,0)] = data->neigh[NEIGH_IND(1,i,1)];
      data->neigh[NEIGH_IND(1,i,1)] = neigh_buffer;

      bound_buffer = data->boundary[NEIGH_IND(1,i,0)];
      data->boundary[NEIGH_IND(1,i,0)] = data->boundary[NEIGH_IND(1,i,1)];
      data->boundary[NEIGH_IND(1,i,1)] = bound_buffer;
    }
  }

  return(result);
}
#endif

/*--------------------------------------------------------------------------*/
/* macro_test(): In 1D only element orientation is checked (and corrected). */
/*--------------------------------------------------------------------------*/

static void macro_test_1d(MACRO_DATA *data, const char *new_name)
{
  FUNCNAME("macro_test");
 
  U_CHAR error_found = false;

#if DIM_OF_WORLD == 1
  if(orientation_1d(data)) {
    error_found = true;
    WARNING("Element orientation was corrected for some elements.\n");
  }
#endif

  if (error_found && new_name) {
    MSG("Attempting to write corrected macro data to file %s...\n", new_name);
    write_macro_data(data, new_name);
  }

  return;
}
