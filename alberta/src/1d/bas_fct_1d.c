/*--------------------------------------------------------------------------*/
/* ALBERTA:  an Adaptive multi Level finite element toolbox using           */
/*           Bisectioning refinement and Error control by Residual          */
/*           Techniques for scientific Applications                         */
/*                                                                          */
/* file:     bas_fct_1d.c                                                   */
/*                                                                          */
/*                                                                          */
/* description:  Lagrange basis functions up to order 4 in 1d               */
/*               includes ./lagrange_[1234]_1d.c                            */
/*               includes ./disc_lagrange_[012]_1d.c                        */
/*                                                                          */
/*--------------------------------------------------------------------------*/
/*                                                                          */
/*  authors:   Alfred Schmidt                                               */
/*             Zentrum fuer Technomathematik                                */
/*             Fachbereich 3 Mathematik/Informatik                          */
/*             Universitaet Bremen                                          */
/*             Bibliothekstr. 2                                             */
/*             D-28359 Bremen, Germany                                      */
/*                                                                          */
/*             Kunibert G. Siebert                                          */
/*             Institut fuer Mathematik                                     */
/*             Universitaet Augsburg                                        */
/*             Universitaetsstr. 14                                         */
/*             D-86159 Augsburg, Germany                                    */
/*                                                                          */
/*  http://www.mathematik.uni-freiburg.de/IAM/ALBERTA                       */
/*                                                                          */
/*  (c) by A. Schmidt and K.G. Siebert (1996-2003)                          */
/*                                                                          */
/*--------------------------------------------------------------------------*/

#include "alberta_intern.h"

#define N_BAS_LAG_0_1D N_BAS_LAGRANGE(0, 1)
#define N_BAS_LAG_1_1D N_BAS_LAGRANGE(1, 1)
#define N_BAS_LAG_2_1D N_BAS_LAGRANGE(2, 1)
#define N_BAS_LAG_3_1D N_BAS_LAGRANGE(3, 1)
#define N_BAS_LAG_4_1D N_BAS_LAGRANGE(4, 1)

/*--------------------------------------------------------------------------*/
/*  Lagrangian basis functions of order 1-4; these                          */
/*  functions are evaluated in barycentric coordinates; the derivatives     */
/*  are those corresponding to these barycentric coordinates.               */
/*--------------------------------------------------------------------------*/

#include "lagrange_1_1d.c"
#include "lagrange_2_1d.c"
#include "lagrange_3_1d.c"
#include "lagrange_4_1d.c"

/*--------------------------------------------------------------------------*/
/*  discontinuous Lagrangian basisfunctions of order 0-2; these             */
/*  functions are evaluated in barycentric coordinates; the derivatives     */
/*  are those corresponding to these barycentric coordinates.               */
/*--------------------------------------------------------------------------*/

#include "disc_lagrange_0_1d.c"
#include "disc_lagrange_1_1d.c"
#include "disc_lagrange_2_1d.c"

#include "disc-ortho-poly.c.h"
