/*--------------------------------------------------------------------------*/
/* ALBERTA:  an Adaptive multi Level finite element toolbox using           */
/*           Bisectioning refinement and Error control by Residual          */
/*           Techniques for scientific Applications                         */
/*                                                                          */
/* file:     disc_lagrange_2_1d.c                                           */
/*                                                                          */
/* description:  piecewise quadratic discontinuous Lagrange elements in 1d  */
/*                                                                          */
/*--------------------------------------------------------------------------*/
/*                                                                          */
/*  authors:   Alfred Schmidt                                               */
/*             Zentrum fuer Technomathematik                                */
/*             Fachbereich 3 Mathematik/Informatik                          */
/*             Universitaet Bremen                                          */
/*             Bibliothekstr. 2                                             */
/*             D-28359 Bremen, Germany                                      */
/*                                                                          */
/*             Kunibert G. Siebert                                          */
/*             Institut fuer Mathematik                                     */
/*             Universitaet Augsburg                                        */
/*             Universitaetsstr. 14                                         */
/*             D-86159 Augsburg, Germany                                    */
/*                                                                          */
/*  http://www.mathematik.uni-freiburg.de/IAM/ALBERTA                       */
/*                                                                          */
/*  (c) by A. Schmidt and K.G. Siebert (1996-2003)                          */
/*                                                                          */
/*--------------------------------------------------------------------------*/

/****************************************************************************/
/*  basisfunction at vertex 0                                               */
/****************************************************************************/

static REAL d_phi2v0_1d(const REAL_B lambda, const BAS_FCTS *thisptr)
{
  return(lambda[0]*(2.0*lambda[0] - 1.0));
}

static const REAL *d_grd_phi2v0_1d(const REAL_B lambda, const BAS_FCTS *thisptr)
{
  static REAL_B grd = {0};

  grd[0] = 4.0*lambda[0] - 1.0;
  return((const REAL *) grd);
}

static const REAL_B *d_D2_phi2v0_1d(const REAL *lambda, const BAS_FCTS *thisptr)
{
  static const REAL_BB D2 = {INIT_BARY_1D(4, 0),
			     INIT_BARY_1D(0, 0),};

  return(D2);
}

/****************************************************************************/
/*  basisfunction at vertex 1                                               */
/****************************************************************************/

static REAL d_phi2v1_1d(const REAL_B lambda, const BAS_FCTS *thisptr)
{
  return(lambda[1]*(2.0*lambda[1] - 1.0));
}

static const REAL *d_grd_phi2v1_1d(const REAL_B lambda, const BAS_FCTS *thisptr)
{
  static REAL_B grd = {0};

  grd[1] = 4.0*lambda[1] - 1.0;
  return((const REAL *) grd);
}

static const REAL_B *d_D2_phi2v1_1d(const REAL *lambda, const BAS_FCTS *thisptr)
{
  static const REAL_BB D2 = {INIT_BARY_1D(0, 0),
			     INIT_BARY_1D(0, 4),};

  return(D2);
}

/****************************************************************************/
/*  basisfunction at center                                                 */
/****************************************************************************/

static REAL d_phi2c0_1d(const REAL_B lambda, const BAS_FCTS *thisptr)
{
  return(4.0*lambda[0]*lambda[1]);
}

static const REAL *d_grd_phi2c0_1d(const REAL_B lambda, const BAS_FCTS *thisptr)
{
  static REAL_B grd = {0};

  grd[0] = 4.0*lambda[1];
  grd[1] = 4.0*lambda[0];
  return((const REAL *) grd);
}

static const REAL_B *d_D2_phi2c0_1d(const REAL *lambda, const BAS_FCTS *thisptr)
{
  static const REAL_BB D2 = {INIT_BARY_1D(0, 4),
			     INIT_BARY_1D(4, 0),};

  return(D2);
}

/*****************************************************************************/

#undef DEF_EL_VEC_D_2_1D
#define DEF_EL_VEC_D_2_1D(type, name)				\
  DEF_EL_VEC_CONST(type, name, N_BAS_LAG_1_1D, N_BAS_LAG_1_1D)

#undef DEFUN_GET_EL_VEC_D_2_1D
#define DEFUN_GET_EL_VEC_D_2_1D(name, type, admin, body, ...)		\
  static const EL_##type##_VEC *					\
  d_get_##name##2_1d(type##_VEC_TYPE *vec, const EL *el, __VA_ARGS__)	\
  {									\
    FUNCNAME("d_get_"#name"d_2_1d");					\
    static DEF_EL_VEC_D_2_1D(type, rvec_space);				\
    type##_VEC_TYPE *rvec = vec ? vec : rvec_space->vec;		\
    int n0, node, ibas;							\
    DOF **dofptr = el->dof, dof;					\
									\
    DEBUG_TEST_EXIT(true, "");						\
									\
    node = (admin)->mesh->node[CENTER];					\
    n0   = (admin)->n0_dof[CENTER];					\
    for (ibas = 0; ibas < N_BAS_LAG_2_1D; ibas++) {			\
      dof = dofptr[node][n0+ibas];					\
      body;								\
    }									\
									\
    return vec ? NULL : rvec_space;					\
  }									\
  struct _AI_semicolon_dummy

#undef DEFUN_GET_EL_DOF_VEC_D_2_1D
#define DEFUN_GET_EL_DOF_VEC_D_2_1D(name, type, ASSIGN)			\
  DEFUN_GET_EL_VEC_D_2_1D(_##name##_vec, type, dv->fe_space->admin,	\
			  ASSIGN(dv->vec[dof], rvec[ibas]),		\
			  const DOF_##type##_VEC *dv);			\
  static const EL_##type##_VEC *					\
  d_get_##name##_vec2_1d(type##_VEC_TYPE *vec, const EL *el,		\
			 const DOF_##type##_VEC *dv)			\
  {									\
    EL_##type##_VEC *vec_loc = dv->vec_loc;				\
									\
    if (vec != NULL || vec_loc == NULL) {				\
      return d_get__##name##_vec2_1d(vec, el, dv);			\
    } else {								\
      d_get__##name##_vec2_1d(vec_loc->vec, el, dv);			\
      return vec_loc;							\
    }									\
  }									\
  struct _AI_semicolon_dummy


#undef COPY_EQ
#define COPY_EQ(a, b) (b) = (a)

/****************************************************************************/
/*  functions for combining basisfunctions with coefficients                */
/****************************************************************************/

DEFUN_GET_EL_VEC_D_2_1D(dof_indices, DOF, admin,
			rvec[ibas] = dof,
			const DOF_ADMIN *admin, const BAS_FCTS *thisptr);

static const EL_BNDRY_VEC *d_get_bound2_1d(BNDRY_FLAGS *vec,
					   const EL_INFO *el_info,
					   const BAS_FCTS *thisptr)
{
  FUNCNAME("d_get_bound1_1d");
  static DEF_EL_VEC_D_1_1D(BNDRY, rvec_space);
  BNDRY_FLAGS *rvec = vec ? vec : rvec_space->vec;
  int i;

  TEST_FLAG(FILL_BOUND, el_info);

  for (i = 0; i < N_BAS_LAG_2_1D; i++) {
    BNDRY_FLAGS_CPY(rvec[i], el_info->edge_bound[0]);
  }

  return vec ? NULL : rvec_space;
}

/*--------------------------------------------------------------------*/
/*--- function for local interpolaton of scalar functions          ---*/
/*--------------------------------------------------------------------*/
 
GENERATE_INTERPOL(d_, 2, 1, 3);

/*--------------------------------------------------------------------*/
/*--- function for local interpolaton of vector functions          ---*/
/*--------------------------------------------------------------------*/
 
GENERATE_INTERPOL_D(d_, 2, 1, 3);

GENERATE_INTERPOL_DOW(d_, 2, 1, 3);

/*--------------------------------------------------------------------*/
/*--- function for accessing a local DOF_INT_VEC                   ---*/
/*--------------------------------------------------------------------*/

DEFUN_GET_EL_DOF_VEC_D_2_1D(int, INT, COPY_EQ);

/*--------------------------------------------------------------------*/
/*--- function for accessing a local DOF_REAL_VEC                  ---*/
/*--------------------------------------------------------------------*/

DEFUN_GET_EL_DOF_VEC_D_2_1D(real, REAL, COPY_EQ);

/*--------------------------------------------------------------------*/
/*--- function for accessing a local DOF_REAL_D_VEC                ---*/
/*--------------------------------------------------------------------*/

DEFUN_GET_EL_DOF_VEC_D_2_1D(real_d, REAL_D, COPY_DOW);

/*--------------------------------------------------------------------*/
/*--- function for accessing a local DOF_SCHAR_VEC                 ---*/
/*--------------------------------------------------------------------*/

DEFUN_GET_EL_DOF_VEC_D_2_1D(schar, SCHAR, COPY_EQ);

/*--------------------------------------------------------------------*/
/*--- function for accessing a local DOF_UCHAR_VEC                 ---*/
/*--------------------------------------------------------------------*/

DEFUN_GET_EL_DOF_VEC_D_2_1D(uchar, UCHAR, COPY_EQ);

/*--------------------------------------------------------------------*/
/*--- function for accessing a local DOF_PTR_VEC                   ---*/
/*--------------------------------------------------------------------*/

DEFUN_GET_EL_DOF_VEC_D_2_1D(ptr, PTR, COPY_EQ);

/*--------------------------------------------------------------------*/
/*--- function for accessing a local DOF_REAL_DD_VEC               ---*/
/*--------------------------------------------------------------------*/

DEFUN_GET_EL_DOF_VEC_D_2_1D(real_dd, REAL_DD, _AI_MCOPY_DOW);

/****************************************************************************/
/*  functions for interpolation/ restriction during refinement/coarsening   */
/****************************************************************************/

static void d_real_refine_inter2_1d(DOF_REAL_VEC *dv, RC_LIST_EL *list, int n)
{
  REAL pvec[N_BAS_LAG_2_1D];
  DOF cdof[N_BAS_LAG_2_1D];
  EL              *el;
  const BAS_FCTS  *bas_fcts = dv->fe_space->bas_fcts;
  const DOF_ADMIN *admin = dv->fe_space->admin;
  REAL            *v = dv->vec;

  if (n < 1) return;

  el = list->el_info.el;
  d_get_real_vec2_1d(pvec, el, dv);

/*--------------------------------------------------------------------*/
/*--- DOFs on first element, first child                           ---*/
/*--------------------------------------------------------------------*/

  d_get_dof_indices2_1d(cdof, el->child[0], admin, bas_fcts);

  v[cdof[0]] = (pvec[0]);
  v[cdof[1]] = (pvec[2]);
  v[cdof[2]] = (0.375*pvec[0] - 0.125*pvec[1] + 0.75*pvec[2]);

/*--------------------------------------------------------------------*/
/*--- DOFs on first element, second child                          ---*/
/*--------------------------------------------------------------------*/

  d_get_dof_indices2_1d(cdof, el->child[1], admin, bas_fcts);

  v[cdof[0]] = (pvec[2]);
  v[cdof[1]] = (pvec[1]);
  v[cdof[2]] = (-0.125*pvec[0] + 0.375*pvec[1] + 0.75*pvec[2]);

  return;
}

static void d_real_coarse_inter2_1d(DOF_REAL_VEC *dv, RC_LIST_EL *list, int n)
{
  REAL cvec[N_BAS_LAG_2_1D];
  DOF pdof[N_BAS_LAG_2_1D];
  EL              *el;
  const BAS_FCTS  *bas_fcts = dv->fe_space->bas_fcts;
  const DOF_ADMIN *admin = dv->fe_space->admin;
  REAL            *v = dv->vec;

  if (n < 1) return;

  el = list->el_info.el;
  d_get_dof_indices2_1d(pdof, el, admin, bas_fcts);

/*--------------------------------------------------------------------*/
/*--- DOFs on first element, from first child                      ---*/
/*--------------------------------------------------------------------*/

  d_get_real_vec2_1d(cvec, el->child[0], dv);

  v[pdof[0]] = cvec[0];

/*--------------------------------------------------------------------*/
/*--- DOFs on first element, second child                          ---*/
/*--------------------------------------------------------------------*/

  d_get_real_vec2_1d(cvec, el->child[1], dv);

  v[pdof[1]] = cvec[1];
  v[pdof[2]] = cvec[0];

  return;
}

static const BAS_FCT     d_phi2_1d[N_BAS_LAG_2_1D] = {
  d_phi2v0_1d, d_phi2v1_1d, d_phi2c0_1d
};
static const GRD_BAS_FCT d_grd_phi2_1d[N_BAS_LAG_2_1D] = {
  d_grd_phi2v0_1d, d_grd_phi2v1_1d, d_grd_phi2c0_1d
};
static const D2_BAS_FCT  d_D2_phi2_1d[N_BAS_LAG_2_1D]  = {
  d_D2_phi2v0_1d, d_D2_phi2v1_1d, d_D2_phi2c0_1d
};

static const BAS_FCTS disc_lagrange2_1d = {
  "disc_lagrange2_1d", 1, 1, N_BAS_LAG_2_1D, N_BAS_LAG_2_1D, 2,
  {0, N_BAS_LAG_2_1D, 0, 0}, 
  -1, /* trace_admin */
  INIT_BFCTS_CHAIN(disc_lagrange2_1d),
  INIT_ELEMENT_INITIALIZER(NULL, FILL_NOTHING), /* init_element + fill-flags */
  d_phi2_1d, d_grd_phi2_1d, d_D2_phi2_1d, 
  NULL, NULL, /* third and fourth derivatives */
  NULL, NULL, NULL, false, /* phi_d etc. */  
  /********************/
  &lagrange_0d,                             /* trace space */
  { { { trace_mapping_lag_2_1d[0],
	trace_mapping_lag_2_1d[1], }, }, }, /* trace mapping */
  { N_BAS_LAG_0D, N_BAS_LAG_0D, },          /* n_trace_bas_fcts */
  d_get_dof_indices2_1d, 
  d_get_bound2_1d,
  d_interpol2_1d,
  d_interpol_d_2_1d,
  d_interpol_dow_2_1d,
  d_get_int_vec2_1d,
  d_get_real_vec2_1d,
  d_get_real_d_vec2_1d,
  (GET_REAL_VEC_D_TYPE)d_get_real_d_vec2_1d,
  d_get_uchar_vec2_1d,
  d_get_schar_vec2_1d,
  d_get_ptr_vec2_1d,
  d_get_real_dd_vec2_1d,
  d_real_refine_inter2_1d,
  d_real_coarse_inter2_1d,
  NULL,
  NULL,
  NULL,
  NULL,
  NULL,
  NULL,
  NULL,
  (void *)&lag_2_1d_data
};
