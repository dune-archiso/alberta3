/*--------------------------------------------------------------------*/
/*--- ALBERTA:  an Adaptive multi Level finite element toolbox using -*/
/*---           Bisectioning refinement and Error control by Residual */
/*---           Techniques for scientific Applications             ---*/
/*---                                                              ---*/
/*--- file: lagrange_3_1d.c                                        ---*/
/*---                                                              ---*/
/*--- description: implementation of the basis functions           ---*/
/*---              lagrange3 in 1d                                 ---*/
/*---                                                              ---*/
/*--- created by: kgs on host enigma                               ---*/
/*---           at 16:50 on 28 of March 2003                       ---*/
/*--------------------------------------------------------------------*/
/*---                                                              ---*/
/*--- authors:   Alfred Schmidt                                    ---*/
/*---            Zentrum fuer Technomathematik                     ---*/
/*---            Fachbereich 3 Mathematik/Informatik               ---*/
/*---            Universitaet Bremen                               ---*/
/*---            Bibliothekstr. 2                                  ---*/
/*---            D-28359 Bremen, Germany                           ---*/
/*---                                                              ---*/
/*---            Kunibert G. Siebert                               ---*/
/*---            Institut fuer Mathematik                          ---*/
/*---            Universitaet Augsburg                             ---*/
/*---            Universitaetsstr. 14                              ---*/
/*---            D-86159 Augsburg, Germany                         ---*/
/*---                                                              ---*/
/*--- http://www.mathematik.uni-freiburg.de/IAM/ALBERTA            ---*/
/*---                                                              ---*/
/*--- (c) by A. Schmidt and K.G. Siebert (1996-2003)               ---*/
/*---                                                              ---*/
/*--------------------------------------------------------------------*/

static const REAL_B bary3_1d[N_BAS_LAG_3_1D] = {INIT_BARY_1D(1.0, 0.0),
						INIT_BARY_1D(0.0, 1.0),
						INIT_BARY_1D(2.0/3.0, 1.0/3.0),
						INIT_BARY_1D(1.0/3.0, 2.0/3.0)};

static LAGRANGE_DATA lag_3_1d_data = {
  bary3_1d,
  NULL /* lumping_quad */,
};

/*--------------------------------------------------------------------------*/
/*---  basisfunction 0 located at vertex 0                               ---*/
/*--------------------------------------------------------------------------*/

static REAL phi3_0_1d(const REAL_B lambda, const BAS_FCTS *thisptr)
{
  return (4.5*(lambda[0] - 1.0)*lambda[0] + 1.0)*lambda[0];
}

static const REAL *grd_phi3_0_1d(const REAL_B lambda, const BAS_FCTS *thisptr)
{
  static REAL_B grd;
  grd[0] = (13.5*lambda[0] - 9.0)*lambda[0] + 1;
  return (const REAL *)grd;
}

static const REAL_B *D2_phi3_0_1d(const REAL *lambda, const BAS_FCTS *thisptr)
{
  static  REAL_BB D2;
  D2[0][0] = 27.0*lambda[0] - 9.0;
  return (const REAL_B *)D2;
}

static const REAL_BB *D3_phi3_0_1d(const REAL *lambda, const BAS_FCTS *thisptr)
{
  static  REAL_BBB D3;
  D3[0][0][0] = 27.0;
  return (const REAL_BB *)D3;
}

/*--------------------------------------------------------------------------*/
/*---  basisfunction 1 located at vertex 1                               ---*/
/*--------------------------------------------------------------------------*/

static REAL phi3_1_1d(const REAL_B lambda, const BAS_FCTS *thisptr)
{
  return (4.5*(lambda[1] - 1.0)*lambda[1] + 1.0)*lambda[1];
}

static const REAL *grd_phi3_1_1d(const REAL_B lambda, const BAS_FCTS *thisptr)
{
  static REAL_B grd;
  grd[1] = (13.5*lambda[1] - 9.0)*lambda[1] + 1;
  return (const REAL *)grd;
}

static const REAL_B *D2_phi3_1_1d(const REAL *lambda, const BAS_FCTS *thisptr)
{
  static REAL_BB D2;
  D2[1][1] = 27.0*lambda[1] - 9.0;
  return (const REAL_B *)D2;
}

static const REAL_BB *D3_phi3_1_1d(const REAL *lambda, const BAS_FCTS *thisptr)
{
  static REAL_BBB D3;
  D3[1][1][1] = 27.0;
  return (const REAL_BB *)D3;
}

/*--------------------------------------------------------------------------*/
/*---  basisfunction 2, first at center                                  ---*/
/*--------------------------------------------------------------------------*/

static REAL phi3_2_1d(const REAL_B lambda, const BAS_FCTS *thisptr)
{
  return (13.5*lambda[0] - 4.5)*lambda[0]*lambda[1];
}

static const REAL *grd_phi3_2_1d(const REAL_B lambda, const BAS_FCTS *thisptr)
{
  static REAL_B grd;
  grd[0] = (27.0*lambda[0] - 4.5)*lambda[1];
  grd[1] = (13.5*lambda[0] - 4.5)*lambda[0];
  return (const REAL *)grd;
}

static const REAL_B *D2_phi3_2_1d(const REAL *lambda, const BAS_FCTS *thisptr)
{
  static REAL_BB D2;
  D2[0][0] = 27.0*lambda[1];
  D2[0][1] = D2[1][0] = 27.0*lambda[0] - 4.5;
  return (const REAL_B *)D2;
}

static const REAL_BB *D3_phi3_2_1d(const REAL *lambda, const BAS_FCTS *thisptr)
{
  static REAL_BBB D3;
  D3[0][0][1] = D3[0][1][0] = D3[1][0][0] = 27.0;
  return (const REAL_BB *)D3;
}

/*--------------------------------------------------------------------------*/
/*---  basisfunction 3, second at center                                 ---*/
/*--------------------------------------------------------------------------*/

static REAL phi3_3_1d(const REAL_B lambda, const BAS_FCTS *thisptr)
{
  return (13.5*lambda[1] - 4.5)*lambda[1]*lambda[0];
}

static const REAL *grd_phi3_3_1d(const REAL_B lambda, const BAS_FCTS *thisptr)
{
  static REAL_B grd;
  grd[0] = (13.5*lambda[1] - 4.5)*lambda[1];
  grd[1] = (27.0*lambda[1] - 4.5)*lambda[0];
  return((const REAL *) grd);
}

static const REAL_B *D2_phi3_3_1d(const REAL *lambda, const BAS_FCTS *thisptr)
{
  static REAL_BB D2;
  D2[0][1] = D2[1][0] = 27.0*lambda[1] - 4.5;
  D2[1][1] = 27.0*lambda[0];
  return (const REAL_B *)D2;
}

static const REAL_BB *D3_phi3_3_1d(const REAL *lambda, const BAS_FCTS *thisptr)
{
  static REAL_BBB D3;
  D3[0][1][1] = D3[1][0][1] = D3[1][1][0] = 27.0;
  return (const REAL_BB *)D3;
}

/******************************************************************************/

#undef DEF_EL_VEC_3_1D
#define DEF_EL_VEC_3_1D(type, name)				\
  DEF_EL_VEC_CONST(type, name, N_BAS_LAG_3_1D, N_BAS_LAG_3_1D)

#undef DEFUN_GET_EL_VEC_3_1D
#define DEFUN_GET_EL_VEC_3_1D(name, type, admin, body, ...)		\
  static const EL_##type##_VEC *					\
  get_##name##3_1d(type##_VEC_TYPE *vec, const EL *el, __VA_ARGS__)	\
  {									\
    FUNCNAME("get_"#name"3_1d");					\
    static DEF_EL_VEC_3_1D(type, rvec_space);				\
    type##_VEC_TYPE *rvec = vec ? vec : rvec_space->vec;		\
    int n0, node, inode, ibas;						\
    DOF **dofptr = el->dof, dof;					\
									\
    DEBUG_TEST_EXIT(true, "");						\
									\
    /*--------------------------------------------------------------------*/ \
    /*--- DOFs at vertices                                             ---*/ \
    /*--------------------------------------------------------------------*/ \
									\
    node = (admin)->mesh->node[VERTEX];					\
    n0   = (admin)->n0_dof[VERTEX];					\
    for (ibas = inode = 0; inode < N_VERTICES_1D; inode++, ibas++) {	\
      dof = dofptr[node+inode][n0];					\
      body;								\
    }									\
									\
    /*--------------------------------------------------------------------*/ \
    /*--- DOFs at center                                               ---*/ \
    /*--------------------------------------------------------------------*/ \
									\
    node = (admin)->mesh->node[CENTER];					\
    n0   = (admin)->n0_dof[CENTER];					\
    for (inode = 0; inode < 2; inode++, ibas++) {			\
      dof = dofptr[node][n0+inode];					\
      body;								\
    }									\
									\
    return vec ? NULL : rvec_space;					\
  }									\
  struct _AI_semicolon_dummy

#undef DEFUN_GET_EL_DOF_VEC_3_1D
#define DEFUN_GET_EL_DOF_VEC_3_1D(name, type, ASSIGN)			\
  DEFUN_GET_EL_VEC_3_1D(_##name##_vec, type, dv->fe_space->admin,	\
			ASSIGN(dv->vec[dof], rvec[ibas]),		\
			const DOF_##type##_VEC *dv);			\
  static const EL_##type##_VEC *					\
  get_##name##_vec3_1d(type##_VEC_TYPE *vec, const EL *el,		\
		       const DOF_##type##_VEC *dv)			\
  {									\
    EL_##type##_VEC *vec_loc = dv->vec_loc;				\
									\
    if (vec != NULL || vec_loc == NULL) {				\
      return get__##name##_vec3_1d(vec, el, dv);			\
    } else {								\
      get__##name##_vec3_1d(vec_loc->vec, el, dv);			\
      return vec_loc;							\
    }									\
  }									\
  struct _AI_semicolon_dummy

#undef COPY_EQ
#define COPY_EQ(a, b) (b) = (a)

/*--------------------------------------------------------------------*/
/*--- function for accessing local DOFs on an element              ---*/
/*--------------------------------------------------------------------*/

DEFUN_GET_EL_VEC_3_1D(dof_indices, DOF, admin,
		      rvec[ibas] = dof,
		      const DOF_ADMIN *admin, const BAS_FCTS *thisptr);

/*--------------------------------------------------------------------*/
/*--- function for accessing boundary type of DOFs                 ---*/
/*--------------------------------------------------------------------*/

static const EL_BNDRY_VEC *get_bound3_1d(BNDRY_FLAGS *vec,
					 const EL_INFO *el_info,
					 const BAS_FCTS *thisptr)
{
  FUNCNAME("get_bound3_1d");
  static DEF_EL_VEC_3_1D(BNDRY, rvec_space);
  BNDRY_FLAGS *rvec = vec ? vec : rvec_space->vec;
  int ibas = 0, i, j;

  DEBUG_TEST_FLAG(FILL_BOUND, el_info);

/*--------------------------------------------------------------------*/
/*--- basis functions at vertices                                  ---*/
/*--------------------------------------------------------------------*/

  for (i = 0; i < N_VERTICES_1D; i++) {
    BNDRY_FLAGS_CPY(rvec[i], el_info->vertex_bound[i]);
  }

/*--------------------------------------------------------------------*/
/*--- basis functions at center                                    ---*/
/*--------------------------------------------------------------------*/

  for (j = 0; j < 2; j++) {
    BNDRY_FLAGS_INIT(rvec[ibas++]);
  }

  return vec ? NULL : rvec_space;
}

/*--------------------------------------------------------------------*/
/*--- function for local interpolaton of scalar functions          ---*/
/*--------------------------------------------------------------------*/

GENERATE_INTERPOL(/**/, 3, 1, N_BAS_LAG_3_1D);

/*--------------------------------------------------------------------*/
/*--- function for local interpolaton of vector functions          ---*/
/*--------------------------------------------------------------------*/

GENERATE_INTERPOL_D(/**/, 3, 1, N_BAS_LAG_3_1D);

GENERATE_INTERPOL_DOW(/**/, 3, 1, N_BAS_LAG_3_1D);

/*--------------------------------------------------------------------*/
/*--- function for accessing a local DOF_INT_VEC                   ---*/
/*--------------------------------------------------------------------*/

DEFUN_GET_EL_DOF_VEC_3_1D(int, INT, COPY_EQ);

/*--------------------------------------------------------------------*/
/*--- function for accessing a local DOF_REAL_VEC                  ---*/
/*--------------------------------------------------------------------*/

DEFUN_GET_EL_DOF_VEC_3_1D(real, REAL, COPY_EQ);

/*--------------------------------------------------------------------*/
/*--- function for accessing a local DOF_REAL_D_VEC                ---*/
/*--------------------------------------------------------------------*/

DEFUN_GET_EL_DOF_VEC_3_1D(real_d, REAL_D, COPY_DOW);

/*--------------------------------------------------------------------*/
/*--- function for accessing a local DOF_SCHAR_VEC                 ---*/
/*--------------------------------------------------------------------*/

DEFUN_GET_EL_DOF_VEC_3_1D(schar, SCHAR, COPY_EQ);

/*--------------------------------------------------------------------*/
/*--- function for accessing a local DOF_UCHAR_VEC                 ---*/
/*--------------------------------------------------------------------*/

DEFUN_GET_EL_DOF_VEC_3_1D(uchar, UCHAR, COPY_EQ);

/*--------------------------------------------------------------------*/
/*--- function for accessing a local DOF_PTR_VEC                   ---*/
/*--------------------------------------------------------------------*/

DEFUN_GET_EL_DOF_VEC_3_1D(ptr, PTR, COPY_EQ);

/*--------------------------------------------------------------------*/
/*--- function for accessing a local DOF_REAL_DD_VEC               ---*/
/*--------------------------------------------------------------------*/

DEFUN_GET_EL_DOF_VEC_3_1D(real_dd, REAL_DD, _AI_MCOPY_DOW);

/*--------------------------------------------------------------------*/
/*--- function for interpolaton of DOF_REAL_VECs during refinement ---*/
/*--------------------------------------------------------------------*/

static void refine_inter3_1d(DOF_REAL_VEC *dv, RC_LIST_EL *list, int n_el)
{
  REAL pvec[N_BAS_LAG_3_1D];
  DOF cdof[N_BAS_LAG_3_1D];
  EL              *el;
  const BAS_FCTS  *bas_fcts = dv->fe_space->bas_fcts;
  const DOF_ADMIN *admin = dv->fe_space->admin;
  REAL            *v = dv->vec;

  el = list->el_info.el;
  get_real_vec3_1d(pvec, el, dv);

/*--------------------------------------------------------------------*/
/*--- DOFs on first element, first child                           ---*/
/*--------------------------------------------------------------------*/

  get_dof_indices3_1d(cdof, el->child[0], admin, bas_fcts);

  v[cdof[1]] = (-0.0625*pvec[0] - 0.0625*pvec[1] + 0.5625*pvec[2] + 0.5625*pvec[3]);
  v[cdof[2]] = (0.3125*pvec[0] + 0.0625*pvec[1] + 0.9375*pvec[2] - 0.3125*pvec[3]);
  v[cdof[3]] = (pvec[2]);

/*--------------------------------------------------------------------*/
/*--- DOFs on first element, second child                          ---*/
/*--------------------------------------------------------------------*/

  get_dof_indices3_1d(cdof, el->child[1], admin, bas_fcts);

  v[cdof[2]] = (pvec[3]);
  v[cdof[3]] = (0.0625*pvec[0] + 0.3125*pvec[1] - 0.3125*pvec[2] + 0.9375*pvec[3]);

  return;
}

/*--------------------------------------------------------------------*/
/*--- function for interpolaton of DOF_REAL_D_VECs during refinement -*/
/*--------------------------------------------------------------------*/

static void refine_inter_d3_1d(DOF_REAL_D_VEC *dv, RC_LIST_EL *list, int n_el)
{
  REAL_D pvec[N_BAS_LAG_3_1D];
  DOF cdof[N_BAS_LAG_3_1D];
  EL              *el;
  int             n;
  const BAS_FCTS  *bas_fcts = dv->fe_space->bas_fcts;
  const DOF_ADMIN *admin = dv->fe_space->admin;
  REAL_D          *v = dv->vec;

  el = list->el_info.el;
  get_real_d_vec3_1d(pvec, el, dv);

/*--------------------------------------------------------------------*/
/*--- DOFs on first element, first child                           ---*/
/*--------------------------------------------------------------------*/

  get_dof_indices3_1d(cdof, el->child[0], admin, bas_fcts);

  for (n = 0; n < DIM_OF_WORLD; n++)
  {
    v[cdof[1]][n] = (-0.0625*pvec[0][n] - 0.0625*pvec[1][n] + 0.5625*pvec[2][n] + 0.5625*pvec[3][n]);
    v[cdof[2]][n] = (0.3125*pvec[0][n] + 0.0625*pvec[1][n] + 0.9375*pvec[2][n] - 0.3125*pvec[3][n]);
    v[cdof[3]][n] = (pvec[2][n]);
  }

/*--------------------------------------------------------------------*/
/*--- DOFs on first element, second child                          ---*/
/*--------------------------------------------------------------------*/

  get_dof_indices3_1d(cdof, el->child[1], admin, bas_fcts);

  for (n = 0; n < DIM_OF_WORLD; n++)
  {
    v[cdof[2]][n] = (pvec[3][n]);
    v[cdof[3]][n] = (0.0625*pvec[0][n] + 0.3125*pvec[1][n] - 0.3125*pvec[2][n] + 0.9375*pvec[3][n]);
  }
}

/*--------------------------------------------------------------------*/
/*--- function for interpolaton of DOF_REAL_VECs during coarsening ---*/
/*--------------------------------------------------------------------*/

static void coarse_inter3_1d(DOF_REAL_VEC *dv, RC_LIST_EL *list, int n_el)
{
  REAL cvec[N_BAS_LAG_3_1D];
  DOF pdof[N_BAS_LAG_3_1D];
  EL              *el;
  const BAS_FCTS  *bas_fcts = dv->fe_space->bas_fcts;
  const DOF_ADMIN *admin = dv->fe_space->admin;
  REAL            *v = dv->vec;

  el = list->el_info.el;
  get_dof_indices3_1d(pdof, el, admin, bas_fcts);

/*--------------------------------------------------------------------*/
/*--- DOFs on first element, from first child                      ---*/
/*--------------------------------------------------------------------*/

  get_real_vec3_1d(cvec, el->child[0], dv);

  v[pdof[2]] = (cvec[3]);

/*--------------------------------------------------------------------*/
/*--- DOFs on first element, second child                          ---*/
/*--------------------------------------------------------------------*/

  get_real_vec3_1d(cvec, el->child[1], dv);

  v[pdof[3]] = (cvec[2]);

  return;
}

/*--------------------------------------------------------------------*/
/*--- function for interpolaton of DOF_REAL_D_VECs during coarsening -*/
/*--------------------------------------------------------------------*/

static void coarse_inter_d3_1d(DOF_REAL_D_VEC *dv, RC_LIST_EL *list, int n_el)
{
  REAL_D cvec[N_BAS_LAG_3_1D];
  DOF pdof[N_BAS_LAG_3_1D];
  EL              *el;
  const BAS_FCTS  *bas_fcts = dv->fe_space->bas_fcts;
  const DOF_ADMIN *admin = dv->fe_space->admin;
  REAL_D          *v = dv->vec;

  el = list->el_info.el;
  get_dof_indices3_1d(pdof, el, admin, bas_fcts);

/*--------------------------------------------------------------------*/
/*--- DOFs on first element, from first child                      ---*/
/*--------------------------------------------------------------------*/

  get_real_d_vec3_1d(cvec, el->child[0], dv);

  COPY_DOW(cvec[3], v[pdof[2]]);

/*--------------------------------------------------------------------*/
/*--- DOFs on first element, second child                          ---*/
/*--------------------------------------------------------------------*/

  get_real_d_vec3_1d(cvec, el->child[1], dv);

  COPY_DOW(cvec[2], v[pdof[3]]);
}

/*--------------------------------------------------------------------*/
/*--- function for restriction of DOF_REAL_VECs during coarsening  ---*/
/*--------------------------------------------------------------------*/

static void coarse_restr3_1d(DOF_REAL_VEC *dv, RC_LIST_EL *list, int n_el)
{
  REAL cvec[N_BAS_LAG_3_1D];
  DOF pdof[N_BAS_LAG_3_1D];
  EL              *el;
  const BAS_FCTS  *bas_fcts = dv->fe_space->bas_fcts;
  const DOF_ADMIN *admin = dv->fe_space->admin;
  REAL            *v = dv->vec;

  el = list->el_info.el;
  get_dof_indices3_1d(pdof, el, admin, bas_fcts);

/*--------------------------------------------------------------------*/
/*--- DOFs on first element, from first child                      ---*/
/*--------------------------------------------------------------------*/

  get_real_vec3_1d(cvec, el->child[0], dv);

  v[pdof[0]] += (-0.0625*cvec[1] + 0.3125*cvec[2]);
  v[pdof[1]] += (-0.0625*cvec[1] + 0.0625*cvec[2]);
  v[pdof[2]]  = (0.5625*cvec[1] + 0.9375*cvec[2] + cvec[3]);
  v[pdof[3]]  = (0.5625*cvec[1] - 0.3125*cvec[2]);

/*--------------------------------------------------------------------*/
/*--- DOFs on first element, from second child                     ---*/
/*--------------------------------------------------------------------*/

  get_real_vec3_1d(cvec, el->child[1], dv);

  v[pdof[0]] += (0.0625*cvec[3]);
  v[pdof[1]] += (0.3125*cvec[3]);
  v[pdof[2]] += (-0.3125*cvec[3]);
  v[pdof[3]] += (cvec[2] + 0.9375*cvec[3]);
}

/*--------------------------------------------------------------------*/
/*--- function for restriction of DOF_REAL_D_VECs during coarsening --*/
/*--------------------------------------------------------------------*/

static void coarse_restr_d3_1d(DOF_REAL_D_VEC *dv, RC_LIST_EL *list, int n_el)
{
  REAL_D cvec[N_BAS_LAG_3_1D];
  DOF pdof[N_BAS_LAG_3_1D];
  EL              *el;
  int             n;
  const BAS_FCTS  *bas_fcts = dv->fe_space->bas_fcts;
  const DOF_ADMIN *admin = dv->fe_space->admin;
  REAL_D          *v = dv->vec;

  el = list->el_info.el;
  get_dof_indices3_1d(pdof, el, admin, bas_fcts);

/*--------------------------------------------------------------------*/
/*--- DOFs on first element, from first child                      ---*/
/*--------------------------------------------------------------------*/

  get_real_d_vec3_1d(cvec, el->child[0], dv);

  for (n = 0; n < DIM_OF_WORLD; n++)
  {
    v[pdof[0]][n] += (-0.0625*cvec[1][n] + 0.3125*cvec[2][n]);
    v[pdof[1]][n] += (-0.0625*cvec[1][n] + 0.0625*cvec[2][n]);
    v[pdof[2]][n]  = (0.5625*cvec[1][n] + 0.9375*cvec[2][n] + cvec[3][n]);
    v[pdof[3]][n]  = (0.5625*cvec[1][n] - 0.3125*cvec[2][n]);
  }

/*--------------------------------------------------------------------*/
/*--- DOFs on first element, from second child                     ---*/
/*--------------------------------------------------------------------*/

  get_real_d_vec3_1d(cvec, el->child[1], dv);

  for (n = 0; n < DIM_OF_WORLD; n++)
  {
    v[pdof[0]][n] += (0.0625*cvec[3][n]);
    v[pdof[1]][n] += (0.3125*cvec[3][n]);
    v[pdof[2]][n] += (-0.3125*cvec[3][n]);
    v[pdof[3]][n] += (cvec[2][n] + 0.9375*cvec[3][n]);
  }

  return;
}

/*--------------------------------------------------------------------*/
/*--- Collect all information about basis functions                ---*/
/*--------------------------------------------------------------------*/

static const BAS_FCT phi3_1d[N_BAS_LAG_3_1D] =
{
  phi3_0_1d, phi3_1_1d, phi3_2_1d, phi3_3_1d
};

static const GRD_BAS_FCT grd_phi3_1d[N_BAS_LAG_3_1D] =
{
  grd_phi3_0_1d, grd_phi3_1_1d, grd_phi3_2_1d, grd_phi3_3_1d
};

static const D2_BAS_FCT D2_phi3_1d[N_BAS_LAG_3_1D] =
{
  D2_phi3_0_1d, D2_phi3_1_1d, D2_phi3_2_1d, D2_phi3_3_1d
};

static const D3_BAS_FCT D3_phi3_1d[N_BAS_LAG_3_1D] =
{
  D3_phi3_0_1d, D3_phi3_1_1d, D3_phi3_2_1d, D3_phi3_3_1d
};

/* For each wall the mapping from the wall basis functions to the
 * local DOFs on the reference element.
 */
static const int trace_mapping_lag_3_1d[N_WALLS_1D][N_BAS_LAG_0D] = {
  { 1 }, { 0 }
 };

static const BAS_FCTS lagrange3_1d =
{
  "lagrange3_1d", 1, 1, N_BAS_LAG_3_1D, N_BAS_LAG_3_1D, 3,
  {1, 2, 0, 0},  /* VERTEX, CENTER, EDGE, FACE   */
  -1, /* trace_admin */
  INIT_BFCTS_CHAIN(lagrange3_1d),
  INIT_ELEMENT_INITIALIZER(NULL, FILL_NOTHING), /* init_element + fill-flags */
  phi3_1d, grd_phi3_1d, D2_phi3_1d, D3_phi3_1d,
  NULL, /* fourth derivatives */
  NULL, NULL, NULL, false, /* phi_d etc. */  
  /********************/
  &lagrange_0d,                             /* trace space */
  { { { trace_mapping_lag_3_1d[0],
	trace_mapping_lag_3_1d[1], }, }, }, /* trace mapping */
  { N_BAS_LAG_0D, N_BAS_LAG_0D, },          /* n_trace_bas_fcts */
  get_dof_indices3_1d,
  get_bound3_1d,
  interpol3_1d,
  interpol_d_3_1d,
  interpol_dow_3_1d,
  get_int_vec3_1d,
  get_real_vec3_1d,
  get_real_d_vec3_1d,
  (GET_REAL_VEC_D_TYPE)get_real_d_vec3_1d,
  get_uchar_vec3_1d,
  get_schar_vec3_1d,
  get_ptr_vec3_1d,
  get_real_dd_vec3_1d,
  refine_inter3_1d,
  coarse_inter3_1d,
  coarse_restr3_1d,
  refine_inter_d3_1d,
  coarse_inter_d3_1d,
  coarse_restr_d3_1d,
  (REF_INTER_FCT_D)refine_inter_d3_1d,
  (REF_INTER_FCT_D)coarse_inter_d3_1d,
  (REF_INTER_FCT_D)coarse_restr_d3_1d,
  (void *)&lag_3_1d_data
};
