/*--------------------------------------------------------------------------*/
/* ALBERTA:  an Adaptive multi Level finite element toolbox using           */
/*           Bisectioning refinement and Error control by Residual          */
/*           Techniques for scientific Applications                         */
/*                                                                          */
/* file:     lagrange_2_3d.c                                                */
/*                                                                          */
/* description:  piecewise quadratic Lagrange elements in 3d                */
/*                                                                          */
/*--------------------------------------------------------------------------*/
/*                                                                          */
/*  authors:   Alfred Schmidt                                               */
/*             Zentrum fuer Technomathematik                                */
/*             Fachbereich 3 Mathematik/Informatik                          */
/*             Universitaet Bremen                                          */
/*             Bibliothekstr. 2                                             */
/*             D-28359 Bremen, Germany                                      */
/*                                                                          */
/*             Kunibert G. Siebert                                          */
/*             Institut fuer Mathematik                                     */
/*             Universitaet Augsburg                                        */
/*             Universitaetsstr. 14                                         */
/*             D-86159 Augsburg, Germany                                    */
/*                                                                          */
/*  http://www.mathematik.uni-freiburg.de/IAM/ALBERTA                       */
/*                                                                          */
/*  (c) by A. Schmidt and K.G. Siebert (1996-2003)                          */
/*                                                                          */
/*--------------------------------------------------------------------------*/

static const REAL_B bary2_3d[N_BAS_LAG_2_3D] = {
  {1.0, 0.0, 0.0, 0.0},
  {0.0, 1.0, 0.0, 0.0},
  {0.0, 0.0, 1.0, 0.0},
  {0.0, 0.0, 0.0, 1.0},
  {0.5, 0.5, 0.0, 0.0},
  {0.5, 0.0, 0.5, 0.0},
  {0.5, 0.0, 0.0, 0.5},
  {0.0, 0.5, 0.5, 0.0},
  {0.0, 0.5, 0.0, 0.5},
  {0.0, 0.0, 0.5, 0.5}
};

static LAGRANGE_DATA lag_2_3d_data = {
  bary2_3d,
  NULL /* lumping_quad */,
};

/*--------------------------------------------------------------------------*/
/*  basisfunction at vertex 0                                               */
/*--------------------------------------------------------------------------*/

static REAL phi2v0_3d(const REAL_B lambda, const BAS_FCTS *thisptr)
{
  return lambda[0]*(2.0*lambda[0] - 1.0);
}

static const REAL *grd_phi2v0_3d(const REAL_B lambda, const BAS_FCTS *thisptr)
{
  static REAL_B grd = {0};
  grd[0] = 4.0*lambda[0] - 1.0;
  return (const REAL *) grd;
}

static const REAL_B *D2_phi2v0_3d(const REAL_B lambda, const BAS_FCTS *thisptr)
{
  static const REAL_BB D2 = {{4, 0, 0, 0}, {0, 0, 0, 0},
			     {0, 0, 0, 0}, {0, 0, 0, 0}};
  return D2;
}

/*--------------------------------------------------------------------------*/
/*  basisfunction at vertex 1                                               */
/*--------------------------------------------------------------------------*/

static REAL phi2v1_3d(const REAL_B lambda, const BAS_FCTS *thisptr)
{
  return lambda[1]*(2.0*lambda[1] - 1.0);
}

static const REAL *grd_phi2v1_3d(const REAL_B lambda, const BAS_FCTS *thisptr)
{
  static REAL_B grd = {0};
  grd[1] = 4.0*lambda[1] - 1.0;
  return (const REAL *) grd;
}

static const REAL_B *D2_phi2v1_3d(const REAL_B lambda, const BAS_FCTS *thisptr)
{
  static const REAL_BB D2 = {{0, 0, 0, 0}, {0, 4, 0, 0},
			     {0, 0, 0, 0}, {0, 0, 0, 0}};
  return D2;
}

/*--------------------------------------------------------------------------*/
/*  basisfunction at vertex 2                                               */
/*--------------------------------------------------------------------------*/

static REAL phi2v2_3d(const REAL_B lambda, const BAS_FCTS *thisptr)
{
  return lambda[2]*(2.0*lambda[2] - 1.0);
}

static const REAL *grd_phi2v2_3d(const REAL_B lambda, const BAS_FCTS *thisptr)
{
  static REAL_B grd = {0};
  grd[2] = 4.0*lambda[2] - 1.0;
  return (const REAL *) grd;
}

static const REAL_B *D2_phi2v2_3d(const REAL_B lambda, const BAS_FCTS *thisptr)
{
  static const REAL_BB D2 = {{0, 0, 0, 0}, {0, 0, 0, 0},
			     {0, 0, 4, 0}, {0, 0, 0, 0}};
  return D2;
}

/*--------------------------------------------------------------------------*/
/*  basisfunction at vertex 3                                               */
/*--------------------------------------------------------------------------*/

static REAL phi2v3_3d(const REAL_B lambda, const BAS_FCTS *thisptr)
{
  return lambda[3]*(2.0*lambda[3] - 1.0);
}

static const REAL *grd_phi2v3_3d(const REAL_B lambda, const BAS_FCTS *thisptr)
{
  static REAL_B grd = {0};
  grd[3] = 4.0*lambda[3] - 1.0;
  return (const REAL *) grd;
}

static const REAL_B *D2_phi2v3_3d(const REAL_B lambda, const BAS_FCTS *thisptr)
{
  static const REAL_BB D2 = {{0, 0, 0, 0}, {0, 0, 0, 0},
			     {0, 0, 0, 0}, {0, 0, 0, 4}};
  return D2;
}

/*--------------------------------------------------------------------------*/
/*  basisfunction at edge 0                                                 */
/*--------------------------------------------------------------------------*/

static REAL phi2e0_3d(const REAL_B lambda, const BAS_FCTS *thisptr)
{
  return 4.0*lambda[0]*lambda[1];
}

static const REAL *grd_phi2e0_3d(const REAL_B lambda, const BAS_FCTS *thisptr)
{
  static REAL_B grd;
  grd[0] = 4.0*lambda[1];
  grd[1] = 4.0*lambda[0];
  return (const REAL *)grd;
}

static const REAL_B *D2_phi2e0_3d(const REAL_B lambda, const BAS_FCTS *thisptr)
{
  static const REAL_BB D2 = {{0, 4, 0, 0}, {4, 0, 0, 0},
			     {0, 0, 0, 0}, {0, 0, 0, 0}};
  return D2;
}

/*--------------------------------------------------------------------------*/
/*  basisfunction at edge 1                                                 */
/*--------------------------------------------------------------------------*/

static REAL phi2e1_3d(const REAL_B lambda, const BAS_FCTS *thisptr)
{
  return 4.0*lambda[0]*lambda[2];
}

static const REAL *grd_phi2e1_3d(const REAL_B lambda, const BAS_FCTS *thisptr)
{
  static REAL_B grd = {0};
  grd[0] = 4.0*lambda[2];
  grd[2] = 4.0*lambda[0];
  return (const REAL *) grd;
}

static const REAL_B *D2_phi2e1_3d(const REAL_B lambda, const BAS_FCTS *thisptr)
{
  static const REAL_BB D2 = {{0, 0, 4, 0}, {0, 0, 0, 0},
			     {4, 0, 0, 0}, {0, 0, 0, 0}};
  return D2;
}

/*--------------------------------------------------------------------------*/
/*  basisfunction at edge 2                                                 */
/*--------------------------------------------------------------------------*/

static REAL phi2e2_3d(const REAL_B lambda, const BAS_FCTS *thisptr)
{
  return 4.0*lambda[0]*lambda[3];
}

static const REAL *grd_phi2e2_3d(const REAL_B lambda, const BAS_FCTS *thisptr)
{
  static REAL_B grd = {0};
  grd[0] = 4.0*lambda[3];
  grd[3] = 4.0*lambda[0];
  return (const REAL *) grd;
}

static const REAL_B *D2_phi2e2_3d(const REAL_B lambda, const BAS_FCTS *thisptr)
{
  static const REAL_BB D2 = {{0, 0, 0, 4}, {0, 0, 0, 0},
			     {0, 0, 0, 0}, {4, 0, 0, 0}};
  return D2;
}

/*--------------------------------------------------------------------------*/
/*  basisfunction at edge 3                                                 */
/*--------------------------------------------------------------------------*/

static REAL phi2e3_3d(const REAL_B lambda, const BAS_FCTS *thisptr)
{
  return 4.0*lambda[1]*lambda[2];
}

static const REAL *grd_phi2e3_3d(const REAL_B lambda, const BAS_FCTS *thisptr)
{
  static REAL_B grd = {0};
  grd[1] = 4.0*lambda[2];
  grd[2] = 4.0*lambda[1];
  return (const REAL *) grd;
}

static const REAL_B *D2_phi2e3_3d(const REAL_B lambda, const BAS_FCTS *thisptr)
{
  static const REAL_BB D2 = {{0, 0, 0, 0}, {0, 0, 4, 0},
			     {0, 4, 0, 0}, {0, 0, 0, 0}};
  return D2;
}

/*--------------------------------------------------------------------------*/
/*  basisfunction at edge 4                                                 */
/*--------------------------------------------------------------------------*/

static REAL phi2e4_3d(const REAL_B lambda, const BAS_FCTS *thisptr)
{
  return 4.0*lambda[1]*lambda[3];
}

static const REAL *grd_phi2e4_3d(const REAL_B lambda, const BAS_FCTS *thisptr)
{
  static REAL_B grd = {0};
  grd[1] = 4.0*lambda[3];
  grd[3] = 4.0*lambda[1];
  return (const REAL *) grd;
}

static const REAL_B *D2_phi2e4_3d(const REAL_B lambda, const BAS_FCTS *thisptr)
{
  static const REAL_BB D2 = {{0, 0, 0, 0}, {0, 0, 0, 4},
			     {0, 0, 0, 0}, {0, 4, 0, 0}};
  return D2;
}

/*--------------------------------------------------------------------------*/
/*  basisfunction at edge 5                                                 */
/*--------------------------------------------------------------------------*/

static REAL phi2e5_3d(const REAL_B lambda, const BAS_FCTS *thisptr)
{
  return 4.0*lambda[2]*lambda[3];
}

static const REAL *grd_phi2e5_3d(const REAL_B lambda, const BAS_FCTS *thisptr)
{
  static REAL_B grd = {0};
  grd[2] = 4.0*lambda[3];
  grd[3] = 4.0*lambda[2];
  return (const REAL *) grd;
}

static const REAL_B *D2_phi2e5_3d(const REAL_B lambda, const BAS_FCTS *thisptr)
{
  static const REAL_BB D2 = {{0, 0, 0, 0}, {0, 0, 0, 0},
			     {0, 0, 0, 4}, {0, 0, 4, 0}};
  return D2;
}

/******************************************************************************/

#undef DEF_EL_VEC_2_3D
#define DEF_EL_VEC_2_3D(type, name)				\
  DEF_EL_VEC_CONST(type, name, N_BAS_LAG_2_3D, N_BAS_LAG_2_3D)

#undef DEFUN_GET_EL_VEC_2_3D
#define DEFUN_GET_EL_VEC_2_3D(name, type, admin, body, ...)		\
  static const EL_##type##_VEC *					\
  get_##name##2_3d(type##_VEC_TYPE *vec, const EL *el, __VA_ARGS__)	\
  {									\
    FUNCNAME("get_"#name"2_3d");					\
    static DEF_EL_VEC_2_3D(type, rvec_space);				\
    type##_VEC_TYPE *rvec = vec ? vec : rvec_space->vec;		\
    int n0, node0, ibas, inode;						\
    DOF **dofptr = el->dof, dof;					\
									\
    DEBUG_TEST_EXIT(true, "");						\
									\
    n0 = (admin)->n0_dof[VERTEX];					\
    for (ibas = inode = 0; ibas < N_VERTICES_3D; inode++, ibas++) {	\
      dof = dofptr[inode][n0];						\
      body;								\
    }									\
									\
    n0 = (admin)->n0_dof[EDGE];						\
    node0 = (admin)->mesh->node[EDGE];					\
    for (inode = 0; inode < N_EDGES_3D; inode++, ibas++) {		\
      dof = dofptr[node0+inode][n0];					\
      body;								\
    }									\
									\
    return vec ? NULL: rvec_space;					\
  }									\
  struct _AI_semicolon_dummy

#undef DEFUN_GET_EL_DOF_VEC_2_3D
#define DEFUN_GET_EL_DOF_VEC_2_3D(name, type, ASSIGN)			\
  DEFUN_GET_EL_VEC_2_3D(_##name##_vec, type, dv->fe_space->admin,	\
			ASSIGN(dv->vec[dof], rvec[ibas]),		\
			const DOF_##type##_VEC *dv);			\
  static const EL_##type##_VEC *					\
  get_##name##_vec2_3d(type##_VEC_TYPE *vec, const EL *el,		\
		       const DOF_##type##_VEC *dv)			\
  {									\
    EL_##type##_VEC *vec_loc = dv->vec_loc;				\
									\
    if (vec != NULL || vec_loc == NULL) {				\
      return get__##name##_vec2_3d(vec, el, dv);			\
    } else {								\
      get__##name##_vec2_3d(vec_loc->vec, el, dv);			\
      return vec_loc;							\
    }									\
  }									\
  struct _AI_semicolon_dummy

#undef COPY_EQ
#define COPY_EQ(a, b) (b) = (a)

/****************************************************************************/
/*  functions for combining basisfunctions with coefficients                */
/****************************************************************************/

DEFUN_GET_EL_VEC_2_3D(dof_indices, DOF, admin,
		      rvec[ibas] = dof,
		      const DOF_ADMIN *admin, const BAS_FCTS *thisptr);

static const EL_BNDRY_VEC *
get_bound2_3d(BNDRY_FLAGS *vec,
	      const EL_INFO *el_info, const BAS_FCTS *thisptr)
{
  FUNCNAME("get_bound2_3d");
  static DEF_EL_VEC_2_3D(BNDRY, rvec_space);
  BNDRY_FLAGS *rvec = vec ? vec : rvec_space->vec;
  int i;

  TEST_FLAG(FILL_BOUND, el_info);

  for (i = 0; i < N_VERTICES_3D; i++) {
    BNDRY_FLAGS_CPY(rvec[i], el_info->vertex_bound[i]);
  }
  for (i = 0; i < N_EDGES_3D; i++) {
    BNDRY_FLAGS_CPY(rvec[N_VERTICES_3D+i], el_info->edge_bound[i]);
  }

  return vec ? NULL : rvec_space;
}

/*--------------------------------------------------------------------*/
/*--- function for accessing a local DOF_INT_VEC                   ---*/
/*--------------------------------------------------------------------*/

DEFUN_GET_EL_DOF_VEC_2_3D(int, INT, COPY_EQ);

/*--------------------------------------------------------------------*/
/*--- function for accessing a local DOF_REAL_VEC                  ---*/
/*--------------------------------------------------------------------*/

DEFUN_GET_EL_DOF_VEC_2_3D(real, REAL, COPY_EQ);

/*--------------------------------------------------------------------*/
/*--- function for accessing a local DOF_REAL_D_VEC                ---*/
/*--------------------------------------------------------------------*/

DEFUN_GET_EL_DOF_VEC_2_3D(real_d, REAL_D, COPY_DOW);

/*--------------------------------------------------------------------*/
/*--- function for accessing a local DOF_SCHAR_VEC                 ---*/
/*--------------------------------------------------------------------*/

DEFUN_GET_EL_DOF_VEC_2_3D(schar, SCHAR, COPY_EQ);

/*--------------------------------------------------------------------*/
/*--- function for accessing a local DOF_UCHAR_VEC                 ---*/
/*--------------------------------------------------------------------*/

DEFUN_GET_EL_DOF_VEC_2_3D(uchar, UCHAR, COPY_EQ);

/*--------------------------------------------------------------------*/
/*--- function for accessing a local DOF_PTR_VEC                   ---*/
/*--------------------------------------------------------------------*/

DEFUN_GET_EL_DOF_VEC_2_3D(ptr, PTR, COPY_EQ);

/*--------------------------------------------------------------------*/
/*--- function for accessing a local DOF_REAL_DD_VEC               ---*/
/*--------------------------------------------------------------------*/

DEFUN_GET_EL_DOF_VEC_2_3D(real_dd, REAL_DD, _AI_MCOPY_DOW);

/*--------------------------------------------------------------------*/
/*--- function for local interpolaton of scalar functions          ---*/
/*--------------------------------------------------------------------*/

GENERATE_INTERPOL(/**/, 2, 3, N_BAS_LAG_2_3D);

/*--------------------------------------------------------------------*/
/*--- function for local interpolaton of vector functions          ---*/
/*--------------------------------------------------------------------*/
 
GENERATE_INTERPOL_D(/**/, 2, 3, N_BAS_LAG_2_3D);

GENERATE_INTERPOL_DOW(/**/, 2, 3, N_BAS_LAG_2_3D);

/*--------------------------------------------------------------------------*/
/*  functions for interpolation/ restriction during refinement/coarsening   */
/*--------------------------------------------------------------------------*/

static void real_refine_inter2_3d(DOF_REAL_VEC *drv, RC_LIST_EL *list, int n)
{
  FUNCNAME("real_refine_inter2_3d");
  DOF pdof[N_BAS_LAG_2_3D];
  DOF cdof[N_BAS_LAG_2_3D];
  EL        *el;
  REAL      *v = NULL;
  DOF       cdofi;
  int       i, lr_set;
  int       node0, n0;
  const DOF_ADMIN *admin;
  const BAS_FCTS *bas_fcts;

  if (n < 1) return;
  el = list->el_info.el;

  GET_DOF_VEC(v, drv);
  if (!drv->fe_space)
  {
    ERROR("no fe_space in dof_real_vec %s\n", NAME(drv));
    return;
  }
  else if (!drv->fe_space->bas_fcts)
  {
    ERROR("no basis functions in fe_space %s\n", NAME(drv->fe_space));
    return;
  }
  GET_STRUCT(admin, drv->fe_space);
  GET_STRUCT(bas_fcts, drv->fe_space);

  get_dof_indices2_3d(pdof, el, admin, bas_fcts);

  node0 = drv->fe_space->admin->mesh->node[EDGE];
  n0 = admin->n0_dof[EDGE];

/*--------------------------------------------------------------------------*/
/*  values on child[0]                                                      */
/*--------------------------------------------------------------------------*/

  get_dof_indices2_3d(cdof, el->child[0], admin, bas_fcts);

  v[cdof[3]] = (v[pdof[4]]);
  v[cdof[6]] = (0.375*v[pdof[0]] - 0.125*v[pdof[1]]
		+ 0.75*v[pdof[4]]);
  v[cdof[8]] = (0.125*(-v[pdof[0]] - v[pdof[1]]) + 0.25*v[pdof[4]]
		+ 0.5*(v[pdof[5]] + v[pdof[7]]));
  v[cdof[9]] = (0.125*(-v[pdof[0]] - v[pdof[1]]) + 0.25*v[pdof[4]]
		+ 0.5*(v[pdof[6]] + v[pdof[8]]));

/*--------------------------------------------------------------------------*/
/*  values on child[1]                                                      */
/*--------------------------------------------------------------------------*/

  cdofi = el->child[1]->dof[node0+2][n0];
  v[cdofi] = (-0.125*v[pdof[0]] + 0.375*v[pdof[1]]
	      + 0.75*v[pdof[4]]);

/*--------------------------------------------------------------------------*/
/*   adjust neighbour values                                                */
/*--------------------------------------------------------------------------*/

  for (i = 1; i < n; i++)
  {
    el = list[i].el_info.el;
    get_dof_indices2_3d(pdof, el, admin, bas_fcts);

    lr_set = 0;
    if (list[i].neigh[0]  &&  list[i].neigh[0]->no < i)
      lr_set = 1;

    if (list[i].neigh[1]  &&  list[i].neigh[1]->no < i)
      lr_set += 2;

    DEBUG_TEST_EXIT(lr_set, "no values set on both neighbours\n");

/*--------------------------------------------------------------------------*/
/*  values on child[0]                                                      */
/*--------------------------------------------------------------------------*/

    switch (lr_set)
    {
    case 1:
      cdofi = el->child[0]->dof[node0+4][n0];
      v[cdofi] = (0.125*(-v[pdof[0]] - v[pdof[1]]) + 0.25*v[pdof[4]]
		  + 0.5*(v[pdof[5]] + v[pdof[7]]));
      break;
    case 2:
      cdofi = el->child[0]->dof[node0+5][n0];
      v[cdofi] = (0.125*(-v[pdof[0]] - v[pdof[1]]) + 0.25*v[pdof[4]]
		  + 0.5*(v[pdof[6]] + v[pdof[8]]));
    }
  }
  return;
}

static void real_coarse_inter2_3d(DOF_REAL_VEC *drv, RC_LIST_EL *list, int n)
{
  FUNCNAME("real_coarse_inter2_3d");
  EL        *el;
  REAL      *v = NULL;
  int       cdof, pdof;
  const DOF_ADMIN *admin;
  MESH      *mesh = NULL;

  if (n < 1) return;
  el = list->el_info.el;

  GET_DOF_VEC(v, drv);
  if (!drv->fe_space)
  {
    ERROR("no fe_space in dof_real_vec %s\n", NAME(drv));
    return;
  }
  else if (!drv->fe_space->bas_fcts)
  {
    ERROR("no basis functions in fe_space %s\n", NAME(drv->fe_space));
    return;
  }
  GET_STRUCT(admin,drv->fe_space);
  GET_STRUCT(mesh,drv->fe_space);

  cdof = el->child[0]->dof[mesh->node[VERTEX]+3][admin->n0_dof[VERTEX]];
  pdof = el->dof[mesh->node[EDGE]][admin->n0_dof[EDGE]];
  v[pdof] = v[cdof];

  return;
}

static void real_coarse_restr2_3d(DOF_REAL_VEC *drv, RC_LIST_EL *list, int n)
{
  FUNCNAME("real_coarse_restr2_3d");
  DOF pdof[N_BAS_LAG_2_3D];
  DOF cdof[N_BAS_LAG_2_3D];
  EL        *el;
  REAL      *v = NULL;
  DOF       cdofi;
  int       i, lr_set;
  int       node0, n0;
  const DOF_ADMIN *admin;
  const BAS_FCTS *bas_fcts;

  if (n < 1) return;
  el = list->el_info.el;

  GET_DOF_VEC(v, drv);
  if (!drv->fe_space)
  {
    ERROR("no fe_space in dof_real_vec %s\n", NAME(drv));
    return;
  }
  else if (!drv->fe_space->bas_fcts)
  {
    ERROR("no basis functions in fe_space %s\n", NAME(drv->fe_space));
    return;
  }
  GET_STRUCT(admin, drv->fe_space);
  GET_STRUCT(bas_fcts, drv->fe_space);

  get_dof_indices2_3d(pdof, el, admin, bas_fcts);

  node0 = drv->fe_space->admin->mesh->node[EDGE];
  n0 = admin->n0_dof[EDGE];

/*--------------------------------------------------------------------------*/
/*  values on child[0]                                                      */
/*--------------------------------------------------------------------------*/

  get_dof_indices2_3d(cdof, el->child[0], admin, bas_fcts);

  v[pdof[0]] += (0.375*v[cdof[6]] + 0.125*(-v[cdof[8]] - v[cdof[9]]));
  v[pdof[1]] += (0.125*(-v[cdof[6]] - v[cdof[8]] - v[cdof[9]]));
  v[pdof[4]] = (v[cdof[3]] + 0.75*v[cdof[6]]
		+ 0.25*(v[cdof[8]] + v[cdof[9]]));
  v[pdof[5]] += (0.5*v[cdof[8]]);
  v[pdof[6]] += (0.5*v[cdof[9]]);
  v[pdof[7]] += (0.5*v[cdof[8]]);
  v[pdof[8]] += (0.5*v[cdof[9]]);

/*--------------------------------------------------------------------------*/
/*  values on child[1]                                                      */
/*--------------------------------------------------------------------------*/

  get_dof_indices2_3d(cdof, el->child[1], admin, bas_fcts);
  cdofi = el->child[1]->dof[node0+2][n0];

  v[pdof[0]] += (-0.125*v[cdofi]);
  v[pdof[1]] += (0.375*v[cdofi]);
  v[pdof[4]] += (0.75*v[cdofi]);

/*--------------------------------------------------------------------------*/
/*   adjust neighbour values                                                */
/*--------------------------------------------------------------------------*/

  for (i = 1; i < n; i++)
  {
    el = list[i].el_info.el;
    get_dof_indices2_3d(pdof, el, admin, bas_fcts);

    lr_set = 0;
    if (list[i].neigh[0]  &&  list[i].neigh[0]->no < i)
      lr_set = 1;

    if (list[i].neigh[1]  &&  list[i].neigh[1]->no < i)
      lr_set += 2;

    DEBUG_TEST_EXIT(lr_set, "no values set on both neighbours\n");

/*--------------------------------------------------------------------------*/
/*  values on child[0]                                                      */
/*--------------------------------------------------------------------------*/

    get_dof_indices2_3d(cdof, el->child[0], admin, bas_fcts);

    switch(lr_set)
    {
    case 1:
      cdofi = el->child[0]->dof[node0+4][n0];
      v[pdof[0]] += (-0.125*v[cdofi]);
      v[pdof[1]] += (-0.125*v[cdofi]);
      v[pdof[4]] += (0.25*v[cdofi]);
      v[pdof[5]] += (0.5*v[cdofi]);
      v[pdof[7]] += (0.5*v[cdofi]);
      break;
    case 2:
      cdofi = el->child[0]->dof[node0+5][n0];
      v[pdof[0]] += (-0.125*v[cdofi]);
      v[pdof[1]] += (-0.125*v[cdofi]);
      v[pdof[4]] += (0.25*v[cdofi]);
      v[pdof[6]] += (0.5*v[cdofi]);
      v[pdof[8]] += (0.5*v[cdofi]);
      break;
    }
  }
  return;
}

static void real_d_refine_inter2_3d(DOF_REAL_D_VEC *drdv, RC_LIST_EL *list,
				    int n)
{
  FUNCNAME("real_d_refine_inter2_3d");
  DOF pd[N_BAS_LAG_2_3D];
  DOF cd[N_BAS_LAG_2_3D];
  EL        *el;
  REAL_D    *v = NULL;
  DOF       cdi;
  int       i, k, lr_set;
  int       node0, n0;
  const DOF_ADMIN *admin;
  const BAS_FCTS *bas_fcts;

  if (n < 1) return;
  el = list->el_info.el;

  GET_DOF_VEC(v, drdv);
  if (!drdv->fe_space)
  {
    ERROR("no fe_space in dof_real_d_vec %s\n", NAME(drdv));
    return;
  }
  else if (!drdv->fe_space->bas_fcts)
  {
    ERROR("no basis functions in fe_space %s\n", NAME(drdv->fe_space));
    return;
  }
  GET_STRUCT(admin, drdv->fe_space);
  GET_STRUCT(bas_fcts, drdv->fe_space);

  get_dof_indices2_3d(pd, el, admin, bas_fcts);

  node0 = drdv->fe_space->admin->mesh->node[EDGE];
  n0 = admin->n0_dof[EDGE];

/*--------------------------------------------------------------------------*/
/*  values on child[0]                                                      */
/*--------------------------------------------------------------------------*/

  get_dof_indices2_3d(cd, el->child[0], admin, bas_fcts);

  for (k = 0; k < DIM_OF_WORLD; k++)
  {
    v[cd[3]][k] = (v[pd[4]][k]);
    v[cd[6]][k] = (0.375*v[pd[0]][k] - 0.125*v[pd[1]][k] + 0.75*v[pd[4]][k]);
    v[cd[8]][k] = (0.125*(-v[pd[0]][k] - v[pd[1]][k]) + 0.25*v[pd[4]][k]
		   + 0.5*(v[pd[5]][k] + v[pd[7]][k]));
    v[cd[9]][k] = (0.125*(-v[pd[0]][k] - v[pd[1]][k]) + 0.25*v[pd[4]][k]
		   + 0.5*(v[pd[6]][k] + v[pd[8]][k]));
  }
/*--------------------------------------------------------------------------*/
/*  values on child[1]                                                      */
/*--------------------------------------------------------------------------*/

  cdi = el->child[1]->dof[node0+2][n0];
  for (k = 0; k < DIM_OF_WORLD; k++)
    v[cdi][k] = (-0.125*v[pd[0]][k] + 0.375*v[pd[1]][k] + 0.75*v[pd[4]][k]);

/*--------------------------------------------------------------------------*/
/*   adjust neighbour values                                                */
/*--------------------------------------------------------------------------*/

  for (i = 1; i < n; i++)
  {
    el = list[i].el_info.el;
    get_dof_indices2_3d(pd, el, admin, bas_fcts);

    lr_set = 0;
    if (list[i].neigh[0]  &&  list[i].neigh[0]->no < i)
      lr_set = 1;

    if (list[i].neigh[1]  &&  list[i].neigh[1]->no < i)
      lr_set += 2;

    DEBUG_TEST_EXIT(lr_set, "no values set on both neighbours\n");

/*--------------------------------------------------------------------------*/
/*  values on child[0]                                                      */
/*--------------------------------------------------------------------------*/

    switch (lr_set)
    {
    case 1:
      cdi = el->child[0]->dof[node0+4][n0];
      for (k = 0; k < DIM_OF_WORLD; k++)
	v[cdi][k] = (0.125*(-v[pd[0]][k] - v[pd[1]][k]) + 0.25*v[pd[4]][k]
		     + 0.5*(v[pd[5]][k] + v[pd[7]][k]));
      break;
    case 2:
      cdi = el->child[0]->dof[node0+5][n0];
      for (k = 0; k < DIM_OF_WORLD; k++)
	v[cdi][k] = (0.125*(-v[pd[0]][k] - v[pd[1]][k]) + 0.25*v[pd[4]][k]
		     + 0.5*(v[pd[6]][k] + v[pd[8]][k]));
    }
  }
  return;
}

static void real_d_coarse_inter2_3d(DOF_REAL_D_VEC *drdv, RC_LIST_EL *list,
				    int n)
{
  FUNCNAME("real_d_coarse_inter2_3d");
  EL        *el;
  REAL_D    *v = NULL;
  int       cd, pd, k;
  const DOF_ADMIN *admin;
  MESH      *mesh = NULL;

  if (n < 1) return;
  el = list->el_info.el;

  GET_DOF_VEC(v, drdv);
  if (!drdv->fe_space)
  {
    ERROR("no fe_space in dof_real_d_vec %s\n", NAME(drdv));
    return;
  }
  else if (!drdv->fe_space->bas_fcts)
  {
    ERROR("no basis functions in fe_space %s\n", NAME(drdv->fe_space));
    return;
  }
  GET_STRUCT(admin,drdv->fe_space);
  GET_STRUCT(mesh,drdv->fe_space);

  cd = el->child[0]->dof[mesh->node[VERTEX]+3][admin->n0_dof[VERTEX]];
  pd = el->dof[mesh->node[EDGE]][admin->n0_dof[EDGE]];
  for (k = 0; k < DIM_OF_WORLD; k++)
    v[pd][k] = v[cd][k];

  return;
}

static void real_d_coarse_restr2_3d(DOF_REAL_D_VEC *drdv, RC_LIST_EL *list,
				    int n)
{
  FUNCNAME("real_d_coarse_restr2_3d");
  DOF pd[N_BAS_LAG_2_3D];
  DOF cd[N_BAS_LAG_2_3D];
  EL        *el;
  REAL_D    *v = NULL;
  DOF       cdi;
  int       i, k, lr_set;
  int       node0, n0;
  const DOF_ADMIN *admin;
  const BAS_FCTS *bas_fcts;

  if (n < 1) return;
  el = list->el_info.el;

  GET_DOF_VEC(v, drdv);
  if (!drdv->fe_space)
  {
    ERROR("no fe_space in dof_real_d_vec %s\n", NAME(drdv));
    return;
  }
  else if (!drdv->fe_space->bas_fcts)
  {
    ERROR("no basis functions in fe_space %s\n", NAME(drdv->fe_space));
    return;
  }
  GET_STRUCT(admin, drdv->fe_space);
  GET_STRUCT(bas_fcts, drdv->fe_space);

  get_dof_indices2_3d(pd, el, admin, bas_fcts);

  node0 = drdv->fe_space->admin->mesh->node[EDGE];
  n0 = admin->n0_dof[EDGE];

/*--------------------------------------------------------------------------*/
/*  values on child[0]                                                      */
/*--------------------------------------------------------------------------*/

  get_dof_indices2_3d(cd, el->child[0], admin, bas_fcts);

  for (k = 0; k < DIM_OF_WORLD; k++)
  {
    v[pd[0]][k] += (0.375*v[cd[6]][k] + 0.125*(-v[cd[8]][k] - v[cd[9]][k]));
    v[pd[1]][k] += (0.125*(-v[cd[6]][k] - v[cd[8]][k] - v[cd[9]][k]));
    v[pd[4]][k] = (v[cd[3]][k] + 0.75*v[cd[6]][k]
		   + 0.25*(v[cd[8]][k] + v[cd[9]][k]));
    v[pd[5]][k] += (0.5*v[cd[8]][k]);
    v[pd[6]][k] += (0.5*v[cd[9]][k]);
    v[pd[7]][k] += (0.5*v[cd[8]][k]);
    v[pd[8]][k] += (0.5*v[cd[9]][k]);
  }
/*--------------------------------------------------------------------------*/
/*  values on child[1]                                                      */
/*--------------------------------------------------------------------------*/

  get_dof_indices2_3d(cd, el->child[1], admin, bas_fcts);
  cdi = el->child[1]->dof[node0+2][n0];

  for (k = 0; k < DIM_OF_WORLD; k++)
  {
    v[pd[0]][k] += (-0.125*v[cdi][k]);
    v[pd[1]][k] += (0.375*v[cdi][k]);
    v[pd[4]][k] += (0.75*v[cdi][k]);
  }

/*--------------------------------------------------------------------------*/
/*   adjust neighbour values                                                */
/*--------------------------------------------------------------------------*/

  for (i = 1; i < n; i++)
  {
    el = list[i].el_info.el;
    get_dof_indices2_3d(pd, el, admin, bas_fcts);

    lr_set = 0;
    if (list[i].neigh[0]  &&  list[i].neigh[0]->no < i)
      lr_set = 1;

    if (list[i].neigh[1]  &&  list[i].neigh[1]->no < i)
      lr_set += 2;

    DEBUG_TEST_EXIT(lr_set, "no values set on both neighbours\n");

/*--------------------------------------------------------------------------*/
/*  values on child[0]                                                      */
/*--------------------------------------------------------------------------*/

    get_dof_indices2_3d(cd, el->child[0], admin, bas_fcts);

    switch(lr_set)
    {
    case 1:
      cdi = el->child[0]->dof[node0+4][n0];
      for (k = 0; k < DIM_OF_WORLD; k++)
      {
	v[pd[0]][k] += (-0.125*v[cdi][k]);
	v[pd[1]][k] += (-0.125*v[cdi][k]);
	v[pd[4]][k] += (0.25*v[cdi][k]);
	v[pd[5]][k] += (0.5*v[cdi][k]);
	v[pd[7]][k] += (0.5*v[cdi][k]);
      }
      break;
    case 2:
      cdi = el->child[0]->dof[node0+5][n0];
      for (k = 0; k < DIM_OF_WORLD; k++)
      {
	v[pd[0]][k] += (-0.125*v[cdi][k]);
	v[pd[1]][k] += (-0.125*v[cdi][k]);
	v[pd[4]][k] += (0.25*v[cdi][k]);
	v[pd[6]][k] += (0.5*v[cdi][k]);
	v[pd[8]][k] += (0.5*v[cdi][k]);
      }
      break;
    }
  }
  return;
}

static const BAS_FCT      phi2_3d[N_BAS_LAG_2_3D]     = {
  phi2v0_3d, phi2v1_3d,
  phi2v2_3d, phi2v3_3d,
  phi2e0_3d, phi2e1_3d,
  phi2e2_3d, phi2e3_3d,
  phi2e4_3d, phi2e5_3d
};
static const GRD_BAS_FCT  grd_phi2_3d[N_BAS_LAG_2_3D] = {
  grd_phi2v0_3d, grd_phi2v1_3d,
  grd_phi2v2_3d, grd_phi2v3_3d,
  grd_phi2e0_3d, grd_phi2e1_3d,
  grd_phi2e2_3d, grd_phi2e3_3d,
  grd_phi2e4_3d, grd_phi2e5_3d
};
static const D2_BAS_FCT   D2_phi2_3d[N_BAS_LAG_2_3D]  = {
  D2_phi2v0_3d, D2_phi2v1_3d,
  D2_phi2v2_3d, D2_phi2v3_3d,
  D2_phi2e0_3d, D2_phi2e1_3d,
  D2_phi2e2_3d, D2_phi2e3_3d,
  D2_phi2e4_3d, D2_phi2e5_3d
};

/* For each wall the mapping from the wall basis functions to the
 * local DOFs on the reference element. See also submesh.c.
 *
 * Meaning of the dimensions is the same as in the BAS_FCTS structure
 *
 * [type][orientation][wall][bas_fcts]
 */
static const int trace_mapping_lag_2_3d[2][2][N_WALLS_3D][N_BAS_LAG_2_2D] = {
  { 
    { {3, 1, 2, 7, 9, 8},
      {2, 0, 3, 6, 9, 5},
      {0, 1, 3, 8, 6, 4},
      {1, 0, 2, 5, 7, 4} }, /* t = 0, o = + */
    { {1, 3, 2, 9, 7, 8},
      {0, 2, 3, 9, 6, 5},
      {1, 0, 3, 6, 8, 4},
      {0, 1, 2, 7, 5, 4} }  /* t = 0, o = - */
  },
  {
    { {1, 2, 3, 9, 8, 7},
      {2, 0, 3, 6, 9, 5},
      {0, 1, 3, 8, 6, 4},
      {1, 0, 2, 5, 7, 4} }, /* t = 1, o = + */
    { {2, 1, 3, 8, 9, 7},
      {0, 2, 3, 9, 6, 5},
      {1, 0, 3, 6, 8, 4},
      {0, 1, 2, 7, 5, 4} }  /* t = 1, o = - */
  }
};

static const BAS_FCTS lagrange2_3d = {
  "lagrange2_3d", 3, 1, N_BAS_LAG_2_3D, N_BAS_LAG_2_3D, 2,
  {1,0,1,0},/* VERTEX, CENTER, EDGE,FACE*/
  -1, /* trace_admin */
  INIT_BFCTS_CHAIN(lagrange2_3d),
  INIT_ELEMENT_INITIALIZER(NULL, FILL_NOTHING), /* init_element + fill-flags */
  phi2_3d, grd_phi2_3d, D2_phi2_3d,
  NULL, NULL, /* third and fourth derivatives */
  NULL, NULL, NULL, false, /* phi_d etc. */  
  /********************/
  &lagrange2_2d, /* trace space */
  { { { trace_mapping_lag_2_3d[0][0][0], /* t = 0, o = + */
	trace_mapping_lag_2_3d[0][0][1],
	trace_mapping_lag_2_3d[0][0][2],
	trace_mapping_lag_2_3d[0][0][3] },
      { trace_mapping_lag_2_3d[0][1][0], /* t = 0, o = - */
	trace_mapping_lag_2_3d[0][1][1],
	trace_mapping_lag_2_3d[0][1][2],
	trace_mapping_lag_2_3d[0][1][3] } },
    { { trace_mapping_lag_2_3d[1][0][0], /* t = 1, o = + */
	trace_mapping_lag_2_3d[1][0][1],
	trace_mapping_lag_2_3d[1][0][2],
	trace_mapping_lag_2_3d[1][0][3] },
      { trace_mapping_lag_2_3d[1][1][0], /* t = 1, o = - */
	trace_mapping_lag_2_3d[1][1][1],
	trace_mapping_lag_2_3d[1][1][2],
	trace_mapping_lag_2_3d[1][1][3] } } }, /* trace mapping */
  { N_BAS_LAG_2_2D,
    N_BAS_LAG_2_2D,
    N_BAS_LAG_2_2D,
    N_BAS_LAG_2_2D }, /* n_trace_bas_fcts */
  get_dof_indices2_3d,
  get_bound2_3d,
  interpol2_3d,
  interpol_d_2_3d,
  interpol_dow_2_3d,
  get_int_vec2_3d,
  get_real_vec2_3d,
  get_real_d_vec2_3d,
  (GET_REAL_VEC_D_TYPE)get_real_d_vec2_3d,
  get_uchar_vec2_3d,
  get_schar_vec2_3d,
  get_ptr_vec2_3d,
  get_real_dd_vec2_3d,
  real_refine_inter2_3d,
  real_coarse_inter2_3d,
  real_coarse_restr2_3d,
  real_d_refine_inter2_3d,
  real_d_coarse_inter2_3d,
  real_d_coarse_restr2_3d,
  (REF_INTER_FCT_D)real_d_refine_inter2_3d,
  (REF_INTER_FCT_D)real_d_coarse_inter2_3d,
  (REF_INTER_FCT_D)real_d_coarse_restr2_3d,
  (void *)&lag_2_3d_data
};
