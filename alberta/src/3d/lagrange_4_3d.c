/*--------------------------------------------------------------------------*/
/* ALBERTA:  an Adaptive multi Level finite element toolbox using           */
/*           Bisectioning refinement and Error control by Residual          */
/*           Techniques for scientific Applications                         */
/*                                                                          */
/* file:     lagrange_4_3d.c                                                */
/*                                                                          */
/* description:  piecewise quartic Lagrange elements in 3d                  */
/*                                                                          */
/*--------------------------------------------------------------------------*/
/*                                                                          */
/*  authors:   Alfred Schmidt                                               */
/*             Zentrum fuer Technomathematik                                */
/*             Fachbereich 3 Mathematik/Informatik                          */
/*             Universitaet Bremen                                          */
/*             Bibliothekstr. 2                                             */
/*             D-28359 Bremen, Germany                                      */
/*                                                                          */
/*             Kunibert G. Siebert                                          */
/*             Institut fuer Mathematik                                     */
/*             Universitaet Augsburg                                        */
/*             Universitaetsstr. 14                                         */
/*             D-86159 Augsburg, Germany                                    */
/*                                                                          */
/*  http://www.mathematik.uni-freiburg.de/IAM/ALBERTA                       */
/*                                                                          */
/*  (c) by A. Schmidt and K.G. Siebert (1996-2003)                          */
/*                                                                          */
/*--------------------------------------------------------------------------*/

static const REAL_B bary4_3d[N_BAS_LAG_4_3D] = {
  {    1.0,     0.0,     0.0,     0.0}, /* vertices */
  {    0.0,     1.0,     0.0,     0.0},
  {    0.0,     0.0,     1.0,     0.0},
  {    0.0,     0.0,     0.0,     1.0},
  {3.0/4.0, 1.0/4.0,     0.0,     0.0}, /* edge 0 */
  {    0.5,     0.5,     0.0,     0.0},
  {1.0/4.0, 3.0/4.0,     0.0,     0.0},
  {3.0/4.0,     0.0, 1.0/4.0,     0.0}, /* edge 1 */
  {    0.5,     0.0,     0.5,     0.0},
  {1.0/4.0,     0.0, 3.0/4.0,     0.0},
  {3.0/4.0,     0.0,     0.0, 1.0/4.0}, /* edge 2 */
  {    0.5,     0.0,     0.0,     0.5},
  {1.0/4.0,     0.0,     0.0, 3.0/4.0},
  {    0.0, 3.0/4.0, 1.0/4.0,     0.0}, /* edge 3 */
  {    0.0,     0.5,     0.5,     0.0},
  {    0.0, 1.0/4.0, 3.0/4.0,     0.0},
  {    0.0, 3.0/4.0,     0.0, 1.0/4.0}, /* edge 4 */
  {    0.0,     0.5,     0.0,     0.5},
  {    0.0, 1.0/4.0,     0.0, 3.0/4.0},
  {    0.0,     0.0, 3.0/4.0, 1.0/4.0}, /* edge 5 */
  {    0.0,     0.0,     0.5,     0.5},
  {    0.0,     0.0, 1.0/4.0, 3.0/4.0},
  {    0.0, 1.0/2.0, 1.0/4.0, 1.0/4.0}, /* face 0 */
  {    0.0, 1.0/4.0, 1.0/2.0, 1.0/4.0},
  {    0.0, 1.0/4.0, 1.0/4.0, 1.0/2.0},
  {1.0/2.0,    0.0,  1.0/4.0, 1.0/4.0}, /* face 1 */
  {1.0/4.0,    0.0,  1.0/2.0, 1.0/4.0},
  {1.0/4.0,    0.0,  1.0/4.0, 1.0/2.0},
  {1.0/2.0, 1.0/4.0,    0.0,  1.0/4.0}, /* face 2 */
  {1.0/4.0, 1.0/2.0,    0.0,  1.0/4.0},
  {1.0/4.0, 1.0/4.0,    0.0,  1.0/2.0},
  {1.0/2.0, 1.0/4.0, 1.0/4.0,     0.0}, /* face 3 */
  {1.0/4.0, 1.0/2.0, 1.0/4.0,     0.0},
  {1.0/4.0, 1.0/4.0, 1.0/2.0,     0.0},
  {1.0/4.0, 1.0/4.0, 1.0/4.0, 1.0/4.0}  /* center */
};

static LAGRANGE_DATA lag_4_3d_data = {
  bary4_3d,
  NULL /* lumping_quad */,
};

/*--------------------------------------------------------------------------*/
/*  basisfunction at vertex 0                                               */
/*--------------------------------------------------------------------------*/

static REAL phi4v0_3d(const REAL_B l, const BAS_FCTS *thisptr)
{
  return (((32.0*l[0] - 48.0)*l[0] + 22.0)*l[0] - 3.0)*l[0]/3.0;
}

static const REAL *grd_phi4v0_3d(const REAL_B l, const BAS_FCTS *thisptr)
{
  static REAL_B grd;
  grd[0] = ((128.0*l[0] - 144.0)*l[0] + 44.0)*l[0]/3.0 - 1.0;
  return (const REAL *) grd;
}

static const REAL_B *D2_phi4v0_3d(const REAL_B l, const BAS_FCTS *thisptr)
{
  static REAL_BB D2 = {{0.0}};
  D2[0][0] = (128.0*l[0] - 96.0)*l[0] + 44.0/3.0;
  return (const REAL_B *) D2;
}

static const REAL_BB *D3_phi4v0_3d(const REAL_B l, const BAS_FCTS *thisptr)
{
  static REAL_BBB D3;

  D3[0][0][0] = 256.0*l[0] - 96.0;

  return (const REAL_BB *)D3;
}

static const REAL_BBB *D4_phi4v0_3d(const REAL_B l, const BAS_FCTS *thisptr)
{
  static REAL_BBBB D4;

  D4[0][0][0][0] = 256.0;

  return (const REAL_BBB *)D4;
}

/*--------------------------------------------------------------------------*/
/*  basisfunction at vertex 1                                               */
/*--------------------------------------------------------------------------*/

static REAL phi4v1_3d(const REAL_B l, const BAS_FCTS *thisptr)
{
  return (((32.0*l[1] - 48.0)*l[1] + 22.0)*l[1] - 3.0)*l[1]/3.0;
}

static const REAL *grd_phi4v1_3d(const REAL_B l, const BAS_FCTS *thisptr)
{
  static REAL_B grd;
  grd[1] = ((128.0*l[1] - 144.0)*l[1] + 44.0)*l[1]/3.0 - 1.0;
  return (const REAL *) grd;
}

static const REAL_B *D2_phi4v1_3d(const REAL_B l, const BAS_FCTS *thisptr)
{
  static REAL_BB D2;
  D2[1][1] = (128.0*l[1] - 96.0)*l[1] + 44.0/3.0;
  return (const REAL_B *) D2;
}

static const REAL_BB *D3_phi4v1_3d(const REAL_B l, const BAS_FCTS *thisptr)
{
  static REAL_BBB D3;

  D3[1][1][1] = 256.0*l[1] - 96.0;

  return (const REAL_BB *)D3;
}

static const REAL_BBB *D4_phi4v1_3d(const REAL_B l, const BAS_FCTS *thisptr)
{
  static REAL_BBBB D4;

  D4[1][1][1][1] = 256.0;

  return (const REAL_BBB *)D4;
}

/*--------------------------------------------------------------------------*/
/*  basisfunction at vertex 2                                               */
/*--------------------------------------------------------------------------*/

static REAL phi4v2_3d(const REAL_B l, const BAS_FCTS *thisptr)
{
  return (((32.0*l[2] - 48.0)*l[2] + 22.0)*l[2] - 3.0)*l[2]/3.0;
}

static const REAL *grd_phi4v2_3d(const REAL_B l, const BAS_FCTS *thisptr)
{
  static REAL_B grd;
  grd[2] = ((128.0*l[2] - 144.0)*l[2] + 44.0)*l[2]/3.0 - 1.0;
  return (const REAL *) grd;
}

static const REAL_B *D2_phi4v2_3d(const REAL_B l, const BAS_FCTS *thisptr)
{
  static REAL_BB D2;
  D2[2][2] = (128.0*l[2] - 96.0)*l[2] + 44.0/3.0;
  return (const REAL_B *) D2;
}

static const REAL_BB *D3_phi4v2_3d(const REAL_B l, const BAS_FCTS *thisptr)
{
  static REAL_BBB D3;

  D3[2][2][2] = 256.0*l[2] - 96.0;

  return (const REAL_BB *)D3;
}

static const REAL_BBB *D4_phi4v2_3d(const REAL_B l, const BAS_FCTS *thisptr)
{
  static REAL_BBBB D4;

  D4[2][2][2][2] = 256.0;

  return (const REAL_BBB *)D4;
}

/*--------------------------------------------------------------------------*/
/*  basisfunction at vertex 3                                               */
/*--------------------------------------------------------------------------*/

static REAL phi4v3_3d(const REAL_B l, const BAS_FCTS *thisptr)
{
  return (((32.0*l[3] - 48.0)*l[3] + 22.0)*l[3] - 3.0)*l[3]/3.0;
}

static const REAL *grd_phi4v3_3d(const REAL_B l, const BAS_FCTS *thisptr)
{
  static REAL_B grd;
  grd[3] = ((128.0*l[3] - 144.0)*l[3] + 44.0)*l[3]/3.0 - 1.0;
  return (const REAL *) grd;
}

static const REAL_B *D2_phi4v3_3d(const REAL_B l, const BAS_FCTS *thisptr)
{
  static REAL_BB D2;
  D2[3][3] = (128.0*l[3] - 96.0)*l[3] + 44.0/3.0;
  return (const REAL_B *) D2;
}

static const REAL_BB *D3_phi4v3_3d(const REAL_B l, const BAS_FCTS *thisptr)
{
  static REAL_BBB D3;

  D3[3][3][3] = 256.0*l[3] - 96.0;

  return (const REAL_BB *)D3;
}

static const REAL_BBB *D4_phi4v3_3d(const REAL_B l, const BAS_FCTS *thisptr)
{
  static REAL_BBBB D4;

  D4[3][3][3][3] = 256.0;

  return (const REAL_BBB *)D4;
}

/*--------------------------------------------------------------------------*/
/*  basisfunction 0 at edge 0 (between vertex 0 and 1)                      */
/*--------------------------------------------------------------------------*/

static REAL phi4e00_3d(const REAL_B l, const BAS_FCTS *thisptr)
{
  return ((128.0*l[0] - 96.0)*l[0] + 16.0)*l[0]*l[1]/3.0;
}

static const REAL *grd_phi4e00_3d(const REAL_B l, const BAS_FCTS *thisptr)
{
  static REAL_B grd;
  grd[0] = ((128*l[0] - 64.0)*l[0] + 16.0/3.0)*l[1];
  grd[1] = ((128*l[0] - 96.0)*l[0] + 16.0)*l[0]/3.0;
  return (const REAL *) grd;
}

static const REAL_B *D2_phi4e00_3d(const REAL_B l, const BAS_FCTS *thisptr)
{
  static REAL_BB D2;
  D2[0][0] = (256.0*l[0] - 64.0)*l[1];
  D2[0][1] = D2[1][0] = (128.0*l[0] - 64.0)*l[0] + 16.0/3.0;
  return (const REAL_B *) D2;
}

static const REAL_BB *D3_phi4e00_3d(const REAL_B l, const BAS_FCTS *thisptr)
{
  static REAL_BBB D3;

  D3[0][0][0] = 256.0*l[1];
  D3[1][0][0] = D3[0][1][0] = D3[0][0][1] = 256.0*l[0] - 64.0;

  return (const REAL_BB *)D3;
}

static const REAL_BBB *D4_phi4e00_3d(const REAL_B l, const BAS_FCTS *thisptr)
{
  static REAL_BBBB D4;

  D4[0][0][0][1] =
    D4[0][0][1][0] =
    D4[0][1][0][0] =
    D4[1][0][0][0] = 256.0;

  return (const REAL_BBB *)D4;
}

/*--------------------------------------------------------------------------*/
/*  basisfunction 1 at edge 0                                               */
/*--------------------------------------------------------------------------*/

static REAL phi4e01_3d(const REAL_B l, const BAS_FCTS *thisptr)
{
  return (4.0*l[0] - 1.0)*l[0]*(4.0*l[1] - 1.0)*l[1]*4.0;
}

static const REAL *grd_phi4e01_3d(const REAL_B l, const BAS_FCTS *thisptr)
{
  static REAL_B grd;
  grd[0] = 4.0*(8.0*l[0] - 1.0)*l[1]*(4.0*l[1] - 1.0);
  grd[1] = 4.0*l[0]*(4.0*l[0] - 1.0)*(8.0*l[1] - 1.0);
  return (const REAL *) grd;
}

static const REAL_B *D2_phi4e01_3d(const REAL_B l, const BAS_FCTS *thisptr)
{
  static REAL_BB D2;
  D2[0][0] = 32.0*l[1]*(4.0*l[1] - 1.0);
  D2[0][1] = D2[1][0] = 4.0*(8.0*l[0] - 1.0)*(8.0*l[1] - 1.0);
  D2[1][1] = 32.0*l[0]*(4.0*l[0] - 1.0);
  return (const REAL_B *) D2;
}

static const REAL_BB *D3_phi4e01_3d(const REAL_B l, const BAS_FCTS *thisptr)
{
  static REAL_BBB D3;

  D3[0][0][1] = D3[0][1][0] = D3[1][0][0] = 256.0*l[1] - 32.0;
  D3[0][1][1] = D3[1][0][1] = D3[1][1][0] = 256.0*l[0] - 32.0;

  return (const REAL_BB *)D3;
}

static const REAL_BBB *D4_phi4e01_3d(const REAL_B l, const BAS_FCTS *thisptr)
{
  static REAL_BBBB D4;

  D4[0][0][1][1] =
    D4[1][1][0][0] =
    D4[0][1][0][1] =
    D4[1][0][1][0] =
    D4[1][0][0][1] =
    D4[0][1][1][0] = 256.0;

  return (const REAL_BBB *)D4;
}

/*--------------------------------------------------------------------------*/
/*  basisfunction 2 at edge 0                                               */
/*--------------------------------------------------------------------------*/

static REAL phi4e02_3d(const REAL_B l, const BAS_FCTS *thisptr)
{
  return ((128.0*l[1] - 96.0)*l[1] + 16.0)*l[1]*l[0]/3.0;
}

static const REAL *grd_phi4e02_3d(const REAL_B l, const BAS_FCTS *thisptr)
{
  static REAL_B grd;
  grd[0] = ((128*l[1] - 96.0)*l[1] + 16.0)*l[1]/3.0;
  grd[1] = ((128*l[1] - 64.0)*l[1] + 16.0/3.0)*l[0];
  return (const REAL *) grd;
}

static const REAL_B *D2_phi4e02_3d(const REAL_B l, const BAS_FCTS *thisptr)
{
  static REAL_BB D2;
  D2[1][1] = (256.0*l[1] - 64.0)*l[0];
  D2[0][1] = D2[1][0] = (128.0*l[1] - 64.0)*l[1] + 16.0/3.0;
  return (const REAL_B *) D2;
}

static const REAL_BB *D3_phi4e02_3d(const REAL_B l, const BAS_FCTS *thisptr)
{
  static REAL_BBB D3;

  D3[1][1][1] = 256.0*l[0];
  D3[0][1][1] = D3[1][0][1] = D3[1][1][0] = 256.0*l[1] - 64.0;

  return (const REAL_BB *)D3;
}

static const REAL_BBB *D4_phi4e02_3d(const REAL_B l, const BAS_FCTS *thisptr)
{
  static REAL_BBBB D4;

  D4[0][1][1][1] =
    D4[1][0][1][1] =
    D4[1][1][0][1] =
    D4[1][1][1][0] = 256.0;

  return (const REAL_BBB *)D4;
}

/*--------------------------------------------------------------------------*/
/*  basisfunction 0 at edge 1                                               */
/*--------------------------------------------------------------------------*/

static REAL phi4e10_3d(const REAL_B l, const BAS_FCTS *thisptr)
{
  return ((128.0*l[0] - 96.0)*l[0] + 16.0)*l[0]*l[2]/3.0;
}

static const REAL *grd_phi4e10_3d(const REAL_B l, const BAS_FCTS *thisptr)
{
  static REAL_B grd;
  grd[0] = ((128*l[0] - 64.0)*l[0] + 16.0/3.0)*l[2];
  grd[2] = ((128*l[0] - 96.0)*l[0] + 16.0)*l[0]/3.0;
  return (const REAL *) grd;
}

static const REAL_B *D2_phi4e10_3d(const REAL_B l, const BAS_FCTS *thisptr)
{
  static REAL_BB D2;
  D2[0][0] = (256.0*l[0] - 64.0)*l[2];
  D2[0][2] = D2[2][0] = (128.0*l[0] - 64.0)*l[0] + 16.0/3.0;
  return (const REAL_B *) D2;
}

static const REAL_BB *D3_phi4e10_3d(const REAL_B l, const BAS_FCTS *thisptr)
{
  static REAL_BBB D3;

  D3[0][0][0] = 256.0*l[2];
  D3[0][0][2] = D3[0][2][0] = D3[2][0][0] = 256.0*l[0] - 64.0;

  return (const REAL_BB *)D3;
}

static const REAL_BBB *D4_phi4e10_3d(const REAL_B l, const BAS_FCTS *thisptr)
{
  static REAL_BBBB D4;

  D4[2][0][0][0] =
  D4[0][2][0][0] =
  D4[0][0][2][0] =
  D4[0][0][0][2] = 256.0;

  return (const REAL_BBB *)D4;
}

/*--------------------------------------------------------------------------*/
/*  basisfunction 1 at edge 1                                               */
/*--------------------------------------------------------------------------*/

static REAL phi4e11_3d(const REAL_B l, const BAS_FCTS *thisptr)
{
  return (4.0*l[0] - 1.0)*l[0]*(4.0*l[2] - 1.0)*l[2]*4.0;
}

static const REAL *grd_phi4e11_3d(const REAL_B l, const BAS_FCTS *thisptr)
{
  static REAL_B grd;
  grd[0] = 4.0*(8.0*l[0] - 1.0)*l[2]*(4.0*l[2] - 1.0);
  grd[2] = 4.0*l[0]*(4.0*l[0] - 1.0)*(8.0*l[2] - 1.0);

  return (const REAL *) grd;
}

static const REAL_B *D2_phi4e11_3d(const REAL_B l, const BAS_FCTS *thisptr)
{
  static REAL_BB D2;
  D2[0][0] = 32.0*l[2]*(4.0*l[2] - 1.0);
  D2[0][2] = D2[2][0] = 4.0*(8.0*l[0] - 1.0)*(8.0*l[2] - 1.0);
  D2[2][2] = 32.0*l[0]*(4.0*l[0] - 1.0);
  return (const REAL_B *) D2;
}

static const REAL_BB *D3_phi4e11_3d(const REAL_B l, const BAS_FCTS *thisptr)
{
  static REAL_BBB D3;

  D3[0][0][2] = D3[0][2][0] = D3[2][0][0] = 256.0*l[2] - 32.0;
  D3[0][2][2] = D3[2][0][2] = D3[2][2][0] = 256.0*l[0] - 32.0;

  return (const REAL_BB *)D3;
}

static const REAL_BBB *D4_phi4e11_3d(const REAL_B l, const BAS_FCTS *thisptr)
{
  static REAL_BBBB D4;

  D4[0][0][2][2] =
    D4[2][2][0][0] =
    D4[0][2][0][2] =
    D4[2][0][2][0] =
    D4[2][0][0][2] =
    D4[0][2][2][0] = 256.0;

  return (const REAL_BBB *)D4;
}

/*--------------------------------------------------------------------------*/
/*  basisfunction 2 at edge 1                                               */
/*--------------------------------------------------------------------------*/

static REAL phi4e12_3d(const REAL_B l, const BAS_FCTS *thisptr)
{
  return ((128.0*l[2] - 96.0)*l[2] + 16.0)*l[2]*l[0]/3.0;
}

static const REAL *grd_phi4e12_3d(const REAL_B l, const BAS_FCTS *thisptr)
{
  static REAL_B grd;
  grd[0] = ((128*l[2] - 96.0)*l[2] + 16.0)*l[2]/3.0;
  grd[2] = ((128*l[2] - 64.0)*l[2] + 16.0/3.0)*l[0];
  return (const REAL *) grd;
}

static const REAL_B *D2_phi4e12_3d(const REAL_B l, const BAS_FCTS *thisptr)
{
  static REAL_BB D2;
  D2[2][2] = (256.0*l[2] - 64.0)*l[0];
  D2[0][2] = D2[2][0] = (128.0*l[2] - 64.0)*l[2] + 16.0/3.0;
  return (const REAL_B *) D2;
}

static const REAL_BB *D3_phi4e12_3d(const REAL_B l, const BAS_FCTS *thisptr)
{
  static REAL_BBB D3;

  D3[2][2][2] = 256.0*l[0];
  D3[0][2][2] = D3[2][0][2] = D3[2][2][0] = 256.0*l[2] - 64.0;

  return (const REAL_BB *)D3;
}

static const REAL_BBB *D4_phi4e12_3d(const REAL_B l, const BAS_FCTS *thisptr)
{
  static REAL_BBBB D4;

  D4[0][2][2][2] =
    D4[2][0][2][2] =
    D4[2][2][0][2] =
    D4[2][2][2][0] = 256.0;

  return (const REAL_BBB *)D4;
}

/*--------------------------------------------------------------------------*/
/*  basisfunction 0 at edge 2                                               */
/*--------------------------------------------------------------------------*/

static REAL phi4e20_3d(const REAL_B l, const BAS_FCTS *thisptr)
{
  return ((128.0*l[0] - 96.0)*l[0] + 16.0)*l[0]*l[3]/3.0;
}

static const REAL *grd_phi4e20_3d(const REAL_B l, const BAS_FCTS *thisptr)
{
  static REAL_B grd;
  grd[0] = ((128*l[0] - 64.0)*l[0] + 16.0/3.0)*l[3];
  grd[3] = ((128*l[0] - 96.0)*l[0] + 16.0)*l[0]/3.0;
  return (const REAL *) grd;
}

static const REAL_B *D2_phi4e20_3d(const REAL_B l, const BAS_FCTS *thisptr)
{
  static REAL_BB D2;
  D2[0][0] = (256.0*l[0] - 64.0)*l[3];
  D2[0][3] = D2[3][0] = (128.0*l[0] - 64.0)*l[0] + 16.0/3.0;
  return (const REAL_B *) D2;
}

static const REAL_BB *D3_phi4e20_3d(const REAL_B l, const BAS_FCTS *thisptr)
{
  static REAL_BBB D3;

  D3[0][0][0] = 256.0*l[3];
  D3[0][0][3] = D3[0][3][0] = D3[3][0][0] = 256.0*l[0] - 64.0;

  return (const REAL_BB *)D3;
}

static const REAL_BBB *D4_phi4e20_3d(const REAL_B l, const BAS_FCTS *thisptr)
{
  static REAL_BBBB D4;

  D4[3][0][0][0] =
  D4[0][3][0][0] =
  D4[0][0][3][0] =
  D4[0][0][0][3] = 256.0;

  return (const REAL_BBB *)D4;
}

/*--------------------------------------------------------------------------*/
/*  basisfunction 1 at edge 2                                               */
/*--------------------------------------------------------------------------*/

static REAL phi4e21_3d(const REAL_B l, const BAS_FCTS *thisptr)
{
  return (4.0*l[0] - 1.0)*l[0]*(4.0*l[3] - 1.0)*l[3]*4.0;
}

static const REAL *grd_phi4e21_3d(const REAL_B l, const BAS_FCTS *thisptr)
{
  static REAL_B grd;
  grd[0] = 4.0*(8.0*l[0] - 1.0)*l[3]*(4.0*l[3] - 1.0);
  grd[3] = 4.0*l[0]*(4.0*l[0] - 1.0)*(8.0*l[3] - 1.0);
  return (const REAL *) grd;
}

static const REAL_B *D2_phi4e21_3d(const REAL_B l, const BAS_FCTS *thisptr)
{
  static REAL_BB D2;
  D2[0][0] = 32.0*l[3]*(4.0*l[3] - 1.0);
  D2[0][3] = D2[3][0] = 4.0*(8.0*l[0] - 1.0)*(8.0*l[3] - 1.0);
  D2[3][3] = 32.0*l[0]*(4.0*l[0] - 1.0);
  return (const REAL_B *) D2;
}

static const REAL_BB *D3_phi4e21_3d(const REAL_B l, const BAS_FCTS *thisptr)
{
  static REAL_BBB D3;

  D3[0][0][3] = D3[0][3][0] = D3[3][0][0] = 256.0*l[3] - 32.0;
  D3[0][3][3] = D3[3][0][3] = D3[3][3][0] = 256.0*l[0] - 32.0;

  return (const REAL_BB *)D3;
}

static const REAL_BBB *D4_phi4e21_3d(const REAL_B l, const BAS_FCTS *thisptr)
{
  static REAL_BBBB D4;

  D4[0][0][3][3] =
    D4[3][3][0][0] =
    D4[0][3][0][3] =
    D4[3][0][3][0] =
    D4[3][0][0][3] =
    D4[0][3][3][0] = 256.0;

  return (const REAL_BBB *)D4;
}

/*--------------------------------------------------------------------------*/
/*  basisfunction 2 at edge 2                                               */
/*--------------------------------------------------------------------------*/

static REAL phi4e22_3d(const REAL_B l, const BAS_FCTS *thisptr)
{
  return ((128.0*l[3] - 96.0)*l[3] + 16.0)*l[3]*l[0]/3.0;
}

static const REAL *grd_phi4e22_3d(const REAL_B l, const BAS_FCTS *thisptr)
{
  static REAL_B grd;
  grd[0] = ((128*l[3] - 96.0)*l[3] + 16.0)*l[3]/3.0;
  grd[3] = ((128*l[3] - 64.0)*l[3] + 16.0/3.0)*l[0];
  return (const REAL *) grd;
}

static const REAL_B *D2_phi4e22_3d(const REAL_B l, const BAS_FCTS *thisptr)
{
  static REAL_BB D2;
  D2[3][3] = (256.0*l[3] - 64.0)*l[0];
  D2[0][3] = D2[3][0] = (128.0*l[3] - 64.0)*l[3] + 16.0/3.0;
  return (const REAL_B *) D2;
}

static const REAL_BB *D3_phi4e22_3d(const REAL_B l, const BAS_FCTS *thisptr)
{
  static REAL_BBB D3;

  D3[3][3][3] = 256.0*l[0];
  D3[0][3][3] = D3[3][0][3] = D3[3][3][0] = 256.0*l[3] - 64.0;

  return (const REAL_BB *)D3;
}

static const REAL_BBB *D4_phi4e22_3d(const REAL_B l, const BAS_FCTS *thisptr)
{
  static REAL_BBBB D4;

  D4[0][3][3][3] =
    D4[3][0][3][3] =
    D4[3][3][0][3] =
    D4[3][3][3][0] = 256.0;

  return (const REAL_BBB *)D4;
}

/*--------------------------------------------------------------------------*/
/*  basisfunction 0 at edge 3                                               */
/*--------------------------------------------------------------------------*/

static REAL phi4e30_3d(const REAL_B l, const BAS_FCTS *thisptr)
{
  return ((128.0*l[1] - 96.0)*l[1] + 16.0)*l[1]*l[2]/3.0;
}

static const REAL *grd_phi4e30_3d(const REAL_B l, const BAS_FCTS *thisptr)
{
  static REAL_B grd;
  grd[1] = ((128*l[1] - 64.0)*l[1] + 16.0/3.0)*l[2];
  grd[2] = ((128*l[1] - 96.0)*l[1] + 16.0)*l[1]/3.0;
  return (const REAL *) grd;
}

static const REAL_B *D2_phi4e30_3d(const REAL_B l, const BAS_FCTS *thisptr)
{
  static REAL_BB D2;
  D2[1][1] = (256.0*l[1] - 64.0)*l[2];
  D2[1][2] = D2[2][1] = (128.0*l[1] - 64.0)*l[1] + 16.0/3.0;
  return (const REAL_B *) D2;
}

static const REAL_BB *D3_phi4e30_3d(const REAL_B l, const BAS_FCTS *thisptr)
{
  static REAL_BBB D3;

  D3[1][1][1] = 256.0*l[2];
  D3[1][1][2] = D3[1][2][1] = D3[2][1][1] = 256.0*l[1] - 64.0;

  return (const REAL_BB *)D3;
}

static const REAL_BBB *D4_phi4e30_3d(const REAL_B l, const BAS_FCTS *thisptr)
{
  static REAL_BBBB D4;

  D4[2][1][1][1] =
  D4[1][2][1][1] =
  D4[1][1][2][1] =
  D4[1][1][1][2] = 256.0;

  return (const REAL_BBB *)D4;
}

/*--------------------------------------------------------------------------*/
/*  basisfunction 1 at edge 3                                               */
/*--------------------------------------------------------------------------*/

static REAL phi4e31_3d(const REAL_B l, const BAS_FCTS *thisptr)
{
  return (4.0*l[1] - 1.0)*l[1]*(4.0*l[2] - 1.0)*l[2]*4.0;
}

static const REAL *grd_phi4e31_3d(const REAL_B l, const BAS_FCTS *thisptr)
{
  static REAL_B grd;
  grd[1] = 4.0*(8.0*l[1] - 1.0)*l[2]*(4.0*l[2] - 1.0);
  grd[2] = 4.0*l[1]*(4.0*l[1] - 1.0)*(8.0*l[2] - 1.0);
  return (const REAL *) grd;
}

static const REAL_B *D2_phi4e31_3d(const REAL_B l, const BAS_FCTS *thisptr)
{
  static REAL_BB D2;
  D2[1][1] = 32.0*l[2]*(4.0*l[2] - 1.0);
  D2[1][2] = D2[2][1] = 4.0*(8.0*l[1] - 1.0)*(8.0*l[2] - 1.0);
  D2[2][2] = 32.0*l[1]*(4.0*l[1] - 1.0);
  return (const REAL_B *) D2;
}

static const REAL_BB *D3_phi4e31_3d(const REAL_B l, const BAS_FCTS *thisptr)
{
  static REAL_BBB D3;

  D3[1][1][2] = D3[1][2][1] = D3[2][1][1] = 256.0*l[2] - 32.0;
  D3[1][2][2] = D3[2][1][2] = D3[2][2][1] = 256.0*l[1] - 32.0;

  return (const REAL_BB *)D3;
}

static const REAL_BBB *D4_phi4e31_3d(const REAL_B l, const BAS_FCTS *thisptr)
{
  static REAL_BBBB D4;

  D4[1][1][2][2] =
    D4[2][2][1][1] =
    D4[1][2][1][2] =
    D4[2][1][2][1] =
    D4[2][1][1][2] =
    D4[1][2][2][1] = 256.0;

  return (const REAL_BBB *)D4;
}

/*--------------------------------------------------------------------------*/
/*  basisfunction 2 at edge 3                                               */
/*--------------------------------------------------------------------------*/

static REAL phi4e32_3d(const REAL_B l, const BAS_FCTS *thisptr)
{
  return ((128.0*l[2] - 96.0)*l[2] + 16.0)*l[2]*l[1]/3.0;
}

static const REAL *grd_phi4e32_3d(const REAL_B l, const BAS_FCTS *thisptr)
{
  static REAL_B grd;
  grd[1] = ((128*l[2] - 96.0)*l[2] + 16.0)*l[2]/3.0;
  grd[2] = ((128*l[2] - 64.0)*l[2] + 16.0/3.0)*l[1];
  return (const REAL *) grd;
}

static const REAL_B *D2_phi4e32_3d(const REAL_B l, const BAS_FCTS *thisptr)
{
  static REAL_BB D2;
  D2[2][2] = (256.0*l[2] - 64.0)*l[1];
  D2[1][2] = D2[2][1] = (128.0*l[2] - 64.0)*l[2] + 16.0/3.0;
  return (const REAL_B *) D2;
}

static const REAL_BB *D3_phi4e32_3d(const REAL_B l, const BAS_FCTS *thisptr)
{
  static REAL_BBB D3;

  D3[2][2][2] = 256.0*l[1];
  D3[1][2][2] = D3[2][1][2] = D3[2][2][1] = 256.0*l[2] - 64.0;

  return (const REAL_BB *)D3;
}

static const REAL_BBB *D4_phi4e32_3d(const REAL_B l, const BAS_FCTS *thisptr)
{
  static REAL_BBBB D4;

  D4[1][2][2][2] =
    D4[2][1][2][2] =
    D4[2][2][1][2] =
    D4[2][2][2][1] = 256.0;

  return (const REAL_BBB *)D4;
}

/*--------------------------------------------------------------------------*/
/*  basisfunction 0 at edge 4                                               */
/*--------------------------------------------------------------------------*/

static REAL phi4e40_3d(const REAL_B l, const BAS_FCTS *thisptr)
{
  return ((128.0*l[1] - 96.0)*l[1] + 16.0)*l[1]*l[3]/3.0;
}

static const REAL *grd_phi4e40_3d(const REAL_B l, const BAS_FCTS *thisptr)
{
  static REAL_B grd;
  grd[1] = ((128*l[1] - 64.0)*l[1] + 16.0/3.0)*l[3];
  grd[3] = ((128*l[1] - 96.0)*l[1] + 16.0)*l[1]/3.0;
  return (const REAL *) grd;
}

static const REAL_B *D2_phi4e40_3d(const REAL_B l, const BAS_FCTS *thisptr)
{
  static REAL_BB D2;
  D2[1][1] = (256.0*l[1] - 64.0)*l[3];
  D2[1][3] = D2[3][1] = (128.0*l[1] - 64.0)*l[1] + 16.0/3.0;
  return (const REAL_B *) D2;
}

static const REAL_BB *D3_phi4e40_3d(const REAL_B l, const BAS_FCTS *thisptr)
{
  static REAL_BBB D3;

  D3[1][1][1] = 256.0*l[3];
  D3[1][1][3] = D3[1][3][1] = D3[3][1][1] = 256.0*l[1] - 64.0;

  return (const REAL_BB *)D3;
}

static const REAL_BBB *D4_phi4e40_3d(const REAL_B l, const BAS_FCTS *thisptr)
{
  static REAL_BBBB D4;

  D4[3][1][1][1] =
  D4[1][3][1][1] =
  D4[1][1][3][1] =
  D4[1][1][1][3] = 256.0;

  return (const REAL_BBB *)D4;
}

/*--------------------------------------------------------------------------*/
/*  basisfunction 1 at edge 4                                               */
/*--------------------------------------------------------------------------*/

static REAL phi4e41_3d(const REAL_B l, const BAS_FCTS *thisptr)
{
  return (4.0*l[1] - 1.0)*l[1]*(4.0*l[3] - 1.0)*l[3]*4.0;
}

static const REAL *grd_phi4e41_3d(const REAL_B l, const BAS_FCTS *thisptr)
{
  static REAL_B grd;
  grd[1] = 4.0*(8.0*l[1] - 1.0)*l[3]*(4.0*l[3] - 1.0);
  grd[3] = 4.0*l[1]*(4.0*l[1] - 1.0)*(8.0*l[3] - 1.0);
  return (const REAL *) grd;
}

static const REAL_B *D2_phi4e41_3d(const REAL_B l, const BAS_FCTS *thisptr)
{
  static REAL_BB D2;
  D2[1][1] = 32.0*l[3]*(4.0*l[3] - 1.0);
  D2[1][3] = D2[3][1] = 4.0*(8.0*l[1] - 1.0)*(8.0*l[3] - 1.0);
  D2[3][3] = 32.0*l[1]*(4.0*l[1] - 1.0);
  return (const REAL_B *) D2;
}

static const REAL_BB *D3_phi4e41_3d(const REAL_B l, const BAS_FCTS *thisptr)
{
  static REAL_BBB D3;

  D3[1][1][3] = D3[1][3][1] = D3[3][1][1] = 256.0*l[3] - 32.0;
  D3[1][3][3] = D3[3][1][3] = D3[3][3][1] = 256.0*l[1] - 32.0;

  return (const REAL_BB *)D3;
}

static const REAL_BBB *D4_phi4e41_3d(const REAL_B l, const BAS_FCTS *thisptr)
{
  static REAL_BBBB D4;

  D4[1][1][3][3] =
    D4[3][3][1][1] =
    D4[1][3][1][3] =
    D4[3][1][3][1] =
    D4[3][1][1][3] =
    D4[1][3][3][1] = 256.0;

  return (const REAL_BBB *)D4;
}

/*--------------------------------------------------------------------------*/
/*  basisfunction 2 at edge 4                                               */
/*--------------------------------------------------------------------------*/

static REAL phi4e42_3d(const REAL_B l, const BAS_FCTS *thisptr)
{
  return ((128.0*l[3] - 96.0)*l[3] + 16.0)*l[3]*l[1]/3.0;
}

static const REAL *grd_phi4e42_3d(const REAL_B l, const BAS_FCTS *thisptr)
{
  static REAL_B grd;
  grd[1] = ((128*l[3] - 96.0)*l[3] + 16.0)*l[3]/3.0;
  grd[3] = ((128*l[3] - 64.0)*l[3] + 16.0/3.0)*l[1];
  return (const REAL *) grd;
}

static const REAL_B *D2_phi4e42_3d(const REAL_B l, const BAS_FCTS *thisptr)
{
  static REAL_BB D2;
  D2[3][3] = (256.0*l[3] - 64.0)*l[1];
  D2[1][3] = D2[3][1] = (128.0*l[3] - 64.0)*l[3] + 16.0/3.0;
  return (const REAL_B *) D2;
}

static const REAL_BB *D3_phi4e42_3d(const REAL_B l, const BAS_FCTS *thisptr)
{
  static REAL_BBB D3;

  D3[3][3][3] = 256.0*l[1];
  D3[1][3][3] = D3[3][1][3] = D3[3][3][1] = 256.0*l[3] - 64.0;

  return (const REAL_BB *)D3;
}

static const REAL_BBB *D4_phi4e42_3d(const REAL_B l, const BAS_FCTS *thisptr)
{
  static REAL_BBBB D4;

  D4[1][3][3][3] =
    D4[3][1][3][3] =
    D4[3][3][1][3] =
    D4[3][3][3][1] = 256.0;

  return (const REAL_BBB *)D4;
}

/*--------------------------------------------------------------------------*/
/*  basisfunction 0 at edge 5                                               */
/*--------------------------------------------------------------------------*/

static REAL phi4e50_3d(const REAL_B l, const BAS_FCTS *thisptr)
{
  return ((128.0*l[2] - 96.0)*l[2] + 16.0)*l[2]*l[3]/3.0;
}

static const REAL *grd_phi4e50_3d(const REAL_B l, const BAS_FCTS *thisptr)
{
  static REAL_B grd;
  grd[2] = ((128*l[2] - 64.0)*l[2] + 16.0/3.0)*l[3];
  grd[3] = ((128*l[2] - 96.0)*l[2] + 16.0)*l[2]/3.0;
  return (const REAL *) grd;
}

static const REAL_B *D2_phi4e50_3d(const REAL_B l, const BAS_FCTS *thisptr)
{
  static REAL_BB D2;
  D2[2][2] = (256.0*l[2] - 64.0)*l[3];
  D2[2][3] = D2[3][2] = (128.0*l[2] - 64.0)*l[2] + 16.0/3.0;
  return (const REAL_B *) D2;
}

static const REAL_BB *D3_phi4e50_3d(const REAL_B l, const BAS_FCTS *thisptr)
{
  static REAL_BBB D3;

  D3[2][2][2] = 256.0*l[3];
  D3[2][2][3] = D3[2][3][2] = D3[3][2][2] = 256.0*l[2] - 64.0;

  return (const REAL_BB *)D3;
}

static const REAL_BBB *D4_phi4e50_3d(const REAL_B l, const BAS_FCTS *thisptr)
{
  static REAL_BBBB D4;

  D4[3][2][2][2] =
  D4[2][3][2][2] =
  D4[2][2][3][2] =
  D4[2][2][2][3] = 256.0;

  return (const REAL_BBB *)D4;
}

/*--------------------------------------------------------------------------*/
/*  basisfunction 1 at edge 5                                               */
/*--------------------------------------------------------------------------*/

static REAL phi4e51_3d(const REAL_B l, const BAS_FCTS *thisptr)
{
  return (4.0*l[2] - 1.0)*l[2]*(4.0*l[3] - 1.0)*l[3]*4.0;
}

static const REAL *grd_phi4e51_3d(const REAL_B l, const BAS_FCTS *thisptr)
{
  static REAL_B grd;
  grd[2] = 4.0*(8.0*l[2] - 1.0)*l[3]*(4.0*l[3] - 1.0);
  grd[3] = 4.0*l[2]*(4.0*l[2] - 1.0)*(8.0*l[3] - 1.0);
  return (const REAL *) grd;
}

static const REAL_B *D2_phi4e51_3d(const REAL_B l, const BAS_FCTS *thisptr)
{
  static REAL_BB D2;
  D2[2][2] = 32.0*l[3]*(4.0*l[3] - 1.0);
  D2[2][3] = D2[3][2] = 4.0*(8.0*l[2] - 1.0)*(8.0*l[3] - 1.0);
  D2[3][3] = 32.0*l[2]*(4.0*l[2] - 1.0);
  return (const REAL_B *) D2;
}

static const REAL_BB *D3_phi4e51_3d(const REAL_B l, const BAS_FCTS *thisptr)
{
  static REAL_BBB D3;

  D3[2][2][3] = D3[2][3][2] = D3[3][2][2] = 256.0*l[3] - 32.0;
  D3[3][3][2] = D3[3][2][3] = D3[2][3][3] = 256.0*l[2] - 32.0;

  return (const REAL_BB *)D3;
}

static const REAL_BBB *D4_phi4e51_3d(const REAL_B l, const BAS_FCTS *thisptr)
{
  static REAL_BBBB D4;

  D4[2][2][3][3] =
    D4[3][3][2][2] =
    D4[2][3][2][3] =
    D4[3][2][3][2] =
    D4[3][2][2][3] =
    D4[2][3][3][2] = 256.0;

  return (const REAL_BBB *)D4;
}

/*--------------------------------------------------------------------------*/
/*  basisfunction 2 at edge 5                                               */
/*--------------------------------------------------------------------------*/

static REAL phi4e52_3d(const REAL_B l, const BAS_FCTS *thisptr)
{
  return ((128.0*l[3] - 96.0)*l[3] + 16.0)*l[3]*l[2]/3.0;
}

static const REAL *grd_phi4e52_3d(const REAL_B l, const BAS_FCTS *thisptr)
{
  static REAL_B grd;
  grd[2] = ((128*l[3] - 96.0)*l[3] + 16.0)*l[3]/3.0;
  grd[3] = ((128*l[3] - 64.0)*l[3] + 16.0/3.0)*l[2];
  return (const REAL *) grd;
}

static const REAL_B *D2_phi4e52_3d(const REAL_B l, const BAS_FCTS *thisptr)
{
  static REAL_BB D2;
  D2[3][3] = (256.0*l[3] - 64.0)*l[2];
  D2[2][3] = D2[3][2] = (128.0*l[3] - 64.0)*l[3] + 16.0/3.0;
  return (const REAL_B *) D2;
}

static const REAL_BB *D3_phi4e52_3d(const REAL_B l, const BAS_FCTS *thisptr)
{
  static REAL_BBB D3;

  D3[3][3][3] = 256.0*l[2];
  D3[2][3][3] = D3[3][2][3] = D3[3][3][2] = 256.0*l[3] - 64.0;

  return (const REAL_BB *)D3;
}

static const REAL_BBB *D4_phi4e52_3d(const REAL_B l, const BAS_FCTS *thisptr)
{
  static REAL_BBBB D4;

  D4[2][3][3][3] =
    D4[3][2][3][3] =
    D4[3][3][2][3] =
    D4[3][3][3][2] = 256.0;

  return (const REAL_BBB *)D4;
}

/*--------------------------------------------------------------------------*/
/*  basisfunction 0 at face 3                                               */
/*--------------------------------------------------------------------------*/

static REAL phi4f30_3d(const REAL_B l, const BAS_FCTS *thisptr)
{
  return (4.0*l[0] - 1.0)*l[0]*l[1]*l[2]*32.0;
}

static const REAL *grd_phi4f30_3d(const REAL_B l, const BAS_FCTS *thisptr)
{
  static REAL_B grd;
  grd[0] = 32.0*(8.0*l[0] - 1.0)*l[1]*l[2];
  grd[1] = 32.0*(4.0*l[0] - 1.0)*l[0]*l[2];
  grd[2] = 32.0*(4.0*l[0] - 1.0)*l[0]*l[1];
  return (const REAL *) grd;
}

static const REAL_B *D2_phi4f30_3d(const REAL_B l, const BAS_FCTS *thisptr)
{
  static REAL_BB D2;
  D2[0][0] = 256.0*l[1]*l[2];
  D2[0][1] = D2[1][0] = 32.0*(8.0*l[0] - 1.0)*l[2];
  D2[0][2] = D2[2][0] = 32.0*(8.0*l[0] - 1.0)*l[1];
  D2[1][2] = D2[2][1] = 32.0*(4.0*l[0] - 1.0)*l[0];
  return (const REAL_B *) D2;
}

static const REAL_BB *D3_phi4f30_3d(const REAL_B l, const BAS_FCTS *thisptr)
{
  static REAL_BBB D3;

  D3[0][0][1] = D3[0][1][0] = D3[1][0][0] = 256.0*l[2];
  D3[0][0][2] = D3[0][2][0] = D3[2][0][0] = 256.0*l[1];

  D3[0][1][2] = D3[0][2][1] = D3[1][0][2] = 
    D3[2][1][0] = D3[2][0][1] = D3[1][2][0] = 256.0*l[0] - 32.0;

  return (const REAL_BB *)D3;
}

#define D4_IDX_CYCLE_3(a, b, c)			\
  D4[a][a][b][c] =				\
    D4[a][b][a][c] =				\
    D4[a][c][b][a] =				\
    D4[a][a][c][b] =				\
    D4[a][c][a][b] =				\
    D4[a][b][c][a] =				\
    D4[c][a][b][a] =				\
    D4[b][a][a][c] =				\
    D4[c][a][a][b] =				\
    D4[b][a][c][a] =				\
    D4[c][b][a][a] =				\
    D4[b][c][a][a]

static const REAL_BBB *D4_phi4f30_3d(const REAL_B l, const BAS_FCTS *thisptr)
{
  static REAL_BBBB D4;

  D4_IDX_CYCLE_3(0, 1, 2) = 256.0;
  
  return (const REAL_BBB *)D4;
}

/*--------------------------------------------------------------------------*/
/*  basisfunction 1 at face 3                                               */
/*--------------------------------------------------------------------------*/

static REAL phi4f31_3d(const REAL_B l, const BAS_FCTS *thisptr)
{
  return l[0]*(4.0*l[1] - 1.0)*l[1]*l[2]*32.0;
}

static const REAL *grd_phi4f31_3d(const REAL_B l, const BAS_FCTS *thisptr)
{
  static REAL_B grd;
  grd[0] = 32.0*(4.0*l[1] - 1.0)*l[1]*l[2];
  grd[1] = 32.0*(8.0*l[1] - 1.0)*l[0]*l[2];
  grd[2] = 32.0*(4.0*l[1] - 1.0)*l[0]*l[1];
  return (const REAL *) grd;
}

static const REAL_B *D2_phi4f31_3d(const REAL_B l, const BAS_FCTS *thisptr)
{
  static REAL_BB D2;
  D2[0][1] = D2[1][0] = 32.0*(8.0*l[1] - 1.0)*l[2];
  D2[0][2] = D2[2][0] = 32.0*(4.0*l[1] - 1.0)*l[1];
  D2[1][1] = 256.0*l[0]*l[2];
  D2[1][2] = D2[2][1] = 32.0*(8.0*l[1] - 1.0)*l[0];
  return (const REAL_B *) D2;
}

static const REAL_BB *D3_phi4f31_3d(const REAL_B l, const BAS_FCTS *thisptr)
{
  static REAL_BBB D3;

  D3[1][1][0] = D3[1][0][1] = D3[0][1][1] = 256.0*l[2];
  D3[1][1][2] = D3[1][2][1] = D3[2][1][1] = 256.0*l[0];

  D3[0][1][2] = D3[0][2][1] = D3[1][0][2] = 
    D3[2][1][0] = D3[2][0][1] = D3[1][2][0] = 256.0*l[1] - 32.0;

  return (const REAL_BB *)D3;
}

static const REAL_BBB *D4_phi4f31_3d(const REAL_B l, const BAS_FCTS *thisptr)
{
  static REAL_BBBB D4;

  D4_IDX_CYCLE_3(1, 0, 2) = 256.0;
  
  return (const REAL_BBB *)D4;
}

/*--------------------------------------------------------------------------*/
/*  basisfunction 2 at face 3                                               */
/*--------------------------------------------------------------------------*/

static REAL phi4f32_3d(const REAL_B l, const BAS_FCTS *thisptr)
{
  return l[0]*l[1]*(4.0*l[2] - 1.0)*l[2]*32.0;
}

static const REAL *grd_phi4f32_3d(const REAL_B l, const BAS_FCTS *thisptr)
{
  static REAL_B grd;
  grd[0] = 32.0*(4.0*l[2] - 1.0)*l[1]*l[2];
  grd[1] = 32.0*(4.0*l[2] - 1.0)*l[0]*l[2];
  grd[2] = 32.0*(8.0*l[2] - 1.0)*l[0]*l[1];
  return (const REAL *) grd;
}

static const REAL_B *D2_phi4f32_3d(const REAL_B l, const BAS_FCTS *thisptr)
{
  static REAL_BB D2;
  D2[0][1] = D2[1][0] = 32.0*(4.0*l[2] - 1.0)*l[2];
  D2[0][2] = D2[2][0] = 32.0*(8.0*l[2] - 1.0)*l[1];
  D2[1][2] = D2[2][1] = 32.0*(8.0*l[2] - 1.0)*l[0];
  D2[2][2] = 256.0*l[0]*l[1];
  return (const REAL_B *) D2;
}

static const REAL_BB *D3_phi4f32_3d(const REAL_B l, const BAS_FCTS *thisptr)
{
  static REAL_BBB D3;

  D3[2][2][0] = D3[2][0][2] = D3[0][2][2] = 256.0*l[1];
  D3[2][2][1] = D3[2][1][2] = D3[1][2][2] = 256.0*l[0];

  D3[0][1][2] = D3[0][2][1] = D3[1][0][2] = 
    D3[2][1][0] = D3[2][0][1] = D3[1][2][0] = 256.0*l[2] - 32.0;

  return (const REAL_BB *)D3;
}

static const REAL_BBB *D4_phi4f32_3d(const REAL_B l, const BAS_FCTS *thisptr)
{
  static REAL_BBBB D4;

  D4_IDX_CYCLE_3(2, 0, 1) = 256.0;
  
  return (const REAL_BBB *)D4;
}

/*--------------------------------------------------------------------------*/
/*  basisfunction 0 at face 2                                               */
/*--------------------------------------------------------------------------*/

static REAL phi4f20_3d(const REAL_B l, const BAS_FCTS *thisptr)
{
  return (4.0*l[0] - 1.0)*l[0]*l[1]*l[3]*32.0;
}

static const REAL *grd_phi4f20_3d(const REAL_B l, const BAS_FCTS *thisptr)
{
  static REAL_B grd;
  grd[0] = 32.0*(8.0*l[0] - 1.0)*l[1]*l[3];
  grd[1] = 32.0*(4.0*l[0] - 1.0)*l[0]*l[3];
  grd[3] = 32.0*(4.0*l[0] - 1.0)*l[0]*l[1];
  return (const REAL *) grd;
}

static const REAL_B *D2_phi4f20_3d(const REAL_B l, const BAS_FCTS *thisptr)
{
  static REAL_BB D2;
  D2[0][0] = 256.0*l[1]*l[3];
  D2[0][1] = D2[1][0] = 32.0*(8.0*l[0] - 1.0)*l[3];
  D2[0][3] = D2[3][0] = 32.0*(8.0*l[0] - 1.0)*l[1];
  D2[1][3] = D2[3][1] = 32.0*(4.0*l[0] - 1.0)*l[0];
  return (const REAL_B *) D2;
}

static const REAL_BB *D3_phi4f20_3d(const REAL_B l, const BAS_FCTS *thisptr)
{
  static REAL_BBB D3;

  D3[0][0][1] = D3[0][1][0] = D3[1][0][0] = 256.0*l[3];
  D3[0][0][3] = D3[0][3][0] = D3[3][0][0] = 256.0*l[1];

  D3[0][1][3] = D3[0][3][1] = D3[1][0][3] = 
    D3[3][1][0] = D3[3][0][1] = D3[1][3][0] = 256.0*l[0] - 32.0;

  return (const REAL_BB *)D3;
}

static const REAL_BBB *D4_phi4f20_3d(const REAL_B l, const BAS_FCTS *thisptr)
{
  static REAL_BBBB D4;

  D4_IDX_CYCLE_3(0, 1, 3) = 256.0;
  
  return (const REAL_BBB *)D4;
}

/*--------------------------------------------------------------------------*/
/*  basisfunction 1 at face 2                                               */
/*--------------------------------------------------------------------------*/

static REAL phi4f21_3d(const REAL_B l, const BAS_FCTS *thisptr)
{
  return l[0]*(4.0*l[1] - 1.0)*l[1]*l[3]*32.0;
}

static const REAL *grd_phi4f21_3d(const REAL_B l, const BAS_FCTS *thisptr)
{
  static REAL_B grd;
  grd[0] = 32.0*(4.0*l[1] - 1.0)*l[1]*l[3];
  grd[1] = 32.0*(8.0*l[1] - 1.0)*l[0]*l[3];
  grd[3] = 32.0*(4.0*l[1] - 1.0)*l[0]*l[1];
  return (const REAL *) grd;
}

static const REAL_B *D2_phi4f21_3d(const REAL_B l, const BAS_FCTS *thisptr)
{
  static REAL_BB D2;
  D2[0][1] = D2[1][0] = 32.0*(8.0*l[1] - 1.0)*l[3];
  D2[0][3] = D2[3][0] = 32.0*(4.0*l[1] - 1.0)*l[1];
  D2[1][1] = 256.0*l[0]*l[3];
  D2[1][3] = D2[3][1] = 32.0*(8.0*l[1] - 1.0)*l[0];
  return (const REAL_B *) D2;
}

static const REAL_BB *D3_phi4f21_3d(const REAL_B l, const BAS_FCTS *thisptr)
{
  static REAL_BBB D3;

  D3[1][1][0] = D3[1][0][1] = D3[0][1][1] = 256.0*l[3];
  D3[1][1][3] = D3[1][3][1] = D3[3][1][1] = 256.0*l[0];

  D3[0][1][3] = D3[0][3][1] = D3[1][0][3] = 
    D3[3][1][0] = D3[3][0][1] = D3[1][3][0] = 256.0*l[1] - 32.0;

  return (const REAL_BB *)D3;
}

static const REAL_BBB *D4_phi4f21_3d(const REAL_B l, const BAS_FCTS *thisptr)
{
  static REAL_BBBB D4;

  D4_IDX_CYCLE_3(1, 0, 3) = 256.0;
  
  return (const REAL_BBB *)D4;
}

/*--------------------------------------------------------------------------*/
/*  basisfunction 2 at face 2                                               */
/*--------------------------------------------------------------------------*/

static REAL phi4f22_3d(const REAL_B l, const BAS_FCTS *thisptr)
{
  return l[0]*l[1]*(4.0*l[3] - 1.0)*l[3]*32.0;
}

static const REAL *grd_phi4f22_3d(const REAL_B l, const BAS_FCTS *thisptr)
{
  static REAL_B grd;
  grd[0] = 32.0*(4.0*l[3] - 1.0)*l[1]*l[3];
  grd[1] = 32.0*(4.0*l[3] - 1.0)*l[0]*l[3];
  grd[3] = 32.0*(8.0*l[3] - 1.0)*l[0]*l[1];
  return (const REAL *) grd;
}

static const REAL_B *D2_phi4f22_3d(const REAL_B l, const BAS_FCTS *thisptr)
{
  static REAL_BB D2;
  D2[0][1] = D2[1][0] = 32.0*(4.0*l[3] - 1.0)*l[3];
  D2[0][3] = D2[3][0] = 32.0*(8.0*l[3] - 1.0)*l[1];
  D2[1][3] = D2[3][1] = 32.0*(8.0*l[3] - 1.0)*l[0];
  D2[3][3] = 256.0*l[0]*l[1];
  return (const REAL_B *) D2;
}

static const REAL_BB *D3_phi4f22_3d(const REAL_B l, const BAS_FCTS *thisptr)
{
  static REAL_BBB D3;

  D3[3][3][0] = D3[3][0][3] = D3[0][3][3] = 256.0*l[1];
  D3[3][3][1] = D3[3][1][3] = D3[1][3][3] = 256.0*l[0];

  D3[0][1][3] = D3[0][3][1] = D3[1][0][3] = 
    D3[3][1][0] = D3[3][0][1] = D3[1][3][0] = 256.0*l[3] - 32.0;

  return (const REAL_BB *)D3;
}

static const REAL_BBB *D4_phi4f22_3d(const REAL_B l, const BAS_FCTS *thisptr)
{
  static REAL_BBBB D4;

  D4_IDX_CYCLE_3(3, 0, 1) = 256.0;
  
  return (const REAL_BBB *)D4;
}

/*--------------------------------------------------------------------------*/
/*  basisfunction 0 at face 1                                               */
/*--------------------------------------------------------------------------*/

static REAL phi4f10_3d(const REAL_B l, const BAS_FCTS *thisptr)
{
  return (4.0*l[0] - 1.0)*l[0]*l[2]*l[3]*32.0;
}

static const REAL *grd_phi4f10_3d(const REAL_B l, const BAS_FCTS *thisptr)
{
  static REAL_B grd;
  grd[0] = 32.0*(8.0*l[0] - 1.0)*l[2]*l[3];
  grd[2] = 32.0*(4.0*l[0] - 1.0)*l[0]*l[3];
  grd[3] = 32.0*(4.0*l[0] - 1.0)*l[0]*l[2];
  return (const REAL *) grd;
}

static const REAL_B *D2_phi4f10_3d(const REAL_B l, const BAS_FCTS *thisptr)
{
  static REAL_BB D2;
  D2[0][0] = 256.0*l[2]*l[3];
  D2[0][2] = D2[2][0] = 32.0*(8.0*l[0] - 1.0)*l[3];
  D2[0][3] = D2[3][0] = 32.0*(8.0*l[0] - 1.0)*l[2];
  D2[2][3] = D2[3][2] = 32.0*(4.0*l[0] - 1.0)*l[0];
  return (const REAL_B *) D2;
}

static const REAL_BB *D3_phi4f10_3d(const REAL_B l, const BAS_FCTS *thisptr)
{
  static REAL_BBB D3;

  D3[0][0][2] = D3[0][2][0] = D3[2][0][0] = 256.0*l[3];
  D3[0][0][3] = D3[0][3][0] = D3[3][0][0] = 256.0*l[2];

  D3[0][2][3] = D3[0][3][2] = D3[2][0][3] = 
    D3[3][2][0] = D3[3][0][2] = D3[2][3][0] = 256.0*l[0] - 32.0;

  return (const REAL_BB *)D3;
}

static const REAL_BBB *D4_phi4f10_3d(const REAL_B l, const BAS_FCTS *thisptr)
{
  static REAL_BBBB D4;

  D4_IDX_CYCLE_3(0, 2, 3) = 256.0;
  
  return (const REAL_BBB *)D4;
}

/*--------------------------------------------------------------------------*/
/*  basisfunction 1 at face 1                                               */
/*--------------------------------------------------------------------------*/

static REAL phi4f11_3d(const REAL_B l, const BAS_FCTS *thisptr)
{
  return l[0]*(4.0*l[2] - 1.0)*l[2]*l[3]*32.0;
}

static const REAL *grd_phi4f11_3d(const REAL_B l, const BAS_FCTS *thisptr)
{
  static REAL_B grd;
  grd[0] = 32.0*(4.0*l[2] - 1.0)*l[2]*l[3];
  grd[2] = 32.0*(8.0*l[2] - 1.0)*l[0]*l[3];
  grd[3] = 32.0*(4.0*l[2] - 1.0)*l[0]*l[2];
  return (const REAL *) grd;
}

static const REAL_B *D2_phi4f11_3d(const REAL_B l, const BAS_FCTS *thisptr)
{
  static REAL_BB D2;
  D2[0][2] = D2[2][0] = 32.0*(8.0*l[2] - 1.0)*l[3];
  D2[0][3] = D2[3][0] = 32.0*(4.0*l[2] - 1.0)*l[2];
  D2[2][2] = 256.0*l[0]*l[3];
  D2[2][3] = D2[3][2] = 32.0*(8.0*l[2] - 1.0)*l[0];
  return (const REAL_B *) D2;
}

static const REAL_BB *D3_phi4f11_3d(const REAL_B l, const BAS_FCTS *thisptr)
{
  static REAL_BBB D3;

  D3[2][2][0] = D3[2][0][2] = D3[0][2][2] = 256.0*l[3];
  D3[2][2][3] = D3[2][3][2] = D3[3][2][2] = 256.0*l[0];

  D3[0][2][3] = D3[0][3][2] = D3[2][0][3] = 
    D3[3][2][0] = D3[3][0][2] = D3[2][3][0] = 256.0*l[2] - 32.0;

  return (const REAL_BB *)D3;
}

static const REAL_BBB *D4_phi4f11_3d(const REAL_B l, const BAS_FCTS *thisptr)
{
  static REAL_BBBB D4;

  D4_IDX_CYCLE_3(2, 0, 3) = 256.0;
  
  return (const REAL_BBB *)D4;
}

/*--------------------------------------------------------------------------*/
/*  basisfunction 2 at face 1                                               */
/*--------------------------------------------------------------------------*/

static REAL phi4f12_3d(const REAL_B l, const BAS_FCTS *thisptr)
{
  return l[0]*l[2]*(4.0*l[3] - 1.0)*l[3]*32.0;
}

static const REAL *grd_phi4f12_3d(const REAL_B l, const BAS_FCTS *thisptr)
{
  static REAL_B grd;
  grd[0] = 32.0*(4.0*l[3] - 1.0)*l[2]*l[3];
  grd[2] = 32.0*(4.0*l[3] - 1.0)*l[0]*l[3];
  grd[3] = 32.0*(8.0*l[3] - 1.0)*l[0]*l[2];
  return (const REAL *) grd;
}

static const REAL_B *D2_phi4f12_3d(const REAL_B l, const BAS_FCTS *thisptr)
{
  static REAL_BB D2;
  D2[0][2] = D2[2][0] = 32.0*(4.0*l[3] - 1.0)*l[3];
  D2[0][3] = D2[3][0] = 32.0*(8.0*l[3] - 1.0)*l[2];
  D2[2][3] = D2[3][2] = 32.0*(8.0*l[3] - 1.0)*l[0];
  D2[3][3] = 256.0*l[0]*l[2];
  return (const REAL_B *) D2;
}

static const REAL_BB *D3_phi4f12_3d(const REAL_B l, const BAS_FCTS *thisptr)
{
  static REAL_BBB D3;

  D3[3][3][0] = D3[3][0][3] = D3[0][3][3] = 256.0*l[2];
  D3[3][3][2] = D3[3][2][3] = D3[2][3][3] = 256.0*l[0];

  D3[0][2][3] = D3[0][3][2] = D3[2][0][3] = 
    D3[3][2][0] = D3[3][0][2] = D3[2][3][0] = 256.0*l[3] - 32.0;

  return (const REAL_BB *)D3;
}

static const REAL_BBB *D4_phi4f12_3d(const REAL_B l, const BAS_FCTS *thisptr)
{
  static REAL_BBBB D4;

  D4_IDX_CYCLE_3(3, 0, 2) = 256.0;
  
  return (const REAL_BBB *)D4;
}

/*--------------------------------------------------------------------------*/
/*  basisfunction 0 at face 0                                               */
/*--------------------------------------------------------------------------*/

static REAL phi4f00_3d(const REAL_B l, const BAS_FCTS *thisptr)
{
  return (4.0*l[1] - 1.0)*l[1]*l[2]*l[3]*32.0;
}

static const REAL *grd_phi4f00_3d(const REAL_B l, const BAS_FCTS *thisptr)
{
  static REAL_B grd;
  grd[1] = 32.0*(8.0*l[1] - 1.0)*l[2]*l[3];
  grd[2] = 32.0*(4.0*l[1] - 1.0)*l[1]*l[3];
  grd[3] = 32.0*(4.0*l[1] - 1.0)*l[1]*l[2];
  return (const REAL *) grd;
}

static const REAL_B *D2_phi4f00_3d(const REAL_B l, const BAS_FCTS *thisptr)
{
  static REAL_BB D2;
  D2[1][1] = 256.0*l[2]*l[3];
  D2[1][2] = D2[2][1] = 32.0*(8.0*l[1] - 1.0)*l[3];
  D2[1][3] = D2[3][1] = 32.0*(8.0*l[1] - 1.0)*l[2];
  D2[2][3] = D2[3][2] = 32.0*(4.0*l[1] - 1.0)*l[1];
  return (const REAL_B *) D2;
}

static const REAL_BB *D3_phi4f00_3d(const REAL_B l, const BAS_FCTS *thisptr)
{
  static REAL_BBB D3;

  D3[1][1][2] = D3[1][2][1] = D3[2][1][1] = 256.0*l[3];
  D3[1][1][3] = D3[1][3][1] = D3[3][1][1] = 256.0*l[2];

  D3[1][2][3] = D3[1][3][2] = D3[2][1][3] = 
    D3[3][2][1] = D3[3][1][2] = D3[2][3][1] = 256.0*l[1] - 32.0;

  return (const REAL_BB *)D3;
}

static const REAL_BBB *D4_phi4f00_3d(const REAL_B l, const BAS_FCTS *thisptr)
{
  static REAL_BBBB D4;

  D4_IDX_CYCLE_3(1, 2, 3) = 256.0;
  
  return (const REAL_BBB *)D4;
}

/*--------------------------------------------------------------------------*/
/*  basisfunction 1 at face 0                                               */
/*--------------------------------------------------------------------------*/

static REAL phi4f01_3d(const REAL_B l, const BAS_FCTS *thisptr)
{
  return l[1]*(4.0*l[2] - 1.0)*l[2]*l[3]*32.0;
}

static const REAL *grd_phi4f01_3d(const REAL_B l, const BAS_FCTS *thisptr)
{
  static REAL_B grd;
  grd[1] = 32.0*(4.0*l[2] - 1.0)*l[2]*l[3];
  grd[2] = 32.0*(8.0*l[2] - 1.0)*l[1]*l[3];
  grd[3] = 32.0*(4.0*l[2] - 1.0)*l[1]*l[2];
  return (const REAL *) grd;
}

static const REAL_B *D2_phi4f01_3d(const REAL_B l, const BAS_FCTS *thisptr)
{
  static REAL_BB D2;
  D2[1][2] = D2[2][1] = 32.0*(8.0*l[2] - 1.0)*l[3];
  D2[1][3] = D2[3][1] = 32.0*(4.0*l[2] - 1.0)*l[2];
  D2[2][2] = 256.0*l[1]*l[3];
  D2[2][3] = D2[3][2] = 32.0*(8.0*l[2] - 1.0)*l[1];
  return (const REAL_B *) D2;
}

static const REAL_BB *D3_phi4f01_3d(const REAL_B l, const BAS_FCTS *thisptr)
{
  static REAL_BBB D3;

  D3[2][2][1] = D3[2][1][2] = D3[1][2][2] = 256.0*l[3];
  D3[2][2][3] = D3[2][3][2] = D3[3][2][2] = 256.0*l[1];

  D3[1][2][3] = D3[1][3][2] = D3[2][1][3] = 
    D3[3][2][1] = D3[3][1][2] = D3[2][3][1] = 256.0*l[2] - 32.0;

  return (const REAL_BB *)D3;
}

static const REAL_BBB *D4_phi4f01_3d(const REAL_B l, const BAS_FCTS *thisptr)
{
  static REAL_BBBB D4;

  D4_IDX_CYCLE_3(2, 1, 3) = 256.0;
  
  return (const REAL_BBB *)D4;
}

/*--------------------------------------------------------------------------*/
/*  basisfunction 2 at face 0                                               */
/*--------------------------------------------------------------------------*/

static REAL phi4f02_3d(const REAL_B l, const BAS_FCTS *thisptr)
{
  return l[1]*l[2]*(4.0*l[3] - 1.0)*l[3]*32.0;
}

static const REAL *grd_phi4f02_3d(const REAL_B l, const BAS_FCTS *thisptr)
{
  static REAL_B grd;
  grd[1] = 32.0*(4.0*l[3] - 1.0)*l[2]*l[3];
  grd[2] = 32.0*(4.0*l[3] - 1.0)*l[1]*l[3];
  grd[3] = 32.0*(8.0*l[3] - 1.0)*l[1]*l[2];
  return (const REAL *) grd;
}

static const REAL_B *D2_phi4f02_3d(const REAL_B l, const BAS_FCTS *thisptr)
{
  static REAL_BB D2;
  D2[1][2] = D2[2][1] = 32.0*(4.0*l[3] - 1.0)*l[3];
  D2[1][3] = D2[3][1] = 32.0*(8.0*l[3] - 1.0)*l[2];
  D2[2][3] = D2[3][2] = 32.0*(8.0*l[3] - 1.0)*l[1];
  D2[3][3] = 256.0*l[1]*l[2];
  return (const REAL_B *) D2;
}

static const REAL_BB *D3_phi4f02_3d(const REAL_B l, const BAS_FCTS *thisptr)
{
  static REAL_BBB D3;

  D3[3][3][1] = D3[3][1][3] = D3[1][3][3] = 256.0*l[2];
  D3[3][3][2] = D3[3][2][3] = D3[2][3][3] = 256.0*l[1];

  D3[1][2][3] = D3[1][3][2] = D3[2][1][3] = 
    D3[3][2][1] = D3[3][1][2] = D3[2][3][1] = 256.0*l[3] - 32.0;

  return (const REAL_BB *)D3;
}

static const REAL_BBB *D4_phi4f02_3d(const REAL_B l, const BAS_FCTS *thisptr)
{
  static REAL_BBBB D4;

  D4_IDX_CYCLE_3(3, 1, 2) = 256.0;
  
  return (const REAL_BBB *)D4;
}

/*--------------------------------------------------------------------------*/
/*  basisfunction at center                                                 */
/*--------------------------------------------------------------------------*/

static REAL phi4c_3d(const REAL_B l, const BAS_FCTS *thisptr)
{
  return 256.0*l[0]*l[1]*l[2]*l[3];
}

static const REAL *grd_phi4c_3d(const REAL_B l, const BAS_FCTS *thisptr)
{
  static REAL_B grd;
  grd[0] = 256.0*l[1]*l[2]*l[3];
  grd[1] = 256.0*l[0]*l[2]*l[3];
  grd[2] = 256.0*l[0]*l[1]*l[3];
  grd[3] = 256.0*l[0]*l[1]*l[2];
  return (const REAL *) grd;
}

static const REAL_B *D2_phi4c_3d(const REAL_B l, const BAS_FCTS *thisptr)
{
  static REAL_BB D2;
  D2[0][1] = D2[1][0] = 256.0*l[2]*l[3];
  D2[0][2] = D2[2][0] = 256.0*l[1]*l[3];
  D2[0][3] = D2[3][0] = 256.0*l[1]*l[2];
  D2[1][2] = D2[2][1] = 256.0*l[0]*l[3];
  D2[1][3] = D2[3][1] = 256.0*l[0]*l[2];
  D2[2][3] = D2[3][2] = 256.0*l[0]*l[1];
  return (const REAL_B *) D2;
}

static const REAL_BB *D3_phi4c_3d(const REAL_B l, const BAS_FCTS *thisptr)
{
  static REAL_BBB D3;

  D3[0][1][2] = D3[0][2][1] = D3[1][0][2] = 
    D3[2][1][0] = D3[2][0][1] = D3[1][2][0] = 256.0*l[3];
  D3[0][1][3] = D3[0][3][1] = D3[1][0][3] = 
    D3[3][1][0] = D3[3][0][1] = D3[1][3][0] = 256.0*l[2];
  D3[0][2][3] = D3[0][3][2] = D3[2][0][3] = 
    D3[3][2][0] = D3[3][0][2] = D3[2][3][0] = 256.0*l[1]; 
  D3[1][2][3] = D3[1][3][2] = D3[2][1][3] = 
    D3[3][2][1] = D3[3][1][2] = D3[2][3][1] = 256.0*l[0];

  return (const REAL_BB *)D3;
}

static const REAL_BBB *D4_phi4c_3d(const REAL_B l, const BAS_FCTS *thisptr)
{
  static REAL_BBBB D4;

  D4[0][1][2][3] = D4[0][2][1][3] = D4[1][0][2][3] = 
    D4[2][1][0][3] = D4[2][0][1][3] = D4[1][2][0][3] =
    D4[0][1][3][2] = D4[0][3][1][2] = D4[1][0][3][2] = 
    D4[3][1][0][2] = D4[3][0][1][2] = D4[1][3][0][2] =
    D4[0][2][3][1] = D4[0][3][2][1] = D4[2][0][3][1] = 
    D4[3][2][0][1] = D4[3][0][2][1] = D4[2][3][0][1] = 
    D4[1][2][3][0] = D4[1][3][2][0] = D4[2][1][3][0] = 
    D4[3][2][1][0] = D4[3][1][2][0] = D4[2][3][1][0] = 256.0;

  return (const REAL_BBB *)D4;
}

/******************************************************************************/

#undef DEF_EL_VEC_4_3D
#define DEF_EL_VEC_4_3D(type, name)				\
  DEF_EL_VEC_CONST(type, name, N_BAS_LAG_4_3D, N_BAS_LAG_4_3D)

#undef DEFUN_GET_EL_VEC_4_3D
#define DEFUN_GET_EL_VEC_4_3D(name, type, admin, body, ...)		\
  static const EL_##type##_VEC *					\
  get_##name##4_3d(type##_VEC_TYPE *vec, const EL *el, __VA_ARGS__)	\
  {									\
    FUNCNAME("get_"#name"4_3d");					\
    static DEF_EL_VEC_4_3D(type, rvec_space);				\
    type##_VEC_TYPE *rvec = vec ? vec : rvec_space->vec;		\
    int n0, ibas, inode, node0, k;					\
    const int *indi;							\
    DOF **dofptr = el->dof, dof;					\
									\
    DEBUG_TEST_EXIT(true, "");						\
									\
    n0 = admin->n0_dof[VERTEX];						\
    for (ibas = inode = 0; inode < N_VERTICES_3D; inode++, ibas++) {	\
      dof = dofptr[inode][n0]; body;					\
    }									\
									\
    n0 = admin->n0_dof[EDGE];						\
    node0 = admin->mesh->node[EDGE];					\
    for (inode = 0; inode < N_EDGES_3D; inode++) {			\
      if (dofptr[vertex_of_edge_3d[inode][0]][0]			\
	  <								\
	  dofptr[vertex_of_edge_3d[inode][1]][0]) {			\
	dof = dofptr[node0+inode][n0+0]; body; ibas++;			\
	dof = dofptr[node0+inode][n0+1]; body; ibas++;			\
	dof = dofptr[node0+inode][n0+2]; body; ibas++;			\
      } else {								\
	dof = dofptr[node0+inode][n0+2]; body; ibas++;			\
	dof = dofptr[node0+inode][n0+1]; body; ibas++;			\
	dof = dofptr[node0+inode][n0+0]; body; ibas++;			\
      }									\
    }									\
									\
    n0 = admin->n0_dof[FACE];						\
    node0 = admin->mesh->node[FACE];					\
    for (inode = 0; inode < N_FACES_3D; inode++) {			\
      indi = order_f_indices_3d(el, inode);				\
									\
      for (k = 0; k < 3; k++) {						\
	dof = dofptr[node0+inode][n0+indi[k]]; body; ibas++;		\
      }									\
    }									\
									\
    DEBUG_TEST_EXIT(ibas == N_BAS_LAG_4_3D-1,				\
		    "Did not find enough dofs.\n");			\
									\
    n0 = admin->n0_dof[CENTER];						\
    node0 = admin->mesh->node[CENTER];					\
    dof = dofptr[node0][n0]; body;					\
									\
    return vec ? NULL : rvec_space;					\
  }									\
  struct _AI_semicolon_dummy

#undef DEFUN_GET_EL_DOF_VEC_4_3D
#define DEFUN_GET_EL_DOF_VEC_4_3D(name, type, ASSIGN)			\
  DEFUN_GET_EL_VEC_4_3D(_##name##_vec, type, dv->fe_space->admin,	\
			ASSIGN(dv->vec[dof], rvec[ibas]),		\
			const DOF_##type##_VEC *dv);			\
  static const EL_##type##_VEC *					\
  get_##name##_vec4_3d(type##_VEC_TYPE *vec, const EL *el,		\
		       const DOF_##type##_VEC *dv)			\
  {									\
    EL_##type##_VEC *vec_loc = dv->vec_loc;				\
    									\
    if (vec != NULL || vec_loc == NULL) {				\
      return get__##name##_vec4_3d(vec, el, dv);			\
    } else {								\
      get__##name##_vec4_3d(vec_loc->vec, el, dv);			\
      return vec_loc;							\
    }									\
  }									\
  struct _AI_semicolon_dummy
  

#undef COPY_EQ
#define COPY_EQ(a, b) (b) = (a)

/*--------------------------------------------------------------------------*/
/*  functions for combining basisfunctions with coefficients                */
/*--------------------------------------------------------------------------*/

static const int *order_f_indices_3d(const EL *el, int face)
{
  FUNCNAME("order_f_indices_3d");
  static const int sorted[DIM_FAC_3D][N_VERTICES_2D] = {
    /* Oh boy, the last two items are interchanged with respect to the
     * ordering defined by sorted_wall_vertices_3d[] in
     * element_3d.c. This does not matter, but it _is_ somewhat
     * ugly. We should not change this, because this would imply to
     * add an input filter for reading old simulation data. So: leave this!
     */
    {0,2,1}, {1,0,2}, {0,1,2}, {2,1,0}, {1,2,0}, {2,0,1}
  };
  int       no;
  const int *vof;
  DOF       **dof = el->dof;

  vof = vertex_of_wall_3d[face];
  no = -1;
  if (dof[vof[0]][0] < dof[vof[1]][0])
    no++;
  if (dof[vof[1]][0] < dof[vof[2]][0])
    no += 2;
  if (dof[vof[2]][0] < dof[vof[0]][0])
    no += 4;

  if (no >= 0  &&  no <= 5)
    return (const int *) sorted[no];
  else
  {
    MSG("can not sort face indices of element %d at face %d\n",
	INDEX(el), face);
    return NULL;
  }
}

DEFUN_GET_EL_VEC_4_3D(dof_indices, DOF, admin,
		      rvec[ibas] = dof,
		      const DOF_ADMIN *admin, const BAS_FCTS *thisptr);

static const EL_BNDRY_VEC *
get_bound4_3d(BNDRY_FLAGS *vec,
	      const EL_INFO *el_info, const BAS_FCTS *thisptr)
{
  FUNCNAME("get_bound4_3d");
  static DEF_EL_VEC_4_3D(BNDRY, rvec_space);
  BNDRY_FLAGS *rvec = vec ? vec : rvec_space->vec;
  int i, j, k;

  DEBUG_TEST_FLAG(FILL_BOUND, el_info);

  for (j = i = 0; i < N_VERTICES_3D; j++, i++) {
    BNDRY_FLAGS_CPY(rvec[j], el_info->vertex_bound[i]);
  }
  for (i = 0; i < N_EDGES_3D; j += 3, i++) {
    for (k = 0; k < 3; k++) {
      BNDRY_FLAGS_CPY(rvec[j+k], el_info->edge_bound[i]);
    }
  }
  for (i = 0; i < N_FACES_3D; j += 3, i++) {
    for (k = 0; k < 3; k++) {
      BNDRY_FLAGS_INIT(rvec[j+k]);
      BNDRY_FLAGS_SET(rvec[j+k], el_info->face_bound[i]);
    }
  }

  BNDRY_FLAGS_INIT(rvec[j]);

  return vec ? NULL : rvec_space;
}

/*--------------------------------------------------------------------*/
/*--- function for accessing a local DOF_INT_VEC                   ---*/
/*--------------------------------------------------------------------*/

DEFUN_GET_EL_DOF_VEC_4_3D(int, INT, COPY_EQ);

/*--------------------------------------------------------------------*/
/*--- function for accessing a local DOF_REAL_VEC                  ---*/
/*--------------------------------------------------------------------*/

DEFUN_GET_EL_DOF_VEC_4_3D(real, REAL, COPY_EQ);

/*--------------------------------------------------------------------*/
/*--- function for accessing a local DOF_REAL_D_VEC                ---*/
/*--------------------------------------------------------------------*/

DEFUN_GET_EL_DOF_VEC_4_3D(real_d, REAL_D, COPY_DOW);

/*--------------------------------------------------------------------*/
/*--- function for accessing a local DOF_SCHAR_VEC                 ---*/
/*--------------------------------------------------------------------*/

DEFUN_GET_EL_DOF_VEC_4_3D(schar, SCHAR, COPY_EQ);

/*--------------------------------------------------------------------*/
/*--- function for accessing a local DOF_UCHAR_VEC                 ---*/
/*--------------------------------------------------------------------*/

DEFUN_GET_EL_DOF_VEC_4_3D(uchar, UCHAR, COPY_EQ);

/*--------------------------------------------------------------------*/
/*--- function for accessing a local DOF_PTR_VEC                   ---*/
/*--------------------------------------------------------------------*/

DEFUN_GET_EL_DOF_VEC_4_3D(ptr, PTR, COPY_EQ);

/*--------------------------------------------------------------------*/
/*--- function for accessing a local DOF_REAL_DD_VEC               ---*/
/*--------------------------------------------------------------------*/

DEFUN_GET_EL_DOF_VEC_4_3D(real_dd, REAL_DD, _AI_MCOPY_DOW);

/*--------------------------------------------------------------------*/
/*--- function for local interpolaton of scalar functions          ---*/
/*--------------------------------------------------------------------*/
 
GENERATE_INTERPOL(/**/, 4, 3, N_BAS_LAG_4_3D);

/*--------------------------------------------------------------------*/
/*--- function for local interpolaton of vector functions          ---*/
/*--------------------------------------------------------------------*/

GENERATE_INTERPOL_D(/**/, 4, 3, N_BAS_LAG_4_3D);

GENERATE_INTERPOL_DOW(/**/, 4, 3, N_BAS_LAG_4_3D);

/*--------------------------------------------------------------------------*/
/*  functions for interpolation/ restriction during refinement/coarsening   */
/*--------------------------------------------------------------------------*/

static void real_refine_inter4_3d(DOF_REAL_VEC *drv, RC_LIST_EL *list, int n)
{
  FUNCNAME("real_refine_inter4_3d");
  DOF pd[N_BAS_LAG_4_3D];
  DOF cd[N_BAS_LAG_4_3D];
  EL                *el;
  REAL              *v = NULL;
  int                i, lr_set;
  U_CHAR             typ;
  const DOF_ADMIN   *admin;
  const BAS_FCTS *bas_fcts;

  if (n < 1) return;
  el = list->el_info.el;
  typ = list->el_info.el_type;

  GET_DOF_VEC(v, drv);
  if (!drv->fe_space)
  {
    ERROR("no fe_space in dof_real_vec %s\n", NAME(drv));
    return;
  }
  else if (!drv->fe_space->bas_fcts)
  {
    ERROR("no basis functions in fe_space %s\n", NAME(drv->fe_space));
    return;
  }
  GET_STRUCT(admin, drv->fe_space);
  GET_STRUCT(bas_fcts, drv->fe_space);

  get_dof_indices4_3d(pd, el, admin, bas_fcts);

/*--------------------------------------------------------------------------*/
/*  values on child[0]                                                      */
/*--------------------------------------------------------------------------*/

  get_dof_indices4_3d(cd, el->child[0], admin, bas_fcts);

  v[cd[3]] = (v[pd[5]]);
  v[cd[10]] = (0.2734375*v[pd[0]] - 0.0390625*v[pd[1]]
	       + 1.09375*v[pd[4]] - 0.546875*v[pd[5]]
	       + 0.21875*v[pd[6]]);
  v[cd[11]] = (v[pd[4]]);
  v[cd[12]] = (-0.0390625*v[pd[0]] + 0.0234375*v[pd[1]]
	       + 0.46875*v[pd[4]] + 0.703125*v[pd[5]]
	       - 0.15625*v[pd[6]]);
  v[cd[16]] = (0.0390625*(-v[pd[0]] - v[pd[1]])
	       + 0.03125*(v[pd[4]] + v[pd[6]])
	       + 0.015625*v[pd[5]] +
	       0.1875*(v[pd[7]]+v[pd[13]]-v[pd[31]]-v[pd[32]])
	       + 0.375*(-v[pd[8]] - v[pd[14]])
	       + 0.5*(v[pd[9]] + v[pd[15]]) + 0.75*v[pd[33]]);
  v[cd[17]] = (v[pd[33]]);
  v[cd[18]] = (0.0234375*(v[pd[0]] + v[pd[1]])
	       + 0.09375*(-v[pd[4]] - v[pd[6]]) + 0.140625*v[pd[5]]
	       + 0.0625*(-v[pd[7]] - v[pd[13]])
	       + 0.5625*(v[pd[31]] + v[pd[32]]));
  v[cd[19]] = (0.0390625*(-v[pd[0]] - v[pd[1]])
	       + 0.03125*(v[pd[4]] + v[pd[6]]) + 0.015625*v[pd[5]]
	       + 0.1875*(v[pd[10]]+v[pd[16]]-v[pd[28]]-v[pd[29]])
	       + 0.375*(-v[pd[11]] - v[pd[17]])
	       + 0.5*(v[pd[12]] + v[pd[18]]) + 0.75*v[pd[30]]);
  v[cd[20]] = (v[pd[30]]);
  v[cd[21]] = (0.0234375*(v[pd[0]] + v[pd[1]])
	       + 0.09375*(-v[pd[4]] - v[pd[6]]) + 0.140625*v[pd[5]]
	       + 0.0625*(-v[pd[10]] - v[pd[16]])
	       + 0.5625*(v[pd[28]] + v[pd[29]]));
  v[cd[22]] = (0.0390625*(-v[pd[0]] - v[pd[1]])
	       + 0.03125*(v[pd[4]] + v[pd[6]]) + 0.015625*v[pd[5]]
	       + 0.125*(v[pd[7]]-v[pd[8]]+v[pd[13]]-v[pd[14]]
			-v[pd[31]]-v[pd[32]])
	       + 0.0625*(v[pd[10]]+v[pd[16]]-v[pd[28]]-v[pd[29]])
	       + 0.25*(-v[pd[22]] - v[pd[25]] + v[pd[33]])
	       + 0.5*(v[pd[23]] + v[pd[26]] + v[pd[34]]));
  v[cd[23]] = (0.0390625*(-v[pd[0]] - v[pd[1]])
	       + 0.03125*(v[pd[4]] + v[pd[6]]) + 0.015625*v[pd[5]]
	       + 0.0625*(v[pd[7]]+v[pd[13]]-v[pd[31]]-v[pd[32]])
	       + 0.125*(v[pd[10]]-v[pd[11]]+v[pd[16]]-v[pd[17]]
			-v[pd[28]]-v[pd[29]])
	       + 0.25*(-v[pd[22]] - v[pd[25]] + v[pd[30]])
	       + 0.5*(v[pd[24]] + v[pd[27]] + v[pd[34]]));
  v[cd[24]] = (v[pd[34]]);
  v[cd[25]] = (0.0390625*(-v[pd[0]] - v[pd[1]])
	       + 0.15625*(v[pd[4]] + v[pd[6]]) - 0.234375*v[pd[5]]
	       + 0.3125*(v[pd[10]] - v[pd[29]]) + 0.0625*v[pd[16]]
	       + 0.9375*v[pd[28]]);
  v[cd[26]] = (0.0234375*v[pd[0]] - 0.0390625*v[pd[1]]
	       - 0.03125*v[pd[4]] - 0.046875*v[pd[5]]
	       + 0.09375*v[pd[6]]
	       + 0.125*(-v[pd[10]] + v[pd[16]] - v[pd[17]])
	       + 0.375*(v[pd[11]] + v[pd[28]] - v[pd[29]])
	       + 0.75*v[pd[30]]);
  v[cd[27]] = (v[pd[28]]);
  v[cd[28]] = (0.0390625*(-v[pd[0]] - v[pd[1]])
	       + 0.15625*(v[pd[4]] + v[pd[6]]) - 0.234375*v[pd[5]]
	       + 0.3125*(v[pd[7]] - v[pd[32]]) + 0.0625*v[pd[13]]
	       + 0.9375*v[pd[31]]);
  v[cd[29]] = (0.0234375*v[pd[0]] - 0.0390625*v[pd[1]]
	       - 0.03125*v[pd[4]] - 0.046875*v[pd[5]]
	       + 0.09375*v[pd[6]]
	       + 0.125*(-v[pd[7]] + v[pd[13]] - v[pd[14]])
	       + 0.375*(v[pd[8]] + v[pd[31]] - v[pd[32]])
	       + 0.75*v[pd[33]]);
  v[cd[30]] = (v[pd[31]]);
  v[cd[34]] = (0.0234375*v[pd[0]] - 0.0390625*v[pd[1]]
	       - 0.03125*v[pd[4]] - 0.046875*v[pd[5]]
	       + 0.09375*v[pd[6]]
	       + 0.0625*(-v[pd[7]]-v[pd[10]]+v[pd[13]]+v[pd[16]])
	       - 0.125*v[pd[22]] + 0.375*v[pd[25]]
	       + 0.1875*(v[pd[28]]-v[pd[29]]+v[pd[31]]-v[pd[32]])
	       + 0.75*v[pd[34]]);

/*--------------------------------------------------------------------------*/
/*  values on child[1]                                                      */
/*--------------------------------------------------------------------------*/

  get_dof_indices4_3d(cd, el->child[1], admin, bas_fcts);
  if (typ == 0)
  {
/*--------------------------------------------------------------------------*/
/*  parent of el_type 0                                                     */
/*--------------------------------------------------------------------------*/

    v[cd[10]] = (-0.0390625*v[pd[0]] + 0.2734375*v[pd[1]]
		 + 0.21875*v[pd[4]] - 0.546875*v[pd[5]]
		 + 1.09375*v[pd[6]]);
    v[cd[11]] = v[pd[6]];
    v[cd[12]] = (0.0234375*v[pd[0]] - 0.0390625*v[pd[1]]
		 - 0.15625*v[pd[4]] + 0.703125*v[pd[5]]
		 + 0.46875*v[pd[6]]);
    v[cd[25]] = (0.0390625*(-v[pd[0]] - v[pd[1]])
		 + 0.15625*(v[pd[4]] + v[pd[6]])
		 - 0.234375*v[pd[5]] + 0.0625*v[pd[7]]
		 + 0.3125*(v[pd[13]] - v[pd[31]])
		 + 0.9375*v[pd[32]]);
    v[cd[26]] = (-0.0390625*v[pd[0]] + 0.0234375*v[pd[1]]
		 + 0.09375*v[pd[4]] - 0.046875*v[pd[5]]
		 - 0.03125*v[pd[6]]
		 + 0.125*(v[pd[7]] - v[pd[8]] - v[pd[13]])
		 + 0.375*(v[pd[14]] - v[pd[31]] + v[pd[32]])
		 + 0.75*v[pd[33]]);
    v[cd[27]] = v[pd[32]];
    v[cd[28]] = (0.0390625*(-v[pd[0]] - v[pd[1]])
		 + 0.15625*(v[pd[4]] + v[pd[6]])
		 - 0.234375*v[pd[5]] + 0.0625*v[pd[10]]
		 + 0.3125*(v[pd[16]] - v[pd[28]])
		 + 0.9375*v[pd[29]]);
    v[cd[29]] = (-0.0390625*v[pd[0]] + 0.0234375*v[pd[1]]
		 + 0.09375*v[pd[4]] - 0.046875*v[pd[5]]
		 - 0.03125*v[pd[6]]
		 + 0.125*(v[pd[10]] - v[pd[11]] - v[pd[16]])
		 + 0.375*(v[pd[17]] - v[pd[28]] + v[pd[29]])
		 + 0.75*v[pd[30]]);
    v[cd[30]] = v[pd[29]];
    v[cd[34]] = (-0.0390625*v[pd[0]] + 0.0234375*v[pd[1]]
		 + 0.09375*v[pd[4]] - 0.046875*v[pd[5]]
		 - 0.03125*v[pd[6]]
		 + 0.0625*(v[pd[7]] + v[pd[10]]
			   -v[pd[13]] - v[pd[16]])
		 + 0.375*v[pd[22]] - 0.125*v[pd[25]]
		 + 0.1875*(-v[pd[28]] + v[pd[29]]
			   -v[pd[31]] + v[pd[32]])
		 + 0.75*v[pd[34]]);
  }
  else
  {
/*--------------------------------------------------------------------------*/
/*  parent of el_type 1|2                                                   */
/*--------------------------------------------------------------------------*/

    v[cd[10]] = (-0.0390625*v[pd[0]] + 0.2734375*v[pd[1]]
		 + 0.21875*v[pd[4]] - 0.546875*v[pd[5]]
		 + 1.09375*v[pd[6]]);
    v[cd[11]] = v[pd[6]];
    v[cd[12]] = (0.0234375*v[pd[0]] - 0.0390625*v[pd[1]]
		 - 0.15625*v[pd[4]] + 0.703125*v[pd[5]]
		 + 0.46875*v[pd[6]]);
    v[cd[25]] = (0.0390625*(-v[pd[0]] - v[pd[1]])
		 + 0.15625*(v[pd[4]] + v[pd[6]])
		 - 0.234375*v[pd[5]] + 0.0625*v[pd[10]]
		 + 0.3125*(v[pd[16]] - v[pd[28]])
		 + 0.9375*v[pd[29]]);
    v[cd[26]] = (-0.0390625*v[pd[0]] + 0.0234375*v[pd[1]]
		 + 0.09375*v[pd[4]] - 0.046875*v[pd[5]]
		 - 0.03125*v[pd[6]]
		 + 0.125*(v[pd[10]] - v[pd[11]] - v[pd[16]])
		 + 0.375*(v[pd[17]] - v[pd[28]] + v[pd[29]])
		 + 0.75*v[pd[30]]);
    v[cd[27]] = v[pd[29]];
    v[cd[28]] = (0.0390625*(-v[pd[0]] - v[pd[1]])
		 + 0.15625*(v[pd[4]] + v[pd[6]])
		 - 0.234375*v[pd[5]] + 0.0625*v[pd[7]]
		 + 0.3125*(v[pd[13]] - v[pd[31]])
		 + 0.9375*v[pd[32]]);
    v[cd[29]] = (-0.0390625*v[pd[0]] + 0.0234375*v[pd[1]]
		 + 0.09375*v[pd[4]] - 0.046875*v[pd[5]]
		 - 0.03125*v[pd[6]]
		 + 0.125*(v[pd[7]] - v[pd[8]] - v[pd[13]])
		 + 0.375*(v[pd[14]] - v[pd[31]] + v[pd[32]])
		 + 0.75*v[pd[33]]);
    v[cd[30]] = v[pd[32]];
    v[cd[34]] = (-0.0390625*v[pd[0]] + 0.0234375*v[pd[1]]
		 + 0.09375*v[pd[4]] - 0.046875*v[pd[5]]
		 - 0.03125*v[pd[6]]
		 + 0.0625*(v[pd[7]] + v[pd[10]]
			   - v[pd[13]] - v[pd[16]])
		 + 0.375*v[pd[22]] - 0.125*v[pd[25]]
		 + 0.1875*(-v[pd[28]] + v[pd[29]]
			   - v[pd[31]] + v[pd[32]])
		 + 0.75*v[pd[34]]);
  }

/*--------------------------------------------------------------------------*/
/*   adjust neighbour values                                                */
/*--------------------------------------------------------------------------*/

  for (i = 1; i < n; i++)
  {
    el = list[i].el_info.el;
    typ = list[i].el_info.el_type;

    get_dof_indices4_3d(pd, el, admin, bas_fcts);

    lr_set = 0;
    if (list[i].neigh[0]  &&  list[i].neigh[0]->no < i)
      lr_set = 1;

    if (list[i].neigh[1]  &&  list[i].neigh[1]->no < i)
      lr_set += 2;

    DEBUG_TEST_EXIT(lr_set, "no values set on both neighbours\n");

/*--------------------------------------------------------------------------*/
/*  values on child[0]                                                      */
/*--------------------------------------------------------------------------*/

    get_dof_indices4_3d(cd, el->child[0], admin, bas_fcts);

    switch(lr_set)
    {
    case 1:
      v[cd[16]] = (0.0390625*(-v[pd[0]] - v[pd[1]])
		   + 0.03125*(v[pd[4]] + v[pd[6]])
		   + 0.015625*v[pd[5]]
		   + 0.1875*(v[pd[7]] + v[pd[13]]
			     - v[pd[31]] - v[pd[32]])
		   + 0.375*(-v[pd[8]] - v[pd[14]])
		   + 0.5*(v[pd[9]] + v[pd[15]])
		   + 0.75*v[pd[33]]);
      v[cd[17]] = v[pd[33]];
      v[cd[18]] = (0.0234375*(v[pd[0]] + v[pd[1]])
		   + 0.09375*(-v[pd[4]] - v[pd[6]])
		   + 0.140625*v[pd[5]]
		   + 0.0625*(-v[pd[7]] - v[pd[13]])
		   + 0.5625*(v[pd[31]] + v[pd[32]]));
      v[cd[22]] = (0.0390625*(-v[pd[0]] - v[pd[1]])
		   + 0.03125*(v[pd[4]] + v[pd[6]])
		   + 0.015625*v[pd[5]]
		   + 0.125*(v[pd[7]] - v[pd[8]] + v[pd[13]]
			    - v[pd[14]] - v[pd[31]] - v[pd[32]])
		   + 0.0625*(v[pd[10]] + v[pd[16]]
			     - v[pd[28]] - v[pd[29]])
		   + 0.25*(-v[pd[22]] - v[pd[25]] + v[pd[33]])
		   + 0.5*(v[pd[23]] + v[pd[26]] + v[pd[34]]));
      v[cd[23]] = (0.0390625*(-v[pd[0]] - v[pd[1]])
		   + 0.03125*(v[pd[4]] + v[pd[6]])
		   + 0.015625*v[pd[5]]
		   + 0.0625*(v[pd[7]] + v[pd[13]]
			     - v[pd[31]] - v[pd[32]])
		   + 0.125*(v[pd[10]] - v[pd[11]] + v[pd[16]]
			    - v[pd[17]] - v[pd[28]] - v[pd[29]])
		   + 0.25*(-v[pd[22]] - v[pd[25]] + v[pd[30]])
		   + 0.5*(v[pd[24]] + v[pd[27]] + v[pd[34]]));
      v[cd[24]] = v[pd[34]];
      v[cd[28]] = (0.0390625*(-v[pd[0]] - v[pd[1]])
		   + 0.15625*(v[pd[4]] + v[pd[6]])
		   - 0.234375*v[pd[5]]
		   + 0.3125*(v[pd[7]] - v[pd[32]])
		   + 0.0625*v[pd[13]] + 0.9375*v[pd[31]]);
      v[cd[29]] = (0.0234375*v[pd[0]] - 0.0390625*v[pd[1]]
		   - 0.03125*v[pd[4]] - 0.046875*v[pd[5]]
		   + 0.09375*v[pd[6]]
		   + 0.125*(-v[pd[7]] + v[pd[13]] - v[pd[14]])
		   + 0.375*(v[pd[8]] + v[pd[31]] - v[pd[32]])
		   + 0.75*v[pd[33]]);
      v[cd[30]] = v[pd[31]];
      v[cd[34]] = (0.0234375*v[pd[0]] - 0.0390625*v[pd[1]]
		   - 0.03125*v[pd[4]] - 0.046875*v[pd[5]]
		   + 0.09375*v[pd[6]]
		   + 0.0625*(-v[pd[7]] - v[pd[10]]
			     + v[pd[13]] + v[pd[16]])
		   - 0.125*v[pd[22]] + 0.375*v[pd[25]]
		   + 0.1875*(v[pd[28]] - v[pd[29]]
			     + v[pd[31]] - v[pd[32]])
		   + 0.75*v[pd[34]]);
      break;
    case 2:
      v[cd[19]] = (0.0390625*(-v[pd[0]] - v[pd[1]])
		   + 0.03125*(v[pd[4]] + v[pd[6]])
		   + 0.015625*v[pd[5]]
		   + 0.1875*(v[pd[10]] + v[pd[16]]
			     - v[pd[28]] - v[pd[29]])
		   + 0.375*(-v[pd[11]] - v[pd[17]])
		   + 0.5*(v[pd[12]] + v[pd[18]])
		   + 0.75*v[pd[30]]);
      v[cd[20]] = v[pd[30]];
      v[cd[21]] = (0.0234375*(v[pd[0]] + v[pd[1]])
		   + 0.09375*(-v[pd[4]] - v[pd[6]])
		   + 0.140625*v[pd[5]]
		   + 0.0625*(-v[pd[10]] - v[pd[16]])
		   + 0.5625*(v[pd[28]] + v[pd[29]]));
      v[cd[22]] = (0.0390625*(-v[pd[0]] - v[pd[1]])
		   + 0.03125*(v[pd[4]] + v[pd[6]])
		   + 0.015625*v[pd[5]]
		   + 0.125*(v[pd[7]] - v[pd[8]] + v[pd[13]]
			    - v[pd[14]] - v[pd[31]] - v[pd[32]])
		   + 0.0625*(v[pd[10]] + v[pd[16]]
			     - v[pd[28]] - v[pd[29]])
		   + 0.25*(-v[pd[22]] - v[pd[25]] + v[pd[33]])
		   + 0.5*(v[pd[23]] + v[pd[26]] + v[pd[34]]));
      v[cd[23]] = (0.0390625*(-v[pd[0]] - v[pd[1]])
		   + 0.03125*(v[pd[4]] + v[pd[6]])
		   + 0.015625*v[pd[5]]
		   + 0.0625*(v[pd[7]] + v[pd[13]]
			     - v[pd[31]] - v[pd[32]])
		   + 0.125*(v[pd[10]] - v[pd[11]] + v[pd[16]]
			    - v[pd[17]] - v[pd[28]] - v[pd[29]])
		   + 0.25*(-v[pd[22]] - v[pd[25]] + v[pd[30]])
		   + 0.5*(v[pd[24]] + v[pd[27]] + v[pd[34]]));
      v[cd[24]] = v[pd[34]];
      v[cd[25]] = (0.0390625*(-v[pd[0]] - v[pd[1]])
		   + 0.15625*(v[pd[4]] + v[pd[6]])
		   - 0.234375*v[pd[5]]
		   + 0.3125*(v[pd[10]] - v[pd[29]])
		   + 0.0625*v[pd[16]] + 0.9375*v[pd[28]]);
      v[cd[26]] = (0.0234375*v[pd[0]] - 0.0390625*v[pd[1]]
		   - 0.03125*v[pd[4]] - 0.046875*v[pd[5]]
		   + 0.09375*v[pd[6]]
		   + 0.125*(-v[pd[10]] + v[pd[16]] - v[pd[17]])
		   + 0.375*(v[pd[11]] + v[pd[28]] - v[pd[29]])
		   + 0.75*v[pd[30]]);
      v[cd[27]] = v[pd[28]];
      v[cd[34]] = (0.0234375*v[pd[0]] - 0.0390625*v[pd[1]]
		   - 0.03125*v[pd[4]] - 0.046875*v[pd[5]]
		   + 0.09375*v[pd[6]]
		   + 0.0625*(-v[pd[7]] - v[pd[10]]
			     + v[pd[13]] + v[pd[16]])
		   - 0.125*v[pd[22]] + 0.375*v[pd[25]]
		   + 0.1875*(v[pd[28]] - v[pd[29]]
			     + v[pd[31]] - v[pd[32]])
		   + 0.75*v[pd[34]]);
      break;
    case 3:
      v[cd[22]] = (0.0390625*(-v[pd[0]] - v[pd[1]])
		   + 0.03125*(v[pd[4]] + v[pd[6]])
		   + 0.015625*v[pd[5]]
		   + 0.125*(v[pd[7]] - v[pd[8]] + v[pd[13]]
			    - v[pd[14]] - v[pd[31]] - v[pd[32]])
		   + 0.0625*(v[pd[10]] + v[pd[16]]
			     - v[pd[28]] - v[pd[29]])
		   + 0.25*(-v[pd[22]] - v[pd[25]] + v[pd[33]])
		   + 0.5*(v[pd[23]] + v[pd[26]] + v[pd[34]]));
      v[cd[23]] = (0.0390625*(-v[pd[0]] - v[pd[1]])
		   + 0.03125*(v[pd[4]] + v[pd[6]])
		   + 0.015625*v[pd[5]]
		   + 0.0625*(v[pd[7]] + v[pd[13]]
			     - v[pd[31]] - v[pd[32]])
		   + 0.125*(v[pd[10]] - v[pd[11]] + v[pd[16]]
			    - v[pd[17]] - v[pd[28]] - v[pd[29]])
		   + 0.25*(-v[pd[22]] - v[pd[25]] + v[pd[30]])
		   + 0.5*(v[pd[24]] + v[pd[27]] + v[pd[34]]));
      v[cd[24]] = v[pd[34]];
      v[cd[34]] = (0.0234375*v[pd[0]] - 0.0390625*v[pd[1]]
		   - 0.03125*v[pd[4]] - 0.046875*v[pd[5]]
		   + 0.09375*v[pd[6]]
		   + 0.0625*(-v[pd[7]] - v[pd[10]]
			     + v[pd[13]] + v[pd[16]])
		   - 0.125*v[pd[22]] + 0.375*v[pd[25]]
		   + 0.1875*(v[pd[28]] - v[pd[29]]
			     + v[pd[31]] - v[pd[32]])
		   + 0.75*v[pd[34]]);
      break;
    }

/*--------------------------------------------------------------------------*/
/*  values on child[1]                                                      */
/*--------------------------------------------------------------------------*/

    get_dof_indices4_3d(cd, el->child[1], admin, bas_fcts);

    if (typ == 0)
    {
      switch(lr_set)
      {
      case 1:
	v[cd[25]] = (0.0390625*(-v[pd[0]] - v[pd[1]])
		     + 0.15625*(v[pd[4]] + v[pd[6]])
		     - 0.234375*v[pd[5]] + 0.0625*v[pd[7]]
		     + 0.3125*(v[pd[13]] - v[pd[31]])
		     + 0.9375*v[pd[32]]);
	v[cd[26]] = (-0.0390625*v[pd[0]] + 0.0234375*v[pd[1]]
		     + 0.09375*v[pd[4]] - 0.046875*v[pd[5]]
		     - 0.03125*v[pd[6]]
		     + 0.125*(v[pd[7]] - v[pd[8]] - v[pd[13]])
		     + 0.375*(v[pd[14]] - v[pd[31]] + v[pd[32]])
		     + 0.75*v[pd[33]]);
	v[cd[27]] = v[pd[32]];
	v[cd[34]] = (-0.0390625*v[pd[0]] + 0.0234375*v[pd[1]]
		     + 0.09375*v[pd[4]] - 0.046875*v[pd[5]]
		     - 0.03125*v[pd[6]]
		     + 0.0625*(v[pd[7]] + v[pd[10]]
			       - v[pd[13]] - v[pd[16]])
		     + 0.375*v[pd[22]] - 0.125*v[pd[25]]
		     + 0.1875*(-v[pd[28]] + v[pd[29]]
			       - v[pd[31]] + v[pd[32]])
		     + 0.75*v[pd[34]]);
	break;
      case 2:
	v[cd[28]] = (0.0390625*(-v[pd[0]] - v[pd[1]])
		     + 0.15625*(v[pd[4]] + v[pd[6]])
		     - 0.234375*v[pd[5]] + 0.0625*v[pd[10]]
		     + 0.3125*(v[pd[16]] - v[pd[28]])
		     + 0.9375*v[pd[29]]);
	v[cd[29]] = (-0.0390625*v[pd[0]] + 0.0234375*v[pd[1]]
		     + 0.09375*v[pd[4]] - 0.046875*v[pd[5]]
		     - 0.03125*v[pd[6]]
		     + 0.125*(v[pd[10]] - v[pd[11]] - v[pd[16]])
		     + 0.375*(v[pd[17]] - v[pd[28]] + v[pd[29]])
		     + 0.75*v[pd[30]]);
	v[cd[30]] = v[pd[29]];
	v[cd[34]] = (-0.0390625*v[pd[0]] + 0.0234375*v[pd[1]]
		     + 0.09375*v[pd[4]] - 0.046875*v[pd[5]]
		     - 0.03125*v[pd[6]]
		     + 0.0625*(v[pd[7]] + v[pd[10]]
			       - v[pd[13]] - v[pd[16]])
		     + 0.375*v[pd[22]] - 0.125*v[pd[25]]
		     + 0.1875*(-v[pd[28]] + v[pd[29]]
			       - v[pd[31]] + v[pd[32]])
		     + 0.75*v[pd[34]]);
	break;
      case 3:
	v[cd[34]] = (-0.0390625*v[pd[0]] + 0.0234375*v[pd[1]]
		     + 0.09375*v[pd[4]] - 0.046875*v[pd[5]]
		     - 0.03125*v[pd[6]]
		     + 0.0625*(v[pd[7]] + v[pd[10]]
			       - v[pd[13]] - v[pd[16]])
		     + 0.375*v[pd[22]] - 0.125*v[pd[25]]
		     + 0.1875*(-v[pd[28]] + v[pd[29]]
			       - v[pd[31]] + v[pd[32]])
		     + 0.75*v[pd[34]]);
	break;
      }
    }
    else
    {
      switch(lr_set)
      {
      case 1:
	v[cd[28]] = (0.0390625*(-v[pd[0]] - v[pd[1]])
		     + 0.15625*(v[pd[4]] + v[pd[6]])
		     - 0.234375*v[pd[5]] + 0.0625*v[pd[7]]
		     + 0.3125*(v[pd[13]] - v[pd[31]])
		     + 0.9375*v[pd[32]]);
	v[cd[29]] = (-0.0390625*v[pd[0]] + 0.0234375*v[pd[1]]
		     + 0.09375*v[pd[4]] - 0.046875*v[pd[5]]
		     - 0.03125*v[pd[6]]
		     + 0.125*(v[pd[7]] - v[pd[8]] - v[pd[13]])
		     + 0.375*(v[pd[14]] - v[pd[31]] + v[pd[32]])
		     + 0.75*v[pd[33]]);
	v[cd[30]] = v[pd[32]];
	v[cd[34]] = (-0.0390625*v[pd[0]] + 0.0234375*v[pd[1]]
		     + 0.09375*v[pd[4]] - 0.046875*v[pd[5]]
		     - 0.03125*v[pd[6]]
		     + 0.0625*(v[pd[7]] + v[pd[10]]
			       - v[pd[13]] - v[pd[16]])
		     + 0.375*v[pd[22]] - 0.125*v[pd[25]]
		     + 0.1875*(-v[pd[28]] + v[pd[29]]
			       - v[pd[31]] + v[pd[32]])
		     + 0.75*v[pd[34]]);
	break;
      case 2:
	v[cd[25]] = (0.0390625*(-v[pd[0]] - v[pd[1]])
		     + 0.15625*(v[pd[4]] + v[pd[6]])
		     - 0.234375*v[pd[5]] + 0.0625*v[pd[10]]
		     + 0.3125*(v[pd[16]] - v[pd[28]])
		     + 0.9375*v[pd[29]]);
	v[cd[26]] = (-0.0390625*v[pd[0]] + 0.0234375*v[pd[1]]
		     + 0.09375*v[pd[4]] - 0.046875*v[pd[5]]
		     - 0.03125*v[pd[6]]
		     + 0.125*(v[pd[10]] - v[pd[11]] - v[pd[16]])
		     + 0.375*(v[pd[17]] - v[pd[28]] + v[pd[29]])
		     + 0.75*v[pd[30]]);
	v[cd[27]] = v[pd[29]];
	v[cd[34]] = (-0.0390625*v[pd[0]] + 0.0234375*v[pd[1]]
		     + 0.09375*v[pd[4]] - 0.046875*v[pd[5]]
		     - 0.03125*v[pd[6]]
		     + 0.0625*(v[pd[7]] + v[pd[10]]
			       - v[pd[13]] - v[pd[16]])
		     + 0.375*v[pd[22]] - 0.125*v[pd[25]]
		     + 0.1875*(-v[pd[28]] + v[pd[29]]
			       - v[pd[31]] + v[pd[32]])
		     + 0.75*v[pd[34]]);
	break;
      case 3:
	v[cd[34]] = (-0.0390625*v[pd[0]] + 0.0234375*v[pd[1]]
		     + 0.09375*v[pd[4]] - 0.046875*v[pd[5]]
		     - 0.03125*v[pd[6]]
		     + 0.0625*(v[pd[7]] + v[pd[10]]
			       - v[pd[13]] - v[pd[16]])
		     + 0.375*v[pd[22]] - 0.125*v[pd[25]]
		     + 0.1875*(-v[pd[28]] + v[pd[29]]
			       - v[pd[31]] + v[pd[32]])
		     + 0.75*v[pd[34]]);
	break;
      }
    }
  }

  return;
}

static void real_coarse_inter4_3d(DOF_REAL_VEC *drv, RC_LIST_EL *list, int n)
{
  FUNCNAME("real_coarse_inter4_3d");
  DOF pd[N_BAS_LAG_4_3D];
  DOF cd[N_BAS_LAG_4_3D];
  EL              *el;
  REAL            *v = NULL;
  int             i, lr_set;
  U_CHAR          typ;
  const DOF_ADMIN *admin;
  const BAS_FCTS  *bas_fcts;

  if (n < 1) return;
  el = list->el_info.el;
  typ = list->el_info.el_type;

  GET_DOF_VEC(v, drv);
  if (!drv->fe_space)
  {
    ERROR("no fe_space in dof_real_vec %s\n", NAME(drv));
    return;
  }
  else if (!drv->fe_space->bas_fcts)
  {
    ERROR("no basis functions in fe_space %s\n", NAME(drv->fe_space));
    return;
  }
  GET_STRUCT(admin, drv->fe_space);
  GET_STRUCT(bas_fcts, drv->fe_space);

  get_dof_indices4_3d(pd, el, admin, bas_fcts);

/*--------------------------------------------------------------------------*/
/*  values on child[0]                                                      */
/*--------------------------------------------------------------------------*/

  get_dof_indices4_3d(cd, el->child[0], admin, bas_fcts);

  v[pd[4]] = v[cd[11]];
  v[pd[5]] = v[cd[3]];
  v[pd[28]] = v[cd[27]];
  v[pd[30]] = v[cd[20]];
  v[pd[31]] = v[cd[30]];
  v[pd[33]] = v[cd[17]];
  v[pd[34]] = v[cd[24]];

/*--------------------------------------------------------------------------*/
/*  values on child[1]                                                      */
/*--------------------------------------------------------------------------*/

  get_dof_indices4_3d(cd, el->child[1], admin, bas_fcts);

  if (typ == 0)
  {
/*--------------------------------------------------------------------------*/
/*  parent of el_type 0                                                     */
/*--------------------------------------------------------------------------*/

    v[pd[6]] = v[cd[11]];
    v[pd[29]] = v[cd[30]];
    v[pd[32]] = v[cd[27]];
  }
  else
  {
/*--------------------------------------------------------------------------*/
/*  parent of el_type 1|2                                                   */
/*--------------------------------------------------------------------------*/

    v[pd[6]] = v[cd[11]];
    v[pd[29]] = v[cd[27]];
    v[pd[32]] = v[cd[30]];
  }

/*--------------------------------------------------------------------------*/
/*   adjust neighbour values                                                */
/*--------------------------------------------------------------------------*/

  for (i = 1; i < n; i++)
  {
    el = list[i].el_info.el;
    typ = list[i].el_info.el_type;

    get_dof_indices4_3d(pd, el, admin, bas_fcts);

    lr_set = 0;
    if (list[i].neigh[0]  &&  list[i].neigh[0]->no < i)
      lr_set = 1;

    if (list[i].neigh[1]  &&  list[i].neigh[1]->no < i)
      lr_set += 2;

    DEBUG_TEST_EXIT(lr_set, "no values set on both neighbours\n");

/*--------------------------------------------------------------------------*/
/*  values on child[0]                                                      */
/*--------------------------------------------------------------------------*/

    get_dof_indices4_3d(cd, el->child[0], admin, bas_fcts);

    switch(lr_set)
    {
    case 1:
      v[pd[31]] = v[cd[30]];
      v[pd[33]] = v[cd[17]];
      v[pd[34]] = v[cd[24]];
      break;
    case 2:
      v[pd[28]] = v[cd[27]];
      v[pd[30]] = v[cd[20]];
      v[pd[34]] = v[cd[24]];
      break;
    case 3:
      v[pd[34]] = v[cd[24]];
      break;
    }

/*--------------------------------------------------------------------------*/
/*  values on child[1]                                                      */
/*--------------------------------------------------------------------------*/

    get_dof_indices4_3d(cd, el->child[1], admin, bas_fcts);

    if (typ == 0)
    {
      switch(lr_set)
      {
      case 1:
	v[pd[32]] = v[cd[27]];
	break;
      case 2:
	v[pd[29]] = v[cd[30]];
	break;
      }
    }
    else
    {
      switch(lr_set)
      {
      case 1:
	v[pd[32]] = v[cd[30]];
	break;
      case 2:
	v[pd[29]] = v[cd[27]];
	break;
      }
    }
  }

  return;
}



static void real_coarse_restr4_3d(DOF_REAL_VEC *drv, RC_LIST_EL *list, int n)
{
  FUNCNAME("real_coarse_restr4_3d");
  DOF pd[N_BAS_LAG_4_3D];
  DOF cd[N_BAS_LAG_4_3D];
  EL                *el;
  REAL              *v = NULL;
  int               i, lr_set;
  U_CHAR            typ;
  const DOF_ADMIN   *admin;
  const BAS_FCTS    *bas_fcts;

  if (n < 1) return;
  el = list->el_info.el;
  typ = list->el_info.el_type;

  GET_DOF_VEC(v, drv);
  GET_DOF_VEC(v, drv);
  if (!drv->fe_space)
  {
    ERROR("no fe_space in dof_real_vec %s\n", NAME(drv));
    return;
  }
  else if (!drv->fe_space->bas_fcts)
  {
    ERROR("no basis functions in fe_space %s\n", NAME(drv->fe_space));
    return;
  }
  GET_STRUCT(admin, drv->fe_space);
  GET_STRUCT(bas_fcts, drv->fe_space);

  get_dof_indices4_3d(pd, el, admin, bas_fcts);

/*--------------------------------------------------------------------------*/
/*  values on child[0]                                                      */
/*--------------------------------------------------------------------------*/

  get_dof_indices4_3d(cd, el->child[0], admin, bas_fcts);

  v[pd[0]] += (0.2734375*v[cd[10]]
	       + 0.0390625*(-v[cd[12]] - v[cd[16]] - v[cd[19]]
			    - v[cd[22]] - v[cd[23]] - v[cd[25]]
			    - v[cd[28]])
	       + 0.0234375*(v[cd[18]] + v[cd[21]] + v[cd[26]]
			    + v[cd[29]] + v[cd[34]]));
  v[pd[1]] += (0.0390625*(-v[cd[10]] - v[cd[16]] - v[cd[19]]
			  - v[cd[22]] - v[cd[23]] - v[cd[25]]
			  - v[cd[26]] - v[cd[28]] - v[cd[29]]
			  - v[cd[34]])
	       + 0.0234375*(v[cd[12]] + v[cd[18]] + v[cd[21]]));
  v[pd[4]] = (v[cd[11]] + 1.09375*v[cd[10]] + 0.46875*v[cd[12]]
	      + 0.03125*(v[cd[16]] + v[cd[19]] + v[cd[22]]
			 + v[cd[23]] - v[cd[26]] - v[cd[29]]
			 - v[cd[34]])
	      + 0.09375*(-v[cd[18]] - v[cd[21]])
	      + 0.15625*(v[cd[25]] + v[cd[28]]));
  v[pd[5]] = (v[cd[3]] - 0.546875*v[cd[10]]
	      + 0.703125*v[cd[12]]
	      + 0.015625*(v[cd[16]] + v[cd[19]]
			  + v[cd[22]] + v[cd[23]])
	      + 0.140625*(v[cd[18]] + v[cd[21]])
	      + 0.234375*(-v[cd[25]] - v[cd[28]])
	      + 0.046875*(-v[cd[26]] - v[cd[29]] - v[cd[34]]));
  v[pd[6]] = (0.21875*v[cd[10]]
	      + 0.15625*(-v[cd[12]] + v[cd[25]] + v[cd[28]])
	      + 0.03125*(v[cd[16]] + v[cd[19]]
			 + v[cd[22]] + v[cd[23]])
	      + 0.09375*(-v[cd[18]] - v[cd[21]] + v[cd[26]]
			 + v[cd[29]] + v[cd[34]]));
  v[pd[7]] += (0.1875*v[cd[16]]
	       + 0.0625*(-v[cd[18]] + v[cd[23]] - v[cd[34]])
	       + 0.125*(v[cd[22]] - v[cd[29]]) + 0.3125*v[cd[28]]);
  v[pd[8]] += (0.375*(-v[cd[16]] + v[cd[29]]) - 0.125*v[cd[22]]);
  v[pd[9]] += 0.5*v[cd[16]];
  v[pd[10]] += (0.1875*v[cd[19]]
		+ 0.0625*(-v[cd[21]] + v[cd[22]] - v[cd[34]])
		+ 0.125*(v[cd[23]] - v[cd[26]])
		+ 0.3125*v[cd[25]]);
  v[pd[11]] += (0.375*(-v[cd[19]] + v[cd[26]])
		- 0.125*v[cd[23]]);
  v[pd[12]] += 0.5*v[cd[19]];
  v[pd[13]] += (0.1875*v[cd[16]]
		+ 0.0625*(-v[cd[18]] + v[cd[23]]
			  + v[cd[28]] + v[cd[34]])
		+ 0.125*(v[cd[22]] + v[cd[29]]));
  v[pd[14]] += (-0.375*v[cd[16]] + 0.125*(-v[cd[22]] - v[cd[29]]));
  v[pd[15]] += 0.5*v[cd[16]];
  v[pd[16]] += (0.1875*v[cd[19]]
		+ 0.0625*(-v[cd[21]] + v[cd[22]]
			  + v[cd[25]] + v[cd[34]])
		+ 0.125*(v[cd[23]] + v[cd[26]]));
  v[pd[17]] += (-0.375*v[cd[19]] + 0.125*(-v[cd[23]] - v[cd[26]]));
  v[pd[18]] += 0.5*v[cd[19]];
  v[pd[22]] += (0.25*(-v[cd[22]] - v[cd[23]]) - 0.125*v[cd[34]]);
  v[pd[23]] += 0.5*v[cd[22]];
  v[pd[24]] += 0.5*v[cd[23]];
  v[pd[25]] += (0.25*(-v[cd[22]] - v[cd[23]]) + 0.375*v[cd[34]]);
  v[pd[26]] += 0.5*v[cd[22]];
  v[pd[27]] += 0.5*v[cd[23]];
  v[pd[28]] = (v[cd[27]] + 0.1875*(-v[cd[19]] + v[cd[34]])
	       + 0.5625*v[cd[21]] - 0.0625*v[cd[22]]
	       - 0.125*v[cd[23]] + 0.9375*v[cd[25]]
	       + 0.375*v[cd[26]]);
  v[pd[29]] = (0.1875*(-v[cd[19]] - v[cd[34]])
	       + 0.5625*v[cd[21]] - 0.0625*v[cd[22]]
	       - 0.125*v[cd[23]] - 0.3125*v[cd[25]]
	       - 0.375*v[cd[26]]);
  v[pd[30]] = (v[cd[20]] + 0.75*(v[cd[19]] + v[cd[26]])
	       + 0.25*v[cd[23]]);
  v[pd[31]] = (v[cd[30]] + 0.1875*(-v[cd[16]] + v[cd[34]])
	       + 0.5625*v[cd[18]] - 0.125*v[cd[22]]
	       - 0.0625*v[cd[23]] + 0.9375*v[cd[28]]
	       + 0.375*v[cd[29]]);
  v[pd[32]] = (0.1875*(-v[cd[16]] - v[cd[34]])
	       + 0.5625*v[cd[18]] - 0.125*v[cd[22]]
	       - 0.0625*v[cd[23]] - 0.3125*v[cd[28]]
	       - 0.375*v[cd[29]]);
  v[pd[33]] = (v[cd[17]] + 0.75*(v[cd[16]] + v[cd[29]])
	       + 0.25*v[cd[22]]);
  v[pd[34]] = (v[cd[24]] + 0.5*(v[cd[22]] + v[cd[23]])
	       + 0.75*v[cd[34]]);

/*--------------------------------------------------------------------------*/
/*  values on child[1]                                                      */
/*--------------------------------------------------------------------------*/

  get_dof_indices4_3d(cd, el->child[1], admin, bas_fcts);

  if (typ == 0)
  {
/*--------------------------------------------------------------------------*/
/*  parent of el_type 0                                                     */
/*--------------------------------------------------------------------------*/

    v[pd[0]] += (0.0390625*(-v[cd[10]] - v[cd[25]] - v[cd[26]]
			    - v[cd[28]] - v[cd[29]] - v[cd[34]])
		 + 0.0234375*v[cd[12]]);
    v[pd[1]] += (0.2734375*v[cd[10]]
		 + 0.0390625*(-v[cd[12]] - v[cd[25]] - v[cd[28]])
		 + 0.0234375*(v[cd[26]] + v[cd[29]] + v[cd[34]]));
    v[pd[4]] += (0.21875*v[cd[10]]
		 + 0.15625*(-v[cd[12]] + v[cd[25]] + v[cd[28]])
		 + 0.09375*(v[cd[26]] + v[cd[29]] + v[cd[34]]));
    v[pd[5]] += (-0.546875*v[cd[10]] + 0.703125*v[cd[12]]
		 + 0.234375*(-v[cd[25]] - v[cd[28]])
		 + 0.046875*(-v[cd[26]] - v[cd[29]] - v[cd[34]]));
    v[pd[6]] += (v[cd[11]] + 1.09375*v[cd[10]] + 0.46875*v[cd[12]]
		 + 0.15625*(v[cd[25]] + v[cd[28]])
		 + 0.03125*(-v[cd[26]] - v[cd[29]] - v[cd[34]]));
    v[pd[7]] += (0.0625*(v[cd[25]] + v[cd[34]]) + 0.125*v[cd[26]]);
    v[pd[8]] += -0.125*v[cd[26]];
    v[pd[10]] += (0.0625*(v[cd[28]] + v[cd[34]]) + 0.125*v[cd[29]]);
    v[pd[11]] += -0.125*v[cd[29]];
    v[pd[13]] += (0.3125*v[cd[25]] - 0.125*v[cd[26]]
		  - 0.0625*v[cd[34]]);
    v[pd[14]] += 0.375*v[cd[26]];
    v[pd[16]] += (0.3125*v[cd[28]] - 0.125*v[cd[29]]
		  - 0.0625*v[cd[34]]);
    v[pd[17]] += 0.375*v[cd[29]];
    v[pd[22]] += 0.375*v[cd[34]];
    v[pd[25]] += -0.125*v[cd[34]];
    v[pd[28]] += (-0.3125*v[cd[28]] - 0.375*v[cd[29]]
		  - 0.1875*v[cd[34]]);
    v[pd[29]] += (v[cd[30]] + 0.9375*v[cd[28]] + 0.375*v[cd[29]]
		  + 0.1875*v[cd[34]]);
    v[pd[30]] += 0.75*v[cd[29]];
    v[pd[31]] += (-0.3125*v[cd[25]] - 0.375*v[cd[26]]
		  - 0.1875*v[cd[34]]);
    v[pd[32]] += (v[cd[27]] + 0.9375*v[cd[25]]
		  + 0.375*v[cd[26]] + 0.1875*v[cd[34]]);
    v[pd[33]] += 0.75*v[cd[26]];
    v[pd[34]] += 0.75*v[cd[34]];
  }
  else
  {
/*--------------------------------------------------------------------------*/
/*  parent of el_type 1|2                                                   */
/*--------------------------------------------------------------------------*/

    v[pd[0]] += (0.0390625*(-v[cd[10]] - v[cd[25]] - v[cd[26]]
			    - v[cd[28]] - v[cd[29]] - v[cd[34]])
		 + 0.0234375*v[cd[12]]);
    v[pd[1]] += (0.2734375*v[cd[10]]
		 + 0.0390625*(-v[cd[12]] - v[cd[25]] - v[cd[28]])
		 + 0.0234375*(v[cd[26]] + v[cd[29]] + v[cd[34]]));
    v[pd[4]] += (0.21875*v[cd[10]]
		 + 0.15625*(-v[cd[12]] + v[cd[25]] + v[cd[28]])
		 + 0.09375*(v[cd[26]] + v[cd[29]] + v[cd[34]]));
    v[pd[5]] += (-0.546875*v[cd[10]] + 0.703125*v[cd[12]]
		 + 0.234375*(-v[cd[25]] - v[cd[28]])
		 + 0.046875*(-v[cd[26]] - v[cd[29]] - v[cd[34]]));
    v[pd[6]] += (v[cd[11]] + 1.09375*v[cd[10]] + 0.46875*v[cd[12]]
		 + 0.15625*(v[cd[25]] + v[cd[28]])
		 + 0.03125*(-v[cd[26]] - v[cd[29]] - v[cd[34]]));
    v[pd[7]] += (0.0625*(v[cd[28]] + v[cd[34]]) + 0.125*v[cd[29]]);
    v[pd[8]] += -0.125*v[cd[29]];
    v[pd[10]] += (0.0625*(v[cd[25]] + v[cd[34]]) + 0.125*v[cd[26]]);
    v[pd[11]] += -0.125*v[cd[26]];
    v[pd[13]] += (0.3125*v[cd[28]] - 0.125*v[cd[29]]
		  - 0.0625*v[cd[34]]);
    v[pd[14]] += 0.375*v[cd[29]];
    v[pd[16]] += (0.3125*v[cd[25]] - 0.125*v[cd[26]]
		  - 0.0625*v[cd[34]]);
    v[pd[17]] += 0.375*v[cd[26]];
    v[pd[22]] += 0.375*v[cd[34]];
    v[pd[25]] += -0.125*v[cd[34]];
    v[pd[28]] += (-0.3125*v[cd[25]] - 0.375*v[cd[26]]
		  - 0.1875*v[cd[34]]);
    v[pd[29]] += (v[cd[27]] + 0.9375*v[cd[25]] + 0.375*v[cd[26]]
		  + 0.1875*v[cd[34]]);
    v[pd[30]] += 0.75*v[cd[26]];
    v[pd[31]] += (-0.3125*v[cd[28]] - 0.375*v[cd[29]]
		  - 0.1875*v[cd[34]]);
    v[pd[32]] += (v[cd[30]] + 0.9375*v[cd[28]]
		  + 0.375*v[cd[29]] + 0.1875*v[cd[34]]);
    v[pd[33]] += 0.75*v[cd[29]];
    v[pd[34]] += 0.75*v[cd[34]];
  }

/*--------------------------------------------------------------------------*/
/*   adjust neighbour values                                                */
/*--------------------------------------------------------------------------*/

  for (i = 1; i < n; i++)
  {
    el = list[i].el_info.el;
    typ = list[i].el_info.el_type;

    get_dof_indices4_3d(pd, el, admin, bas_fcts);

    lr_set = 0;
    if (list[i].neigh[0]  &&  list[i].neigh[0]->no < i)
      lr_set = 1;

    if (list[i].neigh[1]  &&  list[i].neigh[1]->no < i)
      lr_set += 2;

    DEBUG_TEST_EXIT(lr_set, "no values set on both neighbours\n");

/*--------------------------------------------------------------------------*/
/*  values on child[0]                                                      */
/*--------------------------------------------------------------------------*/

    get_dof_indices4_3d(cd, el->child[0], admin, bas_fcts);

    switch(lr_set)
    {
    case 1:
      v[pd[0]] += (0.0390625*(-v[cd[16]] - v[cd[22]]
			      - v[cd[23]] - v[cd[28]])
		   + 0.0234375*(v[cd[18]] + v[cd[29]] + v[cd[34]]));
      v[pd[1]] += (0.0390625*(-v[cd[16]] - v[cd[22]] - v[cd[23]]
			      - v[cd[28]] - v[cd[29]] - v[cd[34]])
		   + 0.0234375*v[cd[18]]);
      v[pd[4]] += (0.03125*(v[cd[16]] + v[cd[22]] + v[cd[23]]
			    - v[cd[29]] - v[cd[34]])
		   - 0.09375*v[cd[18]] + 0.15625*v[cd[28]]);
      v[pd[5]] += (0.015625*(v[cd[16]] + v[cd[22]] + v[cd[23]])
		   + 0.140625*v[cd[18]] - 0.234375*v[cd[28]]
		   + 0.046875*(-v[cd[29]] - v[cd[34]]));
      v[pd[6]] += (0.03125*(v[cd[16]] + v[cd[22]] + v[cd[23]])
		   + 0.09375*(-v[cd[18]] + v[cd[29]] + v[cd[34]])
		   + 0.15625*v[cd[28]]);
      v[pd[7]] += (0.1875*v[cd[16]]
		   + 0.0625*(-v[cd[18]] + v[cd[23]] - v[cd[34]])
		   + 0.125*(v[cd[22]] - v[cd[29]])
		   + 0.3125*v[cd[28]]);
      v[pd[8]] += (0.375*(-v[cd[16]] + v[cd[29]])
		   - 0.125*v[cd[22]]);
      v[pd[9]] += 0.5*v[cd[16]];
      v[pd[10]] += (0.0625*(v[cd[22]] - v[cd[34]]) + 0.125*v[cd[23]]);
      v[pd[11]] += -0.125*v[cd[23]];
      v[pd[13]] += (0.1875*v[cd[16]]
		    + 0.0625*(-v[cd[18]] + v[cd[23]]
			      + v[cd[28]] + v[cd[34]])
		    + 0.125*(v[cd[22]] + v[cd[29]]));
      v[pd[14]] += (-0.375*v[cd[16]]
		    + 0.125*(-v[cd[22]] - v[cd[29]]));
      v[pd[15]] += 0.5*v[cd[16]];
      v[pd[16]] += (0.0625*(v[cd[22]] + v[cd[34]]) + 0.125*v[cd[23]]);
      v[pd[17]] += -0.125*v[cd[23]];
      v[pd[22]] += (0.25*(-v[cd[22]] - v[cd[23]]) - 0.125*v[cd[34]]);
      v[pd[23]] += 0.5*v[cd[22]];
      v[pd[24]] += 0.5*v[cd[23]];
      v[pd[25]] += (0.25*(-v[cd[22]] - v[cd[23]]) + 0.375*v[cd[34]]);
      v[pd[26]] += 0.5*v[cd[22]];
      v[pd[27]] += 0.5*v[cd[23]];
      v[pd[28]] += (-0.0625*v[cd[22]] - 0.125*v[cd[23]]
		    + 0.1875*v[cd[34]]);
      v[pd[29]] += (-0.0625*v[cd[22]] - 0.125*v[cd[23]]
		    - 0.1875*v[cd[34]]);
      v[pd[30]] += 0.25*v[cd[23]];
      v[pd[31]] = (v[cd[30]] + 0.1875*(-v[cd[16]] + v[cd[34]])
		   + 0.5625*v[cd[18]] - 0.125*v[cd[22]]
		   - 0.0625*v[cd[23]] + 0.9375*v[cd[28]]
		   + 0.375*v[cd[29]]);
      v[pd[32]] = (0.1875*(-v[cd[16]] - v[cd[34]])
		   + 0.5625*v[cd[18]] - 0.125*v[cd[22]]
		   - 0.0625*v[cd[23]] - 0.3125*v[cd[28]]
		   - 0.375*v[cd[29]]);
      v[pd[33]] = (v[cd[17]] + 0.75*(v[cd[16]] + v[cd[29]])
		   + 0.25*v[cd[22]]);
      v[pd[34]] = (v[cd[24]] + 0.5*(v[cd[22]] + v[cd[23]])
		   + 0.75*v[cd[34]]);
      break;
    case 2:
      v[pd[0]] += (0.0390625*(-v[cd[19]] - v[cd[22]]
			      - v[cd[23]] - v[cd[25]])
		   + 0.0234375*(v[cd[21]] + v[cd[26]] + v[cd[34]]));
      v[pd[1]] += (0.0390625*(-v[cd[19]] - v[cd[22]] - v[cd[23]]
			      - v[cd[25]] - v[cd[26]] - v[cd[34]])
		   + 0.0234375*v[cd[21]]);
      v[pd[4]] += (0.03125*(v[cd[19]] + v[cd[22]] + v[cd[23]]
			    - v[cd[26]] - v[cd[34]])
		   - 0.09375*v[cd[21]] + 0.15625*v[cd[25]]);
      v[pd[5]] += (0.015625*(v[cd[19]] + v[cd[22]] + v[cd[23]])
		   + 0.140625*v[cd[21]] - 0.234375*v[cd[25]]
		   + 0.046875*(-v[cd[26]] - v[cd[34]]));
      v[pd[6]] += (0.03125*(v[cd[19]] + v[cd[22]] + v[cd[23]])
		   + 0.09375*(-v[cd[21]] + v[cd[26]] + v[cd[34]])
		   + 0.15625*v[cd[25]]);
      v[pd[7]] += (0.125*v[cd[22]] + 0.0625*(v[cd[23]] - v[cd[34]]));
      v[pd[8]] += -0.125*v[cd[22]];
      v[pd[10]] += (0.1875*v[cd[19]]
		    + 0.0625*(-v[cd[21]] + v[cd[22]] - v[cd[34]])
		    + 0.125*(v[cd[23]] - v[cd[26]])
		    + 0.3125*v[cd[25]]);
      v[pd[11]] += (0.375*(-v[cd[19]] + v[cd[26]]) - 0.125*v[cd[23]]);
      v[pd[12]] += 0.5*v[cd[19]];
      v[pd[13]] += (0.125*v[cd[22]] + 0.0625*(v[cd[23]] + v[cd[34]]));
      v[pd[14]] += -0.125*v[cd[22]];
      v[pd[16]] += (0.1875*v[cd[19]]
		    + 0.0625*(-v[cd[21]] + v[cd[22]]
			      + v[cd[25]] + v[cd[34]])
		    + 0.125*(v[cd[23]] + v[cd[26]]));
      v[pd[17]] += (-0.375*v[cd[19]]
		    + 0.125*(-v[cd[23]] - v[cd[26]]));
      v[pd[18]] += 0.5*v[cd[19]];
      v[pd[22]] += (0.25*(-v[cd[22]] - v[cd[23]]) - 0.125*v[cd[34]]);
      v[pd[23]] += (0.5*v[cd[22]]);
      v[pd[24]] += (0.5*v[cd[23]]);
      v[pd[25]] += (0.25*(-v[cd[22]] - v[cd[23]]) + 0.375*v[cd[34]]);
      v[pd[26]] += (0.5*v[cd[22]]);
      v[pd[27]] += (0.5*v[cd[23]]);
      v[pd[28]] = (v[cd[27]] + 0.1875*(-v[cd[19]] + v[cd[34]])
		   + 0.5625*v[cd[21]] - 0.0625*v[cd[22]]
		   - 0.125*v[cd[23]] + 0.9375*v[cd[25]]
		   + 0.375*v[cd[26]]);
      v[pd[29]] = (0.1875*(-v[cd[19]] - v[cd[34]])
		   + 0.5625*v[cd[21]] - 0.0625*v[cd[22]]
		   - 0.125*v[cd[23]] - 0.3125*v[cd[25]]
		   - 0.375*v[cd[26]]);
      v[pd[30]] = (v[cd[20]] + 0.75*(v[cd[19]] + v[cd[26]])
		   + 0.25*v[cd[23]]);
      v[pd[31]] += (-0.125*v[cd[22]] - 0.0625*v[cd[23]]
		    + 0.1875*v[cd[34]]);
      v[pd[32]] += (-0.125*v[cd[22]] - 0.0625*v[cd[23]]
		    - 0.1875*v[cd[34]]);
      v[pd[33]] += 0.25*v[cd[22]];
      v[pd[34]] = (v[cd[24]] + 0.5*(v[cd[22]] + v[cd[23]])
		   + 0.75*v[cd[34]]);
      break;
    case 3:
      v[pd[0]] += (0.0390625*(-v[cd[22]] - v[cd[23]])
		   + 0.0234375*v[cd[34]]);
      v[pd[1]] += (0.0390625*(-v[cd[22]] - v[cd[23]] - v[cd[34]]));
      v[pd[4]] += (0.03125*(v[cd[22]] + v[cd[23]] - v[cd[34]]));
      v[pd[5]] += (0.015625*(v[cd[22]] + v[cd[23]])
		   - 0.046875*v[cd[34]]);
      v[pd[6]] += (0.03125*(v[cd[22]] + v[cd[23]])
		   + 0.09375*v[cd[34]]);
      v[pd[7]] += (0.125*v[cd[22]] + 0.0625*(v[cd[23]] - v[cd[34]]));
      v[pd[8]] += -0.125*v[cd[22]];
      v[pd[10]] += (0.0625*(v[cd[22]] - v[cd[34]]) + 0.125*v[cd[23]]);
      v[pd[11]] += -0.125*v[cd[23]];
      v[pd[13]] += (0.125*v[cd[22]] + 0.0625*(v[cd[23]] + v[cd[34]]));
      v[pd[14]] += -0.125*v[cd[22]];
      v[pd[16]] += (0.0625*(v[cd[22]] + v[cd[34]]) + 0.125*v[cd[23]]);
      v[pd[17]] += -0.125*v[cd[23]];
      v[pd[22]] += (0.25*(-v[cd[22]] - v[cd[23]]) - 0.125*v[cd[34]]);
      v[pd[23]] += 0.5*v[cd[22]];
      v[pd[24]] += 0.5*v[cd[23]];
      v[pd[25]] += (0.25*(-v[cd[22]] - v[cd[23]]) + 0.375*v[cd[34]]);
      v[pd[26]] += 0.5*v[cd[22]];
      v[pd[27]] += 0.5*v[cd[23]];
      v[pd[28]] += (-0.0625*v[cd[22]] - 0.125*v[cd[23]]
		    + 0.1875*v[cd[34]]);
      v[pd[29]] += (-0.0625*v[cd[22]] - 0.125*v[cd[23]]
		    - 0.1875*v[cd[34]]);
      v[pd[30]] += 0.25*v[cd[23]];
      v[pd[31]] += (-0.125*v[cd[22]] - 0.0625*v[cd[23]]
		    + 0.1875*v[cd[34]]);
      v[pd[32]] += (-0.125*v[cd[22]] - 0.0625*v[cd[23]]
		    - 0.1875*v[cd[34]]);
      v[pd[33]] += 0.25*v[cd[22]];
      v[pd[34]] = (v[cd[24]] + 0.5*(v[cd[22]] + v[cd[23]])
		   + 0.75*v[cd[34]]);
      break;
    }

/*--------------------------------------------------------------------------*/
/*  values on child[1]                                                      */
/*--------------------------------------------------------------------------*/

    get_dof_indices4_3d(cd, el->child[1], admin, bas_fcts);

    if (typ == 0)
    {
      switch(lr_set)
      {
      case 1:
	v[pd[0]] += (0.0390625*(-v[cd[25]] - v[cd[26]] - v[cd[34]]));
	v[pd[1]] += (-0.0390625*v[cd[25]]
		     + 0.0234375*(v[cd[26]] + v[cd[34]]));
	v[pd[4]] += (0.15625*v[cd[25]]
		     + 0.09375*(v[cd[26]] + v[cd[34]]));
	v[pd[5]] += (-0.234375*v[cd[25]]
		     + 0.046875*(-v[cd[26]] - v[cd[34]]));
	v[pd[6]] += (0.15625*v[cd[25]]
		     + 0.03125*(-v[cd[26]] - v[cd[34]]));
	v[pd[7]] += (0.0625*(v[cd[25]] + v[cd[34]])
		     + 0.125*v[cd[26]]);
	v[pd[8]] += -0.125*v[cd[26]];
	v[pd[10]] += 0.0625*v[cd[34]];
	v[pd[13]] += (0.3125*v[cd[25]] - 0.125*v[cd[26]]
		      - 0.0625*v[cd[34]]);
	v[pd[14]] += 0.375*v[cd[26]];
	v[pd[16]] += -0.0625*v[cd[34]];
	v[pd[22]] += 0.375*v[cd[34]];
	v[pd[25]] += -0.125*v[cd[34]];
	v[pd[28]] += -0.1875*v[cd[34]];
	v[pd[29]] += 0.1875*v[cd[34]];
	v[pd[31]] += (-0.3125*v[cd[25]] - 0.375*v[cd[26]]
		      - 0.1875*v[cd[34]]);
	v[pd[32]] += (v[cd[27]] + 0.9375*v[cd[25]]
		      + 0.375*v[cd[26]] + 0.1875*v[cd[34]]);
	v[pd[33]] += 0.75*v[cd[26]];
	v[pd[34]] += 0.75*v[cd[34]];
	break;
      case 2:
	v[pd[0]] += (0.0390625*(-v[cd[28]] - v[cd[29]] - v[cd[34]]));
	v[pd[1]] += (-0.0390625*v[cd[28]]
		     + 0.0234375*(v[cd[29]] + v[cd[34]]));
	v[pd[4]] += (0.15625*v[cd[28]]
		     + 0.09375*(v[cd[29]] + v[cd[34]]));
	v[pd[5]] += (-0.234375*v[cd[28]]
		     + 0.046875*(-v[cd[29]] - v[cd[34]]));
	v[pd[6]] += (0.15625*v[cd[28]]
		     + 0.03125*(-v[cd[29]] - v[cd[34]]));
	v[pd[7]] += 0.0625*v[cd[34]];
	v[pd[10]] += (0.0625*(v[cd[28]] + v[cd[34]])
		      + 0.125*v[cd[29]]);
	v[pd[11]] += -0.125*v[cd[29]];
	v[pd[13]] += -0.0625*v[cd[34]];
	v[pd[16]] += (0.3125*v[cd[28]] - 0.125*v[cd[29]]
		      - 0.0625*v[cd[34]]);
	v[pd[17]] += 0.375*v[cd[29]];
	v[pd[22]] += 0.375*v[cd[34]];
	v[pd[25]] += -0.125*v[cd[34]];
	v[pd[28]] += (-0.3125*v[cd[28]] - 0.375*v[cd[29]]
		      - 0.1875*v[cd[34]]);
	v[pd[29]] += (v[cd[30]] + 0.9375*v[cd[28]] + 0.375*v[cd[29]]
		      + 0.1875*v[cd[34]]);
	v[pd[30]] += 0.75*v[cd[29]];
	v[pd[31]] += -0.1875*v[cd[34]];
	v[pd[32]] += 0.1875*v[cd[34]];
	v[pd[34]] += 0.75*v[cd[34]];
	break;
      case 3:
	v[pd[0]] += -0.0390625*v[cd[34]];
	v[pd[1]] += 0.0234375*v[cd[34]];
	v[pd[4]] += 0.09375*v[cd[34]];
	v[pd[5]] += -0.046875*v[cd[34]];
	v[pd[6]] += -0.03125*v[cd[34]];
	v[pd[7]] += 0.0625*v[cd[34]];
	v[pd[10]] += 0.0625*v[cd[34]];
	v[pd[13]] += -0.0625*v[cd[34]];
	v[pd[16]] += -0.0625*v[cd[34]];
	v[pd[22]] += 0.375*v[cd[34]];
	v[pd[25]] += -0.125*v[cd[34]];
	v[pd[28]] += -0.1875*v[cd[34]];
	v[pd[29]] += 0.1875*v[cd[34]];
	v[pd[31]] += -0.1875*v[cd[34]];
	v[pd[32]] += 0.1875*v[cd[34]];
	v[pd[34]] += 0.75*v[cd[34]];
	break;
      }
    }
    else
    {
      switch(lr_set)
      {
      case 1:
	v[pd[0]] += (0.0390625*(-v[cd[28]] - v[cd[29]] - v[cd[34]]));
	v[pd[1]] += (-0.0390625*v[cd[28]]
		     + 0.0234375*(v[cd[29]] + v[cd[34]]));
	v[pd[4]] += (0.15625*v[cd[28]]
		     + 0.09375*(v[cd[29]] + v[cd[34]]));
	v[pd[5]] += (-0.234375*v[cd[28]]
		     + 0.046875*(-v[cd[29]] - v[cd[34]]));
	v[pd[6]] += (0.15625*v[cd[28]]
		     + 0.03125*(-v[cd[29]] - v[cd[34]]));
	v[pd[7]] += (0.0625*(v[cd[28]] + v[cd[34]])
		     + 0.125*v[cd[29]]);
	v[pd[8]] += -0.125*v[cd[29]];
	v[pd[10]] += 0.0625*v[cd[34]];
	v[pd[13]] += (0.3125*v[cd[28]] - 0.125*v[cd[29]]
		      - 0.0625*v[cd[34]]);
	v[pd[14]] += 0.375*v[cd[29]];
	v[pd[16]] += -0.0625*v[cd[34]];
	v[pd[22]] += 0.375*v[cd[34]];
	v[pd[25]] += -0.125*v[cd[34]];
	v[pd[28]] += -0.1875*v[cd[34]];
	v[pd[29]] += 0.1875*v[cd[34]];
	v[pd[31]] += (-0.3125*v[cd[28]] - 0.375*v[cd[29]]
		      - 0.1875*v[cd[34]]);
	v[pd[32]] += (v[cd[30]] + 0.9375*v[cd[28]] + 0.375*v[cd[29]]
		      + 0.1875*v[cd[34]]);
	v[pd[33]] += 0.75*v[cd[29]];
	v[pd[34]] += 0.75*v[cd[34]];
	break;
      case 2:
	v[pd[0]] += (0.0390625*(-v[cd[25]] - v[cd[26]] - v[cd[34]]));
	v[pd[1]] += (-0.0390625*v[cd[25]]
		     + 0.0234375*(v[cd[26]] + v[cd[34]]));
	v[pd[4]] += (0.15625*v[cd[25]]
		     + 0.09375*(v[cd[26]] + v[cd[34]]));
	v[pd[5]] += (-0.234375*v[cd[25]]
		     + 0.046875*(-v[cd[26]] - v[cd[34]]));
	v[pd[6]] += (0.15625*v[cd[25]]
		     + 0.03125*(-v[cd[26]] - v[cd[34]]));
	v[pd[7]] += 0.0625*v[cd[34]];
	v[pd[10]] += (0.0625*(v[cd[25]] + v[cd[34]])
		      + 0.125*v[cd[26]]);
	v[pd[11]] += -0.125*v[cd[26]];
	v[pd[13]] += -0.0625*v[cd[34]];
	v[pd[16]] += (0.3125*v[cd[25]] - 0.125*v[cd[26]]
		      - 0.0625*v[cd[34]]);
	v[pd[17]] += 0.375*v[cd[26]];
	v[pd[22]] += 0.375*v[cd[34]];
	v[pd[25]] += -0.125*v[cd[34]];
	v[pd[28]] += (-0.3125*v[cd[25]] - 0.375*v[cd[26]]
		      - 0.1875*v[cd[34]]);
	v[pd[29]] += (v[cd[27]] + 0.9375*v[cd[25]]
		      + 0.375*v[cd[26]] + 0.1875*v[cd[34]]);
	v[pd[30]] += 0.75*v[cd[26]];
	v[pd[31]] += -0.1875*v[cd[34]];
	v[pd[32]] += 0.1875*v[cd[34]];
	v[pd[34]] += 0.75*v[cd[34]];
	break;
      case 3:
	v[pd[0]] += -0.0390625*v[cd[34]];
	v[pd[1]] += 0.0234375*v[cd[34]];
	v[pd[4]] += 0.09375*v[cd[34]];
	v[pd[5]] += -0.046875*v[cd[34]];
	v[pd[6]] += -0.03125*v[cd[34]];
	v[pd[7]] += 0.0625*v[cd[34]];
	v[pd[10]] += 0.0625*v[cd[34]];
	v[pd[13]] += -0.0625*v[cd[34]];
	v[pd[16]] += -0.0625*v[cd[34]];
	v[pd[22]] += 0.375*v[cd[34]];
	v[pd[25]] += -0.125*v[cd[34]];
	v[pd[28]] += -0.1875*v[cd[34]];
	v[pd[29]] += 0.1875*v[cd[34]];
	v[pd[31]] += -0.1875*v[cd[34]];
	v[pd[32]] += 0.1875*v[cd[34]];
	v[pd[34]] += 0.75*v[cd[34]];
	break;
      }
    }
  }

  return;
}


static void real_d_refine_inter4_3d(DOF_REAL_D_VEC *drdv, RC_LIST_EL *list, int n)
{
  FUNCNAME("real_d_refine_inter4_3d");
  DOF pd[N_BAS_LAG_4_3D];
  DOF cd[N_BAS_LAG_4_3D];
  EL              *el;
  REAL_D          *v = NULL;
  int             i, k, lr_set;
  U_CHAR          typ;
  const DOF_ADMIN *admin;
  const BAS_FCTS  *bas_fcts;

  if (n < 1) return;
  el = list->el_info.el;
  typ = list->el_info.el_type;

  GET_DOF_VEC(v, drdv);
  if (!drdv->fe_space)
  {
    ERROR("no fe_space in dof_real_d_vec %s\n", NAME(drdv));
    return;
  }
  else if (!drdv->fe_space->bas_fcts)
  {
    ERROR("no basis functions in fe_space %s\n", NAME(drdv->fe_space));
    return;
  }
  GET_STRUCT(admin, drdv->fe_space);
  GET_STRUCT(bas_fcts, drdv->fe_space);

  get_dof_indices4_3d(pd, el, admin, bas_fcts);

/*--------------------------------------------------------------------------*/
/*  values on child[0]                                                      */
/*--------------------------------------------------------------------------*/

  get_dof_indices4_3d(cd, el->child[0], admin, bas_fcts);

  for (k = 0; k < DIM_OF_WORLD; k++)
  {
    v[cd[3]][k] = (v[pd[5]][k]);
    v[cd[10]][k] = (0.2734375*v[pd[0]][k] - 0.0390625*v[pd[1]][k]
		    + 1.09375*v[pd[4]][k] - 0.546875*v[pd[5]][k]
		    + 0.21875*v[pd[6]][k]);
    v[cd[11]][k] = (v[pd[4]][k]);
    v[cd[12]][k] = (-0.0390625*v[pd[0]][k] + 0.0234375*v[pd[1]][k]
		    + 0.46875*v[pd[4]][k] + 0.703125*v[pd[5]][k]
		    - 0.15625*v[pd[6]][k]);
    v[cd[16]][k] = (0.0390625*(-v[pd[0]][k] - v[pd[1]][k])
		    + 0.03125*(v[pd[4]][k] + v[pd[6]][k])
		    + 0.015625*v[pd[5]][k] +
		    0.1875*(v[pd[7]][k]+v[pd[13]][k]
			    -v[pd[31]][k]-v[pd[32]][k])
		    + 0.375*(-v[pd[8]][k] - v[pd[14]][k])
		    + 0.5*(v[pd[9]][k] + v[pd[15]][k]) + 0.75*v[pd[33]][k]);
    v[cd[17]][k] = (v[pd[33]][k]);
    v[cd[18]][k] = (0.0234375*(v[pd[0]][k] + v[pd[1]][k])
		    + 0.09375*(-v[pd[4]][k] - v[pd[6]][k])
		    + 0.140625*v[pd[5]][k]
		    + 0.0625*(-v[pd[7]][k] - v[pd[13]][k])
		    + 0.5625*(v[pd[31]][k] + v[pd[32]][k]));
    v[cd[19]][k] = (0.0390625*(-v[pd[0]][k] - v[pd[1]][k])
		    + 0.03125*(v[pd[4]][k] + v[pd[6]][k])
		    + 0.015625*v[pd[5]][k]
		    + 0.1875*(v[pd[10]][k]+v[pd[16]][k]
			      -v[pd[28]][k]-v[pd[29]][k])
		    + 0.375*(-v[pd[11]][k] - v[pd[17]][k])
		    + 0.5*(v[pd[12]][k] + v[pd[18]][k]) + 0.75*v[pd[30]][k]);
    v[cd[20]][k] = (v[pd[30]][k]);
    v[cd[21]][k] = (0.0234375*(v[pd[0]][k] + v[pd[1]][k])
		    + 0.09375*(-v[pd[4]][k] - v[pd[6]][k])
		    + 0.140625*v[pd[5]][k]
		    + 0.0625*(-v[pd[10]][k] - v[pd[16]][k])
		    + 0.5625*(v[pd[28]][k] + v[pd[29]][k]));
    v[cd[22]][k] = (0.0390625*(-v[pd[0]][k] - v[pd[1]][k])
		    + 0.03125*(v[pd[4]][k] + v[pd[6]][k])
		    + 0.015625*v[pd[5]][k]
		    + 0.125*(v[pd[7]][k]-v[pd[8]][k]+v[pd[13]][k]-v[pd[14]][k]
			     -v[pd[31]][k]-v[pd[32]][k])
		    + 0.0625*(v[pd[10]][k]+v[pd[16]][k]
			      -v[pd[28]][k]-v[pd[29]][k])
		    + 0.25*(-v[pd[22]][k] - v[pd[25]][k] + v[pd[33]][k])
		    + 0.5*(v[pd[23]][k] + v[pd[26]][k] + v[pd[34]][k]));
    v[cd[23]][k] = (0.0390625*(-v[pd[0]][k] - v[pd[1]][k])
		    + 0.03125*(v[pd[4]][k] + v[pd[6]][k])
		    + 0.015625*v[pd[5]][k]
		    + 0.0625*(v[pd[7]][k]+v[pd[13]][k]
			      -v[pd[31]][k]-v[pd[32]][k])
		    + 0.125*(v[pd[10]][k] - v[pd[11]][k] + v[pd[16]][k]
			     - v[pd[17]][k] - v[pd[28]][k] - v[pd[29]][k])
		    + 0.25*(-v[pd[22]][k] - v[pd[25]][k] + v[pd[30]][k])
		    + 0.5*(v[pd[24]][k] + v[pd[27]][k] + v[pd[34]][k]));
    v[cd[24]][k] = (v[pd[34]][k]);
    v[cd[25]][k] = (0.0390625*(-v[pd[0]][k] - v[pd[1]][k])
		    + 0.15625*(v[pd[4]][k] + v[pd[6]][k])
		    - 0.234375*v[pd[5]][k]
		    + 0.3125*(v[pd[10]][k] - v[pd[29]][k])
		    + 0.0625*v[pd[16]][k] + 0.9375*v[pd[28]][k]);
    v[cd[26]][k] = (0.0234375*v[pd[0]][k] - 0.0390625*v[pd[1]][k]
		    - 0.03125*v[pd[4]][k] - 0.046875*v[pd[5]][k]
		    + 0.09375*v[pd[6]][k]
		    + 0.125*(-v[pd[10]][k] + v[pd[16]][k] - v[pd[17]][k])
		    + 0.375*(v[pd[11]][k] + v[pd[28]][k] - v[pd[29]][k])
		    + 0.75*v[pd[30]][k]);
    v[cd[27]][k] = (v[pd[28]][k]);
    v[cd[28]][k] = (0.0390625*(-v[pd[0]][k] - v[pd[1]][k])
		    + 0.15625*(v[pd[4]][k] + v[pd[6]][k])
		    - 0.234375*v[pd[5]][k]
		    + 0.3125*(v[pd[7]][k] - v[pd[32]][k])
		    + 0.0625*v[pd[13]][k] + 0.9375*v[pd[31]][k]);
    v[cd[29]][k] = (0.0234375*v[pd[0]][k] - 0.0390625*v[pd[1]][k]
		    - 0.03125*v[pd[4]][k] - 0.046875*v[pd[5]][k]
		    + 0.09375*v[pd[6]][k]
		    + 0.125*(-v[pd[7]][k] + v[pd[13]][k] - v[pd[14]][k])
		    + 0.375*(v[pd[8]][k] + v[pd[31]][k] - v[pd[32]][k])
		    + 0.75*v[pd[33]][k]);
    v[cd[30]][k] = (v[pd[31]][k]);
    v[cd[34]][k] = (0.0234375*v[pd[0]][k] - 0.0390625*v[pd[1]][k]
		    - 0.03125*v[pd[4]][k] - 0.046875*v[pd[5]][k]
		    + 0.09375*v[pd[6]][k]
		    + 0.0625*(-v[pd[7]][k] - v[pd[10]][k]
			      + v[pd[13]][k] + v[pd[16]][k])
		    - 0.125*v[pd[22]][k] + 0.375*v[pd[25]][k]
		    + 0.1875*(v[pd[28]][k] - v[pd[29]][k]
			      + v[pd[31]][k] - v[pd[32]][k])
		    + 0.75*v[pd[34]][k]);
  }
/*--------------------------------------------------------------------------*/
/*  values on child[1]                                                      */
/*--------------------------------------------------------------------------*/

  get_dof_indices4_3d(cd, el->child[1], admin, bas_fcts);
  if (typ == 0)
  {
/*--------------------------------------------------------------------------*/
/*  parent of el_type 0                                                     */
/*--------------------------------------------------------------------------*/

    for (k = 0; k < DIM_OF_WORLD; k++)
    {
      v[cd[10]][k] = (-0.0390625*v[pd[0]][k] + 0.2734375*v[pd[1]][k]
		      + 0.21875*v[pd[4]][k] - 0.546875*v[pd[5]][k]
		      + 1.09375*v[pd[6]][k]);
      v[cd[11]][k] = v[pd[6]][k];
      v[cd[12]][k] = (0.0234375*v[pd[0]][k] - 0.0390625*v[pd[1]][k]
		      - 0.15625*v[pd[4]][k] + 0.703125*v[pd[5]][k]
		      + 0.46875*v[pd[6]][k]);
      v[cd[25]][k] = (0.0390625*(-v[pd[0]][k] - v[pd[1]][k])
		      + 0.15625*(v[pd[4]][k] + v[pd[6]][k])
		      - 0.234375*v[pd[5]][k] + 0.0625*v[pd[7]][k]
		      + 0.3125*(v[pd[13]][k] - v[pd[31]][k])
		      + 0.9375*v[pd[32]][k]);
      v[cd[26]][k] = (-0.0390625*v[pd[0]][k] + 0.0234375*v[pd[1]][k]
		      + 0.09375*v[pd[4]][k] - 0.046875*v[pd[5]][k]
		      - 0.03125*v[pd[6]][k]
		      + 0.125*(v[pd[7]][k] - v[pd[8]][k] - v[pd[13]][k])
		      + 0.375*(v[pd[14]][k] - v[pd[31]][k] + v[pd[32]][k])
		      + 0.75*v[pd[33]][k]);
      v[cd[27]][k] = v[pd[32]][k];
      v[cd[28]][k] = (0.0390625*(-v[pd[0]][k] - v[pd[1]][k])
		      + 0.15625*(v[pd[4]][k] + v[pd[6]][k])
		      - 0.234375*v[pd[5]][k] + 0.0625*v[pd[10]][k]
		      + 0.3125*(v[pd[16]][k] - v[pd[28]][k])
		      + 0.9375*v[pd[29]][k]);
      v[cd[29]][k] = (-0.0390625*v[pd[0]][k] + 0.0234375*v[pd[1]][k]
		      + 0.09375*v[pd[4]][k] - 0.046875*v[pd[5]][k]
		      - 0.03125*v[pd[6]][k]
		      + 0.125*(v[pd[10]][k] - v[pd[11]][k] - v[pd[16]][k])
		      + 0.375*(v[pd[17]][k] - v[pd[28]][k] + v[pd[29]][k])
		      + 0.75*v[pd[30]][k]);
      v[cd[30]][k] = v[pd[29]][k];
      v[cd[34]][k] = (-0.0390625*v[pd[0]][k] + 0.0234375*v[pd[1]][k]
		      + 0.09375*v[pd[4]][k] - 0.046875*v[pd[5]][k]
		      - 0.03125*v[pd[6]][k]
		      + 0.0625*(v[pd[7]][k] + v[pd[10]][k]
				-v[pd[13]][k] - v[pd[16]][k])
		      + 0.375*v[pd[22]][k] - 0.125*v[pd[25]][k]
		      + 0.1875*(-v[pd[28]][k] + v[pd[29]][k]
				-v[pd[31]][k] + v[pd[32]][k])
		      + 0.75*v[pd[34]][k]);
    }
  }
  else
  {
/*--------------------------------------------------------------------------*/
/*  parent of el_type 1|2                                                   */
/*--------------------------------------------------------------------------*/

    for (k = 0; k < DIM_OF_WORLD; k++)
    {
      v[cd[10]][k] = (-0.0390625*v[pd[0]][k] + 0.2734375*v[pd[1]][k]
		      + 0.21875*v[pd[4]][k] - 0.546875*v[pd[5]][k]
		      + 1.09375*v[pd[6]][k]);
      v[cd[11]][k] = v[pd[6]][k];
      v[cd[12]][k] = (0.0234375*v[pd[0]][k] - 0.0390625*v[pd[1]][k]
		      - 0.15625*v[pd[4]][k] + 0.703125*v[pd[5]][k]
		      + 0.46875*v[pd[6]][k]);
      v[cd[25]][k] = (0.0390625*(-v[pd[0]][k] - v[pd[1]][k])
		      + 0.15625*(v[pd[4]][k] + v[pd[6]][k])
		      - 0.234375*v[pd[5]][k] + 0.0625*v[pd[10]][k]
		      + 0.3125*(v[pd[16]][k] - v[pd[28]][k])
		      + 0.9375*v[pd[29]][k]);
      v[cd[26]][k] = (-0.0390625*v[pd[0]][k] + 0.0234375*v[pd[1]][k]
		      + 0.09375*v[pd[4]][k] - 0.046875*v[pd[5]][k]
		      - 0.03125*v[pd[6]][k]
		      + 0.125*(v[pd[10]][k] - v[pd[11]][k] - v[pd[16]][k])
		      + 0.375*(v[pd[17]][k] - v[pd[28]][k] + v[pd[29]][k])
		      + 0.75*v[pd[30]][k]);
      v[cd[27]][k] = v[pd[29]][k];
      v[cd[28]][k] = (0.0390625*(-v[pd[0]][k] - v[pd[1]][k])
		      + 0.15625*(v[pd[4]][k] + v[pd[6]][k])
		      - 0.234375*v[pd[5]][k] + 0.0625*v[pd[7]][k]
		      + 0.3125*(v[pd[13]][k] - v[pd[31]][k])
		      + 0.9375*v[pd[32]][k]);
      v[cd[29]][k] = (-0.0390625*v[pd[0]][k] + 0.0234375*v[pd[1]][k]
		      + 0.09375*v[pd[4]][k] - 0.046875*v[pd[5]][k]
		      - 0.03125*v[pd[6]][k]
		      + 0.125*(v[pd[7]][k] - v[pd[8]][k] - v[pd[13]][k])
		      + 0.375*(v[pd[14]][k] - v[pd[31]][k] + v[pd[32]][k])
		      + 0.75*v[pd[33]][k]);
      v[cd[30]][k] = v[pd[32]][k];
      v[cd[34]][k] = (-0.0390625*v[pd[0]][k] + 0.0234375*v[pd[1]][k]
		      + 0.09375*v[pd[4]][k] - 0.046875*v[pd[5]][k]
		      - 0.03125*v[pd[6]][k]
		      + 0.0625*(v[pd[7]][k] + v[pd[10]][k]
				- v[pd[13]][k] - v[pd[16]][k])
		      + 0.375*v[pd[22]][k] - 0.125*v[pd[25]][k]
		      + 0.1875*(-v[pd[28]][k] + v[pd[29]][k]
				- v[pd[31]][k] + v[pd[32]][k])
		      + 0.75*v[pd[34]][k]);
    }
  }

/*--------------------------------------------------------------------------*/
/*   adjust neighbour values                                                */
/*--------------------------------------------------------------------------*/

  for (i = 1; i < n; i++)
  {
    el = list[i].el_info.el;
    typ = list[i].el_info.el_type;

    get_dof_indices4_3d(pd, el, admin, bas_fcts);

    lr_set = 0;
    if (list[i].neigh[0]  &&  list[i].neigh[0]->no < i)
      lr_set = 1;

    if (list[i].neigh[1]  &&  list[i].neigh[1]->no < i)
      lr_set += 2;

    DEBUG_TEST_EXIT(lr_set, "no values set on both neighbours\n");

/*--------------------------------------------------------------------------*/
/*  values on child[0]                                                      */
/*--------------------------------------------------------------------------*/

    get_dof_indices4_3d(cd, el->child[0], admin, bas_fcts);

    switch(lr_set)
    {
    case 1:
      for (k = 0; k < DIM_OF_WORLD; k++)
      {
	v[cd[16]][k] = (0.0390625*(-v[pd[0]][k] - v[pd[1]][k])
			+ 0.03125*(v[pd[4]][k] + v[pd[6]][k])
			+ 0.015625*v[pd[5]][k]
			+ 0.1875*(v[pd[7]][k] + v[pd[13]][k]
				  - v[pd[31]][k] - v[pd[32]][k])
			+ 0.375*(-v[pd[8]][k] - v[pd[14]][k])
			+ 0.5*(v[pd[9]][k] + v[pd[15]][k])
			+ 0.75*v[pd[33]][k]);
	v[cd[17]][k] = v[pd[33]][k];
	v[cd[18]][k] = (0.0234375*(v[pd[0]][k] + v[pd[1]][k])
			+ 0.09375*(-v[pd[4]][k] - v[pd[6]][k])
			+ 0.140625*v[pd[5]][k]
			+ 0.0625*(-v[pd[7]][k] - v[pd[13]][k])
			+ 0.5625*(v[pd[31]][k] + v[pd[32]][k]));
	v[cd[22]][k] = (0.0390625*(-v[pd[0]][k] - v[pd[1]][k])
			+ 0.03125*(v[pd[4]][k] + v[pd[6]][k])
			+ 0.015625*v[pd[5]][k]
			+ 0.125*(v[pd[7]][k] - v[pd[8]][k] + v[pd[13]][k]
				 - v[pd[14]][k] - v[pd[31]][k] - v[pd[32]][k])
			+ 0.0625*(v[pd[10]][k] + v[pd[16]][k]
				  - v[pd[28]][k] - v[pd[29]][k])
			+ 0.25*(-v[pd[22]][k] - v[pd[25]][k] + v[pd[33]][k])
			+ 0.5*(v[pd[23]][k] + v[pd[26]][k] + v[pd[34]][k]));
	v[cd[23]][k] = (0.0390625*(-v[pd[0]][k] - v[pd[1]][k])
			+ 0.03125*(v[pd[4]][k] + v[pd[6]][k])
			+ 0.015625*v[pd[5]][k]
			+ 0.0625*(v[pd[7]][k] + v[pd[13]][k]
				  - v[pd[31]][k] - v[pd[32]][k])
			+ 0.125*(v[pd[10]][k] - v[pd[11]][k] + v[pd[16]][k]
				 - v[pd[17]][k] - v[pd[28]][k] - v[pd[29]][k])
			+ 0.25*(-v[pd[22]][k] - v[pd[25]][k] + v[pd[30]][k])
			+ 0.5*(v[pd[24]][k] + v[pd[27]][k] + v[pd[34]][k]));
	v[cd[24]][k] = v[pd[34]][k];
	v[cd[28]][k] = (0.0390625*(-v[pd[0]][k] - v[pd[1]][k])
			+ 0.15625*(v[pd[4]][k] + v[pd[6]][k])
			- 0.234375*v[pd[5]][k]
			+ 0.3125*(v[pd[7]][k] - v[pd[32]][k])
			+ 0.0625*v[pd[13]][k] + 0.9375*v[pd[31]][k]);
	v[cd[29]][k] = (0.0234375*v[pd[0]][k] - 0.0390625*v[pd[1]][k]
			- 0.03125*v[pd[4]][k] - 0.046875*v[pd[5]][k]
			+ 0.09375*v[pd[6]][k]
			+ 0.125*(-v[pd[7]][k] + v[pd[13]][k] - v[pd[14]][k])
			+ 0.375*(v[pd[8]][k] + v[pd[31]][k] - v[pd[32]][k])
			+ 0.75*v[pd[33]][k]);
	v[cd[30]][k] = v[pd[31]][k];
	v[cd[34]][k] = (0.0234375*v[pd[0]][k] - 0.0390625*v[pd[1]][k]
			- 0.03125*v[pd[4]][k] - 0.046875*v[pd[5]][k]
			+ 0.09375*v[pd[6]][k]
			+ 0.0625*(-v[pd[7]][k] - v[pd[10]][k]
				  + v[pd[13]][k] + v[pd[16]][k])
			- 0.125*v[pd[22]][k] + 0.375*v[pd[25]][k]
			+ 0.1875*(v[pd[28]][k] - v[pd[29]][k]
				  + v[pd[31]][k] - v[pd[32]][k])
			+ 0.75*v[pd[34]][k]);
      }
      break;
    case 2:
      for (k = 0; k < DIM_OF_WORLD; k++)
      {
      v[cd[19]][k] = (0.0390625*(-v[pd[0]][k] - v[pd[1]][k])
		     + 0.03125*(v[pd[4]][k] + v[pd[6]][k])
		     + 0.015625*v[pd[5]][k]
		     + 0.1875*(v[pd[10]][k] + v[pd[16]][k]
			       - v[pd[28]][k] - v[pd[29]][k])
		     + 0.375*(-v[pd[11]][k] - v[pd[17]][k])
		     + 0.5*(v[pd[12]][k] + v[pd[18]][k])
		     + 0.75*v[pd[30]][k]);
      v[cd[20]][k] = v[pd[30]][k];
      v[cd[21]][k] = (0.0234375*(v[pd[0]][k] + v[pd[1]][k])
		     + 0.09375*(-v[pd[4]][k] - v[pd[6]][k])
		     + 0.140625*v[pd[5]][k]
		     + 0.0625*(-v[pd[10]][k] - v[pd[16]][k])
		     + 0.5625*(v[pd[28]][k] + v[pd[29]][k]));
      v[cd[22]][k] = (0.0390625*(-v[pd[0]][k] - v[pd[1]][k])
		     + 0.03125*(v[pd[4]][k] + v[pd[6]][k])
		     + 0.015625*v[pd[5]][k]
		     + 0.125*(v[pd[7]][k] - v[pd[8]][k] + v[pd[13]][k]
			      - v[pd[14]][k] - v[pd[31]][k] - v[pd[32]][k])
		     + 0.0625*(v[pd[10]][k] + v[pd[16]][k]
			       - v[pd[28]][k] - v[pd[29]][k])
		     + 0.25*(-v[pd[22]][k] - v[pd[25]][k] + v[pd[33]][k])
		     + 0.5*(v[pd[23]][k] + v[pd[26]][k] + v[pd[34]][k]));
      v[cd[23]][k] = (0.0390625*(-v[pd[0]][k] - v[pd[1]][k])
		     + 0.03125*(v[pd[4]][k] + v[pd[6]][k])
		     + 0.015625*v[pd[5]][k]
		     + 0.0625*(v[pd[7]][k] + v[pd[13]][k]
			       - v[pd[31]][k] - v[pd[32]][k])
		     + 0.125*(v[pd[10]][k] - v[pd[11]][k] + v[pd[16]][k]
			      - v[pd[17]][k] - v[pd[28]][k] - v[pd[29]][k])
		     + 0.25*(-v[pd[22]][k] - v[pd[25]][k] + v[pd[30]][k])
		     + 0.5*(v[pd[24]][k] + v[pd[27]][k] + v[pd[34]][k]));
      v[cd[24]][k] = v[pd[34]][k];
      v[cd[25]][k] = (0.0390625*(-v[pd[0]][k] - v[pd[1]][k])
		     + 0.15625*(v[pd[4]][k] + v[pd[6]][k])
		     - 0.234375*v[pd[5]][k]
		     + 0.3125*(v[pd[10]][k] - v[pd[29]][k])
		     + 0.0625*v[pd[16]][k] + 0.9375*v[pd[28]][k]);
      v[cd[26]][k] = (0.0234375*v[pd[0]][k] - 0.0390625*v[pd[1]][k]
		     - 0.03125*v[pd[4]][k] - 0.046875*v[pd[5]][k]
		     + 0.09375*v[pd[6]][k]
		     + 0.125*(-v[pd[10]][k] + v[pd[16]][k] - v[pd[17]][k])
		     + 0.375*(v[pd[11]][k] + v[pd[28]][k] - v[pd[29]][k])
		      + 0.75*v[pd[30]][k]);
      v[cd[27]][k] = v[pd[28]][k];
      v[cd[34]][k] = (0.0234375*v[pd[0]][k] - 0.0390625*v[pd[1]][k]
		      - 0.03125*v[pd[4]][k] - 0.046875*v[pd[5]][k]
		      + 0.09375*v[pd[6]][k]
		      + 0.0625*(-v[pd[7]][k] - v[pd[10]][k]
				+ v[pd[13]][k] + v[pd[16]][k])
		      - 0.125*v[pd[22]][k] + 0.375*v[pd[25]][k]
		      + 0.1875*(v[pd[28]][k] - v[pd[29]][k]
				+ v[pd[31]][k] - v[pd[32]][k])
		      + 0.75*v[pd[34]][k]);
      }
      break;
    case 3:
      for (k = 0; k < DIM_OF_WORLD; k++)
      {
	v[cd[22]][k] = (0.0390625*(-v[pd[0]][k] - v[pd[1]][k])
			+ 0.03125*(v[pd[4]][k] + v[pd[6]][k])
			+ 0.015625*v[pd[5]][k]
			+ 0.125*(v[pd[7]][k] - v[pd[8]][k] + v[pd[13]][k]
				 - v[pd[14]][k] - v[pd[31]][k] - v[pd[32]][k])
			+ 0.0625*(v[pd[10]][k] + v[pd[16]][k]
				  - v[pd[28]][k] - v[pd[29]][k])
			+ 0.25*(-v[pd[22]][k] - v[pd[25]][k] + v[pd[33]][k])
			+ 0.5*(v[pd[23]][k] + v[pd[26]][k] + v[pd[34]][k]));
	v[cd[23]][k] = (0.0390625*(-v[pd[0]][k] - v[pd[1]][k])
			+ 0.03125*(v[pd[4]][k] + v[pd[6]][k])
			+ 0.015625*v[pd[5]][k]
			+ 0.0625*(v[pd[7]][k] + v[pd[13]][k]
				  - v[pd[31]][k] - v[pd[32]][k])
			+ 0.125*(v[pd[10]][k] - v[pd[11]][k] + v[pd[16]][k]
				 - v[pd[17]][k] - v[pd[28]][k] - v[pd[29]][k])
			+ 0.25*(-v[pd[22]][k] - v[pd[25]][k] + v[pd[30]][k])
			+ 0.5*(v[pd[24]][k] + v[pd[27]][k] + v[pd[34]][k]));
	v[cd[24]][k] = v[pd[34]][k];
	v[cd[34]][k] = (0.0234375*v[pd[0]][k] - 0.0390625*v[pd[1]][k]
			- 0.03125*v[pd[4]][k] - 0.046875*v[pd[5]][k]
			+ 0.09375*v[pd[6]][k]
			+ 0.0625*(-v[pd[7]][k] - v[pd[10]][k]
				  + v[pd[13]][k] + v[pd[16]][k])
			- 0.125*v[pd[22]][k] + 0.375*v[pd[25]][k]
			+ 0.1875*(v[pd[28]][k] - v[pd[29]][k]
				  + v[pd[31]][k] - v[pd[32]][k])
			+ 0.75*v[pd[34]][k]);
      }
      break;
    }

/*--------------------------------------------------------------------------*/
/*  values on child[1]                                                      */
/*--------------------------------------------------------------------------*/

    get_dof_indices4_3d(cd, el->child[1], admin, bas_fcts);

    if (typ == 0)
    {
      switch(lr_set)
      {
      case 1:
	for (k = 0; k < DIM_OF_WORLD; k++)
	{
	  v[cd[25]][k] = (0.0390625*(-v[pd[0]][k] - v[pd[1]][k])
			  + 0.15625*(v[pd[4]][k] + v[pd[6]][k])
			  - 0.234375*v[pd[5]][k] + 0.0625*v[pd[7]][k]
			  + 0.3125*(v[pd[13]][k] - v[pd[31]][k])
			  + 0.9375*v[pd[32]][k]);
	  v[cd[26]][k] = (-0.0390625*v[pd[0]][k] + 0.0234375*v[pd[1]][k]
			  + 0.09375*v[pd[4]][k] - 0.046875*v[pd[5]][k]
			  - 0.03125*v[pd[6]][k]
			  + 0.125*(v[pd[7]][k] - v[pd[8]][k] - v[pd[13]][k])
			  + 0.375*(v[pd[14]][k] - v[pd[31]][k] + v[pd[32]][k])
			  + 0.75*v[pd[33]][k]);
	  v[cd[27]][k] = v[pd[32]][k];
	  v[cd[34]][k] = (-0.0390625*v[pd[0]][k] + 0.0234375*v[pd[1]][k]
			  + 0.09375*v[pd[4]][k] - 0.046875*v[pd[5]][k]
			  - 0.03125*v[pd[6]][k]
			  + 0.0625*(v[pd[7]][k] + v[pd[10]][k]
				    - v[pd[13]][k] - v[pd[16]][k])
			  + 0.375*v[pd[22]][k] - 0.125*v[pd[25]][k]
			  + 0.1875*(-v[pd[28]][k] + v[pd[29]][k]
				    - v[pd[31]][k] + v[pd[32]][k])
			  + 0.75*v[pd[34]][k]);
	}
	break;
      case 2:
	for (k = 0; k < DIM_OF_WORLD; k++)
	{
	  v[cd[28]][k] = (0.0390625*(-v[pd[0]][k] - v[pd[1]][k])
			  + 0.15625*(v[pd[4]][k] + v[pd[6]][k])
			  - 0.234375*v[pd[5]][k] + 0.0625*v[pd[10]][k]
			  + 0.3125*(v[pd[16]][k] - v[pd[28]][k])
			  + 0.9375*v[pd[29]][k]);
	  v[cd[29]][k] = (-0.0390625*v[pd[0]][k] + 0.0234375*v[pd[1]][k]
			  + 0.09375*v[pd[4]][k] - 0.046875*v[pd[5]][k]
			  - 0.03125*v[pd[6]][k]
			  + 0.125*(v[pd[10]][k] - v[pd[11]][k] - v[pd[16]][k])
			  + 0.375*(v[pd[17]][k] - v[pd[28]][k] + v[pd[29]][k])
			  + 0.75*v[pd[30]][k]);
	  v[cd[30]][k] = v[pd[29]][k];
	  v[cd[34]][k] = (-0.0390625*v[pd[0]][k] + 0.0234375*v[pd[1]][k]
			  + 0.09375*v[pd[4]][k] - 0.046875*v[pd[5]][k]
			  - 0.03125*v[pd[6]][k]
			  + 0.0625*(v[pd[7]][k] + v[pd[10]][k]
				    - v[pd[13]][k] - v[pd[16]][k])
			  + 0.375*v[pd[22]][k] - 0.125*v[pd[25]][k]
			  + 0.1875*(-v[pd[28]][k] + v[pd[29]][k]
				    - v[pd[31]][k] + v[pd[32]][k])
			  + 0.75*v[pd[34]][k]);
	}
	break;
      case 3:
	for (k = 0; k < DIM_OF_WORLD; k++)
	{
	  v[cd[34]][k] = (-0.0390625*v[pd[0]][k] + 0.0234375*v[pd[1]][k]
			  + 0.09375*v[pd[4]][k] - 0.046875*v[pd[5]][k]
			  - 0.03125*v[pd[6]][k]
			  + 0.0625*(v[pd[7]][k] + v[pd[10]][k]
				    - v[pd[13]][k] - v[pd[16]][k])
			  + 0.375*v[pd[22]][k] - 0.125*v[pd[25]][k]
			  + 0.1875*(-v[pd[28]][k] + v[pd[29]][k]
				    - v[pd[31]][k] + v[pd[32]][k])
			  + 0.75*v[pd[34]][k]);
	}
	break;
      }
    }
    else
    {
      switch(lr_set)
      {
      case 1:
	for (k = 0; k < DIM_OF_WORLD; k++)
	{
	  v[cd[28]][k] = (0.0390625*(-v[pd[0]][k] - v[pd[1]][k])
			  + 0.15625*(v[pd[4]][k] + v[pd[6]][k])
			  - 0.234375*v[pd[5]][k] + 0.0625*v[pd[7]][k]
			  + 0.3125*(v[pd[13]][k] - v[pd[31]][k])
			  + 0.9375*v[pd[32]][k]);
	  v[cd[29]][k] = (-0.0390625*v[pd[0]][k] + 0.0234375*v[pd[1]][k]
			  + 0.09375*v[pd[4]][k] - 0.046875*v[pd[5]][k]
			  - 0.03125*v[pd[6]][k]
			  + 0.125*(v[pd[7]][k] - v[pd[8]][k] - v[pd[13]][k])
			  + 0.375*(v[pd[14]][k] - v[pd[31]][k] + v[pd[32]][k])
			  + 0.75*v[pd[33]][k]);
	  v[cd[30]][k] = v[pd[32]][k];
	  v[cd[34]][k] = (-0.0390625*v[pd[0]][k] + 0.0234375*v[pd[1]][k]
			  + 0.09375*v[pd[4]][k] - 0.046875*v[pd[5]][k]
			  - 0.03125*v[pd[6]][k]
			  + 0.0625*(v[pd[7]][k] + v[pd[10]][k]
				    - v[pd[13]][k] - v[pd[16]][k])
			  + 0.375*v[pd[22]][k] - 0.125*v[pd[25]][k]
			  + 0.1875*(-v[pd[28]][k] + v[pd[29]][k]
				    - v[pd[31]][k] + v[pd[32]][k])
			  + 0.75*v[pd[34]][k]);
	}
	break;
      case 2:
	for (k = 0; k < DIM_OF_WORLD; k++)
	{
	  v[cd[25]][k] = (0.0390625*(-v[pd[0]][k] - v[pd[1]][k])
			  + 0.15625*(v[pd[4]][k] + v[pd[6]][k])
			  - 0.234375*v[pd[5]][k] + 0.0625*v[pd[10]][k]
			  + 0.3125*(v[pd[16]][k] - v[pd[28]][k])
			  + 0.9375*v[pd[29]][k]);
	  v[cd[26]][k] = (-0.0390625*v[pd[0]][k] + 0.0234375*v[pd[1]][k]
			  + 0.09375*v[pd[4]][k] - 0.046875*v[pd[5]][k]
			  - 0.03125*v[pd[6]][k]
			  + 0.125*(v[pd[10]][k] - v[pd[11]][k] - v[pd[16]][k])
			  + 0.375*(v[pd[17]][k] - v[pd[28]][k] + v[pd[29]][k])
			  + 0.75*v[pd[30]][k]);
	  v[cd[27]][k] = v[pd[29]][k];
	  v[cd[34]][k] = (-0.0390625*v[pd[0]][k] + 0.0234375*v[pd[1]][k]
			  + 0.09375*v[pd[4]][k] - 0.046875*v[pd[5]][k]
			  - 0.03125*v[pd[6]][k]
			  + 0.0625*(v[pd[7]][k] + v[pd[10]][k]
				    - v[pd[13]][k] - v[pd[16]][k])
			  + 0.375*v[pd[22]][k] - 0.125*v[pd[25]][k]
			  + 0.1875*(-v[pd[28]][k] + v[pd[29]][k]
				    - v[pd[31]][k] + v[pd[32]][k])
			  + 0.75*v[pd[34]][k]);
	}
	break;
      case 3:
	for (k = 0; k < DIM_OF_WORLD; k++)
	{
	  v[cd[34]][k] = (-0.0390625*v[pd[0]][k] + 0.0234375*v[pd[1]][k]
			  + 0.09375*v[pd[4]][k] - 0.046875*v[pd[5]][k]
			  - 0.03125*v[pd[6]][k]
			  + 0.0625*(v[pd[7]][k] + v[pd[10]][k]
				    - v[pd[13]][k] - v[pd[16]][k])
			  + 0.375*v[pd[22]][k] - 0.125*v[pd[25]][k]
			  + 0.1875*(-v[pd[28]][k] + v[pd[29]][k]
				    - v[pd[31]][k] + v[pd[32]][k])
			  + 0.75*v[pd[34]][k]);
	}
	break;
      }
    }
  }

  return;
}

static void real_d_coarse_inter4_3d(DOF_REAL_D_VEC *drdv, RC_LIST_EL *list,
				    int n)
{
  FUNCNAME("real_d_coarse_inter4_3d");
  DOF pd[N_BAS_LAG_4_3D];
  DOF cd[N_BAS_LAG_4_3D];
  EL              *el;
  REAL_D          *v = NULL;
  int             i, k, lr_set;
  U_CHAR          typ;
  const DOF_ADMIN *admin;
  const BAS_FCTS  *bas_fcts;

  if (n < 1) return;
  el = list->el_info.el;
  typ = list->el_info.el_type;

  GET_DOF_VEC(v, drdv);
  if (!drdv->fe_space)
  {
    ERROR("no fe_space in dof_real_vec %s\n", NAME(drdv));
    return;
  }
  else if (!drdv->fe_space->bas_fcts)
  {
    ERROR("no basis functions in fe_space %s\n", NAME(drdv->fe_space));
    return;
  }
  GET_STRUCT(admin, drdv->fe_space);
  GET_STRUCT(bas_fcts, drdv->fe_space);

  get_dof_indices4_3d(pd, el, admin, bas_fcts);

/*--------------------------------------------------------------------------*/
/*  values on child[0]                                                      */
/*--------------------------------------------------------------------------*/

  get_dof_indices4_3d(cd, el->child[0], admin, bas_fcts);

  for (k = 0; k < DIM_OF_WORLD; k++)
  {
    v[pd[4]][k] = v[cd[11]][k];
    v[pd[5]][k] = v[cd[3]][k];
    v[pd[28]][k] = v[cd[27]][k];
    v[pd[30]][k] = v[cd[20]][k];
    v[pd[31]][k] = v[cd[30]][k];
    v[pd[33]][k] = v[cd[17]][k];
    v[pd[34]][k] = v[cd[24]][k];
  }

/*--------------------------------------------------------------------------*/
/*  values on child[1]                                                      */
/*--------------------------------------------------------------------------*/

  get_dof_indices4_3d(cd, el->child[1], admin, bas_fcts);

  if (typ == 0)
  {
/*--------------------------------------------------------------------------*/
/*  parent of el_type 0                                                     */
/*--------------------------------------------------------------------------*/

    for (k = 0; k < DIM_OF_WORLD; k++)
    {
      v[pd[6]][k] = v[cd[11]][k];
      v[pd[29]][k] = v[cd[30]][k];
      v[pd[32]][k] = v[cd[27]][k];
    }
  }
  else
  {
/*--------------------------------------------------------------------------*/
/*  parent of el_type 1|2                                                   */
/*--------------------------------------------------------------------------*/

    for (k = 0; k < DIM_OF_WORLD; k++)
    {
      v[pd[6]][k] = v[cd[11]][k];
      v[pd[29]][k] = v[cd[27]][k];
      v[pd[32]][k] = v[cd[30]][k];
    }
  }

/*--------------------------------------------------------------------------*/
/*   adjust neighbour values                                                */
/*--------------------------------------------------------------------------*/

  for (i = 1; i < n; i++)
  {
    el = list[i].el_info.el;
    typ = list[i].el_info.el_type;

    get_dof_indices4_3d(pd, el, admin, bas_fcts);

    lr_set = 0;
    if (list[i].neigh[0]  &&  list[i].neigh[0]->no < i)
      lr_set = 1;

    if (list[i].neigh[1]  &&  list[i].neigh[1]->no < i)
      lr_set += 2;

    DEBUG_TEST_EXIT(lr_set, "no values set on both neighbours\n");

/*--------------------------------------------------------------------------*/
/*  values on child[0]                                                      */
/*--------------------------------------------------------------------------*/

    get_dof_indices4_3d(cd, el->child[0], admin, bas_fcts);

    switch(lr_set)
    {
    case 1:
      for (k = 0; k < DIM_OF_WORLD; k++)
      {
	v[pd[31]][k] = v[cd[30]][k];
	v[pd[33]][k] = v[cd[17]][k];
	v[pd[34]][k] = v[cd[24]][k];
      }
      break;
    case 2:
      for (k = 0; k < DIM_OF_WORLD; k++)
      {
	v[pd[28]][k] = v[cd[27]][k];
	v[pd[30]][k] = v[cd[20]][k];
	v[pd[34]][k] = v[cd[24]][k];
      }
      break;
    case 3:
      for (k = 0; k < DIM_OF_WORLD; k++)
	v[pd[34]][k] = v[cd[24]][k];
      break;
    }

/*--------------------------------------------------------------------------*/
/*  values on child[1]                                                      */
/*--------------------------------------------------------------------------*/

    get_dof_indices4_3d(cd, el->child[1], admin, bas_fcts);

    if (typ == 0)
    {
      switch(lr_set)
      {
      case 1:
	for (k = 0; k < DIM_OF_WORLD; k++)
	  v[pd[32]][k] = v[cd[27]][k];
	break;
      case 2:
	for (k = 0; k < DIM_OF_WORLD; k++)
	  v[pd[29]][k] = v[cd[30]][k];
	break;
      }
    }
    else
    {
      switch(lr_set)
      {
      case 1:
	for (k = 0; k < DIM_OF_WORLD; k++)
	  v[pd[32]][k] = v[cd[30]][k];
	break;
      case 2:
	for (k = 0; k < DIM_OF_WORLD; k++)
	  v[pd[29]][k] = v[cd[27]][k];
	break;
      }
    }
  }

  return;
}



static void real_d_coarse_restr4_3d(DOF_REAL_D_VEC *drdv,
				    RC_LIST_EL *list, int n)
{
  FUNCNAME("real_d_coarse_restr4_3d");
  DOF pd[N_BAS_LAG_4_3D];
  DOF cd[N_BAS_LAG_4_3D];
  EL                *el;
  REAL_D            *v = NULL;
  int                i, k, lr_set;
  U_CHAR             typ;
  const DOF_ADMIN   *admin;
  const BAS_FCTS *bas_fcts;

  if (n < 1) return;
  el = list->el_info.el;
  typ = list->el_info.el_type;

  GET_DOF_VEC(v, drdv);
  if (!drdv->fe_space)
  {
    ERROR("no fe_space in dof_real_vec %s\n", NAME(drdv));
    return;
  }
  else if (!drdv->fe_space->bas_fcts)
  {
    ERROR("no basis functions in fe_space %s\n", NAME(drdv->fe_space));
    return;
  }
  GET_STRUCT(admin, drdv->fe_space);
  GET_STRUCT(bas_fcts, drdv->fe_space);

  get_dof_indices4_3d(pd, el, admin, bas_fcts);

/*--------------------------------------------------------------------------*/
/*  values on child[0]                                                      */
/*--------------------------------------------------------------------------*/

  get_dof_indices4_3d(cd, el->child[0], admin, bas_fcts);

  for (k = 0; k < DIM_OF_WORLD; k++)
  {
    v[pd[0]][k] += (0.2734375*v[cd[10]][k]
		    + 0.0390625*(-v[cd[12]][k] - v[cd[16]][k] - v[cd[19]][k]
				 - v[cd[22]][k] - v[cd[23]][k] - v[cd[25]][k]
				 - v[cd[28]][k])
		    + 0.0234375*(v[cd[18]][k] + v[cd[21]][k] + v[cd[26]][k]
				 + v[cd[29]][k] + v[cd[34]][k]));
    v[pd[1]][k] += (0.0390625*(-v[cd[10]][k] - v[cd[16]][k] - v[cd[19]][k]
			       - v[cd[22]][k] - v[cd[23]][k] - v[cd[25]][k]
			       - v[cd[26]][k] - v[cd[28]][k] - v[cd[29]][k]
			       - v[cd[34]][k])
		    + 0.0234375*(v[cd[12]][k] + v[cd[18]][k] + v[cd[21]][k]));
    v[pd[4]][k] = (v[cd[11]][k] + 1.09375*v[cd[10]][k] + 0.46875*v[cd[12]][k]
		   + 0.03125*(v[cd[16]][k] + v[cd[19]][k] + v[cd[22]][k]
			      + v[cd[23]][k] - v[cd[26]][k] - v[cd[29]][k]
			      - v[cd[34]][k])
		   + 0.09375*(-v[cd[18]][k] - v[cd[21]][k])
		   + 0.15625*(v[cd[25]][k] + v[cd[28]][k]));
    v[pd[5]][k] = (v[cd[3]][k] - 0.546875*v[cd[10]][k]
		   + 0.703125*v[cd[12]][k]
		   + 0.015625*(v[cd[16]][k] + v[cd[19]][k]
			       + v[cd[22]][k] + v[cd[23]][k])
		   + 0.140625*(v[cd[18]][k] + v[cd[21]][k])
		   + 0.234375*(-v[cd[25]][k] - v[cd[28]][k])
		   + 0.046875*(-v[cd[26]][k] - v[cd[29]][k] - v[cd[34]][k]));
    v[pd[6]][k] = (0.21875*v[cd[10]][k]
		   + 0.15625*(-v[cd[12]][k] + v[cd[25]][k] + v[cd[28]][k])
		   + 0.03125*(v[cd[16]][k] + v[cd[19]][k]
			      + v[cd[22]][k] + v[cd[23]][k])
		   + 0.09375*(-v[cd[18]][k] - v[cd[21]][k] + v[cd[26]][k]
			      + v[cd[29]][k] + v[cd[34]][k]));
    v[pd[7]][k] += (0.1875*v[cd[16]][k]
		    + 0.0625*(-v[cd[18]][k] + v[cd[23]][k] - v[cd[34]][k])
		    + 0.125*(v[cd[22]][k] - v[cd[29]][k])
		    + 0.3125*v[cd[28]][k]);
    v[pd[8]][k] += (0.375*(-v[cd[16]][k] + v[cd[29]][k])
		    - 0.125*v[cd[22]][k]);
    v[pd[9]][k] += 0.5*v[cd[16]][k];
    v[pd[10]][k] += (0.1875*v[cd[19]][k]
		     + 0.0625*(-v[cd[21]][k] + v[cd[22]][k] - v[cd[34]][k])
		     + 0.125*(v[cd[23]][k] - v[cd[26]][k])
		     + 0.3125*v[cd[25]][k]);
    v[pd[11]][k] += (0.375*(-v[cd[19]][k] + v[cd[26]][k])
		     - 0.125*v[cd[23]][k]);
    v[pd[12]][k] += 0.5*v[cd[19]][k];
    v[pd[13]][k] += (0.1875*v[cd[16]][k]
		     + 0.0625*(-v[cd[18]][k] + v[cd[23]][k]
			       + v[cd[28]][k] + v[cd[34]][k])
		     + 0.125*(v[cd[22]][k] + v[cd[29]][k]));
    v[pd[14]][k] += (-0.375*v[cd[16]][k]
		     + 0.125*(-v[cd[22]][k] - v[cd[29]][k]));
    v[pd[15]][k] += 0.5*v[cd[16]][k];
    v[pd[16]][k] += (0.1875*v[cd[19]][k]
		     + 0.0625*(-v[cd[21]][k] + v[cd[22]][k]
			       + v[cd[25]][k] + v[cd[34]][k])
		     + 0.125*(v[cd[23]][k] + v[cd[26]][k]));
    v[pd[17]][k] += (-0.375*v[cd[19]][k]
		     + 0.125*(-v[cd[23]][k] - v[cd[26]][k]));
    v[pd[18]][k] += 0.5*v[cd[19]][k];
    v[pd[22]][k] += (0.25*(-v[cd[22]][k] - v[cd[23]][k])
		     - 0.125*v[cd[34]][k]);
    v[pd[23]][k] += 0.5*v[cd[22]][k];
    v[pd[24]][k] += 0.5*v[cd[23]][k];
    v[pd[25]][k] += (0.25*(-v[cd[22]][k] - v[cd[23]][k])
		     + 0.375*v[cd[34]][k]);
    v[pd[26]][k] += 0.5*v[cd[22]][k];
    v[pd[27]][k] += 0.5*v[cd[23]][k];
    v[pd[28]][k] = (v[cd[27]][k] + 0.1875*(-v[cd[19]][k] + v[cd[34]][k])
		    + 0.5625*v[cd[21]][k] - 0.0625*v[cd[22]][k]
		    - 0.125*v[cd[23]][k] + 0.9375*v[cd[25]][k]
		    + 0.375*v[cd[26]][k]);
    v[pd[29]][k] = (0.1875*(-v[cd[19]][k] - v[cd[34]][k])
		    + 0.5625*v[cd[21]][k] - 0.0625*v[cd[22]][k]
		    - 0.125*v[cd[23]][k] - 0.3125*v[cd[25]][k]
		    - 0.375*v[cd[26]][k]);
    v[pd[30]][k] = (v[cd[20]][k] + 0.75*(v[cd[19]][k] + v[cd[26]][k])
		    + 0.25*v[cd[23]][k]);
    v[pd[31]][k] = (v[cd[30]][k] + 0.1875*(-v[cd[16]][k] + v[cd[34]][k])
		    + 0.5625*v[cd[18]][k] - 0.125*v[cd[22]][k]
		    - 0.0625*v[cd[23]][k] + 0.9375*v[cd[28]][k]
		    + 0.375*v[cd[29]][k]);
    v[pd[32]][k] = (0.1875*(-v[cd[16]][k] - v[cd[34]][k])
		    + 0.5625*v[cd[18]][k] - 0.125*v[cd[22]][k]
		    - 0.0625*v[cd[23]][k] - 0.3125*v[cd[28]][k]
		    - 0.375*v[cd[29]][k]);
    v[pd[33]][k] = (v[cd[17]][k] + 0.75*(v[cd[16]][k] + v[cd[29]][k])
		    + 0.25*v[cd[22]][k]);
    v[pd[34]][k] = (v[cd[24]][k] + 0.5*(v[cd[22]][k] + v[cd[23]][k])
		    + 0.75*v[cd[34]][k]);
  }
/*--------------------------------------------------------------------------*/
/*  values on child[1]                                                      */
/*--------------------------------------------------------------------------*/

  get_dof_indices4_3d(cd, el->child[1], admin, bas_fcts);

  if (typ == 0)
  {
/*--------------------------------------------------------------------------*/
/*  parent of el_type 0                                                     */
/*--------------------------------------------------------------------------*/
    for (k = 0; k < DIM_OF_WORLD; k++)
    {
      v[pd[0]][k] += (0.0390625*(-v[cd[10]][k] - v[cd[25]][k] - v[cd[26]][k]
				 - v[cd[28]][k] - v[cd[29]][k] - v[cd[34]][k])
		      + 0.0234375*v[cd[12]][k]);
      v[pd[1]][k] += (0.2734375*v[cd[10]][k]
		      + 0.0390625*(-v[cd[12]][k] - v[cd[25]][k]
				   - v[cd[28]][k])
		      + 0.0234375*(v[cd[26]][k] + v[cd[29]][k]
				   + v[cd[34]][k]));
      v[pd[4]][k] += (0.21875*v[cd[10]][k]
		      + 0.15625*(-v[cd[12]][k] + v[cd[25]][k] + v[cd[28]][k])
		      + 0.09375*(v[cd[26]][k] + v[cd[29]][k] + v[cd[34]][k]));
      v[pd[5]][k] += (-0.546875*v[cd[10]][k] + 0.703125*v[cd[12]][k]
		      + 0.234375*(-v[cd[25]][k] - v[cd[28]][k])
		      + 0.046875*(-v[cd[26]][k] - v[cd[29]][k]
				  - v[cd[34]][k]));
      v[pd[6]][k] += (v[cd[11]][k] + 1.09375*v[cd[10]][k]
		      + 0.46875*v[cd[12]][k]
		      + 0.15625*(v[cd[25]][k] + v[cd[28]][k])
		      + 0.03125*(-v[cd[26]][k] - v[cd[29]][k]
				 - v[cd[34]][k]));
      v[pd[7]][k] += (0.0625*(v[cd[25]][k] + v[cd[34]][k])
		      + 0.125*v[cd[26]][k]);
      v[pd[8]][k] += -0.125*v[cd[26]][k];
      v[pd[10]][k] += (0.0625*(v[cd[28]][k] + v[cd[34]][k])
		       + 0.125*v[cd[29]][k]);
      v[pd[11]][k] += -0.125*v[cd[29]][k];
      v[pd[13]][k] += (0.3125*v[cd[25]][k] - 0.125*v[cd[26]][k]
		       - 0.0625*v[cd[34]][k]);
      v[pd[14]][k] += 0.375*v[cd[26]][k];
      v[pd[16]][k] += (0.3125*v[cd[28]][k] - 0.125*v[cd[29]][k]
		       - 0.0625*v[cd[34]][k]);
      v[pd[17]][k] += 0.375*v[cd[29]][k];
      v[pd[22]][k] += 0.375*v[cd[34]][k];
      v[pd[25]][k] += -0.125*v[cd[34]][k];
      v[pd[28]][k] += (-0.3125*v[cd[28]][k] - 0.375*v[cd[29]][k]
		       - 0.1875*v[cd[34]][k]);
      v[pd[29]][k] += (v[cd[30]][k] + 0.9375*v[cd[28]][k] + 0.375*v[cd[29]][k]
		       + 0.1875*v[cd[34]][k]);
      v[pd[30]][k] += 0.75*v[cd[29]][k];
      v[pd[31]][k] += (-0.3125*v[cd[25]][k] - 0.375*v[cd[26]][k]
		       - 0.1875*v[cd[34]][k]);
      v[pd[32]][k] += (v[cd[27]][k] + 0.9375*v[cd[25]][k]
		       + 0.375*v[cd[26]][k] + 0.1875*v[cd[34]][k]);
      v[pd[33]][k] += 0.75*v[cd[26]][k];
      v[pd[34]][k] += 0.75*v[cd[34]][k];
    }
  }
  else
  {
/*--------------------------------------------------------------------------*/
/*  parent of el_type 1|2                                                   */
/*--------------------------------------------------------------------------*/
    for (k = 0; k < DIM_OF_WORLD; k++)
    {
      v[pd[0]][k] += (0.0390625*(-v[cd[10]][k] - v[cd[25]][k] - v[cd[26]][k]
				 - v[cd[28]][k] - v[cd[29]][k] - v[cd[34]][k])
		      + 0.0234375*v[cd[12]][k]);
      v[pd[1]][k] += (0.2734375*v[cd[10]][k]
		      + 0.0390625*(-v[cd[12]][k] - v[cd[25]][k]
				   - v[cd[28]][k])
		      + 0.0234375*(v[cd[26]][k] + v[cd[29]][k]
				   + v[cd[34]][k]));
      v[pd[4]][k] += (0.21875*v[cd[10]][k]
		      + 0.15625*(-v[cd[12]][k] + v[cd[25]][k] + v[cd[28]][k])
		      + 0.09375*(v[cd[26]][k] + v[cd[29]][k] + v[cd[34]][k]));
      v[pd[5]][k] += (-0.546875*v[cd[10]][k] + 0.703125*v[cd[12]][k]
		      + 0.234375*(-v[cd[25]][k] - v[cd[28]][k])
		      + 0.046875*(-v[cd[26]][k] - v[cd[29]][k]
				  - v[cd[34]][k]));
      v[pd[6]][k] += (v[cd[11]][k] + 1.09375*v[cd[10]][k]
		      + 0.46875*v[cd[12]][k]
		      + 0.15625*(v[cd[25]][k] + v[cd[28]][k])
		      + 0.03125*(-v[cd[26]][k] - v[cd[29]][k]
				 - v[cd[34]][k]));
      v[pd[7]][k] += (0.0625*(v[cd[28]][k] + v[cd[34]][k])
		      + 0.125*v[cd[29]][k]);
      v[pd[8]][k] += -0.125*v[cd[29]][k];
      v[pd[10]][k] += (0.0625*(v[cd[25]][k] + v[cd[34]][k])
		       + 0.125*v[cd[26]][k]);
      v[pd[11]][k] += -0.125*v[cd[26]][k];
      v[pd[13]][k] += (0.3125*v[cd[28]][k] - 0.125*v[cd[29]][k]
		       - 0.0625*v[cd[34]][k]);
      v[pd[14]][k] += 0.375*v[cd[29]][k];
      v[pd[16]][k] += (0.3125*v[cd[25]][k] - 0.125*v[cd[26]][k]
		       - 0.0625*v[cd[34]][k]);
      v[pd[17]][k] += 0.375*v[cd[26]][k];
      v[pd[22]][k] += 0.375*v[cd[34]][k];
      v[pd[25]][k] += -0.125*v[cd[34]][k];
      v[pd[28]][k] += (-0.3125*v[cd[25]][k] - 0.375*v[cd[26]][k]
		       - 0.1875*v[cd[34]][k]);
      v[pd[29]][k] += (v[cd[27]][k] + 0.9375*v[cd[25]][k] + 0.375*v[cd[26]][k]
		       + 0.1875*v[cd[34]][k]);
      v[pd[30]][k] += 0.75*v[cd[26]][k];
      v[pd[31]][k] += (-0.3125*v[cd[28]][k] - 0.375*v[cd[29]][k]
		       - 0.1875*v[cd[34]][k]);
      v[pd[32]][k] += (v[cd[30]][k] + 0.9375*v[cd[28]][k]
		       + 0.375*v[cd[29]][k] + 0.1875*v[cd[34]][k]);
      v[pd[33]][k] += 0.75*v[cd[29]][k];
      v[pd[34]][k] += 0.75*v[cd[34]][k];
    }
  }

/*--------------------------------------------------------------------------*/
/*   adjust neighbour values                                                */
/*--------------------------------------------------------------------------*/

  for (i = 1; i < n; i++)
  {
    el = list[i].el_info.el;
    typ = list[i].el_info.el_type;

    get_dof_indices4_3d(pd, el, admin, bas_fcts);

    lr_set = 0;
    if (list[i].neigh[0]  &&  list[i].neigh[0]->no < i)
      lr_set = 1;

    if (list[i].neigh[1]  &&  list[i].neigh[1]->no < i)
      lr_set += 2;

    DEBUG_TEST_EXIT(lr_set, "no values set on both neighbours\n");

/*--------------------------------------------------------------------------*/
/*  values on child[0]                                                      */
/*--------------------------------------------------------------------------*/

    get_dof_indices4_3d(cd, el->child[0], admin, bas_fcts);

    switch(lr_set)
    {
    case 1:
      for (k = 0; k < DIM_OF_WORLD; k++)
      {
	v[pd[0]][k] += (0.0390625*(-v[cd[16]][k] - v[cd[22]][k]
				   - v[cd[23]][k] - v[cd[28]][k])
			+ 0.0234375*(v[cd[18]][k] + v[cd[29]][k]
				     + v[cd[34]][k]));
	v[pd[1]][k] += (0.0390625*(-v[cd[16]][k] - v[cd[22]][k] - v[cd[23]][k]
				   - v[cd[28]][k]-v[cd[29]][k]-v[cd[34]][k])
			+ 0.0234375*v[cd[18]][k]);
	v[pd[4]][k] += (0.03125*(v[cd[16]][k] + v[cd[22]][k] + v[cd[23]][k]
				 - v[cd[29]][k] - v[cd[34]][k])
			- 0.09375*v[cd[18]][k] + 0.15625*v[cd[28]][k]);
	v[pd[5]][k] += (0.015625*(v[cd[16]][k] + v[cd[22]][k] + v[cd[23]][k])
			+ 0.140625*v[cd[18]][k] - 0.234375*v[cd[28]][k]
			+ 0.046875*(-v[cd[29]][k] - v[cd[34]][k]));
	v[pd[6]][k] += (0.03125*(v[cd[16]][k] + v[cd[22]][k] + v[cd[23]][k])
			+ 0.09375*(-v[cd[18]][k]+v[cd[29]][k]+v[cd[34]][k])
			+ 0.15625*v[cd[28]][k]);
	v[pd[7]][k] += (0.1875*v[cd[16]][k]
			+ 0.0625*(-v[cd[18]][k] + v[cd[23]][k] - v[cd[34]][k])
			+ 0.125*(v[cd[22]][k] - v[cd[29]][k])
			+ 0.3125*v[cd[28]][k]);
	v[pd[8]][k] += (0.375*(-v[cd[16]][k] + v[cd[29]][k])
			- 0.125*v[cd[22]][k]);
	v[pd[9]][k] += 0.5*v[cd[16]][k];
	v[pd[10]][k] += (0.0625*(v[cd[22]][k] - v[cd[34]][k])
			 + 0.125*v[cd[23]][k]);
	v[pd[11]][k] += -0.125*v[cd[23]][k];
	v[pd[13]][k] += (0.1875*v[cd[16]][k]
			 + 0.0625*(-v[cd[18]][k] + v[cd[23]][k]
				   + v[cd[28]][k] + v[cd[34]][k])
			 + 0.125*(v[cd[22]][k] + v[cd[29]][k]));
	v[pd[14]][k] += (-0.375*v[cd[16]][k]
			 + 0.125*(-v[cd[22]][k] - v[cd[29]][k]));
	v[pd[15]][k] += 0.5*v[cd[16]][k];
	v[pd[16]][k] += (0.0625*(v[cd[22]][k] + v[cd[34]][k])
			 + 0.125*v[cd[23]][k]);
	v[pd[17]][k] += -0.125*v[cd[23]][k];
	v[pd[22]][k] += (0.25*(-v[cd[22]][k] - v[cd[23]][k])
			 - 0.125*v[cd[34]][k]);
	v[pd[23]][k] += 0.5*v[cd[22]][k];
	v[pd[24]][k] += 0.5*v[cd[23]][k];
	v[pd[25]][k] += (0.25*(-v[cd[22]][k] - v[cd[23]][k])
			 + 0.375*v[cd[34]][k]);
	v[pd[26]][k] += 0.5*v[cd[22]][k];
	v[pd[27]][k] += 0.5*v[cd[23]][k];
	v[pd[28]][k] += (-0.0625*v[cd[22]][k] - 0.125*v[cd[23]][k]
			 + 0.1875*v[cd[34]][k]);
	v[pd[29]][k] += (-0.0625*v[cd[22]][k] - 0.125*v[cd[23]][k]
			 - 0.1875*v[cd[34]][k]);
	v[pd[30]][k] += 0.25*v[cd[23]][k];
	v[pd[31]][k] = (v[cd[30]][k] + 0.1875*(-v[cd[16]][k] + v[cd[34]][k])
			+ 0.5625*v[cd[18]][k] - 0.125*v[cd[22]][k]
			- 0.0625*v[cd[23]][k] + 0.9375*v[cd[28]][k]
			+ 0.375*v[cd[29]][k]);
	v[pd[32]][k] = (0.1875*(-v[cd[16]][k] - v[cd[34]][k])
			+ 0.5625*v[cd[18]][k] - 0.125*v[cd[22]][k]
			- 0.0625*v[cd[23]][k] - 0.3125*v[cd[28]][k]
			- 0.375*v[cd[29]][k]);
	v[pd[33]][k] = (v[cd[17]][k] + 0.75*(v[cd[16]][k] + v[cd[29]][k])
			+ 0.25*v[cd[22]][k]);
	v[pd[34]][k] = (v[cd[24]][k] + 0.5*(v[cd[22]][k] + v[cd[23]][k])
			+ 0.75*v[cd[34]][k]);
      }
      break;
    case 2:
      for (k = 0; k < DIM_OF_WORLD; k++)
      {
	v[pd[0]][k] += (0.0390625*(-v[cd[19]][k] - v[cd[22]][k]
				   - v[cd[23]][k] - v[cd[25]][k])
			+ 0.0234375*(v[cd[21]][k] + v[cd[26]][k]
				     + v[cd[34]][k]));
	v[pd[1]][k] += (0.0390625*(-v[cd[19]][k]-v[cd[22]][k]-v[cd[23]][k]
				   - v[cd[25]][k]-v[cd[26]][k]-v[cd[34]][k])
			+ 0.0234375*v[cd[21]][k]);
	v[pd[4]][k] += (0.03125*(v[cd[19]][k] + v[cd[22]][k] + v[cd[23]][k]
				 - v[cd[26]][k] - v[cd[34]][k])
			- 0.09375*v[cd[21]][k] + 0.15625*v[cd[25]][k]);
	v[pd[5]][k] += (0.015625*(v[cd[19]][k] + v[cd[22]][k] + v[cd[23]][k])
			+ 0.140625*v[cd[21]][k] - 0.234375*v[cd[25]][k]
			+ 0.046875*(-v[cd[26]][k] - v[cd[34]][k]));
	v[pd[6]][k] += (0.03125*(v[cd[19]][k] + v[cd[22]][k] + v[cd[23]][k])
			+ 0.09375*(-v[cd[21]][k]+v[cd[26]][k]+v[cd[34]][k])
			+ 0.15625*v[cd[25]][k]);
	v[pd[7]][k] += (0.125*v[cd[22]][k]
			+ 0.0625*(v[cd[23]][k]-v[cd[34]][k]));
	v[pd[8]][k] += -0.125*v[cd[22]][k];
	v[pd[10]][k] += (0.1875*v[cd[19]][k]
			 + 0.0625*(-v[cd[21]][k]+v[cd[22]][k]-v[cd[34]][k])
			 + 0.125*(v[cd[23]][k] - v[cd[26]][k])
		      + 0.3125*v[cd[25]][k]);
	v[pd[11]][k] += (0.375*(-v[cd[19]][k] + v[cd[26]][k])
			 - 0.125*v[cd[23]][k]);
	v[pd[12]][k] += 0.5*v[cd[19]][k];
	v[pd[13]][k] += (0.125*v[cd[22]][k]
			 + 0.0625*(v[cd[23]][k] + v[cd[34]][k]));
	v[pd[14]][k] += -0.125*v[cd[22]][k];
	v[pd[16]][k] += (0.1875*v[cd[19]][k]
			 + 0.0625*(-v[cd[21]][k] + v[cd[22]][k]
				   + v[cd[25]][k] + v[cd[34]][k])
			 + 0.125*(v[cd[23]][k] + v[cd[26]][k]));
	v[pd[17]][k] += (-0.375*v[cd[19]][k]
			 + 0.125*(-v[cd[23]][k] - v[cd[26]][k]));
	v[pd[18]][k] += 0.5*v[cd[19]][k];
	v[pd[22]][k] += (0.25*(-v[cd[22]][k] - v[cd[23]][k])
			 - 0.125*v[cd[34]][k]);
	v[pd[23]][k] += (0.5*v[cd[22]][k]);
	v[pd[24]][k] += (0.5*v[cd[23]][k]);
	v[pd[25]][k] += (0.25*(-v[cd[22]][k] - v[cd[23]][k])
			 + 0.375*v[cd[34]][k]);
	v[pd[26]][k] += (0.5*v[cd[22]][k]);
	v[pd[27]][k] += (0.5*v[cd[23]][k]);
	v[pd[28]][k] = (v[cd[27]][k] + 0.1875*(-v[cd[19]][k] + v[cd[34]][k])
			+ 0.5625*v[cd[21]][k] - 0.0625*v[cd[22]][k]
			- 0.125*v[cd[23]][k] + 0.9375*v[cd[25]][k]
			+ 0.375*v[cd[26]][k]);
	v[pd[29]][k] = (0.1875*(-v[cd[19]][k] - v[cd[34]][k])
			+ 0.5625*v[cd[21]][k] - 0.0625*v[cd[22]][k]
			- 0.125*v[cd[23]][k] - 0.3125*v[cd[25]][k]
			- 0.375*v[cd[26]][k]);
	v[pd[30]][k] = (v[cd[20]][k] + 0.75*(v[cd[19]][k] + v[cd[26]][k])
			+ 0.25*v[cd[23]][k]);
	v[pd[31]][k] += (-0.125*v[cd[22]][k] - 0.0625*v[cd[23]][k]
			 + 0.1875*v[cd[34]][k]);
	v[pd[32]][k] += (-0.125*v[cd[22]][k] - 0.0625*v[cd[23]][k]
			 - 0.1875*v[cd[34]][k]);
	v[pd[33]][k] += 0.25*v[cd[22]][k];
	v[pd[34]][k] = (v[cd[24]][k] + 0.5*(v[cd[22]][k] + v[cd[23]][k])
			+ 0.75*v[cd[34]][k]);
      }
      break;
    case 3:
      for (k = 0; k < DIM_OF_WORLD; k++)
      {
	v[pd[0]][k] += (0.0390625*(-v[cd[22]][k] - v[cd[23]][k])
			+ 0.0234375*v[cd[34]][k]);
	v[pd[1]][k] += (0.0390625*(-v[cd[22]][k] - v[cd[23]][k]
				   - v[cd[34]][k]));
	v[pd[4]][k] += (0.03125*(v[cd[22]][k] + v[cd[23]][k] - v[cd[34]][k]));
	v[pd[5]][k] += (0.015625*(v[cd[22]][k] + v[cd[23]][k])
			- 0.046875*v[cd[34]][k]);
	v[pd[6]][k] += (0.03125*(v[cd[22]][k] + v[cd[23]][k])
			+ 0.09375*v[cd[34]][k]);
	v[pd[7]][k] += (0.125*v[cd[22]][k]
			+ 0.0625*(v[cd[23]][k] - v[cd[34]][k]));
	v[pd[8]][k] += -0.125*v[cd[22]][k];
	v[pd[10]][k] += (0.0625*(v[cd[22]][k] - v[cd[34]][k])
			 + 0.125*v[cd[23]][k]);
	v[pd[11]][k] += -0.125*v[cd[23]][k];
	v[pd[13]][k] += (0.125*v[cd[22]][k]
			 + 0.0625*(v[cd[23]][k] + v[cd[34]][k]));
	v[pd[14]][k] += -0.125*v[cd[22]][k];
	v[pd[16]][k] += (0.0625*(v[cd[22]][k] + v[cd[34]][k])
			 + 0.125*v[cd[23]][k]);
	v[pd[17]][k] += -0.125*v[cd[23]][k];
	v[pd[22]][k] += (0.25*(-v[cd[22]][k] - v[cd[23]][k])
			 - 0.125*v[cd[34]][k]);
	v[pd[23]][k] += 0.5*v[cd[22]][k];
	v[pd[24]][k] += 0.5*v[cd[23]][k];
	v[pd[25]][k] += (0.25*(-v[cd[22]][k] - v[cd[23]][k])
			 + 0.375*v[cd[34]][k]);
	v[pd[26]][k] += 0.5*v[cd[22]][k];
	v[pd[27]][k] += 0.5*v[cd[23]][k];
	v[pd[28]][k] += (-0.0625*v[cd[22]][k] - 0.125*v[cd[23]][k]
			 + 0.1875*v[cd[34]][k]);
	v[pd[29]][k] += (-0.0625*v[cd[22]][k] - 0.125*v[cd[23]][k]
			 - 0.1875*v[cd[34]][k]);
	v[pd[30]][k] += 0.25*v[cd[23]][k];
	v[pd[31]][k] += (-0.125*v[cd[22]][k] - 0.0625*v[cd[23]][k]
			 + 0.1875*v[cd[34]][k]);
	v[pd[32]][k] += (-0.125*v[cd[22]][k] - 0.0625*v[cd[23]][k]
			 - 0.1875*v[cd[34]][k]);
	v[pd[33]][k] += 0.25*v[cd[22]][k];
	v[pd[34]][k] = (v[cd[24]][k] + 0.5*(v[cd[22]][k] + v[cd[23]][k])
			+ 0.75*v[cd[34]][k]);
      }
      break;
    }

/*--------------------------------------------------------------------------*/
/*  values on child[1]                                                      */
/*--------------------------------------------------------------------------*/

    get_dof_indices4_3d(cd, el->child[1], admin, bas_fcts);

    if (typ == 0)
    {
      switch(lr_set)
      {
      case 1:
	for (k = 0; k < DIM_OF_WORLD; k++)
	{
	  v[pd[0]][k] += (0.0390625*(-v[cd[25]][k] - v[cd[26]][k]
				     - v[cd[34]][k]));
	  v[pd[1]][k] += (-0.0390625*v[cd[25]][k]
			  + 0.0234375*(v[cd[26]][k] + v[cd[34]][k]));
	  v[pd[4]][k] += (0.15625*v[cd[25]][k]
			  + 0.09375*(v[cd[26]][k] + v[cd[34]][k]));
	  v[pd[5]][k] += (-0.234375*v[cd[25]][k]
			  + 0.046875*(-v[cd[26]][k] - v[cd[34]][k]));
	  v[pd[6]][k] += (0.15625*v[cd[25]][k]
			  + 0.03125*(-v[cd[26]][k] - v[cd[34]][k]));
	  v[pd[7]][k] += (0.0625*(v[cd[25]][k] + v[cd[34]][k])
			  + 0.125*v[cd[26]][k]);
	  v[pd[8]][k] += -0.125*v[cd[26]][k];
	  v[pd[10]][k] += 0.0625*v[cd[34]][k];
	  v[pd[13]][k] += (0.3125*v[cd[25]][k] - 0.125*v[cd[26]][k]
			   - 0.0625*v[cd[34]][k]);
	  v[pd[14]][k] += 0.375*v[cd[26]][k];
	  v[pd[16]][k] += -0.0625*v[cd[34]][k];
	  v[pd[22]][k] += 0.375*v[cd[34]][k];
	  v[pd[25]][k] += -0.125*v[cd[34]][k];
	  v[pd[28]][k] += -0.1875*v[cd[34]][k];
	  v[pd[29]][k] += 0.1875*v[cd[34]][k];
	  v[pd[31]][k] += (-0.3125*v[cd[25]][k] - 0.375*v[cd[26]][k]
			   - 0.1875*v[cd[34]][k]);
	  v[pd[32]][k] += (v[cd[27]][k] + 0.9375*v[cd[25]][k]
			   + 0.375*v[cd[26]][k] + 0.1875*v[cd[34]][k]);
	  v[pd[33]][k] += 0.75*v[cd[26]][k];
	  v[pd[34]][k] += 0.75*v[cd[34]][k];
	}
	break;
      case 2:
	for (k = 0; k < DIM_OF_WORLD; k++)
	{
	  v[pd[0]][k] += (0.0390625*(-v[cd[28]][k] - v[cd[29]][k]
				     - v[cd[34]][k]));
	  v[pd[1]][k] += (-0.0390625*v[cd[28]][k]
			  + 0.0234375*(v[cd[29]][k] + v[cd[34]][k]));
	  v[pd[4]][k] += (0.15625*v[cd[28]][k]
			  + 0.09375*(v[cd[29]][k] + v[cd[34]][k]));
	  v[pd[5]][k] += (-0.234375*v[cd[28]][k]
			  + 0.046875*(-v[cd[29]][k] - v[cd[34]][k]));
	  v[pd[6]][k] += (0.15625*v[cd[28]][k]
			  + 0.03125*(-v[cd[29]][k] - v[cd[34]][k]));
	  v[pd[7]][k] += 0.0625*v[cd[34]][k];
	  v[pd[10]][k] += (0.0625*(v[cd[28]][k] + v[cd[34]][k])
			   + 0.125*v[cd[29]][k]);
	  v[pd[11]][k] += -0.125*v[cd[29]][k];
	  v[pd[13]][k] += -0.0625*v[cd[34]][k];
	  v[pd[16]][k] += (0.3125*v[cd[28]][k] - 0.125*v[cd[29]][k]
			   - 0.0625*v[cd[34]][k]);
	  v[pd[17]][k] += 0.375*v[cd[29]][k];
	  v[pd[22]][k] += 0.375*v[cd[34]][k];
	  v[pd[25]][k] += -0.125*v[cd[34]][k];
	  v[pd[28]][k] += (-0.3125*v[cd[28]][k] - 0.375*v[cd[29]][k]
			   - 0.1875*v[cd[34]][k]);
	  v[pd[29]][k] += (v[cd[30]][k] + 0.9375*v[cd[28]][k]
			   + 0.375*v[cd[29]][k] + 0.1875*v[cd[34]][k]);
	  v[pd[30]][k] += 0.75*v[cd[29]][k];
	  v[pd[31]][k] += -0.1875*v[cd[34]][k];
	  v[pd[32]][k] += 0.1875*v[cd[34]][k];
	  v[pd[34]][k] += 0.75*v[cd[34]][k];
	}
	break;
      case 3:
	for (k = 0; k < DIM_OF_WORLD; k++)
	{
	  v[pd[0]][k] += -0.0390625*v[cd[34]][k];
	  v[pd[1]][k] += 0.0234375*v[cd[34]][k];
	  v[pd[4]][k] += 0.09375*v[cd[34]][k];
	  v[pd[5]][k] += -0.046875*v[cd[34]][k];
	  v[pd[6]][k] += -0.03125*v[cd[34]][k];
	  v[pd[7]][k] += 0.0625*v[cd[34]][k];
	  v[pd[10]][k] += 0.0625*v[cd[34]][k];
	  v[pd[13]][k] += -0.0625*v[cd[34]][k];
	  v[pd[16]][k] += -0.0625*v[cd[34]][k];
	  v[pd[22]][k] += 0.375*v[cd[34]][k];
	  v[pd[25]][k] += -0.125*v[cd[34]][k];
	  v[pd[28]][k] += -0.1875*v[cd[34]][k];
	  v[pd[29]][k] += 0.1875*v[cd[34]][k];
	  v[pd[31]][k] += -0.1875*v[cd[34]][k];
	  v[pd[32]][k] += 0.1875*v[cd[34]][k];
	  v[pd[34]][k] += 0.75*v[cd[34]][k];
	}
	break;
      }
    }
    else
    {
      switch(lr_set)
      {
      case 1:
	for (k = 0; k < DIM_OF_WORLD; k++)
	{
	  v[pd[0]][k] += (0.0390625*(-v[cd[28]][k] - v[cd[29]][k]
				     - v[cd[34]][k]));
	  v[pd[1]][k] += (-0.0390625*v[cd[28]][k]
			  + 0.0234375*(v[cd[29]][k] + v[cd[34]][k]));
	  v[pd[4]][k] += (0.15625*v[cd[28]][k]
			  + 0.09375*(v[cd[29]][k] + v[cd[34]][k]));
	  v[pd[5]][k] += (-0.234375*v[cd[28]][k]
			  + 0.046875*(-v[cd[29]][k] - v[cd[34]][k]));
	  v[pd[6]][k] += (0.15625*v[cd[28]][k]
			  + 0.03125*(-v[cd[29]][k] - v[cd[34]][k]));
	  v[pd[7]][k] += (0.0625*(v[cd[28]][k] + v[cd[34]][k])
			  + 0.125*v[cd[29]][k]);
	  v[pd[8]][k] += -0.125*v[cd[29]][k];
	  v[pd[10]][k] += 0.0625*v[cd[34]][k];
	  v[pd[13]][k] += (0.3125*v[cd[28]][k] - 0.125*v[cd[29]][k]
			   - 0.0625*v[cd[34]][k]);
	  v[pd[14]][k] += 0.375*v[cd[29]][k];
	  v[pd[16]][k] += -0.0625*v[cd[34]][k];
	  v[pd[22]][k] += 0.375*v[cd[34]][k];
	  v[pd[25]][k] += -0.125*v[cd[34]][k];
	  v[pd[28]][k] += -0.1875*v[cd[34]][k];
	  v[pd[29]][k] += 0.1875*v[cd[34]][k];
	  v[pd[31]][k] += (-0.3125*v[cd[28]][k] - 0.375*v[cd[29]][k]
			   - 0.1875*v[cd[34]][k]);
	  v[pd[32]][k] += (v[cd[30]][k] + 0.9375*v[cd[28]][k]
			   + 0.375*v[cd[29]][k] + 0.1875*v[cd[34]][k]);
	  v[pd[33]][k] += 0.75*v[cd[29]][k];
	  v[pd[34]][k] += 0.75*v[cd[34]][k];
	}
	break;
      case 2:
	for (k = 0; k < DIM_OF_WORLD; k++)
	{
	  v[pd[0]][k] += (0.0390625*(-v[cd[25]][k] - v[cd[26]][k]
				     - v[cd[34]][k]));
	  v[pd[1]][k] += (-0.0390625*v[cd[25]][k]
			  + 0.0234375*(v[cd[26]][k] + v[cd[34]][k]));
	  v[pd[4]][k] += (0.15625*v[cd[25]][k]
			  + 0.09375*(v[cd[26]][k] + v[cd[34]][k]));
	  v[pd[5]][k] += (-0.234375*v[cd[25]][k]
			  + 0.046875*(-v[cd[26]][k] - v[cd[34]][k]));
	  v[pd[6]][k] += (0.15625*v[cd[25]][k]
			  + 0.03125*(-v[cd[26]][k] - v[cd[34]][k]));
	  v[pd[7]][k] += 0.0625*v[cd[34]][k];
	  v[pd[10]][k] += (0.0625*(v[cd[25]][k] + v[cd[34]][k])
			   + 0.125*v[cd[26]][k]);
	  v[pd[11]][k] += -0.125*v[cd[26]][k];
	  v[pd[13]][k] += -0.0625*v[cd[34]][k];
	  v[pd[16]][k] += (0.3125*v[cd[25]][k] - 0.125*v[cd[26]][k]
			   - 0.0625*v[cd[34]][k]);
	  v[pd[17]][k] += 0.375*v[cd[26]][k];
	  v[pd[22]][k] += 0.375*v[cd[34]][k];
	  v[pd[25]][k] += -0.125*v[cd[34]][k];
	  v[pd[28]][k] += (-0.3125*v[cd[25]][k] - 0.375*v[cd[26]][k]
			   - 0.1875*v[cd[34]][k]);
	  v[pd[29]][k] += (v[cd[27]][k] + 0.9375*v[cd[25]][k]
			   + 0.375*v[cd[26]][k] + 0.1875*v[cd[34]][k]);
	  v[pd[30]][k] += 0.75*v[cd[26]][k];
	  v[pd[31]][k] += -0.1875*v[cd[34]][k];
	  v[pd[32]][k] += 0.1875*v[cd[34]][k];
	  v[pd[34]][k] += 0.75*v[cd[34]][k];
	}
	break;
      case 3:
	for (k = 0; k < DIM_OF_WORLD; k++)
	{
	  v[pd[0]][k] += -0.0390625*v[cd[34]][k];
	  v[pd[1]][k] += 0.0234375*v[cd[34]][k];
	  v[pd[4]][k] += 0.09375*v[cd[34]][k];
	  v[pd[5]][k] += -0.046875*v[cd[34]][k];
	  v[pd[6]][k] += -0.03125*v[cd[34]][k];
	  v[pd[7]][k] += 0.0625*v[cd[34]][k];
	  v[pd[10]][k] += 0.0625*v[cd[34]][k];
	  v[pd[13]][k] += -0.0625*v[cd[34]][k];
	  v[pd[16]][k] += -0.0625*v[cd[34]][k];
	  v[pd[22]][k] += 0.375*v[cd[34]][k];
	  v[pd[25]][k] += -0.125*v[cd[34]][k];
	  v[pd[28]][k] += -0.1875*v[cd[34]][k];
	  v[pd[29]][k] += 0.1875*v[cd[34]][k];
	  v[pd[31]][k] += -0.1875*v[cd[34]][k];
	  v[pd[32]][k] += 0.1875*v[cd[34]][k];
	  v[pd[34]][k] += 0.75*v[cd[34]][k];
	}
	break;
      }
    }
  }

  return;
}

/*--------------------------------------------------------------------------*/
/*--------------------------------------------------------------------------*/

static const BAS_FCT phi4_3d[N_BAS_LAG_4_3D] = {
  phi4v0_3d,  phi4v1_3d,
  phi4v2_3d,  phi4v3_3d,
  phi4e00_3d, phi4e01_3d, phi4e02_3d,
  phi4e10_3d, phi4e11_3d, phi4e12_3d,
  phi4e20_3d, phi4e21_3d, phi4e22_3d,
  phi4e30_3d, phi4e31_3d, phi4e32_3d,
  phi4e40_3d, phi4e41_3d, phi4e42_3d,
  phi4e50_3d, phi4e51_3d, phi4e52_3d,
  phi4f00_3d, phi4f01_3d, phi4f02_3d,
  phi4f10_3d, phi4f11_3d, phi4f12_3d,
  phi4f20_3d, phi4f21_3d, phi4f22_3d,
  phi4f30_3d, phi4f31_3d, phi4f32_3d,
  phi4c_3d
};
static const GRD_BAS_FCT  grd_phi4_3d[N_BAS_LAG_4_3D] = {
  grd_phi4v0_3d,  grd_phi4v1_3d,
  grd_phi4v2_3d,  grd_phi4v3_3d,
  grd_phi4e00_3d, grd_phi4e01_3d, grd_phi4e02_3d,
  grd_phi4e10_3d, grd_phi4e11_3d, grd_phi4e12_3d,
  grd_phi4e20_3d, grd_phi4e21_3d, grd_phi4e22_3d,
  grd_phi4e30_3d, grd_phi4e31_3d, grd_phi4e32_3d,
  grd_phi4e40_3d, grd_phi4e41_3d, grd_phi4e42_3d,
  grd_phi4e50_3d, grd_phi4e51_3d, grd_phi4e52_3d,
  grd_phi4f00_3d, grd_phi4f01_3d, grd_phi4f02_3d,
  grd_phi4f10_3d, grd_phi4f11_3d, grd_phi4f12_3d,
  grd_phi4f20_3d, grd_phi4f21_3d, grd_phi4f22_3d,
  grd_phi4f30_3d, grd_phi4f31_3d, grd_phi4f32_3d,
  grd_phi4c_3d
};
static const D2_BAS_FCT  D2_phi4_3d[N_BAS_LAG_4_3D] = {
  D2_phi4v0_3d,  D2_phi4v1_3d,
  D2_phi4v2_3d,  D2_phi4v3_3d,
  D2_phi4e00_3d, D2_phi4e01_3d, D2_phi4e02_3d,
  D2_phi4e10_3d, D2_phi4e11_3d, D2_phi4e12_3d,
  D2_phi4e20_3d, D2_phi4e21_3d, D2_phi4e22_3d,
  D2_phi4e30_3d, D2_phi4e31_3d, D2_phi4e32_3d,
  D2_phi4e40_3d, D2_phi4e41_3d, D2_phi4e42_3d,
  D2_phi4e50_3d, D2_phi4e51_3d, D2_phi4e52_3d,
  D2_phi4f00_3d, D2_phi4f01_3d, D2_phi4f02_3d,
  D2_phi4f10_3d, D2_phi4f11_3d, D2_phi4f12_3d,
  D2_phi4f20_3d, D2_phi4f21_3d, D2_phi4f22_3d,
  D2_phi4f30_3d, D2_phi4f31_3d, D2_phi4f32_3d,
  D2_phi4c_3d
};

const D3_BAS_FCT D3_phi4_3d[N_BAS_LAG_4_3D] = {
  D3_phi4v0_3d,  D3_phi4v1_3d,
  D3_phi4v2_3d,  D3_phi4v3_3d,
  D3_phi4e00_3d, D3_phi4e01_3d, D3_phi4e02_3d,
  D3_phi4e10_3d, D3_phi4e11_3d, D3_phi4e12_3d,
  D3_phi4e20_3d, D3_phi4e21_3d, D3_phi4e22_3d,
  D3_phi4e30_3d, D3_phi4e31_3d, D3_phi4e32_3d,
  D3_phi4e40_3d, D3_phi4e41_3d, D3_phi4e42_3d,
  D3_phi4e50_3d, D3_phi4e51_3d, D3_phi4e52_3d,
  D3_phi4f00_3d, D3_phi4f01_3d, D3_phi4f02_3d,
  D3_phi4f10_3d, D3_phi4f11_3d, D3_phi4f12_3d,
  D3_phi4f20_3d, D3_phi4f21_3d, D3_phi4f22_3d,
  D3_phi4f30_3d, D3_phi4f31_3d, D3_phi4f32_3d,
  D3_phi4c_3d
};

const D4_BAS_FCT D4_phi4_3d[N_BAS_LAG_4_3D] = {
  D4_phi4v0_3d,  D4_phi4v1_3d,
  D4_phi4v2_3d,  D4_phi4v3_3d,
  D4_phi4e00_3d, D4_phi4e01_3d, D4_phi4e02_3d,
  D4_phi4e10_3d, D4_phi4e11_3d, D4_phi4e12_3d,
  D4_phi4e20_3d, D4_phi4e21_3d, D4_phi4e22_3d,
  D4_phi4e30_3d, D4_phi4e31_3d, D4_phi4e32_3d,
  D4_phi4e40_3d, D4_phi4e41_3d, D4_phi4e42_3d,
  D4_phi4e50_3d, D4_phi4e51_3d, D4_phi4e52_3d,
  D4_phi4f00_3d, D4_phi4f01_3d, D4_phi4f02_3d,
  D4_phi4f10_3d, D4_phi4f11_3d, D4_phi4f12_3d,
  D4_phi4f20_3d, D4_phi4f21_3d, D4_phi4f22_3d,
  D4_phi4f30_3d, D4_phi4f31_3d, D4_phi4f32_3d,
  D4_phi4c_3d
};

/* For each wall the mapping from the wall basis functions to the
 * local DOFs on the reference element. See also submesh.c.
 *
 * Meaning of the dimensions is the same as in the BAS_FCTS structure
 *
 * [type][orientation][wall][bas_fcts]
 */
static const int trace_mapping_lag_4_3d[2][2][N_WALLS_3D][N_BAS_LAG_4_2D] = {
  {
    { /* t = 0, o = + */
      {3,  1,  2, 13, 14, 15, 19, 20, 21, 18, 17, 16, 24, 22, 23},
      {2,  0,  3, 10, 11, 12, 21, 20, 19,  9,  8,  7, 26, 25, 27},
      {0,  1,  3, 16, 17, 18, 12, 11, 10,  4,  5,  6, 28, 29, 30},
      {1,  0,  2,  7,  8,  9, 15, 14, 13,  6,  5,  4, 32, 31, 33}
    },
    { /* t = 0, o = - */
      {1,  3,  2, 21, 20, 19, 15, 14, 13, 16, 17, 18, 22, 24, 23},
      {0,  2,  3, 19, 20, 21, 12, 11, 10,  7,  8,  9, 25, 26, 27},
      {1,  0,  3, 10, 11, 12, 18, 17, 16,  6,  5,  4, 29, 28, 30},
      {0,  1,  2, 13, 14, 15,  9,  8,  7,  4,  5,  6, 31, 32, 33}
    }
  },
  {
    { /* t = 1, o = + */
      {1,  2,  3, 19, 20, 21, 18, 17, 16, 13, 14, 15, 22, 23, 24},
      {2,  0,  3, 10, 11, 12, 21, 20, 19,  9,  8,  7, 26, 25, 27},
      {0,  1,  3, 16, 17, 18, 12, 11, 10,  4,  5,  6, 28, 29, 30},
      {1,  0,  2,  7,  8,  9, 15, 14, 13,  6,  5,  4, 32, 31, 33}
    },
    { /* t = 1, o = - */
      {2,  1,  3, 16, 17, 18, 21, 20, 19, 15, 14, 13, 23, 22, 24},
      {0,  2,  3, 19, 20, 21, 12, 11, 10,  7,  8,  9, 25, 26, 27},
      {1,  0,  3, 10, 11, 12, 18, 17, 16,  6,  5,  4, 29, 28, 30},
      {0,  1,  2, 13, 14, 15,  9,  8,  7,  4,  5,  6, 31, 32, 33}
    }
  }
};

static const BAS_FCTS lagrange4_3d = {
  "lagrange4_3d", 3, 1, N_BAS_LAG_4_3D, N_BAS_LAG_4_3D, 4,
  {1, 1, 3, 3}, /* VERTEX,CENTER,EDGE,FACE */
  -1, /* trace_admin */
  INIT_BFCTS_CHAIN(lagrange4_3d),
  INIT_ELEMENT_INITIALIZER(NULL, FILL_NOTHING), /* init_element + fill-flags */
  phi4_3d, grd_phi4_3d, D2_phi4_3d, D3_phi4_3d, D4_phi4_3d,
  NULL, NULL, NULL, false, /* phi_d etc. */  
  /********************/
  &lagrange4_2d, /* trace space */
  { { { trace_mapping_lag_4_3d[0][0][0], /* t = 0, o = + */
	trace_mapping_lag_4_3d[0][0][1],
	trace_mapping_lag_4_3d[0][0][2],
	trace_mapping_lag_4_3d[0][0][3] },
      { trace_mapping_lag_4_3d[0][1][0], /* t = 0, o = - */
	trace_mapping_lag_4_3d[0][1][1],
	trace_mapping_lag_4_3d[0][1][2],
	trace_mapping_lag_4_3d[0][1][3] } },
    { { trace_mapping_lag_4_3d[1][0][0], /* t = 1, o = + */
	trace_mapping_lag_4_3d[1][0][1],
	trace_mapping_lag_4_3d[1][0][2],
	trace_mapping_lag_4_3d[1][0][3] },
      { trace_mapping_lag_4_3d[1][1][0], /* t = 1, o = - */
	trace_mapping_lag_4_3d[1][1][1],
	trace_mapping_lag_4_3d[1][1][2],
	trace_mapping_lag_4_3d[1][1][3] } } }, /* trace mapping */
  { N_BAS_LAG_4_2D,
    N_BAS_LAG_4_2D,
    N_BAS_LAG_4_2D,
    N_BAS_LAG_4_2D }, /* n_trace_bas_fcts */
  get_dof_indices4_3d,
  get_bound4_3d,
  interpol4_3d,
  interpol_d_4_3d,
  interpol_dow_4_3d,
  get_int_vec4_3d,
  get_real_vec4_3d,
  get_real_d_vec4_3d,
  (GET_REAL_VEC_D_TYPE)get_real_d_vec4_3d,
  get_uchar_vec4_3d,
  get_schar_vec4_3d,
  get_ptr_vec4_3d,
  get_real_dd_vec4_3d,
  real_refine_inter4_3d,
  real_coarse_inter4_3d,
  real_coarse_restr4_3d,
  real_d_refine_inter4_3d,
  real_d_coarse_inter4_3d,
  real_d_coarse_restr4_3d,
  (REF_INTER_FCT_D)real_d_refine_inter4_3d,
  (REF_INTER_FCT_D)real_d_coarse_inter4_3d,
  (REF_INTER_FCT_D)real_d_coarse_restr4_3d,
  (void *)&lag_4_3d_data
};
