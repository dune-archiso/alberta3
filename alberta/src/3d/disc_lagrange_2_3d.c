/*--------------------------------------------------------------------------*/
/* ALBERTA:  an Adaptive multi Level finite element toolbox using           */
/*           Bisectioning refinement and Error control by Residual          */
/*           Techniques for scientific Applications                         */
/*                                                                          */
/* file:     disc_lagrange_2_3d.c                                           */
/*                                                                          */
/* description:  piecewise quadratic discontinuous Lagrange elements in 3d  */
/*                                                                          */
/*--------------------------------------------------------------------------*/
/*                                                                          */
/*  authors:   Alfred Schmidt                                               */
/*             Zentrum fuer Technomathematik                                */
/*             Fachbereich 3 Mathematik/Informatik                          */
/*             Universitaet Bremen                                          */
/*             Bibliothekstr. 2                                             */
/*             D-28359 Bremen, Germany                                      */
/*                                                                          */
/*             Kunibert G. Siebert                                          */
/*             Institut fuer Mathematik                                     */
/*             Universitaet Augsburg                                        */
/*             Universitaetsstr. 14                                         */
/*             D-86159 Augsburg, Germany                                    */
/*                                                                          */
/*  http://www.mathematik.uni-freiburg.de/IAM/ALBERTA                       */
/*                                                                          */
/*  (c) by A. Schmidt and K.G. Siebert (1996-2003)                          */
/*                                                                          */
/*--------------------------------------------------------------------------*/

#if 0
/* The inverse of the mass-matrix on the reference element. */
static const REAL inv_mass_2_3d[N_BAS_LAG_2_3D][N_BAS_LAG_2_3D] = {
  {600., 60., 60., 60., 15., 15., 15., 60., 60., 60.},
  {60., 600., 60., 60., 15., 60., 60., 15., 15., 60.},
  {60., 60., 600., 60., 60., 15., 60., 15., 60., 15.},
  {60., 60., 60., 600., 60., 60., 15., 60., 15., 15.},
  {15., 15., 60., 60., 172.5, -41.25, -41.25, -41.25, -41.25, 60.},
  {15., 60., 15., 60., -41.25, 172.5, -41.25, -41.25, 60., -41.25},
  {15., 60., 60., 15., -41.25, -41.25, 172.5, 60., -41.25, -41.25},
  {60., 15., 15., 60., -41.25, -41.25, 60., 172.5, -41.25, -41.25},
  {60., 15., 60., 15., -41.25, 60., -41.25, -41.25, 172.5, -41.25},
  {60., 60., 15., 15., 60., -41.25, -41.25, -41.25, -41.25, 172.5}
};
#endif

/****************************************************************************/
/*  basisfunction at vertex 0                                               */
/****************************************************************************/

static REAL d_phi2v0_3d(const REAL_B lambda, const BAS_FCTS *thisptr)
{
  return(lambda[0]*(2.0*lambda[0] - 1.0));
}

static const REAL *d_grd_phi2v0_3d(const REAL_B lambda, const BAS_FCTS *thisptr)
{
  static REAL_B grd = {0};

  grd[0] = 4.0*lambda[0] - 1.0;
  return((const REAL *) grd);
}

static const REAL_B (*d_D2_phi2v0_3d(const REAL *lambda, const BAS_FCTS *thisptr))
{
  static const REAL_BB D2 = {{4, 0, 0, 0},
			     {0, 0, 0, 0},
			     {0, 0, 0, 0},
			     {0, 0, 0, 0}};

  return(D2);
}

/****************************************************************************/
/*  basisfunction at vertex 1                                               */
/****************************************************************************/

static REAL d_phi2v1_3d(const REAL_B lambda, const BAS_FCTS *thisptr)
{
  return(lambda[1]*(2.0*lambda[1] - 1.0));
}

static const REAL *d_grd_phi2v1_3d(const REAL_B lambda, const BAS_FCTS *thisptr)
{
  static REAL_B grd = {0};

  grd[1] = 4.0*lambda[1] - 1.0;
  return((const REAL *) grd);
}

static const REAL_B (*d_D2_phi2v1_3d(const REAL *lambda, const BAS_FCTS *thisptr))
{
  static const REAL_BB D2 = {{0, 0, 0, 0},
			     {0, 4, 0, 0},
			     {0, 0, 0, 0},
			     {0, 0, 0, 0}};

  return(D2);
}

/****************************************************************************/
/*  basisfunction at vertex 2                                               */
/****************************************************************************/

static REAL d_phi2v2_3d(const REAL_B lambda, const BAS_FCTS *thisptr)
{
  return(lambda[2]*(2.0*lambda[2] - 1.0));
}

static const REAL *d_grd_phi2v2_3d(const REAL_B lambda, const BAS_FCTS *thisptr)
{
  static REAL_B grd = {0};

  grd[2] = 4.0*lambda[2] - 1.0;
  return((const REAL *) grd);
}

static const REAL_B (*d_D2_phi2v2_3d(const REAL *lambda, const BAS_FCTS *thisptr))
{
  static const REAL_BB D2 = {{0, 0, 0, 0},
			     {0, 0, 0, 0},
			     {0, 0, 4, 0},
			     {0, 0, 0, 0}};

  return(D2);
}

/****************************************************************************/
/*  basisfunction at vertex 3                                               */
/****************************************************************************/

static REAL d_phi2v3_3d(const REAL_B lambda, const BAS_FCTS *thisptr)
{
  return(lambda[3]*(2.0*lambda[3] - 1.0));
}

static const REAL *d_grd_phi2v3_3d(const REAL_B lambda, const BAS_FCTS *thisptr)
{
  static REAL_B grd = {0};

  grd[3] = 4.0*lambda[3] - 1.0;
  return((const REAL *) grd);
}

static const REAL_B (*d_D2_phi2v3_3d(const REAL *lambda, const BAS_FCTS *thisptr))
{
  static const REAL_BB D2 = {{0, 0, 0, 0},
			     {0, 0, 0, 0},
			     {0, 0, 0, 0},
			     {0, 0, 0, 4}};

  return(D2);
}

/****************************************************************************/
/*  basisfunction at edge 0                                                 */
/****************************************************************************/

static REAL d_phi2e0_3d(const REAL_B lambda, const BAS_FCTS *thisptr)
{
  return(4.0*lambda[0]*lambda[1]);
}

static const REAL *d_grd_phi2e0_3d(const REAL_B lambda, const BAS_FCTS *thisptr)
{
  static REAL_B grd = {0};

  grd[0] = 4.0*lambda[1];
  grd[1] = 4.0*lambda[0];
  return((const REAL *) grd);
}

static const REAL_B (*d_D2_phi2e0_3d(const REAL *lambda, const BAS_FCTS *thisptr))
{
  static const REAL_BB D2 = {{0, 4, 0, 0},
			     {4, 0, 0, 0},
			     {0, 0, 0, 0},
			     {0, 0, 0, 0}};
  return(D2);
}

/****************************************************************************/
/*  basisfunction at edge 1                                                 */
/****************************************************************************/

static REAL d_phi2e1_3d(const REAL_B lambda, const BAS_FCTS *thisptr)
{
  return(4.0*lambda[0]*lambda[2]);
}

static const REAL *d_grd_phi2e1_3d(const REAL_B lambda, const BAS_FCTS *thisptr)
{
  static REAL_B grd = {0};

  grd[0] = 4.0*lambda[2];
  grd[2] = 4.0*lambda[0];
  return((const REAL *) grd);
}

static const REAL_B (*d_D2_phi2e1_3d(const REAL *lambda, const BAS_FCTS *thisptr))
{
  static const REAL_BB D2 = {{0, 0, 4, 0},
			     {0, 0, 0, 0},
			     {4, 0, 0, 0},
			     {0, 0, 0, 0}};
  return(D2);
}

/****************************************************************************/
/*  basisfunction at edge 2                                                 */
/****************************************************************************/

static REAL d_phi2e2_3d(const REAL_B lambda, const BAS_FCTS *thisptr)
{
  return(4.0*lambda[0]*lambda[3]);
}

static const REAL *d_grd_phi2e2_3d(const REAL_B lambda, const BAS_FCTS *thisptr)
{
  static REAL_B grd = {0};

  grd[0] = 4.0*lambda[3];
  grd[3] = 4.0*lambda[0];
  return((const REAL *) grd);
}

static const REAL_B (*d_D2_phi2e2_3d(const REAL *lambda, const BAS_FCTS *thisptr))
{
  static const REAL_BB D2 = {{0, 0, 0, 4},
			     {0, 0, 0, 0},
			     {4, 0, 0, 0},
			     {0, 0, 0, 0}};
  return(D2);
}

/****************************************************************************/
/*  basisfunction at edge 3                                                 */
/****************************************************************************/

static REAL d_phi2e3_3d(const REAL_B lambda, const BAS_FCTS *thisptr)
{
  return(4.0*lambda[1]*lambda[2]);
}

static const REAL *d_grd_phi2e3_3d(const REAL_B lambda, const BAS_FCTS *thisptr)
{
  static REAL_B grd = {0};

  grd[1] = 4.0*lambda[2];
  grd[2] = 4.0*lambda[1];
  return((const REAL *) grd);
}

static const REAL_B (*d_D2_phi2e3_3d(const REAL *lambda, const BAS_FCTS *thisptr))
{
  static const REAL_BB D2 = {{0, 0, 0, 0},
			     {0, 0, 4, 0},
			     {0, 4, 0, 0},
			     {0, 0, 0, 0}};
  return(D2);
}

/****************************************************************************/
/*  basisfunction at edge 4                                                 */
/****************************************************************************/

static REAL d_phi2e4_3d(const REAL_B lambda, const BAS_FCTS *thisptr)
{
  return(4.0*lambda[1]*lambda[3]);
}

static const REAL *d_grd_phi2e4_3d(const REAL_B lambda, const BAS_FCTS *thisptr)
{
  static REAL_B grd = {0};

  grd[1] = 4.0*lambda[3];
  grd[3] = 4.0*lambda[1];
  return((const REAL *) grd);
}

static const REAL_B (*d_D2_phi2e4_3d(const REAL *lambda, const BAS_FCTS *thisptr))
{
  static const REAL_BB D2 = {{0, 0, 0, 0},
			     {0, 0, 0, 4},
			     {0, 0, 0, 0},
			     {0, 4, 0, 0}};
  return(D2);
}

/****************************************************************************/
/*  basisfunction at edge 5                                                 */
/****************************************************************************/

static REAL d_phi2e5_3d(const REAL_B lambda, const BAS_FCTS *thisptr)
{
  return(4.0*lambda[2]*lambda[3]);
}

static const REAL *d_grd_phi2e5_3d(const REAL_B lambda, const BAS_FCTS *thisptr)
{
  static REAL_B grd = {0};

  grd[2] = 4.0*lambda[3];
  grd[3] = 4.0*lambda[2];
  return((const REAL *) grd);
}

static const REAL_B (*d_D2_phi2e5_3d(const REAL *lambda, const BAS_FCTS *thisptr))
{
  static const REAL_BB D2 = {{0, 0, 0, 0},
			     {0, 0, 0, 0},
			     {0, 0, 0, 4},
			     {0, 0, 4, 0}};
  return(D2);
}

/*****************************************************************************/

#undef DEF_EL_VEC_D_2_3D
#define DEF_EL_VEC_D_2_3D(type, name)				\
  DEF_EL_VEC_CONST(type, name, N_BAS_LAG_2_3D, N_BAS_LAG_2_3D)

#undef DEFUN_GET_EL_VEC_D_2_3D
#define DEFUN_GET_EL_VEC_D_2_3D(name, type, admin, body, ...)		\
  static const EL_##type##_VEC *					\
  d_get_##name##2_3d(type##_VEC_TYPE *vec, const EL *el, __VA_ARGS__)	\
  {									\
    FUNCNAME("d_get_"#name"d_2_3d");					\
    static DEF_EL_VEC_D_2_3D(type, rvec_space);				\
    type##_VEC_TYPE *rvec = vec ? vec : rvec_space->vec;		\
    int n0, node, ibas;							\
    DOF **dofptr = el->dof, dof;					\
									\
    DEBUG_TEST_EXIT(true, "");						\
									\
    node = (admin)->mesh->node[CENTER];					\
    n0   = (admin)->n0_dof[CENTER];					\
    for (ibas = 0; ibas < N_BAS_LAG_2_3D; ibas++) {			\
      dof = dofptr[node][n0+ibas];					\
      body;								\
    }									\
									\
    return vec ? NULL : rvec_space;					\
  }									\
  struct _AI_semicolon_dummy

#undef DEFUN_GET_EL_DOF_VEC_D_2_3D
#define DEFUN_GET_EL_DOF_VEC_D_2_3D(name, type, ASSIGN)			\
  DEFUN_GET_EL_VEC_D_2_3D(_##name##_vec, type, dv->fe_space->admin,	\
			  ASSIGN(dv->vec[dof], rvec[ibas]),		\
			  const DOF_##type##_VEC *dv);			\
  static const EL_##type##_VEC *					\
  d_get_##name##_vec2_3d(type##_VEC_TYPE *vec, const EL *el,		\
			 const DOF_##type##_VEC *dv)			\
  {									\
    EL_##type##_VEC *vec_loc = dv->vec_loc;				\
									\
    if (vec != NULL || vec_loc == NULL) {				\
      return d_get__##name##_vec2_3d(vec, el, dv);			\
    } else {								\
      d_get__##name##_vec2_3d(vec_loc->vec, el, dv);			\
      return vec_loc;							\
    }									\
  }									\
  struct _AI_semicolon_dummy

#undef COPY_EQ
#define COPY_EQ(a, b) (b) = (a)

/*--------------------------------------------------------------------------*/
/*  functions for combining basisfunctions with coefficients                */
/*--------------------------------------------------------------------------*/

DEFUN_GET_EL_VEC_D_2_3D(dof_indices, DOF, admin,
			rvec[ibas] = dof,
			const DOF_ADMIN *admin, const BAS_FCTS *thisptr);

static const EL_BNDRY_VEC *d_get_bound2_3d(BNDRY_FLAGS *vec,
					   const EL_INFO *el_info,
					   const BAS_FCTS *thisptr)
{
  FUNCNAME("d_get_bound2_3d");
  static DEF_EL_VEC_D_2_3D(BNDRY, rvec_space);
  BNDRY_FLAGS *rvec = vec ? vec : rvec_space->vec;
  int i;

  TEST_FLAG(FILL_BOUND, el_info);

  for (i = 0; i < N_BAS_LAG_2_3D; i++) {
    BNDRY_FLAGS_INIT(rvec[i]);
    BNDRY_FLAGS_SET(rvec[i], el_info->face_bound[0]);
  }

  return vec ? NULL : rvec_space;
}

/*--------------------------------------------------------------------*/
/*--- function for accessing a local DOF_INT_VEC                   ---*/
/*--------------------------------------------------------------------*/

DEFUN_GET_EL_DOF_VEC_D_2_3D(int, INT, COPY_EQ);

/*--------------------------------------------------------------------*/
/*--- function for accessing a local DOF_REAL_VEC                  ---*/
/*--------------------------------------------------------------------*/

DEFUN_GET_EL_DOF_VEC_D_2_3D(real, REAL, COPY_EQ);

/*--------------------------------------------------------------------*/
/*--- function for accessing a local DOF_REAL_D_VEC                ---*/
/*--------------------------------------------------------------------*/

DEFUN_GET_EL_DOF_VEC_D_2_3D(real_d, REAL_D, COPY_DOW);

/*--------------------------------------------------------------------*/
/*--- function for accessing a local DOF_SCHAR_VEC                 ---*/
/*--------------------------------------------------------------------*/

DEFUN_GET_EL_DOF_VEC_D_2_3D(schar, SCHAR, COPY_EQ);

/*--------------------------------------------------------------------*/
/*--- function for accessing a local DOF_UCHAR_VEC                 ---*/
/*--------------------------------------------------------------------*/

DEFUN_GET_EL_DOF_VEC_D_2_3D(uchar, UCHAR, COPY_EQ);

/*--------------------------------------------------------------------*/
/*--- function for accessing a local DOF_REAL_DD_VEC               ---*/
/*--------------------------------------------------------------------*/

DEFUN_GET_EL_DOF_VEC_D_2_3D(real_dd, REAL_DD, _AI_MCOPY_DOW);

/*--------------------------------------------------------------------*/
/*--- function for accessing a local DOF_PTR_VEC                   ---*/
/*--------------------------------------------------------------------*/

DEFUN_GET_EL_DOF_VEC_D_2_3D(ptr, PTR, COPY_EQ);

/*--------------------------------------------------------------------*/
/*--- function for local interpolaton of scalar functions          ---*/
/*--------------------------------------------------------------------*/

GENERATE_INTERPOL(d_, 2, 3, N_BAS_LAG_2_3D);

/*--------------------------------------------------------------------*/
/*--- function for local interpolaton of vector functions          ---*/
/*--------------------------------------------------------------------*/

GENERATE_INTERPOL_D(d_, 2, 3, N_BAS_LAG_2_3D);

GENERATE_INTERPOL_DOW(d_, 2, 3, N_BAS_LAG_2_3D);

/****************************************************************************/
/*  functions for interpolation/ restriction during refinement/coarsening   */
/****************************************************************************/

static void d_real_refine_inter2_3d(DOF_REAL_VEC *drv, RC_LIST_EL *list, int n)
{
  FUNCNAME("d_real_refine_inter2_3d");
  DOF pdof[N_BAS_LAG_2_3D];
  DOF cdof[N_BAS_LAG_2_3D];
  EL        *el;
  REAL      *vec = NULL;
  int       i;
  U_CHAR    type;
  const DOF_ADMIN *admin;
  const BAS_FCTS *bas_fcts;

  if (n < 1) return;

  GET_DOF_VEC(vec, drv);
  if (!drv->fe_space)
  {
    ERROR("no fe_space in dof_real_vec %s\n", NAME(drv));
    return;
  }
  else if (!drv->fe_space->bas_fcts)
  {
    ERROR("no basis functions in fe_space %s\n", NAME(drv->fe_space));
    return;
  }
  GET_STRUCT(admin, drv->fe_space);
  GET_STRUCT(bas_fcts, drv->fe_space);

  for (i = 0; i < n; i++)
  {
    el = list[i].el_info.el;

    d_get_dof_indices2_3d(pdof, el, admin, bas_fcts);

/****************************************************************************/
/*  values on child[0]                                                      */
/****************************************************************************/

    d_get_dof_indices2_3d(cdof, el->child[0], admin, bas_fcts);

    vec[cdof[0]] = vec[pdof[0]];
    vec[cdof[1]] = vec[pdof[2]];
    vec[cdof[2]] = vec[pdof[3]];
    vec[cdof[3]] = vec[pdof[4]];
    vec[cdof[4]] = vec[pdof[5]];
    vec[cdof[5]] = vec[pdof[6]];
    vec[cdof[6]] = (0.375*vec[pdof[0]] - 0.125*vec[pdof[1]]
		    + 0.75*vec[pdof[4]]);
    vec[cdof[7]] = vec[pdof[9]];
    vec[cdof[8]] = (0.125*(-vec[pdof[0]] - vec[pdof[1]]) + 0.25*vec[pdof[4]]
		    + 0.5*(vec[pdof[5]] + vec[pdof[7]]));
    vec[cdof[9]] = (0.125*(-vec[pdof[0]] - vec[pdof[1]]) + 0.25*vec[pdof[4]]
		    + 0.5*(vec[pdof[6]] + vec[pdof[8]]));

/****************************************************************************/
/*  values on child[1]                                                      */
/****************************************************************************/

#if NEIGH_IN_EL
    type = list[i].el_info.el->el_type;
#else
    type = list[i].el_info.el_type;
#endif

    d_get_dof_indices2_3d(cdof, el->child[1], admin, bas_fcts);
    vec[cdof[0]] = vec[pdof[1]];
    if (type == 0)
    {
      vec[cdof[1]] = vec[pdof[3]];
      vec[cdof[2]] = vec[pdof[2]];
    }
    else
    {
      vec[cdof[1]] = vec[pdof[2]];
      vec[cdof[2]] = vec[pdof[3]];
    }
    vec[cdof[3]] = vec[pdof[4]];
    if (type == 0)
    {
      vec[cdof[4]] = vec[pdof[8]];
      vec[cdof[5]] = vec[pdof[7]];
    }
    else
    {
      vec[cdof[4]] = vec[pdof[7]];
      vec[cdof[5]] = vec[pdof[8]];
    }
    vec[cdof[6]] = (0.375*vec[pdof[1]] - 0.125*vec[pdof[0]]
		    + 0.75*vec[pdof[4]]);
    vec[cdof[7]] = vec[pdof[9]];
    if (type == 0)
    {
      vec[cdof[8]] = (0.125*(-vec[pdof[0]] - vec[pdof[1]]) + 0.25*vec[pdof[4]]
		      + 0.5*(vec[pdof[6]] + vec[pdof[8]]));
      vec[cdof[9]] = (0.125*(-vec[pdof[0]] - vec[pdof[1]]) + 0.25*vec[pdof[4]]
		      + 0.5*(vec[pdof[5]] + vec[pdof[7]]));
    }
    else
    {
      vec[cdof[8]] = (0.125*(-vec[pdof[0]] - vec[pdof[1]]) + 0.25*vec[pdof[4]]
		      + 0.5*(vec[pdof[5]] + vec[pdof[7]]));
      vec[cdof[9]] = (0.125*(-vec[pdof[0]] - vec[pdof[1]]) + 0.25*vec[pdof[4]]
		      + 0.5*(vec[pdof[6]] + vec[pdof[8]]));
    }
  }
  return;
}

static void d_real_coarse_inter2_3d(DOF_REAL_VEC *drv, RC_LIST_EL *list, int n)
{
  FUNCNAME("d_real_coarse_inter2_3d");
  DOF pdof[N_BAS_LAG_2_3D];
  DOF cdof0[N_BAS_LAG_2_3D];
  DOF cdof1[N_BAS_LAG_2_3D];
  EL        *el;
  REAL      *v = NULL;
  U_CHAR    type;
  const DOF_ADMIN *admin;
  const BAS_FCTS *bas_fcts;
  MESH      *mesh = NULL;
  int       i;

  if (n < 1) return;

  GET_DOF_VEC(v, drv);
  if (!drv->fe_space)
  {
    ERROR("no fe_space in dof_real_vec %s\n", NAME(drv));
    return;
  }
  else if (!drv->fe_space->bas_fcts)
  {
    ERROR("no basis functions in fe_space %s\n", NAME(drv->fe_space));
    return;
  }

  GET_STRUCT(admin, drv->fe_space);
  GET_STRUCT(bas_fcts, drv->fe_space);
  GET_STRUCT(mesh, drv->fe_space);

  for (i = 0; i < n; i++)
  {
    el = list[i].el_info.el;

#if NEIGH_IN_EL
    type = el->el_type;
#else
    type = list[i].el_info.el_type;
#endif

    d_get_dof_indices2_3d(pdof, el, admin, bas_fcts);
    d_get_dof_indices2_3d(cdof0, el->child[0], admin, bas_fcts);
    d_get_dof_indices2_3d(cdof1, el->child[1], admin, bas_fcts);

    v[pdof[0]] = v[cdof0[0]];
    v[pdof[1]] = v[cdof1[0]];
    v[pdof[2]] = 0.5 * (v[cdof0[1]] + v[cdof1[ type ? 1 : 2 ]]);
    v[pdof[3]] = 0.5 * (v[cdof0[2]] + v[cdof1[ type ? 2 : 1 ]]);
    v[pdof[4]] = 0.5 * (v[cdof0[3]] + v[cdof1[3]]);
    v[pdof[5]] = 0.5 * (v[cdof0[9]] + v[cdof1[ type ? 9 : 8 ]]);
    v[pdof[6]] = v[cdof0[5]];
    v[pdof[7]] = v[cdof1[ type ? 4 : 5]];
    v[pdof[8]] = v[cdof1[ type ? 5 : 4]];
    v[pdof[9]] = 0.5 * (v[cdof0[7]] + v[cdof1[7]]);
  }

  return;
}


static const BAS_FCT     d_phi2_3d[N_BAS_LAG_2_3D]     = {
  d_phi2v0_3d, d_phi2v1_3d,
  d_phi2v2_3d, d_phi2v3_3d,
  d_phi2e0_3d, d_phi2e1_3d,
  d_phi2e2_3d, d_phi2e3_3d,
  d_phi2e4_3d, d_phi2e5_3d
};
static const GRD_BAS_FCT d_grd_phi2_3d[N_BAS_LAG_2_3D] = {
  d_grd_phi2v0_3d, d_grd_phi2v1_3d,
  d_grd_phi2v2_3d, d_grd_phi2v3_3d,
  d_grd_phi2e0_3d, d_grd_phi2e1_3d,
  d_grd_phi2e2_3d, d_grd_phi2e3_3d,
  d_grd_phi2e4_3d, d_grd_phi2e5_3d
};
static const D2_BAS_FCT  d_D2_phi2_3d[N_BAS_LAG_2_3D]  = {
  d_D2_phi2v0_3d, d_D2_phi2v1_3d,
  d_D2_phi2v2_3d, d_D2_phi2v3_3d,
  d_D2_phi2e0_3d, d_D2_phi2e1_3d,
  d_D2_phi2e2_3d, d_D2_phi2e3_3d,
  d_D2_phi2e4_3d, d_D2_phi2e5_3d
};

static BAS_FCTS disc_lagrange2_3d = {
  "disc_lagrange2_3d", 3, 1, N_BAS_LAG_2_3D, N_BAS_LAG_2_3D, 2,
  {0, N_BAS_LAG_2_3D, 0, 0},
  -1, /* trace_admin */
  INIT_BFCTS_CHAIN(disc_lagrange2_3d),
  INIT_ELEMENT_INITIALIZER(NULL, FILL_NOTHING), /* init_element + fill-flags */
  d_phi2_3d, d_grd_phi2_3d, d_D2_phi2_3d,
  NULL, NULL, /* third and fourth derivatives */
  NULL, NULL, NULL, false, /* phi_d etc. */  
  /********************/
  &disc_lagrange2_2d, /* trace space */
  { { { trace_mapping_lag_2_3d[0][0][0], /* t = 0, o = + */
	trace_mapping_lag_2_3d[0][0][1],
	trace_mapping_lag_2_3d[0][0][2],
	trace_mapping_lag_2_3d[0][0][3] },
      { trace_mapping_lag_2_3d[0][1][0], /* t = 0, o = - */
	trace_mapping_lag_2_3d[0][1][1],
	trace_mapping_lag_2_3d[0][1][2],
	trace_mapping_lag_2_3d[0][1][3] } },
    { { trace_mapping_lag_2_3d[1][0][0], /* t = 1, o = + */
	trace_mapping_lag_2_3d[1][0][1],
	trace_mapping_lag_2_3d[1][0][2],
	trace_mapping_lag_2_3d[1][0][3] },
      { trace_mapping_lag_2_3d[1][1][0], /* t = 1, o = - */
	trace_mapping_lag_2_3d[1][1][1],
	trace_mapping_lag_2_3d[1][1][2],
	trace_mapping_lag_2_3d[1][1][3] } } }, /* trace mapping */
  { N_BAS_LAG_2_2D,
    N_BAS_LAG_2_2D,
    N_BAS_LAG_2_2D,
    N_BAS_LAG_2_2D }, /* n_trace_bas_fcts */
  d_get_dof_indices2_3d,
  d_get_bound2_3d,
  d_interpol2_3d,
  d_interpol_d_2_3d,
  d_interpol_dow_2_3d,
  d_get_int_vec2_3d,
  d_get_real_vec2_3d,
  d_get_real_d_vec2_3d,
  (GET_REAL_VEC_D_TYPE)d_get_real_d_vec2_3d,
  d_get_uchar_vec2_3d,
  d_get_schar_vec2_3d,
  d_get_ptr_vec2_3d,
  d_get_real_dd_vec2_3d,
  d_real_refine_inter2_3d,
  d_real_coarse_inter2_3d,
  NULL,
  NULL,
  NULL,
  NULL,
  NULL,
  NULL,
  NULL,
  (void *)&lag_2_3d_data
};
