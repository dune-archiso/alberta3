# Hey, Emacs, we're -*- makefile -*- mode!
# 
# Makefile.am fragment shared by all builds
#

ASSEMBLE_SRC = \
 ../Common/M_M_M_M_assemble_fcts.c\
 ../Common/M_M_DM_DM_assemble_fcts.c\
 ../Common/M_M_SCM_SCM_assemble_fcts.c\
 ../Common/DM_DM_DM_DM_assemble_fcts.c\
 ../Common/DM_DM_SCM_SCM_assemble_fcts.c\
 ../Common/SCM_SCM_SCM_SCM_assemble_fcts.c
VSSV_ASSEMBLE_SRC = \
 ../Common/DM_DM_DM_DM_assemble_fcts.c \
 ../Common/DM_DM_SCM_SCM_assemble_fcts.c \
 ../Common/SCM_SCM_SCM_SCM_assemble_fcts.c

ASSEMBLE_BNDRY_SRC =\
 ../Common/M_M_assemble_bndry_fcts.c\
 ../Common/M_DM_assemble_bndry_fcts.c\
 ../Common/M_SCM_assemble_bndry_fcts.c\
 ../Common/DM_DM_assemble_bndry_fcts.c\
 ../Common/DM_SCM_assemble_bndry_fcts.c\
 ../Common/SCM_SCM_assemble_bndry_fcts.c
VSSV_ASSEMBLE_BNDRY_SRC =\
 ../Common/DM_DM_assemble_bndry_fcts.c\
 ../Common/DM_SCM_assemble_bndry_fcts.c\
 ../Common/SCM_SCM_assemble_bndry_fcts.c

assemble_sources =\
 ../Common/assemble.c ../Common/assemble.h\
 ../Common/assemble_bndry.c ../Common/assemble_bndry.h\
 ../Common/assemble_neigh.c\
 $(ASSEMBLE_SRC) $(ASSEMBLE_SRC:.c=.h) $(ASSEMBLE_BNDRY_SRC)

VSSV_assemble_sources =\
 ../Common/assemble.c ../Common/assemble.h\
 ../Common/assemble_bndry.c ../Common/assemble_bndry.h\
 ../Common/assemble_neigh.c\
 $(VSSV_ASSEMBLE_SRC) $(VSSV_ASSEMBLE_SRC:.c=.h) $(VSSV_ASSEMBLE_BNDRY_SRC)

common_fem_sources = \
  ../Common/HB_precon.c \
  ../Common/MG.c \
  ../Common/MG_s.c \
  ../Common/MG_s1.c \
  ../Common/MG_s2.c \
  ../Common/SSOR_precon.c \
  ../Common/adapt.c \
  ../Common/assemble-instat.c \
  ../Common/block_precon.c \
  ../Common/bndry_cond.c \
  ../Common/crs_matrix.c \
  ../Common/crs_matrix.h \
  ../Common/diag_precon.c \
  ../Common/error.c \
  ../Common/estimator.c \
  ../Common/estimator_dowb.c \
  ../Common/ilu_k_precon.c \
  ../Common/ilu_k_precon.h \
  ../Common/l2scp.c \
  ../Common/level.c \
  ../Common/oem_solve.c \
  ../Common/oem_sp_solve.c \
  ../Common/sor.c \
  ../Common/ssor.c \
  ../Common/wall_quad.c

common_sources = \
  ../Common/bas_fct.c \
  ../Common/check.c \
  ../Common/coarsen.c \
  ../Common/dof_admin.c \
  ../Common/element.c \
  ../Common/eval.c \
  ../Common/gauss-quad.c \
  ../Common/macro.c \
  ../Common/memory.c \
  ../Common/numint.c \
  ../Common/parametric.c \
  ../Common/parametric_intern.h \
  ../Common/periodic.c \
  ../Common/quad_cache.c \
  ../Common/read_mesh.c \
  ../Common/read_mesh_xdr_1.2.c \
  ../Common/refine.c \
  ../Common/submesh.c \
  ../Common/trav_xy.c \
  ../Common/traverse_nr.c \
  ../Common/traverse_r.c \
  ../Common/write_mesh.c \
  ../Common/write_mesh_ps.c

lowdim_sources =\
  ../Common/write_mesh_gmv.c

if HAVE_GPSKCA
common_fem_sources += ../Common/optimize_profile.c ../Common/optimize_profile.h
endif

AM_CPPFLAGS =\
 -I$(top_builddir)/include\
 -I$(srcdir)/../0d\
 -I$(srcdir)/../1d\
 -I$(srcdir)/../2d\
 -I$(srcdir)/../3d\
 -I$(srcdir)/../Common\
 -I$(top_srcdir)/alberta_util/src\
 -I$(top_builddir)/alberta_util/src\
 -I../Common \
 @OPENGL_INCLUDES@ @GLTOOLS_INCLUDES@ @OPENDX_INCLUDES@ @TIRPC_CPPFLAGS@ @OBSTACK_CPPFLAGS@

#
# gltools and opendx are implicitly disabled when ALBERTA_USE_GRAPHICS
# is false
#
gfxsources = ../Common/gfx-dummy.c # nothing
if HAVE_OPENGL
gfxsources += ../Common/graphXO.c
endif
if HAVE_GLTOOLS
gfxsources += ../Common/gltools.c
endif
if HAVE_OPENDX
gfxsources += ../Common/dxtools.c
endif

EXTRA_DIST =\
 ../Common/optimize_profile.c ../Common/optimize_profile.h\
 ../Common/graphXO.c ../Common/gltools.c ../Common/dxtools.c

DEFS = @DEFS@

$(ASSEMBLE_SRC): \
 ../Common/assemble_fcts.c.in ../Common/assemble_fcts.h.in \
 $(top_srcdir)/gen-assemble-fcts.sh
	make -C ../Common

$(ASSEMBLE_SRC:.c=.h): \
 ../Common/assemble_fcts.h.in $(top_srcdir)/gen-assemble-fcts.sh
	make -C ../Common

$(ASSEMBLE_BNDRY_SRC): \
 ../Common/assemble_bndry_fcts.c.in $(top_srcdir)/gen-assemble-fcts.sh
	make -C ../Common
