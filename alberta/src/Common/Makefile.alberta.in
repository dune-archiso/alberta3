# Hey, Emacs, we're -*- makefile -*- mode!

########################################################################
#   master Makefile, included by application Makefiles                 #
#   Daniel Koester                                                     #
########################################################################

########################################################################
#   Shell to use (for running libtool, e.g.)                           #
########################################################################

SHELL = @SHELL@

########################################################################
#   C and FORTRAN compilers and loader                                 #
########################################################################

CC = @CC@
FC = @F77@
LD = @CC@

########################################################################
#   prefix and exec_prefix                                             #
########################################################################

prefix = @prefix@
exec_prefix = @exec_prefix@

########################################################################
#   used libraries and define flags                                    #
########################################################################

BLAS_LIB = @BLAS_ALL_LIBS@
GPSKCA_LIB = @GPSKCA_ALL_LIBS@

ifndef ALBERTA_DEBUG
  ALBERTA_DEBUG=0
endif

ifndef ALBERTA_PROFILE
  ALBERTA_PROFILE=0
endif

ifndef ALBERTA_USE_GRAPHICS
  ALBERTA_USE_GRAPHICS=@ALBERTA_USE_GRAPHICS@
endif

DFLAGS =\
 -DDIM_OF_WORLD=$(DIM_OF_WORLD)\
 -DALBERTA_DEBUG=$(ALBERTA_DEBUG)\
 -DALBERTA_PROFILE=$(ALBERTA_PROFILE)\
 -DALBERTA_USE_GRAPHICS=$(ALBERTA_USE_GRAPHICS)

@HAVE_OPENGL_TRUE@DFLAGS += -DHAVE_LIBGL=1
@HAVE_GLTOOLS_TRUE@DFLAGS += -DHAVE_LIBGLTOOLS=1
@HAVE_OPENDX_TRUE@DFLAGS += -DHAVE_DXTOOLS=1

ifeq ($(ALBERTA_DEBUG), 1)
ALBERTA_EFENCE = @ALBERTA_EFENCE@
ALBERTA_DUMA = @ALBERTA_DUMA@
ifeq ($(ALBERRA_EFENCE), 1)
EFENCE_LIB = @EFENCE_ALL_LIBS@
endif
ifeq ($(ALBERRA_DUMA), 1)
DUMA_LIB = @DUMA_ALL_LIBS@
endif
UTIL_LIB = -l@PACKAGE@_utilities_debug
else
UTIL_LIB = -l@PACKAGE@_utilities
endif


ifeq ($(ALBERTA_USE_GRAPHICS),1)
EXTRA_LIBS = @GPSKCA_ALL_LIBS@ @GLTOOLS_LIBS@ @OPENDX_ALL_LIBS@ @OPENGL_LIBS@ @X_ALL_LIBS@ @LIBS@
else
EXTRA_LIBS = @GPSKCA_ALL_LIBS@ @LIBS@
endif

########################################################################
#   used libtool                                                       #
########################################################################

ifeq (@INSTALL_LIBTOOL@,1)
  LIBTOOL_LINK = @libexecdir@/@PACKAGE@-@VERSION@/libtool --mode=link
endif

########################################################################
#   compile commands                                                   #
########################################################################


.c.o:
	$(CC) $(DFLAGS) $(CPPFLAGS) $(CFLAGS) \
-I${prefix}/include -I$(ALBERTA_INCLUDE_PATH) -I$(ALBERTA_INCLUDE_PATH)/alberta -c $<

.f.o:
	$(FC) $(FFLAGS) -c $<

########################################################################
#   define default link commands                                       #
########################################################################

LINK = $(LIBTOOL_LINK) $(LD) $(CFLAGS) $(LDFLAGS) -o $@
F77LINK = $(LIBTOOL_LINK) $(F77) $(FFLAGS) $(LDFLAGS) -o $@

########################################################################
#   the ALBERTA library and libraries used by ALBERTA                    #
########################################################################

ALBERTA_LIB = -lalberta_fem_$(DIM_OF_WORLD)d -lalberta_$(DIM_OF_WORLD)d
ifeq ($(ALBERTA_PROFILE), 1)
ALBERTA_LIB = -lalberta_fem_$(DIM_OF_WORLD)d_profile -lalberta_$(DIM_OF_WORLD)d_profile
endif
ifeq ($(ALBERTA_DEBUG), 1)
ALBERTA_LIB = -lalberta_fem_$(DIM_OF_WORLD)d_debug -lalberta_$(DIM_OF_WORLD)d_debug
endif

ifeq ($(ALBERTA_USE_GRAPHICS),1)
ifeq ($(ALBERTA_DEBUG), 1)
ALBERTA_GFX_LIB = -lalberta_gfx_$(DIM_OF_WORLD)d_debug
else
ALBERTA_GFX_LIB = -lalberta_gfx_$(DIM_OF_WORLD)d
endif
endif

LIBS = -L$(ALBERTA_LIB_PATH) $(ALBERTA_LIB) $(ALBERTA_GFX_LIB) $(UTIL_LIB) $(BLAS_LIB) $(GPSKCA_LIB) $(EXTRA_LIBS) $(EFENCE_LIB) $(DUMA_LIB)

########################################################################
#   some often used targets                                            #
########################################################################

default: $(DEFAULT)

.PHONY: albertaclean albertacleano albertanew
albertaclean: albertacleano
	-rm -rf .libs

albertacleano:
	-rm -rf *.o *.lo

albertarealclean: albertaclean
	-rm -f *~

albertanew: cleano default

########################################################################
#   end of master makefile                                             #
########################################################################
