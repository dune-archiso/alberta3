/*--------------------------------------------------------------------------*/
/* ALBERTA:  an Adaptive multi Level finite element toolbox using           */
/*           Bisectioning refinement and Error control by Residual          */
/*           Techniques for scientific Applications                         */
/*                                                                          */
/* file:     numint.c                                                       */
/*                                                                          */
/* description:  quadrature formulas and routines for numerical quadrature  */
/*                                                                          */
/*--------------------------------------------------------------------------*/
/*                                                                          */
/*  authors:   Alfred Schmidt                                               */
/*             Zentrum fuer Technomathematik                                */
/*             Fachbereich 3 Mathematik/Informatik                          */
/*             Universitaet Bremen                                          */
/*             Bibliothekstr. 2                                             */
/*             D-28359 Bremen, Germany                                      */
/*                                                                          */
/*             Kunibert G. Siebert                                          */
/*             Institut fuer Mathematik                                     */
/*             Universitaet Augsburg                                        */
/*             Universitaetsstr. 14                                         */
/*             D-86159 Augsburg, Germany                                    */
/*                                                                          */
/*             Claus-Justus Heine                                           */
/*             Abteilung fuer Angewandte Mathematik                         */
/*             Universitaet Freiburg                                        */
/*             Hermann-Herder-Strasse 10                                    */
/*             79104 Freiburg, Germany                                      */
/*                                                                          */
/*  www.alberta-fem.de                                                      */
/*                                                                          */
/*  (c) by A. Schmidt and K.G. Siebert (1996-2003),                         */
/*         C.-J. Heine (2005-2007).                                         */
/*--------------------------------------------------------------------------*/

#include "alberta_intern.h"
#include "alberta.h"

#define  zero  0.0
#define  one   1.0
#define  half  0.5
#define  third 1.0/3.0
#define  quart 1.0/4.0

/* initializer for dim, codim and the number of the sub-simplex. All
 * tabulated quadrature rules have co-dim 0, quadratures for faces and
 * edges are generated dynamically (see get_wall_quadrature()).
 */
#define QR(NAME, DEG, DIM, NPT, L, W, MD)				\
  { NAME, DEG, DIM, 0, -1, NPT, NPT, L, W, MD, NULL, FILL_NOTHING }

/*--------------------------------------------------------------------------*/
/*  0d quadrature formulas using 1 barycentric coordinates                  */
/*--------------------------------------------------------------------------*/

static const REAL_B x_0d[1] = {INIT_BARY_0D(1.0)};
static const REAL   w_0d[1] = {1.0};

#define MAX_QUAD_DEG_0D 19

static QUAD_METADATA qmd_0d[MAX_QUAD_DEG_0D+1];

static const QUAD quad_0d[MAX_QUAD_DEG_0D+1] = {
  QR("0d",  0, 0, 1, x_0d, w_0d, &qmd_0d[0]),  /* P_0   */
  QR("0d",  1, 0, 1, x_0d, w_0d, &qmd_0d[1]),  /* P_1   */
  QR("0d",  2, 0, 1, x_0d, w_0d, &qmd_0d[2]),  /* P_2   */
  QR("0d",  3, 0, 1, x_0d, w_0d, &qmd_0d[3]),  /* P_3   */
  QR("0d",  4, 0, 1, x_0d, w_0d, &qmd_0d[4]),  /* P_4   */
  QR("0d",  5, 0, 1, x_0d, w_0d, &qmd_0d[5]),  /* P_5   */
  QR("0d",  6, 0, 1, x_0d, w_0d, &qmd_0d[6]),  /* P_6   */
  QR("0d",  7, 0, 1, x_0d, w_0d, &qmd_0d[7]),  /* P_7   */
  QR("0d",  8, 0, 1, x_0d, w_0d, &qmd_0d[8]),  /* P_8   */
  QR("0d",  9, 0, 1, x_0d, w_0d, &qmd_0d[9]),  /* P_9   */
  QR("0d", 10, 0, 1, x_0d, w_0d, &qmd_0d[10]), /* P_10  */
  QR("0d", 11, 0, 1, x_0d, w_0d, &qmd_0d[11]), /* P_11  */
  QR("0d", 12, 0, 1, x_0d, w_0d, &qmd_0d[12]), /* P_12  */
  QR("0d", 13, 0, 1, x_0d, w_0d, &qmd_0d[13]), /* P_13  */
  QR("0d", 14, 0, 1, x_0d, w_0d, &qmd_0d[14]), /* P_14  */
  QR("0d", 15, 0, 1, x_0d, w_0d, &qmd_0d[15]), /* P_15  */
  QR("0d", 16, 0, 1, x_0d, w_0d, &qmd_0d[16]), /* P_16  */
  QR("0d", 17, 0, 1, x_0d, w_0d, &qmd_0d[17]), /* P_17  */
  QR("0d", 18, 0, 1, x_0d, w_0d, &qmd_0d[18]), /* P_18  */
  QR("0d", 19, 0, 1, x_0d, w_0d, &qmd_0d[19]), /* P_19  */
};

/*--------------------------------------------------------------------------*/
/*  1d quadrature formulas using 2 barycentric coordinates                  */
/*--------------------------------------------------------------------------*/

#define MAX_QUAD_DEG_1D 19

#define  StdVol 1.0

/*--------------------------------------------------------------------------*/
/*  quadrature exact on P_1                                                 */
/*--------------------------------------------------------------------------*/

static const REAL_B x0_1d[1] = {INIT_BARY_1D(0.5, 0.5)};
static const REAL   w0_1d[1] = {StdVol*1.0};

/*--------------------------------------------------------------------------*/
/*  quadrature exact on P_3                                                 */
/*--------------------------------------------------------------------------*/

static const REAL_B x1_1d[2] = {
  INIT_BARY_1D(0.788675134594813, 0.211324865405187),
  INIT_BARY_1D(0.211324865405187, 0.788675134594813)
};
static const REAL  w1_1d[2] = {StdVol*0.5, StdVol*0.5};

/*--------------------------------------------------------------------------*/
/*  quadrature exact on P_5                                                 */
/*--------------------------------------------------------------------------*/

static const REAL_B x2_1d[3] = {
  INIT_BARY_1D(0.887298334620741, 0.112701665379259),
  INIT_BARY_1D(0.500000000000000, 0.500000000000000),
  INIT_BARY_1D(0.112701665379259, 0.887298334620741)
};
static const REAL  w2_1d[3] = {
  StdVol*0.277777777777778,
  StdVol*0.444444444444444,
  StdVol*0.277777777777778
};

/*--------------------------------------------------------------------------*/
/*  quadrature exact on P_7                                                 */
/*--------------------------------------------------------------------------*/

static const REAL_B x3_1d[4] = {
  INIT_BARY_1D(0.930568155797026, 0.069431844202973),
  INIT_BARY_1D(0.669990521792428, 0.330009478207572),
  INIT_BARY_1D(0.330009478207572, 0.669990521792428),
  INIT_BARY_1D(0.069431844202973, 0.930568155797026)
};
static const REAL  w3_1d[4] = {
  StdVol*0.173927422568727,
  StdVol*0.326072577431273,
  StdVol*0.326072577431273,
  StdVol*0.173927422568727
};

/*--------------------------------------------------------------------------*/
/*  quadrature exact on P_9                                                 */
/*--------------------------------------------------------------------------*/

static const REAL_B x4_1d[5] = {
  INIT_BARY_1D(0.953089922969332, 0.046910077030668),
  INIT_BARY_1D(0.769234655052841, 0.230765344947159),
  INIT_BARY_1D(0.500000000000000, 0.500000000000000),
  INIT_BARY_1D(0.230765344947159, 0.769234655052841),
  INIT_BARY_1D(0.046910077030668, 0.953089922969332)
};
static const REAL  w4_1d[5] = {
  StdVol*0.118463442528095,
  StdVol*0.239314335249683,
  StdVol*0.284444444444444,
  StdVol*0.239314335249683,
  StdVol*0.118463442528095
};

/*--------------------------------------------------------------------------*/
/*  quadrature exact on P_11                                                */
/*--------------------------------------------------------------------------*/

static const REAL_B x5_1d[6] = {
  INIT_BARY_1D(0.966234757101576, 0.033765242898424),
  INIT_BARY_1D(0.830604693233133, 0.169395306766867),
  INIT_BARY_1D(0.619309593041598, 0.380690406958402),
  INIT_BARY_1D(0.380690406958402, 0.619309593041598),
  INIT_BARY_1D(0.169395306766867, 0.830604693233133),
  INIT_BARY_1D(0.033765242898424, 0.966234757101576)
};
static const REAL  w5_1d[6] = {
  StdVol*0.085662246189585,
  StdVol*0.180380786524069,
  StdVol*0.233956967286345,
  StdVol*0.233956967286345,
  StdVol*0.180380786524069,
  StdVol*0.085662246189585
};

/*--------------------------------------------------------------------------*/
/*  quadrature exact on P_13                                                */
/*--------------------------------------------------------------------------*/

static const REAL_B x6_1d[7] = {
  INIT_BARY_1D(0.974553956171380, 0.025446043828620),
  INIT_BARY_1D(0.870765592799697, 0.129234407200303),
  INIT_BARY_1D(0.702922575688699, 0.297077424311301),
  INIT_BARY_1D(0.500000000000000, 0.500000000000000),
  INIT_BARY_1D(0.297077424311301, 0.702922575688699),
  INIT_BARY_1D(0.129234407200303, 0.870765592799697),
  INIT_BARY_1D(0.025446043828620, 0.974553956171380)
};
static const REAL  w6_1d[7] = {
  StdVol*0.064742483084435,
  StdVol*0.139852695744614,
  StdVol*0.190915025252559,
  StdVol*0.208979591836735,
  StdVol*0.190915025252559,
  StdVol*0.139852695744614,
  StdVol*0.064742483084435
};

/*--------------------------------------------------------------------------*/
/*  quadrature exact on P_15                                                */
/*--------------------------------------------------------------------------*/

static const REAL_B x7_1d[8] = {
  INIT_BARY_1D(0.980144928248768, 0.019855071751232),
  INIT_BARY_1D(0.898333238706813, 0.101666761293187),
  INIT_BARY_1D(0.762766204958164, 0.237233795041836),
  INIT_BARY_1D(0.591717321247825, 0.408282678752175),
  INIT_BARY_1D(0.408282678752175, 0.591717321247825),
  INIT_BARY_1D(0.237233795041836, 0.762766204958164),
  INIT_BARY_1D(0.101666761293187, 0.898333238706813),
  INIT_BARY_1D(0.019855071751232, 0.980144928248768)
};
static const REAL  w7_1d[8] = {
  StdVol*0.050614268145188,
  StdVol*0.111190517226687,
  StdVol*0.156853322938943,
  StdVol*0.181341891689181,
  StdVol*0.181341891689181,
  StdVol*0.156853322938943,
  StdVol*0.111190517226687,
  StdVol*0.050614268145188
};

/*--------------------------------------------------------------------------*/
/*  quadrature exact on P_17                                                */
/*--------------------------------------------------------------------------*/

static const REAL_B x8_1d[9] = {
  INIT_BARY_1D(0.984080119753813, 0.015919880246187),
  INIT_BARY_1D(0.918015553663318, 0.081984446336682),
  INIT_BARY_1D(0.806685716350295, 0.193314283649705),
  INIT_BARY_1D(0.662126711701905, 0.337873288298095),
  INIT_BARY_1D(0.500000000000000, 0.500000000000000),
  INIT_BARY_1D(0.337873288298095, 0.662126711701905),
  INIT_BARY_1D(0.193314283649705, 0.806685716350295),
  INIT_BARY_1D(0.081984446336682, 0.918015553663318),
  INIT_BARY_1D(0.015919880246187, 0.984080119753813)
};
static const REAL  w8_1d[9] = {
  StdVol*0.040637194180787,
  StdVol*0.090324080347429,
  StdVol*0.130305348201467,
  StdVol*0.156173538520001,
  StdVol*0.165119677500630,
  StdVol*0.156173538520001,
  StdVol*0.130305348201467,
  StdVol*0.090324080347429,
  StdVol*0.040637194180787
};

/*--------------------------------------------------------------------------*/
/*  quadrature exact on P_19                                                */
/*--------------------------------------------------------------------------*/

static const REAL_B x9_1d[10] = {
  INIT_BARY_1D(0.986953264258586, 0.013046735741414),
  INIT_BARY_1D(0.932531683344493, 0.067468316655508),
  INIT_BARY_1D(0.839704784149512, 0.160295215850488),
  INIT_BARY_1D(0.716697697064623, 0.283302302935377),
  INIT_BARY_1D(0.574437169490815, 0.425562830509185),
  INIT_BARY_1D(0.425562830509185, 0.574437169490815),
  INIT_BARY_1D(0.283302302935377, 0.716697697064623),
  INIT_BARY_1D(0.160295215850488, 0.839704784149512),
  INIT_BARY_1D(0.067468316655508, 0.932531683344493),
  INIT_BARY_1D(0.013046735741414, 0.986953264258586)
};
static const REAL  w9_1d[10] = {
  StdVol*0.033335672154344,
  StdVol*0.074725674575291,
  StdVol*0.109543181257991,
  StdVol*0.134633359654998,
  StdVol*0.147762112357376,
  StdVol*0.147762112357376,
  StdVol*0.134633359654998,
  StdVol*0.109543181257991,
  StdVol*0.074725674575291,
  StdVol*0.033335672154344
};

static QUAD_METADATA qmd_1d[MAX_QUAD_DEG_1D+1];

static const QUAD quad_1d[MAX_QUAD_DEG_1D+1] = {
  QR("1d-Gauss: P_1",   1, 1,  1, x0_1d, w0_1d, &qmd_1d[0]),  /* P_0  */
  QR("1d-Gauss: P_1",   1, 1,  1, x0_1d, w0_1d, &qmd_1d[1]),  /* P_1  */
  QR("1d-Gauss: P_3",   3, 1,  2, x1_1d, w1_1d, &qmd_1d[2]),  /* P_2  */
  QR("1d-Gauss: P_3",   3, 1,  2, x1_1d, w1_1d, &qmd_1d[3]),  /* P_3  */
  QR("1d-Gauss: P_5",   5, 1,  3, x2_1d, w2_1d, &qmd_1d[4]),  /* P_4  */
  QR("1d-Gauss: P_5",   5, 1,  3, x2_1d, w2_1d, &qmd_1d[5]),  /* P_5  */
  QR("1d-Gauss: P_7",   7, 1,  4, x3_1d, w3_1d, &qmd_1d[6]),  /* P_6  */
  QR("1d-Gauss: P_7",   7, 1,  4, x3_1d, w3_1d, &qmd_1d[7]),  /* P_7  */
  QR("1d-Gauss: P_9",   9, 1,  5, x4_1d, w4_1d, &qmd_1d[8]),  /* P_8  */
  QR("1d-Gauss: P_9",   9, 1,  5, x4_1d, w4_1d, &qmd_1d[9]),  /* P_9  */
  QR("1d-Gauss: P_11", 11, 1,  6, x5_1d, w5_1d, &qmd_1d[10]), /* P_10 */
  QR("1d-Gauss: P_11", 11, 1,  6, x5_1d, w5_1d, &qmd_1d[11]), /* P_11 */
  QR("1d-Gauss: P_13", 13, 1,  7, x6_1d, w6_1d, &qmd_1d[12]), /* P_12 */
  QR("1d-Gauss: P_13", 13, 1,  7, x6_1d, w6_1d, &qmd_1d[13]), /* P_13 */
  QR("1d-Gauss: P_15", 15, 1,  8, x7_1d, w7_1d, &qmd_1d[14]), /* P_14 */
  QR("1d-Gauss: P_15", 15, 1,  8, x7_1d, w7_1d, &qmd_1d[15]), /* P_15 */
  QR("1d-Gauss: P_17", 17, 1,  9, x8_1d, w8_1d, &qmd_1d[16]), /* P_16 */
  QR("1d-Gauss: P_17", 17, 1,  9, x8_1d, w8_1d, &qmd_1d[17]), /* P_17 */
  QR("1d-Gauss: P_19", 19, 1, 10, x9_1d, w9_1d, &qmd_1d[18]), /* P_18 */
  QR("1d-Gauss: P_19", 19, 1, 10, x9_1d, w9_1d, &qmd_1d[19]), /* P_19 */
};

#undef StdVol
/*--------------------------------------------------------------------------*/
/*--------------------------------------------------------------------------*/
/*--------------------------------------------------------------------------*/
/*  2d quadrature formulas using 3 barycentric coordinates                  */
/*--------------------------------------------------------------------------*/
/*--------------------------------------------------------------------------*/
/*--------------------------------------------------------------------------*/

#define CYCLE(c1, c2, c3)			\
  INIT_BARY_2D(c1, c2, c3),			\
    INIT_BARY_2D(c2, c3, c1),			\
    INIT_BARY_2D(c3, c1, c2)
#define ALL_COMB(c1, c2, c3) CYCLE(c1, c2, c3), CYCLE(c1, c3, c2)
#define W_CYCLE(w1)          w1, w1, w1
#define W_ALL_COMB(w1)       W_CYCLE(w1), W_CYCLE(w1)

#define MAX_QUAD_DEG_2D   17

#define StdVol 0.5

/* Rule structure for formulas from 
 * "Encyclopaedia of Cubature Formulas" at
 * http://www.cs.kuleuven.ac.be/~nines/research/ecf/
 *
 * BIG FAT NOTE: those quadrature formulas are already relative to the
 * reference simplex, so the weights MUST NOT be mutiplicated by
 * StdVol.
 * 
 * Index dim = 2
 *  1    (1,0,0)
 *  2	 (1/2,1/2,0)
 *  3	 (a,b,0)
 *  4	 (1/3,1/3,1/3)
 *  5	 (a,a,b)
 *  6	 (a,b,c)
 */

/* Generate nodes for index 1 */
#define CYCLE1_2D       CYCLE(1.0L, 0.0L, 0.0L)
#define CYCLE1_2D_W(w)  W_CYCLE(w)

/* Generate nodes for index 2 */
#define CYCLE2_2D       CYCLE(0.5L, 0.5L, 0.0L)
#define CYCLE2_2D_W(w)  W_CYCLE(w)

/* Generate nodes for index 3 */
#define CYCLE3_2D(a)    ALL_COMB(a, 1.0L - a, 0.0)
#define CYCLE3_2D_W(w)  W_ALL_COMB(w)

/* Generate nodes for index 4 */
#define CYCLE4_2D       INIT_BARY_2D(1.0L/3.0L, 1.0L/3.0L, 1.0L/3.0L)
#define CYCLE4_2D_W(w)  (w)

/* Generate nodes for index 5 */
#define CYCLE5_2D(a)    CYCLE(a, a, 1.0L - 2.0L*a)
#define CYCLE5_2D_W(w)  W_CYCLE(w)

/* Generate nodes for index 6 */
#define CYCLE6_2D(a, b) ALL_COMB(a, b, 1.0L-a-b)
#define CYCLE6_2D_W(w)  W_ALL_COMB(w)

/*--------------------------------------------------------------------------*/
/*  quadrature exact on P 1                                                 */
/*--------------------------------------------------------------------------*/

#define N1  1

#define c1  1.0/3.0
#define w1  StdVol*1.0

static const REAL_B x1_2d[N1] = {INIT_BARY_2D(c1, c1, c1)};
static const REAL   w1_2d[N1] = {w1};

#undef c1
#undef w1

/*--------------------------------------------------------------------------*/
/*  quadrature exact on P 2                                                 */
/* Stroud, A.H.: Approximate calculation of multiple integrals              */
/* Prentice-Hall Series in Automatic Computation. (1971)                    */
/* optimal number of points: 3, number of points: 3                         */
/* interior points, completly symmetric in barycentric coordinates          */
/*--------------------------------------------------------------------------*/

#define N2  3

#define c1  2.0/3.0
#define c2  1.0/6.0
#define w1  StdVol/3.0

static const REAL_B x2_2d[3] = {CYCLE(c1, c2, c2)};
static const REAL   w2_2d[3] = {W_CYCLE(w1)};

#undef c1
#undef c2
#undef w1

/*--------------------------------------------------------------------------*/
/*  quadrature exact on P_3                                                 */
/*--------------------------------------------------------------------------*/

#define N3  6

#define c1  0.0
#define c2  1.0/2.0
#define c3  4.0/6.0
#define c4  1.0/6.0
#define w1  StdVol*1.0/30.0
#define w2  StdVol*9.0/30.0

static const REAL_B x3_2d[N3] = {CYCLE(c1, c2, c2), CYCLE(c3, c4, c4)};
static const REAL  w3_2d[N3]  = {W_CYCLE(w1), W_CYCLE(w2)};

#undef c1
#undef c2
#undef c3
#undef c4
#undef w1
#undef w2

/*--------------------------------------------------------------------------*/
/*  quadrature exact on P 4                                                 */
/* Dunavant, D.A.: High degree efficient symmetrical Gaussian quadrature    */
/* rules for the triangle. Int. J. Numer. Methods Eng. 21, 1129-1148 (1985) */
/* nearly optimal number of (interior) points, positive wheights  (PI)      */
/* number of points: 6, optimal number of points: 6                         */
/*--------------------------------------------------------------------------*/

#define N4  6

#define c1  0.816847572980459
#define c2  0.091576213509771
#define c3  0.108103018168070
#define c4  0.445948490915965
#define w1  StdVol*0.109951743655322
#define w2  StdVol*0.223381589678011

static const REAL_B x4_2d[N4] = {CYCLE(c1, c2, c2), CYCLE(c3, c4, c4)};
static const REAL   w4_2d[N4] = {W_CYCLE(w1), W_CYCLE(w2)};

#undef c1
#undef c2
#undef c3
#undef c4
#undef w1
#undef w2


/*--------------------------------------------------------------------------*/
/*  quadrature exact on P 5                                                 */
/*--------------------------------------------------------------------------*/

#if 0
/*--------------------------------------------------------------------------*/
/* Stroud, A.H.: Approximate calculation of multiple integrals              */
/* Prentice-Hall Series in Automatic Computation. (1971)                    */
/* number of points: 7, optimal number of points: 7                         */
/*--------------------------------------------------------------------------*/

# define N5   7

# define c1   1.0/3.0
# define c2   0.0
# define c3   1.0/2.0
# define c4   1.0
# define c5   0.0
# define w1   StdVol*0.45
# define w2   StdVol*4.0/30.0
# define w3   StdVol*0.05
#else

/*--------------------------------------------------------------------------*/
/* Dunavant, D.A.: High degree efficient symmetrical Gaussian quadrature    */
/* rules for the triangle. Int. J. Numer. Methods Eng. 21, 1129-1148 (1985) */
/* nealy optimal number of (interior) points, positive wheights  (PI)       */
/* number of points: 7, optimal number of points: 7                         */
/*--------------------------------------------------------------------------*/

# define N5   7

# define c1   1.0/3.0
# define c2   0.797426985353087
# define c3   0.101286507323456
# define c4   0.059715871789770
# define c5   0.470142064105115
# define w1   StdVol*0.225000000000000
# define w2   StdVol*0.125939180544827
# define w3   StdVol*0.132394152788506
#endif

static const REAL_B x5_2d[N5] = {
  INIT_BARY_2D(c1, c1, c1),
  CYCLE(c2, c3, c3),
  CYCLE(c4, c5, c5)
};
static const REAL  w5_2d[N5] = {w1, W_CYCLE(w2), W_CYCLE(w3)};

#undef c1
#undef c2
#undef c3
#undef c4
#undef c5
#undef w1
#undef w2
#undef w3

/*--------------------------------------------------------------------------*/
/*  quadrature exact on P 6: only 12 point rule available in the literature */
/*  ->  use quadrature exact on P 7 with 12 points                          */
/*--------------------------------------------------------------------------*/

/*--------------------------------------------------------------------------*/
/*  quadrature exact on P 7                                                 */
/*--------------------------------------------------------------------------*/

#if 1
/*--------------------------------------------------------------------------*/
/* Gatermann, Karin: The construction of symmetric cubature formulas for    */
/* the square and the triangle. Computing 40, No.3, 229-240 (1988)          */
/* optimal number of points: 12, number of points: 12                       */
/* only interior points, not completly symmetric in barycentric coordinates */
/*--------------------------------------------------------------------------*/

# define N7   12

# define c1   0.06238226509439084
# define c2   0.06751786707392436
# define c3   0.87009986783168480
# define c4   0.05522545665692000
# define c5   0.32150249385201560
# define c6   0.62327204949106440
# define c7   0.03432430294509488
# define c8   0.66094919618679800
# define c9   0.30472650086810720
# define c10  0.5158423343536001
# define c11  0.2777161669764050
# define c12  0.2064414986699949
# define w1   0.02651702815743450
# define w2   0.04388140871444811
# define w3   0.02877504278497528
# define w4   0.06749318700980879

static const REAL_B x7_2d[N7] = {
  CYCLE(c1, c2, c3), CYCLE(c4, c5, c6),
  CYCLE(c7, c8, c9), CYCLE(c10, c11, c12)
};
static const REAL  w7_2d[N7] = {
  W_CYCLE(w1), W_CYCLE(w2),
  W_CYCLE(w3), W_CYCLE(w4)
};

# undef c1
# undef c2
# undef c3
# undef c4
# undef c5
# undef c6
# undef c7
# undef c8
# undef c9
# undef c10
# undef c11
# undef c12
# undef w1
# undef w2
# undef w3
# undef w4

#else

/*--------------------------------------------------------------------------*/
/* Stroud, A.H.: Approximate calculation of multiple integrals              */
/* Prentice-Hall Series in Automatic Computation. (1971)                    */
/* optimal number of points: 12, number of points: 16                       */
/* only interior points, not symmetric in barycentric coordinates           */
/*--------------------------------------------------------------------------*/

# define N7  16

static const REAL_B x7_2d[N7] = {
  {0.0571041961, 0.065466992667427, 0.877428811232573, 0.0},
  {0.2768430136, 0.050210121765552, 0.672946864634448, 0.0},
  {0.5835904324, 0.028912083388173, 0.387497484211827, 0.0},
  {0.8602401357, 0.009703784843971, 0.130056079456029, 0.0},
  {0.0571041961, 0.311164552242009, 0.631731251657992, 0.0},
  {0.2768430136, 0.238648659738548, 0.484508326661452, 0.0},
  {0.5835904324, 0.137419104121164, 0.278990463478836, 0.0},
  {0.8602401357, 0.046122079890946, 0.093637784409054, 0.0},
  {0.0571041961, 0.631731251657991, 0.311164552242009, 0.0},
  {0.2768430136, 0.484508326661451, 0.238648659738549, 0.0},
  {0.5835904324, 0.278990463478836, 0.137419104121164, 0.0},
  {0.8602401357, 0.093637784409054, 0.046122079890946, 0.0},
  {0.0571041961, 0.877428809346781, 0.065466994553219, 0.0},
  {0.2768430136, 0.672946863188134, 0.050210123211866, 0.0},
  {0.5835904324, 0.387497483379007, 0.028912084220993, 0.0},
  {0.8602401357, 0.130056079176509, 0.009703785123491, 0.0}
};
static const REAL  w7_2d[N7] = {
  StdVol*0.047136736384287,
  StdVol*0.070776135805325,
  StdVol*0.045168098569998,
  StdVol*0.010846451805605,
  StdVol*0.088370177015713,
  StdVol*0.132688432194675,
  StdVol*0.084679449030002,
  StdVol*0.020334519094395,
  StdVol*0.088370177015713,
  StdVol*0.132688432194675,
  StdVol*0.084679449030002,
  StdVol*0.020334519094395,
  StdVol*0.047136736384287,
  StdVol*0.070776135805325,
  StdVol*0.045168098569998,
  StdVol*0.010846451805605
};

#endif


/*--------------------------------------------------------------------------*/
/*  quadrature exact on P 8                                                 */
/* Dunavant, D.A.: High degree efficient symmetrical Gaussian quadrature    */
/* rules for the triangle. Int. J. Numer. Methods Eng. 21, 1129-1148 (1985) */
/* nealy optimal number of (interior) points, positive wheights  (PI)       */
/* number of points: 16, optimal number of points: 15                       */
/* only interior points, completly symmetric in barycentric coordinates     */
/*--------------------------------------------------------------------------*/

#define N8  16

#define c1   1.0/3.0
#define c2   0.081414823414554
#define c3   0.459292588292723
#define c4   0.658861384496480
#define c5   0.170569307751760
#define c6   0.898905543365938
#define c7   0.050547228317031
#define c8   0.008394777409958
#define c9   0.263112829634638
#define c10  0.728492392955404
#define w1   StdVol*0.144315607677787
#define w2   StdVol*0.095091634267285
#define w3   StdVol*0.103217370534718
#define w4   StdVol*0.032458497623198
#define w5   StdVol*0.027230314174435

static const REAL_B x8_2d[N8] = {
  INIT_BARY_2D(c1, c1, c1),
  CYCLE(c2, c3, c3),
  CYCLE(c4, c5, c5),
  CYCLE(c6, c7, c7),
  ALL_COMB(c8, c9, c10)
};
static const REAL  w8_2d[N8] = {
  w1, W_CYCLE(w2), W_CYCLE(w3),
  W_CYCLE(w4), W_ALL_COMB(w5)
};

#undef c1
#undef c2
#undef c3
#undef c4
#undef c5
#undef c6
#undef c7
#undef c8
#undef c9
#undef c10
#undef w1
#undef w2
#undef w3
#undef w4
#undef w5

/*--------------------------------------------------------------------------*/
/*  quadrature exact on P 9                                                 */
/* Dunavant, D.A.: High degree efficient symmetrical Gaussian quadrature    */
/* rules for the triangle. Int. J. Numer. Methods Eng. 21, 1129-1148 (1985) */
/* nealy optimal number of (interior) points, positive wheights  (PI)       */
/* optimal number of points: ?, number of points: 19                        */
/* only interior points, completly symmetric in barycentric coordinates     */
/*--------------------------------------------------------------------------*/

#define N9  19

#define c1   1.0/3.0
#define c2   0.020634961602525
#define c3   0.489682519198738
#define c4   0.125820817014127
#define c5   0.437089591492937
#define c6   0.623592928761935
#define c7   0.188203535619033
#define c8   0.910540973211095
#define c9   0.044729513394453
#define c10  0.036838412054736
#define c11  0.221962989160766
#define c12  0.741198598784498
#define w1   StdVol*0.097135796282799
#define w2   StdVol*0.031334700227139
#define w3   StdVol*0.077827541004774
#define w4   StdVol*0.079647738927210
#define w5   StdVol*0.025577675658698
#define w6   StdVol*0.043283539377289

static const REAL_B x9_2d[N9] = {
  INIT_BARY_2D(c1, c1, c1),
  CYCLE(c2, c3, c3),
  CYCLE(c4, c5, c5),
  CYCLE(c6, c7, c7),
  CYCLE(c8, c9, c9),
  ALL_COMB(c10, c11, c12)
};
static const REAL  w9_2d[N9] = {
  w1,
  W_CYCLE(w2),
  W_CYCLE(w3),
  W_CYCLE(w4),
  W_CYCLE(w5),
  W_ALL_COMB(w6)
};

#undef c1
#undef c2
#undef c3
#undef c4
#undef c5
#undef c6
#undef c7
#undef c8
#undef c9
#undef c10
#undef c11
#undef c12
#undef w1
#undef w2
#undef w3
#undef w4
#undef w5
#undef w6

/*--------------------------------------------------------------------------*/
/*  quadrature exact on P 10                                                */
/* Dunavant, D.A.: High degree efficient symmetrical Gaussian quadrature    */
/* rules for the triangle. Int. J. Numer. Methods Eng. 21, 1129-1148 (1985) */
/* nealy optimal number of (interior) points, positive wheights  (PI)       */
/* optimal number of points: ?, number of points: 25                        */
/* only interior points, completly symmetric in barycentric coordinates     */
/*--------------------------------------------------------------------------*/

#define N10 25

#define c1   1.0/3.0

#define c2   0.028844733232685
#define c3   0.485577633383657

#define c4   0.781036849029926
#define c5   0.109481575485037

#define c6   0.141707219414880
#define c7   0.307939838764121
#define c8   0.550352941820999

#define c9   0.025003534762686
#define c10  0.246672560639903
#define c11  0.728323904597411

#define c12  0.009540815400299
#define c13  0.066803251012200
#define c14  0.923655933587500

#define w1   StdVol*0.090817990382754
#define w2   StdVol*0.036725957756467
#define w3   StdVol*0.045321059435528
#define w4   StdVol*0.072757916845420
#define w5   StdVol*0.028327242531057
#define w6   StdVol*0.009421666963733

static const REAL_B x10_2d[N10] = {
  INIT_BARY_2D(c1, c1, c1),
  CYCLE(c2, c3, c3),
  CYCLE(c4, c5, c5),
  ALL_COMB(c6, c7, c8),
  ALL_COMB(c9, c10, c11),
  ALL_COMB(c12, c13, c14)
};
static const REAL  w10_2d[N10] = {
  w1,
  W_CYCLE(w2),
  W_CYCLE(w3),
  W_ALL_COMB(w4),
  W_ALL_COMB(w5),
  W_ALL_COMB(w6)
};

#undef c1
#undef c2
#undef c3
#undef c4
#undef c5
#undef c6
#undef c7
#undef c8
#undef c9
#undef c10
#undef c11
#undef c12
#undef c13
#undef c14
#undef w1
#undef w2
#undef w3
#undef w4
#undef w5
#undef w6

#if 0
/* Replaced by the 28-point formula from 
 *
 * "Encyclopaedia of Cubature Formulas" at
 * http://www.cs.kuleuven.ac.be/~nines/research/ecf/
 *
 * which really has only interior points.
 */

/*--------------------------------------------------------------------------*/
/*  quadrature exact on P 11                                                */
/* Dunavant, D.A.: High degree efficient symmetrical Gaussian quadrature    */
/* rules for the triangle. Int. J. Numer. Methods Eng. 21, 1129-1148 (1985) */
/* nealy optimal number of (interior) points, positive wheights  (PI)       */
/* optimal number of points: ?, number of points: 27                        */
/* only interior points, completly symmetric in barycentric coordinates     */
/*--------------------------------------------------------------------------*/

#define N11 27

#define c1  -0.069222096541517
#define c2   0.534611048270758

#define c3   0.202061394068290
#define c4   0.398969302965855

#define c5   0.593380199137435
#define c6   0.203309900431282

#define c7   0.761298175434837
#define c8   0.119350912282581

#define c9   0.935270103777448
#define c10  0.032364948111276

#define c11  0.050178138310495
#define c12  0.356620648261293
#define c13  0.593201213428213

#define c14  0.021022016536166
#define c15  0.171488980304042
#define c16  0.807489003159792

#define w1   StdVol*0.000927006328961
#define w2   StdVol*0.077149534914813
#define w3   StdVol*0.059322977380774
#define w4   StdVol*0.036184540503418
#define w5   StdVol*0.013659731002678
#define w6   StdVol*0.052337111962204
#define w7   StdVol*0.020707659639141

static const REAL_B x11_2d[N11] = {
  CYCLE(c1, c2, c2),
  CYCLE(c3, c4, c4),
  CYCLE(c5, c6, c6),
  CYCLE(c7, c8, c8),
  CYCLE(c9, c10, c10),
  ALL_COMB(c11, c12, c13),
  ALL_COMB(c14, c15, c16)
};
static const REAL  w11_2d[N11] = {
  W_CYCLE(w1),
  W_CYCLE(w2),
  W_CYCLE(w3),
  W_CYCLE(w4),
  W_CYCLE(w5),
  W_ALL_COMB(w6),
  W_ALL_COMB(w7)
};

#undef c1
#undef c2
#undef c3
#undef c4
#undef c5
#undef c6
#undef c7
#undef c8
#undef c9
#undef c10
#undef c11
#undef c12
#undef c13
#undef c14
#undef c15
#undef c16
#undef w1
#undef w2
#undef w3
#undef w4
#undef w5
#undef w6
#undef w7

#else

/* Quadrature exact on P 11.
 *
 * after
 *
 * J.N. Lyness and D. Jespersen, Moderate degree symmetric quadrature
 * rules for the triangle, J. Inst. Math. Appl.  15 (1975), 19--32.
 *
 * Copied from
 *
 * "Encyclopaedia of Cubature Formulas" at
 * http://www.cs.kuleuven.ac.be/~nines/research/ecf/
 *
 */

#define N11 28

#define c1 0.858870281282636704039173938058347L
#define c2 0.141129718717363295960826061941652L
#define w1 3.68119189165027713212944752369032e-3L

#define c3 0.333333333333333333333333333333333L
#define c4 0.333333333333333333333333333333333L
#define w2 0.0439886505811161193990465846607278L

#define c5 0.0259891409282873952600324854988407L
#define c6 0.0259891409282873952600324854988407L
#define w3 4.37215577686801152475821439991262e-3L

#define c7 0.0942875026479224956305697762754049L
#define c8 0.0942875026479224956305697762754049L
#define w4 0.0190407859969674687575121697178070L

#define c9  0.494636775017213813741632602306436L
#define c10 0.494636775017213813741632602306436L
#define w5  9.42772402806564602923839129555767e-3L

#define c11 0.207343382614511333452934024112966L
#define c12 0.207343382614511333452934024112966L
#define w6  0.0360798487723697630620149942932315L

#define c13 0.438907805700492095061065381636127L
#define c14 0.438907805700492095061065381636127L
#define w7  0.0346645693527679499208828254519072L

#define c15 0.677937654882590401542126141188747L
#define c16 0.0448416775891304433090523914688007L
#define w8  0.0205281577146442833208261574536469L

static const REAL_B x11_2d[N11] = {
  CYCLE3_2D(c1),
  CYCLE4_2D,
  CYCLE5_2D(c5),
  CYCLE5_2D(c7),
  CYCLE5_2D(c9),
  CYCLE5_2D(c11),
  CYCLE5_2D(c13),
  CYCLE6_2D(c15, c16)
};
static const REAL  w11_2d[N11] = {
  CYCLE3_2D_W(w1),
  CYCLE4_2D_W(w2),
  CYCLE5_2D_W(w3),
  CYCLE5_2D_W(w4),
  CYCLE5_2D_W(w5),
  CYCLE5_2D_W(w6),
  CYCLE5_2D_W(w7),
  CYCLE6_2D_W(w8)
};

#undef c1
#undef c2
#undef c3
#undef c4
#undef c5
#undef c6
#undef c7
#undef c8
#undef c9
#undef c10
#undef c11
#undef c12
#undef c13
#undef c14
#undef c15
#undef c16
#undef w1
#undef w2
#undef w3
#undef w4
#undef w5
#undef w6
#undef w7
#undef w8

#endif

/*--------------------------------------------------------------------------*/
/*  quadrature exact on P 12                                                */
/* Dunavant, D.A.: High degree efficient symmetrical Gaussian quadrature    */
/* rules for the triangle. Int. J. Numer. Methods Eng. 21, 1129-1148 (1985) */
/* nealy optimal number of (interior) points, positive wheights  (PI)       */
/* optimal number of points: 2, number of points: 25                        */
/* only interior points, completly symmetric in barycentric coordinates     */
/*--------------------------------------------------------------------------*/

#define N12 33

#define c1   0.023565220452390
#define c2   0.488217389773805

#define c3   0.120551215411079
#define c4   0.439724392294460

#define c5   0.457579229975768
#define c6   0.271210385012116

#define c7   0.744847708916828
#define c8   0.127576145541586

#define c9   0.957365299093579
#define c10  0.021317350453210

#define c11  0.115343494534698
#define c12  0.275713269685514
#define c13  0.608943235779788

#define c14  0.022838332222257
#define c15  0.281325580989940
#define c16  0.695836086787803

#define c17  0.025734050548330
#define c18  0.116251915907597
#define c19  0.858014033544073

#define w1   StdVol*0.025731066440455
#define w2   StdVol*0.043692544538038
#define w3   StdVol*0.062858224217885
#define w4   StdVol*0.034796112930709
#define w5   StdVol*0.006166261051559
#define w6   StdVol*0.040371557766381
#define w7   StdVol*0.022356773202303
#define w8   StdVol*0.017316231108659

static const REAL_B x12_2d[N12] = {
  CYCLE(c1, c2, c2),
  CYCLE(c3, c4, c4),
  CYCLE(c5, c6, c6),
  CYCLE(c7, c8, c8),
  CYCLE(c9, c10, c10),
  ALL_COMB(c11, c12, c13),
  ALL_COMB(c14, c15, c16),
  ALL_COMB(c17, c18, c19)
};
static const REAL  w12_2d[N12] = {
  W_CYCLE(w1),
  W_CYCLE(w2),
  W_CYCLE(w3),
  W_CYCLE(w4),
  W_CYCLE(w5),
  W_ALL_COMB(w6),
  W_ALL_COMB(w7),
  W_ALL_COMB(w8)
};

#undef c1
#undef c2
#undef c3
#undef c4
#undef c5
#undef c6
#undef c7
#undef c8
#undef c9
#undef c10
#undef c11
#undef c12
#undef c13
#undef c14
#undef c15
#undef c16
#undef c17
#undef c18
#undef c19
#undef w1
#undef w2
#undef w3
#undef w4
#undef w5
#undef w6
#undef w7
#undef w8

/* Quadrature exact on P 13.
 *
 * after
 *
 *     J. Berntsen and T.O. Espelid, Degree 13 symmetric quadrature
 *     rules for the triangle, Reports in Informatics 44, Dept. of
 *     Informatics, University of Bergen, 1990.
 *
 *
 * Copied from
 *
 * "Encyclopaedia of Cubature Formulas" at
 * http://www.cs.kuleuven.ac.be/~nines/research/ecf/
 *
 */

#define N13 37

#define c1 0.5L
#define w1 2.67845189554543044455908674650066e-3L

#define c2 1.0L/3.0L
#define w2 0.0293480398063595158995969648597808L

#define c3 0.0246071886432302181878499494124643L
#define c4 0.0246071886432302181878499494124643L
#define w3 3.92538414805004016372590903990464e-3L

#define c5 0.420308753101194683716920537182100L
#define c6 0.420308753101194683716920537182100L
#define w4 0.0253344765879434817105476355306468L

#define c7 0.227900255506160619646298948153592L
#define c8 0.227900255506160619646298948153592L
#define w5 0.0250401630452545330803738542916538L

#define c9  0.116213058883517905247155321839271L
#define c10 0.116213058883517905247155321839271L
#define w6  0.0158235572961491595176634480481793L

#define c11 0.476602980049079152951254215211496L
#define c12 0.476602980049079152951254215211496L
#define w7  0.0157462815379843978450278590138683L

#define c13 0.851775587145410469734660003794168L
#define c14 0.0227978945382486125477207592747430L
#define w8  7.90126610763037567956187298486575e-3L

#define c15 0.692797317566660854594116289398433L
#define c16 0.0162757709910885409437036075960413L
#define w9  7.99081889046420266145965132482933e-3L

#define c17 0.637955883864209538412552782122039L
#define c18 0.0897330604516053590796290561145196L
#define w10 0.0182757511120486476280967518782978L

static const REAL_B x13_2d[N13] = {
  CYCLE2_2D,
  CYCLE4_2D,
  CYCLE5_2D(c3),
  CYCLE5_2D(c5),
  CYCLE5_2D(c7),
  CYCLE5_2D(c9),
  CYCLE5_2D(c11),
  CYCLE6_2D(c13, c14),
  CYCLE6_2D(c15, c16),
  CYCLE6_2D(c17, c18),
};

static const REAL  w13_2d[N13] = {
  CYCLE2_2D_W(w1),
  CYCLE4_2D_W(w2),
  CYCLE5_2D_W(w3),
  CYCLE5_2D_W(w4),
  CYCLE5_2D_W(w5),
  CYCLE5_2D_W(w6),
  CYCLE5_2D_W(w7),
  CYCLE6_2D_W(w8),
  CYCLE6_2D_W(w9),
  CYCLE6_2D_W(w10),
};

#undef c1
#undef c2
#undef c3
#undef c4
#undef c5
#undef c6
#undef c7
#undef c8
#undef c9
#undef c10
#undef c11
#undef c12
#undef c13
#undef c14
#undef c15
#undef c16
#undef c17
#undef c18
#undef w1
#undef w2
#undef w3
#undef w4
#undef w5
#undef w6
#undef w7
#undef w8
#undef w9
#undef w10

/*--------------------------------------------------------------------------*/
/*  quadrature exact on P 17                                                */
/* Dunavant, D.A.: High degree efficient symmetrical Gaussian quadrature    */
/* rules for the triangle. Int. J. Numer. Methods Eng. 21, 1129-1148 (1985) */
/* nealy optimal number of (interior) points, positive wheights  (PI)       */
/* optimal number of points: ?, number of points: 61                        */
/* only interior points, completly symmetric in barycentric coordinates     */
/*--------------------------------------------------------------------------*/

#define N17 61

#define c1   1.0/3.0

#define c2   0.005658918886452
#define c3   0.497170540556774

#define c4   0.035647354750751
#define c5   0.482176322624625

#define c6   0.099520061958437
#define c7   0.450239969020782

#define c8   0.199467521245206
#define c9   0.400266239377397

#define c10  0.495717464058095
#define c11  0.252141267970953

#define c12  0.675905990683077
#define c13  0.162047004658461

#define c14  0.848248235478508
#define c15  0.075875882260746

#define c16  0.968690546064356
#define c17  0.015654726967822

#define c18  0.010186928826919
#define c19  0.334319867363658
#define c20  0.655493203809423

#define c21  0.135440871671036
#define c22  0.292221537796944
#define c23  0.572337590532020

#define c24  0.054423924290583
#define c25  0.319574885423190
#define c26  0.626001190286228

#define c27  0.012868560833637
#define c28  0.190704224192292
#define c29  0.796427214974071

#define c30  0.067165782413524
#define c31  0.180483211648746
#define c32  0.752351005937729

#define c33  0.014663182224828
#define c34  0.080711313679564
#define c35  0.904625504095608

#define w1   StdVol*0.033437199290803
#define w2   StdVol*0.005093415440507
#define w3   StdVol*0.014670864527638
#define w4   StdVol*0.024350878353672
#define w5   StdVol*0.031107550868969
#define w6   StdVol*0.031257111218620
#define w7   StdVol*0.024815654339665
#define w8   StdVol*0.014056073070557
#define w9   StdVol*0.003194676173779
#define w10  StdVol*0.008119655318993
#define w11  StdVol*0.026805742283163
#define w12  StdVol*0.018459993210822
#define w13  StdVol*0.008476868534328
#define w14  StdVol*0.018292796770025
#define w15  StdVol*0.006665632004165

static const REAL_B x17_2d[N17] = {
  INIT_BARY_2D(c1, c1, c1),
  CYCLE(c2, c3, c3),
  CYCLE(c4, c5, c5),
  CYCLE(c6, c7, c7),
  CYCLE(c8, c9, c9),
  CYCLE(c10, c11, c11),
  CYCLE(c12, c13, c13),
  CYCLE(c14, c15, c15),
  CYCLE(c16, c17, c17),
  ALL_COMB(c18, c19, c20),
  ALL_COMB(c21, c22, c23),
  ALL_COMB(c24, c25, c26),
  ALL_COMB(c27, c28, c29),
  ALL_COMB(c30, c31, c32),
  ALL_COMB(c33, c34, c35)
};
static const REAL  w17_2d[N17] = {
  w1,
  W_CYCLE(w2),
  W_CYCLE(w3),
  W_CYCLE(w4),
  W_CYCLE(w5),
  W_CYCLE(w6),
  W_CYCLE(w7),
  W_CYCLE(w8),
  W_CYCLE(w9),
  W_ALL_COMB(w10),
  W_ALL_COMB(w11),
  W_ALL_COMB(w12),
  W_ALL_COMB(w13),
  W_ALL_COMB(w14),
  W_ALL_COMB(w15)
};

#undef c1
#undef c2
#undef c3
#undef c4
#undef c5
#undef c6
#undef c7
#undef c8
#undef c9
#undef c10
#undef c11
#undef c12
#undef c13
#undef c14
#undef c15
#undef c16
#undef c17
#undef c18
#undef c19
#undef c20
#undef c21
#undef c22
#undef c23
#undef c24
#undef c25
#undef c26
#undef c27
#undef c28
#undef c29
#undef c30
#undef c31
#undef c32
#undef c33
#undef c34
#undef c35

#undef w1
#undef w2
#undef w3
#undef w4
#undef w5
#undef w6
#undef w7
#undef w8
#undef w9
#undef w10
#undef w11
#undef w12
#undef w13
#undef w14
#undef w15

static QUAD_METADATA qmd_2d[MAX_QUAD_DEG_2D+1];

static const QUAD quad_2d[MAX_QUAD_DEG_2D+1] = {
  QR("2d-P_1",              1, 2,  N1,  x1_2d,  w1_2d,  &qmd_2d[0]), /* P 0  */
  QR("2d-P_1",              1, 2,  N1,  x1_2d,  w1_2d,  &qmd_2d[1]), /* P 1  */
  QR("2d  Stroud: P_2",     2, 2,  N2,  x2_2d,  w2_2d,  &qmd_2d[2]), /* P 2  */
  QR("2d  Stroud: P_3",     3, 2,  N3,  x3_2d,  w3_2d,  &qmd_2d[3]), /* P 3  */
  QR("2d  Dunavant: P_4",   4, 2,  N4,  x4_2d,  w4_2d,  &qmd_2d[4]), /* P 4  */
  QR("2d  Dunavant: P_5",   5, 2,  N5,  x5_2d,  w5_2d,  &qmd_2d[5]), /* P 5  */
  QR("2d  Gattermann: P_7", 7, 2,  N7,  x7_2d,  w7_2d,  &qmd_2d[6]), /* P 6  */
  QR("2d  Gattermann: P_7", 7, 2,  N7,  x7_2d,  w7_2d,  &qmd_2d[7]), /* P 7  */
  QR("2d  Dunavant: P_8",   8, 2,  N8,  x8_2d,  w8_2d,  &qmd_2d[8]), /* P 8  */
  QR("2d  Dunavant: P_9",   9, 2,  N9,  x9_2d,  w9_2d,  &qmd_2d[9]), /* P 9  */
  QR("2d  Dunavant: P_10", 10, 2, N10, x10_2d, w10_2d, &qmd_2d[10]), /* P10 */
  QR("2d  Lyness-Jesperson: P_11",
                           11, 2, N11, x11_2d, w11_2d, &qmd_2d[11]), /* P11 */
  QR("2d  Dunavant: P_12", 12, 2, N12, x12_2d, w12_2d, &qmd_2d[12]), /* P12 */
  QR("2d  Berntsen/Espelid: P_13",
                           13, 2, N13, x13_2d, w13_2d, &qmd_2d[13]), /* P13 */
  QR("2d  Dunavant: P_17", 17, 2, N17, x17_2d, w17_2d, &qmd_2d[14]), /* P14 */
  QR("2d  Dunavant: P_17", 17, 2, N17, x17_2d, w17_2d, &qmd_2d[15]), /* P15 */
  QR("2d  Dunavant: P_17", 17, 2, N17, x17_2d, w17_2d, &qmd_2d[16]), /* P16 */
  QR("2d  Dunavant: P_17", 17, 2, N17, x17_2d, w17_2d, &qmd_2d[17]), /* P17 */
};

#define N_QUAD_POINTS_MAX_2D 61

#undef StdVol
#undef N1
#undef N2
#undef N3
#undef N4
#undef N5
#undef N6
#undef N7
#undef N8
#undef N9
#undef N10
#undef N11
#undef N12
#undef N17

/*--------------------------------------------------------------------------*/
/*  3d quadrature formulas using 4 barycentric coordinates                  */
/*--------------------------------------------------------------------------*/

#define StdVol (1.0L/6.0L)

#define MAX_QUAD_DEG_3D     6
#define MAX_DYN_QUAD_DEG_3d 13 /* conical product rule */

/* Permutations of generators of quadrature rules */

/* Rule structure for formulas from 
 * "Encyclopaedia of Cubature Formulas" at
 * http://www.cs.kuleuven.ac.be/~nines/research/ecf/
 *
 * BIG FAT NOTE: those quadrature formulas are already relative to the
 * reference simplex, so the weights MUST NOT be mutiplicated by
 * StdVol.
 * 
 * Index dim = 3
 *  1    (1,0,0,0)
 *  2	 (1/2,1/2,0,0)
 *  3	 (a,b,0,0)
 *  4	 (1/3,1/3,1/3,0)
 *  5	 (a,a,b,0)
 *  6	 (a,b,c,0)
 *  7	 (1/4,1/4,1/4,1/4)
 *  8	 (a,a,a,b)
 *  9	 (a,a,b,b)
 * 10	 (a,a,b,c)
 * 11	 (a,b,c,d)
 */

/* Generate nodes for index 1 */
#define CYCLE1_3D CYCLE8_3D(0.0L)
#define CYCLE1_3D_W(w) CYCLE8_3D_W(w)

/* Generate nodes for index 2 */
#define CYCLE2_3D CYCLE9_3D(0.0L)
#define CYCLE2_3D_W(w) CYCLE9_3D_W(w)

/* Generate nodes for index 3 */
#define CYCLE3_3D(a) CYCLE10_3D(0.0L, a)
#define CYCLE3_3D_W(w) CYCLE10_3D_W(w)

/* Generate nodes for index 4 */
#define CYCLE4_3D CYCLE8_3D(1.0L/3.0L)
#define CYCLE4_3D_W(w) CYCLE8_3D_W(w)

/* Generate nodes for index 5 */
#define CYCLE5_3D(a) CYCLE10_3D(a, 0.0L)
#define CYCLE5_3D_W(w) CYCLE10_3D_W(w)

/* Generate nodes for index 6 */
#define CYCLE6_3D(a, b) CYCLE11_3D(a, b, 0.0L)
#define CYCLE6_3D_W(w) CYCLE11_3D_W(w)

/* Generate nodes for index 7 */
#define CYCLE7_3D INIT_BARY_3D(0.25L, 0.25L, 0.25L, 0.25L)
#define CYCLE7_3D_W(w) (w)

/* Generate nodes for index 8 */
#define CYCLE8_3D(a)					\
  INIT_BARY_3D(1.0L - 3.0L*(a), (a), (a), (a)),		\
    INIT_BARY_3D((a), 1.0L - 3.0L*(a), (a), (a)),	\
    INIT_BARY_3D((a), (a), 1.0L - 3.0L*(a), (a)),	\
    INIT_BARY_3D((a), (a), (a), 1.0L - 3.0L*(a))
#define CYCLE8_3D_W(w) (w), (w), (w), (w)

/* Generate nodes for index 9 */
#define CYCLE9_3D(a)				\
  INIT_BARY_3D((a), (a), 0.5L-(a), 0.5L-(a)),	\
    INIT_BARY_3D((a), 0.5L-(a), (a), 0.5L-(a)),	\
    INIT_BARY_3D((a), 0.5L-(a), 0.5L-(a), (a)),	\
    INIT_BARY_3D(0.5L-(a), 0.5L-(a), (a), (a)),	\
    INIT_BARY_3D(0.5L-(a), (a), 0.5L-(a), (a)),	\
    INIT_BARY_3D(0.5L-(a), (a), (a), 0.5L-(a))
#define CYCLE9_3D_W(w) (w), (w), (w), (w), (w), (w)

/* Generate nodes for index 10 */
#define CYCLE10_3D(a, b)				\
  INIT_BARY_3D((a), (a), (b), 1.0L-2.0*(a)-(b)),	\
    INIT_BARY_3D((a), (a), 1.0L-2.0*(a)-(b), (b)),	\
    INIT_BARY_3D((a), (b), (a), 1.0L-2.0*(a)-(b)),	\
    INIT_BARY_3D((a), 1.0L-2.0*(a)-(b), (a), (b)),	\
    INIT_BARY_3D((a), (b), 1.0L-2.0*(a)-(b), (a)),	\
    INIT_BARY_3D((a), 1.0L-2.0*(a)-(b), (b), (a)),	\
    INIT_BARY_3D((b), (a), 1.0L-2.0*(a)-(b), (a)),	\
    INIT_BARY_3D(1.0L-2.0*(a)-(b), (a), (b), (a)),	\
    INIT_BARY_3D((b), 1.0L-2.0*(a)-(b), (a), (a)),	\
    INIT_BARY_3D(1.0L-2.0*(a)-(b), (b), (a), (a)),	\
    INIT_BARY_3D((b), (a), (a), 1.0L-2.0*(a)-(b)),	\
    INIT_BARY_3D(1.0L-2.0*(a)-(b), (a), (a), (b))
#define CYCLE10_3D_W(w)						\
  (w), (w), (w), (w), (w), (w), (w), (w), (w), (w), (w), (w)

/* Generate nodes for index 11 (all coordinates are pairwise different) */
#define CYCLE11_3D(a, b, c)				     \
  INIT_BARY_3D((a), (b), (c), 1.0L-(a)-(b)-(c)), /* unity */		\
    INIT_BARY_3D((b), (a), 1.0L-(a)-(b)-(c), (c)), /* disjoint trans. */ \
    INIT_BARY_3D(1.0L-(a)-(b)-(c), (c), (b), (a)),			\
    INIT_BARY_3D((c), 1.0L-(a)-(b)-(c), (a), (b)),			\
    INIT_BARY_3D((c), (a), (b), 1.0L-(a)-(b)-(c)), /* 3 cycles */	\
    INIT_BARY_3D((b), (c), (a), 1.0L-(a)-(b)-(c)),			\
    INIT_BARY_3D(1.0L-(a)-(b)-(c), (a), (c), (b)),			\
    INIT_BARY_3D((b), 1.0L-(a)-(b)-(c), (c), (a)),			\
    INIT_BARY_3D(1.0L-(a)-(b)-(c), (b), (a), (c)),			\
    INIT_BARY_3D((c), (b), 1.0L-(a)-(b)-(c), (a)),			\
    INIT_BARY_3D((a), 1.0L-(a)-(b)-(c), (b), (c)),			\
    INIT_BARY_3D((a), (c), 1.0L-(a)-(b)-(c), (b)),			\
    INIT_BARY_3D((b), (a), (c), 1.0L-(a)-(b)-(c)), /* single trans.*/	\
    INIT_BARY_3D((c), (b), (a), 1.0L-(a)-(b)-(c)),			\
    INIT_BARY_3D(1.0L-(a)-(b)-(c), (b), (c), (a)),			\
    INIT_BARY_3D((a), (c), (b), 1.0L-(a)-(b)-(c)),			\
    INIT_BARY_3D((a), 1.0L-(a)-(b)-(c), (c), (b)),			\
    INIT_BARY_3D((a), (b), 1.0L-(a)-(b)-(c), (c)),			\
    INIT_BARY_3D(1.0L-(a)-(b)-(c), (a), (b), (c)), /* 4 cycles */	\
    INIT_BARY_3D((c), 1.0L-(a)-(b)-(c), (a), (b)),			\
    INIT_BARY_3D((b), (c), 1.0L-(a)-(b)-(c), (a)),			\
    INIT_BARY_3D((b), (c), 1.0L-(a)-(b)-(c), (a)),			\
    INIT_BARY_3D((c), 1.0L-(a)-(b)-(c), (a), (b)),			\
    INIT_BARY_3D(1.0L-(a)-(b)-(c), (a), (b), (c))
#define CYCLE11_3D_W(w)							\
  (w), (w), (w), (w), (w), (w), (w), (w), (w), (w), (w), (w),		\
    (w), (w), (w), (w), (w), (w), (w), (w), (w), (w), (w), (w)

/*--------------------------------------------------------------------------*/
/*  quadrature exact on P_1                                                 */
/*--------------------------------------------------------------------------*/

static const REAL_B x1_3d[1] = {INIT_BARY_3D(quart, quart, quart, quart)};
static const REAL   w1_3d[1] = {StdVol*one};

/*--------------------------------------------------------------------------*/
/*  Quad quadrature exact on P_2                                           */
/*--------------------------------------------------------------------------*/

#define c14   0.585410196624969
#define c15   0.138196601125011

static const REAL_B x2_3d[4] = {
  INIT_BARY_3D(c14, c15, c15, c15),
  INIT_BARY_3D(c15, c14, c15, c15),
  INIT_BARY_3D(c15, c15, c14, c15),
  INIT_BARY_3D(c15, c15, c15, c14)
};
static REAL  w2_3d[4] = {
  StdVol*quart, StdVol*quart,
  StdVol*quart, StdVol*quart
};

/*--------------------------------------------------------------------------*/
/*  quadrature exact on P_3                                                 */
/*--------------------------------------------------------------------------*/

#define w8  1.0/40.0
#define w9  9.0/40.0

static const REAL_B x3_3d[8] = {
  INIT_BARY_3D(one,  zero,  zero,  zero),
  INIT_BARY_3D(zero,   one,  zero,  zero),
  INIT_BARY_3D(zero,  zero,   one,  zero),
  INIT_BARY_3D(zero,  zero,  zero,   one),
  INIT_BARY_3D(zero, third, third, third),
  INIT_BARY_3D(third,  zero, third, third),
  INIT_BARY_3D(third, third, zero,  third),
  INIT_BARY_3D(third, third, third,  zero)
};
static const REAL   w3_3d[8] = {
  StdVol*w8, StdVol*w8, StdVol*w8, StdVol*w8,
  StdVol*w9, StdVol*w9, StdVol*w9, StdVol*w9
};

/*--------------------------------------------------------------------------*/
/*  quadrature exact on P_5                                                 */
/*--------------------------------------------------------------------------*/

#if 0
static const REAL_B x5_3d[15] = {
  INIT_BARY_3D(0.250000000000000, 0.250000000000000, 0.250000000000000, 0.250000000000000),
  INIT_BARY_3D(0.091971078052723, 0.091971078052723, 0.091971078052723, 0.724086765841831),
  INIT_BARY_3D(0.724086765841831, 0.091971078052723, 0.091971078052723, 0.091971078052723),
  INIT_BARY_3D(0.091971078052723, 0.724086765841831, 0.091971078052723, 0.091971078052723),
  INIT_BARY_3D(0.091971078052723, 0.091971078052723, 0.724086765841831, 0.091971078052723),
  INIT_BARY_3D(0.319793627829630, 0.319793627829630, 0.319793627829630, 0.040619116511110),
  INIT_BARY_3D(0.040619116511110, 0.319793627829630, 0.319793627829630, 0.319793627829630),
  INIT_BARY_3D(0.319793627829630, 0.040619116511110, 0.319793627829630, 0.319793627829630),
  INIT_BARY_3D(0.319793627829630, 0.319793627829630, 0.040619116511110, 0.319793627829630),
  INIT_BARY_3D(0.443649167310371, 0.056350832689629, 0.056350832689629, 0.443649167310371),
  INIT_BARY_3D(0.056350832689629, 0.443649167310371, 0.056350832689629, 0.443649167310371),
  INIT_BARY_3D(0.056350832689629, 0.056350832689629, 0.443649167310371, 0.443649167310371),
  INIT_BARY_3D(0.443649167310371, 0.056350832689629, 0.443649167310371, 0.056350832689629),
  INIT_BARY_3D(0.443649167310371, 0.443649167310371, 0.056350832689629, 0.056350832689629),
  INIT_BARY_3D(0.056350832689629, 0.443649167310371, 0.443649167310371, 0.056350832689629)
};
static const REAL w5_3d[15] = {
  StdVol*0.118518518518519,
  StdVol*0.071937083779019,
  StdVol*0.071937083779019,
  StdVol*0.071937083779019,
  StdVol*0.071937083779019,
  StdVol*0.069068207226272,
  StdVol*0.069068207226272,
  StdVol*0.069068207226272,
  StdVol*0.069068207226272,
  StdVol*0.052910052910053,
  StdVol*0.052910052910053,
  StdVol*0.052910052910053,
  StdVol*0.052910052910053,
  StdVol*0.052910052910053,
  StdVol*0.052910052910053
};
#else

/* A. Grundmann and H.M. Moeller, Invariant integration formulas for
 * the n-simplex by combinatorial methods, SIAM J. Numer. Anal.  15
 * (1978), 282--290.
 *
 * Data taken from "Encyclopaedia of Cubature Formulas" at
 * www.cs.kuleuven.ac.be/~nines/research/ecf/
 *
 * Region: Simplex
 * Dimension: 3
 * Degree: 5
 * Points: 14
 * Structure: Fully symmetric
 * Rule struct: 0 0 0 0 0 0 0 2 1 0 0
 * Generator: [ Fully symmetric (4Pkte) ]
 * ( 0.0927352503108912264023239137370306,
 * 0.0927352503108912264023239137370306,
 * 0.0927352503108912264023239137370306,
 * )
 * Corresponding weight:
 * 0.0122488405193936582572850342477212,
 *
 * Generator: [ Fully symmetric (4Pkte) ]
 * ( 0.310885919263300609797345733763457,
 * 0.310885919263300609797345733763457,
 * 0.310885919263300609797345733763457,
 * )
 * Corresponding weight:
 * 0.0187813209530026417998642753888810,
 *
 * Generator: [ Fully symmetric ]
 * ( 0.454496295874350350508119473720660,
 * 0.454496295874350350508119473720660,
 * 0.0455037041256496494918805262793394,
 * )
 * Corresponding weight:
 * 10 ^ -3 x 7.09100346284691107301157135337624,
 */

static const REAL_B x5_3d[14] = {
  CYCLE8_3D(0.0927352503108912264023239137370306L),
  CYCLE8_3D(0.310885919263300609797345733763457L),
  CYCLE9_3D(0.454496295874350350508119473720660L)
};

static const REAL w5_3d[14] = {
  CYCLE8_3D_W(0.0122488405193936582572850342477212L),
  CYCLE8_3D_W(0.0187813209530026417998642753888810L),
  CYCLE9_3D_W(7.09100346284691107301157135337624e-3L)
};

#endif

/*--------------------------------------------------------------------------*/
/*  quadrature exact on P_6                                                 */
/*--------------------------------------------------------------------------*/

/* Kea86
 *
 * P. Keast, Moderate-degree tetrahedral quadrature formulas,
 * Comput. Methods Appl. Mech. Engrg. 55 (1986), 339--348.
 *
 * Data taken from "Encyclopaedia of Cubature Formulas" at
 * www.cs.kuleuven.ac.be/~nines/research/ecf/
 *
 * Region: Simplex
 * Dimension: 3
 * Degree: 6
 * Points: 24
 * Structure: Fully symmetric
 * Rule struct: 0 0 0 0 0 0 0 3 0 1 0
 * Generator: [ Fully symmetric ]
 * ( 0.214602871259152029288839219386284,
 * 0.214602871259152029288839219386284,
 * 0.214602871259152029288839219386284,
 * )
 * Corresponding weight:
 * 10 ^ -3 x 6.65379170969458201661510459291332,
 *
 * Generator: [ Fully symmetric ]
 * ( 0.0406739585346113531155794489564100,
 * 0.0406739585346113531155794489564100,
 * 0.0406739585346113531155794489564100,
 * )
 * Corresponding weight:
 * 10 ^ -3 x 1.67953517588677382466887290765614,
 *
 * Generator: [ Fully symmetric ]
 * ( 0.322337890142275510343994470762492,
 * 0.322337890142275510343994470762492,
 * 0.322337890142275510343994470762492,
 * )
 * Corresponding weight:
 * 10 ^ -3 x 9.22619692394245368252554630895433,
 *
 * Generator: [ Fully symmetric ]
 * ( 0.0636610018750175252992355276057269,
 * 0.0636610018750175252992355276057269,
 * 0.269672331458315808034097805727606,
 * )
 * Corresponding weight:
 * 10 ^ -3 x 8.03571428571428571428571428571428,
 *
 */

static const REAL_B x6_3d[24] = {
  CYCLE8_3D(0.214602871259152029288839219386284L),
  CYCLE8_3D(0.0406739585346113531155794489564100L),
  CYCLE8_3D(0.322337890142275510343994470762492L),
  CYCLE10_3D(0.0636610018750175252992355276057269L,
	     0.269672331458315808034097805727606L)
};

static const REAL w6_3d[24] = {
  CYCLE8_3D_W(6.65379170969458201661510459291332e-3L),
  CYCLE8_3D_W(1.67953517588677382466887290765614e-3L),
  CYCLE8_3D_W(9.22619692394245368252554630895433e-3L),
  CYCLE10_3D_W(8.03571428571428571428571428571428e-3L)  
};

#if 0
/* The following rules have negative weights, I just include them for
 * "completeness".
 */

/* Kea86
 *
 * P. Keast, Moderate-degree tetrahedral quadrature formulas,
 * Comput. Methods Appl. Mech. Engrg. 55 (1986), 339--348.
 *
 * Data taken from "Encyclopaedia of Cubature Formulas" at
 * www.cs.kuleuven.ac.be/~nines/research/ecf/
 *
 * Region: Simplex
 * Dimension: 3
 * Degree: 7
 * Points: 31
 * Structure: Fully symmetric
 * Rule struct: 0 1 0 0 0 0 1 3 0 1 0
 * Generator: [ Fully symmetric ]
 * ( 0.5, 0.5, 0., )
 * Corresponding weight:
 * 10 ^ -4 x 9.70017636684303350970017636684303,
 *
 * Generator: [ Fully symmetric ]
 * ( 0.25, 0.25, 0.25, )
 * Corresponding weight:
 * 0.0182642234661088202912015685649462,
 *
 * Generator: [ Fully symmetric ]
 * ( 0.0782131923303180643739942508375545,
 * 0.0782131923303180643739942508375545,
 * 0.0782131923303180643739942508375545,
 * )
 * Corresponding weight:
 * 0.0105999415244136869164138748545257,
 *
 * Generator: [ Fully symmetric ]
 * ( 0.121843216663905174652156372684818,
 * 0.121843216663905174652156372684818,
 * 0.121843216663905174652156372684818,
 * )
 * Corresponding weight:
 * -0.0625177401143318516914703474927900,
 *
 * Generator: [ Fully symmetric ]
 * ( 0.332539164446420624152923823157707,
 * 0.332539164446420624152923823157707,
 * 0.332539164446420624152923823157707,
 * )
 * Corresponding weight:
 * 10 ^ -3 x 4.89142526307349938479576303671027,
 *
 * Generator: [ Fully symmetric ]
 * ( 0.1, 0.1, 0.2, )
 * Corresponding weight:
 * 0.0275573192239858906525573192239858,
 */

static const REAL_B x7_3d[31] = {
  CYCLE2_3D,
  CYCLE7_3D,
  CYCLE8_3D(0.0782131923303180643739942508375545L),
  CYCLE8_3D(0.121843216663905174652156372684818L),
  CYCLE8_3D(0.332539164446420624152923823157707L),
  CYCLE10_3D(0.1L, 0.2L)
};
static const REAL w7_3d[31] = {
  CYCLE2_3D_W(9.70017636684303350970017636684303e-4L),
  CYCLE7_3D_W(0.0182642234661088202912015685649462L),
  CYCLE8_3D_W(0.0105999415244136869164138748545257L),
  CYCLE8_3D_W(-0.0625177401143318516914703474927900L),
  CYCLE8_3D_W(4.89142526307349938479576303671027e-3L),
  CYCLE10_3D_W(0.0275573192239858906525573192239858L)
};

/* BH90
 *
 * M. Beckers and A. Haegemans, The construction of cubature formulae
 * for the tetrahedron, Report TW 128, Dept. of Computer Science,
 * K.U. Leuven, 1990.
 *
 * Region: Simplex
 * Dimension: 3
 * Degree: 8
 * Points: 43
 * Structure: Fully symmetric
 * Rule struct: 0 0 0 0 0 0 1 3 1 2 0
 * Generator: [ Fully symmetric ]
 * ( 0.25, 0.25, 0.25, )
 * Corresponding weight:
 * -0.0205001886586399158405865177642941,
 *
 * Generator: [ Fully symmetric ]
 * ( 0.206829931610673204083980900024961,
 * 0.206829931610673204083980900024961,
 * 0.206829931610673204083980900024961,
 * )
 * Corresponding weight:
 * 0.0142503058228669012484397415358704,
 *
 * Generator: [ Fully symmetric ]
 * ( 0.0821035883105467230906058078714215,
 * 0.0821035883105467230906058078714215,
 * 0.0821035883105467230906058078714215,
 * )
 * Corresponding weight:
 * 10 ^ -3 x 1.96703331313390098756280342445466,
 *
 * Generator: [ Fully symmetric ]
 * ( 10 ^ -3 x 5.78195050519799725317663886414270,
 * 10 ^ -3 x 5.78195050519799725317663886414270,
 * 10 ^ -3 x 5.78195050519799725317663886414270,
 * )
 * Corresponding weight:
 * 10 ^ -4 x 1.69834109092887379837744566704016,
 *
 * Generator: [ Fully symmetric ]
 * ( 0.0505327400188942244256245285579071,
 * 0.0505327400188942244256245285579071,
 * 0.449467259981105775574375471442092,
 * )
 * Corresponding weight:
 * 10 ^ -3 x 4.57968382446728180074351446297276,
 *
 * Generator: [ Fully symmetric ]
 * ( 0.229066536116811139600408854554753,
 * 0.229066536116811139600408854554753,
 * 0.0356395827885340437169173969506114,
 * )
 * Corresponding weight:
 * 10 ^ -3 x 5.70448580868191850680255862783040,
 *
 * Generator: [ Fully symmetric ]
 * ( 0.0366077495531974236787738546327104,
 * 0.0366077495531974236787738546327104,
 * 0.190486041934633455699433285315099,
 * )
 * Corresponding weight:
 * 10 ^ -3 x 2.14051914116209259648335300092023,
 */
static const REAL_B x8_3d[43] = {
  CYCLE7_3D,
  CYCLE8_3D(0.206829931610673204083980900024961L),
  CYCLE8_3D(0.0821035883105467230906058078714215L),
  CYCLE8_3D(5.78195050519799725317663886414270e-3L),
  CYCLE9_3D(0.0505327400188942244256245285579071L,
	    0.449467259981105775574375471442092L),
  CYCLE10_3D(0.229066536116811139600408854554753L,
	     0.0356395827885340437169173969506114L),
  CYCLE10_3D(0.0366077495531974236787738546327104L,
	     0.190486041934633455699433285315099L)
};

static const REAL w8_3d[43] = {
  CYCLE7_3D_W(-0.0205001886586399158405865177642941),
  CYCLE8_3D_W(0.0142503058228669012484397415358704L),
  CYCLE8_3D_W(1.96703331313390098756280342445466e-3L),
  CYCLE8_3D_W(1.69834109092887379837744566704016e-4L),
  CYCLE9_3D_W(4.57968382446728180074351446297276e-3L),
  CYCLE10_3D_W(5.70448580868191850680255862783040e-3L),
  CYCLE10_3D_W(2.1405191411620925964833530009202e-3L)
};

/* There is also a formula for degree 9 with 53 points in
 *
 * M. Beckers and A. Haegemans, The construction of cubature
 * formulae for the tetrahedron, Report TW 128, Dept. of Computer
 * Science, K.U. Leuven, 1990.
 *
 * But only with barycentric
 * coordinates lying in the exterior, which we do not want.
 *
 * There is also a formula for degree 11 with 87 points in
 *
 * J.I. Maeztu and E. Sainz de la Maza, An invariant quadrature rule
 * of degree 11 for the tetrahedron, C. R. Acad. Sci. Paris 321
 * (1995), 1263--1267.
 *
 * But that person did only publish 32bit precision floats
 * (efficiently, the published barycentric coordinates do not sum up
 * to 1, and the error is of magnitude FLT_EPSILON.
 */
#endif

#if 0
/* cH: The weights and coordinates of the quadrature points are quite
 * inexact; also: we can generate an 48 point formula by computing a
 * conic product rule with a P_7 2d quadrature formula. This is done
 * on-the-fly now for P_7/8/9 in get_quadrature. Therefore this rule
 * is disabled because it is also only a conic product-rule.
 */

/*--------------------------------------------------------------------------*/
/*  quadrature exact on P_7                                                 */
/*--------------------------------------------------------------------------*/

/* cH: AFAIK, the following is just the usual conic Gauss quadrature
 * formula (which is unsymmetric, of course). So instead of using the
 * stuff below one could write a function generating such rules ...
 *
 * Couldn't we just combine a D7 2d quadrature (12 points) with a D7
 * 1d quadrature (4 points) to get a 48 points quad-rule?
 */
static const REAL_B x7_3d[64] = {
  INIT_BARY_3D(0.0485005494, 0.0543346112, 0.0622918076, 0.8348730318),
  INIT_BARY_3D(0.0485005494, 0.0543346112, 0.2960729005, 0.6010919389),
  INIT_BARY_3D(0.0485005494, 0.0543346112, 0.6010919389, 0.2960729005),
  INIT_BARY_3D(0.0485005494, 0.0543346112, 0.8348730300, 0.0622918093),
  INIT_BARY_3D(0.0485005494, 0.2634159753, 0.0477749033, 0.6403085720),
  INIT_BARY_3D(0.0485005494, 0.2634159753, 0.2270740686, 0.4610094066),
  INIT_BARY_3D(0.0485005494, 0.2634159753, 0.4610094066, 0.2270740686),
  INIT_BARY_3D(0.0485005494, 0.2634159753, 0.6403085706, 0.0477749047),
  INIT_BARY_3D(0.0485005494, 0.5552859758, 0.0275098315, 0.3687036433),
  INIT_BARY_3D(0.0485005494, 0.5552859758, 0.1307542021, 0.2654592727),
  INIT_BARY_3D(0.0485005494, 0.5552859758, 0.2654592727, 0.1307542021),
  INIT_BARY_3D(0.0485005494, 0.5552859758, 0.3687036425, 0.0275098323),
  INIT_BARY_3D(0.0485005494, 0.8185180165, 0.0092331459, 0.1237482881),
  INIT_BARY_3D(0.0485005494, 0.8185180165, 0.0438851337, 0.0890963004),
  INIT_BARY_3D(0.0485005494, 0.8185180165, 0.0890963004, 0.0438851337),
  INIT_BARY_3D(0.0485005494, 0.8185180165, 0.1237482879, 0.0092331462),
  INIT_BARY_3D(0.2386007376, 0.0434790928, 0.0498465199, 0.6680736497),
  INIT_BARY_3D(0.2386007376, 0.0434790928, 0.2369204606, 0.4809997090),
  INIT_BARY_3D(0.2386007376, 0.0434790928, 0.4809997090, 0.2369204606),
  INIT_BARY_3D(0.2386007376, 0.0434790928, 0.6680736482, 0.0498465214),
  INIT_BARY_3D(0.2386007376, 0.2107880664, 0.0382299497, 0.5123812464),
  INIT_BARY_3D(0.2386007376, 0.2107880664, 0.1817069135, 0.3689042825),
  INIT_BARY_3D(0.2386007376, 0.2107880664, 0.3689042825, 0.1817069135),
  INIT_BARY_3D(0.2386007376, 0.2107880664, 0.5123812453, 0.0382299508),
  INIT_BARY_3D(0.2386007376, 0.4443453248, 0.0220136390, 0.2950402987),
  INIT_BARY_3D(0.2386007376, 0.4443453248, 0.1046308045, 0.2124231331),
  INIT_BARY_3D(0.2386007376, 0.4443453248, 0.2124231331, 0.1046308045),
  INIT_BARY_3D(0.2386007376, 0.4443453248, 0.2950402980, 0.0220136396),
  INIT_BARY_3D(0.2386007376, 0.6549862048, 0.0073884546, 0.0990246030),
  INIT_BARY_3D(0.2386007376, 0.6549862048, 0.0351173176, 0.0712957400),
  INIT_BARY_3D(0.2386007376, 0.6549862048, 0.0712957400, 0.0351173176),
  INIT_BARY_3D(0.2386007376, 0.6549862048, 0.0990246028, 0.0073884548),
  INIT_BARY_3D(0.5170472951, 0.0275786260, 0.0316174612, 0.4237566177),
  INIT_BARY_3D(0.5170472951, 0.0275786260, 0.1502777622, 0.3050963168),
  INIT_BARY_3D(0.5170472951, 0.0275786260, 0.3050963168, 0.1502777622),
  INIT_BARY_3D(0.5170472951, 0.0275786260, 0.4237566168, 0.0316174621),
  INIT_BARY_3D(0.5170472951, 0.1337020823, 0.0242491141, 0.3250015085),
  INIT_BARY_3D(0.5170472951, 0.1337020823, 0.1152560157, 0.2339946069),
  INIT_BARY_3D(0.5170472951, 0.1337020823, 0.2339946069, 0.1152560157),
  INIT_BARY_3D(0.5170472951, 0.1337020823, 0.3250015078, 0.0242491148),
  INIT_BARY_3D(0.5170472951, 0.2818465779, 0.0139631689, 0.1871429581),
  INIT_BARY_3D(0.5170472951, 0.2818465779, 0.0663669280, 0.1347391990),
  INIT_BARY_3D(0.5170472951, 0.2818465779, 0.1347391990, 0.0663669280),
  INIT_BARY_3D(0.5170472951, 0.2818465779, 0.1871429577, 0.0139631693),
  INIT_BARY_3D(0.5170472951, 0.4154553004, 0.0046864691, 0.0628109354),
  INIT_BARY_3D(0.5170472951, 0.4154553004, 0.0222747832, 0.0452226213),
  INIT_BARY_3D(0.5170472951, 0.4154553004, 0.0452226213, 0.0222747832),
  INIT_BARY_3D(0.5170472951, 0.4154553004, 0.0628109352, 0.0046864693),
  INIT_BARY_3D(0.7958514179, 0.0116577407, 0.0133649937, 0.1791258477),
  INIT_BARY_3D(0.7958514179, 0.0116577407, 0.0635238021, 0.1289670393),
  INIT_BARY_3D(0.7958514179, 0.0116577407, 0.1289670393, 0.0635238021),
  INIT_BARY_3D(0.7958514179, 0.0116577407, 0.1791258473, 0.0133649941),
  INIT_BARY_3D(0.7958514179, 0.0565171087, 0.0102503252, 0.1373811482),
  INIT_BARY_3D(0.7958514179, 0.0565171087, 0.0487197855, 0.0989116879),
  INIT_BARY_3D(0.7958514179, 0.0565171087, 0.0989116879, 0.0487197855),
  INIT_BARY_3D(0.7958514179, 0.0565171087, 0.1373811479, 0.0102503255),
  INIT_BARY_3D(0.7958514179, 0.1191391593, 0.0059023608, 0.0791070620),
  INIT_BARY_3D(0.7958514179, 0.1191391593, 0.0280539153, 0.0569555075),
  INIT_BARY_3D(0.7958514179, 0.1191391593, 0.0569555075, 0.0280539153),
  INIT_BARY_3D(0.7958514179, 0.1191391593, 0.0791070618, 0.0059023610),
  INIT_BARY_3D(0.7958514179, 0.1756168040, 0.0019810139, 0.0265507642),
  INIT_BARY_3D(0.7958514179, 0.1756168040, 0.0094157572, 0.0191160209),
  INIT_BARY_3D(0.7958514179, 0.1756168040, 0.0191160209, 0.0094157572),
  INIT_BARY_3D(0.7958514179, 0.1756168040, 0.0265507642, 0.0019810140)};
static const REAL w7_3d[64] = {
  StdVol*0.0156807540, StdVol*0.0293976870,
  StdVol*0.0293976870, StdVol*0.0156807540,
  StdVol*0.0235447608, StdVol*0.0441408300,
  StdVol*0.0441408300, StdVol*0.0235447608,
  StdVol*0.0150258564, StdVol*0.0281699100,
  StdVol*0.0281699100, StdVol*0.0150258564,
  StdVol*0.0036082374, StdVol*0.0067645878,
  StdVol*0.0067645878, StdVol*0.0036082374,
  StdVol*0.0202865376, StdVol*0.0380324358,
  StdVol*0.0380324358, StdVol*0.0202865376,
  StdVol*0.0304603764, StdVol*0.0571059660,
  StdVol*0.0571059660, StdVol*0.0304603764,
  StdVol*0.0194392824, StdVol*0.0364440336,
  StdVol*0.0364440336, StdVol*0.0194392824,
  StdVol*0.0046680564, StdVol*0.0087514968,
  StdVol*0.0087514968, StdVol*0.0046680564,
  StdVol*0.0097055322, StdVol*0.0181955664,
  StdVol*0.0181955664, StdVol*0.0097055322,
  StdVol*0.0145729242, StdVol*0.0273207684,
  StdVol*0.0273207684, StdVol*0.0145729242,
  StdVol*0.0093001866, StdVol*0.0174356394,
  StdVol*0.0174356394, StdVol*0.0093001866,
  StdVol*0.0022333026, StdVol*0.0041869110,
  StdVol*0.0041869110, StdVol*0.0022333026,
  StdVol*0.0014639124, StdVol*0.0027444882,
  StdVol*0.0027444882, StdVol*0.0014639124,
  StdVol*0.0021980748, StdVol*0.0041208678,
  StdVol*0.0041208678, StdVol*0.0021980748,
  StdVol*0.0014027730, StdVol*0.0026298660,
  StdVol*0.0026298660, StdVol*0.0014027730,
  StdVol*0.0003368550, StdVol*0.0006315234,
  StdVol*0.0006315234, StdVol*0.0003368550
};
#endif

/*--------------------------------------------------------------------------*/
/*  build a vector of Quad' quadrature formulars. For quadrature of degree */
/*  use that of degree (only on function evaluation also)                   */
/*--------------------------------------------------------------------------*/

#define NPT(pts) sizeof(pts)/sizeof(*pts)

static QUAD_METADATA qmd_3d[MAX_QUAD_DEG_3D+1];

static const QUAD quad_3d[MAX_QUAD_DEG_3D+1] = {
  QR("3d Stroud: P_1", 1, 3,  1, x1_3d, w1_3d, &qmd_3d[0]),  /* P_0  */
  QR("3d Stroud: P_1", 1, 3,  1, x1_3d, w1_3d, &qmd_3d[1]),  /* P_1  */
  QR("3d Stroud: P_2", 2, 3,  4, x2_3d, w2_3d, &qmd_3d[2]),  /* P_2  */
  QR("3d Stroud: P_3", 3, 3,  8, x3_3d, w3_3d, &qmd_3d[3]),  /* P_3  */
  QR("3d gm78: P_5",   5, 3, 14, x5_3d, w5_3d, &qmd_3d[4]),  /* P_4  */
  QR("3d gm78: P_5",   5, 3, 14, x5_3d, w5_3d, &qmd_3d[5]),  /* P_5  */
  QR("3d kea86: P_6",  6, 3, 24, x6_3d, w6_3d, &qmd_3d[6]),  /* P_6  */
/*  QR("3d Stroud: P_7", 7, 3, 64, x7_3d, w7_3d, &qmd_3d[6]),   / * P_7  */
};

#define N_QUAD_POINTS_MAX_3D 24 /* maximal number for < MAX_QUAD_DEG_3D */

#undef StdVol

/*--------------------------------------------------------------------------*/
/*  integration in different dimensions                                     */
/*--------------------------------------------------------------------------*/

static const QUAD *quad_nd[DIM_LIMIT+1] = {
  quad_0d, quad_1d, quad_2d, quad_3d
};
static U_CHAR max_quad_deg[DIM_LIMIT+1];

/* Maximal degree of tabulated quadrature formulas. */
int n_quad_points_max[] = {
  1,
  MAX_QUAD_DEG_1D/2+1, /* Gauss-Legendre quadrature */
#if DIM_MAX > 1
  N_QUAD_POINTS_MAX_2D,
# if DIM_MAX > 2
  N_QUAD_POINTS_MAX_3D,
#  if DIM_MAX > 3
#   error FIXME
#  endif
# endif
#endif
};

/* List of dynamic or user added quadratures */
struct quad_list {
  struct quad_list *next;
  const QUAD *quad;
};

static struct quad_list *ql_head[DIM_MAX+1];

const QUAD *get_quadrature(int dim, int degree)
{
  FUNCNAME("get_quadrature");

  TEST_EXIT((dim >= 0) && (dim < 4),
	    "Bad dim %d - must be between 0 and 3!\n");

  degree = MAX(0, degree);

  if (degree > max_quad_deg[dim]) {
    struct quad_list *pos;

    /* allocate the per-element cache space now for all quadratures,
     * this is efficient enough, because it is a one-time only
     * initialization, and the "if()" clause above leads to a slow
     * code-path anyway.
     */
    if (max_quad_deg[dim] == 0) {
      int d, deg;

      max_quad_deg[0] = MAX_QUAD_DEG_0D;
      max_quad_deg[1] = MAX_QUAD_DEG_1D;
      max_quad_deg[2] = MAX_QUAD_DEG_2D;
      max_quad_deg[3] = MAX_QUAD_DEG_3D;

      for (d = 0; d <= DIM_MAX; d++) {
	for (deg = 0; deg <= max_quad_deg[d]; deg++) {
	  const QUAD    *quad = &quad_nd[d][deg];
	  QUAD_EL_CACHE *qmd  = &((QUAD_METADATA *)quad->metadata)->el_cache;

	  qmd->world           = MEM_ALLOC(quad->n_points_max, REAL_D);
	  qmd->param.det       = MEM_ALLOC(quad->n_points_max, REAL);
	  qmd->param.Lambda    = MEM_ALLOC(quad->n_points_max, REAL_BD);
	  qmd->param.DLambda   = MEM_ALLOC(quad->n_points_max, REAL_BDD);
	  qmd->param.grd_world = MEM_ALLOC(quad->n_points_max, REAL_BD);
	  qmd->param.D2_world  = MEM_ALLOC(quad->n_points_max, REAL_BDB);
	  qmd->param.D3_world  = MEM_ALLOC(quad->n_points_max, REAL_BDBB);
	  /* wall_det and wall_normal make no sense here as codim == 0 */
	}
      }
      return get_quadrature(dim, degree);
    }

    for (pos = ql_head[dim]; pos; pos = pos->next) {
      if (pos->quad->dim == dim && pos->quad->degree >= degree) {
	return pos->quad;
      }
    }
#if DIM_MAX == 3
    /* ALBERTA's tabulated 3d quad-rule of degree 7 is somewhat poor:
     * it is a conical product rule with the maximal number of points
     * (64), copied out of Stroud's book. In addition, the precision
     * of that rule is somewhat poor. We can do a little bit better
     * nowadays: we have better 2d rules, so we do the conical product
     * ourselves for degree 7 (48 points), 8 (80 points) and 9 (95
     * points).
     */
    if (dim == 3) {
      if (degree > MAX_QUAD_DEG_3D && degree <= MAX_DYN_QUAD_DEG_3d) {
	return get_product_quad(get_quadrature(2, degree));
      }
      MSG("degree %d too large; changing to %d\n", degree, MAX_DYN_QUAD_DEG_3d);
      degree = MAX_DYN_QUAD_DEG_3d;
      return get_quadrature(dim, degree);
    }
#endif

    MSG("degree %d too large; changing to %d\n", degree, max_quad_deg[dim]);
    degree = max_quad_deg[dim];
  }
  
  return (const QUAD *)&quad_nd[dim][degree];
}

typedef struct _AI_qfast_list {
  struct _AI_qfast_list *next;
  QUAD_FAST             *quad_fast;
} _AI_QFAST_LIST;

/* allocate the QUAD->metadata and update n_quad_points_max[QUAD->dim],
 * but do not add the quadrature to the list of default quadratures.
 */
void register_quadrature(QUAD *quad)
{
  QUAD_METADATA *md;
  int deg;
  
  TEST_EXIT(quad->dim >= 0 && quad->dim <= DIM_MAX,
	    "Invalid quadrature dimension %d (must have 0 <=  dim <= %d).\n",
	    DIM_MAX);
  TEST_EXIT(quad->codim == 0 || quad->codim == 1,
	    "Only co-dimensino 0 and 1 quadratures are support ATM.\n");
  TEST_EXIT(quad->codim == 0 ||
	    (quad->subsplx >= 0 && quad->subsplx <= quad->dim+1),
	    "Ivalid sub-simplex number %d (dimension is %d!!!).\n",
	    quad->subsplx, quad->dim);

  if ((md = (QUAD_METADATA *)quad->metadata) != NULL) {
    /* assume that this is an update, re-allocate the meta-data caches. */
    MEM_FREE(md->el_cache.world, md->n_points, REAL_D);
    MEM_FREE(md->el_cache.param.det, md->n_points, REAL);
    MEM_FREE(md->el_cache.param.Lambda, md->n_points, REAL_BD);
    MEM_FREE(md->el_cache.param.DLambda, md->n_points, REAL_BDD);
    MEM_FREE(md->el_cache.param.grd_world, md->n_points, REAL_BD);
    MEM_FREE(md->el_cache.param.D2_world, md->n_points, REAL_BDB);
    MEM_FREE(md->el_cache.param.D3_world, md->n_points, REAL_BDBB);
    if (quad->codim == 1) {
      MEM_FREE(md->el_cache.param.wall_det, md->n_points, REAL);
      MEM_FREE(md->el_cache.param.wall_normal, md->n_points, REAL_D);
      MEM_FREE(md->el_cache.param.grd_normal, md->n_points, REAL_DB);
      MEM_FREE(md->el_cache.param.D2_normal, md->n_points, REAL_DBB);
    }
    if (md->delete_param_data) {
      for (deg = 0; deg < LAGRANGE_DEG_MAX; deg++) {
	md->delete_param_data(md->param_data[deg]);
	md->param_data[deg] = NULL;
      }
    }
    _AI_QFAST_LIST *ai_qf;
    /* Re-initialize the quad-fast structures attached to this
     * quadrature
     */
    for (ai_qf = (_AI_QFAST_LIST *)md->quad_fast_head.ordinary;
	 ai_qf;
	 ai_qf = (_AI_QFAST_LIST *)ai_qf->next) {
      INIT_OBJECT(ai_qf->quad_fast);
    }
    for (ai_qf = (_AI_QFAST_LIST *)md->quad_fast_head.tangential;
	 ai_qf;
	 ai_qf = (_AI_QFAST_LIST *)ai_qf->next) {
      INIT_OBJECT(ai_qf->quad_fast);
    }
  } else {
    quad->metadata = md = MEM_CALLOC(1, QUAD_METADATA);
  }
  
  /* allocate the per-element quadrature space */
  md->el_cache.world           = MEM_CALLOC(quad->n_points_max, REAL_D);
  md->el_cache.param.det       = MEM_CALLOC(quad->n_points_max, REAL);
  md->el_cache.param.Lambda    = MEM_CALLOC(quad->n_points_max, REAL_BD);
  md->el_cache.param.DLambda   = MEM_CALLOC(quad->n_points_max, REAL_BDD);
  md->el_cache.param.grd_world = MEM_CALLOC(quad->n_points_max, REAL_BD);
  md->el_cache.param.D2_world  = MEM_CALLOC(quad->n_points_max, REAL_BDB);
  md->el_cache.param.D3_world  = MEM_CALLOC(quad->n_points_max, REAL_BDBB);
  if (quad->codim == 1) {
    md->el_cache.param.wall_det    = MEM_CALLOC(quad->n_points_max, REAL);
    md->el_cache.param.wall_normal = MEM_CALLOC(quad->n_points_max, REAL_D);
    md->el_cache.param.grd_normal  = MEM_CALLOC(quad->n_points_max, REAL_DB);
    md->el_cache.param.D2_normal   = MEM_CALLOC(quad->n_points_max, REAL_DBB);
  }
  md->n_points = quad->n_points_max;

  /* register new number of maximal quadrature points */
  n_quad_points_max[quad->dim] =
    MAX(n_quad_points_max[quad->dim], quad->n_points_max);
}

/* Link the given rule into the list of available quadratures.  Before
 * doing so, init_quadrature() must have been called to initialize the
 * quadrature object's meta-data.
 */
bool new_quadrature(const QUAD *quad)
{
  QUAD_METADATA *qmd = (QUAD_METADATA *)quad->metadata;
  struct quad_list *pos, *prev, *newq;
  int dim = quad->dim;

  if (quad->metadata &&
      qmd->el_cache.world &&
      qmd->el_cache.param.det &&
      qmd->el_cache.param.Lambda &&
      qmd->el_cache.param.DLambda) {
    int deg;
    
    for (deg = 0; deg < LAGRANGE_DEG_MAX; deg++) {
      TEST_EXIT(qmd->param_data[deg] == NULL,
		"Quadrature with badly initialized meta-data.\n");
    }
  } else {
    MSG("Qudrature without or with badly initialized meta-data.\n");
    ERROR_EXIT("Did you call register_quadrature() before?\n");
  }

  /* register new number of maximal quadrature points */
  n_quad_points_max[dim] = MAX(n_quad_points_max[dim], quad->n_points_max);

  for (prev = pos = ql_head[dim]; pos; prev = pos, pos = pos->next) {
    if (pos->quad->degree >= quad->degree) {
      break;
    }
  }
  if (prev) {
    if (pos && pos->quad->degree == quad->degree) {
      pos->quad = quad; /* simply replace with new quad formula */
    } else {
      newq = MEM_ALLOC(1, struct quad_list);
      newq->next = pos;
      prev->next = newq;
      newq->quad = quad;
    }
  } else {
    if (pos && pos->quad->degree == quad->degree) {
      pos->quad = quad; /* simply replace with new quad formula */
    } else {
      newq = MEM_ALLOC(1, struct quad_list);
      ql_head[dim] = newq;
      newq->next   = NULL;
      newq->quad   = quad;
    }
  }

  return true;
}

/* Generate a quadrature of the same degree as quad but for one
 * dimension higher by using a conic product rule with a suitable
 * Gauss-Jacobi quadrature. It is possible to (recursively) generate
 * quadrature rules of arbitray degree and dimension this way, but the
 * number of points is sub-optimal (i.e. maximal ...) and the rule is
 * not symmetric.
 *
 * Gauss-Jocobi: quadrature with weight function
 * (1-x)^\alpha*(1+x)^\beta, we need \alpha = oq->dim, \beta = 0.0.
 */
const QUAD *get_product_quad(const QUAD *oq)
{
  FUNCNAME("get_product_quad");
  int gj_np = oq->degree / 2 + 1; /* exact on (1-x)^d*p, p\in\P_{2 np - 1} */
  REAL gj_x[gj_np], gj_w[gj_np], *w;
  QUAD *quad;
  int gj_qp, oqp, i;

  /* Note: the following function computes the quadrature for the
   * interval [-1,1], using cartesian coordinates. This means the
   * weights have to be scaled by the factor 0.5 and the barycentric
   * coordinates are (0.5*(x[qp]+1)).
   */
  _AI_gauss_quad(5 /* Gauss-Jacobi */,
		 gj_np /* number of points */,
		 (REAL)oq->dim /* alpha */, 0.0 /* beta */,
		 0, NULL, gj_x, gj_w);

  /* Transform to (0,1) interval */
  for (i = 0; i < gj_np; i++) {
    gj_w[i] /= 8.0;
    gj_x[i]  = 0.5*gj_x[i] + 0.5;
  }  

  quad               = MEM_CALLOC(1, QUAD);
  quad->name         = MEM_ALLOC(strlen(oq->name) + 20, char);
  sprintf((char *)quad->name, "\"Gauss-Jacobi\" x \"%s\"", oq->name);
  quad->degree       = oq->degree;
  quad->dim          = 1 + oq->dim;
  quad->n_points     = gj_np * oq->n_points;
  quad->n_points_max = quad->n_points;
  quad->lambda       = MEM_CALLOC(quad->n_points, const REAL_B);
  quad->w            = w = MEM_ALLOC(quad->n_points, REAL);

  for (gj_qp = 0; gj_qp < gj_np; ++gj_qp) {
    for (oqp = 0; oqp < oq->n_points; ++oqp) {
      int idx = gj_qp*oq->n_points + oqp;
      REAL *lambda = (REAL *)quad->lambda[idx];
      REAL gj_l0, gj_l1;
      
      gj_l1 = gj_x[gj_qp]; /* v1 is (1,0) */
      gj_l0 = 1.0 - gj_l1; /* v0 is (0,0) */

      for (i = 0; i < N_VERTICES(oq->dim); i++) {
	lambda[i] = oq->lambda[oqp][i] * gj_l0;
      }
      lambda[i] = gj_l1;
      
      /* The weights of oq have to be scaled by powers of gj_l0 */
      w[idx] = oq->w[oqp] * gj_w[gj_qp];
    }
  }
  /*check_quadrature(quad);*/

  register_quadrature(quad); /* alloc meta-data etc. */
  new_quadrature(quad); /* Make the stuff available via get_quadrature() */

  return quad;
}

/* Check performed on standard-simplex */
static REAL check_quadrature_1d(const QUAD *quad)
{
  FUNCNAME("check_quadrature_1d");
  REAL error, total_error = 0.0;
  int i, k, qp;

  for (i = 0; i <= quad->degree; i++){
    REAL sum = 0.0;
    for (qp = 0; qp < quad->n_points; qp++){
      REAL x_i = 1.0; /* x^{i-j} */

      for (k = 1; k <= i; k++)
	x_i *= quad->lambda[qp][1];
      sum += quad->w[qp] * x_i;
    }

#if SIZEOF_LONG == 8
	long a = 1;
	long b = 1;
#elif SIZEOF_LONG_LONG == 8
	long long a = 1;
	long long b = 1;
#else
	REAL a = 1;
	REAL b = 1;
#endif
    for (k = 1; k <= i; k++)
      a *= k;
    for (k = 1; k <= i+1; k++)
      b *= k;
	
    error = fabs(sum - (REAL)a / (REAL)b);
    total_error += error;
    
    MSG("x^%d, err: %e\n", i, error);
  }

  return total_error;
}

#if DIM_MAX > 1
static REAL check_quadrature_2d(const QUAD *quad)
{
  FUNCNAME("check_quadrature_2d");
  REAL error, total_error = 0.0;
  int i, j, k, qp;

  for (i = 0; i <= quad->degree; i++){
    for (j = 0; j <= i; j++){
      int i_j = i-j;

      REAL sum = 0.0;
      for (qp = 0; qp < quad->n_points; qp++){
	REAL x_i_j = 1.0; /* x^{i-j} */
	REAL y_j   = 1.0; /* y^{j-l} */

	for (k = 1; k <= i_j; k++)
	  x_i_j *= quad->lambda[qp][1];
	for (k = 1; k <= j; k++)
	  y_j   *= quad->lambda[qp][2];
	sum += quad->w[qp] * x_i_j * y_j;
      }

#if SIZEOF_LONG == 8
	long a = 1;
	long b = 1;
#elif SIZEOF_LONG_LONG == 8
	long long a = 1;
	long long b = 1;
#else
	REAL a = 1;
	REAL b = 1;
#endif
      for (k = 2; k <= i_j; k++)
	a *= k;
      for (k = 2; k <= j; k++)
	a *= k;
      for (k = 2; k <= i+2; k++)
	b *= k;
	
      error = fabs(sum - (REAL)a / (REAL)b);
      total_error += error;
	
      MSG("x^%d y^%d, err: %e\n", i_j, j, error);
    }
  }

  return total_error;
}
#endif

#if DIM_MAX > 2
static REAL check_quadrature_3d(const QUAD *quad)
{
  FUNCNAME("check_quadrature_3d");
  REAL error, total_error = 0.0;
  int i, j, k, l, qp;

  for (i = 0; i <= quad->degree; i++){
    for (j = 0; j <= i; j++){
      for (l = 0; l <= j; l++){
	int i_j = i-j;
	int j_l = j-l;

	REAL sum = 0.0;
	for (qp = 0; qp < quad->n_points; qp++){
	  REAL x_i_j = 1.0; /* x^{i-j} */
	  REAL y_j_l = 1.0; /* y^{j-l} */
	  REAL z_l = 1.0;   /* z^l     */
	  for (k = 1; k <= i_j; k++)
	    x_i_j *= quad->lambda[qp][1];
	  for (k = 1; k <= j_l; k++)
	    y_j_l *= quad->lambda[qp][2];
	  for (k = 1; k <= l; k++)
	    z_l *= quad->lambda[qp][3];
	  sum += quad->w[qp] * x_i_j * y_j_l * z_l;
	}

#if SIZEOF_LONG == 8
	long a = 1;
	long b = 1;
#elif SIZEOF_LONG_LONG == 8
	long long a = 1;
	long long b = 1;
#else
	REAL a = 1;
	REAL b = 1;
#endif
	for (k = 2; k <= i_j; k++)
	  a *= k;
	for (k = 2; k <= j_l; k++)
	  a *= k;
	for (k = 2; k <= l; k++)
	  a *= k;
	for (k = 2; k <= i+3; k++)
	  b *= k;
	
	error = fabs(sum - (REAL)a / (REAL)b);
	total_error += error;
	
	MSG("x^%d y^%d z^%d, err: %e\n", i_j, j_l, l, error);
      }
    }
  }
  return total_error;
}
#endif

void check_quadrature(const QUAD *quad)
{
  FUNCNAME("check_quadrature");
  REAL total_error = HUGE_VAL;
  REAL wsum = HUGE_VAL;
  int qp;

  switch (quad->dim) {
  case 1:
    total_error = check_quadrature_1d(quad);
    break;
#if DIM_MAX > 1
  case 2:
    total_error = check_quadrature_2d(quad);
    break;
#endif
#if DIM_MAX > 2
  case 3:
    total_error = check_quadrature_3d(quad);
    break;
#endif
  default:
    ERROR_EXIT("quad->dim = %d > %d!??\n", quad->dim, DIM_MAX);
  }
  
  wsum = 0.0;
  for (qp = 0; qp < quad->n_points; qp++) {
    wsum += quad->w[qp];
  }
  MSG("#points: %d\n", quad->n_points);
  MSG("#degree: %d\n", quad->degree);
  MSG("weight sum: %e\n", wsum);
  MSG("total error: %e\n", total_error);
}

/*---8<---------------------------------------------------------------------*/
/*---                                                                    ---*/
/*--------------------------------------------------------------------->8---*/

void print_quadrature(const QUAD *quad)
{
  FUNCNAME("print_quadrature");
  int      i, j;

  MSG("quadrature %s for dimension %d exact on P_%d\n",
      quad->name, quad->dim, quad->degree);
  MSG("%d points with weights and quadrature points:\n", quad->n_points);

  for (i = 0; i < quad->n_points; i++)
  {
    MSG("w[%2d] = %.16le, lambda[%2d] = (", i, quad->w[i], i);
    for (j = 0; j <= quad->dim; j++)
      print_msg("%.16le%s", quad->lambda[i][j], j < quad->dim ? ", " : ")\n");
  }
  return;
}

/*---8<---------------------------------------------------------------------*/
/*---                                                                    ---*/
/*--------------------------------------------------------------------->8---*/

const QUAD *get_lumping_quadrature(int dim)
{
  FUNCNAME("get_lumping_quadrature");
  static const REAL_B vlambda[N_VERTICES_LIMIT] = {
    INIT_BARY_3D(1.0, 0.0, 0.0, 0.0),
    INIT_BARY_3D(0.0, 1.0, 0.0, 0.0),
    INIT_BARY_3D(0.0, 0.0, 1.0, 0.0),
    INIT_BARY_3D(0.0, 0.0, 0.0, 1.0),
  };
#define StdVol 1.0
  static const REAL weight0[N_VERTICES_0D] = { StdVol*1.0 };
  static const REAL weight1[N_VERTICES_1D] = { StdVol*0.5, StdVol*0.5 };
#undef StdVol
#define StdVol 0.5
  static const REAL weight2[N_VERTICES_2D] = {
    StdVol/3.0, StdVol/3.0, StdVol/3.0
  };
#undef StdVol
#define StdVol (1.0/6.0)
  static const REAL weight3[N_VERTICES_3D] = {
    StdVol/4.0, StdVol/4.0, StdVol/4.0, StdVol/4.0
  };
#undef StdVol

  static REAL_D   world0[1], world1[2], world2[3], world3[4];
  static REAL     det0[1], det1[2], det2[3], det3[4];
  static REAL_BD  Lambda0[1], Lambda1[2], Lambda2[3], Lambda3[4];
  static REAL_BDD DLambda0[1], DLambda1[2], DLambda2[3], DLambda3[4];
  static QUAD_METADATA qmd[4] = {
    { { NULL, 0U, world0, { det0, Lambda0, DLambda0, NULL, NULL }}, },
    { { NULL, 0U, world1, { det1, Lambda1, DLambda1, NULL, NULL }}, },
    { { NULL, 0U, world2, { det2, Lambda2, DLambda2, NULL, NULL }}, },
    { { NULL, 0U, world3, { det3, Lambda3, DLambda3, NULL, NULL }}, },
  };

  static const QUAD lumping[4]={
    QR("lump0", 1, 0, 1, vlambda, weight0, &qmd[0]),
    QR("lump1", 1, 1, 2, vlambda, weight1, &qmd[1]),
    QR("lump2", 1, 2, 3, vlambda, weight2, &qmd[2]),
    QR("lump3", 1, 3, 4, vlambda, weight3, &qmd[3])
  };

  TEST_EXIT(dim >= 0 && dim < 4,"invalid dim: %d\n", dim);

  return &lumping[dim];
}

/*--------------------------------------------------------------------------*/
/*  integrate f over reference simplex in R^(dim+1) 			    */
/*--------------------------------------------------------------------------*/

REAL integrate_std_simp(const QUAD *quad, REAL (*f)(const REAL *))
{
  FUNCNAME("integrate_std_simp");
  REAL   val;
  int      i;

  if (!quad || !f)
  {
    if (!quad) ERROR("quad is pointer to NULL; return value is 0.0\n");
    if (!f) ERROR("f() is pointer to NULL; return value is 0.0\n");
    return(0.0);
  }

  for (val = i = 0; i < quad->n_points; i++)
    val += quad->w[i]*(*f)(quad->lambda[i]);

  return(val);
}

/*-------------------------------------------------------------------------*/
/*       new internal struct for adapted quad_fast struct                  */
/*-------------------------------------------------------------------------*/

#define QUAD_FAST_MAGIC "AIQF"

typedef struct _AI_quad_fast _AI_QUAD_FAST;
struct _AI_quad_fast {
  char            magic[4];

  _AI_QUAD_FAST   *next;

  INIT_EL_TAG     bfcts_tag;
  INIT_EL_TAG     quad_tag;

  /* non default values */
  struct {
    const REAL      (*const*phi);     /* [qp][bf] */
    const REAL_B    (*const*grd_phi);
    const REAL_BB   (*const*D2_phi);
    const REAL_BBB  (*const*D3_phi);
    const REAL_BBBB (*const*D4_phi);
  } special;

  /* default values */
  struct {
    int             n_points;
    int             n_bas_fcts;
    const REAL      *w;
    const REAL      (*const*phi);     /* [qp][bf] */
    const REAL_B    (*const*grd_phi);
    const REAL_BB   (*const*D2_phi);
    const REAL_BBB  (*const*D3_phi);
    const REAL_BBBB (*const*D4_phi);
  } dflt;

  /* Storage for the product functions */
  struct {
    const REAL_D    (*const*phi_d);     /* [qp][bf] */
    const REAL_DB   (*const*grd_phi_d);
    const REAL_DBB  (*const*D2_phi_d);
    FLAGS           valid;
    const EL        *cur_el;
    const EL_INFO   *cur_el_info;
  } vect_cache;

  int n_points_max;
  int n_bas_fcts_max;
};

static inline void realloc_quad_caches(QUAD_FAST *qfast)
{
  _AI_QUAD_FAST  *aiqf     = (_AI_QUAD_FAST *)qfast->internal;
  const QUAD     *quad     = qfast->quad;
  const BAS_FCTS *bas_fcts = qfast->bas_fcts;
  bool size_changed =
    quad->n_points_max != qfast->n_points_max
    ||
    bas_fcts->n_bas_fcts_max != qfast->n_bas_fcts_max;
  bool too_small =
    quad->n_points_max > aiqf->n_points_max
    ||
    bas_fcts->n_bas_fcts_max > aiqf->n_bas_fcts_max;
  
  if ((bas_fcts->phi_d != NULL && bas_fcts->dir_pw_const)
      &&
      (bas_fcts->n_bas_fcts_max > aiqf->n_bas_fcts_max
       ||
       qfast->phi_d == NULL)) {
    QUAD_FAST *unchained = (QUAD_FAST *)qfast->unchained;
    if (qfast->phi_d) {
      MEM_FREE(qfast->phi_d, qfast->n_bas_fcts_max, REAL_D);
      if (unchained != qfast && unchained->phi_d != qfast->phi_d) {
	MEM_FREE(qfast->unchained->phi_d,
		 qfast->unchained->n_bas_fcts_max, REAL_D);	
      }      
    }
    qfast->phi_d = MEM_ALLOC(bas_fcts->n_bas_fcts_max, const REAL_D);
    unchained->phi_d = qfast->phi_d;
  }

  /* values */
  if ((qfast->init_flag & INIT_PHI) &&
      (too_small || aiqf->special.phi == NULL)) {
    if (aiqf->special.phi) {
      MAT_FREE(aiqf->special.phi,
	       qfast->n_points_max, qfast->n_bas_fcts_max, REAL);
    }
    aiqf->special.phi = (const REAL *const*)
      MAT_ALLOC(quad->n_points_max, bas_fcts->n_bas_fcts_max, REAL);
  }

  /* first derivatives */
  if ((qfast->init_flag & INIT_GRD_PHI) &&
      (too_small || aiqf->special.grd_phi == NULL)) {
    if (aiqf->special.grd_phi) {
      MAT_FREE(aiqf->special.grd_phi, 
	       qfast->n_points_max, qfast->n_bas_fcts_max, REAL_B);
    }
    aiqf->special.grd_phi = (const REAL_B *const*)
      MAT_ALLOC(quad->n_points_max, bas_fcts->n_bas_fcts_max, REAL_B);
  }

  /* second derivatives */
  if ((qfast->init_flag & INIT_D2_PHI) &&
      (too_small || aiqf->special.D2_phi == NULL)) {
    if (aiqf->special.D2_phi) {
      MAT_FREE(aiqf->special.D2_phi, 
	       qfast->n_points_max, qfast->n_bas_fcts_max, REAL_BB);
    }
    aiqf->special.D2_phi = (const REAL_BB *const*)
      MAT_ALLOC(quad->n_points_max, bas_fcts->n_bas_fcts_max, REAL_BB);
  }

    /* third derivatives */
  if ((qfast->init_flag & INIT_D3_PHI) &&
      (too_small || aiqf->special.D3_phi == NULL)) {
    if (aiqf->special.D3_phi) {
      MAT_FREE(aiqf->special.D3_phi, 
	       qfast->n_points_max, qfast->n_bas_fcts_max, REAL_BBB);
    }
    aiqf->special.D3_phi = (const REAL_BBB *const*)
      MAT_ALLOC(quad->n_points_max, bas_fcts->n_bas_fcts_max, REAL_BBB);
  }
  
  /* fourth derivatives */
  if ((qfast->init_flag & INIT_D4_PHI) &&
      (too_small || aiqf->special.D4_phi == NULL)) {
    if (aiqf->special.D4_phi) {
      MAT_FREE(aiqf->special.D4_phi, 
	       qfast->n_points_max, qfast->n_bas_fcts_max, REAL_BBBB);
    }
    aiqf->special.D4_phi = (const REAL_BBBB *const*)
      MAT_ALLOC(quad->n_points_max, bas_fcts->n_bas_fcts_max, REAL_BBBB);
  }

  /* values, Jacobian, Hessian for vector valued stuff. */
  if (bas_fcts->phi_d) {
    if ((qfast->init_flag & INIT_PHI) &&
	(too_small || aiqf->vect_cache.phi_d == NULL)) {
      if (aiqf->vect_cache.phi_d) {
	MAT_FREE(aiqf->vect_cache.phi_d,
		 qfast->n_points_max, qfast->n_bas_fcts_max, REAL_D);
      }
      aiqf->vect_cache.phi_d = (const REAL_D *const*)
	MAT_ALLOC(quad->n_points_max, bas_fcts->n_bas_fcts_max, REAL_D);
    }
    if ((qfast->init_flag & INIT_GRD_PHI) &&
	(too_small || aiqf->vect_cache.grd_phi_d == NULL)) {
      if (aiqf->vect_cache.grd_phi_d) {
	MAT_FREE(aiqf->vect_cache.grd_phi_d, 
		 qfast->n_points_max, qfast->n_bas_fcts_max, REAL_DB);
      }
      aiqf->vect_cache.grd_phi_d = (const REAL_DB *const*)
	  MAT_ALLOC(quad->n_points_max, bas_fcts->n_bas_fcts_max, REAL_DB);
    }
    if ((qfast->init_flag & INIT_D2_PHI) &&
	(too_small || aiqf->vect_cache.D2_phi_d == NULL)) {
      if (aiqf->vect_cache.D2_phi_d) {
	MAT_FREE(aiqf->vect_cache.D2_phi_d, 
		 qfast->n_points_max, qfast->n_bas_fcts_max, REAL_DBB);
      }
      aiqf->vect_cache.D2_phi_d = (const REAL_DBB *const*)
	  MAT_ALLOC(quad->n_points_max, bas_fcts->n_bas_fcts_max, REAL_DBB);
    }
  }

  if (size_changed) {
    qfast->n_points_max   = quad->n_points_max;
    qfast->n_bas_fcts_max = bas_fcts->n_bas_fcts_max;
  }
  if (too_small) {
    aiqf->n_points_max   = quad->n_points_max;
    aiqf->n_bas_fcts_max = bas_fcts->n_bas_fcts_max;
  }
}

static inline void default_quad_caches(QUAD_FAST *qfast)
{
  _AI_QUAD_FAST *aiqf = (_AI_QUAD_FAST *)qfast->internal;

  qfast->n_points    = aiqf->dflt.n_points;
  qfast->n_bas_fcts  = aiqf->dflt.n_bas_fcts;
  qfast->w           = aiqf->dflt.w;
  qfast->phi         = aiqf->dflt.phi;
  qfast->grd_phi     = aiqf->dflt.grd_phi;
  qfast->D2_phi      = aiqf->dflt.D2_phi;
  qfast->D3_phi      = aiqf->dflt.D3_phi;
  qfast->D4_phi      = aiqf->dflt.D4_phi;
}

static inline void fill_quad_caches(QUAD_FAST *qfast)
{
  const BAS_FCTS *bas_fcts = qfast->bas_fcts;
  int qp, bf;

  if (qfast->init_flag & INIT_PHI) {
    REAL *const*phi = (REAL *const*)qfast->phi;

    for (qp = 0; qp < qfast->n_points; qp ++) {
      for (bf = 0; bf < qfast->n_bas_fcts; bf++) {
	phi[qp][bf] = PHI(bas_fcts, bf, qfast->quad->lambda[qp]);
      }
    }
  }
    
  if (qfast->init_flag & INIT_GRD_PHI) {
    REAL_B *const*grd_phi = (REAL_B *const*)qfast->grd_phi;
    switch (bas_fcts->unchained->degree) {
    case 0:
      for (qp = 0; qp < qfast->n_points; qp ++) {
	memset(grd_phi[qp], 0, qfast->n_bas_fcts*sizeof(REAL_B));
      }
      break;
    case 1:
      if (qfast->n_points > 0) {
	for (bf = 0; bf < qfast->n_bas_fcts; bf++) {
	  COPY_BAR(DIM_MAX,
		   GRD_PHI(bas_fcts, bf, qfast->quad->lambda[0]),
		   grd_phi[0][bf]);
	}
	for (qp = 1; qp < qfast->n_points; qp ++) {
	  for (bf = 0; bf < qfast->n_bas_fcts; bf++) {
	    COPY_BAR(DIM_MAX, grd_phi[0][bf], grd_phi[qp][bf]);
	  }
	}
      }
      break;
    default:
      for (qp = 0; qp < qfast->n_points; qp ++) {
	for (bf = 0; bf < qfast->n_bas_fcts; bf++) {
	  COPY_BAR(DIM_MAX,
		   GRD_PHI(bas_fcts, bf, qfast->quad->lambda[qp]),
		   grd_phi[qp][bf]);
	}
      }
      break;
    }
  }

  if(qfast->init_flag & INIT_D2_PHI) {
    REAL_BB *const*D2_phi = (REAL_BB *const*)qfast->D2_phi;
    switch (bas_fcts->unchained->degree) {
    case 0:
    case 1:
      for (qp = 0; qp < qfast->n_points; qp ++) {
	memset(D2_phi[qp], 0, qfast->n_bas_fcts*sizeof(REAL_BB));
      }
      break;
    case 2:
      if (qfast->n_points > 0) {
	for (bf = 0; bf < qfast->n_bas_fcts; bf++) {
	  MCOPY_BAR(DIM_MAX, 
		    D2_PHI(bas_fcts, bf, qfast->quad->lambda[0]),
		    D2_phi[0][bf]);
	}
	for (qp = 1; qp < qfast->n_points; qp ++) {
	  for (bf = 0; bf < qfast->n_bas_fcts; bf++) {
	    MCOPY_BAR(DIM_MAX, (const REAL_B *)D2_phi[0][bf], D2_phi[qp][bf]);
	  }
	}
      }
      break;
    default:
      for (qp = 0; qp < qfast->n_points; qp ++) {
	for (bf = 0; bf < qfast->n_bas_fcts; bf++) {
	  MCOPY_BAR(DIM_MAX, 
		    D2_PHI(bas_fcts, bf, qfast->quad->lambda[qp]),
		    D2_phi[qp][bf]);
	}
      }
      break;
    }
  }

  if(qfast->init_flag & INIT_D3_PHI) {
    REAL_BB *const*D3_phi = (REAL_BB *const*)qfast->D3_phi;
    for (qp = 0; qp < qfast->n_points; qp ++) {
      for (bf = 0; bf < qfast->n_bas_fcts; bf++) {
	memcpy(D3_phi[qp][bf], D3_PHI(bas_fcts, bf, qfast->quad->lambda[qp]),
	       sizeof(REAL_BBB));
      }
    }
  }

  if(qfast->init_flag & INIT_D4_PHI) {
    REAL_BB *const*D4_phi = (REAL_BB *const*)qfast->D4_phi;
    for (qp = 0; qp < qfast->n_points; qp ++) {
      for (bf = 0; bf < qfast->n_bas_fcts; bf++) {
	memcpy(D4_phi[qp][bf], D4_PHI(bas_fcts, bf, qfast->quad->lambda[qp]),
	       sizeof(REAL_BBB));
      }
    }
  }
}

static inline void fill_quad_caches_tangential(QUAD_FAST *qfast)
{
  const BAS_FCTS *bas_fcts    = qfast->bas_fcts;
  const QUAD     *quad        = qfast->quad;
  int            wall         = quad->subsplx;
  int            n_wall_bfcts = bas_fcts->n_trace_bas_fcts[wall];
  int            bf, qp, i, alpha, beta, gamma;

  if (qfast->init_flag & INIT_PHI) {
    REAL *const*phi = (REAL *const*)qfast->phi;

    for (qp = 0; qp < quad->n_points; qp ++) {
      for (i = 0; i < n_wall_bfcts; i++) {
	bf = bas_fcts->trace_dof_map[0][0][wall][i];
	phi[qp][bf] = PHI(bas_fcts, bf, quad->lambda[qp]);
      }
    }
  }
    
  if (qfast->init_flag & INIT_GRD_PHI) {
    REAL_B *const*grd_phi = (REAL_B *const*)qfast->grd_phi;

    switch (bas_fcts->unchained->degree) {
    case 0:
      for (qp = 0; qp < qfast->n_points; qp ++) {
	memset(grd_phi[qp], 0, qfast->n_bas_fcts*sizeof(REAL_B));
      }
      break;
    case 1:
      if (quad->n_points > 0) {
	for (i = 0; i < n_wall_bfcts; i++) {
	  bf = bas_fcts->trace_dof_map[0][0][wall][i];
	  COPY_BAR(DIM_MAX,
		   GRD_PHI(bas_fcts, bf, quad->lambda[0]), grd_phi[0][bf]);
	  /* The following is automatically zero for Lagrange basis
	   * functions:
	   */
	  grd_phi[0][bf][wall] = 0.0;
	}
	for (qp = 1; qp < quad->n_points; qp ++) {
	  for (i = 0; i < n_wall_bfcts; i++) {
	    bf = bas_fcts->trace_dof_map[0][0][wall][i];
	    COPY_BAR(DIM_MAX, grd_phi[0][bf], grd_phi[qp][bf]);
	  }
	}
      }
      break;
    default:
      for (qp = 0; qp < quad->n_points; qp ++) {
	for (i = 0; i < n_wall_bfcts; i++) {
	  bf = bas_fcts->trace_dof_map[0][0][wall][i];
	  COPY_BAR(DIM_MAX,
		   GRD_PHI(bas_fcts, bf, quad->lambda[qp]), grd_phi[qp][bf]);
	  /* The following is automatically zero for Lagrange basis
	   * functions:
	   */
	  grd_phi[qp][bf][wall] = 0.0;
	}
      }
      break;
    }
  }

  if(qfast->init_flag & INIT_D2_PHI) {
    REAL_BB *const*D2_phi = (REAL_BB *const*)qfast->D2_phi;

    switch (bas_fcts->unchained->degree) {
    case 0:
    case 1:
      for (qp = 0; qp < qfast->n_points; qp ++) {
	memset(D2_phi[qp], 0, qfast->n_bas_fcts*sizeof(REAL_BB));
      }
      break;
    case 2:
      if (quad->n_points > 0) {
	for (i = 0; i < n_wall_bfcts; i++) {
	  bf = bas_fcts->trace_dof_map[0][0][wall][i];
	  MCOPY_BAR(DIM_MAX, 
		    D2_PHI(bas_fcts, bf, quad->lambda[0]), D2_phi[0][bf]);
	  for (alpha = 0; alpha < N_LAMBDA_MAX; alpha++) {
	    D2_phi[0][bf][wall][alpha] = 
	      D2_phi[0][bf][alpha][wall] = 0.0;
	  }
	}
	for (qp = 1; qp < quad->n_points; qp ++) {
	  for (i = 0; i < n_wall_bfcts; i++) {
	    bf = bas_fcts->trace_dof_map[0][0][wall][i];
	    MCOPY_BAR(DIM_MAX, (const REAL_B *)D2_phi[0][bf], D2_phi[qp][bf]);
	  }
	}
      }
      break;
    default:
      for (qp = 0; qp < quad->n_points; qp ++) {
	for (i = 0; i < n_wall_bfcts; i++) {
	  bf = bas_fcts->trace_dof_map[0][0][wall][i];
	  MCOPY_BAR(DIM_MAX, 
		    D2_PHI(bas_fcts, bf, quad->lambda[qp]), D2_phi[qp][bf]);
	  for (alpha = 0; alpha < N_LAMBDA_MAX; alpha++) {
	    D2_phi[qp][bf][wall][alpha] = 
	      D2_phi[qp][bf][alpha][wall] = 0.0;
	  }
	}
      }
      break;
    }
  }

  if(qfast->init_flag & INIT_D3_PHI) {
    REAL_BBB *const*D3_phi = (REAL_BBB *const*)qfast->D3_phi;
      
    for (qp = 0; qp < quad->n_points; qp ++) {
      for (i = 0; i < n_wall_bfcts; i++) {
	bf = bas_fcts->trace_dof_map[0][0][wall][i];
	memcpy(D3_phi[qp][bf], D3_PHI(bas_fcts, bf, quad->lambda[qp]),
	       sizeof(REAL_BBB));
	for (alpha = 0; alpha < N_LAMBDA_MAX; alpha++) {
	  for (beta = 0; beta < N_LAMBDA_MAX; beta++) {
	    D3_phi[qp][bf][wall][alpha][beta] =
	      D3_phi[qp][bf][alpha][wall][beta] =
	      D3_phi[qp][bf][alpha][beta][wall] = 0.0;
	  }
	}
      }
    }
  }

  if(qfast->init_flag & INIT_D4_PHI) {
    REAL_BBBB *const*D4_phi = (REAL_BBBB *const*)qfast->D4_phi;
      
    for (qp = 0; qp < quad->n_points; qp ++) {
      for (i = 0; i < n_wall_bfcts; i++) {
	bf = bas_fcts->trace_dof_map[0][0][wall][i];
	memcpy(D4_phi[qp][bf], D4_PHI(bas_fcts, bf, quad->lambda[qp]),
	       sizeof(REAL_BBBB));
	for (alpha = 0; alpha < N_LAMBDA_MAX; alpha++) {
	  for (beta = 0; beta < N_LAMBDA_MAX; beta++) {
	    for (gamma = 0; gamma < N_LAMBDA_MAX; gamma++) {
	      D4_phi[qp][bf][wall][alpha][beta][gamma] =
		D4_phi[qp][bf][alpha][wall][beta][gamma] =
		D4_phi[qp][bf][alpha][beta][wall][gamma] =
		D4_phi[qp][bf][alpha][beta][gamma][wall] = 0.0;
	    }
	  }
	}
      }
    }
  }

}

static inline void alloc_dflt_caches(QUAD_FAST *quad_fast, FLAGS init_flag);

static INIT_EL_TAG qfast_init_element(const EL_INFO *el_info, void *thisptr)
{
  QUAD_FAST      *qfast = (QUAD_FAST *)thisptr;
  _AI_QUAD_FAST  *aiqf  = (_AI_QUAD_FAST *)qfast->internal;
  const BAS_FCTS *bas_fcts = qfast->bas_fcts;
  INIT_EL_TAG    bfcts_tag, quad_tag;

  bfcts_tag = INIT_ELEMENT_SINGLE(el_info, bas_fcts);
  quad_tag  = INIT_ELEMENT(el_info, qfast->quad);

  if (__UNLIKELY__(el_info == NULL)) {
    /* initialization requested, possibly resize the caches */
    realloc_quad_caches(qfast);
    aiqf->quad_tag = aiqf->bfcts_tag = INIT_EL_TAG_NONE;
    aiqf->vect_cache.cur_el = NULL;
    aiqf->vect_cache.valid  = 0;
  } else if (__builtin_expect(bas_fcts->rdim > 1, false) &&
	     (el_info->el != aiqf->vect_cache.cur_el ||
	      el_info != aiqf->vect_cache.cur_el_info)) {
    /* Always be sure to invalidate the cache for vector valued basis
     * functions if called with a different element.
     */
    aiqf->vect_cache.cur_el      = el_info->el;
    aiqf->vect_cache.cur_el_info = el_info;
    aiqf->vect_cache.valid       = 0;
    if (__LIKELY__(bas_fcts->dir_pw_const)) {
      /* Recompute the directional factor */
      int ib;
      for (ib = 0; ib < bas_fcts->n_bas_fcts; ib++) {
	COPY_DOW(PHI_D(bas_fcts, ib, NULL), (REAL *)qfast->phi_d[ib]);
      }
    }
  }

  /* First case: no change in tags nothing to do, use already computed
   * values.
   */

  /* fast path */
  if (__LIKELY__(quad_tag == aiqf->quad_tag && bfcts_tag == aiqf->bfcts_tag)) {
    return INIT_EL_TAG_CTX_TAG(&qfast->tag_ctx);
  }

  aiqf->bfcts_tag = bfcts_tag;
  aiqf->quad_tag  = quad_tag;

  /* second case: tags == 0, that means default initialising, use
   * default values
   */
  /* fast path */
  if (__LIKELY__(
	quad_tag == INIT_EL_TAG_DFLT && bfcts_tag == INIT_EL_TAG_DFLT)) {
    
    default_quad_caches(qfast);
    INIT_EL_TAG_CTX_DFLT(&qfast->tag_ctx);

    return INIT_EL_TAG_CTX_TAG(&qfast->tag_ctx);
  }

  /* third case: change in tags, compute new values */

  /* slow path */

  qfast->n_points   = qfast->quad->n_points;
  qfast->n_bas_fcts = bas_fcts->n_bas_fcts;
  qfast->w          = qfast->quad->w;
  qfast->phi        = aiqf->special.phi;
  qfast->grd_phi    = aiqf->special.grd_phi;
  qfast->D2_phi     = aiqf->special.D2_phi;
  qfast->D3_phi     = aiqf->special.D3_phi;
  qfast->D4_phi     = aiqf->special.D4_phi;

  if (__LIKELY__(
	quad_tag != INIT_EL_TAG_NULL && bfcts_tag != INIT_EL_TAG_NULL)) {
    /* as we have new quadrature points, or changed basis functions,
     * we have to compute the bas_fcts at these points anew if the
     * init flag is set. Also resize the caches, if needed.
     */
    /* resize_quad_caches(aiqf); should not be needed here */

    if (qfast->init_flag & INIT_TANGENTIAL) {
      fill_quad_caches_tangential(qfast);
    } else {
      fill_quad_caches(qfast);
    }

    /* set new tag, tag must be different from INIT_EL_TAG_DFLT/NULL */
    INIT_EL_TAG_CTX_UNIQ(&qfast->tag_ctx);
  } else {
    INIT_EL_TAG_CTX_NULL(&qfast->tag_ctx);
  }

  return INIT_EL_TAG_CTX_TAG(&qfast->tag_ctx);
}

/* Chain initializer, walk the entire chain and initialize each
 * element in turn. This should be (algorithmically) the same as
 * bfcts_chain_init_element().
 */
static
INIT_EL_TAG qfast_chain_init_element(const EL_INFO *el_info, void *thisptr)
{
  QUAD_FAST   *quad_fast = (QUAD_FAST *)thisptr;
  INIT_EL_TAG chain_tag = INIT_EL_TAG_NONE;
  INIT_EL_TAG old_tag;
  bool        new_tag = false;

  CHAIN_DO(quad_fast, QUAD_FAST) {
    if (INIT_ELEMENT_NEEDED(quad_fast->unchained)) {
      old_tag = INIT_EL_TAG_CTX_TAG(&quad_fast->tag_ctx);
      chain_tag |= INIT_ELEMENT_SINGLE(el_info, quad_fast);
      if (old_tag != INIT_EL_TAG_CTX_TAG(&quad_fast->tag_ctx)) {
	new_tag = true;
      }
    }
  } CHAIN_WHILE(quad_fast, QUAD_FAST);

  if (chain_tag == INIT_EL_TAG_NONE) {
    chain_tag = INIT_EL_TAG_DFLT;
  }
  if (chain_tag == INIT_EL_TAG_DFLT || chain_tag == INIT_EL_TAG_NULL) {
    return chain_tag;
  } else {
    if (new_tag) {
      INIT_EL_TAG_CTX_UNIQ(&quad_fast->tag_ctx);
    }
    return INIT_EL_TAG_CTX_TAG(&quad_fast->tag_ctx);
  }
}

static inline
QUAD_FAST *__get_quad_fast_single(const BAS_FCTS *bas_fcts,
				  const QUAD *quad,
				  FLAGS init_flag,
				  bool init_element_needed,
				  bool dont_reuse)
{
  FUNCNAME("__get_quad_fast_single");
  _AI_QFAST_LIST **first_fast;
  _AI_QFAST_LIST *qfl_pos = NULL;
  QUAD_FAST      *quad_fast;
  union {
    void          **vptr;
    _AI_QFAST_LIST **tptr;
  } alias;
  _AI_QUAD_FAST  *ai_qf;

  DEBUG_TEST_EXIT((init_flag & INIT_TANGENTIAL) == 0 || quad->codim == 1,
		  "INIT_TANGENTIAL only makes sense with codim > 0.\n");

  /* anchor to the list, the list-head is stored in the meta-data of
   * the quad structure.
   */
  if (init_flag & INIT_TANGENTIAL) {
    alias.vptr = &((QUAD_METADATA *)quad->metadata)->quad_fast_head.tangential;
  } else {
    alias.vptr = &((QUAD_METADATA *)quad->metadata)->quad_fast_head.ordinary;
  }
  first_fast = alias.tptr;

  if (bas_fcts->rdim > 1) {
    if (init_flag & INIT_D2_PHI) {
      init_flag |= INIT_GRD_PHI;
    }
    if (init_flag & INIT_GRD_PHI) {
      init_flag |= INIT_PHI;
    }
  }

  if (!dont_reuse) {
    if (init_element_needed) {
      /* In the presence of per-element initialization avoid
       * initialising more components than requested by the caller,
       * otherwise the element initializers have too much work to do.
       */

      for (qfl_pos = *first_fast; qfl_pos != NULL; qfl_pos = qfl_pos->next) {
	quad_fast = qfl_pos->quad_fast;
	if (quad_fast->bas_fcts == bas_fcts &&
	    quad_fast->quad == quad && 
	    quad_fast->init_flag == init_flag) {
	  break;
	}
      }
    } else {
      /* Re-use any quadrature which has all needed bits set; add data
       * to already existing QUAD_FAST objects to keep the number of
       * QUAD_FAST objects as small as possible.
       */
      for (qfl_pos = *first_fast; qfl_pos != NULL; qfl_pos = qfl_pos->next) {
	quad_fast = qfl_pos->quad_fast;
	if (quad_fast->bas_fcts == bas_fcts && quad_fast->quad == quad) {
	  break;
	}
      }
    }
  }
  
  if (dont_reuse || !qfl_pos) {

    /* list management */
    qfl_pos = MEM_ALLOC(1, _AI_QFAST_LIST);
    qfl_pos->next = *first_fast;
    *first_fast = qfl_pos;

    /* now for the real stuff */
    qfl_pos->quad_fast = quad_fast = MEM_CALLOC(1, QUAD_FAST);
    quad_fast->internal = ai_qf = MEM_CALLOC(1, _AI_QUAD_FAST);
    memcpy(ai_qf->magic, QUAD_FAST_MAGIC, sizeof(ai_qf->magic));

    quad_fast->quad     = quad;
    quad_fast->bas_fcts = bas_fcts;
    quad_fast->dim      = quad->dim;

    /* initialize the default cache */
    ai_qf->dflt.n_points   = quad->n_points;
    ai_qf->dflt.n_bas_fcts = bas_fcts->n_bas_fcts;
    ai_qf->dflt.w          = quad->w;

    if (init_element_needed) {

      INIT_ELEMENT_DEFUN(quad_fast, qfast_init_element,
			 bas_fcts->fill_flags|quad->fill_flags);

      /* trigger allocation in the default initializer */
      ai_qf->n_points_max = ai_qf->n_bas_fcts_max =
	quad_fast->n_points_max = quad_fast->n_bas_fcts_max = 0;
    } else {
      ai_qf->n_points_max = quad_fast->n_points_max =
	quad->n_points_max;
      ai_qf->n_bas_fcts_max = quad_fast->n_bas_fcts_max =
	bas_fcts->n_bas_fcts_max;
    }

    quad_fast->init_flag = 0U;

    CHAIN_INIT(quad_fast);
    quad_fast->unchained = quad_fast;

  } else {
    quad_fast = qfl_pos->quad_fast;
  }

  alloc_dflt_caches(quad_fast, init_flag);

  if ((quad_fast->init_flag & init_flag) != init_flag) {
    ERROR("could not initialize quad_fast, returning pointer to NULL\n");
    return NULL;
  }
  
  INIT_OBJECT_SINGLE(quad_fast);

  if (!init_element_needed && bas_fcts->dir_pw_const && bas_fcts->phi_d) {
    int ib;
    for (ib = 0; ib < bas_fcts->n_bas_fcts; ib++) {
      COPY_DOW(PHI_D(bas_fcts, ib, NULL), (REAL *)quad_fast->phi_d[ib]);
    }
  }

  return quad_fast;
}

/* Take chained basis-functions into account. */
const QUAD_FAST *get_quad_fast(const BAS_FCTS *bas_fcts,
			       const QUAD *quad,
			       FLAGS init_flag)
{
  QUAD_FAST *quad_fast, *qfast_chain, *unchained;
  const BAS_FCTS *bfcts_chain;
  bool init_el_needed;

  init_el_needed = INIT_ELEMENT_NEEDED(quad);
  CHAIN_DO(bas_fcts, const BAS_FCTS) {
    init_el_needed =
      init_el_needed || INIT_ELEMENT_NEEDED(bas_fcts->unchained);
  } CHAIN_WHILE(bas_fcts, const BAS_FCTS);

  INIT_OBJECT(quad);
  INIT_OBJECT(bas_fcts);

  quad_fast = __get_quad_fast_single(
    bas_fcts, quad, init_flag, init_el_needed, false /* reuse old structs */);
  if (!CHAIN_SINGLE(bas_fcts) && CHAIN_SINGLE(quad_fast)) {
    quad_fast->unchained = unchained = MEM_CALLOC(1, QUAD_FAST);
    *unchained = *quad_fast;
    CHAIN_INIT(unchained);
    if (!INIT_ELEMENT_NEEDED(quad) &&
	!INIT_ELEMENT_NEEDED(bas_fcts->unchained)) {
      INIT_ELEMENT_DEFUN(unchained,
			 NULL,
			 quad->fill_flags|bas_fcts->unchained->fill_flags);
    }
    CHAIN_FOREACH(bfcts_chain, bas_fcts, const BAS_FCTS) {
      qfast_chain = __get_quad_fast_single(
	bfcts_chain, quad, init_flag, init_el_needed, true /* dont reuse */);
      CHAIN_ADD_TAIL(quad_fast, qfast_chain);
      quad_fast->fill_flags |= qfast_chain->fill_flags;
      qfast_chain->unchained = unchained = MEM_CALLOC(1, QUAD_FAST);
      *unchained = *qfast_chain;
      CHAIN_INIT(unchained);
      if (!INIT_ELEMENT_NEEDED(quad) &&
	  !INIT_ELEMENT_NEEDED(bfcts_chain->unchained)) {
	INIT_ELEMENT_DEFUN(unchained,
			   NULL,
			   quad->fill_flags|bfcts_chain->unchained->fill_flags);
      }
    }
    if (init_el_needed) {
      CHAIN_DO(quad_fast, QUAD_FAST) {
	quad_fast->init_element = qfast_chain_init_element;
      } CHAIN_WHILE(quad_fast, QUAD_FAST);
    }
  } else {
    /* otherwise we are re-using a cached chain. */
    if (quad_fast->unchained != quad_fast) {
      INIT_OBJECT_SINGLE(quad_fast);
    }
    CHAIN_FOREACH(qfast_chain, quad_fast, QUAD_FAST) {
      if ((qfast_chain->init_flag & init_flag) != init_flag) {
	alloc_dflt_caches(qfast_chain, init_flag);
      }
      INIT_OBJECT_SINGLE(qfast_chain);
      if (qfast_chain->unchained != qfast_chain) {
	INIT_OBJECT(qfast_chain->unchained);
      }
    }
  }

  return quad_fast;
}

static inline void alloc_dflt_caches(QUAD_FAST *quad_fast, FLAGS init_flag)
{
  _AI_QUAD_FAST  *ai_qf    = (_AI_QUAD_FAST *)quad_fast->internal;
  const BAS_FCTS *bas_fcts = quad_fast->bas_fcts;
  const QUAD *quad         = quad_fast->quad;
  int n_points;
  int n_bas_fcts;

  if ((init_flag ^ quad_fast->init_flag) != 0) {

    n_points   = quad->n_points;

    n_bas_fcts = bas_fcts->n_bas_fcts;

    if (bas_fcts->phi_d != NULL && bas_fcts->dir_pw_const) {
      quad_fast->phi_d = MEM_CALLOC(bas_fcts->n_bas_fcts_max, const REAL_D);
    }

    if (!ai_qf->dflt.phi && (init_flag & INIT_PHI)) {
      ai_qf->dflt.phi =
	(const REAL *const*)MAT_ALLOC(n_points, n_bas_fcts, REAL);
      quad_fast->init_flag |= INIT_PHI;
    }
    
    if (!ai_qf->dflt.grd_phi && (init_flag & INIT_GRD_PHI)) {
      ai_qf->dflt.grd_phi =
	(const REAL_B *const*)(MAT_ALLOC(n_points, n_bas_fcts, REAL_B));
      quad_fast->init_flag |= INIT_GRD_PHI;
    }

    if (!ai_qf->dflt.D2_phi && (init_flag & INIT_D2_PHI)) {
      ai_qf->dflt.D2_phi =
	(const REAL_BB *const*)(MAT_ALLOC(n_points, n_bas_fcts, REAL_BB));    
      quad_fast->init_flag |= INIT_D2_PHI;
    }

    if (!ai_qf->dflt.D3_phi && (init_flag & INIT_D3_PHI)) {
      TEST_EXIT(bas_fcts->D3_phi != NULL,
		"Requesting caching of non-existent 3rd derivatives.\n");
      ai_qf->dflt.D3_phi =
	(const REAL_BBB *const*)(MAT_ALLOC(n_points, n_bas_fcts, REAL_BBB));    
      quad_fast->init_flag |= INIT_D3_PHI;
    }

    if (!ai_qf->dflt.D4_phi && (init_flag & INIT_D4_PHI)) {
      TEST_EXIT(bas_fcts->D4_phi != NULL,
		"Requesting caching of non-existent fourth derivatives.\n");
      ai_qf->dflt.D4_phi =
	(const REAL_BBBB *const*)(MAT_ALLOC(n_points, n_bas_fcts, REAL_BBBB));
      quad_fast->init_flag |= INIT_D4_PHI;
    }
    
    default_quad_caches(quad_fast);

    if (init_flag & INIT_TANGENTIAL) {
      fill_quad_caches_tangential(quad_fast);
      quad_fast->init_flag |= INIT_TANGENTIAL;
    } else {
      fill_quad_caches(quad_fast);
    }

  }
}

/* Vector-valued basis functions are always factored in to a scalar
 * part which stays potentially the same on each element
 * (w.r.t. barycentric co-ordinates) and a direction which in general
 * depends on the geometry of the mesh element. We _never_ cache the
 * direction. Instead, the following three functions generate the
 * values of the product basis functions at the quadrature nodes from
 * the cached scalar factor and the not-cached vector valued part.
 */
const REAL_D *const*get_quad_fast_phi_dow(const QUAD_FAST *cache)
{
  FUNCNAME("get_quad_fast_phi_dow");
  _AI_QUAD_FAST *aiqf = (_AI_QUAD_FAST *)cache->internal;

  DEBUG_TEST_EXIT(cache->init_flag & INIT_PHI,
		  "INIT_PHI not set in cache->init_flag\n");

  if (aiqf->vect_cache.valid & INIT_PHI) {
    return aiqf->vect_cache.phi_d;
  } else {
    REAL_D *const*phi_d = (REAL_D *const*)aiqf->vect_cache.phi_d;
    const BAS_FCTS *bfcts = cache->bas_fcts;
    int bf,  iq;
    
    if (bfcts->dir_pw_const) {
      for (bf = 0; bf < cache->n_bas_fcts; bf++) {
	const REAL *phival = cache->phi_d[bf];
	for (iq = 0; iq < cache->n_points; iq++) {
	  AXEY_DOW(cache->phi[iq][bf], phival, phi_d[iq][bf]);
	}
      }
    } else {
      for (iq = 0; iq < cache->n_points; iq++) {
	for (bf = 0; bf < cache->n_bas_fcts; bf++) {
	  const REAL *phival = PHI_D(bfcts, bf, cache->quad->lambda[iq]);
	  AXEY_DOW(cache->phi[iq][bf], phival, phi_d[iq][bf]);
	}
      }
    }
  }

  aiqf->vect_cache.valid |= INIT_PHI;
  
  return aiqf->vect_cache.phi_d;
}

const REAL_DB *const*get_quad_fast_grd_phi_dow(const QUAD_FAST *cache)
{
  FUNCNAME("get_quad_fast_grd_phi_dow");
  _AI_QUAD_FAST *aiqf = (_AI_QUAD_FAST *)cache->internal;

  DEBUG_TEST_EXIT(cache->init_flag & INIT_GRD_PHI,
		  "INIT_GRD_PHI not set in cache->init_flag\n");

  if (aiqf->vect_cache.valid & INIT_GRD_PHI) {
    return aiqf->vect_cache.grd_phi_d;
  } else {
    /* Pure vector-valued case */
    REAL_DB *const*grd_phi_d = (REAL_DB *const*)aiqf->vect_cache.grd_phi_d;
    const BAS_FCTS *bfcts = cache->bas_fcts;
    int bf, iq, i;

    if (bfcts->dir_pw_const) {
      for (bf = 0; bf < cache->n_bas_fcts; bf++) {
	const REAL *phival = cache->phi_d[bf];
	for (iq = 0; iq < cache->n_points; iq++) {
	  for (i = 0; i < DIM_OF_WORLD; i++) {
	    AXEY_BAR(DIM_MAX,
		     phival[i], cache->grd_phi[iq][bf], grd_phi_d[iq][bf][i]);
	  }
	}
      }
    } else {
      for (iq = 0; iq < cache->n_points; iq++) {
	for (bf = 0; bf < cache->n_bas_fcts; bf++) {
	  const REAL_B *grdphi = GRD_PHI_D(bfcts, bf, cache->quad->lambda[iq]);
	  const REAL *phival = PHI_D(bfcts, bf, cache->quad->lambda[iq]);

	  for (i = 0; i < DIM_OF_WORLD; i++) {
	    AXEY_BAR(DIM_MAX,
		     phival[i], cache->grd_phi[iq][bf], grd_phi_d[iq][bf][i]);
	    AXPY_BAR(DIM_MAX,
		     cache->phi[iq][bf], grdphi[i], grd_phi_d[iq][bf][i]);
	  }
	}
      }
    }
  }

  aiqf->vect_cache.valid |= INIT_GRD_PHI;
  
  return aiqf->vect_cache.grd_phi_d;
}

const REAL_DBB *const*get_quad_fast_D2_phi_dow(const QUAD_FAST *cache)
{
  FUNCNAME("get_quad_fast_grd_phi_dow");
  _AI_QUAD_FAST *aiqf = (_AI_QUAD_FAST *)cache->internal;

  DEBUG_TEST_EXIT(cache->init_flag & INIT_D2_PHI,
		  "INIT_GRD_PHI not set in cache->init_flag\n");

  if (aiqf->vect_cache.valid & INIT_D2_PHI) {
    return aiqf->vect_cache.D2_phi_d;
  } else {
    /* Pure vector-valued case */
    REAL_DBB *const*D2_phi_d = (REAL_DBB *const*)aiqf->vect_cache.D2_phi_d;
    const BAS_FCTS *bfcts = cache->bas_fcts;
    int bf, iq, i, k, l;

    if (bfcts->dir_pw_const) {    
      for (bf = 0; bf < cache->n_bas_fcts; bf++) {
	const REAL *phival = cache->phi_d[bf];
	for (iq = 0; iq < cache->n_points; iq++) {
	  for (i = 0; i < DIM_OF_WORLD; i++) {
	    MAXEY_BAR(DIM_MAX,
		    phival[i], cache->D2_phi[iq][bf], D2_phi_d[iq][bf][i]);
	  }
	}
      }
    } else {
      for (iq = 0; iq < cache->n_points; iq++) {
	for (bf = 0; bf < cache->n_bas_fcts; bf++) {
	  const REAL_BB *D2phi = D2_PHI_D(bfcts, bf, cache->quad->lambda[iq]);
	  const REAL_B *grdphi = GRD_PHI_D(bfcts, bf, cache->quad->lambda[iq]);
	  const REAL *phival = PHI_D(bfcts, bf, cache->quad->lambda[iq]);

	  for (i = 0; i < DIM_OF_WORLD; i++) {
	    MAXEY_BAR(DIM_MAX,
		      phival[i], cache->D2_phi[iq][bf], D2_phi_d[iq][bf][i]);
	  }
	  for (i = 0; i < DIM_OF_WORLD; i++) {
	    MAXPY_BAR(DIM_MAX, cache->phi[iq][bf], D2phi[i],
		      D2_phi_d[iq][bf][i]);
	    for (k = 0; k < N_LAMBDA_MAX; k++) {
	      D2_phi_d[iq][bf][i][k][k] += 
		2.0 * grdphi[i][k] * cache->grd_phi[iq][bf][k];
	      for (l = k+1; l < N_LAMBDA_MAX; l++) {
		REAL tmp = 
		  grdphi[i][k] * cache->grd_phi[iq][bf][l]
		  +
		  grdphi[i][l] * cache->grd_phi[iq][bf][k];
		D2_phi_d[iq][bf][i][k][l] += tmp;
		D2_phi_d[iq][bf][i][l][k] += tmp;
	      }
	    }
	  }
	}
      }
    }
  }

  aiqf->vect_cache.valid |= INIT_D2_PHI;
  
  return aiqf->vect_cache.D2_phi_d;

}

