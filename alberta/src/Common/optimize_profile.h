#ifndef _ALBERTA_OPTMIZE_PROFILE_H_
#define _ALBERTA_OPTMIZE_PROFILE_H_

#include <alberta.h>

extern void crs_matrix_optimize_profile(const CRS_MATRIX *A,
					int *P, int *PI,
					int *band_width,
					int64_t *profile,
					int optpro,
					int info_level);
extern void dof_matrix_optimize_profile(const DOF_MATRIX *matrix,
					const DOF_SCHAR_VEC *mask,
					int *P, int *PI,
					int *band_width,
					int64_t *profile,
					int optpro,
					int info_level);

#endif /*  _ALBERTA_OPTMIZE_PROFILE_H_ */
