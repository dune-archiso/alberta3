/*--------------------------------------------------------------------------*/
/* ALBERTA:  an Adaptive multi Level finite element toolbox using           */
/*           Bisectioning refinement and Error control by Residual          */
/*           Techniques for scientific Applications                         */
/*                                                                          */
/* file:     lagrange_3_2d.c                                                */
/*                                                                          */
/* description:  piecewise cubic Lagrange elements in 2d                    */
/*                                                                          */
/*--------------------------------------------------------------------------*/
/*                                                                          */
/*  authors:   Alfred Schmidt                                               */
/*             Zentrum fuer Technomathematik                                */
/*             Fachbereich 3 Mathematik/Informatik                          */
/*             Universitaet Bremen                                          */
/*             Bibliothekstr. 2                                             */
/*             D-28359 Bremen, Germany                                      */
/*                                                                          */
/*             Kunibert G. Siebert                                          */
/*             Institut fuer Mathematik                                     */
/*             Universitaet Augsburg                                        */
/*             Universitaetsstr. 14                                         */
/*             D-86159 Augsburg, Germany                                    */
/*                                                                          */
/*  http://www.mathematik.uni-freiburg.de/IAM/ALBERTA                       */
/*                                                                          */
/*  (c) by A. Schmidt and K.G. Siebert (1996-2003)                          */
/*                                                                          */
/*--------------------------------------------------------------------------*/

static const REAL_B bary3_2d[N_BAS_LAG_3_2D] = {
  INIT_BARY_2D(1.0, 0.0, 0.0),
  INIT_BARY_2D(0.0, 1.0, 0.0),
  INIT_BARY_2D(0.0, 0.0, 1.0),
  INIT_BARY_2D(0.0, 2.0/3.0, 1.0/3.0),
  INIT_BARY_2D(0.0, 1.0/3.0, 2.0/3.0),
  INIT_BARY_2D(1.0/3.0, 0.0, 2.0/3.0),
  INIT_BARY_2D(2.0/3.0, 0.0, 1.0/3.0),
  INIT_BARY_2D(2.0/3.0, 1.0/3.0, 0.0),
  INIT_BARY_2D(1.0/3.0, 2.0/3.0, 0.0),
  INIT_BARY_2D(1.0/3.0, 1.0/3.0, 1.0/3.0)
};

static LAGRANGE_DATA lag_3_2d_data = {
  bary3_2d,
  NULL /* lumping_quad */,
};

/*--------------------------------------------------------------------------*/
/*  basisfunction at vertex 0                                               */
/*--------------------------------------------------------------------------*/

static REAL phi3v0_2d(const REAL_B lambda, const BAS_FCTS *thisptr)
{
  return (4.5*(lambda[0] - 1.0)*lambda[0] + 1.0)*lambda[0];
}

static const REAL *grd_phi3v0_2d(const REAL_B lambda, const BAS_FCTS *thisptr)
{
  static REAL_B grd;
  grd[0] = (13.5*lambda[0] - 9.0)*lambda[0] + 1;
  return (const REAL *) grd;
}

static const REAL_B *D2_phi3v0_2d(const REAL *lambda, const BAS_FCTS *thisptr)
{
  static  REAL_BB D2;
  D2[0][0] = 27.0*lambda[0] - 9.0;
  return (const REAL_B *) D2;
}

static const REAL_BB *D3_phi3v0_2d(const REAL *lambda, const BAS_FCTS *thisptr)
{
  static REAL_BBB D3;

  D3[0][0][0] = 27.0;

  return (const REAL_BB *)D3;
}

/*--------------------------------------------------------------------------*/
/*  basisfunction at vertex 1                                               */
/*--------------------------------------------------------------------------*/

static REAL phi3v1_2d(const REAL_B lambda, const BAS_FCTS *thisptr)
{
  return (4.5*(lambda[1] - 1.0)*lambda[1] + 1.0)*lambda[1];
}

static const REAL *grd_phi3v1_2d(const REAL_B lambda, const BAS_FCTS *thisptr)
{
  static REAL_B grd = {0};
  grd[1] = (13.5*lambda[1] - 9.0)*lambda[1] + 1;
  return (const REAL *) grd;
}

static const REAL_B *D2_phi3v1_2d(const REAL *lambda, const BAS_FCTS *thisptr)
{
  static  REAL_BB D2;
  D2[1][1] = 27.0*lambda[1] - 9.0;
  return (const REAL_B *) D2;
}

static const REAL_BB *D3_phi3v1_2d(const REAL *lambda, const BAS_FCTS *thisptr)
{
  static REAL_BBB D3;

  D3[1][1][1] = 27.0;

  return (const REAL_BB *)D3;
}

/*--------------------------------------------------------------------------*/
/*  basisfunction at vertex 2                                               */
/*--------------------------------------------------------------------------*/

static REAL phi3v2_2d(const REAL_B lambda, const BAS_FCTS *thisptr)
{
  return (4.5*(lambda[2] - 1.0)*lambda[2] + 1.0)*lambda[2];
}

static const REAL *grd_phi3v2_2d(const REAL_B lambda, const BAS_FCTS *thisptr)
{
  static REAL_B grd = {0};
  grd[2] = (13.5*lambda[2] - 9.0)*lambda[2] + 1;
  return (const REAL *) grd;
}

static const REAL_B *D2_phi3v2_2d(const REAL *lambda, const BAS_FCTS *thisptr)
{
  static  REAL_BB D2;
  D2[2][2] = 27.0*lambda[2] - 9.0;
  return (const REAL_B *) D2;
}

static const REAL_BB *D3_phi3v2_2d(const REAL *lambda, const BAS_FCTS *thisptr)
{
  static REAL_BBB D3;

  D3[2][2][2] = 27.0;

  return (const REAL_BB *)D3;
}

/*--------------------------------------------------------------------------*/
/*  basisfunction 0 at edge 0                                               */
/*--------------------------------------------------------------------------*/

static REAL phi3e0_2d(const REAL_B lambda, const BAS_FCTS *thisptr)
{
  return (13.5*lambda[1] - 4.5)*lambda[1]*lambda[2];
}

static const REAL *grd_phi3e0_2d(const REAL_B lambda, const BAS_FCTS *thisptr)
{
  static REAL_B grd = {0};
  grd[1] = (27.0*lambda[1] - 4.5)*lambda[2];
  grd[2] = (13.5*lambda[1] - 4.5)*lambda[1];
  return (const REAL *) grd;
}

static const REAL_B *D2_phi3e0_2d(const REAL *lambda, const BAS_FCTS *thisptr)
{
  static  REAL_BB D2;
  D2[1][1] = 27.0*lambda[2];
  D2[1][2] = D2[2][1] = 27.0*lambda[1] - 4.5;
  return (const REAL_B *) D2;
}

static const REAL_BB *D3_phi3e0_2d(const REAL *lambda, const BAS_FCTS *thisptr)
{
  static REAL_BBB D3;

  D3[1][1][2] = D3[1][2][1] = D3[2][1][1] = 27.0;

  return (const REAL_BB *)D3;
}


/*--------------------------------------------------------------------------*/
/*  basisfunction 1 at edge 0                                               */
/*--------------------------------------------------------------------------*/

static REAL phi3e1_2d(const REAL_B lambda, const BAS_FCTS *thisptr)
{
  return (13.5*lambda[2] - 4.5)*lambda[2]*lambda[1];
}

static const REAL *grd_phi3e1_2d(const REAL_B lambda, const BAS_FCTS *thisptr)
{
  static REAL_B grd = {0};
  grd[1] = (13.5*lambda[2] - 4.5)*lambda[2];
  grd[2] = (27.0*lambda[2] - 4.5)*lambda[1];
  return (const REAL *) grd;
}

static const REAL_B *D2_phi3e1_2d(const REAL *lambda, const BAS_FCTS *thisptr)
{
  static  REAL_BB D2;
  D2[1][2] = D2[2][1] = 27.0*lambda[2] - 4.5;
  D2[2][2] = 27.0*lambda[1];
  return (const REAL_B *) D2;
}

static const REAL_BB *D3_phi3e1_2d(const REAL *lambda, const BAS_FCTS *thisptr)
{
  static REAL_BBB D3;

  D3[1][2][2] = D3[2][2][1] = D3[2][1][2] = 27.0;

  return (const REAL_BB *)D3;
}

/*--------------------------------------------------------------------------*/
/*  basisfunction 0 at edge 1                                               */
/*--------------------------------------------------------------------------*/

static REAL phi3e2_2d(const REAL_B lambda, const BAS_FCTS *thisptr)
{
  return (13.5*lambda[2] - 4.5)*lambda[2]*lambda[0];
}

static const REAL *grd_phi3e2_2d(const REAL_B lambda, const BAS_FCTS *thisptr)
{
  static REAL_B grd = {0};
  grd[0] = (13.5*lambda[2] - 4.5)*lambda[2];
  grd[2] = (27.0*lambda[2] - 4.5)*lambda[0];
  return (const REAL *) grd;
}

static const REAL_B *D2_phi3e2_2d(const REAL *lambda, const BAS_FCTS *thisptr)
{
  static  REAL_BB D2;
  D2[0][2] = D2[2][0] = 27.0*lambda[2] - 4.5;
  D2[2][2] = 27.0*lambda[0];
  return (const REAL_B *) D2;
}

static const REAL_BB *D3_phi3e2_2d(const REAL *lambda, const BAS_FCTS *thisptr)
{
  static REAL_BBB D3;

  D3[0][2][2] = D3[2][2][0] = D3[2][0][2] = 27.0;

  return (const REAL_BB *)D3;
}

/*--------------------------------------------------------------------------*/
/*  basisfunction 1 at edge 1                                               */
/*--------------------------------------------------------------------------*/

static REAL phi3e3_2d(const REAL_B lambda, const BAS_FCTS *thisptr)
{
  return (13.5*lambda[0] - 4.5)*lambda[0]*lambda[2];
}

static const REAL *grd_phi3e3_2d(const REAL_B lambda, const BAS_FCTS *thisptr)
{
  static REAL_B grd = {0};
  grd[0] = (27.0*lambda[0] - 4.5)*lambda[2];
  grd[2] = (13.5*lambda[0] - 4.5)*lambda[0];
  return (const REAL *) grd;
}

static const REAL_B *D2_phi3e3_2d(const REAL *lambda, const BAS_FCTS *thisptr)
{
  static  REAL_BB D2;
  D2[0][0] = 27.0*lambda[2];
  D2[0][2] = D2[2][0] = 27.0*lambda[0] - 4.5;
  return (const REAL_B *) D2;
}

static const REAL_BB *D3_phi3e3_2d(const REAL *lambda, const BAS_FCTS *thisptr)
{
  static REAL_BBB D3;

  D3[0][0][2] = D3[0][2][0] = D3[2][0][0] = 27.0;

  return (const REAL_BB *)D3;
}

/*--------------------------------------------------------------------------*/
/*  basisfunction 0 at edge 2                                               */
/*--------------------------------------------------------------------------*/

static REAL phi3e4_2d(const REAL_B lambda, const BAS_FCTS *thisptr)
{
  return (13.5*lambda[0] - 4.5)*lambda[0]*lambda[1];
}

static const REAL *grd_phi3e4_2d(const REAL_B lambda, const BAS_FCTS *thisptr)
{
  static REAL_B grd = {0};
  grd[0] = (27.0*lambda[0] - 4.5)*lambda[1];
  grd[1] = (13.5*lambda[0] - 4.5)*lambda[0];
  return (const REAL *) grd;
}

static const REAL_B *D2_phi3e4_2d(const REAL *lambda, const BAS_FCTS *thisptr)
{
  static  REAL_BB D2;
  D2[0][0] = 27.0*lambda[1];
  D2[0][1] = D2[1][0] = 27.0*lambda[0] - 4.5;
  return (const REAL_B *) D2;
}

static const REAL_BB *D3_phi3e4_2d(const REAL *lambda, const BAS_FCTS *thisptr)
{
  static REAL_BBB D3;

  D3[0][0][1] = D3[0][1][0] = D3[1][0][0] = 27.0;

  return (const REAL_BB *)D3;
}

/*--------------------------------------------------------------------------*/
/*  basisfunction 1 at edge 2                                               */
/*--------------------------------------------------------------------------*/

static REAL phi3e5_2d(const REAL_B lambda, const BAS_FCTS *thisptr)
{
  return (13.5*lambda[1] - 4.5)*lambda[1]*lambda[0];
}

static const REAL *grd_phi3e5_2d(const REAL_B lambda, const BAS_FCTS *thisptr)
{
  static REAL_B grd = {0};
  grd[0] = (13.5*lambda[1] - 4.5)*lambda[1];
  grd[1] = (27.0*lambda[1] - 4.5)*lambda[0];
  return (const REAL *) grd;
}

static const REAL_B *D2_phi3e5_2d(const REAL *lambda, const BAS_FCTS *thisptr)
{
  static  REAL_BB D2;
  D2[0][1] = D2[1][0] = 27.0*lambda[1] - 4.5;
  D2[1][1] = 27.0*lambda[0];
  return (const REAL_B *) D2;
}

static const REAL_BB *D3_phi3e5_2d(const REAL *lambda, const BAS_FCTS *thisptr)
{
  static REAL_BBB D3;

  D3[0][1][1] = D3[1][1][0] = D3[1][0][1] = 27.0;

  return (const REAL_BB *)D3;
}

/*--------------------------------------------------------------------------*/
/*  basisfunction at barycenter                                             */
/*--------------------------------------------------------------------------*/

static REAL phi3c0_2d(const REAL_B lambda, const BAS_FCTS *thisptr)
{
  return 27.0*lambda[0]*lambda[1]*lambda[2];
}

static const REAL *grd_phi3c0_2d(const REAL_B lambda, const BAS_FCTS *thisptr)
{
  static REAL_B grd = {0};
  grd[0] = 27.0*lambda[1]*lambda[2];
  grd[1] = 27.0*lambda[0]*lambda[2];
  grd[2] = 27.0*lambda[0]*lambda[1];
  return (const REAL *) grd;
}

static const REAL_B *D2_phi3c0_2d(const REAL *lambda, const BAS_FCTS *thisptr)
{
  static  REAL_BB D2;
  D2[0][1] = D2[1][0] = 27.0*lambda[2];
  D2[0][2] = D2[2][0] = 27.0*lambda[1];
  D2[1][2] = D2[2][1] = 27.0*lambda[0];
  return (const REAL_B *) D2;
}

static const REAL_BB *D3_phi3c0_2d(const REAL *lambda, const BAS_FCTS *thisptr )
{
  static REAL_BBB D3;

  D3[0][1][2] = D3[0][2][1] = D3[1][0][2] = 
    D3[2][1][0] = D3[2][0][1] = D3[1][2][0] = 27.0;

  return (const REAL_BB *)D3;
}

/******************************************************************************/

#undef DEF_EL_VEC_3_2D
#define DEF_EL_VEC_3_2D(type, name)				\
  DEF_EL_VEC_CONST(type, name, N_BAS_LAG_3_2D, N_BAS_LAG_3_2D)

#undef DEFUN_GET_EL_VEC_3_2D
#define DEFUN_GET_EL_VEC_3_2D(name, type, admin, body, ...)		\
  static const EL_##type##_VEC *					\
  get_##name##3_2d(type##_VEC_TYPE *vec, const EL *el, __VA_ARGS__)	\
  {									\
    FUNCNAME("get_"#name"3_2d");					\
    static DEF_EL_VEC_3_2D(type, rvec_space);				\
    type##_VEC_TYPE *rvec = vec ? vec : rvec_space->vec;		\
    int n0, node0, ibas, inode;						\
    DOF **dofptr = el->dof, dof;					\
									\
    DEBUG_TEST_EXIT(true, "");						\
									\
    n0 = (admin)->n0_dof[VERTEX];					\
    for (ibas = 0; ibas < N_VERTICES_2D; ibas++) {			\
      dof = dofptr[ibas][n0];						\
      body;								\
    }									\
									\
    n0 = (admin)->n0_dof[EDGE];						\
    node0 = (admin)->mesh->node[EDGE];					\
    for (inode = 0, ibas = N_VERTICES_2D; inode < N_EDGES_2D; inode++) { \
      if (dofptr[vertex_of_edge_2d[inode][0]][0]			\
	  < dofptr[vertex_of_edge_2d[inode][1]][0]) {			\
	dof = dofptr[N_VERTICES_2D+inode][n0];				\
	body;								\
	ibas++;								\
	dof = dofptr[N_VERTICES_2D+inode][n0+1];			\
	body;								\
	ibas++;								\
      } else {								\
	dof = dofptr[N_VERTICES_2D+inode][n0+1];			\
	body;								\
	ibas++;								\
	dof  = dofptr[N_VERTICES_2D+inode][n0];				\
	body;								\
	ibas++;								\
      }									\
    }									\
    									\
    n0 = (admin)->n0_dof[CENTER];					\
    node0 = (admin)->mesh->node[CENTER];				\
    dof = dofptr[node0][n0];						\
    body;								\
									\
    return vec ? NULL : rvec_space;					\
  }									\
  struct _AI_semicolon_dummy

#undef DEFUN_GET_EL_DOF_VEC_3_2D
#define DEFUN_GET_EL_DOF_VEC_3_2D(name, type, ASSIGN)			\
  DEFUN_GET_EL_VEC_3_2D(_##name##_vec, type, dv->fe_space->admin,	\
			ASSIGN(dv->vec[dof], rvec[ibas]),		\
			const DOF_##type##_VEC *dv);			\
  static const EL_##type##_VEC *					\
  get_##name##_vec3_2d(type##_VEC_TYPE *vec, const EL *el,		\
		       const DOF_##type##_VEC *dv)			\
  {									\
    EL_##type##_VEC *vec_loc = dv->vec_loc;				\
									\
    if (vec != NULL || vec_loc == NULL) {				\
      return get__##name##_vec3_2d(vec, el, dv);			\
    } else {								\
      get__##name##_vec3_2d(vec_loc->vec, el, dv);			\
      return vec_loc;							\
    }									\
  }									\
  struct _AI_semicolon_dummy


#undef COPY_EQ
#define COPY_EQ(a, b) (b) = (a)

/*--------------------------------------------------------------------------*/
/*  functions for combining basisfunctions with coefficients                */
/*--------------------------------------------------------------------------*/

DEFUN_GET_EL_VEC_3_2D(dof_indices, DOF, admin,
		      rvec[ibas] = dof,
		      const DOF_ADMIN *admin, const BAS_FCTS *thisptr);

static const EL_BNDRY_VEC *
get_bound3_2d(BNDRY_FLAGS *vec,
	      const EL_INFO *el_info, const BAS_FCTS *thisptr)
{
  FUNCNAME("get_bound3_2d");
  static DEF_EL_VEC_3_2D(BNDRY, rvec_space);
  BNDRY_FLAGS *rvec = vec ? vec : rvec_space->vec;
  int i, j, k;

  TEST_FLAG(FILL_BOUND, el_info);

  for (i = 0; i < N_VERTICES_2D; i++) {
    BNDRY_FLAGS_CPY(rvec[i], el_info->vertex_bound[i]);
  }

  for (j = N_VERTICES_2D, i = 0; i < N_EDGES_2D; j += 2, i++) {
    for (k = 0; k < 2; k++) {
      BNDRY_FLAGS_CPY(rvec[j+k], el_info->edge_bound[i]);
    }
  }

  BNDRY_FLAGS_INIT(rvec[9]);
  BNDRY_FLAGS_SET(rvec[9], el_info->face_bound[0]);

  return vec ? NULL : rvec_space;
}

/*--------------------------------------------------------------------*/
/*--- function for accessing a local DOF_INT_VEC                   ---*/
/*--------------------------------------------------------------------*/

DEFUN_GET_EL_DOF_VEC_3_2D(int, INT, COPY_EQ);

/*--------------------------------------------------------------------*/
/*--- function for accessing a local DOF_REAL_VEC                  ---*/
/*--------------------------------------------------------------------*/

DEFUN_GET_EL_DOF_VEC_3_2D(real, REAL, COPY_EQ);

/*--------------------------------------------------------------------*/
/*--- function for accessing a local DOF_REAL_D_VEC                ---*/
/*--------------------------------------------------------------------*/

DEFUN_GET_EL_DOF_VEC_3_2D(real_d, REAL_D, COPY_DOW);

/*--------------------------------------------------------------------*/
/*--- function for accessing a local DOF_SCHAR_VEC                 ---*/
/*--------------------------------------------------------------------*/

DEFUN_GET_EL_DOF_VEC_3_2D(schar, SCHAR, COPY_EQ);

/*--------------------------------------------------------------------*/
/*--- function for accessing a local DOF_UCHAR_VEC                 ---*/
/*--------------------------------------------------------------------*/

DEFUN_GET_EL_DOF_VEC_3_2D(uchar, UCHAR, COPY_EQ);

/*--------------------------------------------------------------------*/
/*--- function for accessing a local DOF_PTR_VEC                   ---*/
/*--------------------------------------------------------------------*/

DEFUN_GET_EL_DOF_VEC_3_2D(ptr, PTR, COPY_EQ);

/*--------------------------------------------------------------------*/
/*--- function for accessing a local DOF_REAL_DD_VEC               ---*/
/*--------------------------------------------------------------------*/

DEFUN_GET_EL_DOF_VEC_3_2D(real_dd, REAL_DD, _AI_MCOPY_DOW);

/*--------------------------------------------------------------------*/
/*--- function for local interpolaton of scalar functions          ---*/
/*--------------------------------------------------------------------*/

GENERATE_INTERPOL(/**/, 3, 2, N_BAS_LAG_3_2D);

/*--------------------------------------------------------------------*/
/*--- function for local interpolaton of vector functions          ---*/
/*--------------------------------------------------------------------*/

GENERATE_INTERPOL_D(/**/, 3, 2, N_BAS_LAG_3_2D);

GENERATE_INTERPOL_DOW(/**/, 3, 2, N_BAS_LAG_3_2D);

/*--------------------------------------------------------------------------*/
/*  functions for interpolation/ restriction during refinement/coarsening   */
/*--------------------------------------------------------------------------*/

static void real_refine_inter3_2d(DOF_REAL_VEC *drv, RC_LIST_EL *list, int n)
{
  FUNCNAME("real_refine_inter3_2d");
  DOF pdof[N_BAS_LAG_3_2D];
  DOF cdof[N_BAS_LAG_3_2D];
  EL        *el;
  REAL      *v = NULL;
  int       dof9, node, n0;
  const DOF_ADMIN *admin;
  const BAS_FCTS  *bas_fcts;

  if (n < 1) return;
  el = list->el_info.el;

  GET_DOF_VEC(v, drv);
  if (!drv->fe_space)
  {
    ERROR("no fe_space in dof_real_vec %s\n", NAME(drv));
    return;
  }
  else if (!drv->fe_space->bas_fcts)
  {
    ERROR("no basis functions in fe_space %s\n", NAME(drv->fe_space));
    return;
  }
  GET_STRUCT(admin, drv->fe_space);
  GET_STRUCT(bas_fcts, drv->fe_space);

  get_dof_indices3_2d(pdof, el, admin, bas_fcts);

/*--------------------------------------------------------------------------*/
/*  values on child[0]                                                      */
/*--------------------------------------------------------------------------*/
  get_dof_indices3_2d(cdof, el->child[0], admin, bas_fcts);

  v[cdof[2]] = (-0.0625*(v[pdof[0]] + v[pdof[1]])
		+ 0.5625*(v[pdof[7]] + v[pdof[8]]));
  v[cdof[3]] = (0.3125*(v[pdof[0]] - v[pdof[8]]) + 0.0625*v[pdof[1]]
		+ 0.9375*v[pdof[7]]);
  v[cdof[4]] =  v[pdof[7]];
  v[cdof[5]] =  v[pdof[9]];
  v[cdof[6]] =  (0.0625*(v[pdof[0]] + v[pdof[1]])
		 - 0.25*(v[pdof[3]] + v[pdof[6]])
		 + 0.5*(v[pdof[4]] + v[pdof[5]] + v[pdof[9]])
		 - 0.0625*(v[pdof[7]] + v[pdof[8]]));
  v[cdof[9]] = (0.0625*(-v[pdof[0]] + v[pdof[1]]) - 0.125*v[pdof[3]]
		+ 0.375*v[pdof[6]] + 0.1875*(v[pdof[7]] - v[pdof[8]])
		+ 0.75*v[pdof[9]]);

/*--------------------------------------------------------------------------*/
/*  values on child[1]                                                      */
/*--------------------------------------------------------------------------*/

  get_dof_indices3_2d(cdof, el->child[1], admin, bas_fcts);

  v[cdof[5]] =  v[pdof[8]];
  v[cdof[6]] =  (0.0625*v[pdof[0]] + 0.9375*v[pdof[8]]
		 + 0.3125*(v[pdof[1]] - v[pdof[7]]));
  v[cdof[9]] =  (0.0625*(v[pdof[0]] - v[pdof[1]]) + 0.375*v[pdof[3]]
		 - 0.125*v[pdof[6]] + 0.1875*(-v[pdof[7]] + v[pdof[8]])
		 + 0.75*v[pdof[9]]);

  if (n <= 1)  return;

/*--------------------------------------------------------------------------*/
/*  adjust the value on the neihgbour                                       */
/*--------------------------------------------------------------------------*/

  el = list[1].el_info.el;
  get_dof_indices3_2d(pdof, el, admin, bas_fcts);

/*--------------------------------------------------------------------------*/
/*  values on neigh's child[0]                                              */
/*--------------------------------------------------------------------------*/
  get_dof_indices3_2d(cdof, el->child[0], admin, bas_fcts);

  v[cdof[5]] =  v[pdof[9]];
  v[cdof[6]] =  (0.0625*(v[pdof[0]] + v[pdof[1]])
		 - 0.25*(v[pdof[3]] + v[pdof[6]])
		 + 0.5*(v[pdof[4]] + v[pdof[5]] + v[pdof[9]])
		 - 0.0625*(v[pdof[7]] + v[pdof[8]]));
  v[cdof[9]] = (0.0625*(-v[pdof[0]] + v[pdof[1]]) - 0.12500*v[pdof[3]]
		+ 0.375*v[pdof[6]] + 0.1875*(v[pdof[7]] - v[pdof[8]])
		+ 0.75*v[pdof[9]]);
/*--------------------------------------------------------------------------*/
/*  values on neigh's child[0]                                              */
/*--------------------------------------------------------------------------*/

  node = drv->fe_space->admin->mesh->node[CENTER];
  n0 = admin->n0_dof[CENTER];
  dof9 = el->child[1]->dof[node][n0];

  v[dof9] =  (0.0625*(v[pdof[0]] - v[pdof[1]]) +  0.375*v[pdof[3]]
	      - 0.125*v[pdof[6]] + 0.1875*(-v[pdof[7]] + v[pdof[8]])
	      + 0.75*v[pdof[9]]);
  return;
}

static void real_coarse_inter3_2d(DOF_REAL_VEC *drv, RC_LIST_EL *list, int n)
{
  FUNCNAME("real_coarse_inter3_2d");
  EL        *el, *child;
  REAL      *v = NULL;
  int       cdof, pdof, node, n0;
  const DOF_ADMIN *admin;
  MESH      *mesh = NULL;

  if (n < 1) return;
  el = list->el_info.el;

  GET_DOF_VEC(v, drv);
  if (!drv->fe_space)
  {
    ERROR("no fe_space in dof_real_vec %s\n", NAME(drv));
    return;
  }
  else if (!drv->fe_space->bas_fcts)
  {
    ERROR("no basis functions in fe_space %s\n", NAME(drv->fe_space));
    return;
  }
  GET_STRUCT(admin, drv->fe_space);
  GET_STRUCT(mesh, drv->fe_space);

  node = mesh->node[EDGE];
  n0 = admin->n0_dof[EDGE];

/*--------------------------------------------------------------------------*/
/*  values on child[0]                                                      */
/*--------------------------------------------------------------------------*/

  child = el->child[0];

  if (el->dof[0][0] < el->dof[1][0])
    pdof = el->dof[node+2][n0];
  else
    pdof = el->dof[node+2][n0+1];

  if (child->dof[1][0] < child->dof[2][0])
    cdof = child->dof[node][n0+1];
  else
    cdof = child->dof[node][n0];

  v[pdof] = v[cdof];

  if (child->dof[2][0] < child->dof[0][0])
    cdof = child->dof[node+1][n0];
  else
    cdof = child->dof[node+1][n0+1];

  v[el->dof[mesh->node[CENTER]][admin->n0_dof[CENTER]]] = v[cdof];

/*--------------------------------------------------------------------------*/
/*  values on child[1]                                                      */
/*--------------------------------------------------------------------------*/

  child = el->child[1];

  if (el->dof[0][0] < el->dof[1][0])
    pdof = el->dof[node+2][n0+1];
  else
    pdof = el->dof[node+2][n0];

  if (child->dof[2][0] < child->dof[0][0])
    cdof = child->dof[node+1][n0];
  else
    cdof = child->dof[node+1][n0+1];

  v[pdof] = v[cdof];

  if (n <= 1)  return;

/*--------------------------------------------------------------------------*/
/*   adjust neighbour values                                                */
/*--------------------------------------------------------------------------*/

  el = list[1].el_info.el;
  child = el->child[0];

  if (child->dof[2][0] < child->dof[0][0])
    cdof = child->dof[node+1][n0];
  else
    cdof = child->dof[node+1][n0+1];

  v[el->dof[mesh->node[CENTER]][admin->n0_dof[CENTER]]] = v[cdof];

  return;
}

static void real_coarse_restr3_2d(DOF_REAL_VEC *drv, RC_LIST_EL *list, int n)
{
  FUNCNAME("real_coarse_restr3_2d");
  DOF pdof[N_BAS_LAG_3_2D];
  DOF cdof[N_BAS_LAG_3_2D];
  EL        *el;
  REAL      *v = NULL;
  int       node, n0;
  DOF dof9;
  const DOF_ADMIN *admin;
  const BAS_FCTS *bas_fcts;

  if (n < 1) return;
  el = list->el_info.el;

  GET_DOF_VEC(v, drv);
  if (!drv->fe_space)
  {
    ERROR("no fe_space in dof_real_vec %s\n", NAME(drv));
    return;
  }
  else if (!drv->fe_space->bas_fcts)
  {
    ERROR("no basis functions in fe_space %s\n", NAME(drv->fe_space));
    return;
  }
  GET_STRUCT(admin, drv->fe_space);
  GET_STRUCT(bas_fcts, drv->fe_space);

  get_dof_indices3_2d(pdof, el, admin, bas_fcts);

/*--------------------------------------------------------------------------*/
/*  values on child[0]                                                      */
/*--------------------------------------------------------------------------*/
  get_dof_indices3_2d(cdof, el->child[0], admin, bas_fcts);

  v[pdof[0]] += (0.0625*(-v[cdof[2]] + v[cdof[6]] - v[cdof[9]])
		 + 0.3125*v[cdof[3]]);
  v[pdof[1]] += 0.0625*(-v[cdof[2]] +  v[cdof[3]] + v[cdof[6]] + v[cdof[9]]);
  v[pdof[3]] += -0.25*v[cdof[6]] - 0.125*v[cdof[9]];
  v[pdof[4]] +=  0.5*v[cdof[6]];
  v[pdof[5]] +=  0.5*v[cdof[6]];
  v[pdof[6]] += -0.25*v[cdof[6]] + 0.375*v[cdof[9]];
  v[pdof[7]] = (0.5625*v[cdof[2]] + 0.9375*v[cdof[3]] + v[cdof[4]]
		- 0.0625*v[cdof[6]] + 0.1875*v[cdof[9]]);
  v[pdof[8]] = (0.5625*v[cdof[2]] - 0.3125*v[cdof[3]]
		- 0.0625*v[cdof[6]] - 0.1875*v[cdof[9]]);
  v[pdof[9]] =  v[cdof[5]] + 0.5*v[cdof[6]] + 0.75*v[cdof[9]];

/*--------------------------------------------------------------------------*/
/*  values on child[1]                                                      */
/*--------------------------------------------------------------------------*/

  get_dof_indices3_2d(cdof, el->child[1], admin, bas_fcts);

  v[pdof[0]] += 0.0625*v[cdof[6]] + 0.0625*v[cdof[9]];
  v[pdof[1]] += 0.3125*v[cdof[6]] - 0.0625*v[cdof[9]];
  v[pdof[3]] += 0.375*v[cdof[9]];
  v[pdof[6]] += -0.125*v[cdof[9]];
  v[pdof[7]] += -0.3125*v[cdof[6]] - 0.18750*v[cdof[9]];
  v[pdof[8]] += v[cdof[5]] + 0.9375*v[cdof[6]] + 0.1875*v[cdof[9]];
  v[pdof[9]] += 0.75*v[cdof[9]];

  if (n <= 1)  return;

/*--------------------------------------------------------------------------*/
/*  adjust the value on the neihgbour                                       */
/*--------------------------------------------------------------------------*/

  el = list[1].el_info.el;
  get_dof_indices3_2d(pdof, el, admin, bas_fcts);

/*--------------------------------------------------------------------------*/
/*  values on neigh's child[0]                                              */
/*--------------------------------------------------------------------------*/
  get_dof_indices3_2d(cdof, el->child[0], admin, bas_fcts);

  v[pdof[0]] += 0.0625*(v[cdof[6]] - v[cdof[9]]);
  v[pdof[1]] += 0.0625*(v[cdof[6]] + v[cdof[9]]);
  v[pdof[3]] += -0.25*v[cdof[6]] - 0.12500*v[cdof[9]];
  v[pdof[4]] += 0.5*v[cdof[6]];
  v[pdof[5]] += 0.5*v[cdof[6]];
  v[pdof[6]] += -0.25*v[cdof[6]] + 0.375*v[cdof[9]];
  v[pdof[7]] += -0.0625*v[cdof[6]] + 0.1875*v[cdof[9]];
  v[pdof[8]] += -0.0625*v[cdof[6]] - 0.1875*v[cdof[9]];
  v[pdof[9]] =  v[cdof[5]] + 0.5*v[cdof[6]] + 0.75*v[cdof[9]];

/*--------------------------------------------------------------------------*/
/*  values on neigh's child[1]                                              */
/*--------------------------------------------------------------------------*/

  node = drv->fe_space->admin->mesh->node[CENTER];
  n0 = admin->n0_dof[CENTER];
  dof9 = el->child[1]->dof[node][n0];

  v[pdof[0]] += 0.0625*v[dof9];
  v[pdof[1]] -= 0.0625*v[dof9];
  v[pdof[3]] += 0.375*v[dof9];
  v[pdof[6]] -= 0.125*v[dof9];
  v[pdof[7]] -= 0.1875*v[dof9];
  v[pdof[8]] += 0.1875*v[dof9];
  v[pdof[9]] += 0.75*v[dof9];

  return;
}

static void real_d_refine_inter3_2d(DOF_REAL_D_VEC *drdv,
				    RC_LIST_EL *list, int n)
{
  FUNCNAME("real_d_refine_inter3_2d");
  DOF pdof[N_BAS_LAG_3_2D];
  DOF cdof[N_BAS_LAG_3_2D];
  EL        *el;
  REAL_D    *v = NULL;
  int       node, n0, j;
  DOF       dof9;
  const DOF_ADMIN *admin;
  const BAS_FCTS *bas_fcts;

  if (n < 1) return;
  el = list->el_info.el;

  GET_DOF_VEC(v, drdv);
  if (!drdv->fe_space)
  {
    ERROR("no fe_space in dof_real_d_vec %s\n", NAME(drdv));
    return;
  }
  else if (!drdv->fe_space->bas_fcts)
  {
    ERROR("no basis functions in fe_space %s\n", NAME(drdv->fe_space));
    return;
  }
  GET_STRUCT(admin, drdv->fe_space);
  GET_STRUCT(bas_fcts, drdv->fe_space);

  get_dof_indices3_2d(pdof, el, admin, bas_fcts);

/*--------------------------------------------------------------------------*/
/*  values on child[0]                                                      */
/*--------------------------------------------------------------------------*/
  get_dof_indices3_2d(cdof, el->child[0], admin, bas_fcts);

  for (j = 0; j < DIM_OF_WORLD; j++)
  {
    v[cdof[2]][j] = (-0.0625*(v[pdof[0]][j] + v[pdof[1]][j])
		     + 0.5625*(v[pdof[7]][j] + v[pdof[8]][j]));
    v[cdof[3]][j] = (0.3125*(v[pdof[0]][j] - v[pdof[8]][j])
		     + 0.0625*v[pdof[1]][j] + 0.9375*v[pdof[7]][j]);
    v[cdof[4]][j] =  v[pdof[7]][j];
    v[cdof[5]][j] =  v[pdof[9]][j];
    v[cdof[6]][j] =  (0.0625*(v[pdof[0]][j] + v[pdof[1]][j])
		      - 0.25*(v[pdof[3]][j] + v[pdof[6]][j])
		      + 0.5*(v[pdof[4]][j] + v[pdof[5]][j] + v[pdof[9]][j])
		      - 0.0625*(v[pdof[7]][j] + v[pdof[8]][j]));
    v[cdof[9]][j] = (0.0625*(-v[pdof[0]][j] + v[pdof[1]][j])
		     - 0.125*v[pdof[3]][j] + 0.375*v[pdof[6]][j]
		     + 0.1875*(v[pdof[7]][j] - v[pdof[8]][j])
		     + 0.75*v[pdof[9]][j]);
  }

/*--------------------------------------------------------------------------*/
/*  values on child[0]                                                      */
/*--------------------------------------------------------------------------*/

  get_dof_indices3_2d(cdof, el->child[1], admin, bas_fcts);

  for (j = 0; j < DIM_OF_WORLD; j++)
  {
    v[cdof[5]][j] =  v[pdof[8]][j];
    v[cdof[6]][j] =  (0.0625*v[pdof[0]][j] + 0.9375*v[pdof[8]][j]
		      + 0.3125*(v[pdof[1]][j] - v[pdof[7]][j]));
    v[cdof[9]][j] =  (0.0625*(v[pdof[0]][j] - v[pdof[1]][j])
		      + 0.375*v[pdof[3]][j] - 0.125*v[pdof[6]][j]
		      + 0.1875*(-v[pdof[7]][j] + v[pdof[8]][j])
		      + 0.75*v[pdof[9]][j]);
  }

  if (n <= 1)  return;

/*--------------------------------------------------------------------------*/
/*  adjust the value on the neihgbour                                       */
/*--------------------------------------------------------------------------*/

  el = list[1].el_info.el;
  get_dof_indices3_2d(pdof, el, admin, bas_fcts);

/*--------------------------------------------------------------------------*/
/*  values on neigh's child[0]                                              */
/*--------------------------------------------------------------------------*/
  get_dof_indices3_2d(cdof, el->child[0], admin, bas_fcts);

  for (j = 0; j < DIM_OF_WORLD; j++)
  {
    v[cdof[5]][j] =  v[pdof[9]][j];
    v[cdof[6]][j] =  (0.0625*(v[pdof[0]][j] + v[pdof[1]][j])
		      - 0.25*(v[pdof[3]][j] + v[pdof[6]][j])
		      + 0.5*(v[pdof[4]][j] + v[pdof[5]][j] + v[pdof[9]][j])
		      - 0.0625*(v[pdof[7]][j] + v[pdof[8]][j]));
    v[cdof[9]][j] = (0.0625*(-v[pdof[0]][j] + v[pdof[1]][j])
		     - 0.12500*v[pdof[3]][j] + 0.375*v[pdof[6]][j]
		     + 0.1875*(v[pdof[7]][j] - v[pdof[8]][j])
		     + 0.75*v[pdof[9]][j]);
  }

/*--------------------------------------------------------------------------*/
/*  values on neigh's child[0]                                              */
/*--------------------------------------------------------------------------*/

  node = drdv->fe_space->admin->mesh->node[CENTER];
  n0 = admin->n0_dof[CENTER];

  for (j = 0; j < DIM_OF_WORLD; j++)
  {
    dof9 = el->child[1]->dof[node][n0];

    v[dof9][j] =  (0.0625*(v[pdof[0]][j] - v[pdof[1]][j])
		   +  0.375*v[pdof[3]][j] - 0.125*v[pdof[6]][j]
		   + 0.1875*(-v[pdof[7]][j] + v[pdof[8]][j])
		   + 0.75*v[pdof[9]][j]);
  }
  return;
}

static void real_d_coarse_inter3_2d(DOF_REAL_D_VEC *drdv,
				    RC_LIST_EL *list, int n)
{
  FUNCNAME("real_d_coarse_inter3_2d");
  EL        *el, *child;
  REAL_D    *v = NULL;
  int       cdof, pdof, node, n0, j;
  const DOF_ADMIN *admin;
  MESH      *mesh = NULL;

  if (n < 1) return;
  el = list->el_info.el;

  GET_DOF_VEC(v, drdv);
  if (!drdv->fe_space)
  {
    ERROR("no fe_space in dof_real_d_vec %s\n", NAME(drdv));
    return;
  }
  else if (!drdv->fe_space->bas_fcts)
  {
    ERROR("no basis functions in fe_space %s\n", NAME(drdv->fe_space));
    return;
  }
  GET_STRUCT(admin, drdv->fe_space);
  GET_STRUCT(mesh, drdv->fe_space);

  node = mesh->node[EDGE];
  n0 = admin->n0_dof[EDGE];

/*--------------------------------------------------------------------------*/
/*  values on child[0]                                                      */
/*--------------------------------------------------------------------------*/

  child = el->child[0];

  if (el->dof[0][0] < el->dof[1][0])
    pdof = el->dof[node+2][n0];
  else
    pdof = el->dof[node+2][n0+1];

  if (child->dof[1][0] < child->dof[2][0])
    cdof = child->dof[node][n0+1];
  else
    cdof = child->dof[node][n0];

  for (j = 0; j < DIM_OF_WORLD; j++)
    v[pdof][j] = v[cdof][j];

  if (child->dof[2][0] < child->dof[0][0])
    cdof = child->dof[node+1][n0];
  else
    cdof = child->dof[node+1][n0+1];

  for (j = 0; j < DIM_OF_WORLD; j++)
    v[el->dof[mesh->node[CENTER]][admin->n0_dof[CENTER]]][j] = v[cdof][j];

/*--------------------------------------------------------------------------*/
/*  values on child[1]                                                      */
/*--------------------------------------------------------------------------*/

  child = el->child[1];

  if (el->dof[0][0] < el->dof[1][0])
    pdof = el->dof[node+2][n0+1];
  else
    pdof = el->dof[node+2][n0];

  if (child->dof[2][0] < child->dof[0][0])
    cdof = child->dof[node+1][n0];
  else
    cdof = child->dof[node+1][n0+1];

  for (j = 0; j < DIM_OF_WORLD; j++)
    v[pdof][j] = v[cdof][j];

  if (n <= 1)  return;

/*--------------------------------------------------------------------------*/
/*   adjust neighbour values                                                */
/*--------------------------------------------------------------------------*/

  el = list[1].el_info.el;
  child = el->child[0];

  if (child->dof[2][0] < child->dof[0][0])
    cdof = child->dof[node+1][n0];
  else
    cdof = child->dof[node+1][n0+1];

  for (j = 0; j < DIM_OF_WORLD; j++)
    v[el->dof[mesh->node[CENTER]][admin->n0_dof[CENTER]]][j] = v[cdof][j];

  return;
}

static void real_d_coarse_restr3_2d(DOF_REAL_D_VEC *drdv,
				    RC_LIST_EL *list, int n)
{
  FUNCNAME("real_d_coarse_restr3_2d");
  DOF pdof[N_BAS_LAG_3_2D];
  DOF cdof[N_BAS_LAG_3_2D];
  EL        *el;
  REAL_D    *v = NULL;
  int       node, n0, j;
  DOF       dof9;
  const DOF_ADMIN *admin;
  const BAS_FCTS *bas_fcts;

  if (n < 1) return;
  el = list->el_info.el;

  GET_DOF_VEC(v, drdv);
  if (!drdv->fe_space)
  {
    ERROR("no fe_space in dof_real_d_vec %s\n", NAME(drdv));
    return;
  }
  else if (!drdv->fe_space->bas_fcts)
  {
    ERROR("no basis functions in fe_space %s\n", NAME(drdv->fe_space));
    return;
  }
  GET_STRUCT(admin, drdv->fe_space);
  GET_STRUCT(bas_fcts, drdv->fe_space);

  get_dof_indices3_2d(pdof, el, admin, bas_fcts);

/*--------------------------------------------------------------------------*/
/*  values on child[0]                                                      */
/*--------------------------------------------------------------------------*/
  get_dof_indices3_2d(cdof, el->child[0], admin, bas_fcts);

  for (j = 0; j < DIM_OF_WORLD; j++)
  {
    v[pdof[0]][j] += (0.0625*(-v[cdof[2]][j] + v[cdof[6]][j] - v[cdof[9]][j])
		      + 0.3125*v[cdof[3]][j]);
    v[pdof[1]][j] += 0.0625*(-v[cdof[2]][j] +  v[cdof[3]][j] + v[cdof[6]][j]
			     + v[cdof[9]][j]);
    v[pdof[3]][j] += -0.25*v[cdof[6]][j] - 0.125*v[cdof[9]][j];
    v[pdof[4]][j] +=  0.5*v[cdof[6]][j];
    v[pdof[5]][j] +=  0.5*v[cdof[6]][j];
    v[pdof[6]][j] += -0.25*v[cdof[6]][j] + 0.375*v[cdof[9]][j];
    v[pdof[7]][j] = (0.5625*v[cdof[2]][j] + 0.9375*v[cdof[3]][j]
		     + v[cdof[4]][j] - 0.0625*v[cdof[6]][j]
		     + 0.1875*v[cdof[9]][j]);
    v[pdof[8]][j] = (0.5625*v[cdof[2]][j] - 0.3125*v[cdof[3]][j]
		     - 0.0625*v[cdof[6]][j] - 0.1875*v[cdof[9]][j]);
    v[pdof[9]][j] =  v[cdof[5]][j] + 0.5*v[cdof[6]][j] + 0.75*v[cdof[9]][j];
  }

/*--------------------------------------------------------------------------*/
/*  values on child[1]                                                      */
/*--------------------------------------------------------------------------*/

  get_dof_indices3_2d(cdof, el->child[1], admin, bas_fcts);

  for (j = 0; j < DIM_OF_WORLD; j++)
  {
    v[pdof[0]][j] += 0.0625*v[cdof[6]][j] + 0.0625*v[cdof[9]][j];
    v[pdof[1]][j] += 0.3125*v[cdof[6]][j] - 0.0625*v[cdof[9]][j];
    v[pdof[3]][j] += 0.375*v[cdof[9]][j];
    v[pdof[6]][j] += -0.125*v[cdof[9]][j];
    v[pdof[7]][j] += -0.3125*v[cdof[6]][j] - 0.18750*v[cdof[9]][j];
    v[pdof[8]][j] += (v[cdof[5]][j] + 0.9375*v[cdof[6]][j]
		      + 0.1875*v[cdof[9]][j]);
    v[pdof[9]][j] += 0.75*v[cdof[9]][j];
  }

  if (n <= 1)  return;

/*--------------------------------------------------------------------------*/
/*  adjust the value on the neihgbour                                       */
/*--------------------------------------------------------------------------*/

  el = list[1].el_info.el;
  get_dof_indices3_2d(pdof, el, admin, bas_fcts);

/*--------------------------------------------------------------------------*/
/*  values on neigh's child[0]                                              */
/*--------------------------------------------------------------------------*/
  get_dof_indices3_2d(cdof, el->child[0], admin, bas_fcts);

  for (j = 0; j < DIM_OF_WORLD; j++)
  {
    v[pdof[0]][j] += 0.0625*(v[cdof[6]][j] - v[cdof[9]][j]);
    v[pdof[1]][j] += 0.0625*(v[cdof[6]][j] + v[cdof[9]][j]);
    v[pdof[3]][j] += -0.25*v[cdof[6]][j] - 0.12500*v[cdof[9]][j];
    v[pdof[4]][j] += 0.5*v[cdof[6]][j];
    v[pdof[5]][j] += 0.5*v[cdof[6]][j];
    v[pdof[6]][j] += -0.25*v[cdof[6]][j] + 0.375*v[cdof[9]][j];
    v[pdof[7]][j] += -0.0625*v[cdof[6]][j] + 0.1875*v[cdof[9]][j];
    v[pdof[8]][j] += -0.0625*v[cdof[6]][j] - 0.1875*v[cdof[9]][j];
    v[pdof[9]][j] =  v[cdof[5]][j] + 0.5*v[cdof[6]][j] + 0.75*v[cdof[9]][j];
  }

/*--------------------------------------------------------------------------*/
/*  values on neigh's child[1]                                              */
/*--------------------------------------------------------------------------*/

  node = drdv->fe_space->admin->mesh->node[CENTER];
  n0 = admin->n0_dof[CENTER];
  dof9 = el->child[1]->dof[node][n0];

  for (j = 0; j < DIM_OF_WORLD; j++)
  {
    v[pdof[0]][j] += 0.0625*v[dof9][j];
    v[pdof[1]][j] -= 0.0625*v[dof9][j];
    v[pdof[3]][j] += 0.375*v[dof9][j];
    v[pdof[6]][j] -= 0.125*v[dof9][j];
    v[pdof[7]][j] -= 0.1875*v[dof9][j];
    v[pdof[8]][j] += 0.1875*v[dof9][j];
    v[pdof[9]][j] += 0.75*v[dof9][j];
  }

  return;
}

static const BAS_FCT      phi3_2d[N_BAS_LAG_3_2D] = {
  phi3v0_2d, phi3v1_2d, phi3v2_2d,
  phi3e0_2d, phi3e1_2d, phi3e2_2d,
  phi3e3_2d, phi3e4_2d, phi3e5_2d,
  phi3c0_2d
};
static const GRD_BAS_FCT  grd_phi3_2d[N_BAS_LAG_3_2D] = {
  grd_phi3v0_2d, grd_phi3v1_2d,
  grd_phi3v2_2d, grd_phi3e0_2d,
  grd_phi3e1_2d, grd_phi3e2_2d,
  grd_phi3e3_2d, grd_phi3e4_2d,
  grd_phi3e5_2d, grd_phi3c0_2d
};
static const D2_BAS_FCT   D2_phi3_2d[N_BAS_LAG_3_2D]  = {
  D2_phi3v0_2d, D2_phi3v1_2d,
  D2_phi3v2_2d, D2_phi3e0_2d,
  D2_phi3e1_2d, D2_phi3e2_2d,
  D2_phi3e3_2d, D2_phi3e4_2d,
  D2_phi3e5_2d, D2_phi3c0_2d
};

const D3_BAS_FCT D3_phi3_2d[N_BAS_LAG_3_2D]  = {
  D3_phi3v0_2d, D3_phi3v1_2d,
  D3_phi3v2_2d, D3_phi3e0_2d,
  D3_phi3e1_2d, D3_phi3e2_2d,
  D3_phi3e3_2d, D3_phi3e4_2d,
  D3_phi3e5_2d, D3_phi3c0_2d
};

/* For each wall the mapping from the wall basis functions to the
 * local DOFs on the reference element. See also submesh.c.
 */
static const int trace_mapping_lag_3_2d[N_WALLS_2D][N_BAS_LAG_3_1D] = {
  { 1, 2, 3, 4 }, { 2, 0, 5, 6 }, { 0, 1, 7, 8 }
};

static const BAS_FCTS lagrange3_2d = {
  "lagrange3_2d", 2, 1, N_BAS_LAG_3_2D, N_BAS_LAG_3_2D, 3,
  {1, 1, 2, 0}, /* VERTEX, CENTER, EDGE, FACE */
  -1, /* trace_admin */
  INIT_BFCTS_CHAIN(lagrange3_2d),
  INIT_ELEMENT_INITIALIZER(NULL, FILL_NOTHING), /* init_element + fill-flags */
  phi3_2d, grd_phi3_2d, D2_phi3_2d, D3_phi3_2d,
  NULL, /* fourth derivatives */
  NULL, NULL, NULL, false, /* phi_d etc. */  
  /********************/
  &lagrange3_1d, /* trace space */
  { { { trace_mapping_lag_3_2d[0],
	trace_mapping_lag_3_2d[1],
	trace_mapping_lag_3_2d[2], }, }, }, /* trace mapping */
  { N_BAS_LAG_3_1D,
    N_BAS_LAG_3_1D,
    N_BAS_LAG_3_1D, }, /* n_trace_bas_fcts */
  get_dof_indices3_2d,
  get_bound3_2d,
  interpol3_2d,
  interpol_d_3_2d,
  interpol_dow_3_2d,
  get_int_vec3_2d,
  get_real_vec3_2d,
  get_real_d_vec3_2d,
  (GET_REAL_VEC_D_TYPE)get_real_d_vec3_2d,
  get_uchar_vec3_2d,
  get_schar_vec3_2d,
  get_ptr_vec3_2d,
  get_real_dd_vec3_2d,
  real_refine_inter3_2d,
  real_coarse_inter3_2d,
  real_coarse_restr3_2d,
  real_d_refine_inter3_2d,
  real_d_coarse_inter3_2d,
  real_d_coarse_restr3_2d,
  (REF_INTER_FCT_D)real_d_refine_inter3_2d,
  (REF_INTER_FCT_D)real_d_coarse_inter3_2d,
  (REF_INTER_FCT_D)real_d_coarse_restr3_2d,
  (void *)&lag_3_2d_data
};
