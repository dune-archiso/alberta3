/*--------------------------------------------------------------------------*/
/* ALBERTA:  an Adaptive multi Level finite element toolbox using           */
/*           Bisectioning refinement and Error control by Residual          */
/*           Techniques for scientific Applications                         */
/*                                                                          */
/* file:     lagrange_2_2d.c                                                */
/*                                                                          */
/* description:  piecewise quadratic Lagrange elements in 2d                */
/*                                                                          */
/*--------------------------------------------------------------------------*/
/*                                                                          */
/*  authors:   Alfred Schmidt                                               */
/*             Zentrum fuer Technomathematik                                */
/*             Fachbereich 3 Mathematik/Informatik                          */
/*             Universitaet Bremen                                          */
/*             Bibliothekstr. 2                                             */
/*             D-28359 Bremen, Germany                                      */
/*                                                                          */
/*             Kunibert G. Siebert                                          */
/*             Institut fuer Mathematik                                     */
/*             Universitaet Augsburg                                        */
/*             Universitaetsstr. 14                                         */
/*             D-86159 Augsburg, Germany                                    */
/*                                                                          */
/*  http://www.mathematik.uni-freiburg.de/IAM/ALBERTA                       */
/*                                                                          */
/*  (c) by A. Schmidt and K.G. Siebert (1996-2003)                          */
/*                                                                          */
/*--------------------------------------------------------------------------*/

static const REAL_B bary2_2d[N_BAS_LAG_2_2D] = {
  INIT_BARY_2D(1.0, 0.0, 0.0),
  INIT_BARY_2D(0.0, 1.0, 0.0),
  INIT_BARY_2D(0.0, 0.0, 1.0),
  INIT_BARY_2D(0.0, 0.5, 0.5),
  INIT_BARY_2D(0.5, 0.0, 0.5),
  INIT_BARY_2D(0.5, 0.5, 0.0)
};

static LAGRANGE_DATA lag_2_2d_data = {
  bary2_2d,
};

/****************************************************************************/
/*  basisfunction at vertex 0                                               */
/****************************************************************************/

static REAL phi2v0_2d(const REAL_B lambda, const BAS_FCTS *thisptr )
{
  return(lambda[0]*(2.0*lambda[0] - 1.0));
}

static const REAL *grd_phi2v0_2d(const REAL_B lambda, const BAS_FCTS *thisptr)
{
  static REAL_B grd = {0};

  grd[0] = 4.0*lambda[0] - 1.0;
  return((const REAL *) grd);
}

static const REAL_B (*D2_phi2v0_2d(const REAL *lambda, const BAS_FCTS *thisptr))
{
  static const REAL_BB D2 = {INIT_BARY_2D(4, 0, 0),
			     INIT_BARY_2D(0, 0, 0),
			     INIT_BARY_2D(0, 0, 0),};

  return(D2);
}

/****************************************************************************/
/*  basisfunction at vertex 1                                               */
/****************************************************************************/

static REAL phi2v1_2d(const REAL_B lambda, const BAS_FCTS *thisptr)
{
  return(lambda[1]*(2.0*lambda[1] - 1.0));
}

static const REAL *grd_phi2v1_2d(const REAL_B lambda, const BAS_FCTS *thisptr)
{
  static REAL_B grd = {0};

  grd[1] = 4.0*lambda[1] - 1.0;
  return((const REAL *) grd);
}

static const REAL_B (*D2_phi2v1_2d(const REAL *lambda, const BAS_FCTS *thisptr))
{
  static const REAL_BB D2 = {INIT_BARY_2D(0, 0, 0),
			     INIT_BARY_2D(0, 4, 0),
			     INIT_BARY_2D(0, 0, 0),};

  return(D2);
}

/****************************************************************************/
/*  basisfunction at vertex 2                                               */
/****************************************************************************/

static REAL phi2v2_2d(const REAL_B lambda, const BAS_FCTS *thisptr)
{
  return(lambda[2]*(2.0*lambda[2] - 1.0));
}

static const REAL *grd_phi2v2_2d(const REAL_B lambda, const BAS_FCTS *thisptr)
{
  static REAL_B grd = {0};

  grd[2] = 4.0*lambda[2] - 1.0;
  return((const REAL *) grd);
}

static const REAL_B (*D2_phi2v2_2d(const REAL *lambda, const BAS_FCTS *thisptr))
{
  static const REAL_BB D2 = {INIT_BARY_2D(0, 0, 0),
			     INIT_BARY_2D(0, 0, 0),
			     INIT_BARY_2D(0, 0, 4),};

  return(D2);
}

/****************************************************************************/
/*  basisfunction at edge 0                                                 */
/****************************************************************************/

static REAL phi2e0_2d(const REAL_B lambda, const BAS_FCTS *thisptr)
{
  return(4.0*lambda[1]*lambda[2]);
}

static const REAL *grd_phi2e0_2d(const REAL_B lambda, const BAS_FCTS *thisptr)
{
  static REAL_B grd = {0};

  grd[1] = 4.0*lambda[2];
  grd[2] = 4.0*lambda[1];
  return((const REAL *) grd);
}

static const REAL_B (*D2_phi2e0_2d(const REAL *lambda, const BAS_FCTS *thisptr))
{
  static const REAL_BB D2 = {INIT_BARY_2D(0, 0, 0),
			     INIT_BARY_2D(0, 0, 4),
			     INIT_BARY_2D(0, 4, 0),};
  return(D2);
}

/****************************************************************************/
/*  basisfunction at edge 1                                                 */
/****************************************************************************/

static REAL phi2e1_2d(const REAL_B lambda, const BAS_FCTS *thisptr)
{
  return(4.0*lambda[0]*lambda[2]);
}

static const REAL *grd_phi2e1_2d(const REAL_B lambda, const BAS_FCTS *thisptr)
{
  static REAL_B grd = {0};

  grd[0] = 4.0*lambda[2];
  grd[2] = 4.0*lambda[0];
  return((const REAL *) grd);
}

static const REAL_B (*D2_phi2e1_2d(const REAL *lambda, const BAS_FCTS *thisptr))
{
  static const REAL_BB D2 = {INIT_BARY_2D(0, 0, 4),
			     INIT_BARY_2D(0, 0, 0),
			     INIT_BARY_2D(4, 0, 0),};
  return(D2);
}

/****************************************************************************/
/*  basisfunction at edge 2                                                 */
/****************************************************************************/

static REAL phi2e2_2d(const REAL_B lambda, const BAS_FCTS *thisptr)
{
  return(4.0*lambda[0]*lambda[1]);
}

static const REAL *grd_phi2e2_2d(const REAL_B lambda, const BAS_FCTS *thisptr)
{
  static REAL_B grd = {0};

  grd[0] = 4.0*lambda[1];
  grd[1] = 4.0*lambda[0];
  return((const REAL *) grd);
}

static const REAL_B (*D2_phi2e2_2d(const REAL *lambda, const BAS_FCTS *thisptr))
{
  static const REAL_BB D2 = {INIT_BARY_2D(0, 4, 0),
			     INIT_BARY_2D(4, 0, 0),
			     INIT_BARY_2D(0, 0, 0),};
  return(D2);
}

/******************************************************************************/

#undef DEF_EL_VEC_2_2D
#define DEF_EL_VEC_2_2D(type, name)				\
  DEF_EL_VEC_CONST(type, name, N_BAS_LAG_2_2D, N_BAS_LAG_2_2D)

#undef DEFUN_GET_EL_VEC_2_2D
#define DEFUN_GET_EL_VEC_2_2D(name, type, admin, body, ...)		\
  static const EL_##type##_VEC *					\
  get_##name##2_2d(type##_VEC_TYPE *vec, const EL *el, __VA_ARGS__)	\
  {									\
    FUNCNAME("get_"#name"2_2d");					\
    static DEF_EL_VEC_2_2D(type, rvec_space);				\
    type##_VEC_TYPE *rvec = vec ? vec : rvec_space->vec;		\
    int n0, node0, ibas, inode;						\
    DOF **dofptr = el->dof, dof;					\
									\
    DEBUG_TEST_EXIT(true, "");						\
									\
    n0 = (admin)->n0_dof[VERTEX];					\
    for (ibas = inode = 0; ibas < N_VERTICES_2D; inode++, ibas++) {	\
      dof = dofptr[inode][n0];						\
      body;								\
    }									\
									\
    n0 = (admin)->n0_dof[EDGE];						\
    node0 = (admin)->mesh->node[EDGE];					\
    for (inode = 0; inode < N_EDGES_2D; inode++, ibas++) {		\
      dof = dofptr[node0+inode][n0];					\
      body;								\
    }									\
									\
    return vec ? NULL : rvec_space;					\
  }									\
  struct _AI_semicolon_dummy

#undef DEFUN_GET_EL_DOF_VEC_2_2D
#define DEFUN_GET_EL_DOF_VEC_2_2D(name, type, ASSIGN)			\
  DEFUN_GET_EL_VEC_2_2D(_##name##_vec, type, dv->fe_space->admin,	\
			ASSIGN(dv->vec[dof], rvec[ibas]),		\
			const DOF_##type##_VEC *dv);			\
  static const EL_##type##_VEC *					\
  get_##name##_vec2_2d(type##_VEC_TYPE *vec, const EL *el,		\
		       const DOF_##type##_VEC *dv)			\
  {									\
    EL_##type##_VEC *vec_loc = dv->vec_loc;				\
									\
    if (vec != NULL || vec_loc == NULL) {				\
      return get__##name##_vec2_2d(vec, el, dv);			\
    } else {								\
      get__##name##_vec2_2d(vec_loc->vec, el, dv);			\
      return vec_loc;							\
    }									\
  }									\
  struct _AI_semicolon_dummy

#undef COPY_EQ
#define COPY_EQ(a, b) (b) = (a)

/****************************************************************************/
/*  functions for combining basisfunctions with coefficients                */
/****************************************************************************/

DEFUN_GET_EL_VEC_2_2D(dof_indices, DOF, admin,
		      rvec[ibas] = dof,
		      const DOF_ADMIN *admin, const BAS_FCTS *thisptr);

static const EL_BNDRY_VEC *
get_bound2_2d(BNDRY_FLAGS *vec,
	      const EL_INFO *el_info, const BAS_FCTS *thisptr)
{
  FUNCNAME("get_bound2_2d");
  static DEF_EL_VEC_2_2D(BNDRY, rvec_space);
  BNDRY_FLAGS *rvec = vec ? vec : rvec_space->vec;
  int i;

  TEST_FLAG(FILL_BOUND, el_info);

  for (i = 0; i < N_VERTICES_2D; i++) {
    BNDRY_FLAGS_CPY(rvec[i], el_info->vertex_bound[i]);
  }
  for (i = 0; i < N_EDGES_2D; i++) {
    BNDRY_FLAGS_CPY(rvec[N_VERTICES_2D+i], el_info->edge_bound[i]);
  }

  return vec ? NULL : rvec_space;
}

/*--------------------------------------------------------------------*/
/*--- function for accessing a local DOF_INT_VEC                   ---*/
/*--------------------------------------------------------------------*/

DEFUN_GET_EL_DOF_VEC_2_2D(int, INT, COPY_EQ);

/*--------------------------------------------------------------------*/
/*--- function for accessing a local DOF_REAL_VEC                  ---*/
/*--------------------------------------------------------------------*/

DEFUN_GET_EL_DOF_VEC_2_2D(real, REAL, COPY_EQ);

/*--------------------------------------------------------------------*/
/*--- function for accessing a local DOF_REAL_D_VEC                ---*/
/*--------------------------------------------------------------------*/

DEFUN_GET_EL_DOF_VEC_2_2D(real_d, REAL_D, COPY_DOW);

/*--------------------------------------------------------------------*/
/*--- function for accessing a local DOF_SCHAR_VEC                 ---*/
/*--------------------------------------------------------------------*/

DEFUN_GET_EL_DOF_VEC_2_2D(schar, SCHAR, COPY_EQ);

/*--------------------------------------------------------------------*/
/*--- function for accessing a local DOF_UCHAR_VEC                 ---*/
/*--------------------------------------------------------------------*/

DEFUN_GET_EL_DOF_VEC_2_2D(uchar, UCHAR, COPY_EQ);

/*--------------------------------------------------------------------*/
/*--- function for accessing a local DOF_PTR_VEC                   ---*/
/*--------------------------------------------------------------------*/

DEFUN_GET_EL_DOF_VEC_2_2D(ptr, PTR, COPY_EQ);

/*--------------------------------------------------------------------*/
/*--- function for accessing a local DOF_REAL_DD_VEC               ---*/
/*--------------------------------------------------------------------*/

DEFUN_GET_EL_DOF_VEC_2_2D(real_dd, REAL_DD, _AI_MCOPY_DOW);

/*--------------------------------------------------------------------*/
/*--- function for local interpolaton of scalar functions          ---*/
/*--------------------------------------------------------------------*/
 
GENERATE_INTERPOL(/**/, 2, 2, N_BAS_LAG_2_2D);

/*--------------------------------------------------------------------*/
/*--- function for local interpolaton of vector functions          ---*/
/*--------------------------------------------------------------------*/
 
GENERATE_INTERPOL_D(/**/, 2, 2, N_BAS_LAG_2_2D);

GENERATE_INTERPOL_DOW(/**/, 2, 2, N_BAS_LAG_2_2D);

/****************************************************************************/
/*  functions for interpolation/ restriction during refinement/coarsening   */
/****************************************************************************/

static void real_refine_inter2_2d(DOF_REAL_VEC *drv, RC_LIST_EL *list, int n)
{
  FUNCNAME("real_refine_inter2_2d");
  DOF pdof[N_BAS_LAG_2_2D];
  EL        *el;
  REAL      *vec = NULL;
  int       node, n0;
  DOF       cdof;
  const DOF_ADMIN *admin;
  const BAS_FCTS  *bas_fcts;

  if (n < 1) {
    return;
  }
  el = list->el_info.el;

  GET_DOF_VEC(vec, drv);
  if (!drv->fe_space) {
    ERROR("no fe_space in dof_real_vec %s\n", NAME(drv));
    return;
  } else if (!drv->fe_space->bas_fcts) {
    ERROR("no basis functions in fe_space %s\n", NAME(drv->fe_space));
    return;
  }
  GET_STRUCT(admin, drv->fe_space);
  GET_STRUCT(bas_fcts, drv->fe_space);
  
  get_dof_indices2_2d(pdof, el, admin, bas_fcts);

  node = drv->fe_space->admin->mesh->node[VERTEX];
  n0 = admin->n0_dof[VERTEX];

/****************************************************************************/
/*  newest vertex of child[0] and child[1]                                  */
/****************************************************************************/

  cdof = el->child[0]->dof[node+2][n0];  /*      newest vertex is 2 */
  vec[cdof] = vec[pdof[5]];

  node = drv->fe_space->admin->mesh->node[EDGE];
  n0 = admin->n0_dof[EDGE];

/****************************************************************************/
/*  midpoint of edge on child[0] at the refinement edge                     */
/****************************************************************************/

  cdof = el->child[0]->dof[node][n0];
  vec[cdof] = 0.375*vec[pdof[0]] - 0.125*vec[pdof[1]]  + 0.75*vec[pdof[5]];

/****************************************************************************/
/* node in the common edge of child[0] and child[1]                         */
/****************************************************************************/

  cdof = el->child[0]->dof[node+1][n0];
  vec[cdof] = -0.125*(vec[pdof[0]] + vec[pdof[1]]) + 0.25*vec[pdof[5]]
    + 0.5*(vec[pdof[3]] + vec[pdof[4]]);

/****************************************************************************/
/*  midpoint of edge on child[1] at the refinement edge                     */
/****************************************************************************/

  cdof = el->child[1]->dof[node+1][n0];
  vec[cdof] = -0.125*vec[pdof[0]] + 0.375*vec[pdof[1]]  + 0.75*vec[pdof[5]];

  if (n > 1)
  {
/****************************************************************************/
/*  adjust the value at the midpoint of the common edge of neigh's children */
/****************************************************************************/
    el = list[1].el_info.el;
    get_dof_indices2_2d(pdof, el, admin, bas_fcts);

    cdof = el->child[0]->dof[node+1][n0];
    vec[cdof] = -0.125*(vec[pdof[0]] + vec[pdof[1]]) + 0.25*vec[pdof[5]]
      + 0.5*(vec[pdof[3]] + vec[pdof[4]]);
  }
  return;
}

static void real_coarse_inter2_2d(DOF_REAL_VEC *drv, RC_LIST_EL *list, int n)
{
  FUNCNAME("real_coarse_inter2_2d");
  EL        *el;
  REAL      *v = NULL;
  DOF       cdof, pdof;
  const DOF_ADMIN *admin;
  MESH      *mesh = NULL;

  if (n < 1) return;
  el = list->el_info.el;

  GET_DOF_VEC(v, drv);
  if (!drv->fe_space)
  {
    ERROR("no fe_space in dof_real_vec %s\n", NAME(drv));
    return;
  }
  else if (!drv->fe_space->bas_fcts)
  {
    ERROR("no basis functions in fe_space %s\n", NAME(drv->fe_space));
    return;
  }
  GET_STRUCT(admin,drv->fe_space);
  GET_STRUCT(mesh,drv->fe_space);

/****************************************************************************/
/*  values on child[0]                                                      */
/****************************************************************************/

  cdof = el->child[0]->dof[mesh->node[VERTEX]+2][admin->n0_dof[VERTEX]];
  pdof = el->dof[mesh->node[EDGE]+2][admin->n0_dof[EDGE]];
  v[pdof] = v[cdof];

  return;
}

static void real_coarse_restr2_2d(DOF_REAL_VEC *drv, RC_LIST_EL *list, int n)
{
  FUNCNAME("real_coarse_restr2_2d");
  DOF pdof[N_BAS_LAG_2_2D];
  EL        *el;
  REAL      *vec = NULL;
  int       node, n0;
  DOF       cdof2, cdof3, cdof4;
  const DOF_ADMIN *admin;
  const BAS_FCTS  *bas_fcts;

  if (n < 1) return;
  el = list->el_info.el;

  GET_DOF_VEC(vec, drv);
  if (!drv->fe_space)
  {
    ERROR("no fe_space in dof_real_vec %s\n", NAME(drv));
    return;
  }
  else if (!drv->fe_space->bas_fcts)
  {
    ERROR("no basis functions in fe_space %s\n", NAME(drv->fe_space));
    return;
  }
  GET_STRUCT(admin, drv->fe_space);
  GET_STRUCT(bas_fcts, drv->fe_space);

  get_dof_indices2_2d(pdof, el, admin, bas_fcts);

/****************************************************************************/
/*  contributions of dofs located on child[0]                               */
/****************************************************************************/

  node = drv->fe_space->admin->mesh->node[VERTEX];
  n0 = admin->n0_dof[VERTEX];
  cdof2 = el->child[0]->dof[node+2][n0];

  node = drv->fe_space->admin->mesh->node[EDGE];
  n0 = admin->n0_dof[EDGE];
  cdof3 = el->child[0]->dof[node][n0];
  cdof4 = el->child[0]->dof[node+1][n0];

  vec[pdof[0]] += 0.375*vec[cdof3] - 0.125*vec[cdof4];
  vec[pdof[1]] += -0.125*(vec[cdof3] + vec[cdof4]);
  vec[pdof[3]] += 0.5*vec[cdof4];
  vec[pdof[4]] += 0.5*vec[cdof4];
  vec[pdof[5]] = vec[cdof2] + 0.75*vec[cdof3] + 0.25*vec[cdof4];

/****************************************************************************/
/*  contributions of dofs located on child[1] and not on child[0]           */
/****************************************************************************/

  cdof4 = el->child[1]->dof[node+1][n0];

  vec[pdof[0]] += -0.125*vec[cdof4];
  vec[pdof[1]] += 0.375*vec[cdof4];
  vec[pdof[5]] += 0.75*vec[cdof4];

  if (n > 1)
  {
    el = list[1].el_info.el;
    get_dof_indices2_2d(pdof, el, admin, bas_fcts);

/****************************************************************************/
/*  first set those values not effected by previous element                 */
/****************************************************************************/

    cdof4 = el->child[0]->dof[node+1][n0];
    vec[pdof[3]] += 0.5*vec[cdof4];
    vec[pdof[4]] += 0.5*vec[cdof4];

/****************************************************************************/
/*  and now, update the values in the refinement edge                       */
/****************************************************************************/

    vec[pdof[0]] += -0.125*vec[cdof4];
    vec[pdof[1]] += -0.125*vec[cdof4];
    vec[pdof[5]] += 0.25*vec[cdof4];
  }
}

static void real_d_refine_inter2_2d(DOF_REAL_D_VEC *drdv, RC_LIST_EL *list,
				    int n)
{
  FUNCNAME("real_d_refine_inter2_2d");
  DOF pdof[N_BAS_LAG_2_2D];
  EL        *el;
  REAL_D    *vec = NULL;
  int       node, n0, j;
  DOF       cdof;
  const DOF_ADMIN *admin;
  const BAS_FCTS  *bas_fcts;

  if (n < 1) return;
  el = list->el_info.el;

  GET_DOF_VEC(vec, drdv);
  if (!drdv->fe_space)
  {
    ERROR("no fe_space in dof_real_d_vec %s\n", NAME(drdv));
    return;
  }
  else if (!drdv->fe_space->bas_fcts)
  {
    ERROR("no basis functions in fe_space %s\n", NAME(drdv->fe_space));
    return;
  }
  GET_STRUCT(admin, drdv->fe_space);
  GET_STRUCT(bas_fcts, drdv->fe_space);

  get_dof_indices2_2d(pdof, el, admin, bas_fcts);

  node = drdv->fe_space->admin->mesh->node[VERTEX];
  n0 = admin->n0_dof[VERTEX];

/****************************************************************************/
/*  newest vertex of child[0] and child[1]                                  */
/****************************************************************************/

  cdof = el->child[0]->dof[node+2][n0];  /*      newest vertex is 2 */
  for (j = 0; j < DIM_OF_WORLD; j++)
    vec[cdof][j] = vec[pdof[5]][j];

  node = drdv->fe_space->admin->mesh->node[EDGE];
  n0 = admin->n0_dof[EDGE];

/****************************************************************************/
/*  midpoint of edge on child[0] at the refinement edge                     */
/****************************************************************************/

  cdof = el->child[0]->dof[node][n0];
  for (j = 0; j < DIM_OF_WORLD; j++)
    vec[cdof][j] = (0.375*vec[pdof[0]][j] - 0.125*vec[pdof[1]][j]
		    + 0.75*vec[pdof[5]][j]);

/****************************************************************************/
/* node in the common edge of child[0] and child[1]                         */
/****************************************************************************/

  cdof = el->child[0]->dof[node+1][n0];
  for (j = 0; j < DIM_OF_WORLD; j++)
    vec[cdof][j] = -0.125*(vec[pdof[0]][j] + vec[pdof[1]][j])
      + 0.25*vec[pdof[5]][j] + 0.5*(vec[pdof[3]][j] + vec[pdof[4]][j]);

/****************************************************************************/
/*  midpoint of edge on child[1] at the refinement edge                     */
/****************************************************************************/

  cdof = el->child[1]->dof[node+1][n0];
  for (j = 0; j < DIM_OF_WORLD; j++)
    vec[cdof][j] = (-0.125*vec[pdof[0]][j] + 0.375*vec[pdof[1]][j]
		    + 0.75*vec[pdof[5]][j]);

  if (n > 1)
  {
/****************************************************************************/
/*  adjust the value at the midpoint of the common edge of neigh's children */
/****************************************************************************/
    el = list[1].el_info.el;
    get_dof_indices2_2d(pdof, el, admin, bas_fcts);

    cdof = el->child[0]->dof[node+1][n0];
    for (j = 0; j < DIM_OF_WORLD; j++)
      vec[cdof][j] = -0.125*(vec[pdof[0]][j] + vec[pdof[1]][j])
	+ 0.25*vec[pdof[5]][j]+ 0.5*(vec[pdof[3]][j] + vec[pdof[4]][j]);
  }
}

static void real_d_coarse_inter2_2d(DOF_REAL_D_VEC *drdv, RC_LIST_EL *list,
				    int n)
{
  FUNCNAME("real_d_coarse_inter2_2d");
  EL        *el;
  REAL_D    *v = NULL;
  int       j;
  DOF       cdof, pdof;
  MESH      *mesh = NULL;
  const DOF_ADMIN *admin;

  if (n < 1) return;
  el = list->el_info.el;

  GET_DOF_VEC(v, drdv);
  if (!drdv->fe_space)
  {
    ERROR("no fe_space in dof_real_d_vec %s\n", NAME(drdv));
    return;
  }
  else if (!drdv->fe_space->bas_fcts)
  {
    ERROR("no basis functions in fe_space %s\n", NAME(drdv->fe_space));
    return;
  }
  GET_STRUCT(admin,drdv->fe_space);
  GET_STRUCT(mesh,drdv->fe_space);

/****************************************************************************/
/*  values on child[0]                                                      */
/****************************************************************************/

  cdof = el->child[0]->dof[mesh->node[VERTEX]+2][admin->n0_dof[VERTEX]];
  pdof = el->dof[mesh->node[EDGE]+2][admin->n0_dof[EDGE]];
  for (j = 0; j < DIM_OF_WORLD; j++)
    v[pdof][j] = v[cdof][j];

  return;
}

static void real_d_coarse_restr2_2d(DOF_REAL_D_VEC *drdv, RC_LIST_EL *list,
				    int n)
{
  FUNCNAME("real_d_coarse_restr2_2d");
  DOF pdof[N_BAS_LAG_2_2D];
  EL        *el;
  REAL_D    *vec = NULL;
  int       node, n0, j;
  DOF       cdof2, cdof3, cdof4;
  const DOF_ADMIN *admin;
  const BAS_FCTS  *bas_fcts;

  if (n < 1) return;
  el = list->el_info.el;

  GET_DOF_VEC(vec, drdv);
  if (!drdv->fe_space)
  {
    ERROR("no fe_space in dof_real_d_vec %s\n", NAME(drdv));
    return;
  }
  else if (!drdv->fe_space->bas_fcts)
  {
    ERROR("no basis functions in fe_space %s\n", NAME(drdv->fe_space));
    return;
  }
  GET_STRUCT(admin, drdv->fe_space);
  GET_STRUCT(bas_fcts, drdv->fe_space);

  get_dof_indices2_2d(pdof, el, admin, bas_fcts);

/****************************************************************************/
/*  contributions of dofs located on child[0]                               */
/****************************************************************************/

  node = drdv->fe_space->admin->mesh->node[VERTEX];
  n0 = admin->n0_dof[VERTEX];
  cdof2 = el->child[0]->dof[node+2][n0];

  node = drdv->fe_space->admin->mesh->node[EDGE];
  n0 = admin->n0_dof[EDGE];
  cdof3 = el->child[0]->dof[node][n0];
  cdof4 = el->child[0]->dof[node+1][n0];

  for (j = 0; j < DIM_OF_WORLD; j++)
  {
    vec[pdof[0]][j] += 0.375*vec[cdof3][j] - 0.125*vec[cdof4][j];
    vec[pdof[1]][j] += -0.125*(vec[cdof3][j] + vec[cdof4][j]);
    vec[pdof[3]][j] += 0.5*vec[cdof4][j];
    vec[pdof[4]][j] += 0.5*vec[cdof4][j];
    vec[pdof[5]][j] = vec[cdof2][j] + 0.75*vec[cdof3][j] + 0.25*vec[cdof4][j];
  }
/****************************************************************************/
/*  contributions of dofs located on child[1] and not on child[0]           */
/****************************************************************************/

  cdof4 = el->child[1]->dof[node+1][n0];

  for (j = 0; j < DIM_OF_WORLD; j++)
  {
    vec[pdof[0]][j] += -0.125*vec[cdof4][j];
    vec[pdof[1]][j] += 0.375*vec[cdof4][j];
    vec[pdof[5]][j] += 0.75*vec[cdof4][j];
  }
  if (n > 1)
  {
    el = list[1].el_info.el;
    get_dof_indices2_2d(pdof, el, admin, bas_fcts);

/****************************************************************************/
/*  first set those values not effected by previous element                 */
/****************************************************************************/

    cdof4 = el->child[0]->dof[node+1][n0];

    for (j = 0; j < DIM_OF_WORLD; j++)
    {
      vec[pdof[3]][j] += 0.5*vec[cdof4][j];
      vec[pdof[4]][j] += 0.5*vec[cdof4][j];

/****************************************************************************/
/*  and now, update the values in the refinement edge                       */
/****************************************************************************/

      vec[pdof[0]][j] += -0.125*vec[cdof4][j];
      vec[pdof[1]][j] += -0.125*vec[cdof4][j];
      vec[pdof[5]][j] += 0.25*vec[cdof4][j];
    }
  }
  return;
}

static const BAS_FCT      phi2_2d[N_BAS_LAG_2_2D]     = {
  phi2v0_2d, phi2v1_2d, phi2v2_2d,
  phi2e0_2d, phi2e1_2d, phi2e2_2d
};
static const GRD_BAS_FCT  grd_phi2_2d[N_BAS_LAG_2_2D] = {
  grd_phi2v0_2d, grd_phi2v1_2d,
  grd_phi2v2_2d, grd_phi2e0_2d,
  grd_phi2e1_2d, grd_phi2e2_2d
};
static const D2_BAS_FCT   D2_phi2_2d[N_BAS_LAG_2_2D]  = {
  D2_phi2v0_2d, D2_phi2v1_2d,
  D2_phi2v2_2d, D2_phi2e0_2d,
  D2_phi2e1_2d, D2_phi2e2_2d
};

/* For each wall the mapping from the wall basis functions to the
 * local DOFs on the reference element. See also submesh.c.
 */
static const int trace_mapping_lag_2_2d[N_WALLS_2D][N_BAS_LAG_2_1D] = {
  { 1, 2, 3 }, { 2, 0, 4 }, { 0, 1, 5 }
};

static const BAS_FCTS lagrange2_2d = {
  "lagrange2_2d", 2, 1, N_BAS_LAG_2_2D, N_BAS_LAG_2_2D, 2,
  {1, 0, 1, 0}, /* VERTEX, CENTER, EDGE, FACE */
  -1, /* trace_admin */
  INIT_BFCTS_CHAIN(lagrange2_2d),
  INIT_ELEMENT_INITIALIZER(NULL, FILL_NOTHING), /* init_element + fill-flags */
  phi2_2d, grd_phi2_2d, D2_phi2_2d,
  NULL, NULL, /* third and fourth derivatives */
  NULL, NULL, NULL, false, /* phi_d etc. */  
  /********************/
  &lagrange2_1d, /* trace space */
  { { { trace_mapping_lag_2_2d[0],
	trace_mapping_lag_2_2d[1],
	trace_mapping_lag_2_2d[2], }, }, }, /* trace mapping */
  { N_BAS_LAG_2_1D,
    N_BAS_LAG_2_1D,
    N_BAS_LAG_2_1D, }, /* n_trace_bas_fcts */
  get_dof_indices2_2d,
  get_bound2_2d,
  interpol2_2d,
  interpol_d_2_2d,
  interpol_dow_2_2d,
  get_int_vec2_2d,
  get_real_vec2_2d,
  get_real_d_vec2_2d,
  (GET_REAL_VEC_D_TYPE)get_real_d_vec2_2d,
  get_uchar_vec2_2d,
  get_schar_vec2_2d,
  get_ptr_vec2_2d,
  get_real_dd_vec2_2d,
  real_refine_inter2_2d,
  real_coarse_inter2_2d,
  real_coarse_restr2_2d,
  real_d_refine_inter2_2d,
  real_d_coarse_inter2_2d,
  real_d_coarse_restr2_2d,
  (REF_INTER_FCT_D)real_d_refine_inter2_2d,
  (REF_INTER_FCT_D)real_d_coarse_inter2_2d,
  (REF_INTER_FCT_D)real_d_coarse_restr2_2d,
  (void *)&lag_2_2d_data
};

