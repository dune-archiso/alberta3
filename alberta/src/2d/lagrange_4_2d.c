/*--------------------------------------------------------------------------*/
/* ALBERTA:  an Adaptive multi Level finite element toolbox using           */
/*           Bisectioning refinement and Error control by Residual          */
/*           Techniques for scientific Applications                         */
/*                                                                          */
/* file:     lagrange_4_2d.c                                                */
/*                                                                          */
/* description:  piecewise quartic Lagrange elements in 2d                  */
/*                                                                          */
/*--------------------------------------------------------------------------*/
/*                                                                          */
/*  authors:   Alfred Schmidt                                               */
/*             Zentrum fuer Technomathematik                                */
/*             Fachbereich 3 Mathematik/Informatik                          */
/*             Universitaet Bremen                                          */
/*             Bibliothekstr. 2                                             */
/*             D-28359 Bremen, Germany                                      */
/*                                                                          */
/*             Kunibert G. Siebert                                          */
/*             Institut fuer Mathematik                                     */
/*             Universitaet Augsburg                                        */
/*             Universitaetsstr. 14                                         */
/*             D-86159 Augsburg, Germany                                    */
/*                                                                          */
/*  http://www.mathematik.uni-freiburg.de/IAM/ALBERTA                       */
/*                                                                          */
/*  (c) by A. Schmidt and K.G. Siebert (1996-2003)                          */
/*                                                                          */
/*--------------------------------------------------------------------------*/

static const REAL_B bary4_2d[N_BAS_LAG_4_2D] = {
  INIT_BARY_2D(    1.0,     0.0,     0.0),
  INIT_BARY_2D(    0.0,     1.0,     0.0),
  INIT_BARY_2D(    0.0,     0.0,     1.0),
  INIT_BARY_2D(    0.0, 3.0/4.0, 1.0/4.0),
  INIT_BARY_2D(    0.0,     0.5,     0.5),
  INIT_BARY_2D(    0.0, 1.0/4.0, 3.0/4.0),
  INIT_BARY_2D(1.0/4.0,     0.0, 3.0/4.0),
  INIT_BARY_2D(    0.5,     0.0,     0.5),
  INIT_BARY_2D(3.0/4.0,     0.0, 1.0/4.0),
  INIT_BARY_2D(3.0/4.0, 1.0/4.0,     0.0),
  INIT_BARY_2D(    0.5,     0.5,     0.0),
  INIT_BARY_2D(1.0/4.0, 3.0/4.0,     0.0),
  INIT_BARY_2D(1.0/2.0, 1.0/4.0, 1.0/4.0),
  INIT_BARY_2D(1.0/4.0, 1.0/2.0, 1.0/4.0),
  INIT_BARY_2D(1.0/4.0, 1.0/4.0, 1.0/2.0)
};

static LAGRANGE_DATA lag_4_2d_data = {
  bary4_2d,
  NULL /* lumping_quad */,
};

/*--------------------------------------------------------------------------*/
/*  basisfunction at vertex 0                                               */
/*--------------------------------------------------------------------------*/

static REAL phi4v0_2d(const REAL_B l, const BAS_FCTS *thisptr)
{
  return (((32.0*l[0] - 48.0)*l[0] + 22.0)*l[0] - 3.0)*l[0]/3.0;
}

static const REAL *grd_phi4v0_2d(const REAL_B l, const BAS_FCTS *thisptr)
{
  static REAL_B grd = {0};
  grd[0] = ((128.0*l[0] - 144.0)*l[0] + 44.0)*l[0]/3.0 - 1.0;
  return (const REAL *) grd;
}

static const REAL_B *D2_phi4v0_2d(const REAL_B l, const BAS_FCTS *thisptr)
{
  static REAL_BB D2;
  D2[0][0] = (128.0*l[0] - 96.0)*l[0] + 44.0/3.0;
  return (const REAL_B *) D2;
}


static const REAL_BB *D3_phi4v0_2d(const REAL_B l, const BAS_FCTS *thisptr)
{
  static REAL_BBB D3;

  D3[0][0][0] = 256.0*l[0] - 96.0;

  return (const REAL_BB *)D3;
}

static const REAL_BBB *D4_phi4v0_2d(const REAL_B l, const BAS_FCTS *thisptr)
{
  static REAL_BBBB D4;

  D4[0][0][0][0] = 256.0;

  return (const REAL_BBB *)D4;
}

/*--------------------------------------------------------------------------*/
/*  basisfunction at vertex 1                                               */
/*--------------------------------------------------------------------------*/

static REAL phi4v1_2d(const REAL_B l, const BAS_FCTS *thisptr)
{
  return (((32.0*l[1] - 48.0)*l[1] + 22.0)*l[1] - 3.0)*l[1]/3.0;
}

static const REAL *grd_phi4v1_2d(const REAL_B l, const BAS_FCTS *thisptr)
{
  static REAL_B grd = {0};
  grd[1] = ((128.0*l[1] - 144.0)*l[1] + 44.0)*l[1]/3.0 - 1.0;
  return (const REAL *) grd;
}

static const REAL_B *D2_phi4v1_2d(const REAL_B l, const BAS_FCTS *thisptr)
{
  static REAL_BB D2;
  D2[1][1] = (128.0*l[1] - 96.0)*l[1] + 44.0/3.0;
  return (const REAL_B *) D2;
}

static const REAL_BB *D3_phi4v1_2d(const REAL_B l, const BAS_FCTS *thisptr)
{
  static REAL_BBB D3;

  D3[1][1][1] = 256.0*l[1] - 96.0;

  return (const REAL_BB *)D3;
}

static const REAL_BBB *D4_phi4v1_2d(const REAL_B l, const BAS_FCTS *thisptr)
{
  static REAL_BBBB D4;

  D4[1][1][1][1] = 256.0;

  return (const REAL_BBB *)D4;
}

/*--------------------------------------------------------------------------*/
/*  basisfunction at vertex 2                                               */
/*--------------------------------------------------------------------------*/

static REAL phi4v2_2d(const REAL_B l, const BAS_FCTS *thisptr)
{
  return (((32.0*l[2] - 48.0)*l[2] + 22.0)*l[2] - 3.0)*l[2]/3.0;
}

static const REAL *grd_phi4v2_2d(const REAL_B l, const BAS_FCTS *thisptr)
{
  static REAL_B grd = {0};
  grd[2] = ((128.0*l[2] - 144.0)*l[2] + 44.0)*l[2]/3.0 - 1.0;
  return (const REAL *) grd;
}

static const REAL_B *D2_phi4v2_2d(const REAL_B l, const BAS_FCTS *thisptr)
{
  static REAL_BB D2;
  D2[2][2] = (128.0*l[2] - 96.0)*l[2] + 44.0/3.0;
  return (const REAL_B *) D2;
}

static const REAL_BB *D3_phi4v2_2d(const REAL_B l, const BAS_FCTS *thisptr)
{
  static REAL_BBB D3;

  D3[2][2][2] = 256.0*l[2] - 96.0;

  return (const REAL_BB *)D3;
}

static const REAL_BBB *D4_phi4v2_2d(const REAL_B l, const BAS_FCTS *thisptr)
{
  static REAL_BBBB D4;

  D4[2][2][2][2] = 256.0;

  return (const REAL_BBB *)D4;
}

/*--------------------------------------------------------------------------*/
/*  basisfunction 0 at edge 0                                               */
/*--------------------------------------------------------------------------*/

static REAL phi4e00_2d(const REAL_B l, const BAS_FCTS *thisptr)
{
  return ((128.0*l[1] - 96.0)*l[1] + 16.0)*l[1]*l[2]/3.0;
}

static const REAL *grd_phi4e00_2d(const REAL_B l, const BAS_FCTS *thisptr)
{
  static REAL_B grd = {0};
  grd[1] = ((128*l[1] - 64.0)*l[1] + 16.0/3.0)*l[2];
  grd[2] = ((128*l[1] - 96.0)*l[1] + 16.0)*l[1]/3.0;
  return (const REAL *) grd;
}

static const REAL_B *D2_phi4e00_2d(const REAL_B l, const BAS_FCTS *thisptr)
{
  static REAL_BB D2;
  D2[1][1] = (256.0*l[1] - 64.0)*l[2];
  D2[1][2] = D2[2][1] = (128.0*l[1] - 64.0)*l[1] + 16.0/3.0;
  return (const REAL_B *) D2;
}

static const REAL_BB *D3_phi4e00_2d(const REAL_B l, const BAS_FCTS *thisptr)
{
  static REAL_BBB D3;

  D3[1][1][1] = 256.0*l[2];
  D3[1][1][2] = D3[1][2][1] = D3[2][1][1] = 256.0*l[1] - 64.0;

  return (const REAL_BB *)D3;
}

static const REAL_BBB *D4_phi4e00_2d(const REAL_B l, const BAS_FCTS *thisptr)
{
  static REAL_BBBB D4;

  D4[2][1][1][1] =
  D4[1][2][1][1] =
  D4[1][1][2][1] =
  D4[1][1][1][2] = 256.0;

  return (const REAL_BBB *)D4;
}

/*--------------------------------------------------------------------------*/
/*  basisfunction 0 at edge 1                                               */
/*--------------------------------------------------------------------------*/

static REAL phi4e10_2d(const REAL_B l, const BAS_FCTS *thisptr)
{
  return ((128.0*l[2] - 96.0)*l[2] + 16.0)*l[2]*l[0]/3.0;
}

static const REAL *grd_phi4e10_2d(const REAL_B l, const BAS_FCTS *thisptr)
{
  static REAL_B grd = {0};
  grd[0] = ((128*l[2] - 96.0)*l[2] + 16.0)*l[2]/3.0;
  grd[2] = ((128*l[2] - 64.0)*l[2] + 16.0/3.0)*l[0];
  return (const REAL *) grd;
}

static const REAL_B *D2_phi4e10_2d(const REAL_B l, const BAS_FCTS *thisptr)
{
  static REAL_BB D2;
  D2[2][2] = (256.0*l[2] - 64.0)*l[0];
  D2[2][0] = D2[0][2] = (128.0*l[2] - 64.0)*l[2] + 16.0/3.0;
  return (const REAL_B *) D2;
}

static const REAL_BB *D3_phi4e10_2d(const REAL_B l, const BAS_FCTS *thisptr)
{
  static REAL_BBB D3;

  D3[2][2][2] = 256.0*l[0];
  D3[0][2][2] = D3[2][0][2] = D3[2][2][0] = 256.0*l[2] - 64.0;

  return (const REAL_BB *)D3;
}

static const REAL_BBB *D4_phi4e10_2d(const REAL_B l, const BAS_FCTS *thisptr)
{
  static REAL_BBBB D4;

  D4[0][2][2][2] =
    D4[2][0][2][2] =
    D4[2][2][0][2] =
    D4[2][2][2][0] = 256.0;

  return (const REAL_BBB *)D4;
}

/*--------------------------------------------------------------------------*/
/*  basisfunction 0 at edge 2                                               */
/*--------------------------------------------------------------------------*/

static REAL phi4e20_2d(const REAL_B l, const BAS_FCTS *thisptr)
{
  return ((128.0*l[0] - 96.0)*l[0] + 16.0)*l[0]*l[1]/3.0;
}

static const REAL *grd_phi4e20_2d(const REAL_B l, const BAS_FCTS *thisptr)
{
  static REAL_B grd = {0};
  grd[0] = ((128*l[0] - 64.0)*l[0] + 16.0/3.0)*l[1];
  grd[1] = ((128*l[0] - 96.0)*l[0] + 16.0)*l[0]/3.0;
  return (const REAL *) grd;
}

static const REAL_B *D2_phi4e20_2d(const REAL_D l, const BAS_FCTS *thisptr)
{
  static REAL_BB D2;
  D2[0][0] = (256.0*l[0] - 64.0)*l[1];
  D2[0][1] = D2[1][0] = (128.0*l[0] - 64.0)*l[0] + 16.0/3.0;
  return (const REAL_B *) D2;
}

static const REAL_BB *D3_phi4e20_2d(const REAL_B l, const BAS_FCTS *thisptr)
{
  static REAL_BBB D3;

  D3[0][0][0] = 256.0*l[1];
  D3[1][0][0] = D3[0][1][0] = D3[0][0][1] = 256.0*l[0] - 64.0;

  return (const REAL_BB *)D3;
}

static const REAL_BBB *D4_phi4e20_2d(const REAL_B l, const BAS_FCTS *thisptr)
{
  static REAL_BBBB D4;

  D4[0][0][0][1] =
    D4[0][0][1][0] =
    D4[0][1][0][0] =
    D4[1][0][0][0] = 256.0;

  return (const REAL_BBB *)D4;
}

/*--------------------------------------------------------------------------*/
/*  basisfunction 1 at edge 0                                               */
/*--------------------------------------------------------------------------*/

static REAL phi4e01_2d(const REAL_B l, const BAS_FCTS *thisptr)
{
  return (4.0*l[1] - 1.0)*l[1]*(4.0*l[2] - 1.0)*l[2]*4.0;
}

static const REAL *grd_phi4e01_2d(const REAL_B l, const BAS_FCTS *thisptr)
{
  static REAL_B grd = {0};
  grd[1] = 4.0*(8.0*l[1] - 1.0)*l[2]*(4.0*l[2] - 1.0);
  grd[2] = 4.0*l[1]*(4.0*l[1] - 1.0)*(8.0*l[2] - 1.0);
  return (const REAL *) grd;
}

static const REAL_B *D2_phi4e01_2d(const REAL_B l, const BAS_FCTS *thisptr)
{
  static REAL_BB D2;
  D2[1][1] = 32.0*l[2]*(4.0*l[2] - 1.0);
  D2[1][2] = D2[2][1] = 4.0*(8.0*l[1] - 1.0)*(8.0*l[2] - 1.0);
  D2[2][2] = 32.0*l[1]*(4.0*l[1] - 1.0);
  return (const REAL_B *) D2;
}

static const REAL_BB *D3_phi4e01_2d(const REAL_B l, const BAS_FCTS *thisptr)
{
  static REAL_BBB D3;

  D3[1][1][2] = D3[1][2][1] = D3[2][1][1] = 256.0*l[2] - 32.0;
  D3[1][2][2] = D3[2][1][2] = D3[2][2][1] = 256.0*l[1] - 32.0;

  return (const REAL_BB *)D3;
}

static const REAL_BBB *D4_phi4e01_2d(const REAL_B l, const BAS_FCTS *thisptr)
{
  static REAL_BBBB D4;

  D4[1][1][2][2] =
    D4[2][2][1][1] =
    D4[1][2][1][2] =
    D4[2][1][2][1] =
    D4[2][1][1][2] =
    D4[1][2][2][1] = 256.0;

  return (const REAL_BBB *)D4;
}

/*--------------------------------------------------------------------------*/
/*  basisfunction 1 at edge 1                                               */
/*--------------------------------------------------------------------------*/

static REAL phi4e11_2d(const REAL_B l, const BAS_FCTS *thisptr)
{
  return (4.0*l[2] - 1.0)*l[2]*(4.0*l[0] - 1.0)*l[0]*4.0;
}

static const REAL *grd_phi4e11_2d(const REAL_B l, const BAS_FCTS *thisptr)
{
  static REAL_B grd = {0};
  grd[0] = 4.0*l[2]*(4.0*l[2] - 1.0)*(8.0*l[0] - 1.0);
  grd[2] = 4.0*(8.0*l[2] - 1.0)*l[0]*(4.0*l[0] - 1.0);
  return (const REAL *) grd;
}

static const REAL_B *D2_phi4e11_2d(const REAL_B l, const BAS_FCTS *thisptr)
{
  static REAL_BB D2;
  D2[2][2] = 32.0*l[0]*(4.0*l[0] - 1.0);
  D2[2][0] = D2[0][2] = 4.0*(8.0*l[2] - 1.0)*(8.0*l[0] - 1.0);
  D2[0][0] = 32.0*l[2]*(4.0*l[2] - 1.0);
  return (const REAL_B *) D2;
}

static const REAL_BB *D3_phi4e11_2d(const REAL_B l, const BAS_FCTS *thisptr)
{
  static REAL_BBB D3;

  D3[0][0][2] = D3[0][2][0] = D3[2][0][0] = 256.0*l[2] - 32.0;
  D3[0][2][2] = D3[2][0][2] = D3[2][2][0] = 256.0*l[0] - 32.0;

  return (const REAL_BB *)D3;
}

static const REAL_BBB *D4_phi4e11_2d(const REAL_B l, const BAS_FCTS *thisptr)
{
  static REAL_BBBB D4;

  D4[0][0][2][2] =
    D4[2][2][0][0] =
    D4[0][2][0][2] =
    D4[2][0][2][0] =
    D4[2][0][0][2] =
    D4[0][2][2][0] = 256.0;

  return (const REAL_BBB *)D4;
}

/*--------------------------------------------------------------------------*/
/*  basisfunction 1 at edge 2                                               */
/*--------------------------------------------------------------------------*/

static REAL phi4e21_2d(const REAL_B l, const BAS_FCTS *thisptr)
{
  return (4.0*l[0] - 1.0)*l[0]*(4.0*l[1] - 1.0)*l[1]*4.0;
}

static const REAL *grd_phi4e21_2d(const REAL_B l, const BAS_FCTS *thisptr)
{
  static REAL_B grd;
  grd[0] = 4.0*(8.0*l[0] - 1.0)*l[1]*(4.0*l[1] - 1.0);
  grd[1] = 4.0*l[0]*(4.0*l[0] - 1.0)*(8.0*l[1] - 1.0);
  return (const REAL *) grd;
}

static const REAL_B *D2_phi4e21_2d(const REAL_B l, const BAS_FCTS *thisptr)
{
  static REAL_BB D2;
  D2[0][0] = 32.0*l[1]*(4.0*l[1] - 1.0);
  D2[0][1] = D2[1][0] = 4.0*(8.0*l[0] - 1.0)*(8.0*l[1] - 1.0);
  D2[1][1] = 32.0*l[0]*(4.0*l[0] - 1.0);
  return (const REAL_B *) D2;
}

static const REAL_BB *D3_phi4e21_2d(const REAL_B l, const BAS_FCTS *thisptr)
{
  static REAL_BBB D3;

  D3[0][0][1] = D3[0][1][0] = D3[1][0][0] = 256.0*l[1] - 32.0;
  D3[0][1][1] = D3[1][0][1] = D3[1][1][0] = 256.0*l[0] - 32.0;

  return (const REAL_BB *)D3;
}

static const REAL_BBB *D4_phi4e21_2d(const REAL_B l, const BAS_FCTS *thisptr)
{
  static REAL_BBBB D4;

  D4[0][0][1][1] =
    D4[1][1][0][0] =
    D4[0][1][0][1] =
    D4[1][0][1][0] =
    D4[1][0][0][1] =
    D4[0][1][1][0] = 256.0;

  return (const REAL_BBB *)D4;
}

/*--------------------------------------------------------------------------*/
/*  basisfunction 2 at edge 0                                               */
/*--------------------------------------------------------------------------*/

static REAL phi4e02_2d(const REAL_B l, const BAS_FCTS *thisptr)
{
  return ((128.0*l[2] - 96.0)*l[2] + 16.0)*l[2]*l[1]/3.0;
}

static const REAL *grd_phi4e02_2d(const REAL_B l, const BAS_FCTS *thisptr)
{
  static REAL_B grd = {0};
  grd[1] = ((128*l[2] - 96.0)*l[2] + 16.0)*l[2]/3.0;
  grd[2] = ((128*l[2] - 64.0)*l[2] + 16.0/3.0)*l[1];
  return (const REAL *) grd;
}

static const REAL_B *D2_phi4e02_2d(const REAL_B l, const BAS_FCTS *thisptr)
{
  static REAL_BB D2;
  D2[2][2] = (256.0*l[2] - 64.0)*l[1];
  D2[1][2] = D2[2][1] = (128.0*l[2] - 64.0)*l[2] + 16.0/3.0;
  return (const REAL_B *) D2;
}

static const REAL_BB *D3_phi4e02_2d(const REAL_B l, const BAS_FCTS *thisptr)
{
  static REAL_BBB D3;

  D3[2][2][2] = 256.0*l[1];
  D3[1][2][2] = D3[2][1][2] = D3[2][2][1] = 256.0*l[2] - 64.0;

  return (const REAL_BB *)D3;
}

static const REAL_BBB *D4_phi4e02_2d(const REAL_B l, const BAS_FCTS *thisptr)
{
  static REAL_BBBB D4;

  D4[1][2][2][2] =
    D4[2][1][2][2] =
    D4[2][2][1][2] =
    D4[2][2][2][1] = 256.0;

  return (const REAL_BBB *)D4;
}

/*--------------------------------------------------------------------------*/
/*  basisfunction 2 at edge 1                                               */
/*--------------------------------------------------------------------------*/

static REAL phi4e12_2d(const REAL_B l, const BAS_FCTS *thisptr)
{
  return ((128.0*l[0] - 96.0)*l[0] + 16.0)*l[0]*l[2]/3.0;
}

static const REAL *grd_phi4e12_2d(const REAL_B l, const BAS_FCTS *thisptr)
{
  static REAL_B grd = {0};
  grd[0] = ((128*l[0] - 64.0)*l[0] + 16.0/3.0)*l[2];
  grd[2] = ((128*l[0] - 96.0)*l[0] + 16.0)*l[0]/3.0;
  return (const REAL *) grd;
}

static const REAL_B *D2_phi4e12_2d(const REAL_B l, const BAS_FCTS *thisptr)
{
  static REAL_BB D2;
  D2[0][0] = (256.0*l[0] - 64.0)*l[2];
  D2[2][0] = D2[0][2] = (128.0*l[0] - 64.0)*l[0] + 16.0/3.0;
  return (const REAL_B *) D2;
}

static const REAL_BB *D3_phi4e12_2d(const REAL_B l, const BAS_FCTS *thisptr)
{
  static REAL_BBB D3;

  D3[0][0][0] = 256.0*l[2];
  D3[0][0][2] = D3[0][2][0] = D3[2][0][0] = 256.0*l[0] - 64.0;

  return (const REAL_BB *)D3;
}

static const REAL_BBB *D4_phi4e12_2d(const REAL_B l, const BAS_FCTS *thisptr)
{
  static REAL_BBBB D4;

  D4[2][0][0][0] =
  D4[0][2][0][0] =
  D4[0][0][2][0] =
  D4[0][0][0][2] = 256.0;

  return (const REAL_BBB *)D4;
}

/*--------------------------------------------------------------------------*/
/*  basisfunction 2 at edge 2                                               */
/*--------------------------------------------------------------------------*/

static REAL phi4e22_2d(const REAL_B l, const BAS_FCTS *thisptr)
{
  return ((128.0*l[1] - 96.0)*l[1] + 16.0)*l[1]*l[0]/3.0;
}

static const REAL *grd_phi4e22_2d(const REAL_B l, const BAS_FCTS *thisptr)
{
  static REAL_B grd = {0};
  grd[0] = ((128*l[1] - 96.0)*l[1] + 16.0)*l[1]/3.0;
  grd[1] = ((128*l[1] - 64.0)*l[1] + 16.0/3.0)*l[0];
  return (const REAL *) grd;
}

static const REAL_B *D2_phi4e22_2d(const REAL_B l, const BAS_FCTS *thisptr)
{
  static REAL_BB D2;
  D2[1][1] = (256.0*l[1] - 64.0)*l[0];
  D2[0][1] = D2[1][0] = (128.0*l[1] - 64.0)*l[1] + 16.0/3.0;
  return (const REAL_B *) D2;
}

static const REAL_BB *D3_phi4e22_2d(const REAL_B l, const BAS_FCTS *thisptr)
{
  static REAL_BBB D3;

  D3[1][1][1] = 256.0*l[0];
  D3[0][1][1] = D3[1][0][1] = D3[1][1][0] = 256.0*l[1] - 64.0;

  return (const REAL_BB *)D3;
}

static const REAL_BBB *D4_phi4e22_2d(const REAL_B l, const BAS_FCTS *thisptr)
{
  static REAL_BBBB D4;

  D4[0][1][1][1] =
    D4[1][0][1][1] =
    D4[1][1][0][1] =
    D4[1][1][1][0] = 256.0;

  return (const REAL_BBB *)D4;
}

/*--------------------------------------------------------------------------*/
/*  basisfunction 0 at center                                               */
/*--------------------------------------------------------------------------*/

static REAL phi4c0_2d(const REAL_B l, const BAS_FCTS *thisptr)
{
  return (4.0*l[0] - 1.0)*l[0]*l[1]*l[2]*32.0;
}

static const REAL *grd_phi4c0_2d(const REAL_B l, const BAS_FCTS *thisptr)
{
  static REAL_B grd = {0};
  grd[0] = 32.0*(8.0*l[0] - 1.0)*l[1]*l[2];
  grd[1] = 32.0*(4.0*l[0] - 1.0)*l[0]*l[2];
  grd[2] = 32.0*(4.0*l[0] - 1.0)*l[0]*l[1];
  return (const REAL *) grd;
}

static const REAL_B *D2_phi4c0_2d(const REAL_B l, const BAS_FCTS *thisptr)
{
  static REAL_BB D2;
  D2[0][0] = 256.0*l[1]*l[2];
  D2[0][1] = D2[1][0] = 32.0*(8.0*l[0] - 1.0)*l[2];
  D2[0][2] = D2[2][0] = 32.0*(8.0*l[0] - 1.0)*l[1];
  D2[1][2] = D2[2][1] = 32.0*(4.0*l[0] - 1.0)*l[0];
  return (const REAL_B *) D2;
}

static const REAL_BB *D3_phi4c0_2d(const REAL_B l, const BAS_FCTS *thisptr)
{
  static REAL_BBB D3;

  D3[0][0][1] = D3[0][1][0] = D3[1][0][0] = 256.0*l[2];
  D3[0][0][2] = D3[0][2][0] = D3[2][0][0] = 256.0*l[1];

  D3[0][1][2] = D3[0][2][1] = D3[1][0][2] = 
    D3[2][1][0] = D3[2][0][1] = D3[1][2][0] = 256.0*l[0] - 32.0;

  return (const REAL_BB *)D3;
}

#define D4_IDX_CYCLE_3(a, b, c)			\
  D4[a][a][b][c] =				\
    D4[a][b][a][c] =				\
    D4[a][c][b][a] =				\
    D4[a][a][c][b] =				\
    D4[a][c][a][b] =				\
    D4[a][b][c][a] =				\
    D4[c][a][b][a] =				\
    D4[b][a][a][c] =				\
    D4[c][a][a][b] =				\
    D4[b][a][c][a] =				\
    D4[c][b][a][a] =				\
    D4[b][c][a][a]

static const REAL_BBB *D4_phi4c0_2d(const REAL_B l, const BAS_FCTS *thisptr)
{
  static REAL_BBBB D4;

  D4_IDX_CYCLE_3(0, 1, 2) = 256.0;
  
  return (const REAL_BBB *)D4;
}

/*--------------------------------------------------------------------------*/
/*  basisfunction 1 at center                                               */
/*--------------------------------------------------------------------------*/

static REAL phi4c1_2d(const REAL_B l, const BAS_FCTS *thisptr)
{
  return l[0]*(4.0*l[1] - 1.0)*l[1]*l[2]*32.0;
}

static const REAL *grd_phi4c1_2d(const REAL_B l, const BAS_FCTS *thisptr)
{
  static REAL_B grd = {0};
  grd[0] = 32.0*(4.0*l[1] - 1.0)*l[1]*l[2];
  grd[1] = 32.0*(8.0*l[1] - 1.0)*l[0]*l[2];
  grd[2] = 32.0*(4.0*l[1] - 1.0)*l[0]*l[1];
  return (const REAL *) grd;
}

static const REAL_B *D2_phi4c1_2d(const REAL_B l, const BAS_FCTS *thisptr)
{
  static REAL_BB D2;
  D2[0][1] = D2[1][0] = 32.0*(8.0*l[1] - 1.0)*l[2];
  D2[0][2] = D2[2][0] = 32.0*(4.0*l[1] - 1.0)*l[1];
  D2[1][1] = 256.0*l[0]*l[2];
  D2[1][2] = D2[2][1] = 32.0*(8.0*l[1] - 1.0)*l[0];
  return (const REAL_B *) D2;
}

static const REAL_BB *D3_phi4c1_2d(const REAL_B l, const BAS_FCTS *thisptr)
{
  static REAL_BBB D3;

  D3[1][1][0] = D3[1][0][1] = D3[0][1][1] = 256.0*l[2];
  D3[1][1][2] = D3[1][2][1] = D3[2][1][1] = 256.0*l[0];

  D3[0][1][2] = D3[0][2][1] = D3[1][0][2] = 
    D3[2][1][0] = D3[2][0][1] = D3[1][2][0] = 256.0*l[1] - 32.0;

  return (const REAL_BB *)D3;
}

static const REAL_BBB *D4_phi4c1_2d(const REAL_B l, const BAS_FCTS *thisptr)
{
  static REAL_BBBB D4;

  D4_IDX_CYCLE_3(1, 0, 2) = 256.0;
  
  return (const REAL_BBB *)D4;
}

/*--------------------------------------------------------------------------*/
/*  basisfunction 2 at center                                               */
/*--------------------------------------------------------------------------*/

static REAL phi4c2_2d(const REAL_B l, const BAS_FCTS *thisptr)
{
  return l[0]*l[1]*(4.0*l[2] - 1.0)*l[2]*32.0;
}

static const REAL *grd_phi4c2_2d(const REAL_B l, const BAS_FCTS *thisptr)
{
  static REAL_B grd = {0};
  grd[0] = 32.0*(4.0*l[2] - 1.0)*l[1]*l[2];
  grd[1] = 32.0*(4.0*l[2] - 1.0)*l[0]*l[2];
  grd[2] = 32.0*(8.0*l[2] - 1.0)*l[0]*l[1];
  return (const REAL *) grd;
}

static const REAL_B *D2_phi4c2_2d(const REAL_B l, const BAS_FCTS *thisptr)
{
  static REAL_BB D2;
  D2[0][1] = D2[1][0] = 32.0*(4.0*l[2] - 1.0)*l[2];
  D2[0][2] = D2[2][0] = 32.0*(8.0*l[2] - 1.0)*l[1];
  D2[1][2] = D2[2][1] = 32.0*(8.0*l[2] - 1.0)*l[0];
  D2[2][2] = 256.0*l[0]*l[1];
  return (const REAL_B *) D2;
}

static const REAL_BB *D3_phi4c2_2d(const REAL_B l, const BAS_FCTS *thisptr)
{
  static REAL_BBB D3;

  D3[2][2][0] = D3[2][0][2] = D3[0][2][2] = 256.0*l[1];
  D3[2][2][1] = D3[2][1][2] = D3[1][2][2] = 256.0*l[0];

  D3[0][1][2] = D3[0][2][1] = D3[1][0][2] = 
    D3[2][1][0] = D3[2][0][1] = D3[1][2][0] = 256.0*l[2] - 32.0;

  return (const REAL_BB *)D3;
}

static const REAL_BBB *D4_phi4c2_2d(const REAL_B l, const BAS_FCTS *thisptr)
{
  static REAL_BBBB D4;

  D4_IDX_CYCLE_3(2, 0, 1) = 256.0;
  
  return (const REAL_BBB *)D4;
}

/******************************************************************************/

#undef DEF_EL_VEC_4_2D
#define DEF_EL_VEC_4_2D(type, name)				\
  DEF_EL_VEC_CONST(type, name, N_BAS_LAG_4_2D, N_BAS_LAG_4_2D)

#undef DEFUN_GET_EL_VEC_4_2D
#define DEFUN_GET_EL_VEC_4_2D(name, type, admin, body, ...)		\
  static const EL_##type##_VEC *					\
  get_##name##4_2d(type##_VEC_TYPE *vec, const EL *el, __VA_ARGS__)	\
  {									\
    FUNCNAME("get_"#name"4_2d");					\
    static DEF_EL_VEC_4_2D(type, rvec_space);				\
    type##_VEC_TYPE *rvec = vec ? vec : rvec_space->vec;		\
    int n0, node0, ibas, inode;						\
    DOF **dofptr = el->dof, dof;					\
									\
    DEBUG_TEST_EXIT(true, "");						\
									\
    n0 = (admin)->n0_dof[VERTEX];					\
    for (ibas = inode = 0; inode < N_VERTICES_2D; inode++, ibas++)	{ \
      dof = dofptr[inode][n0]; body;					\
    }									\
									\
    n0 = (admin)->n0_dof[EDGE];						\
    node0 = (admin)->mesh->node[EDGE];					\
    for (inode = 0; inode < N_EDGES_2D; inode++) {			\
      if (dofptr[vertex_of_edge_2d[inode][0]][0]			\
	  <								\
	  dofptr[vertex_of_edge_2d[inode][1]][0]) {			\
	dof = dofptr[node0+inode][n0];   body; ibas++;			\
	dof = dofptr[node0+inode][n0+1]; body; ibas++;			\
	dof = dofptr[node0+inode][n0+2]; body; ibas++;			\
      } else {								\
	dof = dofptr[node0+inode][n0+2]; body; ibas++;			\
	dof = dofptr[node0+inode][n0+1]; body; ibas++;			\
	dof = dofptr[node0+inode][n0];   body; ibas++;			\
      }									\
    }									\
									\
    n0 = (admin)->n0_dof[CENTER];					\
    node0 = (admin)->mesh->node[CENTER];				\
    for (inode = 0; inode < 3; inode++) {				\
      dof = dofptr[node0][n0+inode]; body; ibas++;			\
    }									\
									\
    return vec ? NULL : rvec_space;					\
  }									\
  struct _AI_semicolon_dummy

#undef DEFUN_GET_EL_DOF_VEC_4_2D
#define DEFUN_GET_EL_DOF_VEC_4_2D(name, type, ASSIGN)			\
  DEFUN_GET_EL_VEC_4_2D(_##name##_vec, type, dv->fe_space->admin,	\
			ASSIGN(dv->vec[dof], rvec[ibas]),		\
			const DOF_##type##_VEC *dv);			\
  static const EL_##type##_VEC *					\
  get_##name##_vec4_2d(type##_VEC_TYPE *vec, const EL *el,		\
		       const DOF_##type##_VEC *dv)			\
  {									\
    EL_##type##_VEC *vec_loc = dv->vec_loc;				\
    									\
    if (vec != NULL || vec_loc == NULL) {				\
      return get__##name##_vec4_2d(vec, el, dv);			\
    } else {								\
      get__##name##_vec4_2d(vec_loc->vec, el, dv);			\
      return vec_loc;							\
    }									\
  }									\
  struct _AI_semicolon_dummy
  

#undef COPY_EQ
#define COPY_EQ(a, b) (b) = (a)

/*--------------------------------------------------------------------------*/
/*  functions for combining basisfunctions with coefficients                */
/*--------------------------------------------------------------------------*/

DEFUN_GET_EL_VEC_4_2D(dof_indices, DOF, admin,
		      rvec[ibas] = dof,
		      const DOF_ADMIN *admin, const BAS_FCTS *thisptr);

static const EL_BNDRY_VEC *
get_bound4_2d(BNDRY_FLAGS *vec,
	      const EL_INFO *el_info, const BAS_FCTS *thisptr)
{
  FUNCNAME("get_bound4_2d");
  static DEF_EL_VEC_4_2D(BNDRY, rvec_space);
  BNDRY_FLAGS *rvec = vec ? vec : rvec_space->vec;
  int i, j, k;

  TEST_FLAG(FILL_BOUND, el_info);

  for (i = 0; i < N_VERTICES_2D; i++) {
    BNDRY_FLAGS_CPY(rvec[i], el_info->vertex_bound[i]);
  }

  for (j = N_VERTICES_2D, i = 0; i < N_EDGES_2D; j += 3, i++) {
    for (k = 0; k < 3; k++) {
      BNDRY_FLAGS_CPY(rvec[j+k], el_info->edge_bound[i]);
    }
  }

  for (i = 0; i < 3; i++) {
    BNDRY_FLAGS_INIT(rvec[12+i]);
    BNDRY_FLAGS_SET(rvec[12+i], el_info->face_bound[0]);
  }

  return vec ? NULL : rvec_space;
}

/*--------------------------------------------------------------------*/
/*--- function for accessing a local DOF_INT_VEC                   ---*/
/*--------------------------------------------------------------------*/

DEFUN_GET_EL_DOF_VEC_4_2D(int, INT, COPY_EQ);

/*--------------------------------------------------------------------*/
/*--- function for accessing a local DOF_REAL_VEC                  ---*/
/*--------------------------------------------------------------------*/

DEFUN_GET_EL_DOF_VEC_4_2D(real, REAL, COPY_EQ);

/*--------------------------------------------------------------------*/
/*--- function for accessing a local DOF_REAL_D_VEC                ---*/
/*--------------------------------------------------------------------*/

DEFUN_GET_EL_DOF_VEC_4_2D(real_d, REAL_D, COPY_DOW);

/*--------------------------------------------------------------------*/
/*--- function for accessing a local DOF_SCHAR_VEC                 ---*/
/*--------------------------------------------------------------------*/

DEFUN_GET_EL_DOF_VEC_4_2D(schar, SCHAR, COPY_EQ);

/*--------------------------------------------------------------------*/
/*--- function for accessing a local DOF_UCHAR_VEC                 ---*/
/*--------------------------------------------------------------------*/

DEFUN_GET_EL_DOF_VEC_4_2D(uchar, UCHAR, COPY_EQ);

/*--------------------------------------------------------------------*/
/*--- function for accessing a local DOF_PTR_VEC                   ---*/
/*--------------------------------------------------------------------*/

DEFUN_GET_EL_DOF_VEC_4_2D(ptr, PTR, COPY_EQ);

/*--------------------------------------------------------------------*/
/*--- function for accessing a local DOF_REAL_DD_VEC               ---*/
/*--------------------------------------------------------------------*/

DEFUN_GET_EL_DOF_VEC_4_2D(real_dd, REAL_DD, _AI_MCOPY_DOW);

/*--------------------------------------------------------------------*/
/*--- function for local interpolaton of scalar functions          ---*/
/*--------------------------------------------------------------------*/
 
GENERATE_INTERPOL(/**/, 4, 2, N_BAS_LAG_4_2D);

/*--------------------------------------------------------------------*/
/*--- function for local interpolaton of vector functions          ---*/
/*--------------------------------------------------------------------*/

GENERATE_INTERPOL_D(/**/, 4, 2, N_BAS_LAG_4_2D);

GENERATE_INTERPOL_DOW(/**/, 4, 2, N_BAS_LAG_4_2D);

/*--------------------------------------------------------------------------*/
/*  functions for interpolation/ restriction during refinement/coarsening   */
/*--------------------------------------------------------------------------*/

static void real_refine_inter4_2d(DOF_REAL_VEC *drv, RC_LIST_EL *list, int n)
{
  FUNCNAME("real_refine_inter4_2d");
  DOF pd[N_BAS_LAG_4_2D];
  DOF cd[N_BAS_LAG_4_2D];
  EL        *el;
  REAL      *v = NULL;
  const DOF_ADMIN *admin;
  const BAS_FCTS *bas_fcts;

  if (n < 1) return;
  el = list->el_info.el;

  GET_DOF_VEC(v, drv);
  if (!drv->fe_space)
  {
    ERROR("no fe_space in dof_real_vec %s\n", NAME(drv));
    return;
  }
  else if (!drv->fe_space->bas_fcts)
  {
    ERROR("no basis functions in fe_space %s\n", NAME(drv->fe_space));
    return;
  }
  GET_STRUCT(admin, drv->fe_space);
  GET_STRUCT(bas_fcts, drv->fe_space);

  get_dof_indices4_2d(pd, el, admin, bas_fcts);

/*--------------------------------------------------------------------------*/
/*  values on child[0]                                                      */
/*--------------------------------------------------------------------------*/

  get_dof_indices4_2d(cd, el->child[0], admin, bas_fcts);

  v[cd[2]] = v[pd[10]];
  v[cd[3]] = (0.2734375*v[pd[0]] - 0.0390625*v[pd[1]]
		+ 1.09375*v[pd[9]] - 0.546875*v[pd[10]]
		+ 0.21875*v[pd[11]]);
  v[cd[4]] = v[pd[9]];
  v[cd[5]] = (-0.0390625*v[pd[0]] + 0.0234375*v[pd[1]]
		+ 0.46875*v[pd[9]] + 0.703125*v[pd[10]]
		- 0.15625*v[pd[11]]);
  v[cd[6]] = (0.0234375*(v[pd[0]] + v[pd[1]])
		+ 0.0625*(-v[pd[3]] - v[pd[8]])
		+ 0.09375*(-v[pd[9]] - v[pd[11]]) + 0.140625*v[pd[10]]
		+ 0.5625*(v[pd[12]] + v[pd[13]]));
  v[cd[7]] = v[pd[14]];
  v[cd[8]] = (0.0390625*(-v[pd[0]] - v[pd[1]])
		+ 0.1875*(v[pd[3]] + v[pd[8]]-v[pd[12]]-v[pd[13]])
		+ 0.375*(-v[pd[4]] - v[pd[7]])
		+ 0.5*(v[pd[5]] + v[pd[6]])
		+ 0.03125*(v[pd[9]] + v[pd[11]])
		+ 0.015625*v[pd[10]] + 0.75*v[pd[14]]);
  v[cd[12]] = (0.0234375*v[pd[0]] - 0.0390625*v[pd[1]]
		 + 0.125*(v[pd[3]] - v[pd[4]] - v[pd[8]])
		 + 0.375*(v[pd[7]] + v[pd[12]] - v[pd[13]])
		 - 0.03125*v[pd[9]] - 0.046875*v[pd[10]]
		 + 0.09375*v[pd[11]] + 0.75*v[pd[14]]);
  v[cd[13]] = (0.0390625*(-v[pd[0]] - v[pd[1]]) + 0.0625*v[pd[3]]
		 + 0.3125*(v[pd[8]] - v[pd[13]])
		 + 0.15625*(v[pd[9]] + v[pd[11]])
		 - 0.234375*v[pd[10]] + 0.9375*v[pd[12]]);
  v[cd[14]] = v[pd[12]];

/*--------------------------------------------------------------------------*/
/*  values on child[1]                                                      */
/*--------------------------------------------------------------------------*/

  get_dof_indices4_2d(cd, el->child[1], admin, bas_fcts);

  v[cd[6]] = (0.0234375*v[pd[0]] - 0.0390625*v[pd[1]]
		- 0.15625*v[pd[9]] + 0.703125*v[pd[10]]
		+ 0.46875*v[pd[11]]);
  v[cd[7]] = v[pd[11]];
  v[cd[8]] = (-0.0390625*v[pd[0]] + 0.2734375*v[pd[1]]
		+ 0.21875*v[pd[9]] - 0.546875*v[pd[10]]
		+ 1.09375*v[pd[11]]);
  v[cd[12]] = (0.0390625*(-v[pd[0]] - v[pd[1]])
		 + 0.3125*(v[pd[3]] - v[pd[12]]) + 0.0625*v[pd[8]]
		 + 0.15625*(v[pd[9]] + v[pd[11]])
		 - 0.234375*v[pd[10]] + 0.9375*v[pd[13]]);
  v[cd[13]] = (-0.0390625*v[pd[0]] + 0.0234375*v[pd[1]]
		 + 0.125*(-v[pd[3]] - v[pd[7]] + v[pd[8]])
		 + 0.375*(v[pd[4]] - v[pd[12]] + v[pd[13]])
		 + 0.09375*v[pd[9]] - 0.046875*v[pd[10]]
		 - 0.03125*v[pd[11]] + 0.75*v[pd[14]]);
  v[cd[14]] = v[pd[13]];

  if (n <= 1)  return;

/*--------------------------------------------------------------------------*/
/*   adjust neighbour values                                                */
/*--------------------------------------------------------------------------*/

  el = list[1].el_info.el;

  get_dof_indices4_2d(pd, el, admin, bas_fcts);

/*--------------------------------------------------------------------------*/
/*  values on neighbour's child[0]                                          */
/*--------------------------------------------------------------------------*/

  get_dof_indices4_2d(cd, el->child[0], admin, bas_fcts);

  v[cd[6]] = (0.0234375*(v[pd[0]] + v[pd[1]])
		+ 0.0625*(-v[pd[3]] - v[pd[8]])
		+ 0.09375*(-v[pd[9]] - v[pd[11]])
		+ 0.140625*v[pd[10]] + 0.5625*(v[pd[12]] + v[pd[13]]));
  v[cd[7]] = v[pd[14]];
  v[cd[8]] = (0.0390625*(-v[pd[0]] - v[pd[1]])
		+ 0.1875*(v[pd[3]]+v[pd[8]]-v[pd[12]]-v[pd[13]])
		+ 0.375*(-v[pd[4]] - v[pd[7]])
		+ 0.5*(v[pd[5]] + v[pd[6]])
		+ 0.03125*(v[pd[9]] + v[pd[11]])
		+ 0.015625*v[pd[10]] + 0.75*v[pd[14]]);
  v[cd[12]] = (0.0234375*v[pd[0]] - 0.0390625*v[pd[1]]
		 + 0.125*(v[pd[3]] - v[pd[4]] - v[pd[8]])
		 + 0.375*(v[pd[7]] + v[pd[12]] - v[pd[13]])
		 - 0.03125*v[pd[9]] - 0.046875*v[pd[10]]
		 + 0.09375*v[pd[11]] + 0.75*v[pd[14]]);
  v[cd[13]] = (0.0390625*(-v[pd[0]] - v[pd[1]]) + 0.0625*v[pd[3]]
		 + 0.3125*(v[pd[8]] - v[pd[13]])
		 + 0.15625*(v[pd[9]] + v[pd[11]])
		 - 0.234375*v[pd[10]] + 0.9375*v[pd[12]]);
  v[cd[14]] = v[pd[12]];

/*--------------------------------------------------------------------------*/
/*  values on neighbour's child[1]                                          */
/*--------------------------------------------------------------------------*/

  get_dof_indices4_2d(cd, el->child[1], admin, bas_fcts);

  v[cd[12]] = (0.0390625*(-v[pd[0]] - v[pd[1]])
		 + 0.3125*(v[pd[3]] - v[pd[12]])
		 + 0.0625*v[pd[8]] + 0.15625*(v[pd[9]] + v[pd[11]])
		 - 0.234375*v[pd[10]] + 0.9375*v[pd[13]]);
  v[cd[13]] = (-0.0390625*v[pd[0]] + 0.0234375*v[pd[1]]
		 + 0.125*(-v[pd[3]] - v[pd[7]] + v[pd[8]])
		 + 0.375*(v[pd[4]] - v[pd[12]] + v[pd[13]])
		 + 0.09375*v[pd[9]] - 0.046875*v[pd[10]]
		 - 0.03125*v[pd[11]] + 0.75*v[pd[14]]);
  v[cd[14]] = v[pd[13]];

  return;
}


static void real_coarse_inter4_2d(DOF_REAL_VEC *drv, RC_LIST_EL *list, int n)
{
  FUNCNAME("real_coarse_inter4_2d");
  DOF pd[N_BAS_LAG_4_2D];
  DOF cd[N_BAS_LAG_4_2D];
  EL        *el;
  REAL      *v = NULL;
  const DOF_ADMIN *admin;
  const BAS_FCTS *bas_fcts;

  if (n < 1) return;
  el = list->el_info.el;

  GET_DOF_VEC(v, drv);
  if (!drv->fe_space)
  {
    ERROR("no fe_space in dof_real_vec %s\n", NAME(drv));
    return;
  }
  else if (!drv->fe_space->bas_fcts)
  {
    ERROR("no basis functions in fe_space %s\n", NAME(drv->fe_space));
    return;
  }
  GET_STRUCT(admin, drv->fe_space);
  GET_STRUCT(bas_fcts, drv->fe_space);

  get_dof_indices4_2d(pd, el, admin, bas_fcts);

/*--------------------------------------------------------------------------*/
/*  values on child[0]                                                      */
/*--------------------------------------------------------------------------*/

  get_dof_indices4_2d(cd, el->child[0], admin, bas_fcts);

  v[pd[9]] = v[cd[4]];
  v[pd[10]] = v[cd[2]];
  v[pd[12]] = v[cd[14]];
  v[pd[14]] = v[cd[7]];

/*--------------------------------------------------------------------------*/
/*  values on child[1]                                                      */
/*--------------------------------------------------------------------------*/

  get_dof_indices4_2d(cd, el->child[1], admin, bas_fcts);

  v[pd[11]] = v[cd[7]];
  v[pd[13]] = v[cd[14]];

  if (n <= 1)  return;

/*--------------------------------------------------------------------------*/
/*   adjust neighbour values                                                */
/*--------------------------------------------------------------------------*/

  el = list[1].el_info.el;

  get_dof_indices4_2d(pd, el, admin, bas_fcts);

/*--------------------------------------------------------------------------*/
/*  values on neighbour's child[0]                                          */
/*--------------------------------------------------------------------------*/

  get_dof_indices4_2d(cd, el->child[0], admin, bas_fcts);

  v[pd[12]] = v[cd[14]];
  v[pd[14]] = v[cd[7]];

/*--------------------------------------------------------------------------*/
/*  values on neighbour's child[1]                                          */
/*--------------------------------------------------------------------------*/

  get_dof_indices4_2d(cd, el->child[1], admin, bas_fcts);

  v[pd[13]] = v[cd[14]];

  return;
}

static void real_coarse_restr4_2d(DOF_REAL_VEC *drv, RC_LIST_EL *list, int n)
{
  FUNCNAME("real_coarse_restr4_2d");
  DOF pd[N_BAS_LAG_4_2D];
  DOF cd[N_BAS_LAG_4_2D];
  EL        *el;
  REAL      *v = NULL;
  const DOF_ADMIN *admin;
  const BAS_FCTS *bas_fcts;

  if (n < 1) return;
  el = list->el_info.el;

  GET_DOF_VEC(v, drv);
  if (!drv->fe_space)
  {
    ERROR("no fe_space in dof_real_vec %s\n", NAME(drv));
    return;
  }
  else if (!drv->fe_space->bas_fcts)
  {
    ERROR("no basis functions in fe_space %s\n", NAME(drv->fe_space));
    return;
  }
  GET_STRUCT(admin, drv->fe_space);
  GET_STRUCT(bas_fcts, drv->fe_space);

  get_dof_indices4_2d(pd, el, admin, bas_fcts);

/*--------------------------------------------------------------------------*/
/*  values on child[0]                                                      */
/*--------------------------------------------------------------------------*/

  get_dof_indices4_2d(cd, el->child[0], admin, bas_fcts);

  v[pd[0]] += (0.2734375*v[cd[3]]
		 + 0.0390625*(-v[cd[5]] - v[cd[8]] - v[cd[13]])
		 + 0.0234375*(v[cd[6]] + v[cd[12]]));
  v[pd[1]] += (0.0390625*(-v[cd[3]] - v[cd[8]]
			    - v[cd[12]] - v[cd[13]])
		 + 0.0234375*(v[cd[5]] + v[cd[6]]));
  v[pd[3]] += (0.0625*(-v[cd[6]] + v[cd[13]]) + 0.1875*v[cd[8]]
		 + 0.125*v[cd[12]]);
  v[pd[4]] += (-0.375*v[cd[8]] - 0.125*v[cd[12]]);
  v[pd[5]] += 0.5*v[cd[8]];
  v[pd[6]] += 0.5*v[cd[8]];
  v[pd[7]] += 0.375*(-v[cd[8]] + v[cd[12]]);
  v[pd[8]] += (-0.0625*v[cd[6]] + 0.1875*v[cd[8]] - 0.125*v[cd[12]]
		 + 0.3125*v[cd[13]]);
  v[pd[9]] = (v[cd[4]] + 1.09375*v[cd[3]] + 0.46875*v[cd[5]]
		- 0.09375*v[cd[6]] + 0.15625*v[cd[13]]
		+ 0.03125*(v[cd[8]] - v[cd[12]]));
  v[pd[10]] = (v[cd[2]] - 0.546875*v[cd[3]] + 0.703125*v[cd[5]]
		 + 0.140625*v[cd[6]] + 0.015625*v[cd[8]]
		 - 0.046875*v[cd[12]] - 0.234375*v[cd[13]]);
  v[pd[11]] = (0.21875*v[cd[3]] + 0.15625*(-v[cd[5]] + v[cd[13]])
		 + 0.09375*(-v[cd[6]] + v[cd[12]]) + 0.03125*v[cd[8]]);
  v[pd[12]] = (v[cd[14]] + 0.5625*v[cd[6]] - 0.1875*v[cd[8]]
		 + 0.375*v[cd[12]] + 0.9375*v[cd[13]]);
  v[pd[13]] = (0.5625*v[cd[6]] - 0.1875*v[cd[8]] - 0.375*v[cd[12]]
		 - 0.3125*v[cd[13]]);
  v[pd[14]] = (v[cd[7]] + 0.75*(v[cd[8]] + v[cd[12]]));

/*--------------------------------------------------------------------------*/
/*  values on child[1]                                                      */
/*--------------------------------------------------------------------------*/

  get_dof_indices4_2d(cd, el->child[1], admin, bas_fcts);

  v[pd[0]] += (0.0234375*v[cd[6]]
		 + 0.0390625*(-v[cd[8]] - v[cd[12]] - v[cd[13]]));
  v[pd[1]] += (0.0390625*(-v[cd[6]] - v[cd[12]])
		 + 0.2734375*v[cd[8]] + 0.0234375*v[cd[13]]);
  v[pd[3]] += 0.3125*v[cd[12]] - 0.125*v[cd[13]];
  v[pd[4]] += 0.375*v[cd[13]];
  v[pd[7]] += -0.125*v[cd[13]];
  v[pd[8]] += 0.0625*v[cd[12]] + 0.125*v[cd[13]];
  v[pd[9]] += (0.15625*(-v[cd[6]] + v[cd[12]]) + 0.21875*v[cd[8]]
		 + 0.09375*v[cd[13]]);
  v[pd[10]] += (0.703125*v[cd[6]] - 0.546875*v[cd[8]]
		  - 0.234375*v[cd[12]] - 0.046875*v[cd[13]]);
  v[pd[11]] += (v[cd[7]] + 0.46875*v[cd[6]] + 1.09375*v[cd[8]]
		  + 0.15625*v[cd[12]] - 0.03125*v[cd[13]]);
  v[pd[12]] += -0.3125*v[cd[12]] - 0.375*v[cd[13]];
  v[pd[13]] += v[cd[14]] + 0.9375*v[cd[12]] + 0.375*v[cd[13]];
  v[pd[14]] += 0.75*v[cd[13]];

  if (n <= 1)  return;

/*--------------------------------------------------------------------------*/
/*   adjust neighbour values                                                */
/*--------------------------------------------------------------------------*/

  el = list[1].el_info.el;

  get_dof_indices4_2d(pd, el, admin, bas_fcts);

/*--------------------------------------------------------------------------*/
/*  values on neighbour's child[0]                                          */
/*--------------------------------------------------------------------------*/

  get_dof_indices4_2d(cd, el->child[0], admin, bas_fcts);

  v[pd[0]] += (0.0234375*(v[cd[6]] + v[cd[12]])
		 + 0.0390625*(-v[cd[8]] - v[cd[13]]));
  v[pd[1]] += (0.0234375*v[cd[6]]
		 + 0.0390625*(-v[cd[8]] - v[cd[12]] - v[cd[13]]));
  v[pd[3]] += (0.0625*(-v[cd[6]] + v[cd[13]]) + 0.1875*v[cd[8]]
		 + 0.12500000*v[cd[12]]);
  v[pd[4]] += -0.375*v[cd[8]] - 0.125*v[cd[12]];
  v[pd[5]] += 0.5*v[cd[8]];
  v[pd[6]] += 0.5*v[cd[8]];
  v[pd[7]] += 0.375*(-v[cd[8]] + v[cd[12]]);
  v[pd[8]] += (-0.0625*v[cd[6]] + 0.1875*v[cd[8]] - 0.125*v[cd[12]]
		 + 0.3125*v[cd[13]]);
  v[pd[9]] += (-0.09375*v[cd[6]] + 0.03125*(v[cd[8]] - v[cd[12]])
		 + 0.15625*v[cd[13]]);
  v[pd[10]] += (0.140625*v[cd[6]] + 0.015625*v[cd[8]]
		  - 0.046875*v[cd[12]] - 0.234375*v[cd[13]]);
  v[pd[11]] += (0.09375*(-v[cd[6]] + v[cd[12]])
		  + 0.03125*v[cd[8]] + 0.15625*v[cd[13]]);
  v[pd[12]] = (v[cd[14]] + 0.5625*v[cd[6]] - 0.1875*v[cd[8]]
		 + 0.375*v[cd[12]] + 0.9375*v[cd[13]]);
  v[pd[13]] = (0.5625*v[cd[6]] - 0.1875*v[cd[8]] - 0.375*v[cd[12]]
		 - 0.3125*v[cd[13]]);
  v[pd[14]] = v[cd[7]] + 0.75*(v[cd[8]] + v[cd[12]]);

/*--------------------------------------------------------------------------*/
/*  values on neighbour's child[1]                                          */
/*--------------------------------------------------------------------------*/

  get_dof_indices4_2d(cd, el->child[1], admin, bas_fcts);

  v[pd[0]] += 0.0390625*(-v[cd[12]] - v[cd[13]]);
  v[pd[1]] += -0.0390625*v[cd[12]] + 0.0234375*v[cd[13]];
  v[pd[3]] += 0.3125*v[cd[12]] - 0.125*v[cd[13]];
  v[pd[4]] += 0.375*v[cd[13]];
  v[pd[7]] += -0.125*v[cd[13]];
  v[pd[8]] += 0.0625*v[cd[12]] + 0.125*v[cd[13]];
  v[pd[9]] += 0.15625*v[cd[12]] + 0.09375*v[cd[13]];
  v[pd[10]] += -0.234375*v[cd[12]] - 0.046875*v[cd[13]];
  v[pd[11]] += 0.15625*v[cd[12]] - 0.03125*v[cd[13]];
  v[pd[12]] += -0.3125*v[cd[12]] - 0.375*v[cd[13]];
  v[pd[13]] += v[cd[14]] + 0.9375*v[cd[12]] + 0.375*v[cd[13]];
  v[pd[14]] += 0.75*v[cd[13]];

  return;
}


static void real_d_refine_inter4_2d(DOF_REAL_D_VEC *drdv, RC_LIST_EL *list,
				    int n)
{
  FUNCNAME("real_d_refine_inter4_2d");
  DOF pd[N_BAS_LAG_4_2D];
  DOF cd[N_BAS_LAG_4_2D];
  EL        *el;
  REAL_D    *v = NULL;
  int       k;
  const DOF_ADMIN *admin;
  const BAS_FCTS  *bas_fcts;


  if (n < 1) return;
  el = list->el_info.el;

  GET_DOF_VEC(v, drdv);
  if (!drdv->fe_space)
  {
    ERROR("no fe_space in dof_real_d_vec %s\n", NAME(drdv));
    return;
  }
  else if (!drdv->fe_space->bas_fcts)
  {
    ERROR("no basis functions in fe_space %s\n", NAME(drdv->fe_space));
    return;
  }
  GET_STRUCT(admin, drdv->fe_space);
  GET_STRUCT(bas_fcts, drdv->fe_space);

  get_dof_indices4_2d(pd, el, admin, bas_fcts);

/*--------------------------------------------------------------------------*/
/*  values on child[0]                                                      */
/*--------------------------------------------------------------------------*/

  get_dof_indices4_2d(cd, el->child[0], admin, bas_fcts);

  for (k = 0; k < DIM_OF_WORLD; k++)
  {
    v[cd[2]][k] = v[pd[10]][k];
    v[cd[3]][k] = (0.2734375*v[pd[0]][k] - 0.0390625*v[pd[1]][k]
		   + 1.09375*v[pd[9]][k] - 0.546875*v[pd[10]][k]
		   + 0.21875*v[pd[11]][k]);
    v[cd[4]][k] = v[pd[9]][k];
    v[cd[5]][k] = (-0.0390625*v[pd[0]][k] + 0.0234375*v[pd[1]][k]
		   + 0.46875*v[pd[9]][k] + 0.703125*v[pd[10]][k]
		   - 0.15625*v[pd[11]][k]);
    v[cd[6]][k] = (0.0234375*(v[pd[0]][k] + v[pd[1]][k])
		   + 0.0625*(-v[pd[3]][k] - v[pd[8]][k])
		   + 0.09375*(-v[pd[9]][k] - v[pd[11]][k])
		   + 0.140625*v[pd[10]][k]
		   + 0.5625*(v[pd[12]][k] + v[pd[13]][k]));
    v[cd[7]][k] = v[pd[14]][k];
    v[cd[8]][k] = (0.0390625*(-v[pd[0]][k] - v[pd[1]][k])
		   + 0.1875*(v[pd[3]][k] + v[pd[8]][k]
			     -v[pd[12]][k]-v[pd[13]][k])
		   + 0.375*(-v[pd[4]][k] - v[pd[7]][k])
		   + 0.5*(v[pd[5]][k] + v[pd[6]][k])
		   + 0.03125*(v[pd[9]][k] + v[pd[11]][k])
		   + 0.015625*v[pd[10]][k] + 0.75*v[pd[14]][k]);
    v[cd[12]][k] = (0.0234375*v[pd[0]][k] - 0.0390625*v[pd[1]][k]
		    + 0.125*(v[pd[3]][k] - v[pd[4]][k] - v[pd[8]][k])
		    + 0.375*(v[pd[7]][k] + v[pd[12]][k]-v[pd[13]][k])
		    - 0.03125*v[pd[9]][k] - 0.046875*v[pd[10]][k]
		    + 0.09375*v[pd[11]][k] + 0.75*v[pd[14]][k]);
    v[cd[13]][k] = (0.0390625*(-v[pd[0]][k] - v[pd[1]][k])
		    + 0.0625*v[pd[3]][k]
		    + 0.3125*(v[pd[8]][k] - v[pd[13]][k])
		    + 0.15625*(v[pd[9]][k] + v[pd[11]][k])
		    - 0.234375*v[pd[10]][k] + 0.9375*v[pd[12]][k]);
    v[cd[14]][k] = v[pd[12]][k];
  }

/*--------------------------------------------------------------------------*/
/*  values on child[1]                                                      */
/*--------------------------------------------------------------------------*/

  get_dof_indices4_2d(cd, el->child[1], admin, bas_fcts);

  for (k = 0; k < DIM_OF_WORLD; k++)
  {
    v[cd[6]][k] = (0.0234375*v[pd[0]][k] - 0.0390625*v[pd[1]][k]
		   - 0.15625*v[pd[9]][k] + 0.703125*v[pd[10]][k]
		   + 0.46875*v[pd[11]][k]);
    v[cd[7]][k] = v[pd[11]][k];
    v[cd[8]][k] = (-0.0390625*v[pd[0]][k] + 0.2734375*v[pd[1]][k]
		   + 0.21875*v[pd[9]][k] - 0.546875*v[pd[10]][k]
		   + 1.09375*v[pd[11]][k]);
    v[cd[12]][k] = (0.0390625*(-v[pd[0]][k] - v[pd[1]][k])
		    + 0.3125*(v[pd[3]][k] - v[pd[12]][k])
		    + 0.0625*v[pd[8]][k]
		    + 0.15625*(v[pd[9]][k] + v[pd[11]][k])
		    - 0.234375*v[pd[10]][k] + 0.9375*v[pd[13]][k]);
    v[cd[13]][k] = (-0.0390625*v[pd[0]][k] + 0.0234375*v[pd[1]][k]
		    + 0.125*(-v[pd[3]][k] - v[pd[7]][k] + v[pd[8]][k])
		    + 0.375*(v[pd[4]][k] - v[pd[12]][k] + v[pd[13]][k])
		    + 0.09375*v[pd[9]][k] - 0.046875*v[pd[10]][k]
		    - 0.03125*v[pd[11]][k] + 0.75*v[pd[14]][k]);
    v[cd[14]][k] = v[pd[13]][k];
  }

  if (n <= 1)  return;

/*--------------------------------------------------------------------------*/
/*   adjust neighbour values                                                */
/*--------------------------------------------------------------------------*/

  el = list[1].el_info.el;

  get_dof_indices4_2d(pd, el, admin, bas_fcts);

/*--------------------------------------------------------------------------*/
/*  values on neighbour's child[0]                                          */
/*--------------------------------------------------------------------------*/

  get_dof_indices4_2d(cd, el->child[0], admin, bas_fcts);

  for (k = 0; k < DIM_OF_WORLD; k++)
  {
    v[cd[6]][k] = (0.0234375*(v[pd[0]][k] + v[pd[1]][k])
		   + 0.0625*(-v[pd[3]][k] - v[pd[8]][k])
		   + 0.09375*(-v[pd[9]][k] - v[pd[11]][k])
		   + 0.140625*v[pd[10]][k]
		   + 0.5625*(v[pd[12]][k] + v[pd[13]][k]));
    v[cd[7]][k] = v[pd[14]][k];
    v[cd[8]][k] = (0.0390625*(-v[pd[0]][k] - v[pd[1]][k])
		   + 0.1875*(v[pd[3]][k] + v[pd[8]][k]
			     - v[pd[12]][k]-v[pd[13]][k])
		   + 0.375*(-v[pd[4]][k] - v[pd[7]][k])
		   + 0.5*(v[pd[5]][k] + v[pd[6]][k])
		   + 0.03125*(v[pd[9]][k] + v[pd[11]][k])
		   + 0.015625*v[pd[10]][k] + 0.75*v[pd[14]][k]);
    v[cd[12]][k] = (0.0234375*v[pd[0]][k] - 0.0390625*v[pd[1]][k]
		    + 0.125*(v[pd[3]][k] - v[pd[4]][k] - v[pd[8]][k])
		    + 0.375*(v[pd[7]][k] + v[pd[12]][k] - v[pd[13]][k])
		    - 0.03125*v[pd[9]][k] - 0.046875*v[pd[10]][k]
		    + 0.09375*v[pd[11]][k] + 0.75*v[pd[14]][k]);
    v[cd[13]][k] = (0.0390625*(-v[pd[0]][k] - v[pd[1]][k])
		    + 0.0625*v[pd[3]][k] + 0.3125*(v[pd[8]][k] - v[pd[13]][k])
		    + 0.15625*(v[pd[9]][k] + v[pd[11]][k])
		    - 0.234375*v[pd[10]][k] + 0.9375*v[pd[12]][k]);
    v[cd[14]][k] = v[pd[12]][k];
  }
/*--------------------------------------------------------------------------*/
/*  values on neighbour's child[1]                                          */
/*--------------------------------------------------------------------------*/

  get_dof_indices4_2d(cd, el->child[1], admin, bas_fcts);

  for (k = 0; k < DIM_OF_WORLD; k++)
  {
    v[cd[12]][k] = (0.0390625*(-v[pd[0]][k] - v[pd[1]][k])
		    + 0.3125*(v[pd[3]][k] - v[pd[12]][k])
		    + 0.0625*v[pd[8]][k]
		    + 0.15625*(v[pd[9]][k] + v[pd[11]][k])
		    - 0.234375*v[pd[10]][k] + 0.9375*v[pd[13]][k]);
    v[cd[13]][k] = (-0.0390625*v[pd[0]][k] + 0.0234375*v[pd[1]][k]
		    + 0.125*(-v[pd[3]][k] - v[pd[7]][k] + v[pd[8]][k])
		    + 0.375*(v[pd[4]][k] - v[pd[12]][k] + v[pd[13]][k])
		    + 0.09375*v[pd[9]][k] - 0.046875*v[pd[10]][k]
		    - 0.03125*v[pd[11]][k] + 0.75*v[pd[14]][k]);
    v[cd[14]][k] = v[pd[13]][k];
  }
  return;
}


static void real_d_coarse_inter4_2d(DOF_REAL_D_VEC *drdv, RC_LIST_EL *list,
				    int n)
{
  FUNCNAME("real_d_coarse_inter4_2d");
  DOF pd[N_BAS_LAG_4_2D];
  DOF cd[N_BAS_LAG_4_2D];
  EL        *el;
  REAL_D    *v = NULL;
  int       k;
  const DOF_ADMIN *admin;
  const BAS_FCTS *bas_fcts;

  if (n < 1) return;
  el = list->el_info.el;

  GET_DOF_VEC(v, drdv);
  if (!drdv->fe_space)
  {
    ERROR("no fe_space in dof_real_d_vec %s\n", NAME(drdv));
    return;
  }
  else if (!drdv->fe_space->bas_fcts)
  {
    ERROR("no basis functions in fe_space %s\n", NAME(drdv->fe_space));
    return;
  }
  GET_STRUCT(admin, drdv->fe_space);
  GET_STRUCT(bas_fcts, drdv->fe_space);

  get_dof_indices4_2d(pd, el, admin, bas_fcts);

/*--------------------------------------------------------------------------*/
/*  values on child[0]                                                      */
/*--------------------------------------------------------------------------*/

  get_dof_indices4_2d(cd, el->child[0], admin, bas_fcts);

  for (k = 0; k < DIM_OF_WORLD; k++)
  {
    v[pd[9]][k] = v[cd[4]][k];
    v[pd[10]][k] = v[cd[2]][k];
    v[pd[12]][k] = v[cd[14]][k];
    v[pd[14]][k] = v[cd[7]][k];
  }

/*--------------------------------------------------------------------------*/
/*  values on child[1]                                                      */
/*--------------------------------------------------------------------------*/

  get_dof_indices4_2d(cd, el->child[1], admin, bas_fcts);

  for (k = 0; k < DIM_OF_WORLD; k++)
  {
    v[pd[11]][k] = v[cd[7]][k];
    v[pd[13]][k] = v[cd[14]][k];
  }

  if (n <= 1)  return;

/*--------------------------------------------------------------------------*/
/*   adjust neighbour values                                                */
/*--------------------------------------------------------------------------*/

  el = list[1].el_info.el;

  get_dof_indices4_2d(pd, el, admin, bas_fcts);

/*--------------------------------------------------------------------------*/
/*  values on neighbour's child[0]                                          */
/*--------------------------------------------------------------------------*/

  get_dof_indices4_2d(cd, el->child[0], admin, bas_fcts);

  for (k = 0; k < DIM_OF_WORLD; k++)
  {
    v[pd[12]][k] = v[cd[14]][k];
    v[pd[14]][k] = v[cd[7]][k];
  }
/*--------------------------------------------------------------------------*/
/*  values on neighbour's child[1]                                          */
/*--------------------------------------------------------------------------*/

  get_dof_indices4_2d(cd, el->child[1], admin, bas_fcts);

  for (k = 0; k < DIM_OF_WORLD; k++)
    v[pd[13]][k] = v[cd[14]][k];

  return;
}

static void real_d_coarse_restr4_2d(DOF_REAL_D_VEC *drdv, RC_LIST_EL *list,
				    int n)
{
  FUNCNAME("real_d_coarse_restr4_2d");
  DOF pd[N_BAS_LAG_4_2D];
  DOF cd[N_BAS_LAG_4_2D];
  EL        *el;
  REAL_D    *v = NULL;
  int       k;
  const DOF_ADMIN *admin;
  const BAS_FCTS *bas_fcts;

  if (n < 1) return;
  el = list->el_info.el;

  GET_DOF_VEC(v, drdv);
  if (!drdv->fe_space)
  {
    ERROR("no fe_space in dof_real_d_vec %s\n", NAME(drdv));
    return;
  }
  else if (!drdv->fe_space->bas_fcts)
  {
    ERROR("no basis functions in fe_space %s\n", NAME(drdv->fe_space));
    return;
  }
  GET_STRUCT(admin, drdv->fe_space);
  GET_STRUCT(bas_fcts, drdv->fe_space);

  get_dof_indices4_2d(pd, el, admin, bas_fcts);

/*--------------------------------------------------------------------------*/
/*  values on child[0]                                                      */
/*--------------------------------------------------------------------------*/

  get_dof_indices4_2d(cd, el->child[0], admin, bas_fcts);

  for (k = 0; k < DIM_OF_WORLD; k++)
  {
    v[pd[0]][k] += (0.2734375*v[cd[3]][k]
		    + 0.0390625*(-v[cd[5]][k] - v[cd[8]][k] - v[cd[13]][k])
		    + 0.0234375*(v[cd[6]][k] + v[cd[12]][k]));
    v[pd[1]][k] += (0.0390625*(-v[cd[3]][k] - v[cd[8]][k]
			       - v[cd[12]][k] - v[cd[13]][k])
		    + 0.0234375*(v[cd[5]][k] + v[cd[6]][k]));
    v[pd[3]][k] += (0.0625*(-v[cd[6]][k] + v[cd[13]][k]) + 0.1875*v[cd[8]][k]
		    + 0.125*v[cd[12]][k]);
    v[pd[4]][k] += (-0.375*v[cd[8]][k] - 0.125*v[cd[12]][k]);
    v[pd[5]][k] += 0.5*v[cd[8]][k];
    v[pd[6]][k] += 0.5*v[cd[8]][k];
    v[pd[7]][k] += 0.375*(-v[cd[8]][k] + v[cd[12]][k]);
    v[pd[8]][k] += (-0.0625*v[cd[6]][k] + 0.1875*v[cd[8]][k]
		    - 0.125*v[cd[12]][k]
		    + 0.3125*v[cd[13]][k]);
    v[pd[9]][k] = (v[cd[4]][k] + 1.09375*v[cd[3]][k] + 0.46875*v[cd[5]][k]
		   - 0.09375*v[cd[6]][k] + 0.15625*v[cd[13]][k]
		   + 0.03125*(v[cd[8]][k] - v[cd[12]][k]));
    v[pd[10]][k] = (v[cd[2]][k] - 0.546875*v[cd[3]][k] + 0.703125*v[cd[5]][k]
		    + 0.140625*v[cd[6]][k] + 0.015625*v[cd[8]][k]
		    - 0.046875*v[cd[12]][k] - 0.234375*v[cd[13]][k]);
    v[pd[11]][k] = (0.21875*v[cd[3]][k]
		    + 0.15625*(-v[cd[5]][k] + v[cd[13]][k])
		    + 0.09375*(-v[cd[6]][k] + v[cd[12]][k])
		    + 0.03125*v[cd[8]][k]);
    v[pd[12]][k] = (v[cd[14]][k] + 0.5625*v[cd[6]][k] - 0.1875*v[cd[8]][k]
		    + 0.375*v[cd[12]][k] + 0.9375*v[cd[13]][k]);
    v[pd[13]][k] = (0.5625*v[cd[6]][k] - 0.1875*v[cd[8]][k]
		    - 0.375*v[cd[12]][k] - 0.3125*v[cd[13]][k]);
    v[pd[14]][k] = (v[cd[7]][k] + 0.75*(v[cd[8]][k] + v[cd[12]][k]));
  }

/*--------------------------------------------------------------------------*/
/*  values on child[1]                                                      */
/*--------------------------------------------------------------------------*/

  get_dof_indices4_2d(cd, el->child[1], admin, bas_fcts);

  for (k = 0; k < DIM_OF_WORLD; k++)
  {
    v[pd[0]][k] += (0.0234375*v[cd[6]][k]
		    + 0.0390625*(-v[cd[8]][k] - v[cd[12]][k] - v[cd[13]][k]));
    v[pd[1]][k] += (0.0390625*(-v[cd[6]][k] - v[cd[12]][k])
		    + 0.2734375*v[cd[8]][k] + 0.0234375*v[cd[13]][k]);
    v[pd[3]][k] += 0.3125*v[cd[12]][k] - 0.125*v[cd[13]][k];
    v[pd[4]][k] += 0.375*v[cd[13]][k];
    v[pd[7]][k] += -0.125*v[cd[13]][k];
    v[pd[8]][k] += 0.0625*v[cd[12]][k] + 0.125*v[cd[13]][k];
    v[pd[9]][k] += (0.15625*(-v[cd[6]][k] + v[cd[12]][k])
		    + 0.21875*v[cd[8]][k] + 0.09375*v[cd[13]][k]);
    v[pd[10]][k] += (0.703125*v[cd[6]][k] - 0.546875*v[cd[8]][k]
		     - 0.234375*v[cd[12]][k] - 0.046875*v[cd[13]][k]);
    v[pd[11]][k] += (v[cd[7]][k] + 0.46875*v[cd[6]][k] + 1.09375*v[cd[8]][k]
		     + 0.15625*v[cd[12]][k] - 0.03125*v[cd[13]][k]);
    v[pd[12]][k] += -0.3125*v[cd[12]][k] - 0.375*v[cd[13]][k];
    v[pd[13]][k] += v[cd[14]][k] + 0.9375*v[cd[12]][k] + 0.375*v[cd[13]][k];
    v[pd[14]][k] += 0.75*v[cd[13]][k];
  }

  if (n <= 1)  return;

/*--------------------------------------------------------------------------*/
/*   adjust neighbour values                                                */
/*--------------------------------------------------------------------------*/

  el = list[1].el_info.el;

  get_dof_indices4_2d(pd, el, admin, bas_fcts);

/*--------------------------------------------------------------------------*/
/*  values on neighbour's child[0]                                          */
/*--------------------------------------------------------------------------*/

  get_dof_indices4_2d(cd, el->child[0], admin, bas_fcts);

  for (k = 0; k < DIM_OF_WORLD; k++)
  {
    v[pd[0]][k] += (0.0234375*(v[cd[6]][k] + v[cd[12]][k])
		    + 0.0390625*(-v[cd[8]][k] - v[cd[13]][k]));
    v[pd[1]][k] += (0.0234375*v[cd[6]][k]
		    + 0.0390625*(-v[cd[8]][k] - v[cd[12]][k] - v[cd[13]][k]));
    v[pd[3]][k] += (0.0625*(-v[cd[6]][k] + v[cd[13]][k]) + 0.1875*v[cd[8]][k]
		    + 0.12500000*v[cd[12]][k]);
    v[pd[4]][k] += -0.375*v[cd[8]][k] - 0.125*v[cd[12]][k];
    v[pd[5]][k] += 0.5*v[cd[8]][k];
    v[pd[6]][k] += 0.5*v[cd[8]][k];
    v[pd[7]][k] += 0.375*(-v[cd[8]][k] + v[cd[12]][k]);
    v[pd[8]][k] += (-0.0625*v[cd[6]][k] + 0.1875*v[cd[8]][k]
		    - 0.125*v[cd[12]][k] + 0.3125*v[cd[13]][k]);
    v[pd[9]][k] += (-0.09375*v[cd[6]][k]
		    + 0.03125*(v[cd[8]][k] - v[cd[12]][k])
		    + 0.15625*v[cd[13]][k]);
    v[pd[10]][k] += (0.140625*v[cd[6]][k] + 0.015625*v[cd[8]][k]
		     - 0.046875*v[cd[12]][k] - 0.234375*v[cd[13]][k]);
    v[pd[11]][k] += (0.09375*(-v[cd[6]][k] + v[cd[12]][k])
		     + 0.03125*v[cd[8]][k] + 0.15625*v[cd[13]][k]);
    v[pd[12]][k] = (v[cd[14]][k] + 0.5625*v[cd[6]][k] - 0.1875*v[cd[8]][k]
		    + 0.375*v[cd[12]][k] + 0.9375*v[cd[13]][k]);
    v[pd[13]][k] = (0.5625*v[cd[6]][k] - 0.1875*v[cd[8]][k]
		    - 0.375*v[cd[12]][k] - 0.3125*v[cd[13]][k]);
    v[pd[14]][k] = v[cd[7]][k] + 0.75*(v[cd[8]][k] + v[cd[12]][k]);
  }

/*--------------------------------------------------------------------------*/
/*  values on neighbour's child[1]                                          */
/*--------------------------------------------------------------------------*/

  get_dof_indices4_2d(cd, el->child[1], admin, bas_fcts);

  for (k = 0; k < DIM_OF_WORLD; k++)
  {
    v[pd[0]][k] += 0.0390625*(-v[cd[12]][k] - v[cd[13]][k]);
    v[pd[1]][k] += -0.0390625*v[cd[12]][k] + 0.0234375*v[cd[13]][k];
    v[pd[3]][k] += 0.3125*v[cd[12]][k] - 0.125*v[cd[13]][k];
    v[pd[4]][k] += 0.375*v[cd[13]][k];
    v[pd[7]][k] += -0.125*v[cd[13]][k];
    v[pd[8]][k] += 0.0625*v[cd[12]][k] + 0.125*v[cd[13]][k];
    v[pd[9]][k] += 0.15625*v[cd[12]][k] + 0.09375*v[cd[13]][k];
    v[pd[10]][k] += -0.234375*v[cd[12]][k] - 0.046875*v[cd[13]][k];
    v[pd[11]][k] += 0.15625*v[cd[12]][k] - 0.03125*v[cd[13]][k];
    v[pd[12]][k] += -0.3125*v[cd[12]][k] - 0.375*v[cd[13]][k];
    v[pd[13]][k] += v[cd[14]][k] + 0.9375*v[cd[12]][k] + 0.375*v[cd[13]][k];
    v[pd[14]][k] += 0.75*v[cd[13]][k];
  }

  return;
}

static const BAS_FCT phi4_2d[N_BAS_LAG_4_2D] = {
  phi4v0_2d, phi4v1_2d, phi4v2_2d,
  phi4e00_2d, phi4e01_2d, phi4e02_2d,
  phi4e10_2d, phi4e11_2d, phi4e12_2d,
  phi4e20_2d, phi4e21_2d, phi4e22_2d,
  phi4c0_2d,  phi4c1_2d,  phi4c2_2d
};
static const GRD_BAS_FCT  grd_phi4_2d[N_BAS_LAG_4_2D] = {
  grd_phi4v0_2d,  grd_phi4v1_2d,  grd_phi4v2_2d,
  grd_phi4e00_2d, grd_phi4e01_2d, grd_phi4e02_2d,
  grd_phi4e10_2d, grd_phi4e11_2d, grd_phi4e12_2d,
  grd_phi4e20_2d, grd_phi4e21_2d, grd_phi4e22_2d,
  grd_phi4c0_2d,  grd_phi4c1_2d,  grd_phi4c2_2d
};

static const D2_BAS_FCT  D2_phi4_2d[N_BAS_LAG_4_2D] = {
  D2_phi4v0_2d,  D2_phi4v1_2d,  D2_phi4v2_2d,
  D2_phi4e00_2d, D2_phi4e01_2d, D2_phi4e02_2d,
  D2_phi4e10_2d, D2_phi4e11_2d, D2_phi4e12_2d,
  D2_phi4e20_2d, D2_phi4e21_2d, D2_phi4e22_2d,
  D2_phi4c0_2d,  D2_phi4c1_2d,  D2_phi4c2_2d
};

const D3_BAS_FCT  D3_phi4_2d[N_BAS_LAG_4_2D] = {
  D3_phi4v0_2d,  D3_phi4v1_2d,  D3_phi4v2_2d,
  D3_phi4e00_2d, D3_phi4e01_2d, D3_phi4e02_2d,
  D3_phi4e10_2d, D3_phi4e11_2d, D3_phi4e12_2d,
  D3_phi4e20_2d, D3_phi4e21_2d, D3_phi4e22_2d,
  D3_phi4c0_2d,  D3_phi4c1_2d,  D3_phi4c2_2d
};

const D4_BAS_FCT  D4_phi4_2d[N_BAS_LAG_4_2D] = {
  D4_phi4v0_2d,  D4_phi4v1_2d,  D4_phi4v2_2d,
  D4_phi4e00_2d, D4_phi4e01_2d, D4_phi4e02_2d,
  D4_phi4e10_2d, D4_phi4e11_2d, D4_phi4e12_2d,
  D4_phi4e20_2d, D4_phi4e21_2d, D4_phi4e22_2d,
  D4_phi4c0_2d,  D4_phi4c1_2d,  D4_phi4c2_2d
};

/* For each wall the mapping from the wall basis functions to the
 * local DOFs on the reference element. See also submesh.c.
 */
static const int trace_mapping_lag_4_2d[N_WALLS_2D][N_BAS_LAG_4_1D] = {
  { 1, 2, 3, 4, 5}, { 2, 0, 6, 7, 8 }, { 0, 1, 9, 10, 11 }
};

static const BAS_FCTS lagrange4_2d = {
  "lagrange4_2d", 2, 1, N_BAS_LAG_4_2D, N_BAS_LAG_4_2D, 4,
  {1, 3, 3, 0}, /* VERTEX, CENTER, EDGE, FACE */
  -1, /* trace_admin */
  INIT_BFCTS_CHAIN(lagrange4_2d),
  INIT_ELEMENT_INITIALIZER(NULL, FILL_NOTHING), /* init_element + fill-flags */
  phi4_2d, grd_phi4_2d, D2_phi4_2d, D3_phi4_2d, D4_phi4_2d,
  NULL, NULL, NULL, false, /* phi_d etc. */  
  /********************/
  &lagrange4_1d, /* trace space */
  { { { trace_mapping_lag_4_2d[0],
	trace_mapping_lag_4_2d[1],
	trace_mapping_lag_4_2d[2], }, }, }, /* trace mapping */
  { N_BAS_LAG_4_1D,
    N_BAS_LAG_4_1D,
    N_BAS_LAG_4_1D, }, /* n_trace_bas_fcts */
  get_dof_indices4_2d,
  get_bound4_2d,
  interpol4_2d,
  interpol_d_4_2d,
  interpol_dow_4_2d,
  get_int_vec4_2d,
  get_real_vec4_2d,
  get_real_d_vec4_2d,
  (GET_REAL_VEC_D_TYPE)get_real_d_vec4_2d,
  get_uchar_vec4_2d,
  get_schar_vec4_2d,
  get_ptr_vec4_2d,
  get_real_dd_vec4_2d,
  real_refine_inter4_2d,
  real_coarse_inter4_2d,
  real_coarse_restr4_2d,
  real_d_refine_inter4_2d,
  real_d_coarse_inter4_2d,
  real_d_coarse_restr4_2d,
  (REF_INTER_FCT_D)real_d_refine_inter4_2d,
  (REF_INTER_FCT_D)real_d_coarse_inter4_2d,
  (REF_INTER_FCT_D)real_d_coarse_restr4_2d,
  (void *)&lag_4_2d_data
};

