#! /bin/sh

# Prepare a freshly checked-out copy:

set -x

./generate-alberta-automakefiles.sh
autoreconf --force --install
