MAINTENANCE DOCUMENTATION FOR ALBERTA PACKAGES

Daniel Koester, April 2005
koester@math.uni-augsburg.de

Claus-Justus Heine, December 2003, May 2007
claus@mathematik.uni-freiburg.de

I) Introduction and Maintenance
===============================

The alberta package is currently distributed using GNU "autoconf
2.69", "automake 1.15", and "libtool 2.4.6". Since some unusual
compilation was necessary, a few hints seemed like a good idea. Here
is the general structure of the alberta packages:

                   __________ alberta/ ___________ src/ _______|--- 1d/
                  /                         \                  |--- 2d/ 
                 /                           _____ doc/        |--- 3d/
                /                             \                |--- Common/
               /                               \               |--- alberta*/
              /                                 \
alberta      /                                   _ book-test/ _|--- 1d/
             \                                                 |--- 2d/
              \                                                |--- 3d/
               \                                               |--- 4d/
                \                                              |--- 5d/
                 \                                             |--- Common/
                  \
                   \__________ alberta_util/ ___ src/
                    \
                     \
                      \_______ add_ons/ ---- grape/ __ mesh/ ____|--- 2d/
                       \               \                         |--- 3d/
                        \               \                        |--- Common/
                         \               \
                          \               - meshtv/ ___|--- 1d/
                           \               \           |--- 2d/
                            \               \          |--- 3d/
                             \               \         |--- Common/
                              \               \
                               \               \- geomview/
                                \
                                 \_ configure.ac and other distribution
                                    files

We keep track of the "Makefile.am"s, which are processed by automake
to create "Makefile.in"s. Two exceptions:

1) The "Makefile.in"s in demo/src/*d are independently
maintained. They have a simpler structure than "real" "Makefile.in"s
produced by automake.

2) The "Makefile.alberta.in" in alberta/src/Common/, see below.

The file "configure.ac" is processed by autoconf to create
"configure". The script "configure" then processes "Makefile.in"s to
create proper Makefiles. The created Makefiles can then be used by us
to test compilation and the demo programs. All Makefiles can be called
independently from any subdirectory. The command "make install" is
used to install a package. All files are installed below a common
prefix path ("configure --prefix=PREFIX"). PREFIX defaults to
`/usr/local/' There are also several variations of "clean" targets.

For the portable building of shared libraries we use "libtool". This involves 
special compilation, e.g.

libtool --mode=compile gcc ...

and

libtool --mode=link gcc ...

By default we therefore install a configured version of "libtool",
`PREFIX/libexec/alberta/' directory. This behaviour can be turned
off by running `configure' with the "--disable-install-libtool"
switch.

II) Package contents
====================

The following subpackages are included in alberta:

1) alberta
The main package.

2) alberta/test/book-demo

A few demonstration programs meant to facilitate the first steps with
ALBERTA.

3) alberta_util
This package contains the old ALBERTA util.c file, now split into
several smaller source files.

It is compiled to form a separate utility library (message macros, 
ALBERTA memory allocation routines, BLAS solver routines and wrappers) and 
should linked to any program using the ALBERTA package. It can also be used as 
a stand-alone utility library.

4) add_ons
add_ons contains extra tools built on top of ALBERTA. At the moment it contains
GRAPE and SILO/MESHTV data visualization interfaces for ALBERTA.

III) External packages
======================

The gltools package is not part of ALBERTA. If it is detected during
installation, the file alberta/src/Common/gltools.c is included in the ALBERTA
libraries. The user can specify the path of an already installed
gltools-package as a "configure" option, see INSTALL.

Same thing applies for Open DX which is used for online visualization 
via the alberta/src/Common/dxtools.c interface.

V) How to create a distribution
===============================

Step 0)
  Become familiar with Git.

Step 1)
  At the moment, the subpackages alberta, alberta_util,
  and add_ons are maintained using the version control
  system Git. To prepare a distribution, one must first check out the
  distribution version of these packages. To this aim there exists an
  "albertadist" module. Checking it out will create the ALBERTA
  package with the directory structure described right at the
  beginning of this file.

    svn checkout svn+ssh://malaga.math.uni-augsburg.de/homes/proj/alberta/ALBERTA_SVN/trunk/albertadist

  also checks out administration directories in each module. This
  is ok, since Makefiles produced by "configure" have a fixed set of
  directories to be included in a distribution. Another possibility is

  svn export svn+ssh://malaga.math.uni-augsburg.de/homes/proj/alberta/ALBERTA_SVN/trunk/albertadist

  which is faster and strips the .svn directories.

  Actually, when making a _real_ distribution (i.e. you are not only
  playing with "make dist", you should set a tag with svn
  _first_. Afterwards, check out _that_ version of ALBERTA

Step 2)
  The next step is to adjust the desired libraries, especially the
  ALBERTA libraries. Each ALBERTA library has its own directory of the
  form "alberta_[DIM_OF_WORLD]d[_debug]" since compilation is
  different for each library version. If these settings are to be
  changed, make a new directory, and adapt the files
  "Makefile.am.template" and "generate-alberta-automakefiles.sh" in
  the top directory.

Step 3)
  Rebuild the Makefile.am's for the ALBERTA libraries, this is done by
  running the shell-script

  ./generate-alberta-automakefiles.sh

Step 4)
  Make sure you have a recent version of GNU "automake", "autoconf" and
  "libtool" and run "autoreconf" in the "albertadist" directory. We
  recommend running "autoreconf --install --force" to be certain that 
  everything will work as expected.

  The version numbers of GNU programs can be checked by running

  foobar --version

  where you have to replace foobar by the proper name of the program.

Step 3/4-alt)
  Instead of step 3) and 4) run

  fromsvnreconf.sh

  This will execute the commands from step 3) and 4).

Step 5)
  Reconfigure ALBERTA with the additional switch
  "--enable-maintainer-mode":

  ./configure BLABLABLA --enable-maintainer-mode

Step 6)
  Do "[g]make dist" at the top level.  This creates alberta-?.?.tar.gz
  in which all CVS-directories, ~-files, and other unnecessary
  components are stripped. To be a bit more precise:

  a) All sources and headers are included, of course.

  b) All "Makefile.am"s and "Makefile.in"s are included. Ideally, the
  user simply enters "configure" followed by "[g]make", "[g]make
  install" to install everything.

  c) "configure" AND "configure.ac" are included, so that possible
  installation problems can (hopefully) be corrected after feedback
  with us.

If you have an already configured version of ALBERTA, then you can make
a distribution by running

make generate-alberta-dist

which runs the steps 4.)-5.).
