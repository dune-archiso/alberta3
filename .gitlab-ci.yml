default:
  timeout: 120 minutes

# always copy code to sub-directory src/ which is passed
# to following stages as an artifact

stages:
  - configure
  - build
  - test
  - tarball

# Debian 12 (bookworm) with GCC 12
# compiler GCC 12, Fortran, out of source build
configure-deb12-gcc12:
  stage: configure
  image: debian:bookworm
  before_script:
    - apt-get update
    - apt-get -y install
        build-essential automake pkg-config libtool gcc gfortran
  script:
    - mkdir src
    - shopt -s extglob
    - cp -r !(src) src
    - cd src
    - ./generate-alberta-automakefiles.sh
    - autoreconf --force --install
    - mkdir build ; cd build ; mkdir out ; cd out ; mkdir of ; cd of ; mkdir source ; cd source
    - ../../../../configure --disable-waiting-in-tests --disable-debug
  artifacts:
    paths:
      - "src"
    expire_in: 1 hour

build-deb12-gcc12:
  stage: build
  needs: ["configure-deb12-gcc12"]
  image: debian:bookworm
  before_script:
    - apt-get update
    - apt-get -y install
        build-essential libtool gcc
  script:
    - cd src
    - cd build/out/of/source
    - make
  artifacts:
    paths:
      - "src"
    expire_in: 1 hour

test-deb12-gcc12:
  stage: test
  needs: ["build-deb12-gcc12"]
  image: debian:bookworm
  before_script:
    - apt-get update
    - apt-get -y install
        build-essential libtool gcc
  script:
    - cd src
    - cd build/out/of/source
    - make check
    - make distclean

# Debian 13 (trixie) with Clang 16
# compiler: Clang 16, disable FEM toolbox, Dune Grid
configure-deb13-clang16-dunegrid:
  stage: configure
  image: debian:trixie
  before_script:
    - apt-get update
    - apt-get -y install
        build-essential clang automake pkg-config libtool libtirpc-dev cmake git
  script:
    - mkdir src
    - shopt -s extglob
    - cp -r !(src) src
    - cd src
    - ./generate-alberta-automakefiles.sh
    - autoreconf --force --install
    - ./configure --disable-fem-toolbox --disable-debug CC="clang"
  artifacts:
    paths:
      - "src"
    expire_in: 1 hour

test-deb13-clang16-dunegrid:
  stage: test
  needs: ["configure-deb13-clang16-dunegrid"]
  image: debian:bookworm
  before_script:
    - apt-get update
    - apt-get -y install
        build-essential clang pkg-config libtool libtirpc-dev cmake git
  script:
    - cd src
    - make install
    - cd ..
    - mkdir dune
    - cd dune
    - git clone https://gitlab.dune-project.org/core/dune-common.git
    - git clone https://gitlab.dune-project.org/core/dune-geometry.git
    - git clone https://gitlab.dune-project.org/core/dune-grid.git
    - ./dune-common/bin/dunecontrol all
    - cd dune-grid/build-cmake/dune/grid/albertagrid/test
    - make test-alberta3d-refine
    - ./test-alberta3d-refine

# Ubuntu 24.04 (noble) with GCC 13
# compiler: GCC 13, no Fortran, disable debug
configure-ubuntu2404:
  stage: configure
  image: ubuntu:24.04
  before_script:
    - apt-get update
    - apt-get -y install
        build-essential automake pkg-config libtool libtirpc-dev
        gcc
  script:
    - mkdir src
    - shopt -s extglob
    - cp -r !(src) src
    - cd src
    - ./generate-alberta-automakefiles.sh
    - autoreconf --force --install
    - ./configure --disable-debug --disable-waiting-in-tests
  artifacts:
    paths:
      - "src"
    expire_in: 1 hour

build-ubuntu2404:
  stage: build
  needs: ["configure-ubuntu2404"]
  image: ubuntu:24.04
  before_script:
    - apt-get update
    - apt-get -y install
        build-essential libtool libtirpc-dev gcc
  script:
    - cd src
    - make
  artifacts:
    paths:
      - "src"
    expire_in: 1 hour

test-ubuntu2404:
  stage: test
  needs: ["build-ubuntu2404"]
  image: ubuntu:24.04
  before_script:
    - apt-get update
    - apt-get -y install
        build-essential libtool libtirpc-dev gcc
  script:
    - cd src
    - make check
  artifacts:
    paths:
      - "src"
    expire_in: 1 hour

tarball-ubuntu2404:
  stage: tarball
  needs: ["build-ubuntu2404"]
  image: ubuntu:24.04
  before_script:
    - apt-get update
    - apt-get -y install
        build-essential libtool libtirpc-dev gcc
  script:
    - cd src
    - make dist
  artifacts:
    paths:
    - src/alberta-3.*.tar.gz

# openSuse Tumbleweed with Clang 18
# compiler: Clang, linker: mold, no Fortran, disable debug, world dims: 4 5, distcheck
configure-opensusetumbleweed:
  stage: configure
  image: opensuse/tumbleweed
  before_script:
    - zypper refresh
    - zypper install -y clang gcc mold make automake pkg-config libtool
        awk gzip find diffutils
        glibc-devel libtirpc-devel
  script:
    - mkdir src
    - shopt -s extglob
    - cp -r !(src) src
    - cd src
    - ./generate-alberta-automakefiles.sh
    - autoreconf --force --install
    - ./configure --enable-dim-of-world="4 5" --disable-debug --disable-waiting-in-tests CC="clang -B/usr/bin/mold"
  artifacts:
    paths:
      - "src"
    expire_in: 1 hour

test-opensusetumbleweed:
  stage: test
  needs: ["configure-opensusetumbleweed"]
  image: opensuse/tumbleweed
  before_script:
    - zypper refresh
    - zypper install -y clang gcc mold make libtool
        awk gzip find diffutils
        glibc-devel libtirpc-devel
  script:
    - cd src
    - make distcheck

# macOS 14, Xcode 15
configure-macOS-14-distcheck:
  stage: configure
  tags:
    - saas-macos-medium-m1
  image: macos-14-xcode-15
  before_script:
    - brew install automake
  script:
    - mkdir src
    - shopt -s extglob
    - cp -r !(src) src
    - cd src
    - ./generate-alberta-automakefiles.sh
    - autoreconf --force --install
    - ./configure --disable-waiting-in-tests --disable-debug
  artifacts:
    paths:
      - "src"
    expire_in: 1 hour

build-macOS-14-distcheck:
  stage: build
  needs: ["configure-macOS-14-distcheck"]
  tags:
    - saas-macos-medium-m1
  image: macos-14-xcode-15
  script:
    - cd src
    - make
  artifacts:
    paths:
      - "src"
    expire_in: 1 hour

test-macOS-14-distcheck:
  stage: test
  needs: ["build-macOS-14-distcheck"]
  tags:
    - saas-macos-medium-m1
  image: macos-14-xcode-15
  script:
    - cd src
    - make distcheck
