#! /bin/sh

# A script to generate the various assemble function flavours from the
# template files alberta/src/Common/assemble_fcts.c.in and
# alberta/src/Common/assemble_bndry_fcts.c.in
#
# The idea is to generate specializations for the various block-matrix
# variants while still only consuming a moderate amount of compilation
# time. So we generate different files for different flavours of
# block-matrices.

SED=${SED:-sed}

if test "$#" -eq 5
then
    top_srcdir=$1
    DST_DOWB_ST=$2
    SRC_DOWB_ST=$2
    LALT_DOWB_ST=$3
    LB_DOWB_ST=$4
    C_DOWB_ST=$5
elif test "$#" -eq 3
then
    top_srcdir=$1
    DST_DOWB_ST=$2
    SRC_DOWB_ST=$3
    LALT_DOWB_ST=$3
    LB_DOWB_ST=$3
    C_DOWB_ST=$3
else
    exec 1>&2
    echo $0 TOP_SRCDIR DST_TYPE SRC_TYPE
    echo *** or ***
    echo $0 TOP_SRCDIR DST_TYPE LALt_TYPE LB_TYPE C_TYPE
    exit 1
fi

SED_STRING=""
for target in DST SRC LALT LB C; do
    DOWB_ST=${target}_DOWB_ST
    TYPE=${target}_TYPE
    RET_TYPE=${target}_RET_TYPE
    PTR_TYPE=${target}_PTR_TYPE
    DOWB_NAME=${target}_DOWB_NAME
    CONSTCAST=${target}_CONSTCAST
    type=`eval echo \$\{${DOWB_ST}\}`
    SED_STRING="${SED_STRING} -e 's|@${DOWB_ST}@|${type}|g'"
    case ${type} in
	M)
	case ${target} in
	    LALT)
	    SED_STRING="${SED_STRING} -e 's|@${RET_TYPE}@|const REAL_BDD *|g'"
	    ;;
	    LB)
	    SED_STRING="${SED_STRING} -e 's|@${RET_TYPE}@|const REAL_DD *|g'"
	    ;;
	    C)
	    SED_STRING="${SED_STRING} -e 's|@${RET_TYPE}@|const REAL_D *|g'"
	    ;;
	esac
	SED_STRING="${SED_STRING} -e 's|@${TYPE}@|REAL_DD|g'"
	SED_STRING="${SED_STRING} -e 's|@${PTR_TYPE}@|REAL_D *|g'"
	SED_STRING="${SED_STRING} -e 's|@${DOWB_NAME}@|real_dd|g'"
	SED_STRING="${SED_STRING} -e 's|@${CONSTCAST}@|(const REAL_D *)|g'"
	if test "${target}" = "LB"; then
	    SED_STRING="${SED_STRING} -e 's|@ADV_${RET_TYPE}@|const REAL_DDD *|g'"
	    SED_STRING="${SED_STRING} -e 's|@ADV_${TYPE}@|REAL_DDD|g'"
	    SED_STRING="${SED_STRING} -e 's|@ADV_${PTR_TYPE}@|REAL_DD *|g'"
	    SED_STRING="${SED_STRING} -e 's|@ADV_${DOWB_NAME}@|real_ddd|g'"
	    SED_STRING="${SED_STRING} -e 's|@ADV_${CONSTCAST}@|(const REAL_DD *)|g'"
	fi
	;;
	DM)
	case ${target} in
	    LALT)
	    SED_STRING="${SED_STRING} -e 's|@${RET_TYPE}@|const REAL_BD *|g'"
	    ;;
	    LB)
	    SED_STRING="${SED_STRING} -e 's|@${RET_TYPE}@|const REAL_D *|g'"
	    ;;
	    C)
	    SED_STRING="${SED_STRING} -e 's|@${RET_TYPE}@|const REAL *|g'"
	    ;;
	esac
	SED_STRING="${SED_STRING} -e 's|@${TYPE}@|REAL_D|g'"
	SED_STRING="${SED_STRING} -e 's|@${PTR_TYPE}@|REAL *|g'"
	SED_STRING="${SED_STRING} -e 's|@${DOWB_NAME}@|real_d|g'"
	SED_STRING="${SED_STRING} -e 's|@${CONSTCAST}@|/**/|g'"
	if test "${target}" = "LB"; then
	    SED_STRING="${SED_STRING} -e 's|@ADV_${RET_TYPE}@|const REAL_DD *|g'"
	    SED_STRING="${SED_STRING} -e 's|@ADV_${TYPE}@|REAL_DD|g'"
	    SED_STRING="${SED_STRING} -e 's|@ADV_${PTR_TYPE}@|REAL_D *|g'"
	    SED_STRING="${SED_STRING} -e 's|@ADV_${DOWB_NAME}@|real_dd|g'"
	    SED_STRING="${SED_STRING} -e 's|@ADV_${CONSTCAST}@|(const REAL_D *)|g'"
	fi
	;;
	SCM)
	case ${target} in
	    LALT)
	    SED_STRING="${SED_STRING} -e 's|@${RET_TYPE}@|const REAL_B *|g'"
	    ;;
	    LB)
	    SED_STRING="${SED_STRING} -e 's|@${RET_TYPE}@|const REAL *|g'"
	    ;;
	    C)
	    SED_STRING="${SED_STRING} -e 's|@${RET_TYPE}@|REAL |g'"
	    ;;
	esac
	SED_STRING="${SED_STRING} -e 's|@${TYPE}@|REAL|g'"
	SED_STRING="${SED_STRING} -e 's|@${PTR_TYPE}@|NOT_NEEDED|g'"
	SED_STRING="${SED_STRING} -e 's|@${DOWB_NAME}@|real|g'"
	SED_STRING="${SED_STRING} -e 's|@${CONSTCAST}@|/**/|g'"
	if test "${target}" = "LB"; then
	    SED_STRING="${SED_STRING} -e 's|@ADV_${RET_TYPE}@|const REAL_D *|g'"
	    SED_STRING="${SED_STRING} -e 's|@ADV_${TYPE}@|REAL_D|g'"
	    SED_STRING="${SED_STRING} -e 's|@ADV_${PTR_TYPE}@|REAL *|g'"
	    SED_STRING="${SED_STRING} -e 's|@ADV_${DOWB_NAME}@|real_d|g'"
	    SED_STRING="${SED_STRING} -e 's|@ADV_${CONSTCAST}@|/**/|g'"
	fi
	;;
    esac
done
SED_STRING="${SED_STRING} -e 's|@top_srcdir@|${top_srcdir}|g'"
eval ${SED} ${SED_STRING}

